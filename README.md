
# N-BRI WEBSITE
Below list of plugins used in this website, **use only free-to-use plugins! Do not used any nulled plugins**

<table>
	<tr>
		<td>Name</td>
		<td>Version</td>
		<td>Purpose</td>
		<td>Link</td>
	</tr>
	<tr>
	<td><b>Frontend</b></td>
	</tr>
	<tr>
		<td>Bootstrap</td>
		<td>4.3</td>
		<td>CSS</td>
		<td> <a href="https://getbootstrap.com/docs/4.6/getting-started/introduction/">Docs</a> </td>
	</tr>
	<tr>
		<td>FontAwesome</td>
		<td>5</td>
		<td>Icon</td>
		<td> <a href="https://fontawesome.com/">Docs</a> </td>
	</tr>
	<tr>
		<td>JQuery</td>
		<td>3.5.1</td>
		<td>Javascript</td>
		<td> <a href="https://jquery.com/">Docs</a> </td>
	</tr>
	<tr>
		<td>AnimateCSS</td>
		<td>4.0.0</td>
		<td>Animation</td>
		<td> <a href="https://animate.style/">Docs</a> </td>
	</tr>
	<tr>
		<td>SlickJS</td>
		<td>1.8.1</td>
		<td>Slider</td>
		<td> <a href="https://kenwheeler.github.io/slick/">Docs</a> </td>
	</tr>
	<tr>
		<td>SweetAlert 2</td>
		<td>10.0.2</td>
		<td>Dialog Box</td>
		<td> <a href="https://sweetalert2.github.io/">Docs</a> </td>
	</tr>
	<tr>
		<td>ThreeDots</td>
		<td>0.2.0</td>
		<td>Loading Indicator in DIalog Box & Login</td>
		<td> <a href="https://nzbin.github.io/three-dots/">Docs</a> </td>
	</tr>
	<tr>
		<td>JQueryForm</td>
		<td>4.3.0</td>
		<td>Ajax Form</td>
		<td> <a href="https://jquery-form.github.io/form/">Docs</a> </td>
	</tr>
	<tr>
		<td>AAuth</td>
		<td>2.5.1.5</td>
		<td>Authentication & Role Group</td>
		<td> <a href="https://jquery-form.github.io/form/">Docs</a> </td>
	</tr>
	<tr>
	<td><b>Backend</b></td>
	</tr>
	<tr>
		<td>Metronic 5</td>
		<td>5</td>
		<td>Template</td>
		<td> <a href="https://keenthemes.com/metronic/?page=docs">Docs</a> </td>
	</tr>
	<tr>
		<td>IonAuth</td>
		<td>3</td>
		<td>Authentication & Role Group</td>
		<td> <a href="http://benedmunds.com/ion_auth/">Docs</a> </td>
	</tr>
	<tr>
		<td>SweetAlert 2</td>
		<td>10.0.2</td>
		<td>Dialog Box</td>
		<td> <a href="https://sweetalert2.github.io/">Docs</a> </td>
	</tr>
	<tr>
		<td>JQueryForm</td>
		<td>4.3.0</td>
		<td>Ajax Form</td>
		<td> <a href="https://jquery-form.github.io/form/">Docs</a> </td>
	</tr>
	<tr>
		<td>TinyMCE</td>
		<td>4</td>
		<td>WYSIWYG Editor</td>
		<td> <a href="https://www.tiny.cloud/docs-4x/">Docs</a> </td>
	</tr>
</table>

CI Installation using **KenjisCodeIgniter** [Docs](https://packagist.org/packages/kenjis/codeigniter-composer-installer) with modification in MX Router FIle

## Folder Structure

```
codeigniter/
├── application/
	├── backend
	├── frontend
├── composer.json
├── composer.lock
├── public/
    ├── .htaccess
    └── index.php -- index for frontend
 	└── backend
		├── .htaccess
		└── index.php -- index for backend
└── vendor/
    └── codeigniter/
        └── framework/
            └── system/
└── env.php -- define environtment variable
└── website_setting.json -- stores website setting
```

## Requirements

* PHP 5.3.7 or later
* `composer` command (See [Composer Installation](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx))
* Git

## How to Use

### Install CodeIgniter

```
$ composer create-project kenjis/codeigniter-composer-installer codeigniter
```

Above command installs `public/.htaccess` to remove `index.php` in your URL. If you don't need it, please remove it.

And it changes `application/config/config.php`:

~~~
$config['composer_autoload'] = FALSE;
↓
$config['composer_autoload'] = realpath(APPPATH . '../vendor/autoload.php');
~~~

~~~
$config['index_page'] = 'index.php';
↓
$config['index_page'] = '';
~~~

#### Install Translations for System Messages

If you want to install translations for system messages:

```
$ cd /path/to/codeigniter
$ php bin/install.php translations 3.1.0
```
[Modular Extensions - HMVC](https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc):

```
$ php bin/install.php modular-extensions-hmvc codeigniter-3.x
```

[Ion Auth](https://github.com/benedmunds/CodeIgniter-Ion-Auth):

```
$ php bin/install.php ion-auth 2

```

### Run PHP built-in server (PHP 5.4 or later)

```
$ cd /path/to/codeigniter
$ bin/server.sh
```

### Update CodeIgniter

```
$ cd /path/to/codeigniter
$ composer update
```

You must update files manually if files in `application` folder or `index.php` change. Check [CodeIgniter User Guide](http://www.codeigniter.com/user_guide/installation/upgrading.html).

## Reference

* [Composer Installation](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
* [CodeIgniter](https://github.com/bcit-ci/CodeIgniter)
* [Translations for CodeIgniter System](https://github.com/bcit-ci/codeigniter3-translations)
