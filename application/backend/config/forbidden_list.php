<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['forbidden_list'] = array(
	'class' => array(
		'Auth',
		'Error_handling',
		'Matches'
	),
	'method' => array()
);