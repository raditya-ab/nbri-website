<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
require_once( BASEPATH .'database/DB'. EXT );

$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'auth/login';
$route['logout'] = 'auth/logout';

$route['news'] = 'article';
$route['news/add'] = 'article/add';
$route['news/edit/(:num)'] = 'article/edit/$1';

// Opportunities Page

// $route['opportunities'] = 'article/index/opportunities';
// $route['opportunities/add'] = 'article/add/opportunities';
// $route['opportunities/get_article_list'] = 'article/get_article_list/opportunities';

$db =& DB();
$db->select('a.id');
$db->from('articles a');
$db->where('a.id IN(SELECT DISTINCT(subcategory) FROM articles b WHERE subcategory IS NOT NULL)', NULL, FALSE);

$result = $db->get()->result();

foreach( $result as $row )
{
    $route[ 'opportunities/subcategory/'.$row->id ] = 'opportunities/subcategory/view_sub/'.$row->id;
}

// Education & Skill Page


// $route['education_and_skill'] = 'article/index/education_and_skill';
// $route['education_and_skill/add'] = 'article/add/education_and_skill';

// $route['education_and_skill'] = 'article/index/education_and_skill';
// $route['education_and_skill/add'] = 'article/add/education_and_skill';

$db =& DB();
$db->select('id');
$db->from('articles');
$db->where('category','4');
$result = $db->get()->result();

foreach( $result as $row )
{
    $route[ 'education_and_skill/edit/'.$row->id ] = 'article/index/education_and_skill/'.$row->id;
}


$db =& DB();
$db->select('id,page_slug');
$db->from('pages');
$result = $db->get()->result();

foreach( $result as $row )
{
    $route[ 'pages/'.$row->id.'/'.$row->page_slug ] = 'sections/index/'.$row->id.'/'.$row->page_slug;
    // $route['product/category/'.$row->gender.'/page/(:num)'] = 'product/category/index/'.$row->gender.'/$1';
    // $route[ 'product/category/'.$row->gender.'/(:num)/(:any)' ] = 'product/detail/$1/$2';
}

$db->select('p.page_slug,s.section_slug');
$db->from('pages_section s');
$db->join('pages p', 'p.id = s.pages_id', 'left');
$result = $db->get()->result();

foreach( $result as $row )
{
    $route[ 'pages/'.$row->page_slug.'/'.$row->section_slug ] = 'sections/edit/'.$row->page_slug.'/'.$row->section_slug;
    // $route['product/cp;,ategory/'.$row->gender.'/page/(:num)'] = 'product/category/index/'.$row->gender.'/$1';
    // $route[ 'product/category/'.$row->gender.'/(:num)/(:any)' ] = 'product/detail/$1/$2';
}



$route['subresearch'] = 'research/subresearch/index';
$route['subresearch/(:num)'] = 'research/subresearch/index/$1';
$route['subresearch/(:any)'] = 'research/subresearch/$1';
$route['subresearch/get_subresearch_list/(:num)'] = 'research/subresearch/get_subresearch_list/$1';
$route['subresearch/add'] = 'research/subresearch/add/';
$route['subresearch/add/(:num)'] = 'research/subresearch/add/$1';
$route['subresearch/edit/(:num)'] = 'research/subresearch/edit/$1';
$route['subresearch/delete/(:num)'] = 'research/subresearch/delete/$1';

$db =& DB();
$select = array(
    'r.id',
    'r.research_category',
    'rc.category_name',
    'r.research_name',
    'rc.slug as category_slug',
    'r.slug',
    'r.filename',
    'r.img_thumb',
    'r.content'
);
$db->select($select);
$db->from('research r');
$db->join('research_category rc', 'rc.id = r.research_category', 'left');

$result = $db->get()->result();

foreach( $result as $row )
{
    $route[ 'subresearch/'.$row->id ] = 'research/subresearch/index/'.$row->id;
}
