<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('common_m','common');
	}

	/*----------  URL  ----------*/

	public function generate_slug($type = 'articles', $field = NULL)
	{
		$passed_flag = FALSE;
		$slug = NULL;
		$text = $this->input->post('param', TRUE);
		$id = $this->input->post('id', TRUE);

		switch($type){
			case 'project':
				$table = 'project';
				break;
			case 'project_category':
				$table = 'project_category';
				break;
			case 'tags':
				$table = 'tags';
				break;
			case 'articles':
			default:
				$table = $type;
				break;
		}

		$this->load->helper('urlcreation');

		$i = 1;
		while($passed_flag == FALSE){
			$slug = generateSeoURL($text);

			if($i > 1){
				$slug .= '-'.$i;
			}

			if($this->common->checkIsSlugAvailable($slug, $table, $id, $field)){
				$passed_flag = TRUE;
			}

			$i++;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode(array('data' => $slug)));
	}

	public function get_website_setting(){
		if($this->input->is_ajax_request()){
			if(!$this->common->setting_been_set()){
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => 'Cannot load website setting'
					)
				);
			} else {
				$output = array(
					'status' => 1,
					'message' => array(
						'settings' => $this->common->get_website_setting()
					)
				);
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			show_404();
		}
	}

}

/* End of file Common.php */
/* Location: ./application/backend/controllers/Common.php */