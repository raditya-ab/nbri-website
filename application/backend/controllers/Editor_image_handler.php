<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editor_image_handler extends Admin_Controller {
	public function __construct()
	{
		parent::__construct();
        $this->load->library('form_validation');
		$this->load->config('accepted_origin');
		$accepted_origin = $this->config->item('accepted_origin');

		if(isset($_SERVER['HTTP_ORIGIN'])){
			if(in_array($_SERVER['HTTP_ORIGIN'], $accepted_origin)){
				header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
			} else {
				header("HTTP/1.0 403 Origin Denied");
		        return;
			}
		}

	}

    public function test()
    {
        echo FRONT_URL;
    }

	public function uploads_image_content($folder = 'articles')
	{
        if(isset($_FILES['userfile'])){
    		$this->form_validation->set_rules('userfile', 'file','trim');

        }
        if(isset($_FILES['file'])){
            $this->form_validation->set_rules('file', 'file','trim');

        }

		if ($this->form_validation->run() == FALSE) {
			$response = array(
				'status' => 'error',
			);

            if(isset($_FILES['userfile'])){
                $response['message'] = array(
                    'userfile' => form_error()
                );

            }
            if(isset($_FILES['file'])){
                $response['message'] = array(
                    'file' => form_error()
                );
            }

		} else {
            $this->load->config('upload_conf');
            $this->load->library('upload');
            $config = $this->config->item('upload_image_editor');
            $config['upload_path'] = "../assets/images/".$folder;

            if( ! file_exists($config['upload_path']))
            {
                mkdir($config['upload_path'], 0755, true);
            }

            $this->upload->initialize($config);

		 	if (isset($_FILES['userfile']))
			{
				if ($this->upload->do_upload('userfile'))
				{
					$data = $this->upload->data();

					$response = array(
                        'link' =>  'assets/images/'.$folder.'/'.$data['file_name'],
                        'location' => 'assets/images/'.$folder.'/'.$data['file_name'],
                        'origin' => $_SERVER['HTTP_ORIGIN']
					);
				}
				else
				{
					$response = array(
                        'status' => 'error',
                        'message'=> array(
                            'userfile' => $this->upload->display_errors()." $folder",
                        )
                    );
				}
			} else if (isset($_FILES['file'])) {
				if ($this->upload->do_upload('file'))
				{
					$data = $this->upload->data();

					$response = array(
                        'link' =>  'assets/images/'.$folder.'/'.$data['file_name'],
                        'location' => 'assets/images/'.$folder.'/'.$data['file_name'],
                        'origin' => $_SERVER['HTTP_ORIGIN']
                    );
				}
				else
				{
					$response = array(
						'status' => 'error',
						'message'=> array(
							'file' => $this->upload->display_errors(),
						)
					);
				}
			}
			else
			{
				$response = array(
					'status' => 'error',
					'message'=> array(
						'userfile' => 'Images can not be empty.',
					)
				);
			}

		}


		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

	public function get_list_image_news()
	{
		$image_list = array();

		$handle = opendir(realpath('../assets/images/article/'));
        while($file = readdir($handle)){
            if($file !== '.' && $file !== '..' && $file !== 'index.html' && $file !== 'index.php' ){
            	// echo $file."<br>";

                $image_list[]['url'] = ($_SERVER['HTTP_HOST'] == 'localhost') ? base_url('../assets/images/article/'.$file) : $_SERVER['HTTP_HOST'].'/assets/images/article/'.$file;
            }
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($image_list));
	}

    public function upload_section_image_content()
    {
        $this->form_validation->set_rules('userfile', 'file','trim');

        if ($this->form_validation->run() == FALSE) {
            $response = array(
                'status' => 'error',
                'message'=> array(
                    'userfile' => form_error('userfile'),
                )
            );
        } else {
            $this->load->library('upload');
            $this->load->config('upload_conf');

            $upload_conf = $this->config->item('page_section');

            $config['upload_path']      = "../assets/news/";
            $config['allowed_types']    = 'gif|jpg|png|jpeg|x-png';
            $config['max_size']         = '10249';
            $config['max_width']        = '6000';
            $config['max_height']       = '6000';
            $config['remove_spaces']    = TRUE;
            $config['encrypt_name']     = TRUE;

            $this->upload->initialize($upload_conf);

            if (isset($_FILES['userfile']))
            {
                if ($this->upload->do_upload('userfile'))
                {
                    $data = $this->upload->data();


                    $response = array(
                        'status' => 'success',
                    );

                    if($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '13.229.78.30'){
                        $response['link'] = base_url('../assets/images/page_section/'.$data['file_name']);
                        $response['location'] = base_url('../assets/images/page_section/'.$data['file_name']);
                    } else {
                        $response['link'] = base_url('../assets/images/page_section/'.$data['file_name']);
                        $response['location'] = base_url('../assets/images/page_section/'.$data['file_name']);
                    }
                }
                else
                {
                    $response = array(
                        'status' => 'error',
                        'message'=> array(
                            'title' => form_error('title'),
                            'userfile' => $this->upload->display_errors(),
                        )
                    );
                }
            } else if (isset($_FILES['file'])) {
                if ($this->upload->do_upload('file'))
                {
                    $data = $this->upload->data();

                    $response = array(
                        'status' => 'success',
                    );

                    if($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '13.229.78.30'){
                        $response['link'] = base_url('../assets/images/page_section/'.$data['file_name']);
                        $response['location'] = base_url('../assets/images/page_section/'.$data['file_name']);
                    } else {
                        $response['link'] = base_url('../assets/images/page_section/'.$data['file_name']);
                        $response['location'] = base_url('../assets/images/page_section/'.$data['file_name']);
                    }
                }
                else
                {
                    $response = array(
                        'status' => 'error',
                        'message'=> array(
                            'title' => form_error('title'),
                            'userfile' => $this->upload->display_errors(),
                        )
                    );
                }
            }
            else
            {
                $response = array(
                    'status' => 'error',
                    'message'=> array(
                        'userfile' => 'Images can not be empty.',
                    )
                );
            }

        }

        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function get_list_image_section()
    {
        $image_list = array();

        $handle = opendir(realpath('../assets/images/page_section/'));
        while($file = readdir($handle)){
            if($file !== '.' && $file !== '..' && $file !== 'index.html' && $file !== 'index.php' ){
                // echo $file."<br>";

                $image_list[]['url'] = ($_SERVER['HTTP_HOST'] == 'localhost') ? base_url('../assets/images/page_section/'.$file) : $_SERVER['HTTP_HOST'].'/assets/images/page_section/'.$file;
            }
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($image_list));
    }
}

/* End of file editor_image_handler.php */
/* Location: ./application/controllers/editor_image_handler.php */