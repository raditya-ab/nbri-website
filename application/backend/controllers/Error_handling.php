<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_handling extends Admin_Controller {

	public function show_404()
	{
		$this->load->view('404');
	}

}

/* End of file Error_handling.php */
/* Location: ./application/backend/controllers/Error_handling.php */