<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('ion_auth');
		if($this->ion_auth->logged_in() === FALSE) {
			redirect('auth/login');
		}

		// Logo
		$this->template->set_partial('logo','partials/logo');

		// Menu Navigation
		$this->template->set_partial('sidebar','partials/sidebar');

		// Topbar. Notification, Quick Action, and Profile must be declared before the topbar. Suit your needs, if you dont't need them all just remove between those three
		// $this->template->set_partial('notification','partials/notification');
		// $this->template->set_partial('quick_action','partials/quick_action');
		// $this->template->set_partial('profile_nav','partials/profile_nav');
		// $this->template->set_partial('quick_sidebar','partials/quick_sidebar');
		$this->template->set_partial('topbar','partials/topbar');

		// $this->template->set_partial('horizontal_menu','partials/horizontal_menu');
		// $this->template->set_partial('searchbox','partials/searchbox');

		// Left Nav menu. Must set layout to sub-main
		// $this->template->set_partial('left_aside','partials/left_aside');

		//Sub Header
		// $this->template->set_partial('subheader','partials/subheader');

		// Footer
		// $this->template->set_partial('footer','partials/footer');

		$this->template->set_metadata('title', !isset($this->data['metatitle']) ? 'NBRI' : $this->data['metatitle']);
		$this->template->set_metadata('description',!isset($this->data['metadesc']) ? 'NBRI Admin Page' : $this->data['metadesc']);

		// Build the template
		$this->template->set_theme('default');
	}
}

/* End of file Admin_controller.php */
/* Location: ./application/core/Admin_controller.php */