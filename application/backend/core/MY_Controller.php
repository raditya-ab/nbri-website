<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {
        protected $data;
        protected $front_url;
        protected $active_menu = 'dashboard';
        private $session_data = array();

        public function __construct()
        {
        	parent::__construct();

        	// Get Url segment
                $segment = explode('//', site_url());

                // Get current protocol
                $protocol = $segment[0];

                // Get domain name
                $front_url = str_replace('/backend','',$segment[1]);

                // Glue all pieces together
                $this->front_url = $protocol.'//'.$front_url;
                defined('FRONT_URL') OR define('FRONT_URL', $this->front_url);

                if(!array_key_exists('website_var', $_SESSION)){
                    $session_data = set_website_variables(); // get website setting from json file, put setting value into container
                    $this->session->set_userdata( $session_data ); // put setting value into session
                } else {
                    if(is_null($_SESSION['website_var'])){
                        $session_data = set_website_variables(); // get website setting from json file, put setting value into container
                        $this->session->set_userdata( $session_data ); // put setting value into session
                    }
                }
                $this->data['website_var'] = $_SESSION['website_var'];

                $segment_array = $this->uri->segment_array();


                if(!empty($segment_array)){
                    $this->active_menu = implode('_', $segment_array);
                    $this->active_menu = preg_replace('/(\_\d+)/i','',$this->active_menu);
                }

                $this->template->set('active_menu',$this->active_menu);
        }
}

/* End of file MY_Controller.php */
/* Location: ./application/backend/core/MY_Controller.php */