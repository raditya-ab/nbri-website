<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('article_m','article');
		$this->template->set_layout('main');
	}

	public function index()
	{
		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/demo/default/custom/components/datatables/base/data-ajax.new.js'),
				'async' => FALSE,
				'defer' => FALSE
			)
		));

		$this->template->build('index', $this->data);
	}

	public function get_article_list()
	{
		$data_list = array();

		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
			$article_data = $this->article->get_article_list();

			if(count($article_data['records']) > 0){

				foreach($article_data['records'] as $data){
					$row = array(
						'id' => $data['id'],
						'title' => $data['title'],
						'author' => (!is_null($data['author'])) ? $data['author'] : '-',
						'status' => $data['status'],
						'created' => date('Y-m-d', $data['created']),
						'published' => date('Y-m-d', $data['published']),
						'modified' => (is_null($data['modified'])) ? '-' : date('Y-m-d', $data['modified'])
					);

					$data_list[] = $row;
				}
			}
			$output = array(
				'data' => $data_list
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			exit('No direct script access allowed');
		}
	}

	public function add()
	{
		$this->load->library('form_validation');
		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[100]|strip_tags|xss_clean');
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[20]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('content', 'Article Content', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('tags[]', 'Tags', 'trim|strip_tags|htmlentities|xss_clean');
			$this->form_validation->set_rules('metatitle', 'Meta Title', 'trim|required|min_length[5]|max_length[20]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('metadescription', 'Meta Description', 'trim|required|min_length[5]|max_length[255]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('category', 'Article Category', 'trim|required|numeric|is_natural_no_zero|is_exist[categories.id]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('featured', 'Featured Article', 'trim|numeric|in_list[NULL,"",1]|is_natural_no_zero|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('inputfile', 'Background File', 'trim|xss_clean');
			$this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

			if ($this->form_validation->run($this) == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'slug' => form_error('slug'),
						'content' => form_error('content'),
						'tags' => form_error('tags'),
						'metatitle' => form_error('metatitle'),
						'metadescription' => form_error('metadescription'),
						'category' => form_error('category'),
						'featured' => form_error('featured')
					)
				);
			} else {
				if(!isset($_FILES['inputfile']['name'])){
					$result = $this->article->add();
					$output = $result;
				} else {
					$this->load->config('upload_conf');
					$bg_upload_conf = $this->config->item('bg_image');

					$this->load->library('upload', $bg_upload_conf);

					if ( ! $this->upload->do_upload('inputfile')){
						$output = array(
							'status' => 0,
							'message' => array(
								'inputfile' => $this->upload->display_errors()
							)
						);
					}
					else{
						$image_data = $this->upload->data();

						$result = $this->article->add($image_data['file_name']);
						$output = $result;
					}
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['categories'] = $this->article->get_categories();
			$this->data['tags'] = $this->article->get_tags();

			// $this->template->set('page_css', array(
			// 	base_url('assets/vendors/custom/froala/css/froala_editor.min.css'),
			// 	base_url('assets/vendors/custom/froala/css/froala_style.min.css')
			// ));

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
					'async' => FALSE,
					'defer' => FALSE
				),
				array(
					'url' => base_url('assets/app/js/article/new_article.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
				),
				// array(
				// 	'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
				// ),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
				),
			));

			$this->template->build('add_article', $this->data);
		}
	}

	public function save(){
		echo "<pre>";
		print_r ($this->input->post());
		echo "</pre>";
	}

	public function edit($id = NULL)
	{
		if(is_null($id) || !is_numeric($id) || !$this->article->article_exist($id)) show_404();

		$this->data['article_detail'] = $this->article->get_article_detail($id);
		$this->data['categories'] = $this->article->get_categories();
		$this->data['tags'] = $this->article->get_tags();

		$this->template->set('page_css', array(
			base_url('assets/vendors/custom/froala/css/froala_editor.min.css'),
			base_url('assets/vendors/custom/froala/css/froala_style.min.css')
		));

		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/vendors/custom/froala/js/froala_editor.min.js'),
				'async' => FALSE,
				'defer' => FALSE
			),
			array(
				'url' => base_url('assets/app/js/article/new_article.js')
			),
			array(
				'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
			)
		));

		echo "<pre>";
		print_r ($this->data['article_detail']);
		echo "</pre>";
		// $this->template->build('add_article', $this->data);
	}



	public function testingsds()
	{
		$this->load->library('controllerlist');
		echo "<pre>";
		print_r ($this->controllerlist->getControllers());
		echo "</pre>";

		// $this->template->build('test', $this->data);
	}
}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */