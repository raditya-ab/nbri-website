<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $article_data = array(
            'title' => $input['title'],
            'slug' => $input['slug'],
            'content' => $input['content'],
            'created' => time(),
            'published' => $input['published'],
            'status' => $input['status'],
            'img' => $filename
        );

        $this->db->trans_start();

        $this->db->insert('articles', $article_data);
        $article_id = $this->db->insert_id();

        $article_meta = array(
            array(
                'article_id' => $article_id,
                'key' => 'title',
                'value' => $input['metatitle']
            ),
            array(
                'article_id' => $article_id,
                'key' => 'description',
                'value' => $input['metadescription']
            )
        );

        $this->db->insert_batch('article_metas', $article_meta);

        if(!empty($input['tags']) && !is_null($input['tags'])){
            $article_tags = array();
            foreach($input['tags'] as $tag){
                $article_tags[] = array(
                    'article_id' => $article_id,
                    'tag_id' => $tag
                );
            }

            $this->db->insert_batch('article_tags', $article_tags);
        }

        $article_category = array(
            'article_id' => $article_id,
            'category_id' => $input['category']
        );
        $this->db->insert('article_categories', $article_category);

        $author_data = array(
            'article_id' => $article_id,
            'author_id' => $this->session->userdata('user_id')
        );

        $this->db->insert('author_articles', $author_data);

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'article_id' => $article_id,
                    'article_slug' => $input['slug']
                )
            );
        }

        return $output;
    }


    /*----------  Get All Data  ----------*/

	public function get_article_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            'a.id',
            'a.title',
            'a.created',
            'a.published',
            'a.modified',
            'up.first_name as author',
            'a.status'
        );

        $this->db->select($select);
        $this->db->from('articles a');
        $this->db->join('author_articles aa', 'a.id = aa.article_id', 'left');
        $this->db->join('user_profiles up', 'aa.author_id = up.user_id', 'left');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $search_select = array('a.title','up.first_name');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search);
                    }
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_categories()
    {
        $this->db->select('id, category_name, slug');
        $result = $this->db->get('categories');

        return $result->result_array();
    }

    public function get_tags()
    {
        $this->db->select('id, tag_name, slug');
        $result = $this->db->get('tags');

        return $result->result_array();

    }

    /*----------  Get Specific Data  ----------*/

    public function get_article_detail($id)
    {
        $select = array(
            'a.id',
            'a.title',
            'a.slug',
            'a.content',
            'a.created',
            'a.published',
            'a.modified',
            'up.first_name as author',
            'a.status'
        );

        $this->db->select($select);
        $this->db->from('articles a');
        $this->db->join('author_articles aa', 'a.id = aa.article_id', 'left');
        $this->db->join('user_profiles up', 'aa.author_id = up.user_id', 'left');

        $this->db->where('a.id', $id);

        $result = $this->db->get()->result_array();

        return $result;
    }

    /*----------  Checking  ----------*/

    public function article_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('articles') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */
/* Location: ./application/backend/modules/article/models/Article_m.php */