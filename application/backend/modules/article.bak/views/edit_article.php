<?php echo form_open('url', array('class'=>'m-form m-form--fit m-form--label-align-right')); ?>
<input type="hidden" name="slug" value="">
<div class="row">
	<div class="col-md-9" id="form_main">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__body">
				<div class="form-group m-form__group row">
					<label for="title">Title</label>
					<input type="text" class="form-control m-input" name="title" value="" placeholder="Article Title">
					<div class="row">
						<div class="col-12">
							<span class="m-form__help">
								<i class="la la-link"></i> <?=FRONT_URL.'articles'.'/'.'beauty'.'/';?><span id="slug_link"></span>
							</span>
						</div>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label>Content</label>
					<div id="editor-instance" style="width: 100%"></div>
				</div>
				<div class="form-group m-form__group row">
					<label for="tag">Tags</label>
					<select class="form-control m-select2" id="m_select2_3" name="param" multiple="multiple">
						<optgroup label="Alaskan/Hawaiian Time Zone">
							<option value="AK" selected>
								Alaska
							</option>
							<option value="HI">
								Hawaii
							</option>
						</optgroup>
						<optgroup label="Pacific Time Zone">
							<option value="CA">
								California
							</option>
							<option value="NV" selected>
								Nevada
							</option>
							<option value="OR">
								Oregon
							</option>
							<option value="WA">
								Washington
							</option>
						</optgroup>
						<optgroup label="Mountain Time Zone">
							<option value="AZ">
								Arizona
							</option>
							<option value="CO">
								Colorado
							</option>
							<option value="ID">
								Idaho
							</option>
							<option value="MT" selected>
								Montana
							</option>
							<option value="NE">
								Nebraska
							</option>
							<option value="NM">
								New Mexico
							</option>
							<option value="ND">
								North Dakota
							</option>
							<option value="UT">
								Utah
							</option>
							<option value="WY">
								Wyoming
							</option>
						</optgroup>
						<optgroup label="Central Time Zone">
							<option value="AL">
								Alabama
							</option>
							<option value="AR">
								Arkansas
							</option>
							<option value="IL">
								Illinois
							</option>
							<option value="IA">
								Iowa
							</option>
							<option value="KS">
								Kansas
							</option>
							<option value="KY">
								Kentucky
							</option>
							<option value="LA">
								Louisiana
							</option>
							<option value="MN">
								Minnesota
							</option>
							<option value="MS">
								Mississippi
							</option>
							<option value="MO">
								Missouri
							</option>
							<option value="OK">
								Oklahoma
							</option>
							<option value="SD">
								South Dakota
							</option>
							<option value="TX">
								Texas
							</option>
							<option value="TN">
								Tennessee
							</option>
							<option value="WI">
								Wisconsin
							</option>
						</optgroup>
						<optgroup label="Eastern Time Zone">
							<option value="CT">
								Connecticut
							</option>
							<option value="DE">
								Delaware
							</option>
							<option value="FL">
								Florida
							</option>
							<option value="GA">
								Georgia
							</option>
							<option value="IN">
								Indiana
							</option>
							<option value="ME">
								Maine
							</option>
							<option value="MD">
								Maryland
							</option>
							<option value="MA">
								Massachusetts
							</option>
							<option value="MI">
								Michigan
							</option>
							<option value="NH">
								New Hampshire
							</option>
							<option value="NJ">
								New Jersey
							</option>
							<option value="NY">
								New York
							</option>
							<option value="NC">
								North Carolina
							</option>
							<option value="OH">
								Ohio
							</option>
							<option value="PA">
								Pennsylvania
							</option>
							<option value="RI">
								Rhode Island
							</option>
							<option value="SC">
								South Carolina
							</option>
							<option value="VT">
								Vermont
							</option>
							<option value="VA">
								Virginia
							</option>
							<option value="WV">
								West Virginia
							</option>
						</optgroup>
					</select>
				</div>
				<div class="form-group m-form__group row">
					<p>The meta tags are automatically generated, or <a href="#.">you want to edit it manually.</a></p>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3" id="form_sidebar">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
				<div class="m-form__actions">
					<div class="row">
						<div class="col m--align-left m--valign-middle">
							<button type="reset" class="btn btn-secondary btn-sm">
								Save Draft
							</button>
						</div>
						<div class="col m--align-right">
							<button type="reset" class="btn btn-secondary btn-sm">
								Preview
							</button>

						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="row">
					<div class="col m--valign-middle">
						<p>
							<label><i class="la la-key"></i> Status: <span id="status"><b>Publish</b></span></label><br>
							<label><i class="fa fa-eye"></i> Visibility: <span id="visibility"><b>Public</b></span></label><br>
							<label><i class="la la-calendar"></i> Publish <span id="publish_time"><b>Immediately</b></span></label>
						</p>
					</div>
				</div>
			</div>
			<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row align-items-center">
						<div class="col m--align-left m--valign-middle">
							<button type="reset" class="btn btn-success btn-block">
								Publish
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Category
						</h3>
					</div>
				</div>
				<div class="m-portlet__head-tools">
					<ul class="m-portlet__nav">
						<li class="m-portlet__nav-item">
							<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-secondary m-btn--icon m-btn--icon-only m-btn--pill">
								<i class="la la-plus"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="form-group m-form__group row">
					<div class="m-radio-list">
						<label class="m-radio m-radio--solid m-radio--success">
							<input type="radio" name="example_5" value="1">
							Option 1
							<span></span>
						</label>
						<label class="m-radio m-radio--solid m-radio--success">
							<input type="radio" name="example_5" value="2">
							Option 2
							<span></span>
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

