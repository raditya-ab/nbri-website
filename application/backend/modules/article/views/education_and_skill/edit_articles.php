<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url('article/edit'), array('class'=>'m-form m-form--fit m-form--label-align-right form_update dropzone','id'=>'m-dropzone-one')); ?>
	<input type="hidden" name="slug" id="slug" value="<?=$article_detail['slug'];?>">
	<input type="hidden" name="own_meta" value="0">
	<input type="hidden" name="status" value="<?=$article_detail['status'];?>">
	<input type="hidden" name="published" value="<?=date('Y-m-d H:i:s', $article_detail['published']);?>">
	<div class="row">
		<div class="col-md-12">
			<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">
				<div class="m-alert__icon">
					<i class="la la-warning"></i>
				</div>
				<div class="m-alert__text">
					Oh snap! Change a few things up and try submitting again.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
			<div class="m-alert m-alert--icon alert alert-success m--hide" role="alert" id="success_msg">
				<div class="m-alert__icon">
					<i class="la la-thumbs-up"></i>
				</div>
				<div class="m-alert__text">
					Success, article has been added to database.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="title">Title</label>
						<input type="text" class="form-control m-input" name="title" id="title" placeholder="Article Title" value="<?=$article_detail['title'];?>">
						<div class="row">
							<div class="col-12">
								<span class="m-form__help">
									<i class="la la-link"></i> <?=FRONT_URL;?>news/<span id="category_slug"><?=$article_detail['category_name'];?></span>/<span id="slug_link"><?=$article_detail['slug'];?></span>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="control-label">Content</label>
						<textarea name="content" class="form-control" id="content"></textarea>
					</div>
					<div class="form-group m-form__group row">
						<label for="tag">Tags</label>
						<select class="form-control m-select2" id="m_select2_3" name="tags[]" multiple="multiple">
							<?php foreach($tags as $tag): ?>
								<option value="<?=$tag['id'];?>" data-slug="<?=$tag['slug'];?>"><?=$tag['tag_name'];?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div id="meta" style="display: none">
						<div class="form-group m-form__group row">
							<label>Meta Title</label>
							<input type="text" class="form-control m-input" name="metatitle" id="metatitle" placeholder="Meta Title" value="<?=@$article_metas['title'];?>">
						</div>
						<div class="form-group m-form__group row">
							<label>Meta Description</label>
							<input type="text" class="form-control m-input" name="metadescription" id="metadescription" placeholder="Meta Description" value="<?=@$article_metas['description'];?>">
						</div>
					</div>
					<div class="form-group m-form__group row">
						<p>The meta tags are automatically generated, <a href="javascript:void(0)" id="show_meta"> or you can edit manually.</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions">
						<div class="row">
							<div class="col m--align-left m--valign-middle">
								<button type="reset" class="btn btn-secondary btn-sm btn-submit" id="btn-draft">
									Save Draft
								</button>
							</div>
							<div class="col m--align-right">
								<button type="reset" class="btn btn-brand btn-sm btn-submit" id="btn-preview">
									Preview
								</button>

							</div>
						</div>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="row">
						<div class="col m--valign-middle">
							<p>
								<label><i class="fa fa-eye"></i> Visibility: <span id="visibility"><b>Public</b></span></label><br>
								<label><i class="la la-calendar"></i> Publish <a href="#." id="publish_time"><b>Immediately</b></a></label>
							</p>
						</div>
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row align-items-center">
							<div class="col m--align-left m--valign-middle">
								<button type="button" class="btn btn-success btn-block btn-submit" id="btn-publish">
									Publish
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Category
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-secondary m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_6">
									<i class="la la-plus"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="row">
						<div class="form-group m-form__group row">
							<div class="m-radio-list">
								<?php foreach($categories as $category): ?>
									<label class="m-radio m-radio--solid m-radio--success">
										<input type="radio" name="category" value="<?=$category['id'];?>" data-slug="<?=$category['slug'];?>">
										<?=$category['category_name'];?>
										<span></span>
									</label>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Background Title
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="row">
						<div class="form-group m-form__group row">
							<label>
								File Browser
							</label>
							<div class="m-dropzone" id="dropzone-preview">
								<div class="m-dropzone__msg dz-message needsclick">
									<h3 class="m-dropzone__msg-title">
										Drop files here or click to upload.
									</h3>
									<span class="m-dropzone__msg-desc">
										This is just a demo dropzone. Selected files are
										<strong>
											not
										</strong>
										actually uploaded.
									</span>
								</div>
							</div>
							<!-- <div></div>
							<div class="custom-file">
								<input type="file" class="custom-file-input" name="inputfile" id="customFile" accept="image/*">
								<label class="custom-file-label" for="customFile">
									Choose file
								</label>
							</div> -->
						</div>
						<div class="form-group m-form__group row">
							<div class="m-checkbox-list">
								<label class="m-checkbox m-checkbox--solid m-checkbox--success">
									<input type="checkbox" name="featured" value="1">
									Set as featured
									<span></span>
								</label>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						<i class="la la-times"></i>
					</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group m-form__group row">
					<label>Category Name</label>
					<input type="text" class="form-control m-input" name="title" id="title" placeholder="Article Title">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">
					Save changes
				</button>
			</div>
		</div>
	</div>
</div>

