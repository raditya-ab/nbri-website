<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subresearch_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $subresearch_data = array(
            'research_name' => $input['title'],
            'slug' => $input['slug'],
            'research_category' => $input['category_id'],
            'content' => $input['content'],
            'status' => $input['status'],
            'img_thumb' => $filename,
            'filename' => $filename
        );

        $this->db->trans_start();

        $this->db->insert('research', $subresearch_data);
        $subresearch_id = $this->db->insert_id();

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'subresearch_id' => $subresearch_id,
                    'subresearch_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function update($id, $filename = NULL)
    {
        $subresearch_id = $id;
        $input = $this->input->post(NULL, TRUE);
        $subresearch_data = array(
            'research_name' => $input['title'],
            'slug' => $input['slug'],
            'research_category' => $input['category_id'],
            'content' => $input['content'],
            'status' => $input['status'],
        );


        if(!is_null($filename) && !empty($filename)){
            $subresearch_data['img_thumb'] = $filename;
            $subresearch_data['filename'] = $filename;
        }

        $this->db->trans_start();

        $this->db->where('id', $id);
        $this->db->update('research', $subresearch_data);

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'subresearch_id' => $subresearch_id,
                    'subresearch_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('research');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_subresearch_list($category_id)
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            'rc.id',
            'rc.category_name',
            'rc.slug as category_slug',
            'r.id as research_id',
            'r.research_name',
            'r.slug',
            'r.img_thumb',
            'r.created',
            'r.modified',
            'r.status'
        );

        $this->db->select($select);
        $this->db->from('research r');

        if($category_id != NULL){
            $this->db->where('rc.id', $category_id);
        }

        $this->db->join('research_category rc', 'rc.id = r.research_category', 'left');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $search_select = array('rc.category_name');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search);
                    }
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('r.status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_categories()
    {
        $this->db->select('id, category_name, slug');
        $result = $this->db->get('categories');

        return $result->result_array();
    }

    public function get_tags()
    {
        $this->db->select('id, tag_name, slug');
        $result = $this->db->get('tags');

        return $result->result_array();

    }

    /*----------  Get Specific Data  ----------*/

    public function get_subresearch_detail($id)
    {
        $select = array(
            'rc.id as category_id',
            'rc.category_name',
            'rc.slug',
            'r.id',
            'r.research_name',
            'r.slug',
            'r.filename',
            'r.img_thumb',
            'r.content',
            'r.created',
            'r.modified',
            'r.status'
        );

        $this->db->select($select);
        $this->db->from('research r');
        $this->db->join('research_category rc', 'r.research_category = rc.id', 'left');
        $this->db->where('r.id', $id);

        $result = $this->db->get()->row_array();

        return $result;
    }

    public function get_article_tags($id, $value_only = FALSE){
        $this->db->select('t.id as tag_id, t.tag_name, t.slug as tag_slug');
        $this->db->from('tags t');
        $this->db->join('article_tags at', 't.id = at.tag_id', 'left');
        $this->db->join('articles a', 'at.article_id = a.id', 'left');
        $this->db->where('a.id', $id);
        $this->db->where('t.status', 1);

        if($value_only == TRUE){
            $result = $this->db->get()->result_array();

            $tags = array();
            foreach($result as $tag){
                $tags[] = $tag['tag_id'];
            }

            return $tags;
        } else {
            return $this->db->get()->result_array();
        }
    }

    /*----------  Checking  ----------*/

    public function subresearch_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('research') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */