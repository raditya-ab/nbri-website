<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->template->set_layout('main');
	}

	public function index()
	{
		$this->template->build('index', $this->data);
	}
}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */