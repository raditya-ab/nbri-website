<?php echo form_open_multipart(site_url('auth/change_password'), array('class'=>'m-form m-form--fit m-form--label-align-right form_change_password','id'=>'m-dropzone-one')); ?>
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">Change Password</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="title">Old Password</label>
						<input type="password" class="form-control m-input" name="old" id="old" placeholder="Enter Old Password">
					</div>
					<div class="form-group m-form__group row">
						<label for="title">New Password</label>
						<input type="password" class="form-control m-input" name="new" id="new" placeholder="Enter New Password">
					</div>
					<div class="form-group m-form__group row">
						<label for="title">Confirm New Password</label>
						<input type="password" class="form-control m-input" name="new_confirm" id="new_confirm" placeholder="Re-enter New Password">
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="row align-items-center">
						<div class="col m--align-left m--valign-middle">
							<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
								Save
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>