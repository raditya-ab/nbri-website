<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Education_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $article_data = array(
            'title' => $input['title'],
            'slug' => $input['slug'],
            'category' => $input['category'],
            'content' => $input['content'],
            'metatitle' => $input['metatitle'],
            'metadescription' => $input['metadescription'],
            'featured' => (isset($input['featured'])) ? $input['featured'] : 0,
            // 'created' => time()
            // 'published' => strtotime($input['published'])
            'status' => $input['status'],
            'img' => $filename
        );

        $this->db->trans_start();

        $this->db->insert('articles', $article_data);
        $article_id = $this->db->insert_id();

        if(!empty($input['tags']) && !is_null($input['tags'])){
            $article_tags = array();
            foreach($input['tags'] as $tag){
                $article_tags[] = array(
                    'article_id' => $article_id,
                    'tag_id' => $tag
                );
            }

            $this->db->insert_batch('article_tags', $article_tags);
        }

        $author_data = array(
            'article_id' => $article_id,
            'author_id' => $this->session->userdata('user_id')
        );

        $this->db->insert('author_articles', $author_data);

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'article_id' => $article_id,
                    'article_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function update($id, $filename = NULL)
    {
        $article_id = $id;
        $input = $this->input->post(NULL, TRUE);
        $article_data = array(
            'title' => $input['title'],
            'slug' => $input['slug'],
            'category' => $input['category'],
            'content' => $input['content'],
            'metatitle' => $input['metatitle'],
            'metadescription' => $input['metadescription'],
            'featured' => (isset($input['featured'])) ? $input['featured'] : 0,
            // 'modified' => time(),
            // 'published' => strtotime($input['published']),
            'status' => $input['status'],
        );


        if(!is_null($filename) && !empty($filename)) $article_data['img'] = $filename;

        $this->db->trans_start();

        $this->db->where('id', $id);
        $this->db->update('articles', $article_data);

        if(!empty($input['tags']) && !is_null($input['tags'])){
            $this->db->where('article_id', $article_id);
            $this->db->delete('article_tags');

            $article_tags = array();
            foreach($input['tags'] as $tag){
                $article_tags[] = array(
                    'article_id' => $article_id,
                    'tag_id' => $tag
                );
            }

            $this->db->insert_batch('article_tags', $article_tags);
        }

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'article_id' => $article_id,
                    'article_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->where('category', '4');
        $this->db->delete('articles');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_article_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            'a.id',
            'a.slug',
            'a.title',
            'a.created',
            'a.published',
            'a.modified',
            'up.first_name as author',
            'a.status',
            'a.featured'
        );

        $this->db->select($select);
        $this->db->from('articles a');
        $this->db->join('author_articles aa', 'a.id = aa.article_id', 'left');
        $this->db->join('user_profiles up', 'aa.author_id = up.user_id', 'left');

        $this->db->where('a.category', '4');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $search_select = array('a.title','up.first_name');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search);
                    }
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_categories()
    {
        $this->db->select('id, category_name, slug');
        $result = $this->db->get('categories');

        return $result->result_array();
    }

    public function get_tags()
    {
        $this->db->select('id, tag_name, slug');
        $result = $this->db->get('tags');

        return $result->result_array();

    }

    /*----------  Get Specific Data  ----------*/

    public function get_article_detail($id)
    {
        $select = array(
            'a.id',
            'a.title',
            'a.slug',
            'a.category',
            'c.category_name',
            'a.metatitle',
            'a.metadescription',
            'a.content',
            'a.created',
            'a.published',
            'a.modified',
            'a.featured',
            'a.img',
            'up.first_name as author',
            'CASE a.status WHEN -1 THEN "Scheduled" WHEN 0 THEN "Draft" ELSE "Published" END as status'
        );

        $this->db->select($select, FALSE);
        $this->db->from('articles a');
        $this->db->join('author_articles aa', 'a.id = aa.article_id', 'left');
        $this->db->join('user_profiles up', 'aa.author_id = up.user_id', 'left');
        $this->db->join('categories c', 'a.category = c.id', 'left');

        $this->db->where('a.id', $id);
        $this->db->where('category', '4');

        $result = $this->db->get()->row_array();

        return $result;
    }

    public function get_article_tags($id, $value_only = FALSE){
        $this->db->select('t.id as tag_id, t.tag_name, t.slug as tag_slug');
        $this->db->from('tags t');
        $this->db->join('article_tags at', 't.id = at.tag_id', 'left');
        $this->db->join('articles a', 'at.article_id = a.id', 'left');
        $this->db->where('a.id', $id);
        $this->db->where('t.status', 1);

        if($value_only == TRUE){
            $result = $this->db->get()->result_array();

            $tags = array();
            foreach($result as $tag){
                $tags[] = $tag['tag_id'];
            }

            return $tags;
        } else {
            return $this->db->get()->result_array();
        }
    }

    /*----------  Checking  ----------*/

    public function article_exist($id)
    {
        $this->db->where('id', $id);
        $this->db->where('category', '4');

        return ($this->db->count_all_results('articles') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */