<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url('expert/edit/'.$expert_detail['id']), array('class'=>'m-form m-form--fit m-form--label-align-right form_update dropzone','id'=>'m-dropzone-one')); ?>
	<input type="hidden" name="slug" id="slug" value="<?=$expert_detail['slug'];?>">

	<input type="hidden" name="status" value="<?=$expert_detail['status'];?>">

	<div class="row">
		<div class="col-md-12">
			<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">
				<div class="m-alert__icon">
					<i class="la la-warning"></i>
				</div>
				<div class="m-alert__text">
					Oh snap! Change a few things up and try submitting again.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
			<div class="m-alert m-alert--icon alert alert-success m--hide" role="alert" id="success_msg">
				<div class="m-alert__icon">
					<i class="la la-thumbs-up"></i>
				</div>
				<div class="m-alert__text">
					Success, article has been added to database.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="name">Member Name</label>
						<input type="text" class="form-control m-input" name="name" id="name" placeholder="Member Name" value="<?=$expert_detail['name'];?>">
						<!-- <div class="row">
							<div class="col-12">
								<span class="m-form__help">
									<i class="la la-link"></i> <?=FRONT_URL;?>news/<span id="category_slug">&lt;category&gt;</span>/<span id="slug_link">&lt;slug&gt;</span>
								</span>
							</div>
						</div> -->
					</div>
                    <div class="form-group m-form__group row">
                        <label for="title">Member Title</label>
                        <input type="text" class="form-control m-input" name="title" id="title" placeholder="Member Title (in NBRI)" value="<?=$expert_detail['title'];?>">
                        <!-- <div class="row">
                            <div class="col-12">
                                <span class="m-form__help">
                                    <i class="la la-link"></i> <?=FRONT_URL;?>news/<span id="category_slug">&lt;category&gt;</span>/<span id="slug_link">&lt;slug&gt;</span>
                                </span>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="control-label">Member Institution</label>
                        <textarea name="subtitle" class="form-control" id="subtitle">
                            <?php echo $expert_detail['subtitle'] ?>
                        </textarea>
                    </div>
					<div class="form-group m-form__group row">
						<label class="control-label">Description</label>
						<textarea name="content" class="form-control" id="content">
                            <?php echo $expert_detail['description'] ?>
                        </textarea>
					</div>
					<div class="form-group m-form__group row">
						<label>Member Photo</label>
						<div class="clearfix" style="width: 100%;margin-bottom: 10px">
                            <img src="<?php echo base_url('../assets/images/member/'.$expert_detail['img']); ?>" alt="<?php echo $expert_detail['img']; ?>" style="max-width: 300px">
                        </div>
                        <div></div>
						<div class="custom-file">
							<input class="custom-file-input" name="inputfile" id="inputfile" type="file">
							<label class="custom-file-label" for="inputfile">
							Choose file
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions">
						<div class="row">
							<div class="col m--align-center m--valign-middle">
								<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
									Save
								</button>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="m-portlet__body">
					<div class="row">
						<div class="col m--valign-middle">
							<p>
								<label><i class="fa fa-eye"></i> Visibility: <span id="visibility"><b>Public</b></span></label><br>
								<label><i class="la la-calendar"></i> Publish <a href="#." id="publish_time"><b>Immediately</b></a></label>
							</p>
						</div>
					</div>
				</div> -->
				<!-- <div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row align-items-center">
							<div class="col m--align-left m--valign-middle">
								<button type="reset" class="btn btn-secondary btn-block btn-sm btn-submit" id="btn-draft">
									Save Draft
								</button>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>