<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Icb_rev_2022 extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('icb_rev_2022_m', 'icb_rev');
		$this->template->set_layout('main');
	}

	public function index()
	{
		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/app/js/icb_rev_2022/index_icb_rev_2022.js'),
				'async' => FALSE,
				'defer' => FALSE
			)
		));

		$this->template->build('index', $this->data);
	}

	public function get_registration_list()
	{
		$data_list = array();

		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
			$registration_data = $this->icb_rev->get_registration_list();

			if(count($registration_data['records']) > 0){
				$no = ($registration_data['meta']['page']*$registration_data['meta']['perpage']-$registration_data['meta']['perpage'])+1;
				foreach($registration_data['records'] as $data){
					$complete_data = json_decode($data['json_value'], true);

					$row = array(
						'no' => $no,
                        'order_id' => $data['order_id'],
						'full_name' => implode(' ',[
							$complete_data['salutation'],
							$complete_data['first_name'],
							$complete_data['last_name'],
							$complete_data['suffix']
						]),
						'email' => $complete_data['email'],
                        'nationality' => $data['nationality'],
                        'occupation' => $complete_data['occupation'],
                        'package_type' => $complete_data['package_type'],
                        'currency' => $data['currency'],
                        'total_billed_in_idr' => number_format($data['total_billed_in_idr'],0,',','.'),
                        'transaction_status' => $data['transaction_status'],
                        'created_at' => $data['created_at'],
						'updated_at' => $data['updated_at']
					);

					$data_list[] = $row;

					$no++;
				}
			}
			$output = array(
				'data' => $data_list,
                'log' => $registration_data['log'],
                'debug' => $registration_data,
                'meta' => $registration_data['meta']
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			exit('No direct script access allowed');
		}
	}

	public function get_payment_status($order_id) {
		$ch = curl_init();

		// set url
		curl_setopt($ch, CURLOPT_URL, MIDTRANS_API_URL . $order_id . '/status');

		// set user agent
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: ' . base64_encode(MIDTRANS_SERVER_KEY),
			'Content-Type: application/json',
			'Accept: application/json',
		));

		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// $output contains the output string
		$result = json_decode(curl_exec($ch));

		// tutup curl
		curl_close($ch);

		$output = [
			'status' => 0
		];

		if($this->icb_rev->update_payment_status($order_id, $result->transaction_status)) {
			$output = [
				'status' => 1
			];
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */