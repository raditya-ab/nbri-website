<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Icb_rev_2022_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Get All Data  ----------*/

    public function get_all()
    {
        return $this->db->get('event_registration')->result_array();
    }
	public function get_registration_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $this->db->select('*')
        ->from('event_registration');

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field == 'search') {
                    $search_select = array('transaction_status','order_id','event_category');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search, 'BOTH');
                    }
                    $this->db->group_end();
                }
                if($field == 'transaction_status'){
                    $this->db->group_start();
                    $this->db->where('transaction_status', $search);
                    $this->db->group_end();

                }
            }

        }
        $query['totalRecords'] = $this->db->count_all_results();

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();
        $query['meta'] = array(
            'page' => $page,
            'pages' => ceil($query['totalRecords']/$per_page),
            'perpage' => $per_page,
            'total' => $query['totalRecords'],
            'sort' => $order['sort'],
            'field' => $order['field']
        );

	    $this->db->flush_cache();

		return $query;
	}

    public function update_payment_status($order_id, $transaction_status) {
        $this->db->where('order_id', $order_id);
        $this->db->update('event_registration', array('transaction_status' => $transaction_status));

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
}

/* End of file Article_m.php */