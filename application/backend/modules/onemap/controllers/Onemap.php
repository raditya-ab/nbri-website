<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onemap extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('onemap_m','onemap');
        $this->load->model('category_m');
		$this->template->set_layout('main');
	}

	public function index()
	{

        $this->data['categories'] = $this->category_m->get_categories();
		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/app/js/onemap/index_onemap.js'),
				'async' => FALSE,
				'defer' => FALSE
			)
		));

		$this->template->build('index', $this->data);
	}

	public function get_onemap_list()
	{
		$data_list = array();

		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
			$onemap_data = $this->onemap->get_onemap_list();

			if(count($onemap_data['records']) > 0){
				$no = ($onemap_data['meta']['page']*$onemap_data['meta']['perpage']-$onemap_data['meta']['perpage'])+1;
				foreach($onemap_data['records'] as $data){
					$row = array(
						'no' => $no,
                        'id' => $data['id'],
                        'name' => $data['name'],
                        'category_id' => $data['category_id'],
                        'category_name' => $data['category_name'],
                        'category_slug' => $data['category_slug'],
                        'subcategory_id' => $data['subcategory_id'],
                        'subcategory_name' => $data['subcategory_name'],
                        'subcategory_slug' => $data['subcategory_slug'],
                        'slug' => $data['slug'],
                        'img_thumb' => $data['img_thumb'],
                        'created' => date('Y-m-d', strtotime($data['created'])),
                        'modified' => ($data['modified'] != NULL && $data['modified'] != '') ? date('Y-m-d', strtotime($data['modified'])) : '-',
                        'status' => $data['status']
					);

					$data_list[] = $row;

					$no++;
				}
			}
			$output = array(
				'data' => $data_list,
                'log' => $onemap_data['log'],
                'debug' => $onemap_data,
                'meta' => $onemap_data['meta']
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			exit('No direct script access allowed');
		}
	}

	public function add()
	{
		$this->load->library('form_validation');
		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[500]|strip_tags|xss_clean');
            $this->form_validation->set_rules('category', 'Company Category', 'trim|required|numeric|addslashes|htmlentities|xss_clean');
            if(!in_array($this->input->post('category', TRUE), array(3,4))){
                $this->form_validation->set_rules('subcategory', 'Company Sub-Category', 'trim|numeric|addslashes|htmlentities|xss_clean');
            } else {
                $this->form_validation->set_rules('subcategory', 'Company Sub-Category', 'trim|required|numeric|addslashes|htmlentities|xss_clean');
            }
			$this->form_validation->set_rules('name', 'Company Name', 'trim|required|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('description', 'Company Description', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('website', 'Company Website', 'trim|required|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('address', 'Company Address', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('latitude', 'Latitude', 'trim|required|numeric|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('longitude', 'Longitude', 'trim|required|numeric|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('inputfile', 'Background File', 'trim|xss_clean');
			$this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

			if ($this->form_validation->run($this) == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				if(!isset($_FILES['inputfile']['name'])){
					$result = $this->onemap->add();
					$output = $result;
				} else {
					$this->load->config('upload_conf');
					$bg_upload_conf = $this->config->item('company');

                    if( ! file_exists($bg_upload_conf['upload_path']))
                    {
                        mkdir($config['upload_path'], 0755, true);
                    }

					$this->load->library('upload', $bg_upload_conf);

					if ( ! $this->upload->do_upload('inputfile')){
						$output = array(
							'status' => 0,
							'message' => array(
								'inputfile' => $this->upload->display_errors()
							)
						);
					}
					else{
						$image_data = $this->upload->data();

						$result = $this->onemap->add($image_data['file_name']);
						$output = $result;
					}
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['categories'] = $this->category_m->get_categories();

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/app/js/onemap/new_onemap.js')
				)
			));

			$this->template->build('add', $this->data);
		}
	}

	public function edit($id = NULL)
	{
		if(is_null($id) || !is_numeric($id) || !$this->onemap->onemap_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		if($this->input->is_ajax_request()){
			$this->load->library('form_validation');

            $this->form_validation->set_rules('id', 'ID', 'trim|required|numeric|strip_tags|xss_clean');
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[500]|strip_tags|xss_clean');
            $this->form_validation->set_rules('category', 'Company Category', 'trim|required|numeric|addslashes|htmlentities|xss_clean');
            if(!in_array($this->input->post('category', TRUE), array(3,4))){
                $this->form_validation->set_rules('subcategory', 'Company Sub-Category', 'trim|numeric|addslashes|htmlentities|xss_clean');
            } else {
                $this->form_validation->set_rules('subcategory', 'Company Sub-Category', 'trim|required|numeric|addslashes|htmlentities|xss_clean');
            }
            $this->form_validation->set_rules('name', 'Company Name', 'trim|required|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('description', 'Company Description', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('website', 'Company Website', 'trim|required|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('address', 'Company Address', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('latitude', 'Latitude', 'trim|required|numeric|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('longitude', 'Longitude', 'trim|required|numeric|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('inputfile', 'Background File', 'trim|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

			if ($this->form_validation->run($this) == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				if(!isset($_FILES['inputfile']['name'])){
					$onemap_detail = $this->onemap->get_onemap_detail($id);
					$result = $this->onemap->update($id,$onemap_detail['img_thumb']);
					$output = $result;
				} else {
					$this->load->config('upload_conf');
					$bg_upload_conf = $this->config->item('company');

					$this->load->library('upload', $bg_upload_conf);

					if ( ! $this->upload->do_upload('inputfile')){
						$output = array(
							'status' => 0,
							'message' => array(
								'inputfile' => $this->upload->display_errors()
							)
						);
					}
					else{
						$image_data = $this->upload->data();

						$result = $this->onemap->update($id, $image_data['file_name']);
						$output = $result;
					}
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['company_detail'] = $this->onemap->get_onemap_detail($id);
            $this->data['categories'] = $this->category_m->get_categories();
            $this->data['subcategories'] = $this->category_m->get_subcategories($this->data['company_detail']['category']);

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/app/js/onemap/edit_onemap.js')
				)
			));

			// echo "<pre>";
			// print_r ($this->data['onemap_metas']);
			// echo "</pre>";
			$this->template->build('edit', $this->data);
		}
	}

	public function get_onemap_image($id)
	{
		$detail = $this->onemap->get_onemap_detail($id);

		if(!is_null($detail['img'])){
			$filename = '../assets/images/companies/'.$detail['img'];
			$filesize = filesize($filename);

			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				'filename' => $detail['img'],
				'filesize' => $filesize
			)));
		} else {
			$this->output->set_content_type('application/json')->set_output(json_encode(array()));
		}
	}

	public function delete($id){
		if(is_null($id) || !is_numeric($id) || !$this->onemap->onemap_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		$detail = $this->onemap->get_onemap_detail($id);

		$this->db->trans_begin();
		if($this->onemap->delete($id)){
			if(!is_null($detail['img_thumb'])){
				if(file_exists('../assets/images/companies/'.$detail['img_thumb'])){
					if(unlink('../assets/images/companies/'.$detail['img_thumb'])){
						$output = array(
							'status' => 1,
							'message' => 'File Deleted'
						);

						$this->db->trans_commit();
					} else {
						$output = array(
							'status' => 0,
							'message' => 'Error when trying to delete file'
						);

						$this->db->trans_rollback();
					}
				} else {
					$output = array(
						'status' => 1,
						'message' => 'File not exist'
					);
					$this->db->trans_commit();
				}
			} else {
				$output = array(
					'status' => 1,
					'message' => 'Article has no image'
				);
				$this->db->trans_commit();
			}
		} else {
			$output = array(
				'status' => 0,
				'message' => $this->db->error()
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */