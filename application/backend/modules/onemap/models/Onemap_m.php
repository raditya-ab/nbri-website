<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onemap_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $onemap_data = array(
            'category' => $input['category'],
            'subcategory' => (empty($input['subcategory']))? NULL : $input['subcategory'],
            'name' => $input['name'],
            'slug' => $input['slug'],
            'description' => $input['description'],
            'address' => $input['address'],
            'latitude' => $input['latitude'],
            'longitude' => $input['longitude'],
            'website' => $input['website'],
            'status' => $input['status'],
            'img_thumb' => $filename
        );

        $this->db->trans_start();

        $this->db->insert('companies', $onemap_data);
        $onemap_id = $this->db->insert_id();

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'onemap_id' => $onemap_id,
                    'onemap_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function update($id, $filename = NULL)
    {
        $onemap_id = $id;
        $input = $this->input->post(NULL, TRUE);
        $onemap_data = array(
            'category' => $input['category'],
            'subcategory' => (empty($input['subcategory']))? NULL : $input['subcategory'],
            'name' => $input['name'],
            'slug' => $input['slug'],
            'description' => $input['description'],
            'address' => $input['address'],
            'latitude' => $input['latitude'],
            'longitude' => $input['longitude'],
            'website' => $input['website'],
            'status' => $input['status'],
            'img_thumb' => $filename
        );


        if(!is_null($filename) && !empty($filename)) $onemap_data['img_thumb'] = $filename;

        $this->db->trans_start();

        $this->db->where('id', $id);
        $this->db->update('companies', $onemap_data);

        // if(!empty($input['tags']) && !is_null($input['tags'])){
        //     $this->db->where('onemap_id', $onemap_id);
        //     $this->db->delete('onemap_tags');

        //     $onemap_tags = array();
        //     foreach($input['tags'] as $tag){
        //         $onemap_tags[] = array(
        //             'onemap_id' => $onemap_id,
        //             'tag_id' => $tag
        //         );
        //     }

        //     $this->db->insert_batch('onemap_tags', $onemap_tags);
        // }

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'onemap_id' => $onemap_id,
                    'onemap_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('companies');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_onemap_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            'c.id',
            'c.name',
            'cc.id as category_id',
            'cc.name as category_name',
            'cc.slug as category_slug',
            'cs.id as subcategory_id',
            'cs.name as subcategory_name',
            'cs.slug as subcategory_slug',
            'c.slug',
            'c.img_thumb',
            'c.created',
            'c.modified',
            'c.status'
        );

        $this->db->select($select);
        $this->db->from('companies c');
        $this->db->join('company_categories cc', 'c.category = cc.id', 'left');
        $this->db->join('company_subcategories cs', 'c.subcategory = cs.id', 'left');



        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field == 'search') {
                    $search_select = array('c.name','cc.name','cs.name');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search, 'BOTH');
                    }
                    $this->db->group_end();
                }
                if($field == 'status'){
                    $this->db->group_start();
                    $this->db->where('c.status', $search);
                    $this->db->group_end();

                }
                if($field == 'category'){
                    $this->db->group_start();
                    $this->db->where('c.category', $search);
                    $this->db->group_end();

                }
            }

        }
        $query['totalRecords'] = $this->db->count_all_results();

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();
        $query['meta'] = array(
            'page' => $page,
            'pages' => ceil($query['totalRecords']/$per_page),
            'perpage' => $per_page,
            'total' => $query['totalRecords'],
            'sort' => $order['sort'],
            'field' => $order['field']
        );

	    $this->db->flush_cache();

		return $query;
	}

    public function get_categories()
    {
        $this->db->select('id, name, slug');
        $this->db->order_by('name', 'asc');
        $result = $this->db->get('company_categories');


        return $result->result_array();
    }

    public function get_tags()
    {
        $this->db->select('id, tag_name, slug');
        $result = $this->db->get('tags');

        return $result->result_array();

    }

    /*----------  Get Specific Data  ----------*/

    public function get_onemap_detail($id)
    {
        $select = array(
            'c.*',
            'cc.name as category_name',
            'cs.name as subcategory_name'
        );

        $this->db->select($select);
        $this->db->from('companies c');
        $this->db->join('company_categories cc', 'c.category = cc.id', 'left');
        $this->db->join('company_subcategories cs', 'c.subcategory = cs.id', 'left');

        $this->db->where('c.id', $id);

        $result = $this->db->get()->row_array();

        return $result;
    }

    public function get_onemap_tags($id, $value_only = FALSE){
        $this->db->select('t.id as tag_id, t.tag_name, t.slug as tag_slug');
        $this->db->from('tags t');
        $this->db->join('onemap_tags at', 't.id = at.tag_id', 'left');
        $this->db->join('onemap a', 'at.onemap_id = a.id', 'left');
        $this->db->where('a.id', $id);
        $this->db->where('t.status', 1);

        if($value_only == TRUE){
            $result = $this->db->get()->result_array();

            $tags = array();
            foreach($result as $tag){
                $tags[] = $tag['tag_id'];
            }

            return $tags;
        } else {
            return $this->db->get()->result_array();
        }
    }

    /*----------  Checking  ----------*/

    public function onemap_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('companies') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */