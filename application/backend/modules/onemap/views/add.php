<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url('onemap/add'), array('class'=>'m-form m-form--fit m-form--label-align-right form_create dropzone','id'=>'m-dropzone-one')); ?>
	<input type="hidden" name="slug" id="slug">
    <input type="hidden" name="subcategory" id="subcategory">
	<input type="hidden" name="status" value="">

	<div class="row">
		<div class="col-md-12">
			<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">
				<div class="m-alert__icon">
					<i class="la la-warning"></i>
				</div>
				<div class="m-alert__text">
					Oh snap! Change a few things up and try submitting again.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
			<div class="m-alert m-alert--icon alert alert-success m--hide" role="alert" id="success_msg">
				<div class="m-alert__icon">
					<i class="la la-thumbs-up"></i>
				</div>
				<div class="m-alert__text">
					Success, article has been added to database.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="title">Category</label>
                        <select name="category" id="category" class="form-control">
                            <option value="">Select Category</option>
    						<?php foreach($categories as $category): ?>
                                <option value="<?=$category['id'];?>"><?=$category['name'];?></option>
                            <?php endforeach; ?>
                        </select>
					</div>
                    <div class="form-group m-form__group row" id="company_subcategory">
                        <label for="title">Sub-category</label>
                        <select id="subcategory_opt" class="form-control" disabled>
                            <option value="">Select Sub-category</option>
                        </select>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="control-label">Company/Institution Name</label>
                        <input type="text" name="name" id="title" class="form-control">
                    </div>
					<div class="form-group m-form__group row">
						<label class="control-label">Description</label>
						<textarea name="description" class="form-control" rows="10"></textarea>
					</div>
                    <div class="form-group m-form__group row">
                        <label class="control-label">Website Address</label>
                        <input type="text" name="website" class="form-control">
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="control-label">Company Address</label>
                        <textarea name="address" class="form-control" rows="5"></textarea>
                    </div>
					<div class="form-group m-form__group row">
                        <div class="col-md-6 pl-0">
                            <label for="tag">Latitude</label>
                            <input type="text" name="latitude" class="form-control" placeholder="Use &lsquo;.&rsquo; for decimal">
                        </div>
                        <div class="col-md-6 pr-0">
                            <label for="tag">Longitude</label>
                            <input type="text" name="longitude" class="form-control" placeholder="Use &lsquo;.&rsquo; for decimal">
                        </div>
					</div>
					<div class="form-group m-form__group row">
						<label>Photo Cover</label>
						<div></div>
						<div class="custom-file">
							<input class="custom-file-input" name="inputfile" id="inputfile" type="file">
							<label class="custom-file-label" for="inputfile">
							Choose file
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions">
						<div class="row">
							<div class="col m--align-center m--valign-middle">
								<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
									Publish
								</button>
							</div>
							<!-- <div class="col m--align-right">
								<button type="reset" class="btn btn-brand btn-sm btn-submit" id="btn-preview">
									Preview
								</button>

							</div> -->
						</div>
					</div>
				</div>
				<!-- <div class="m-portlet__body">
					<div class="row">
						<div class="col m--valign-middle">
							<p>
								<label><i class="fa fa-eye"></i> Visibility: <span id="visibility"><b>Public</b></span></label><br>
								<label><i class="la la-calendar"></i> Publish <a href="#." id="publish_time"><b>Immediately</b></a></label>
							</p>
						</div>
					</div>
				</div> -->
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row align-items-center">
							<div class="col m--align-left m--valign-middle">
								<button type="reset" class="btn btn-secondary btn-block btn-sm btn-submit" id="btn-draft">
									Save Draft
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						<i class="la la-times"></i>
					</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group m-form__group row">
					<label>Category Name</label>
					<input type="text" class="form-control m-input" name="title" id="title" placeholder="Article Title">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">
					Save changes
				</button>
			</div>
		</div>
	</div>
</div>

