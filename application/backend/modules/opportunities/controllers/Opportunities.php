<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opportunities extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('opportunities_m');
		$this->template->set_layout('main');
	}

	public function index()
	{

		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/app/js/opportunities/opportunities_index.js'),
				'async' => FALSE,
				'defer' => FALSE
			)
		));

		$this->template->build('opportunities/opportunities/index', $this->data);
	}

	

	public function get_article_list($subcategory_id = NULL)
	{
		$data_list = array();

		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
			$article_data = $this->opportunities_m->get_article_list($subcategory_id);

			if(count($article_data['records']) > 0){
				$offset = (int)$this->input->post('start', TRUE);
				$no = ++$offset;
				foreach($article_data['records'] as $data){
					$is_subcategory_parent = $this->opportunities_m->check_is_parent($data['id']);

					$row = array(
						'no' => $no,
						'id' => $data['id'],
                        'slug' => $data['slug'],
						'title' => $data['title'],
						'category' => $data['category'],
						'subcategory' => $data['subcategory'],
						'is_subcategory' => $is_subcategory_parent,
						'author' => (!is_null($data['author'])) ? $data['author'] : '-',
						'status' => $data['status'],
						'created' => date('Y-m-d', strtotime($data['created'])),
						'published' => date('Y-m-d', strtotime($data['published'])),
						'modified' => (is_null($data['modified'])) ? '-' : date('Y-m-d', strtotime($data['modified'])),
						'featured' => $data['featured']
					);

					$data_list[] = $row;

					$no++;
				}
			}
			$output = array(
				'data' => $data_list,
                'log' => $article_data['log']
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			exit('No direct script access allowed');
		}
	}

	public function get_sub_article_list($category_id)
	{
		$data_list = array();

		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
			$article_data = $this->opportunities_m->get_article_list();

			if(count($article_data['records']) > 0){
				$offset = (int)$this->input->post('start', TRUE);
				$no = ++$offset;
				foreach($article_data['records'] as $data){
					$row = array(
						'no' => $no,
						'id' => $data['id'],
                        'slug' => $data['slug'],
						'title' => $data['title'],
						'category' => $data['category'],
						'subcategory' => $data['subcategory'],
						'author' => (!is_null($data['author'])) ? $data['author'] : '-',
						'status' => $data['status'],
						'created' => date('Y-m-d', strtotime($data['created'])),
						'published' => date('Y-m-d', strtotime($data['published'])),
						'modified' => (is_null($data['modified'])) ? '-' : date('Y-m-d', strtotime($data['modified'])),
						'featured' => $data['featured']
					);

					$data_list[] = $row;

					$no++;
				}
			}
			$output = array(
				'data' => $data_list,
                'log' => $article_data['log']
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			exit('No direct script access allowed');
		}
	}

	public function add()
	{
		if($this->input->is_ajax_request()){
			$upload_success_flag = 0;
			$error_message = '';
			$image_data = NULL;
			$image_thumb_data = NULL;
			$this->load->library('form_validation');

			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[500]|strip_tags|xss_clean');
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('subtitle', 'Subtitle', 'trim|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('subcategory', 'Article Parent', 'trim|numeric|is_exist[articles.id]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('content', 'Article Content', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('tags[]', 'Tags', 'trim|strip_tags|htmlentities|xss_clean');
			$this->form_validation->set_rules('metatitle', 'Meta Title', 'trim|required|min_length[5]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('metadescription', 'Meta Description', 'trim|required|min_length[5]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('category', 'Article Category', 'trim|required|numeric|is_natural_no_zero|is_exist[categories.id]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('featured', 'Featured Article', 'trim|numeric|in_list[NULL,"",1]|is_natural_no_zero|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('inputfile', 'Background File', 'trim|xss_clean');
			$this->form_validation->set_rules('inputfile2', 'Thumbnail File', 'trim|xss_clean');

			$this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

			if ($this->form_validation->run($this) == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$this->load->config('upload_conf');
				$this->load->library('upload');
				
				// if(!isset($_FILES['inputfile']['name'])){
				// 	$result = $this->opportunities_m->add();
				// 	$output = $result;
				// } else {
				// 	$this->load->config('upload_conf');
				// 	$bg_upload_conf = $this->config->item('article');

				// 	$this->load->library('upload', $bg_upload_conf);

				// 	if ( ! $this->upload->do_upload('inputfile')){
				// 		$output = array(
				// 			'status' => 0,
				// 			'message' => array(
				// 				'inputfile' => $this->upload->display_errors()
				// 			)
				// 		);
				// 	}
				// 	else{
				// 		$image_data = $this->upload->data();

				// 		$result = $this->opportunities_m->add($image_data['file_name']);
				// 		$output = $result;
				// 	}
				// }
				if(isset($_FILES['inputfile']['name'])){
					
					$bg_upload_conf = $this->config->item('article');

					$this->upload->initialize($bg_upload_conf);

					if ( ! $this->upload->do_upload('inputfile')){
						$error_message .= "Error when uploading cover image: ".$this->upload->display_errors()."\n";
					}
					else{
						$image_data = $this->upload->data();
					}
				}

				if(isset($_FILES['inputfile2']['name'])){
					
					$bg_upload_conf = $this->config->item('article');

					$this->upload->initialize($bg_upload_conf);

					if ( ! $this->upload->do_upload('inputfile2')){
						$error_message .= "Error when uploading thumbnail image: ".$this->upload->display_errors()."\n";
					}
					else{
						$image_thumb_data = $this->upload->data();
					}
				}

				if($image_data != NULL && $image_thumb_data != NULL){
					$result = $this->opportunities_m->add($image_data['file_name'],$image_thumb_data['file_name']);
					$output = $result;
				} else if($image_data != NULL){
					$result = $this->opportunities_m->add($image_data['file_name'],NULL);
					$output = $result;
				} else if($image_thumb_data != NULL){
					$result = $this->opportunities_m->add(NULL,$image_thumb_data['file_name']);
					$output = $result;
				} else {
					$result = $this->opportunities_m->add();
					$output = $result;
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['categories'] = $this->opportunities_m->get_categories();
			$this->data['subcategories'] = $this->opportunities_m->get_subcategories(3);
			$this->data['tags'] = $this->opportunities_m->get_tags();

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/app/js/opportunities/new_opportunities.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
				),
				// array(
				// 	'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
				// ),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
					'async' => FALSE,
					'defer' => FALSE
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
				),
			));

			$this->template->build('opportunities/opportunities/add_opportunities', $this->data);
		}
	}

	public function edit($id = NULL)
	{
		if(is_null($id) || !is_numeric($id) || !$this->opportunities_m->article_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		if($this->input->is_ajax_request()){
			$article_detail = $this->opportunities_m->get_article_detail($id);
			$upload_success_flag = 0;
			$error_message = '';
			$image_data = NULL;
			$image_thumb_data = NULL;
			$this->load->library('form_validation');

			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[100]|strip_tags|xss_clean');
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('subtitle', 'Subtitle', 'trim|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('subcategory', 'Article Parent', 'trim|numeric|is_exist[articles.id]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('content', 'Article Content', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('tags[]', 'Tags', 'trim|strip_tags|htmlentities|xss_clean');
			$this->form_validation->set_rules('metatitle', 'Meta Title', 'trim|required|min_length[5]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('metadescription', 'Meta Description', 'trim|required|min_length[5]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('category', 'Article Category', 'trim|required|numeric|is_natural_no_zero|is_exist[categories.id]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('featured', 'Featured Article', 'trim|numeric|in_list[NULL,"",1]|is_natural_no_zero|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('inputfile', 'Background File', 'trim|xss_clean');
			$this->form_validation->set_rules('inputfile2', 'Thumbnail File', 'trim|xss_clean');
			$this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

			if ($this->form_validation->run($this) == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$this->load->config('upload_conf');
				$this->load->library('upload');
				
				if(!isset($_FILES['inputfile']['name'])){
					$result = $this->opportunities_m->update($id,$article_detail['img']);
					$output = $result;

				} else {
					
					$bg_upload_conf = $this->config->item('article');

					$this->upload->initialize($bg_upload_conf);

					if ( ! $this->upload->do_upload('inputfile')){
						$error_message .= "Error when uploading cover image: ".$this->upload->display_errors()."\n";
					}
					else{
						$image_data = $this->upload->data();
					}
				}

				if(!isset($_FILES['inputfile2']['name'])){
					$result = $this->opportunities_m->update($id,NULL,$article_detail['img_thumb']);
					$output = $result;
				} else {
					
					$bg_upload_conf = $this->config->item('article');

					$this->upload->initialize($bg_upload_conf);

					if ( ! $this->upload->do_upload('inputfile2')){
						$error_message .= "Error when uploading thumbnail image: ".$this->upload->display_errors()."\n";
					}
					else{
						$image_thumb_data = $this->upload->data();
					}
				}
				if($image_data != NULL && $image_thumb_data != NULL){
					$result = $this->opportunities_m->update($id, $image_data['file_name'],$image_thumb_data['file_name']);
					$output = $result;
				} else if($image_data != NULL){
					$result = $this->opportunities_m->update($id, $image_data['file_name'],NULL);
					$output = $result;
				} else if($image_thumb_data != NULL){
					$result = $this->opportunities_m->update($id, NULL,$image_thumb_data['file_name']);
					$output = $result;
				} else {
					$result = $this->opportunities_m->update($id);
					$output = $result;
				}
			}
			// echo $this->db->last_query();
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['article_detail'] = $this->opportunities_m->get_article_detail($id);
			// echo $this->db->last_query();
			// exit();
			$this->data['article_tags'] = $this->opportunities_m->get_article_tags($id, TRUE);
			
			$this->data['subcategories'] = $this->opportunities_m->get_subcategories(3,$id);
			$this->data['categories'] = $this->opportunities_m->get_categories();
			$this->data['tags'] = $this->opportunities_m->get_tags();

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
					'async' => FALSE,
					'defer' => FALSE
				),
				array(
					'url' => base_url('assets/app/js/opportunities/edit_opportunities.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
				),
				// array(
				// 	'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
				// ),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
				),
			));

			// echo "<pre>";
			// print_r ($this->data['article_metas']);
			// echo "</pre>";
			$this->template->build('opportunities/opportunities/edit_opportunities', $this->data);
		}
	}

	public function get_article_image($id)
	{
		$detail = $this->opportunities_m->get_article_detail($id);

		if(!is_null($detail['img'])){
			$filename = '../assets/dist/images/article/'.$detail['img'];
			$filesize = filesize($filename);

			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				'filename' => $detail['img'],
				'filesize' => $filesize
			)));
		} else {
			$this->output->set_content_type('application/json')->set_output(json_encode(array()));
		}
	}

	public function delete($id){
		if(is_null($id) || !is_numeric($id) || !$this->opportunities_m->article_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		$detail = $this->opportunities_m->get_article_detail($id);

		$this->db->trans_begin();
		if($this->opportunities_m->delete($id)){
			if(!is_null($detail['img'])){
				if(file_exists('../assets/images/articles/'.$detail['img'])){
					if(unlink('../assets/images/articles/'.$detail['img'])){
						$output = array(
							'status' => 1,
							'message' => 'File Deleted'
						);

						$this->db->trans_commit();
					} else {
						$output = array(
							'status' => 0,
							'message' => 'Error when trying to delete file'
						);

						$this->db->trans_rollback();
					}
				} else {
					$output = array(
						'status' => 1,
						'message' => 'File not exist'
					);
					$this->db->trans_commit();
				}
			} else {
				$output = array(
					'status' => 1,
					'message' => 'Article has no image'
				);
				$this->db->trans_commit();
			}
		} else {
			$output = array(
				'status' => 0,
				'message' => $this->db->error()
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */