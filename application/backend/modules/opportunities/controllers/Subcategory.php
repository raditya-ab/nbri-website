<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategory extends Admin_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('category_m');
		$this->template->set_layout('main');
	}

	public function index()
	{
		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/app/js/opportunities/category/index.js'),
				'async' => FALSE,
				'defer' => FALSE
			)
		));

		$this->template->build('article/category/index', $this->data);
	}

	public function view_sub($id)
	{

		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/app/js/opportunities/opportunities_sub_index.js'),
				'attr' => array(
					'subcategory_id' => $id,
					'id' => 'subcategory-'.$id
				),
				'async' => FALSE,
				'defer' => FALSE
			)
		));

		$this->template->build('opportunities/opportunities/index', $this->data);
	}

	public function get_category_list()
	{
		$data_list = array();

		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
			$article_data = $this->category_m->get_category_list();

			if(count($article_data['records']) > 0){
				$offset = (int)$this->input->post('start', TRUE);
				$no = ++$offset;
				foreach($article_data['records'] as $data){
					$row = array(
						'no' => $no,
						'id' => $data['id'],
						// 'category_id' => $data['category_id'],
						'subcategory_name' => $data['subcategory_name'],
						'slug' => $data['slug'],
						'count' => $data['count'],
					);

					$data_list[] = $row;

					$no++;
				}
			}
			$output = array(
				'data' => $data_list,
				'log' => $article_data['log']
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			exit('No direct script access allowed');
		}
	}

	public function add()
	{
		if($this->input->is_ajax_request()){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('category_name', 'Name', 'trim|required|min_length[4]|max_length[50]|strip_tags|xss_clean|is_unique[categories.category_name]');
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[5]|max_length[100]|alpha_dash|strip_tags|xss_clean|is_unique[categories.slug]');

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$result = $this->category_m->add();
				$output = $result;
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/app/js/opportunities/category/new.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				)
			));

			$this->template->build('opportunities/category/add', $this->data);
		}
	}

	public function edit($id)
	{
		if(is_null($id) || !is_numeric($id) || !$this->category_m->category_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		if($this->input->is_ajax_request()){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('category_name', 'Name', 'trim|required|min_length[4]|max_length[50]|strip_tags|xss_clean');
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[5]|max_length[100]|alpha_dash|strip_tags|xss_clean');

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$result = $this->category_m->edit($id);
				$output = $result;
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['detail'] = $this->category_m->get_category_detail($id);
			
			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/app/js/opportunities/category/edit.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				)
			));

			$this->template->build('opportunities/category/edit', $this->data);
		}
	}

	public function delete($id){
		if(is_null($id) || !is_numeric($id) || !$this->category_m->category_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		$detail = $this->category_m->get_category_detail($id);

		if($this->category_m->delete($id)){
			$output = array(
				'status' => 1
			);
		} else {
			$output = array(
				'status' => 0,
				'message' => $this->db->error()
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file Category.php */
/* Location: ./application/backend/modules/article/controllers/Category.php */