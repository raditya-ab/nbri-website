<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $category_data = array(
            'slug' => $input['slug'],
            'category_name' => $input['category_name'],
        );

        $this->db->insert('categories', $category_data);
        $category_id = $this->db->insert_id();

        if($this->db->affected_rows() > 0){
            $output = array(
                'status' => 1,
                'message' => array(
                    'category_id' => $category_id,
                    'category_slug' => $input['slug']
                )
            );
        } else {
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        }

        return $output;
    }

    /*----------  Update Data  ----------*/
    
    public function edit($category_id)
    {
        $input = $this->input->post(NULL, TRUE);
        $category_data = array(
            'slug' => $input['slug'],
            'category_name' => $input['category_name']
        );

        $this->db->where('id', $category_id);
        $this->db->update('categories', $category_data);

        if($this->db->affected_rows() > 0){
            $output = array(
                'status' => 1,
                'message' => array(
                    'category_id' => $category_id,
                    'category_slug' => $input['slug']
                )
            );
        } else {
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/
    
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('categories');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_category_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            's.id',
            's.subcategory_name',
            's.slug',
            'COUNT(a.subcategory) as count'
        );

        $this->db->select($select);
        $this->db->from('subcategories s');
        // $this->db->join('categories c', 's.category_id = c.id','left');
        $this->db->join('articles a', 's.id = a.subcategory', 'left');
        $this->db->where('s.category_id',3);
        // $this->db->where('a.subcategory IS NOT NULL');
        // $this->db->or_where('a.subcategory IS NULL');

        $this->db->group_by('s.id');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $search_select = array('s.subcategory_name');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search);
                    }
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_categories()
    {
        $this->db->select('id, category_name, slug');
        $result = $this->db->get('categories');

        return $result->result_array();
    }

    /*----------  Get Specific Data  ----------*/

    public function get_category_detail($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('categories')->row_array();
    }

    /*----------  Checking  ----------*/

    public function category_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('categories') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */