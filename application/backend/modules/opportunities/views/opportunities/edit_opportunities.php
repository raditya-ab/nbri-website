<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url('opportunities/edit/'.$article_detail['id']), array('class'=>'m-form m-form--fit m-form--label-align-right form_update dropzone','id'=>'m-dropzone-one')); ?>
	<input type="hidden" name="id" id="id" value="<?=$article_detail['id'];?>">
	<input type="hidden" name="slug" id="slug" value="<?=$article_detail['slug'];?>">
	<input type="hidden" name="own_meta" value="0">
	<input type="hidden" name="status" value="<?=$article_detail['status'];?>">
	<input type="hidden" name="published" value="<?=$article_detail['published'];?>">
    <input type="hidden" name="category" value="3">
	<div class="row">
		<div class="col-md-12">
			<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">
				<div class="m-alert__icon">
					<i class="la la-warning"></i>
				</div>
				<div class="m-alert__text">
					Oh snap! Change a few things up and try submitting again.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
			<div class="m-alert m-alert--icon alert alert-success m--hide" role="alert" id="success_msg">
				<div class="m-alert__icon">
					<i class="la la-thumbs-up"></i>
				</div>
				<div class="m-alert__text">
					Success, article has been added to database.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="title">Title</label>
						<?php echo form_input(['class'=>'form-control m-input','id'=>'title','placeholder'=>'Article Title','name'=>'title'], $article_detail['title']); ?>
						<div class="row">
							<div class="col-12">
								<span class="m-form__help">
									<i class="la la-link"></i> <?=FRONT_URL;?>opportunities/<span id="slug_link"><?=$article_detail['slug'];?></span>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label for="subtitle">Subtitle</label>
						<?=form_input('subtitle', $article_detail['subtitle'], ['class'=>'form-control m-input','id'=>'subtitle','placeholder'=>'Article Subtitle']);?>
					</div>
					<div class="form-group m-form__group row">
						<label for="tag">Subcategory</label>
						<select class="form-control m-select2_3" id="subcategory" name="subcategory">
							<option value="" data-slug="" <?=(empty($article_detail['subcategory']) || $article_detail['subcategory'] == NULL) ? 'selected' : '';?>>Select Parent Article</option>
							<?php foreach($subcategories as $subcategory): ?>
								<option value="<?=$subcategory['id'];?>" data-slug="<?=$subcategory['slug'];?>" <?=($subcategory['id'] == $article_detail['subcategory']) ? 'selected' : '';?> ><?=$subcategory['id'].' - '.$subcategory['subcategory_name'];?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group m-form__group row">
						<label class="control-label">Content</label>
						<?=form_textarea('content', html_entity_decode(stripslashes($article_detail['content'])), ['id'=>'content','class'=>'form-control m-input']);?>
					</div>
					<div class="form-group m-form__group row">
						<label for="tag">Tags</label>
						<select class="form-control m-select2" id="m_select2_3" name="tags[]" multiple="multiple">
							<?php foreach($tags as $tag): ?>
								<option value="<?=$tag['id'];?>" data-slug="<?=$tag['slug'];?>" <?=(in_array($tag['id'], $article_tags)) ? 'selected' : '';?> ><?=$tag['tag_name'];?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div id="meta" style="display: none">
						<div class="form-group m-form__group row">
							<label>Meta Title</label>
							<?=form_input('metatitle', $article_detail['metatitle'], ['class'=>'form-control m-input','id'=>'metatitle','placeholder'=>'Meta Title']);?>
						</div>
						<div class="form-group m-form__group row">
							<label>Meta Description</label>
							<?=form_input('metadescription', $article_detail['metadescription'], ['class'=>'form-control m-input','id'=>'metadescription','placeholder'=>'Meta Description']);?>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<p>The meta tags are automatically generated, <a href="javascript:void(0)" id="show_meta"> or you can edit manually.</a></p>
					</div>
					<div class="form-group m-form__group row">
						<label>Photo Cover</label>
						<div></div>
							<?php if(!is_null($article_detail['img'])): ?>
								<div class="image-preview" style="width: 100%; margin: 10px auto">
									<img src="<?=base_url('../assets/images/articles/'.$article_detail['img']);?>" alt="<?=$article_detail['img'];?>" style="max-height: 100px">
								</div>
							<?php endif; ?>
							<div class="custom-file">
								<input class="custom-file-input" name="inputfile" id="inputfile" type="file">
								<label class="custom-file-label" for="inputfile">
								Choose file
								</label>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label>Photo Thumbnail</label>
						<div></div>
							<?php if(!is_null($article_detail['img_thumb'])): ?>
								<div class="image-preview" style="width: 100%; margin: 10px auto">
									<img src="<?=base_url('../assets/images/articles/'.$article_detail['img_thumb']);?>" alt="<?=$article_detail['img_thumb'];?>" style="max-height: 100px">
								</div>
							<?php endif; ?>
							<div class="custom-file">
								<input class="custom-file-input" name="inputfile2" id="inputfile2" type="file">
								<label class="custom-file-label" for="inputfile2">
								Choose file
								</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions">
						<div class="row">
							<div class="col m--align-center m--valign-middle">
								<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
									Publish
								</button>
							</div>
							<!-- <div class="col m--align-right">
								<button type="reset" class="btn btn-brand btn-sm btn-submit" id="btn-preview">
									Preview
								</button>

							</div> -->
						</div>
					</div>
				</div>
				<!-- <div class="m-portlet__body">
					<div class="row">
						<div class="col m--valign-middle">
							<p>
								<label><i class="fa fa-eye"></i> Visibility: <span id="visibility"><b>Public</b></span></label><br>
								<label><i class="la la-calendar"></i> Publish <a href="#." id="publish_time"><b>Immediately</b></a></label>
							</p>
						</div>
					</div>
				</div> -->
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row align-items-center">
							<div class="col m--align-left m--valign-middle">
								<button type="reset" class="btn btn-secondary btn-block btn-sm btn-submit" id="btn-draft">
									Save Draft
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Featured Article
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="row">
						<div class="form-group m-form__group row">
							<div class="m-checkbox-list">
								<label class="m-checkbox m-checkbox--solid m-checkbox--success">
									<input type="checkbox" name="featured" value="1" <?=($article_detail['featured'] == 1) ? 'checked' : '';?> >
									Set as featured
									<span></span>
								</label>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						<i class="la la-times"></i>
					</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group m-form__group row">
					<label>Category Name</label>
					<input type="text" class="form-control m-input" name="title" id="title" placeholder="Article Title">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">
					Save changes
				</button>
			</div>
		</div>
	</div>
</div>

