<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editor extends Admin_Controller
{
    public function index()
    {
        $this->load->view('editor/index');
    }
}