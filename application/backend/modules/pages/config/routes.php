<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once( BASEPATH .'database/DB'. EXT );

$db =& DB();
$db->select('id,page_slug');
$db->from('pages p');
$result = $db->get()->result();

foreach( $result as $row )
{
    $route[ 'pages/'.$row->page_slug ] = 'sections/index/'.$row->id;
    // $route['product/cp;,ategory/'.$row->gender.'/page/(:num)'] = 'product/category/index/'.$row->gender.'/$1';
    // $route[ 'product/category/'.$row->gender.'/(:num)/(:any)' ] = 'product/detail/$1/$2';
}