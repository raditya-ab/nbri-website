<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Admin_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pages_m');
    }

    public function index()
    {
        $this->template->set('page_js',array(
            array(
                'url' => base_url('assets/app/js/pages/pages_index.js'),
                'async' => FALSE,
                'defer' => FALSE
            )
        ));

        $this->template->build('index', $this->data);
    }

    public function get_pages_list()
    {
        $data_list = array();

        if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
            $article_data = $this->pages_m->get_pages_list();

            if(count($article_data['records']) > 0){
                $offset = (int)$this->input->post('start', TRUE);
                $no = ++$offset;
                foreach($article_data['records'] as $data){
                    $row = array(
                        'no' => $no,
                        'id' => $data['id'],
                        'page_name' => ucfirst($data['page_name']),
                        'page_slug' => $data['page_slug'],
                        'section_name' => $data['section_name'],
                        'status' => $data['status'],
                        'created' => date('Y-m-d', strtotime($data['created']))
                    );

                    $data_list[] = $row;

                    $no++;
                }
            }
            $output = array(
                'data' => $data_list
            );
            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {
            exit('No direct script access allowed');
        }
    }


}

/* End of file Pages.php */
/* Location: ./application/backend/modules/pages/controllers/Pages.php */