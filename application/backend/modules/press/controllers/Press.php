<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Press extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('press_m');
		$this->template->set_layout('main');
	}

	public function index()
	{

		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/app/js/press/index_press.js'),
				'async' => FALSE,
				'defer' => FALSE
			)
		));

		$this->template->build('press/press/index', $this->data);
	}

	public function get_article_list($category = 'article')
	{
		$data_list = array();

		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
			$article_data = $this->press_m->get_article_list($category);

			if(count($article_data['records']) > 0){
				$offset = (int)$this->input->post('start', TRUE);
				$no = ++$offset;
				foreach($article_data['records'] as $data){
					$row = array(
						'no' => $no,
						'id' => $data['id'],
						'slug' => $data['slug'],
						'title' => $data['title'],
                        'tags' => $data['tags'],
						'author' => (!is_null($data['author'])) ? $data['author'] : '-',
						'status' => $data['status'],
						'created' => date('Y-m-d', strtotime($data['created'])),
						'published' => date('Y-m-d', strtotime($data['published'])),
						'modified' => (is_null($data['modified'])) ? '-' : date('Y-m-d', strtotime($data['modified'])),
						'featured' => $data['featured']
					);

					$data_list[] = $row;

					$no++;
				}
			}
			$output = array(
				'data' => $data_list,
                'log' => $article_data['log']
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			exit('No direct script access allowed');
		}
	}

	public function add()
	{
		$this->load->library('form_validation');
		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[500]|strip_tags|xss_clean');
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('content', 'Synopsis', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('author', 'Nama Penulis', 'trim|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('publisher', 'Nama Penerbit', 'trim|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('page_number', 'Jumlah Halaman', 'trim|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('dimension', 'Dimensi', 'trim|min_length[3]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('price', 'Harga', 'trim|numeric|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('isbn', 'ISBN', 'trim|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('tags[]', 'Tags', 'trim|strip_tags|htmlentities|xss_clean');
			// $this->form_validation->set_rules('metatitle', 'Meta Title', 'trim|min_length[5]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
			// $this->form_validation->set_rules('metadescription', 'Meta Description', 'trim|min_length[5]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('featured', 'Featured Article', 'trim|numeric|in_list[NULL,"",1]|is_natural_no_zero|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('inputfile', 'Cover File', 'trim|xss_clean');
			$this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

			if ($this->form_validation->run($this) == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				if(!isset($_FILES['inputfile']['name'])){
					$result = $this->press_m->add();
					$output = $result;
				} else {
					$this->load->config('upload_conf');
					$bg_upload_conf = $this->config->item('publication_image');

					$this->load->library('upload', $bg_upload_conf);

					if ( ! $this->upload->do_upload('inputfile')){
						$output = array(
							'status' => 0,
							'message' => array(
								'inputfile' => $this->upload->display_errors()
							)
						);
					}
					else{
						$image_data = $this->upload->data();

						$result = $this->press_m->add($image_data['file_name']);
						$output = $result;
					}
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['tags'] = $this->press_m->get_tags();

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/app/js/press/new_press.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
				),
				// array(
				// 	'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
				// ),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
					'async' => FALSE,
					'defer' => FALSE
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
				),
			));

			$this->template->build('press/press/add_press', $this->data);
		}
	}

	public function edit($id = NULL)
	{
		if(is_null($id) || !is_numeric($id) || !$this->press_m->article_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		if($this->input->is_ajax_request()){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[500]|strip_tags|xss_clean');
			$this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('content', 'Synopsis', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('author', 'Nama Penulis', 'trim|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('publisher', 'Nama Penerbit', 'trim|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('page_number', 'Jumlah Halaman', 'trim|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('dimension', 'Dimensi', 'trim|min_length[3]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('price', 'Harga', 'trim|numeric|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('isbn', 'ISBN', 'trim|min_length[5]|max_length[500]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('tags[]', 'Tags', 'trim|strip_tags|htmlentities|xss_clean');
			// $this->form_validation->set_rules('metatitle', 'Meta Title', 'trim|min_length[5]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
			// $this->form_validation->set_rules('metadescription', 'Meta Description', 'trim|min_length[5]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('featured', 'Featured Article', 'trim|numeric|in_list[NULL,"",1]|is_natural_no_zero|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('inputfile', 'Cover File', 'trim|xss_clean');
			$this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

			if ($this->form_validation->run($this) == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				if(!isset($_FILES['inputfile']['name'])){
					$article_detail = $this->press_m->get_article_detail($id);
					$result = $this->press_m->update($id,$article_detail['img']);
					$output = $result;
				} else {
					$this->load->config('upload_conf');
					$bg_upload_conf = $this->config->item('publication_image');

					$this->load->library('upload', $bg_upload_conf);

					if ( ! $this->upload->do_upload('inputfile')){
						$output = array(
							'status' => 0,
							'message' => array(
								'inputfile' => $this->upload->display_errors()
							)
						);
					}
					else{
						$image_data = $this->upload->data();

						$result = $this->press_m->update($id, $image_data['file_name']);
						$output = $result;
					}
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['article_detail'] = $this->press_m->get_article_detail($id);
			$this->data['article_tags'] = $this->press_m->get_article_tags($id, TRUE);

			$this->data['tags'] = $this->press_m->get_tags();

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
					'async' => FALSE,
					'defer' => FALSE
				),
				array(
					'url' => base_url('assets/app/js/press/edit_press.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
				),
				// array(
				// 	'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
				// ),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
				),
			));

			// echo "<pre>";
			// print_r ($this->data['article_metas']);
			// echo "</pre>";
			$this->template->build('press/press/edit_press', $this->data);
		}
	}

	public function get_article_image($id)
	{
		$detail = $this->press_m->get_article_detail($id);

		if(!is_null($detail['img'])){
			$filename = '../assets/dist/images/publication/'.$detail['img'];
			$filesize = filesize($filename);

			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				'filename' => $detail['img'],
				'filesize' => $filesize
			)));
		} else {
			$this->output->set_content_type('application/json')->set_output(json_encode(array()));
		}
	}

	public function delete($id){
		if(is_null($id) || !is_numeric($id) || !$this->press_m->article_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		$detail = $this->press_m->get_article_detail($id);

		$this->db->trans_begin();
		if($this->press_m->delete($id)){
			if(!is_null($detail['img'])){
				if(file_exists('../assets/images/publication/'.$detail['img'])){
					if(unlink('../assets/images/publication/'.$detail['img'])){
						$output = array(
							'status' => 1,
							'message' => 'File Deleted'
						);

						$this->db->trans_commit();
					} else {
						$output = array(
							'status' => 0,
							'message' => 'Error when trying to delete file'
						);

						$this->db->trans_rollback();
					}
				} else {
					$output = array(
						'status' => 1,
						'message' => 'File not exist'
					);
					$this->db->trans_commit();
				}
			} else {
				$output = array(
					'status' => 1,
					'message' => 'Article has no image'
				);
				$this->db->trans_commit();
			}
		} else {
			$output = array(
				'status' => 0,
				'message' => $this->db->error()
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */