<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends Admin_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('tags_m');
		$this->template->set_layout('main');
	}

	public function index()
	{
		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/app/js/press/tags/index.js'),
				'async' => FALSE,
				'defer' => FALSE
			)
		));

		$this->template->build('press/tags/index', $this->data);
	}

	public function get_tag_list()
	{
		$data_list = array();

		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
			$press_data = $this->tags_m->get_tag_list();

			if(count($press_data['records']) > 0){
				$offset = (int)$this->input->post('start', TRUE);
				$no = ++$offset;
				foreach($press_data['records'] as $data){
					$row = array(
						'no' => $no,
						'id' => $data['id'],
						'tag_name' => $data['tag_name'],
						'slug' => $data['slug'],
						'count' => $data['count'],
					);

					$data_list[] = $row;

					$no++;
				}
			}
			$output = array(
				'data' => $data_list,
				'log' => $press_data['log']
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			exit('No direct script access allowed');
		}
	}

	public function add()
	{
		if($this->input->is_ajax_request()){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('tag_name', 'Name', 'trim|required|min_length[4]|max_length[50]|strip_tags|xss_clean|is_unique[tags.tag_name]');
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[5]|max_length[100]|alpha_dash|strip_tags|xss_clean|is_unique[tags.slug]');

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$result = $this->tags_m->add();
				$output = $result;
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/app/js/press/tags/new.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				)
			));

			$this->template->build('press/tags/add', $this->data);
		}
	}

	public function edit($id)
	{
		if(is_null($id) || !is_numeric($id) || !$this->tags_m->category_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		if($this->input->is_ajax_request()){
			$this->load->library('form_validation');
			$this->form_validation->set_rules('tag_name', 'Name', 'trim|required|min_length[4]|max_length[50]|strip_tags|xss_clean');
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[5]|max_length[100]|alpha_dash|strip_tags|xss_clean');

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$result = $this->tags_m->edit($id);
				$output = $result;
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['detail'] = $this->tags_m->get_tag_detail($id);

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/app/js/press/tags/edit.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				)
			));

			$this->template->build('press/tags/edit', $this->data);
		}
	}

	public function delete($id){
		if(is_null($id) || !is_numeric($id) || !$this->tags_m->category_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		$detail = $this->tags_m->get_tag_detail($id);

		if($this->tags_m->delete($id)){
			$output = array(
				'status' => 1
			);
		} else {
			$output = array(
				'status' => 0,
				'message' => $this->db->error()
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

}

/* End of file Category.php */
/* Location: ./application/backend/modules/press/controllers/Category.php */