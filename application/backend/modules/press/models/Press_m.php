<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Press_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $data = array(
            'title' => $input['title'],
            'slug' => $input['slug'],
            'synopsis' => $input['content'],
            'author' => $input['author'],
            'publisher' => $input['publisher'],
            'page_number' => $input['page_number'],
            'dimension' => $input['dimension'],
            'price' => $input['price'],
            'isbn' => $input['isbn'],
            'metatitle' => $input['metatitle'],
            'metadescription' => $input['metadescription'],
            'featured' => (isset($input['featured'])) ? $input['featured'] : 0,
            'status' => $input['status'],
            'img' => $filename
        );

        $this->db->trans_start();

        $this->db->insert('publication_press', $data);
        $data_id = $this->db->insert_id();

        if(!empty($input['tags']) && !is_null($input['tags'])){
            $article_tags = array();
            foreach($input['tags'] as $tag){
                $article_tags[] = array(
                    'press_id' => $data_id,
                    'tag_id' => $tag
                );
            }

            $this->db->insert_batch('press_tags', $article_tags);
        }

        // $author_data = array(
        //     'article_id' => $article_id,
        //     'author_id' => $this->session->userdata('user_id')
        // );

        // $this->db->insert('author_articles', $author_data);

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'article_id' => $data_id,
                    'article_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function update($id, $filename = NULL)
    {
        $article_id = $id;
        $input = $this->input->post(NULL, TRUE);
        $article_data = array(
            'title' => $input['title'],
            'slug' => $input['slug'],
            'synopsis' => $input['content'],
            'author' => $input['author'],
            'publisher' => $input['publisher'],
            'page_number' => $input['page_number'],
            'dimension' => $input['dimension'],
            'price' => $input['price'],
            'isbn' => $input['isbn'],
            // 'metatitle' => $input['metatitle'],
            // 'metadescription' => $input['metadescription'],
            'featured' => (isset($input['featured'])) ? $input['featured'] : 0,
            'status' => $input['status'],
        );


        if(!is_null($filename) && !empty($filename)) $article_data['img'] = $filename;

        $this->db->trans_start();

        $this->db->where('id', $id);
        $this->db->update('publication_press', $article_data);

        if(!empty($input['tags']) && !is_null($input['tags'])){
            $this->db->where('press_id', $article_id);
            $this->db->delete('press_tags');

            $article_tags = array();
            foreach($input['tags'] as $tag){
                $article_tags[] = array(
                    'press_id' => $article_id,
                    'tag_id' => $tag
                );
            }

            $this->db->insert_batch('press_tags', $article_tags);
        }

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'article_id' => $article_id,
                    'article_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('publication_press');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_article_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            'a.id',
            'a.slug',
            'a.title',
            'a.created',
            'a.published',
            'a.modified',
            'a.author',
            'a.status',
            'a.featured',
            'GROUP_CONCAT(DISTINCT c.tag_name SEPARATOR ", ") as tags',
        );

        $this->db->select($select);
        $this->db->from('publication_press a');
        $this->db->join('press_tags b', 'a.id = b.press_id', 'left');
        $this->db->join('tags c', 'b.tag_id = c.id', 'left');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $search_select = array('a.title','a.author', 'c.tag_name');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search, 'BOTH');
                    }
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->group_by('a.id');

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_categories()
    {
        $this->db->select('id, category_name, slug');
        $result = $this->db->get('categories');

        return $result->result_array();
    }

    public function get_tags()
    {
        $this->db->select('id, tag_name, slug');
        $result = $this->db->get('tags');

        return $result->result_array();

    }

    /*----------  Get Specific Data  ----------*/

    public function get_article_detail($id)
    {
        $select = array(
            'a.id',
            'a.title',
            'a.slug',
            'a.synopsis',
            'a.author',
            'a.publisher',
            'a.page_number',
            'a.dimension',
            'a.img',
            'a.price',
            'a.isbn',
            'a.featured',
            'GROUP_CONCAT(DISTINCT c.tag_name SEPARATOR ", ") as tags',
            'CASE a.status WHEN -1 THEN "Scheduled" WHEN 0 THEN "Draft" ELSE "Published" END as status'
        );

        $this->db->select($select, FALSE);
        $this->db->from('publication_press a');
        // $this->db->join('author_articles aa', 'a.id = aa.article_id', 'left');
        $this->db->join('press_tags b', 'a.id = b.press_id', 'left');
        $this->db->join('tags c', 'b.tag_id = c.id', 'left');

        $this->db->where('a.id', $id);

        $result = $this->db->get()->row_array();

        return $result;
    }

    public function get_article_tags($id, $value_only = FALSE){
        $this->db->select('t.id as tag_id, t.tag_name, t.slug as tag_slug');
        $this->db->from('tags t');
        $this->db->join('press_tags at', 't.id = at.tag_id', 'left');
        $this->db->join('publication_press a', 'at.press_id = a.id', 'left');
        $this->db->where('a.id', $id);
        $this->db->where('t.status', 1);

        if($value_only == TRUE){
            $result = $this->db->get()->result_array();

            $tags = array();
            foreach($result as $tag){
                $tags[] = $tag['tag_id'];
            }

            return $tags;
        } else {
            return $this->db->get()->result_array();
        }
    }

    /*----------  Checking  ----------*/

    public function article_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('publication_press') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */