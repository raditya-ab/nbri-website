<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add()
    {
        $input = $this->input->post(NULL, TRUE);
        $tag_data = array(
            'slug' => $input['slug'],
            'tag_name' => $input['tag_name'],
        );

        $this->db->insert('tags', $tag_data);
        $tag_id = $this->db->insert_id();

        if($this->db->affected_rows() > 0){
            $output = array(
                'status' => 1,
                'message' => array(
                    'tag_id' => $tag_id,
                    'tag_name' => $input['tag_name'],
                    'tag_slug' => $input['slug']
                )
            );
        } else {
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        }

        return $output;
    }

    /*----------  Update Data  ----------*/

    public function edit($tag_id)
    {
        $input = $this->input->post(NULL, TRUE);
        $tag_data = array(
            'slug' => $input['slug'],
            'tag_name' => $input['tag_name']
        );

        $this->db->where('id', $tag_id);
        $this->db->update('tags', $tag_data);

        if($this->db->affected_rows() > 0){
            $output = array(
                'status' => 1,
                'message' => array(
                    'tag_id' => $tag_id,
                    'tag_name' => $input['tag_name'],
                    'tag_slug' => $input['slug']
                )
            );
        } else {
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tags');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_tag_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            't.id',
            't.tag_name',
            't.slug',
            'COUNT(a.id) as count'
        );

        $this->db->select($select);
        $this->db->from('tags t');
        $this->db->join('article_tags at', 't.id = at.tag_id', 'left');
        $this->db->join('articles a', 'at.article_id = a.id', 'left');
        $this->db->group_by('t.id');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $search_select = array('t.tag_name');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search);
                    }
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('t.status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_tags()
    {
        $this->db->select('id, tag_name, slug');
        $result = $this->db->get('tags');

        return $result->result_array();
    }

    /*----------  Get Specific Data  ----------*/

    public function get_tag_detail($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('tags')->row_array();
    }

    /*----------  Checking  ----------*/

    public function tag_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('tags') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */