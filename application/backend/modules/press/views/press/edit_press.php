<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url("press/edit/$article_detail[id]"), array('class' => 'm-form m-form--fit m-form--label-align-right form_update dropzone', 'id' => 'm-dropzone-one')); ?>
<?= form_hidden([
	"slug" => $article_detail["slug"],
	"status" => $article_detail["status"],
]); ?>

<div class="row">
	<div class="col-md-9" id="form_main">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__body">
				<div class="form-group m-form__group row">
					<label for="title">Title</label>
					<input type="text" class="form-control m-input" name="title" id="title" placeholder="Article Title" value="<?= $article_detail['title']; ?>">
					<div class="row">
						<div class="col-12">
							<span class="m-form__help">
								<i class="la la-link"></i> <?= FRONT_URL; ?>press/detail/<?= $article_detail['slug']; ?>
							</span>
						</div>
					</div>
				</div>
				<div class="form-group m-form__group row">
					<label class="control-label">Synopsis</label>
					<?= form_textarea('content', html_entity_decode(stripslashes($article_detail['synopsis'])), ['id' => 'content', 'class' => 'form-control m-input']); ?>
				</div>
				<div class="form-group m-form__group row">
					<label>Nama Penulis</label>
					<input type="text" class="form-control m-input" name="author" id="author" placeholder="Nama Penulis" value="<?= $article_detail['author']; ?>">
				</div>
				<div class="form-group m-form__group row">
					<label>Nama Penerbit</label>
					<input type="text" class="form-control m-input" name="publisher" id="publisher" placeholder="Nama Penerbit" value="<?= $article_detail['publisher']; ?>">
				</div>
				<div class="form-group m-form__group row">
					<label>Jumlah Halaman</label>
					<input type="text" class="form-control m-input" name="page_number" id="page_number" placeholder="Jumlah Halaman, cth: 70 Halaman" value="<?= $article_detail['page_number']; ?>">
				</div>
				<div class="form-group m-form__group row">
					<label>Dimensi Buku</label>
					<input type="text" class="form-control m-input" name="dimension" id="dimension" placeholder="Dimensi Buku, cth: 14 x 35 cm" value="<?= $article_detail['dimension']; ?>">
				</div>
				<div class="form-group m-form__group row">
					<label>Harga Buku</label>
					<input type="number" class="form-control m-input" name="price" id="price" placeholder="Harga Buku tanpa simbol, cth: 50000" value="<?= $article_detail['price']; ?>">
				</div>
				<div class="form-group m-form__group row">
					<label>ISBN</label>
					<input type="text" class="form-control m-input" name="isbn" id="isbn" placeholder="ISBN" value="<?= $article_detail['isbn']; ?>">
				</div>
				<div class="form-group m-form__group row">
					<label for="tag">Tags</label>
					<select class="form-control m-select2" id="m_select2_3" name="tags[]" multiple="multiple">
						<?php foreach ($tags as $tag) : ?>
							<option value="<?= $tag['id']; ?>" data-slug="<?= $tag['slug']; ?>" <?= (in_array($tag['id'], $article_tags)) ? 'selected' : ''; ?>><?= $tag['tag_name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="form-group m-form__group row">
					<label>Photo Cover</label>
					<div></div>
					<?php if (!is_null($article_detail['img'])) : ?>
						<div class="image-preview" style="width: 100%; margin: 10px auto">
							<img src="<?= base_url('../assets/images/publication/' . $article_detail['img']); ?>" alt="<?= $article_detail['img']; ?>" style="max-height: 100px">
						</div>
					<?php endif; ?>
					<div class="custom-file">
						<input class="custom-file-input" name="inputfile" id="inputfile" type="file">
						<label class="custom-file-label" for="inputfile">
							Choose file
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3" id="form_sidebar">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
				<div class="m-form__actions">
					<div class="row">
						<div class="col m--align-center m--valign-middle">
							<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
								Publish
							</button>
						</div>
						<!-- <div class="col m--align-right">
								<button type="reset" class="btn btn-brand btn-sm btn-submit" id="btn-preview">
									Preview
								</button>

							</div> -->
					</div>
				</div>
			</div>
			<!-- <div class="m-portlet__body">
					<div class="row">
						<div class="col m--valign-middle">
							<p>
								<label><i class="fa fa-eye"></i> Visibility: <span id="visibility"><b>Public</b></span></label><br>
								<label><i class="la la-calendar"></i> Publish <a href="#." id="publish_time"><b>Immediately</b></a></label>
							</p>
						</div>
					</div>
				</div> -->
			<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row align-items-center">
						<div class="col m--align-left m--valign-middle">
							<button type="reset" class="btn btn-secondary btn-block btn-sm btn-submit" id="btn-draft">
								Save Draft
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							Featured
						</h3>
					</div>
				</div>
			</div>
			<div class="m-portlet__body">
				<div class="row">
					<div class="form-group m-form__group row">
						<div class="m-checkbox-list">
							<label class="m-checkbox m-checkbox--solid m-checkbox--success">
								<input type="checkbox" name="featured" value="1" <?= ($article_detail['featured'] == 1) ? 'checked' : ''; ?>>
								Set as featured
								<span></span>
							</label>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>