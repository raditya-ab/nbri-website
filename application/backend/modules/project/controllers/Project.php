<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('project_m');
		$this->template->set_layout('main');
	}

	public function index()
	{
		$this->template->set('page_js',array(
			array(
				'url' => base_url('assets/app/js/project/index.js'),
				'async' => FALSE,
				'defer' => FALSE
			)
		));
		$this->data['categories'] = $this->project_m->get_categories();
		$this->template->build('project/project/index', $this->data);
	}

	public function get_project_list()
	{
		$data_list = array();

		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
			$article_data = $this->project_m->get_project_list();

			if(count($article_data['records']) > 0){
				$offset = (int)$this->input->post('start', TRUE);
				$no = ++$offset;
				foreach($article_data['records'] as $data){
					$row = array(
						'no' => $no,
						'id' => $data['id'],
						'name' => $data['name'],
						'location' => (!is_null($data['location'])) ? $data['location'] : '-',
						'type_name' => $data['type_name'],
						'status' => $data['status'],
						'created' => date('Y-m-d', strtotime($data['created'])),
						'type_slug' => $data['type_slug'],
						'project_slug' => $data['project_slug']
					);

					$data_list[] = $row;

					$no++;
				}
			}
			$output = array(
				'data' => $data_list,
				'log' => $article_data['log']
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			exit('No direct script access allowed');
		}
	}

	public function add()
	{
		$this->load->library('form_validation');
		$this->load->helper('uploading');
		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[100]|strip_tags|xss_clean');
			$this->form_validation->set_rules('project_name', 'Project', 'trim|required|min_length[3]|max_length[50]|strip_tags|addslashes|htmlentities|xss_clean');
			// $this->form_validation->set_rules('floor_size', 'Floor Size', 'trim|required|numeric|strip_tags|addslashes|htmlentities|xss_clean');
			// $this->form_validation->set_rules('land_size', 'Land Size', 'trim|required|numeric|strip_tags|addslashes|htmlentities|xss_clean');
			// $this->form_validation->set_rules('location', 'Location', 'trim|required|min_length[3]|max_length[50]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('content', 'Project Content', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('metatitle', 'Meta Title', 'trim|required|min_length[5]|max_length[50]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('metadescription', 'Meta Description', 'trim|required|min_length[5]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('category', 'Project Category', 'trim|required|numeric|is_natural_no_zero|is_exist[project_category.id]|strip_tags|addslashes|htmlentities|xss_clean');

			if(empty($_FILES['inputfile']['name'])){
				$this->form_validation->set_rules('inputfile', 'Project Image', 'trim|xss_clean|required');
			} else {
				$this->form_validation->set_rules('inputfile', 'Project Image', 'trim|xss_clean');
			}

			if ($this->form_validation->run($this) == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$this->load->library('upload');
				$this->load->config('upload_conf');
				$uploaded = 0;

				$filesCount = count($_FILES['inputfile']['name']);

				$upload_conf = $this->config->item('project');

				$uploaded_file = array();
				prep_multiple_upload('inputfile');
				$temp_file = $_FILES['inputfile'];

				for($i = 0; $i < $filesCount; $i++){
					$_FILES['inputfile'] = $temp_file[$i];
					$this->upload->initialize($upload_conf);

					if ( ! $this->upload->do_upload('inputfile')){
						$error = generate_file_upload_error($this->security->xss_clean($_FILES['inputfile']['name']),$this->upload->display_errors());
					}
					else{
						$uploaded_file[] = $this->upload->data('file_name');
						$uploaded++;
					}
				}

				if($uploaded == $filesCount){
					$result = $this->project_m->add($uploaded_file);
					$output = $result;
				} else {
					for($i=0;$i<count($uploaded_file);$i++){
						@unlink($upload_conf['upload_path'].$uploaded_file[$i]);
					}
					
					$output = array(
						'status' => 0,
						'message' => array(
							'error' => $error
						)
					);
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['categories'] = $this->project_m->get_categories();

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
					'async' => FALSE,
					'defer' => FALSE
				),
				array(
					'url' => base_url('assets/app/js/project/new.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
				),
				// array(
				// 	'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
				// ),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
				),
			));

			$this->template->build('project/project/add', $this->data);
		}
	}

	public function edit($id = NULL)
	{
		if(is_null($id) || !is_numeric($id) || !$this->project_m->project_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		if($this->input->is_ajax_request()){
			$this->load->library('form_validation');
			$this->load->helper('uploading');
			
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[100]|strip_tags|xss_clean');
			$this->form_validation->set_rules('project_name', 'Project', 'trim|required|min_length[3]|max_length[50]|strip_tags|addslashes|htmlentities|xss_clean');
			// $this->form_validation->set_rules('floor_size', 'Floor Size', 'trim|required|numeric|strip_tags|addslashes|htmlentities|xss_clean');
			// $this->form_validation->set_rules('land_size', 'Land Size', 'trim|required|numeric|strip_tags|addslashes|htmlentities|xss_clean');
			// $this->form_validation->set_rules('location', 'Location', 'trim|required|min_length[3]|max_length[50]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('content', 'Project Content', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('metatitle', 'Meta Title', 'trim|required|min_length[5]|max_length[50]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('metadescription', 'Meta Description', 'trim|required|min_length[5]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('category', 'Project Category', 'trim|required|numeric|is_natural_no_zero|is_exist[project_category.id]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('status', 'Project Status', 'trim|required|numeric|in_list[0,1]|strip_tags|addslashes|htmlentities|xss_clean');
			$this->form_validation->set_rules('inputfile', 'Project Image', 'trim|xss_clean');

			if ($this->form_validation->run($this) == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				if(empty($_FILES['inputfile']['name'])){
					$result = $this->project_m->edit($id);
					$output = $result;
				} else {
					$this->load->library('upload');
					$this->load->config('upload_conf');
					$uploaded = 0;

					$filesCount = count($_FILES['inputfile']['name']);

					$upload_conf = $this->config->item('project');

					$uploaded_file = array();
					prep_multiple_upload('inputfile');
					$temp_file = $_FILES['inputfile'];

					for($i = 0; $i < $filesCount; $i++){
						$_FILES['inputfile'] = $temp_file[$i];
						$this->upload->initialize($upload_conf);

						if ( ! $this->upload->do_upload('inputfile')){
							$error = generate_file_upload_error($this->security->xss_clean($_FILES['inputfile']['name']),$this->upload->display_errors());
						}
						else{
							$uploaded_file[] = $this->upload->data('file_name');
							$uploaded++;
						}
					}

					if($uploaded == $filesCount){
						$result = $this->project_m->edit($id,$uploaded_file);
						$output = $result;
					} else {
						for($i=0;$i<count($uploaded_file);$i++){
							@unlink($upload_conf['upload_path'].$uploaded_file[$i]);
						}
						
						$output = array(
							'status' => 0,
							'message' => array(
								'error' => $error
							)
						);
					}
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->data['detail'] = $this->project_m->get_project_detail($id);
			$this->data['categories'] = $this->project_m->get_categories();

			$this->template->set('page_js',array(
				array(
					'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
					'async' => FALSE,
					'defer' => FALSE
				),
				array(
					'url' => base_url('assets/app/js/project/edit.js')
				),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
				),
				// array(
				// 	'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
				// ),
				array(
					'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
				),
				array(
					'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
				),
			));

			$this->template->build('project/project/edit', $this->data);
		}
	}

	public function delete_photo($id){
		if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request())
		{
			if($this->project_m->delete_photo($id)){
				$output = array(
					'status' => 1
				);
			} else {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => $this->db->error_messsage()
					)
				);
			}


			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		}
		else {
			show_404();
		}
	}

	public function feature_photo($id){
		
	}

	public function get_article_image($id)
	{
		$detail = $this->project_m->get_article_detail($id);

		if(!is_null($detail['img'])){
			$filename = '../assets/dist/images/article/'.$detail['img'];
			$filesize = filesize($filename);
			
			$this->output->set_content_type('application/json')->set_output(json_encode(array(
				'filename' => $detail['img'],
				'filesize' => $filesize
			)));
		} else {
			$this->output->set_content_type('application/json')->set_output(json_encode(array()));
		}
	}

	public function delete($id){
		if(is_null($id) || !is_numeric($id) || !$this->project_m->project_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		$detail = $this->project_m->get_project_detail($id);

		$this->db->trans_begin();
		if($this->project_m->delete($id)){
			if(!is_null($detail['gallery']) && !empty($detail['gallery'])){
				foreach($detail['gallery'] as $photos) {

				}
				if(file_exists('../assets/upload_dir/images/product/'.$photos['filename'])){
					if(unlink('../assets/upload_dir/images/product/'.$photos['filename'])){
						$output = array(
							'status' => 1,
							'message' => 'File Deleted'
						);

						$this->db->trans_commit();
					} else {
						$output = array(
							'status' => 0,
							'message' => 'Error when trying to delete file'
						);

						$this->db->trans_rollback();
					}
				} else {
					$output = array(
						'status' => 1,
						'message' => 'File not exist'
					);
					$this->db->trans_commit();
				}
			} else {
				$output = array(
					'status' => 1,
					'message' => 'Article has no image'
				);
				$this->db->trans_commit();
			}
		} else {
			$output = array(
				'status' => 0,
				'message' => $this->db->error()
			);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */