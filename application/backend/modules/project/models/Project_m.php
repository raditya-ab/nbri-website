<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $project_data = array(
            'slug' => $input['slug'],
            'name' => $input['project_name'],
            // 'floor_size' => $input['floor_size'],
            // 'land_size' => $input['land_size'],
            // 'location' => $input['location'],
            'description' => $input['content'],
            'metatitle' => $input['metatitle'],
            'metadescription' => $input['metadescription'],
            'type' => $input['category'],
            'status' => $input['status']
        );

        $this->db->trans_start();

        $this->db->insert('project', $project_data);
        $project_id = $this->db->insert_id();

        if(!empty($filename) && !is_null($filename)){
            $this->save_project_gallery($project_id, $filename);
        }

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'project_id' => $project_id,
                    'project_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function save_project_gallery($project_id, $filename){
        $project_file = array();
        foreach($filename as $file){
            $project_file[] = array(
                'project_id' => $project_id,
                'filename' => $file
            );
        }

        $this->db->insert_batch('project_gallery', $project_file);
    }

    /*----------  Update Data  ----------*/
    
    public function edit($project_id,$filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $project_data = array(
            'slug' => $input['slug'],
            'name' => $input['project_name'],
            // 'floor_size' => $input['floor_size'],
            // 'land_size' => $input['land_size'],
            // 'location' => $input['location'],
            'description' => $input['content'],
            'metatitle' => $input['metatitle'],
            'metadescription' => $input['metadescription'],
            'type' => $input['category'],
            'status' => $input['status']
        );

        $this->db->trans_start();

        $this->db->where('id', $project_id);
        $this->db->update('project', $project_data);

        if(!empty($filename) && !is_null($filename)){
            $this->save_project_gallery($project_id, $filename);
        }

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'project_id' => $project_id,
                    'project_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/
    
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('project');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function delete_photo($photo_id){
        $this->db->where('id', $photo_id);
        $this->db->delete('project_gallery');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_project_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();

        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            'p.id',
            'p.name',
            'p.location',
            'p.floor_size',
            'p.land_size',
            'p.slug as project_slug',
            'p.status',
            'p.created',
            'pc.id as type_id',
            'pc.slug as type_slug',
            'pc.category_name as type_name'
        );

        $this->db->select($select);
        $this->db->from('project p');
        $this->db->join('project_category pc', 'p.type = pc.id', 'left');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $search_select = array('p.name','pc.category_name');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search);
                    }
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_categories()
    {
        $this->db->select('id, category_name, slug');
        $result = $this->db->get('project_category');

        return $result->result_array();
    }

    /*----------  Get Specific Data  ----------*/

    public function get_project_detail($id)
    {
        $select = array(
            'p.id',
            'p.slug',
            'p.name',
            'p.floor_size',
            'p.land_size',
            'p.location',
            'p.description',
            'p.metatitle',
            'p.metadescription',
            'p.type',
            'p.status',
            'c.category_name',
            'c.slug as category_slug'
        );

        $this->db->select($select);
        $this->db->from('project p');
        $this->db->join('project_category c', 'p.type = c.id', 'left');
        $this->db->where('p.id', $id);
        $project_detail = $this->db->get()->row_array();

        $this->db->reset_query();

        
        $project_gallery = $this->get_project_images($id);

        return array(
            'project' => $project_detail,
            'gallery' => $project_gallery
        );
    }

    public function get_project_images($id){
        $this->db->select('id,project_id,filename,featured');
        $this->db->order_by('featured', 'desc');
        $this->db->where('project_id', $id);

        return $this->db->get('project_gallery')->result_array();
    }
    /*----------  Checking  ----------*/

    public function project_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('project') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */