<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url('project/category/edit/'.$detail['id']), array('class'=>'m-form m-form--fit m-form--label-align-right form_update dropzone','id'=>'m-dropzone-one')); ?>
	<input type="hidden" id="slug" name="slug" value="<?=$detail['slug'];?>">
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="title">Category Name</label>
						<input type="text" class="form-control m-input" name="category_name" id="category_name" placeholder="Project Name" value="<?=$detail['category_name'];?>">
					</div>
					<div class="form-group m-form__group row">
						<label for="title">Slug</label>
						<span id="slug_link" class="form-control m-input"><?=$detail['slug'];?></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="row align-items-center">
						<div class="col m--align-left m--valign-middle">
							<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
								Save
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>