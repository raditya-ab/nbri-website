<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url('project/add'), array('class'=>'m-form m-form--fit m-form--label-align-right form_create dropzone','id'=>'m-dropzone-one')); ?>
	<input type="hidden" name="slug" id="slug">
	<input type="hidden" name="own_meta" value="0">
	<input type="hidden" name="status" value="">
	<div class="row">
		<div class="col-md-12">
			<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">
				<div class="m-alert__icon">
					<i class="la la-warning"></i>
				</div>
				<div class="m-alert__text">
					Oh snap! Change a few things up and try submitting again.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
			<div class="m-alert m-alert--icon alert alert-success m--hide" role="alert" id="success_msg">
				<div class="m-alert__icon">
					<i class="la la-thumbs-up"></i>
				</div>
				<div class="m-alert__text">
					Success, article has been added to database.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="title">Product Name</label>
						<input type="text" class="form-control m-input" name="project_name" id="project_name" placeholder="Project Name">
						<span class="m-form__help">
							<i class="la la-link"></i> <?=FRONT_URL;?>product/<span id="category_slug">&lt;category&gt;</span>/<span id="slug_link">&lt;slug&gt;</span>
						</span>
					</div>
					<!-- <div class="row">
						<div class="col-md-6">
							<div class="form-group m-form__group row">
								<label>Floor Size</label>
								<input type="number" class="form-control m-input" name="floor_size" id="floor_size" placeholder="in m2" min="0">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group m-form__group row">
								<label>Land Size</label>
								<input type="number" class="form-control m-input" name="land_size" id="land_size" placeholder="in m2" min="0">
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label for="tag">Location</label>
						<input type="text" class="form-control" name="location" id="location">
					</div> -->
					<div class="form-group m-form__group row">
						<label class="control-label">Description</label>
						<textarea name="content" class="form-control" id="content"></textarea>
					</div>

					<div id="meta" style="display: none">
						<div class="form-group m-form__group row">
							<label>Meta Title</label>
							<input type="text" class="form-control m-input" name="metatitle" id="metatitle" placeholder="Meta Title">
						</div>
						<div class="form-group m-form__group row">
							<label>Meta Description</label>
							<input type="text" class="form-control m-input" name="metadescription" id="metadescription" placeholder="Meta Description">
						</div>
					</div>
					<div class="form-group m-form__group row">
						<p>The meta tags are automatically generated, <a href="javascript:void(0)" id="show_meta"> or you can edit manually.</a></p>
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="title">Gallery</label>
						<div class="m-dropzone form-control" id="dropzone-preview">
							<div class="m-dropzone__msg dz-message needsclick">
								<h3 class="m-dropzone__msg-title">
									Drop files here or click to upload.
								</h3>
								<span class="m-dropzone__msg-desc">
									Only <b>JPG</b>, <b>JPEG</b>, <b>PNG</b>, and <b>GIF</b> files are allowed <br>
									Max Resolution is <b>1400x900</b> <br>
									Max Filesize is <b>2MB</b>
								</span>
							</div>
						</div>
						<div class="post-note" style="margin-top: 10px">
							<b>Note: </b>
							<ul class="clearfix">
								<li>Only <b>JPG</b>, <b>JPEG</b>, <b>PNG</b>, and <b>GIF</b> files are allowed</li>
								<li>Max Resolution is <b>1400x900</b></li>
								<li>Max Filesize is <b>2MB</b></li>
								<li>For best experience, photo with landscape orientation is highly recommended</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="row align-items-center">
						<div class="col m--align-left m--valign-middle">
							<button type="button" class="btn btn-success btn-block btn-submit" id="btn-publish">
								Publish
							</button>
							<button type="button" class="btn btn-defaul btn-block btn-submit" id="btn-draft">
								Save as Draft
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Type
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-secondary m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_6">
									<i class="la la-plus"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="row">
						<div class="form-group m-form__group row">
							<div class="m-radio-list">
								<?php foreach($categories as $category): ?>
									<label class="m-radio m-radio--solid m-radio--success">
										<input type="radio" name="category" value="<?=$category['id'];?>" data-slug="<?=$category['slug'];?>">
										<?=$category['category_name'];?>
										<span></span>
									</label>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						<i class="la la-times"></i>
					</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group m-form__group row">
					<label>Category Name</label>
					<input type="text" class="form-control m-input" name="title" id="title" placeholder="Article Title">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">
					Save changes
				</button>
			</div>
		</div>
	</div>
</div>

