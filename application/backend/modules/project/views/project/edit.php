<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>

<?php echo form_open_multipart(site_url('project/edit/'.$detail['project']['id']), array('class'=>'m-form m-form--fit m-form--label-align-right form_update dropzone','id'=>'m-dropzone-one')); ?>
	
	<input type="hidden" name="slug" id="slug" value="<?=$detail['project']['slug'];?>">
	<input type="hidden" name="own_meta" value="0">
	<input type="hidden" name="status" value="<?=$detail['project']['status'];?>">
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label for="title">Product Name</label>
						<input type="text" class="form-control m-input" name="project_name" id="project_name" placeholder="Project Name" value="<?=$detail['project']['name'];?>">
						<span class="m-form__help">
							<i class="la la-link"></i> <?=FRONT_URL;?>product/<span id="category_slug"><?=$detail['project']['category_slug'];?></span>/<span id="slug_link"><?=$detail['project']['slug'];?></span>
						</span>
					</div>
					<!-- <div class="row">
						<div class="col-md-6">
							<div class="form-group m-form__group row">
								<label>Floor Size</label>
								<input type="number" class="form-control m-input" name="floor_size" id="floor_size" placeholder="in m2" min="0" value="<?=$detail['project']['floor_size'];?>">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group m-form__group row">
								<label>Land Size</label>
								<input type="number" class="form-control m-input" name="land_size" id="land_size" placeholder="in m2" min="0" value="<?=$detail['project']['land_size'];?>">
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label for="tag">Location</label>
						<input type="text" class="form-control" name="location" id="location" value="<?=$detail['project']['location'];?>">
					</div> -->
					<div class="form-group m-form__group row">
						<label class="control-label">Description</label>
						<textarea name="content" class="form-control" id="content"><?=$detail['project']['description'];?></textarea>
					</div>

					<div id="meta" style="display: none">
						<div class="form-group m-form__group row">
							<label>Meta Title</label>
							<input type="text" class="form-control m-input" name="metatitle" id="metatitle" placeholder="Meta Title" value="<?=$detail['project']['metatitle'];?>">
						</div>
						<div class="form-group m-form__group row">
							<label>Meta Description</label>
							<input type="text" class="form-control m-input" name="metadescription" id="metadescription" placeholder="Meta Description" value="<?=$detail['project']['metadescription'];?>">
						</div>
					</div>
					<div class="form-group m-form__group row">
						<p>The meta tags are automatically generated, <a href="javascript:void(0)" id="show_meta"> or you can edit manually.</a></p>
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label>Gallery</label>
					</div>
					<div class="form-group m-form__group row">
						<?php foreach($detail['gallery'] as $photo): ?>
							<div class="col-md-3 text-center" id="photo_<?php echo $photo['id']; ?>" style="height: 150px; margin: 5px auto;">
								<div class="img-thumb" style="margin-bottom: 10px; overflow: hidden">
									<img src="<?php echo base_url('../assets/upload_dir/images/product/'.$photo['filename']); ?>" alt="<?php echo $photo['filename']; ?>" style="margin-left: 50%;transform: translateX(-50%); max-height: 100px">
								</div>
								<div class="img-action">
									<button data-id="<?php echo $photo['id']; ?>" data-toggle="m-tooltip" data-container="body" data-placement="bottom" title="Delete" class="btn btn-sm btn-outline-danger m-btn m-btn--custom btn-delete-photo"><i class="fa fa-trash-o"></i></button>
									<?php if($photo['featured'] != 1): ?>
										<button data-id="<?php echo $photo['id']; ?>" data-toggle="m-tooltip" data-container="body" data-placement="bottom" title="Set as Cover" class="btn btn-sm btn-outline-warning m-btn m-btn--custom btn-featured-photo"><i class="fa fa-star-o"></i></button>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
					<div class="form-group m-form__group row">
						<div class="m-dropzone form-control" id="dropzone-preview">
							<div class="m-dropzone__msg dz-message needsclick">
								<h3 class="m-dropzone__msg-title">
									Drop files here or click to upload.
								</h3>
								<span class="m-dropzone__msg-desc">
									Only <b>JPG</b>, <b>JPEG</b>, <b>PNG</b>, and <b>GIF</b> files are allowed <br>
									Max Resolution is <b>1920x1080</b> <br>
									Min Resolution is <b>800x450</b> <br>
									Max Filesize is <b>2MB</b>
								</span>
							</div>
						</div>
						<div class="post-note" style="margin-top: 10px">
							<b>Note: </b>
							<ul class="clearfix">
								<li>Only <b>JPG</b>, <b>JPEG</b>, <b>PNG</b>, and <b>GIF</b> files are allowed</li>
								<li>Max Resolution is <b>1920x1080</b></li>
								<li>Max Resolution is <b>800x450</b></li>
								<li>Max Filesize is <b>2MB</b></li>
								<li>For best experience, photo with landscape orientation is highly recommended</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="row align-items-center">
						<div class="col m--align-left m--valign-middle">
							<button type="button" class="btn btn-success btn-block btn-submit" id="btn-publish">
								Publish
							</button>
							<button type="button" class="btn btn-defaul btn-block btn-submit" id="btn-draft">
								Save as Draft
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Type
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-secondary m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_6">
									<i class="la la-plus"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="row">
						<div class="form-group m-form__group row">
							<div class="m-radio-list">
								<?php foreach($categories as $category): ?>
									<label class="m-radio m-radio--solid m-radio--success">
										<input type="radio" name="category" value="<?=$category['id'];?>" data-slug="<?=$category['slug'];?>" <?=($category['id'] == $detail['project']['type'])?'checked':''; ?> >
										<?=$category['category_name'];?>
										<span></span>
									</label>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

<div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						<i class="la la-times"></i>
					</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group m-form__group row">
					<label>Category Name</label>
					<input type="text" class="form-control m-input" name="title" id="title" placeholder="Article Title">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">
					Save changes
				</button>
			</div>
		</div>
	</div>
</div>

