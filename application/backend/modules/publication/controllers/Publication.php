<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publication extends Admin_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('publication_m');
    }

    public function index()
    {
        $this->template->set('page_js',array(
            array(
                'url' => base_url('assets/app/js/publication/index_publication.js')
            )
        ));

        $this->template->build('index', $this->data);
    }

    public function get_publication_list()
    {
        $data_list = array();

        if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
            $publication_data = $this->publication_m->get_publication_list();

            if(count($publication_data['records']) > 0){
                $offset = (int)$this->input->post('start', TRUE);
                $no = ++$offset;
                foreach($publication_data['records'] as $data){
                    $row = array(
                        'no' => $no,
                        'id' => $data['id'],
                        'name' => $data['name'],
                        'slug' => $data['slug'],
                        'img_thumb' => $data['img_thumb'],
                        'filename' => $data['filename'],
                        'format' => $data['format'],
                        'created' => date('Y-m-d', strtotime($data['created'])),
                        'modified' => (is_null($data['modified'])) ? '-' : date('Y-m-d', strtotime($data['modified'])),
                        'status' => $data['status']
                    );

                    $data_list[] = $row;

                    $no++;
                }
            }
            $output = array(
                'data' => $data_list
            );
            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {
            exit('No direct script access allowed');
        }
    }

    public function add()
    {
        $document_upload_flag = FALSE;

        $this->load->library('form_validation');
        if($this->input->is_ajax_request()){
            $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[100]|strip_tags|xss_clean');
            $this->form_validation->set_rules('title', 'Publication Name', 'trim|required|min_length[5]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('content', 'Description', 'trim|required|min_length[5]|addslashes');
            $this->form_validation->set_rules('inputfile', 'Background File', 'trim|xss_clean');
            if(empty($_FILES['inputfile_file']['name'])){
                $this->form_validation->set_rules('inputfile_file','Publication Document', 'required');
            }
            $this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

            if ($this->form_validation->run($this) == FALSE) {
                $output = array(
                    'status' => 0,
                    'message' => array(
                        'error' => validation_errors()
                    )
                );
            } else {
                $this->load->config('upload_conf');
                $file_upload_conf = $this->config->item('publication_file');

                $this->load->library('upload', $file_upload_conf);

                
                if ( ! $this->upload->do_upload('inputfile_file')){
                    $output = array(
                        'status' => 0,
                        'message' => array(
                            'error' => 'FIle Error: '.$this->upload->display_errors()
                        )
                    );
                }
                else{
                    $document_filename = $this->upload->data()['file_name'];
                    $document_upload_flag = TRUE;
                    if($document_upload_flag){
                        if(!isset($_FILES['inputfile']['name'])){

                            $result = $this->publication_m->add(NULL,$document_filename);
                            $output = $result;
                        } else {
                            // $this->load->config('upload_conf');
                            $bg_upload_conf = $this->config->item('publication_image');

                            // $this->load->library('upload', $bg_upload_conf);
                            $this->upload->initialize($bg_upload_conf);

                            if ( ! $this->upload->do_upload('inputfile')){
                                $output = array(
                                    'status' => 0,
                                    'message' => array(
                                        'error' => 'Image Error: '.$this->upload->display_errors()
                                    )
                                );
                            }
                            else{
                                $image_data = $this->upload->data();

                                $result = $this->publication_m->add($image_data['file_name'], $document_filename);
                                $output = $result;
                            }
                        }
                    } else {
                        $output = array(
                            'status' => 0,
                            'message' => array(
                                'error' => 'Document not uploaded, data no saved. Cause: '.$this->upload->display_errors()
                            )
                        );
                    }
                }
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {

            $this->template->set('page_js',array(
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
                    'async' => FALSE,
                    'defer' => FALSE
                ),
                array(
                    'url' => base_url('assets/app/js/publication/new_publication.js')
                ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
                ),
                // array(
                //  'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
                // ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
                ),
            ));

            $this->template->build('add_publication', $this->data);
        }
    }

    public function edit($id)
    {
        $publication_detail = $this->publication_m->get_publication_detail($id);
        $document = $publication_detail['filename'];
        $image = $publication_detail['img_thumb'];
        $file_upload_error_message = '';

        if(is_null($id) || !is_numeric($id) || !$this->publication_m->publication_exist($id)) {
            if($this->input->is_ajax_request()){
                $this->output->set_content_type('application/json')->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Doesn\'t exist'
                ]));
            } else {
                show_404();
            }
        }

        if($this->input->is_ajax_request()){
            $this->load->library('form_validation');

            $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[100]|strip_tags|xss_clean');
            $this->form_validation->set_rules('title', 'Publication Name', 'trim|required|min_length[5]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('content', 'Description', 'trim|required|min_length[5]|addslashes');
            $this->form_validation->set_rules('inputfile', 'Background File', 'trim|xss_clean');
            $this->form_validation->set_rules('inputfile_file', 'Publication Document', 'trim|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

            if ($this->form_validation->run($this) == FALSE) {
                $output = array(
                    'status' => 0,
                    'message' => array(
                        'error' => validation_errors()
                    )
                );
            } else {
                if(isset($_FILES['inputfile_file']['name'])){
                    $this->load->config('upload_conf');
                    $file_upload_conf = $this->config->item('publication_file');

                    $this->load->library('upload', $file_upload_conf);

                    if ( ! $this->upload->do_upload('inputfile_file')){
                        $file_upload_error_message .= 'Upload PDF Error: '.$this->upload->display_errors()."<br>";
                    } else {
                        $document_filename = $this->upload->data()['file_name'];
                        $document = $document_filename;
                    }
                }

                if(isset($_FILES['inputfile']['name'])){
                    $this->load->config('upload_conf');
                    $file_upload_conf = $this->config->item('publication_image');

                    $this->load->library('upload', $file_upload_conf);

                    if ( ! $this->upload->do_upload('inputfile')){
                        $file_upload_error_message .= 'Upload IMG Error: '.$this->upload->display_errors()."<br>";
                    } else {
                        $image_filename = $this->upload->data()['file_name'];
                        $image = $image_filename;
                    }
                }

                $result = $this->publication_m->edit($id,$image,$document);
                $output = $result;
                $output['message']['upload_info'] = $file_upload_error_message;
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {
            $this->data['publication_detail'] = $publication_detail;

            $this->template->set('page_js',array(
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
                    'async' => FALSE,
                    'defer' => FALSE
                ),
                array(
                    'url' => base_url('assets/app/js/publication/edit_publication.js')
                ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
                ),
                // array(
                //  'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
                // ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
                ),
            ));

            // echo "<pre>";
            // print_r ($this->data['publication_metas']);
            // echo "</pre>";
            $this->template->build('edit_publication', $this->data);
        }
    }
    public function get_research_image($id)
    {
        $detail = $this->publication_m->get_research_detail($id);

        if(!is_null($detail['thumb_img'])){
            $filename = '../assets/images/research/'.$detail['thumb_img'];
            $filesize = filesize($filename);

            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'filename' => $detail['img'],
                'filesize' => $filesize
            )));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array()));
        }
    }

    public function delete($id){
        if(is_null($id) || !is_numeric($id) || !$this->publication_m->publication_exist($id)) {
            if($this->input->is_ajax_request()){
                $this->output->set_content_type('application/json')->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Doesn\'t exist'
                ]));
            } else {
                show_404();
            }
        }

        $detail = $this->publication_m->get_publication_detail($id);

        $this->db->trans_begin();
        if($this->publication_m->delete($id)){
            if(!is_null($detail['img_thumb'])){
                @unlink('../assets/images/publication/'.$detail['img_thumb']);
            }
            if(!is_null($detail['filename'])){
                @unlink('../assets/images/publication/'.$detail['filename']);
            }

            $output = array(
                'status' => 1,
                'message' => "Success delete publication"
            );

            $this->db->trans_commit();
        } else {
            $output = array(
                'status' => 0,
                'message' => $this->db->error()
            );

            $this->db->trans_rollback();
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

}

/* End of file Publication.php */
/* Location: ./application/backend/modules/publication/controllers/Publication.php */