<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publication_m extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_publication_list()
    {
        $input    = $this->input->post();
        $query    = array();
        $page     = (int)$this->input->post('pagination',TRUE)['page'];
        $per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
        $offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $this->db->from('publication');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $search_select = array('name', 'format');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search);
                    }
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
        $query['filterRecords'] = $this->db->count_all_results();

        $this->db->flush_cache();

        return $query;
    }

    public function add($image = NULL, $document = NULL)
    {
        $input = $this->input->post();
        $publication_data = array(
            'name' => $input['title'],
            'slug' => $input['slug'],
            'img_thumb' => $image,
            'filename' => $document,
            'format' => 'pdf',
            'description' => $input['content'],
            'status' => $input['status']
        );

        $this->db->trans_start();

        $this->db->insert('publication', $publication_data);
        $publication_id = $this->db->insert_id();

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'publication_id' => $publication_id,
                    'publication_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function edit($id, $image, $document)
    {
        $input = $this->input->post();
        $publication_data = array(
            'name' => $input['title'],
            'slug' => $input['slug'],
            'img_thumb' => $image,
            'filename' => $document,
            'description' => $input['content'],
            'status' => $input['status']
        );

        $this->db->trans_start();

        $this->db->where('id', $id);
        $this->db->update('publication', $publication_data);

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'publication_id' => $id,
                    'publication_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('publication');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    public function get_publication_detail($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('publication')->row_array();
    }

    public function publication_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('publication') > 0) ? TRUE : FALSE;
    }

}

/* End of file Publication_m.php */
/* Location: ./application/backend/modules/publication/models/Publication_m.php */