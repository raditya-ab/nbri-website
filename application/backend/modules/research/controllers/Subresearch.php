<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subresearch extends Admin_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('subresearch_m');
    }

    public function index($category_id = NULL)
    {
        if($category_id == NULL){
            $this->template->set('page_js',array(
                array(
                    'url' => base_url('assets/app/js/research/subresearch/index_subresearch.js')
                )
            ));

            $this->template->build('research/subresearch/index_all', $this->data);
        } else {
            $this->load->model('research_m');
            $data['research_detail'] = $this->research_m->get_research_detail($category_id);
            $this->template->set('page_js',array(
                array(
                    'url' => base_url('assets/app/js/research/subresearch/index_subresearch.js'),
                    'attr' => array(
                        'id' => $category_id
                    )
                )
            ));
            $this->template->title($data['research_detail']['category_name']);
            $this->template->build('research/subresearch/index', $this->data);
        }
    }

    public function get_subresearch_list($category_id = NULL)
    {
        $data_list = array();

        if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
            $research_data = $this->subresearch_m->get_subresearch_list($category_id);

            if(count($research_data['records']) > 0){
                $offset = (int)$this->input->post('start', TRUE);
                $no = ++$offset;
                foreach($research_data['records'] as $data){
                    $row = array(
                        'no' => $no,
                        'id' => $data['id'],
                        'research_id' => $data['research_id'],
                        'category_name' => $data['category_name'],
                        'category_slug' => $data['category_slug'],
                        'research_name' => stripslashes($data['research_name']),
                        'slug' => $data['slug'],
                        'img_thumb' => $data['img_thumb'],
                        'created' => date('Y-m-d', strtotime($data['created'])),
                        'modified' => (is_null($data['modified'])) ? '-' : date('Y-m-d', strtotime($data['modified'])),
                        'status' => $data['status']
                    );

                    $data_list[] = $row;

                    $no++;
                }
            }
            $output = array(
                'data' => $data_list
            );
            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {
            exit('No direct script access allowed');
        }
    }

    public function add($category_id = NULL)
    {
        $this->load->model('research_m');
        $this->data['category_list'] = $this->research_m->get_research_category();

        if($category_id != NULL && is_numeric($category_id)){
            $this->data['category_detail'] = $this->research_m->get_research_detail($category_id);
        } else {
            $this->data['category_detail'] = array(
                'id' => NULL
            );
        }

        $this->load->library('form_validation');
        if($this->input->is_ajax_request()){
            $this->form_validation->set_rules('category_id', 'Research Field', 'trim|required|numeric|callback_research_exist|strip_tags|xss_clean');
            $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[100]|strip_tags|xss_clean');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('content', 'Article Content', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('inputfile', 'Background File', 'trim|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

            if ($this->form_validation->run($this) == FALSE) {
                $output = array(
                    'status' => 0,
                    'message' => array(
                        'error' => validation_errors()
                    )
                );
            } else {
                if(!isset($_FILES['inputfile']['name'])){
                    $result = $this->subresearch_m->add();
                    $output = $result;
                } else {
                    $this->load->config('upload_conf');
                    $bg_upload_conf = $this->config->item('subresearch');

                    $this->load->library('upload', $bg_upload_conf);

                    if ( ! $this->upload->do_upload('inputfile')){
                        $output = array(
                            'status' => 0,
                            'message' => array(
                                'error' => $this->upload->display_errors()
                            )
                        );
                    }
                    else{
                        $image_data = $this->upload->data();

                        $result = $this->subresearch_m->add($image_data['file_name']);
                        $output = $result;
                    }
                }
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {

            $this->template->set('page_js',array(
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
                    'async' => FALSE,
                    'defer' => FALSE
                ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
                ),
                // array(
                //  'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
                // ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/app/js/research/subresearch/new_subresearch.js')
                ),
            ));

            $this->template->build('research/subresearch/add_subresearch', $this->data);
        }
    }

    public function edit($id = NULL)
    {
        $this->load->model('research_m');

        if(is_null($id) || !is_numeric($id) || !$this->subresearch_m->subresearch_exist($id)) {
            if($this->input->is_ajax_request()){
                $this->output->set_content_type('application/json')->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Doesn\'t exist'
                ]));
            } else {
                show_404();
            }
        }

        if($this->input->is_ajax_request()){
            $this->load->library('form_validation');

            $this->form_validation->set_rules('category_id', 'Research Field', 'trim|required|numeric|callback_research_exist|strip_tags|xss_clean');
            $this->form_validation->set_rules('slug', 'Slug', 'trim|required|min_length[1]|max_length[100]|strip_tags|xss_clean');
            $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('content', 'Article Content', 'trim|required|min_length[5]|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('inputfile', 'Background File', 'trim|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

            if ($this->form_validation->run($this) == FALSE) {
                $output = array(
                    'status' => 0,
                    'message' => array(
                        'error' => validation_errors()
                    )
                );
            } else {
                if(!isset($_FILES['inputfile']['name'])){
                    $subresearch_detail = $this->subresearch_m->get_subresearch_detail($id);
                    $result = $this->subresearch_m->update($id,$subresearch_detail['img_thumb']);
                    $output = $result;
                } else {
                    $this->load->config('upload_conf');
                    $bg_upload_conf = $this->config->item('subresearch');

                    $this->load->library('upload', $bg_upload_conf);

                    if ( ! $this->upload->do_upload('inputfile')){
                        $output = array(
                            'status' => 0,
                            'message' => array(
                                'inputfile' => $this->upload->display_errors()
                            )
                        );
                    }
                    else{
                        $image_data = $this->upload->data();

                        $result = $this->subresearch_m->update($id, $image_data['file_name']);
                        $output = $result;
                    }
                }
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {
            $this->data['subresearch_detail'] = $this->subresearch_m->get_subresearch_detail($id);

            $this->template->set('page_js',array(
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
                    'async' => FALSE,
                    'defer' => FALSE
                ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
                ),
                // array(
                //  'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
                // ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/app/js/research/subresearch/edit_subresearch.js')
                ),
            ));

            // echo "<pre>";
            // print_r ($this->data['research_metas']);
            // echo "</pre>";
            $this->template->build('research/subresearch/edit_subresearch', $this->data);
        }
    }
    public function get_subresearch_image($id)
    {
        $detail = $this->subresearch_m->get_subresearch_detail($id);

        if(!is_null($detail['img_thumb'])){
            $filename = '../assets/images/subresearch/'.$detail['img_thumb'];
            $filesize = filesize($filename);

            $this->output->set_content_type('application/json')->set_output(json_encode(array(
                'filename' => $detail['img_thumb'],
                'filesize' => $filesize
            )));
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode(array()));
        }
    }

    public function delete($id){
        if(is_null($id) || !is_numeric($id) || !$this->subresearch_m->subresearch_exist($id)) {
            if($this->input->is_ajax_request()){
                $this->output->set_content_type('application/json')->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Doesn\'t exist'
                ]));
            } else {
                show_404();
            }
        }

        $detail = $this->subresearch_m->get_subresearch_detail($id);

        $this->db->trans_begin();
        if($this->subresearch_m->delete($id)){
            if(!is_null($detail['img_thumb'])){
                if(file_exists('../assets/images/subresearch/'.$detail['img_thumb'])){
                    if(unlink('../assets/images/subresearch/'.$detail['img_thumb'])){
                        $output = array(
                            'status' => 1,
                            'message' => 'File Deleted'
                        );

                        $this->db->trans_commit();
                    } else {
                        $output = array(
                            'status' => 0,
                            'message' => 'Error when trying to delete file'
                        );

                        $this->db->trans_rollback();
                    }
                } else {
                    $output = array(
                        'status' => 1,
                        'message' => 'File not exist'
                    );
                    $this->db->trans_commit();
                }
            } else {
                $output = array(
                    'status' => 1,
                    'message' => 'Article has no image'
                );
                $this->db->trans_commit();
            }
        } else {
            $output = array(
                'status' => 0,
                'message' => $this->db->last_query()
            );
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

    // function callback for form validation
    public function research_exist($id)
    {
        $this->load->model('research_m');
        if (!$this->research_m->research_exist($id))
        {
            $this->form_validation->set_message('username_check', 'The {field} field can not be the word "test"');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
}

/* End of file Research.php */
/* Location: ./application/backend/modules/research/controllers/Research.php */