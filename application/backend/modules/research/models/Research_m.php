<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Research_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $research_data = array(
            'category_name' => $input['title'],
            'slug' => $input['slug'],
            'content' => $input['content'],
            'thumb_img' => $filename,
            'status' => $input['status'],
        );

        $this->db->trans_start();

        $this->db->insert('research_category', $research_data);
        $research_id = $this->db->insert_id();

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'research_id' => $research_id,
                    'research_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function update($id, $filename = NULL)
    {
        $research_id = $id;
        $input = $this->input->post(NULL, TRUE);
        $research_data = array(
            'category_name' => $input['title'],
            'slug' => $input['slug'],
            'content' => $input['content'],
            'status' => $input['status'],
            'thumb_img' => $filename
        );


        if(!is_null($filename) && !empty($filename)) $research_data['thumb_img'] = $filename;

        $this->db->trans_start();

        $this->db->where('id', $id);
        $this->db->update('research_category', $research_data);

        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'research_id' => $research_id,
                    'research_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('research_category');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_research_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $this->db->from('research_category a');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $search_select = array('a.category_name');
                    $this->db->group_start();
                    foreach($search_select as $s)
                    {
                        $this->db->or_like('LOWER('.$s.')',$search);
                    }
                    $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_research_category()
    {
        $this->db->select('id, category_name, slug');
        $result = $this->db->get('research_category');

        return $result->result_array();
    }

    public function get_tags()
    {
        $this->db->select('id, tag_name, slug');
        $result = $this->db->get('tags');

        return $result->result_array();

    }

    /*----------  Get Specific Data  ----------*/

    public function get_research_detail($id)
    {
        $this->db->where('id', $id);

        $result = $this->db->get('research_category')->row_array();

        return $result;
    }

    /*----------  Checking  ----------*/

    public function research_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('research_category') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */