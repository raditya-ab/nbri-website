<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
    .select2-container.select2-container-disabled .select2-choice {
        background-color: #ddd;
        border-color: #a8a8a8;
    }
</style>

<?php echo form_open_multipart(site_url('research/add/'.$category_detail['id']), array('class'=>'m-form m-form--fit m-form--label-align-right form_create dropzone','id'=>'m-dropzone-one')); ?>

    <?php if($category_detail['id'] != NULL): ?>
        <input type="hidden" name="category_id" value="<?=$category_detail['id'];?>">
    <?php endif; ?>

	<input type="hidden" name="slug" id="slug">
	<input type="hidden" name="status" value="">
	<input type="hidden" name="published" value="<?=date('Y-m-d H:i:s');?>">
	<div class="row">
		<div class="col-md-12">
			<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">
				<div class="m-alert__icon">
					<i class="la la-warning"></i>
				</div>
				<div class="m-alert__text">
					Oh snap! Change a few things up and try submitting again.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
			<div class="m-alert m-alert--icon alert alert-success m--hide" role="alert" id="success_msg">
				<div class="m-alert__icon">
					<i class="la la-thumbs-up"></i>
				</div>
				<div class="m-alert__text">
					Success, article has been added to database.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <label for="title">Research Topic</label>
                        <?php if($category_detail['id'] == NULL): ?>
                            <select name="category_id" class="form-control m-input select2" id="category_id">
                                <?php foreach($category_list as $category): ?>
                                    <option value="<?=$category['id'];?>"><?=$category['category_name'];?></option>
                                <?php endforeach; ?>
                            </select>
                        <?php else: ?>
                            <input type="text" class="form-control m-input" value="<?=@$category_detail['category_name']?>" readonly style="background-color: #DDD">
                        <?php endif; ?>
                    </div>
					<div class="form-group m-form__group row">
						<label for="title">Sub-Research Title</label>
						<input type="text" class="form-control m-input" name="title" id="title" placeholder="Research Title">
						<div class="row">
							<div class="col-12">
								<span class="m-form__help">
									<i class="la la-link"></i> <?=FRONT_URL;?>research/<span id="slug_link">&lt;slug&gt;</span>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="control-label">Content</label>
						<textarea name="content" class="form-control" id="content"></textarea>
					</div>

					<div class="form-group m-form__group row">
						<label>Photo Cover</label>
						<div></div>
						<div class="custom-file">
							<input class="custom-file-input" name="inputfile" id="inputfile" type="file">
							<label class="custom-file-label" for="inputfile">
							Choose file
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions">
						<div class="row">
							<div class="col m--align-center m--valign-middle">
								<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
									Publish
								</button>
							</div>
							<!-- <div class="col m--align-right">
								<button type="reset" class="btn btn-brand btn-sm btn-submit" id="btn-preview">
									Preview
								</button>

							</div> -->
						</div>
					</div>
				</div>
				<!-- <div class="m-portlet__body">
					<div class="row">
						<div class="col m--valign-middle">
							<p>
								<label><i class="fa fa-eye"></i> Visibility: <span id="visibility"><b>Public</b></span></label><br>
								<label><i class="la la-calendar"></i> Publish <a href="#." id="publish_time"><b>Immediately</b></a></label>
							</p>
						</div>
					</div>
				</div> -->
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row align-items-center">
							<div class="col m--align-left m--valign-middle">
								<button type="reset" class="btn btn-secondary btn-block btn-sm btn-submit" id="btn-draft">
									Save Draft
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Category
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--hover-secondary m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="modal" data-target="#m_modal_6">
									<i class="la la-plus"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="row">
						<div class="form-group m-form__group row">
							<div class="m-radio-list">
								<?php foreach($categories as $category): ?>
									<label class="m-radio m-radio--solid m-radio--success">
										<input type="radio" name="category" value="<?=$category['id'];?>" data-slug="<?=$category['slug'];?>">
										<?=$category['category_name'];?>
										<span></span>
									</label>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Featured Article
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="row">
						<div class="form-group m-form__group row">
							<div class="m-checkbox-list">
								<label class="m-checkbox m-checkbox--solid m-checkbox--success">
									<input type="checkbox" name="featured" value="1">
									Set as featured
									<span></span>
								</label>
							</div>
						</div>
					</div>

				</div>
			</div> -->
		</div>
	</div>
<?php echo form_close(); ?>

<!-- <div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						<i class="la la-times"></i>
					</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group m-form__group row">
					<label>Category Name</label>
					<input type="text" class="form-control m-input" name="title" id="title" placeholder="Article Title">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">
					Save changes
				</button>
			</div>
		</div>
	</div>
</div> -->

