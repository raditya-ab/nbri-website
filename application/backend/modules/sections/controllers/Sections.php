<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sections extends Admin_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('section_m');
    }

    public function index($page_id, $page_slug)
    {
        $this->load->model('pages/pages_m', 'pages_m');

        if(is_null($page_id) || !is_numeric($page_id) || !$this->pages_m->pages_exist($page_id)) {
            if($this->input->is_ajax_request()){
                $this->output->set_content_type('application/json')->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Doesn\'t exist'
                ]));
            } else {
                show_404();
            }
        }

        $this->template->set('page_js',array(
            array(
                'url' => base_url('assets/app/js/section/index_section.js'),
                'async' => FALSE,
                'defer' => FALSE,
                'attr' => array(
                    'page_slug' => $page_slug,
                    'page_id' => $page_id
                )
            )
        ));

        $this->template->build('index', $this->data);
    }

    public function get_section_list($page_id,$page_slug)
    {
        $this->load->model('pages/pages_m','pages_m');
        $data_list = array();
        // $post_data = $this->input->post();
        // $param = array(
        //     'page_id' => $this->input->post('page_id'),
        //     'page_slug' => $this->input->post('page_slug')
        // );
        // echo "<pre>";
        // print_r($param);
        // echo "</pre>";
        // exit();
        if(!is_null($this->input->server('HTTP_REFERER')) && $this->input->is_ajax_request()){
            $section_data = $this->section_m->get_section_list($page_id);
            $page_data = $this->pages_m->get_pages_detail($page_id);

            if(count($section_data['records']) > 0){
                $offset = (int)$this->input->post('start', TRUE);
                $no = ++$offset;
                foreach($section_data['records'] as $data){
                    $row = array(
                        'no' => $no,
                        'id' => $data['id'],
                        'page_name' => ucwords($data['page_name']),
                        'page_slug' => $data['page_slug'],
                        'section_name' => $data['section_name'],
                        'section_slug' => $data['section_slug'],
                        // 'content' => $data['content'],
                        'status' => $data['status'],
                        'created' => date('Y-m-d', strtotime($data['created']))
                    );

                    $data_list[] = $row;

                    $no++;
                }
            }
            $output = array(
                'data' => $data_list
            );
            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {
            exit('No direct script access allowed');
        }
    }

    public function edit($page_slug, $section_slug)
    {
        $this->load->model('pages/pages_m','pages_m');

        if(!$this->pages_m->pages_slug_exist($page_slug) || !$this->section_m->section_slug_exist($section_slug)) {
            if($this->input->is_ajax_request()){
                $this->output->set_content_type('application/json')->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Doesn\'t exist'
                ]));
            } else {
                show_404();
            }
        }

        if($this->input->is_ajax_request()){
            $this->load->library('form_validation');

            $this->form_validation->set_rules('section_slug', 'Slug', 'trim|required|min_length[1]|max_length[100]|strip_tags|xss_clean');
            $this->form_validation->set_rules('section_id', 'ID', 'trim|required|numeric|strip_tags|xss_clean');
            $this->form_validation->set_rules('page_id', 'Page ID', 'trim|required|numeric|greater_than[0]|strip_tags|xss_clean');
            $this->form_validation->set_rules('section_name', 'Page Section Name', 'trim|required|min_length[1]|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('content', 'Section Content', 'trim|min_length[1]|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('status', 'status', 'trim|required|numeric|in_list[-1,0,1]');

            if ($this->form_validation->run($this) == FALSE) {
                $output = array(
                    'status' => 0,
                    'message' => array(
                        'error' => validation_errors()
                    )
                );
            } else {
                $input = $this->input->post(NULL, TRUE);
                $result = $this->section_m->update($input['section_id']);
                $output = $result;
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {
            $this->data['section_detail'] = $this->section_m->get_section_detail(NULL,$section_slug);

            $this->template->set('page_js',array(
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/tinymce.min.js'),
                    'async' => FALSE,
                    'defer' => FALSE
                ),
                array(
                    'url' => base_url('assets/app/js/section/edit_section.js')
                ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/widgets/select2.js')
                ),
                // array(
                //  'url' => base_url('assets/demo/default/custom/components/forms/widgets/dropzone.js')
                // ),
                array(
                    'url' => base_url('assets/demo/default/custom/components/forms/validation/form-controls.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autolink/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autoresize/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/autosave/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/code/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/codesample/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/contextmenu/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/help/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/hr/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/lists/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/image/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/imagetools/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/legacyoutput/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/link/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/paste/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/preview/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/searchreplace/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/media/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/save/plugin.min.js')
                ),
                array(
                    'url' => base_url('assets/vendors/custom/tinymce/plugins/table/plugin.min.js')
                ),
            ));

            // echo "<pre>";
            // print_r ($this->data['article_metas']);
            // echo "</pre>";
            $this->template->build('edit_section', $this->data);
        }
    }

    public function hide($id){
        if(is_null($id) || !is_numeric($id) || !$this->section_m->section_exist($id)) {
            if($this->input->is_ajax_request()){
                $this->output->set_content_type('application/json')->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Doesn\'t exist'
                ]));
            } else {
                show_404();
            }
        } else {
            if($this->section_m->change_status($id, -1)){
                // $slide_detail = $this->section_m->get_section_detail($id);
                // @unlink('../assets/images/slider/'.$slide_detail['filename']);
                $output = array(
                    'status' => 1
                );
            } else {
                $output = array(
                    'status' => 0
                );
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        }
    }

    public function publish($id){
        if(is_null($id) || !is_numeric($id) || !$this->section_m->section_exist($id)) {
            if($this->input->is_ajax_request()){
                $this->output->set_content_type('application/json')->set_output(json_encode([
                    'status' => 0,
                    'message' => 'Doesn\'t exist'
                ]));
            } else {
                show_404();
            }
        } else {
            if($this->section_m->change_status($id, 1)){
                // $slide_detail = $this->section_m->get_section_detail($id);
                // @unlink('../assets/images/slider/'.$slide_detail['filename']);
                $output = array(
                    'status' => 1
                );
            } else {
                $output = array(
                    'status' => 0
                );
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        }
    }

}

/* End of file Sections.php */
/* Location: ./application/modules/sections/controllers/Sections.php */