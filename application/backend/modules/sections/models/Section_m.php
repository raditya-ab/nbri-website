<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section_m extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_section_list($page_id = NULL)
    {
        $input    = $this->input->post();
        $query    = array();
        $page     = (int)$this->input->post('pagination',TRUE)['page'];
        $per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
        $offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        // if(!is_null($page_id) && !empty($page_id)){
        //     $this->db->where('p.id', $page_id);
        // }

        $this->db->start_cache();

        $select = array(
            's.id',
            's.pages_id',
            'p.page_name',
            'p.page_slug',
            's.section_name',
            's.section_slug',
            's.status',
            's.created',
            's.modified'
        );

        $this->db->select($select);

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    // $search_select = array('section_name');
                    $this->db->or_like('LOWER(section_name)', $search);
                    // $this->db->group_start();
                    // foreach($search_select as $s)
                    // {
                    //     $this->db->or_like('LOWER('.$s.')',$search);
                    // }
                    // $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->from('pages_section s');
        $this->db->join('pages p', 'p.id = s.pages_id', 'left');
        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
        $query['filterRecords'] = $this->db->count_all_results();

        $this->db->flush_cache();

        return $query;
    }

    public function get_section_detail($id = NULL, $slug = NULL)
    {
        $select = array(
            's.id',
            'p.page_name',
            'p.page_slug',
            's.pages_id',
            's.section_name',
            's.section_slug',
            's.content',
            's.status'
        );

        $this->db->select($select);

        if($id != NULL){
            $this->db->where('s.id', $id);
        }

        if($slug != NULL){
            $this->db->where('s.section_slug', $slug);
        }

        $this->db->join('pages p', 'p.id = s.pages_id', 'left');
        $result = $this->db->get('pages_section s')->row_array();

        return $result;
    }

    public function section_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('pages_section') > 0) ? TRUE : FALSE;
    }

    public function section_slug_exist($slug)
    {
        $this->db->where('section_slug', $slug);

        return ($this->db->count_all_results('pages_section') > 0) ? TRUE : FALSE;
    }

    public function update($id)
    {
        $section_detail = $this->get_section_detail($id, NULL);
        $input = $this->input->post(NULL, TRUE);
        $section_data = array(
            'section_name' => $input['section_name'],
            'section_slug' => $input['section_slug'],
            'content' => $input['content'],
            'status' => $input['status']
        );

        $this->db->where('id', $id);
        $this->db->update('pages_section', $section_data);
        // echo $this->db->last_query();
        // exit();
        if($this->db->affected_rows() <= 0){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'pages_id' => $section_detail['pages_id'],
                    'pages_slug' => $section_detail['page_slug'],
                    'section_id' => $id,
                    'section_slug' => $input['section_slug']
                )
            );
        }

        return $output;
    }

    public function change_status($id, $status)
    {
        $this->db->where('id', $id);
        $this->db->update('pages_section', array('status' => $status));

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }
}

/* End of file Section_m.php */
/* Location: ./application/backend/modules/pages/models/Section_m.php */