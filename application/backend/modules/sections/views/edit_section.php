<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url('sections/edit/'.$section_detail['page_slug'].'/'.$section_detail['section_slug']), array('class'=>'m-form m-form--fit m-form--label-align-right form_update dropzone','id'=>'m-dropzone-one')); ?>
    <input type="hidden" id="section_id" name="section_id" id="section_id" value="<?=$section_detail['id'];?>">
	<input type="hidden" id="section_slug" name="section_slug" id="section_slug" value="<?=$section_detail['section_slug'];?>">
    <input type="hidden" id="page_id" name="page_id" value="<?=$section_detail['pages_id'];?>">
    <input type="hidden" id="page_slug" name="page_slug" id="page_slug" value="<?=$section_detail['pages_id'];?>">

	<input type="hidden" name="status" value="<?=$section_detail['status'];?>">

	<div class="row">
		<div class="col-md-12">
			<div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="error_msg">
				<div class="m-alert__icon">
					<i class="la la-warning"></i>
				</div>
				<div class="m-alert__text">
					Oh snap! Change a few things up and try submitting again.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
			<div class="m-alert m-alert--icon alert alert-success m--hide" role="alert" id="success_msg">
				<div class="m-alert__icon">
					<i class="la la-thumbs-up"></i>
				</div>
				<div class="m-alert__text">
					Success, article has been added to database.
				</div>
				<div class="m-alert__close">
					<button type="button" class="close" data-close="alert" aria-label="Close"></button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <input type="text" id="section_name" name="section_name" class="form-control" placeholder="Section Name" value="<?=$section_detail['section_name'];?>">
                    </div>
					<div class="form-group m-form__group row">
						<label class="control-label">Content</label>
						<textarea name="content" class="form-control" id="content">
                            <?php echo stripcslashes(stripslashes($section_detail['content'])); ?>
                        </textarea>
					</div>
					<!-- <div class="form-group m-form__group row">
						<label>Member Logo</label>
						<div class="clearfix" style="width: 100%;margin-bottom: 10px">
                            <img src="<?php echo base_url('../assets/images/member/'.$section_detail['img']); ?>" alt="<?php echo $section_detail['img']; ?>" style="max-width: 300px">
                        </div>
                        <div></div>
						<div class="custom-file">
							<input class="custom-file-input" name="inputfile" id="inputfile" type="file">
							<label class="custom-file-label" for="inputfile">
							Choose file
							</label>
						</div>
					</div> -->
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions">
						<div class="row">
							<div class="col m--align-center m--valign-middle">
								<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
									Save
								</button>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="m-portlet__body">
					<div class="row">
						<div class="col m--valign-middle">
							<p>
								<label><i class="fa fa-eye"></i> Visibility: <span id="visibility"><b>Public</b></span></label><br>
								<label><i class="la la-calendar"></i> Publish <a href="#." id="publish_time"><b>Immediately</b></a></label>
							</p>
						</div>
					</div>
				</div> -->
				<div class="m-portlet__foot m-portlet__foot--top m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row align-items-center">
							<div class="col m--align-left m--valign-middle">
								<button type="submit" class="btn btn-secondary btn-block btn-sm btn-submit" id="btn-draft">
									Save Draft
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>