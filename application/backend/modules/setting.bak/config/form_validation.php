<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
    'setting/update' => array(
        array(
            'field' => 'website_name',
            'label' => 'Website Name',
            'rules' => 'trim|required|min_length[5]|max_length[30]|xss_clean'
        ),
        array(
            'field' => 'address',
            'label' => 'Address',
            'rules' => 'trim|required|min_length[5]|max_length[200]|strip_tags|htmlentities|xss_clean'
        ),
        array(
            'field' => 'lat',
            'label' => 'Latitude',
            'rules' => 'trim|required|min_length[1]|max_length[30]|numeric|xss_clean|strip_tags'
        ),
        array(
            'field' => 'lng',
            'label' => 'Longitude',
            'rules' => 'trim|required|min_length[1]|max_length[30]|numeric|xss_clean|strip_tags'
        ),
        array(
            'field' => 'email',
            'label' => 'Email Address',
            'rules' => 'trim|required|min_length[1]|max_length[100]|valid_email|xss_clean|strip_tags'
        ),
        array(
            'field' => 'phone',
            'label' => 'Phone Number',
            'rules' => 'trim|required|min_length[7]|max_length[20]|xss_clean|strip_tags'
        ),
        array(
            'field' => 'fax',
            'label' => 'Fax Number',
            'rules' => 'trim|required|min_length[7]|max_length[20]|xss_clean|strip_tags'
        ),
        array(
            'field' => 'facebook_url',
            'label' => 'Facebook URL',
            'rules' => 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags'
        ),
        array(
            'field' => 'instagram_url',
            'label' => 'Instagram URL',
            'rules' => 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags'
        ),
        array(
            'field' => 'google_url',
            'label' => 'Google URL',
            'rules' => 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags'
        ),
        array(
            'field' => 'Twitter_url',
            'label' => 'Twitter URL',
            'rules' => 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags'
        ),
        array(
            'field' => 'youtube_url',
            'label' => 'Youtube URL',
            'rules' => 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags'
        ),
        array(
            'field' => 'biglogo',
            'label' => 'Website Logo',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'logo',
            'label' => 'Website Small Logo',
            'rules' => 'trim|xss_clean'
        ),
        array(
            'field' => 'favicon',
            'label' => 'Website Favicon',
            'rules' => 'trim|xss_clean'
        )
    )
);