<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('setting_m');
		$this->load->model('common_m');
		$this->template->set_layout('main');
	}

	public function index()
	{
        $setting = get_website_setting();
        if (array_key_exists('website_var', $setting)){
            redirect('setting/edit');
        } else {
            redirect('setting/add');
        }

		// $this->template->build('setting/address/index');
	}

	public function add()
	{
		if(!empty(get_website_setting()) && get_website_setting() != NULL){
            // echo "<pre>";
            // print_r (get_website_setting());
            // echo "</pre>";
			redirect('setting/edit');
		}

        $this->template->set('page_js',
            array(
                array(
                    'url' => base_url('assets/app/js/setting/new.js'),
                    'async' => FALSE,
                    'defer' => FALSE
                ),
                array(
                    'url' => "https://maps.googleapis.com/maps/api/js?key=AIzaSyDMFYwP2fCumvI-0a-z-ae74erp6vRH2gY&libraries=places",
                    'async' => TRUE,
                    'defer' => TRUE
                )
            )
        );

        $this->template->build('setting/address/add');
	}

	public function save(){
		if(is_null($this->input->server('HTTP_REFERER'))) show_404();

		if($this->input->is_ajax_request()){
			$this->load->library('form_validation');

			$this->form_validation->set_rules('website_name', 'Website Name', 'trim|required|min_length[5]|max_length[30]|xss_clean');
			$this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[5]|max_length[200]|strip_tags|htmlentities|xss_clean');
			$this->form_validation->set_rules('lat', 'Latitude', 'trim|required|min_length[1]|max_length[30]|numeric|xss_clean|strip_tags');
			$this->form_validation->set_rules('lng', 'Longitude', 'trim|required|min_length[1]|max_length[30]|numeric|xss_clean|strip_tags');
			$this->form_validation->set_rules('email', 'Email Address', 'trim|required|min_length[1]|max_length[100]|valid_email|xss_clean|strip_tags');
			$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[7]|max_length[20]|xss_clean|strip_tags');
			$this->form_validation->set_rules('fax', 'Fax Number', 'trim|required|min_length[7]|max_length[20]|xss_clean|strip_tags');
			$this->form_validation->set_rules('facebook_url', 'Facebook URL', 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags');
			$this->form_validation->set_rules('instagram_url', 'Instagram URL', 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags');
			$this->form_validation->set_rules('google_url', 'Google URL', 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags');
			$this->form_validation->set_rules('Twitter_url', 'Twitter URL', 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags');
			$this->form_validation->set_rules('youtube_url', 'Youtube URL', 'trim|valid_url|min_length[1]|max_length[50]|xss_clean|strip_tags');

			if(empty($_FILES['biglogo']['name'])){
				$this->form_validation->set_rules('biglogo', 'Website Logo', 'trim|xss_clean|required');
			} else {
				$this->form_validation->set_rules('biglogo', 'Website Logo', 'trim|xss_clean');
			}
			if(empty($_FILES['logo']['name'])){
				$this->form_validation->set_rules('logo', 'Website Small Logo', 'trim|xss_clean|required');
			} else {
				$this->form_validation->set_rules('logo', 'Website Small Logo', 'trim|xss_clean');
			}
			if(empty($_FILES['favicon']['name'])){
				$this->form_validation->set_rules('favicon', 'Website Favicon', 'trim|xss_clean|required');
			} else {
				$this->form_validation->set_rules('favicon', 'Website Favicon', 'trim|xss_clean');
			}

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$this->load->library('upload');
				$this->load->config('upload_conf');
				$this->load->helper('uploading');

				$uploaded_file = array();

				$biglogo_conf = $this->config->item('biglogo');
				$logo_conf = $this->config->item('logo');
				$favicon_conf = $this->config->item('favicon');

				$upload_index = array('biglogo','logo','favicon');

				foreach($upload_index as $param_name){
					$this->upload->initialize(${$param_name."_conf"}, TRUE);

					if ( ! $this->upload->do_upload($param_name)){
						$error = generate_file_upload_error($this->security->xss_clean($_FILES[$param_name]['name']),$this->upload->display_errors());
					}
					else{
						$uploaded_file[] = array(
							'name' => $param_name,
							'filename' => $this->upload->data('file_name')
						);
					}
				}

				if(count($uploaded_file) == count($upload_index)){
					$result = $this->setting_m->add($uploaded_file);
					$output = $result;
				} else {
					foreach($uploaded_file as $file){
						@unlink('assets/images/'.$file['filename']);
					}

					$output = array(
						'status' => 0,
						'message' => array(
							'error' => $error
						)
					);
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			show_404();
		}
	}

	public function edit()
	{
		if(empty(get_website_setting()) OR get_website_setting() == NULL){
			redirect('setting/add');
		}

		$this->data['detail'] = get_website_setting()['website_var'];

		$this->template->set('page_js',
			array(
				array(
					'url' => base_url('assets/app/js/setting/edit.js'),
					'async' => FALSE,
					'defer' => FALSE
				),
				array(
					'url' => "https://maps.googleapis.com/maps/api/js?key=AIzaSyCTWkZ_89tWeceAsNNVnHrr8y1gLsFh9qE&libraries=places",
					'async' => TRUE,
					'defer' => TRUE
				)
			)
		);

		$this->template->build('setting/address/edit', $this->data);
	}

	public function update(){
		if(is_null($this->input->server('HTTP_REFERER'))) show_404();

        if(!setting_been_set()){
			show_404();
		}

		if($this->input->is_ajax_request()){
			$this->load->library('form_validation');

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$this->load->library('upload');
				$this->load->config('upload_conf');
				$this->load->helper('uploading');

				$uploaded_file = array();

				$upload_index = array_keys($_FILES);

				if(!empty($upload_index)){
					$detail = get_website_setting();

					foreach($upload_index as $param_name){
						${$param_name."_conf"} = $this->config->item($param_name);

						$this->upload->initialize(${$param_name."_conf"}, TRUE);

						if ( ! $this->upload->do_upload($param_name)){
							$error = "Upload Pic Error: ".generate_file_upload_error($this->security->xss_clean($_FILES[$param_name]['name']),$this->upload->display_errors());
						}
						else{
							$uploaded_file[] = array(
								'name' => $param_name,
								'filename' => $this->upload->data('file_name')
							);
						}
					}

					if(count($uploaded_file) == count($upload_index)){
						$result = $this->setting_m->edit($uploaded_file);
						$output = $result;
					} else {
						foreach($uploaded_file as $file){
							@unlink('assets/images/'.$file['filename']);
						}

						$output = array(
							'status' => 0,
							'message' => array(
								'error' => "Error: ".$error
							)
						);
					}
				} else {
					$result = $this->setting_m->edit();
					$output = $result;
				}

			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			show_404();
		}
	}
}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */