<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	public function add($filenames){
		$temp = $this->input->post(NULL, TRUE);
		$input = array_map('trim', $temp);

		$data = array(
			'website_name' => $input['website_name'],
			'address' => $input['address'],
			'lat' => $input['lat'],
			'lng' => $input['lng'],
			'email' => $input['email'],
			'phone' => $input['phone'],
			'fax' => $input['fax'],
			'facebook_url' => $input['facebook_url'],
			'instagram_url' => $input['instagram_url'],
			'google_url' => $input['google_url'],
			'twitter_url' => $input['twitter_url'],
			'youtube_url' => $input['youtube_url']
		);

		foreach($filenames as $file){
			$data[$file['name']] = $file['filename'];
		}

		$this->db->insert('website_setting', $data);

		if($this->db->affected_rows() > 0){
			$website_setting = array(
				'website_name' => $input['website_name'],
				'address' => $input['address'],
				'lat' => $input['lat'],
				'lng' => $input['lng'],
				'email' => $input['email'],
				'phone' => $input['phone'],
				'fax' => $input['fax'],
				'facebook_url' => $input['facebook_url'],
				'instagram_url' => $input['instagram_url'],
				'google_url' => $input['google_url'],
				'twitter_url' => $input['twitter_url'],
				'youtube_url' => $input['youtube_url']
			);

			$this->session->set_userdata( $website_setting );

			$output = array(
			    'status' => 1
			);
		} else {
		    $output = array(
		        'status' => 0,
		        'message' => array(
		            'error' => $this->db->error()
		        )
		    );
		}

		return $output;
	}

	public function edit($filenames = NULL){
		$temp = $this->input->post(NULL, TRUE);
		$input = array_map('trim', $temp);

		$data = array(
			'website_name' => $input['website_name'],
			'address' => $input['address'],
			'lat' => $input['lat'],
			'lng' => $input['lng'],
			'email' => $input['email'],
			'phone' => $input['phone'],
			'fax' => $input['fax'],
			'facebook_url' => $input['facebook_url'],
			'instagram_url' => $input['instagram_url'],
			'google_url' => $input['google_url'],
			'twitter_url' => $input['twitter_url'],
			'youtube_url' => $input['youtube_url']
		);

		if(!is_null($filenames)){
			foreach($filenames as $file){
				$data[$file['name']] = $file['filename'];
			}
		}


		// $this->db->update('website_setting', $data);

		if(set_website_setting($data)){
			$website_setting = array(
				'website_name' => $input['website_name'],
				'address' => $input['address'],
				'lat' => $input['lat'],
				'lng' => $input['lng'],
				'email' => $input['email'],
				'phone' => $input['phone'],
				'fax' => $input['fax'],
				'facebook_url' => $input['facebook_url'],
				'instagram_url' => $input['instagram_url'],
				'google_url' => $input['google_url'],
				'twitter_url' => $input['twitter_url'],
				'youtube_url' => $input['youtube_url']
			);

            set_website_variables();
			// $this->session->set_userdata( $website_setting );

			$output = array(
			    'status' => 1
			);
		} else {
		    $output = array(
		        'status' => 0,
		        'message' => array(
		            'error' => "Error updating Data"
		        )
		    );
		}

		return $output;
	}

}

/* End of file Setting_m.php */
/* Location: ./application/backend/modules/setting/models/Setting_m.php */