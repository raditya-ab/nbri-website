<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	public function add($filenames){
		$temp = $this->input->post(NULL, TRUE);
		$input = array_map('trim', $temp);

		$data = array(
			'website_name' => $input['website_name'],
			'address' => $input['address'],
			'lat' => $input['lat'],
			'lng' => $input['lng'],
			'email' => $input['email'],
			'phone' => $input['phone'],
			'fax' => $input['fax'],
			'facebook_url' => $input['facebook_url'],
			'instagram_url' => $input['instagram_url'],
			'google_url' => $input['google_url'],
			'twitter_url' => $input['twitter_url'],
			'youtube_url' => $input['youtube_url']
		);

		foreach($filenames as $file){
			$data[$file['name']] = $file['filename'];
		}

		$this->db->insert('website_setting', $data);

		if($this->db->affected_rows() > 0){
			$website_setting = $data;

            $website_setting_session = array(
                'website_var' => $website_setting
            );

            // foreach($website_setting as $var){
            //     unset($_SESSION['website_var'][$var]);
            // }

            $this->session->set_userdata( $website_setting_session );
            set_website_setting($website_setting_session);

            $output = array(
                'status' => 1
            );
		} else {
		    $output = array(
		        'status' => 0,
		        'message' => array(
		            'error' => $this->db->error()
		        )
		    );
		}

		return $output;
	}

	public function edit($filenames = NULL){
		$temp = $this->input->post(NULL, TRUE);
		$input = array_map('trim', $temp);

		$data = array(
			'website_name' => $input['website_name'],
			'address' => $input['address'],
			'lat' => $input['lat'],
			'lng' => $input['lng'],
			'email' => $input['email'],
			'phone' => $input['phone'],
			'fax' => $input['fax'],
			'facebook_url' => $input['facebook_url'],
			'instagram_url' => $input['instagram_url'],
			'google_url' => $input['google_url'],
			'twitter_url' => $input['twitter_url'],
			'youtube_url' => $input['youtube_url']
		);

		if(!is_null($filenames)){
			foreach($filenames as $file){
				$data[$file['name']] = $file['filename'];
			}
		}

		$this->db->where('id', 1);
		$this->db->update('website_setting', $data);

		if($this->db->affected_rows() > 0){
            $current_web_setting = get_website_setting();

            $new_setting = array_merge($current_web_setting['website_var'], $data);
			
			$website_setting_session = array(
                'website_var' => $new_setting
            );

            set_website_setting($website_setting_session);

            $this->session->set_userdata( $website_setting_session );

			$output = array(
			    'status' => 1
			);
		} else {
		    $output = array(
		        'status' => 0,
		        'message' => array(
		            'error' => "No data updated. Query: ".$this->db->last_query()
		        )
		    );
		}

		return $output;
	}

}

/* End of file Setting_m.php */
/* Location: ./application/backend/modules/setting/models/Setting_m.php */