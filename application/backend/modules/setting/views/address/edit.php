<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url('setting/update'), array('class'=>'m-form m-form--fit m-form--label-align-right form_update dropzone','id'=>'m-dropzone-one')); ?>
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label>Website Name</label>
						<input type="text" class="form-control m-input" name="website_name" id="website_name" placeholder="Enter Website or Company Name" value="<?=$detail['website_name'];?>" required>
					</div>
					<div class="form-group m-form__group row">
						<label>
							Website Logo
						</label>
						<div class="image-preview" style="margin: 10px auto; width:100%">
							<img src="<?=base_url('../assets/images/'.$detail['biglogo']);?>" alt="" class="img-responsive" style="max-height: 50px">
						</div>
						<div></div>
						<div class="custom-file">
							<input class="custom-file-input" id="biglogo" name="biglogo" type="file">
							<label class="custom-file-label" for="biglogo">
								Only JPG, JPEG and PNG are allowed
							</label>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label>
							Small Logo
						</label>
						<div class="image-preview" style="margin: 10px auto; width:100%">
							<img src="<?=base_url('../assets/images/'.$detail['logo']);?>" alt="" class="img-responsive" style="max-height: 30px">
						</div>
						<div></div>
						<div class="custom-file">
							<input class="custom-file-input" id="logo" name="logo" type="file">
							<label class="custom-file-label" for="logo">
								Only JPG, JPEG and PNG are allowed
							</label>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label>
							Favicon
						</label>
						<div class="image-preview" style="margin: 10px auto; width:100%">
							<img src="<?=base_url('../assets/images/'.$detail['favicon']);?>" alt="" class="img-responsive" style="max-height: 32px">
						</div>
						<div></div>
						<div class="custom-file">
							<input class="custom-file-input" id="favicon" name="favicon" type="file">
							<label class="custom-file-label" for="favicon">
								Only ICO and PNG are allowed
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label>Detail Address</label>
						<textarea name="address" id="address" rows="5" class="form-control" required><?=$detail['address'];?></textarea>
					</div>
					<div class="form-group m-form__group row">
						<label>Google Map</label>
						<input type="text" class="form-control m-input" name="map-search" id="map-search" placeholder="Search Address">
						<div id="map" style="min-height: 300px;width: 100%;background-color: #333; margin-top: 20px"></div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-md-6">
							<input type="text" class="form-control m-input" name="lat" id="lat" placeholder="Latitude" value="<?=$detail['lat'];?>" readonly required>
						</div>
						<div class="col-md-6">
							<input type="text" class="form-control m-input" name="lng" id="lng" placeholder="Longitude" value="<?=$detail['lng'];?>" readonly required>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label>Detail Email Address</label>
						<input type="email" class="form-control m-input" name="email" id="email" placeholder="Enter Email Address" value="<?=$detail['email'];?>">
					</div>
					<div class="form-group m-form__group row">
						<label>Detail Phone Number</label>
						<input type="text" class="form-control m-input" name="phone" id="phone" placeholder="(XXX)-XXXX XXXX" value="<?=$detail['phone'];?>">
					</div>
					<div class="form-group m-form__group row">
						<label>Detail Fax Number</label>
						<input type="text" class="form-control m-input" name="fax" id="fax" placeholder="(XXX)-XXXX XXXX" value="<?=$detail['fax'];?>">
					</div>
				</div>
			</div>
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label>FB Pages <i class="fa fa-facebook"></i></label>
						<input type="text" class="form-control m-input" name="facebook_url" id="facebook_url" placeholder="http://" value="<?=$detail['facebook_url'];?>">
					</div>
					<div class="form-group m-form__group row">
						<label>IG Pages <i class="fa fa-instagram"></i></label>
						<input type="text" class="form-control m-input" name="instagram_url" id="instagram_url" placeholder="http://" value="<?=$detail['instagram_url'];?>">
					</div>
					<div class="form-group m-form__group row">
						<label>LinkedIn Pages <i class="fa fa-linkedin"></i></label>
						<input type="text" class="form-control m-input" name="google_url" id="google_url" placeholder="http://" value="<?=$detail['google_url'];?>">
					</div>
					<div class="form-group m-form__group row">
						<label>Twitter Pages <i class="fa fa-twitter"></i></label>
						<input type="text" class="form-control m-input" name="twitter_url" id="twitter_url" placeholder="http://" value="<?=$detail['twitter_url'];?>">
					</div>
					<div class="form-group m-form__group row">
						<label>Youtube Pages <i class="fa fa-youtube"></i></label>
						<input type="text" class="form-control m-input" name="youtube_url" id="youtube_url" placeholder="http://" value="<?=$detail['youtube_url'];?>">
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="row align-items-center">
						<div class="col m--align-left m--valign-middle">
							<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
								Save
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

