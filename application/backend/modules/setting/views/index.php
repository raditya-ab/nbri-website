<!--begin::Portlet-->
<div class="m-portlet m-portlet--tabs">
  <div class="m-portlet__head">
    <div class="m-portlet__head-tools">
      <ul class="nav nav-tabs m-tabs-line m-tabs-line--2x m-tabs-line--primary" role="tablist">
        <li class="nav-item m-tabs__item">
          <a class="nav-link m-tabs__link active" data-toggle="tab" href="#general" role="tab" aria-selected="false">
            <i class="flaticon-multimedia"></i>
            General
          </a>
        </li>
        <li class="nav-item m-tabs__item">
          <a class="nav-link m-tabs__link" data-toggle="tab" href="#menu" role="tab" aria-selected="false">
            <i class="flaticon-cogwheel-2"></i>
            Menu
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="m-portlet__body">
    <div class="tab-content">
      <div class="tab-pane active" id="general" role="tabpanel">
       <div class="row">
         <div class="col-md-12">
           <?php echo form_open('url', ''); ?>
           <div class="form-group">
             <label>Website Name</label>
             <input type="text" class="form-control">
           </div>
           <?php echo form_close(); ?>
         </div>
       </div>
      </div>
      <div class="tab-pane" id="menu" role="tabpanel">
        <div class="row">
          <div class="col-md-5">
            <div class="m-section">
              <h2 class="m-section__heading">Menu Structure</h2>
              <div class="m-section__content">
                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                  <div class="m-demo__preview">
                    <div id="m_tree_5" class="tree-demo"></div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <button class="btn btn-primary">Save</button>
                  <button class="btn btn-default">Revert Changes</button>
                </div>  
              </div>
            </div>
          </div>
          <div class="col">
            <div class="m-section">
              <h2 class="m-section__heading">Add New Menu</h2>
              <div class="row">
                <div class="col-12">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" name="" value="" placeholder="Menu Name">
                  </div>
                  <div class="form-group">
                    <button class="btn btn-primary">Add</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="m_tabs_6_3" role="tabpanel">
       e Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged
      </div>
    </div>
  </div>
</div>
<!--end::Portlet-->