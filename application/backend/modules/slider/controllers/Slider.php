<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends Admin_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('slider_m');
		$this->template->set_layout('main');
	}

	public function index()
	{
		$this->data['slider_list'] = $this->slider_m->get_sliders();
		$this->template->set('page_js',
				array(
					array(
						'url' => base_url('assets/app/js/slider/index.js'),
						'async' => FALSE,
						'defer' => FALSE
					)
				)
			);

		$this->template->build('index', $this->data);
	}

	public function add()
	{
		$this->load->library('form_validation');
		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('title', 'Slider Title', 'trim|min_length[5]|max_length[255]|strip_tags|xss_clean');
			$this->form_validation->set_rules('subtitle', 'Slider Caption', 'trim|min_length[5]|max_length[255]|strip_tags|xss_clean');
			$this->form_validation->set_rules('url', 'URL', 'trim|prep_url|valid_url|min_length[5]|max_length[100]');
            $this->form_validation->set_rules('type', 'Slide Type', 'trim|required|numeric|in_list[1,2,3]');
			if(empty($_FILES['inputfile']['name'])){
				$this->form_validation->set_rules('inputfile', 'Slide Image', 'trim|xss_clean|required');
			} else {
				$this->form_validation->set_rules('inputfile', 'Slide Image', 'trim|xss_clean');
			}

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				$this->load->library('upload');
				$this->load->config('upload_conf');

				$upload_conf = $this->config->item('slider');

				$this->upload->initialize($upload_conf);

				if ( ! $this->upload->do_upload('inputfile')){
					$output = array(
						'status' => 0,
						'message' => array(
							'error' => $this->upload->display_errors()
						)
					);
				}
				else{
					$uploaded_file = $this->upload->data('file_name');
					$result = $this->slider_m->add($uploaded_file);
					$output = $result;
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->template->set('page_js',
				array(
					array(
						'url' => base_url('assets/app/js/slider/new.js')
					)
				)
			);
			$this->template->build('add', $this->data);
		}
	}

	public function edit($id)
	{
		if(is_null($id) || !is_numeric($id) || !$this->slider_m->slide_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		}

		$this->load->library('form_validation');

		$this->data['slide_detail'] = $this->slider_m->get_detail($id);

		if($this->input->is_ajax_request()){
			$this->form_validation->set_rules('title', 'Slider Title', 'trim|min_length[5]|max_length[255]|strip_tags|xss_clean');
			$this->form_validation->set_rules('subtitle', 'Slider Caption', 'trim|min_length[5]|max_length[255]|strip_tags|xss_clean');
			$this->form_validation->set_rules('url', 'URL', 'trim|prep_url|valid_url|min_length[5]|max_length[100]');
            $this->form_validation->set_rules('type', 'Slide Type', 'trim|required|numeric|in_list[1,2,3]');
			$this->form_validation->set_rules('inputfile', 'Slide Image', 'trim|xss_clean');

			if ($this->form_validation->run() == FALSE) {
				$output = array(
					'status' => 0,
					'message' => array(
						'error' => validation_errors()
					)
				);
			} else {
				if(empty($_FILES['inputfile']['name'])){
					$result = $this->slider_m->edit($id);
					$output = $result;
				} else {
					$this->load->library('upload');
					$this->load->config('upload_conf');

					$upload_conf = $this->config->item('slider');

					$this->upload->initialize($upload_conf);

					if ( ! $this->upload->do_upload('inputfile')){
						$output = array(
							'status' => 0,
							'message' => array(
								'error' => $this->upload->display_errors()
							)
						);
					}
					else{
						@unlink('../assets/images/slider/'.$this->data['slide_detail']['filename']);
						$uploaded_file = $this->upload->data('file_name');
						$result = $this->slider_m->edit($id, $uploaded_file);
						$output = $result;
					}
				}

			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			$this->template->set('page_js',
				array(
					array(
						'url' => base_url('assets/app/js/slider/edit.js')
					)
				)
			);
			$this->template->build('edit', $this->data);
		}
	}

	public function delete($id){
		if(is_null($id) || !is_numeric($id) || !$this->slider_m->slide_exist($id)) {
			if($this->input->is_ajax_request()){
				$this->output->set_content_type('application/json')->set_output(json_encode([
					'status' => 0,
					'message' => 'Doesn\'t exist'
				]));
			} else {
				show_404();
			}
		} else {
			if($this->slider_m->delete_slider($id)){
				$slide_detail = $this->slider_m->get_detail($id);
				@unlink('../assets/images/slider/'.$slide_detail['filename']);
				$output = array(
					'status' => 1
				);
			} else {
				$output = array(
					'status' => 0
				);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		}
	}

}

/* End of file Slider.php */
/* Location: ./application/backend/modules/slider/controllers/Slider.php */