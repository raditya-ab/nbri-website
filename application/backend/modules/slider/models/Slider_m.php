<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	public function get_sliders(){
		// $this->db->where('status', 1);
		$this->db->order_by('status', 'desc');
		$this->db->order_by('modified', 'desc');
		$this->db->order_by('created', 'desc');
		return $this->db->get('sliders')->result_array();
	}

	public function get_detail($id) {
		$this->db->where('id', $id);

		return $this->db->get('sliders')->row_array();
	}

	public function add($filename){
		$input_data = array(
			'title' => $this->input->post('title', TRUE),
			'subtitle' => $this->input->post('subtitle', TRUE),
			'url' => $this->input->post('url', TRUE),
            'type' => $this->input->post('type', TRUE),
			'filename' => $filename
		);

		$this->db->insert('sliders',$input_data);

		if($this->db->affected_rows() > 0){
		    $output = array(
		        'status' => 1,
		        'message' => array(
		            'slide_id' => $this->db->insert_id()
		        )
		    );
		} else {
		    $output = array(
		        'status' => 0,
		        'message' => array(
		            'error' => $this->db->error()
		        )
		    );
		}

		return $output;
	}

	public function edit($id, $filename = NULL){
		$input_data = array(
			'title' => $this->input->post('title', TRUE),
			'subtitle' => $this->input->post('subtitle', TRUE),
			'url' => $this->input->post('url', TRUE),
            'type' => $this->input->post('type', TRUE),
		);

		if(!is_null($filename) && !empty($filename)){
			$input_data['filename'] = $filename;
		}

		$this->db->where('id', $id);
		$this->db->update('sliders',$input_data);

		if($this->db->affected_rows() > 0){
		    $output = array(
		        'status' => 1,
		        'message' => array(
		            'slide_id' => $id
		        )
		    );
		} else {
		    $output = array(
		        'status' => 0,
		        'message' => array(
		            'error' => $this->db->error()
		        )
		    );
		}

		return $output;
	}

	public function delete_slider($id){
		$this->db->where('id', $id);

		$this->db->delete('sliders');

		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	public function slide_exist($id)
	{
	    $this->db->where('id', $id);

	    return ($this->db->count_all_results('sliders') > 0) ? TRUE : FALSE;
	}

}

/* End of file Slider_m.php */
/* Location: ./application/backend/modules/slider/models/Slider_m.php */