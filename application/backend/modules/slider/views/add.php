<style>
	.dropzone {
		min-height: 0;
		border: none;
		background: transparent;
		padding: 0px;
	}
</style>
<?php echo form_open_multipart(site_url('slider/add'), array('class'=>'m-form m-form--fit m-form--label-align-right form_create dropzone','id'=>'m-dropzone-one')); ?>
    <input type="hidden" name="status">
	<div class="row">
		<div class="col-md-9" id="form_main">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="form-group m-form__group row">
						<label>Title</label>
						<input type="text" class="form-control m-input" name="title" id="title" placeholder="Enter Slider Title">
					</div>
					<div class="form-group m-form__group row">
						<label>Caption</label>
						<textarea name="subtitle" id="subtitle" rows="5" class="form-control"></textarea>
					</div>
					<div class="form-group m-form__group row">
						<label>URL</label>
						<input type="text" class="form-control m-input" name="url" id="url" placeholder="http://">
					</div>
					<div class="form-group m-form__group row">
						<label>
							Image
						</label>
						<div></div>
						<div class="custom-file">
							<input class="custom-file-input" id="inputfile" name="inputfile" type="file" required>
							<label class="custom-file-label" for="inputfile">
								Choose file. Only JPG, JPEG and PNG are allowed
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3" id="form_sidebar">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__body">
					<div class="row align-items-center">
						<div class="col m--align-left m--valign-middle">
							<button type="submit" class="btn btn-success btn-block btn-submit" id="btn-publish">
								Save
							</button>
						</div>
					</div>
				</div>
			</div>
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Slide Type
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="form-group m-form__group row">
                            <div class="m-radio-list">
                                <label class="m-radio m-radio--solid m-radio--success">
                                    <input type="radio" name="type" value="1" data-slug="1">
                                    Type 1<br><small>(Image on the left, text full block on the right)</small>
                                    <span></span>
                                </label>
                                <label class="m-radio m-radio--solid m-radio--success">
                                    <input type="radio" name="type" value="2" data-slug="2">
                                    Type 2<br><small>(Image full-width, text on the right)</small>
                                    <span></span>
                                </label>
                                <label class="m-radio m-radio--solid m-radio--success">
                                    <input type="radio" name="type" value="3" data-slug="3">
                                    Type 3<br><small>(Image full-width, text on the center)</small>
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
<?php echo form_close(); ?>