<style>
	.slider-item {
		margin-bottom: 20px
	}
</style>
<div class="m-portlet m-portlet--mobile">
  <div class="m-portlet__body">
  	<div class="row row-eq-height">
  		<?php if(!is_null($slider_list) && !empty($slider_list)): ?>
	  		<?php foreach($slider_list as $slider): ?>
		  		<div class="col-md-3 text-center slider-item" style="position: relative; min-height: 200px">
                    <div class="image-container">
    		  			<img src="<?php echo base_url('../assets/images/slider/'.$slider['filename']); ?>" alt="<?php echo $slider['filename'];?>" style="width: 100%; margin-bottom: 5px">
                    </div>
                    <div class="image-nav-container" style="position: absolute; bottom: 0; left: 50%; transform: translateX(-50%);">
                        <?php
                            switch($slider['status']){
                                case 0:
                                    $status_text = 'Unpublished';
                                    $class = array(
                                        'btn-class'=> 'btn-publish',
                                        'btn-status-color' => 'btn-light',
                                    );
                                    break;
                                case 1:
                                    $status_text = 'Published';
                                    $class = array(
                                        'btn-class'=> 'btn-unpublish',
                                        'btn-status-color' => 'btn-success',
                                    );
                                    break;
                            }
                        ?>
                        <button type="button" id="slider-<?=$slider['id'];?>" class="btn btn-sm btn-shadow mb-2 <?=$class['btn-status-color'].' '.$class['btn-class'];?>" onclick="alert(this.id)"><?=$status_text;?></button><br>
    		  			<div class="btn-group">
    		  				<a target="_blank" href="<?php echo base_url('../assets/images/slider/'.$slider['filename']); ?>" class="btn btn-sm btn-success btn-view"><i class="la la-eye"></i></a>
    		  				<a href="<?php echo site_url('slider/edit/'.$slider['id']); ?>" class="btn btn-sm btn-warning btn-edit" title="Edit Slider"><i class="la la-pencil"></i></a>
    			  			<button class="btn btn-sm btn-danger btn-delete" data-id="<?php echo $slider['id'];?>" title="Delete Slider"><i class="la la-trash-o"></i></button>
    		  			</div>
                    </div>
                    <br>
		  		</div>
	  		<?php endforeach; ?>
	  	<?php endif; ?>
  	</div>
  </div>
  <div class="m-portlet__footer">
  	<div class="row">
  		<div class="col-md-12 text-right">
  			<a href="<?=site_url('slider/add');?>" class="btn btn-primary"><i class="la la-plus-square"></i> Add New</a>
  		</div>
  	</div>
  </div>
</div>