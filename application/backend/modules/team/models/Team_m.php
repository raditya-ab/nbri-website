<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $team_data = array(
            'name' => $input['name'],
            'slug' => $input['slug'],
            'title' => $input['title'],
            'subtitle' => $input['subtitle'],
            'description' => $input['content'],
            'status' => $input['status'],
            'img' => $filename
        );

        $this->db->insert('team', $team_data);

        if($this->db->affected_rows() == 0){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $team_id = $this->db->insert_id();
            $output = array(
                'status' => 1,
                'message' => array(
                    'team_id' => $team_id,
                    'team_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function update($id, $filename = NULL)
    {
        $team_id = $id;
        $input = $this->input->post(NULL, TRUE);
        $team_data = array(
            'name' => $input['name'],
            'slug' => $input['slug'],
            'title' => $input['title'],
            'subtitle' => $input['subtitle'],
            'description' => $input['content'],
            'status' => $input['status'],
        );


        if(!is_null($filename) && !empty($filename)) $team_data['img'] = $filename;

        $this->db->where('id', $team_id);
        $this->db->update('team', $team_data);

        $this->db->trans_complete();

        if($this->db->affected_rows() <= 0){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'team_id' => $team_id,
                    'team_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('team');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_team_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            'p.id',
            'p.name',
            'p.slug',
            'p.title',
            'p.subtitle',
            'p.img',
            'p.created',
            'p.modified',
            'p.status'
        );

        $this->db->select($select);
        $this->db->from('team p');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    // $search_select = array('a.name','up.first_name');
                    $this->db->or_like('LOWER(p.name)', $search);
                    // $this->db->group_start();
                    // foreach($search_select as $s)
                    // {
                    //     $this->db->or_like('LOWER('.$s.')',$search);
                    // }
                    // $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_categories()
    {
        $this->db->select('id, category_name, slug');
        $result = $this->db->get('categories');

        return $result->result_array();
    }

    public function get_tags()
    {
        $this->db->select('id, tag_name, slug');
        $result = $this->db->get('tags');

        return $result->result_array();

    }

    /*----------  Get Specific Data  ----------*/

    public function get_team_detail($id)
    {
        $select = array(
            'p.id',
            'p.name',
            'p.slug',
            'p.title',
            'p.subtitle',
            'p.description',
            'p.img',
            'p.created',
            'p.status'
        );

        $this->db->select($select);
        $this->db->from('team p');

        $this->db->where('p.id', $id);

        $result = $this->db->get()->row_array();

        return $result;
    }

    /*----------  Checking  ----------*/

    public function team_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('team') > 0) ? TRUE : FALSE;
    }


}

/* End of file Article_m.php */