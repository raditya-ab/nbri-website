<!DOCTYPE html>
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>

			<?=strtoupper(@$website_var['website_name']);?> Admin | <?=@$template['title'];?>
		</title>

		<?=@$template['metadata'];?>

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {"families":["Montserrat:300,400,500,600,700","Roboto:300,400,500,600,700"]},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>
		<!--end::Web font -->
		<!--begin::Base Styles -->
		<!--begin::Page Vendors -->
		<?php if(isset($page_css)): ?>
		  <?php foreach($page_css as $css): ?>
		    <link rel="stylesheet" href="<?=$css['url'];?>">
		  <?php endforeach; ?>
		<?php endif; ?>
		<!--end::Page Vendors -->
		<link href="<?=base_url('assets/vendors/base/vendors.bundle.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?=base_url('assets/demo/demo4/base/style.bundle.css');?>" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="<?=base_url('../assets/images/'.@$website_var['favicon']);?>" />
	</head>
	<!-- end::Head -->
    <!-- start::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-login m-login--signin  m-login--5" id="m_login" style="background-image: url(<?=base_url('assets/app/media/img//bg/bg-3.jpg');?>);">
				<div class="m-login__wrapper-1 m-portlet-full-height">
					<div class="m-login__wrapper-1-1">
						<div class="m-login__contanier">
							<div class="m-login__content">
								<div class="m-login__logo">
									<a href="#">
										<img src="<?=base_url('../assets/images/'.@$website_var['biglogo']);?>" style="max-height: 100px;">
									</a>
								</div>
								<div class="m-login__title">
									<h3>
										<?=strtoupper(@$website_var['website_name']);?>
									</h3>
								</div>
								<!-- <div class="m-login__desc">
									Didn't have an account?
								</div>
								<div class="m-login__form-action">
									<button type="button" id="m_login_signup" class="btn btn-outline-focus m-btn--pill">
										Get An Account
									</button>
								</div> -->
							</div>
						</div>
						<div class="m-login__border">
							<div></div>
						</div>
					</div>
				</div>
				<div class="m-login__wrapper-2 m-portlet-full-height">
					<div class="m-login__contanier">
						<?=$template['body'];?>
					</div>
				</div>
			</div>
		</div>

		<script>
			var siteURL = '<?=site_url();?>';
			var baseURL = '<?=base_url();?>';
		</script>
		<!-- end:: Page -->
    	<!--begin::Base Scripts -->
		<script src="<?=base_url('assets/vendors/base/vendors.bundle.js');?>" type="text/javascript"></script>
		<script src="<?=base_url('assets/demo/demo4/base/scripts.bundle.js');?>" type="text/javascript"></script>
		<!--end::Base Scripts -->
        <!--begin::Page Snippets -->
		<script src="<?=base_url('assets/snippets/pages/user/login.js');?>" type="text/javascript"></script>
		<?php if(isset($page_js)): ?>
		  <?php foreach($page_js as $js): ?>
		    <script src="<?=$js['url'];?>" <?=($js['async']) ? 'async' : '';?> <?=($js['defer']) ? 'defer' : '';?> ></script>
		  <?php endforeach; ?>
		<?php endif; ?>
		<!--end::Page Snippets -->
	</body>
	<!-- end::Body -->
</html>
