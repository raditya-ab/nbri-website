<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
  <!-- begin::Head -->
  <head>
    <meta charset="utf-8" />
    <title>
      <?php echo $this->session->userdata('website_var')['website_name']; ?> | <?=@$template['title'];?>
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- <script src="https://cdn.tiny.cloud/1/94xqed24ntosz6b25wg39gtn8md1noiz52nwuofb4kwyz5lk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
    </script>
    <!--end::Web font -->
        <!--begin::Base Styles -->
        <!--begin::Page Vendors -->
    <!-- <link href="<?php echo base_url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css');?>" rel="stylesheet" type="text/css" /> -->
    <!--end::Page Vendors -->
    <link href="<?php echo base_url('assets/vendors/base/vendors.bundle.css');?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/demo/default/base/style.bundle.css');?>" rel="stylesheet" type="text/css" />

    <?php if(isset($page_css)): ?>
      <?php foreach($page_css as $css): ?>
        <link rel="stylesheet" href="<?=$css;?>">
      <?php endforeach; ?>
    <?php endif; ?>

    <!--end::Base Styles -->
    <link rel="shortcut icon" href="<?php echo base_url('../assets/images/favicon/favicon.ico');?>" />
  </head>
  <!-- end::Head -->
    <!-- end::Body -->
  <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
      <!-- BEGIN: Header -->
      <header class="m-grid__item    m-header "  data-minimize-offset="200" data-minimize-mobile-offset="200" >
        <div class="m-container m-container--fluid m-container--full-height">
          <div class="m-stack m-stack--ver m-stack--desktop">
            <!-- BEGIN: Brand -->
            <div class="m-stack__item m-brand  m-brand--skin-dark ">
              <div class="m-stack m-stack--ver m-stack--general">
                <div class="m-stack__item m-stack__item--middle m-brand__logo">
                  <a href="<?php echo site_url() ?>" class="m-brand__logo-wrapper">
                    <img alt="" src="<?php echo base_url('../assets/images/'.$_SESSION['website_var']['biglogo']); ?>" style="max-height: 40px;"/>
                  </a>
                </div>
                <div class="m-stack__item m-stack__item--middle m-brand__tools">
                  <!-- BEGIN: Left Aside Minimize Toggle -->
                  <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
                    <span></span>
                  </a>
                  <!-- END -->
                  <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                  <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                    <span></span>
                  </a>
                  <!-- END -->
                  <!-- BEGIN: Responsive Header Menu Toggler -->
                  <!-- <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                    <span></span>
                  </a> -->
                  <!-- END -->
                  <!-- BEGIN: Topbar Toggler -->
                  <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                    <i class="flaticon-more"></i>
                  </a>
                  <!-- BEGIN: Topbar Toggler -->
                </div>
              </div>
            </div>
            <!-- END: Brand -->
            <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
              <!-- BEGIN: Horizontal Menu -->
              <?php echo @$template['partials']['horizontal_menu']; ?>
              <!-- END: Horizontal Menu -->
              <!-- BEGIN: Topbar -->
              <?php echo @$template['partials']['topbar']; ?>
              <!-- END: Topbar -->
            </div>
          </div>
        </div>
      </header>
      <!-- END: Header -->
      <!-- begin::Body -->
      <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <?php echo @$template['partials']['sidebar']; ?>
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title  m-subheader__title--separator">
                  <?php echo $template['title']; ?>
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                  <li class="m-nav__item m-nav__item--home">
                    <a href="<?=site_url();?>" class="m-nav__link m-nav__link--icon" style="margin-right: 0px; padding-right: 0px !important">
                      <i class="m-nav__link-icon la la-home"></i>
                    </a>
                  </li>
                  <li class="m-nav__separator">
                    -
                  </li>
                </ul>
              </div>
              <?php echo @$template['partials']['datepicker']; ?>
            </div>
          </div>
          <!-- END: Subheader -->
          <div class="m-content">
            <?php echo @$template['body']; ?>
          </div>
        </div>
      </div>
      <!-- end:: Body -->
      <!-- begin::Footer -->
      <footer class="m-grid__item   m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
          <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
              <span class="m-footer__copyright">
                2020 &copy; National Battery Research Institute by
                <a href="#" class="m-link">
                  Raditya
                </a>
              </span>
            </div>
          </div>
        </div>
      </footer>
      <!-- end::Footer -->
    </div>
    <!-- end:: Page -->

      <!-- begin::Scroll Top -->
    <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
      <i class="la la-arrow-up"></i>
    </div>
    <!-- end::Scroll Top -->

    <script>
      var baseURL = '<?=base_url();?>';
      var siteURL = '<?=site_url();?>';
      var frontURL = '<?=FRONT_URL;?>';
    </script>

      <!--begin::Base Scripts -->
    <script src="<?php echo base_url('assets/vendors/base/vendors.bundle.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/demo/default/base/scripts.bundle.js');?>" type="text/javascript"></script>
    <!--end::Base Scripts -->

    <script>
      $(document).ready(function() {
        var menu = $("#m_ver_menu").mMenu();

        var activeMenu = "<?=$active_menu;?>";
        console.log(activeMenu);

        if(!activeMenu.includes('edit') && !activeMenu.match('edit')){
          menu.setActiveItem("#"+activeMenu+"_menu");

          var breadCrumbs = menu.getBreadcrumbs("#"+activeMenu+"_menu");

          $.each(breadCrumbs, function(index, el) {
            var breadHref = el.href;
            var breadTitle = el.text.trim();

            var breads = generateBreadCrumb(breadHref,breadTitle);

            $(".m-subheader__breadcrumbs").append(breads);

            if(index < ((breadCrumbs.length)-1)){
              $(".m-subheader__breadcrumbs").append($('<li>', {
                class: 'm-nav__separator',
                html: '&nbsp-&nbsp;'
              }));
            }
          });
        }

        function generateBreadCrumb(href,title){
          var breadCrumbsHTML = '<li class="m-nav__item">';
          breadCrumbsHTML += '<a href="'+href+'" class="m-nav__link">';
          breadCrumbsHTML += '<span class="m-nav__link-text">';
          breadCrumbsHTML += title;
          breadCrumbsHTML += '</span>';
          breadCrumbsHTML += '</a>';
          breadCrumbsHTML += '</li>';

          return breadCrumbsHTML;
        }
      });


    </script>

    <script>
      Dropzone.autoDiscover = false;
    </script>

    <!--begin::Page Vendors -->
    <script src="<?php echo base_url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js');?>" type="text/javascript"></script>
    <!--end::Page Vendors -->
    <!--begin::Page Snippets -->

      <?php if(isset($page_js)): ?>
        <?php foreach($page_js as $js): ?>
            <?php if(@$js['attr'] != NULL && is_array(@$js['attr'])): ?>
                <?php
                    $js_attr = '';
                    foreach($js['attr'] as $key => $val){
                        $js_attr .= "data-$key='$val'";
                        $js_attr .= ' ';
                    }
                ?>
            <?php endif; ?>
          <script type="text/javascript" src="<?=$js['url'];?>" <?=(@$js_attr) ? @$js_attr : '';?> <?=(@$js['async']) ? 'async' : '';?> <?=(@$js['defer']) ? 'defer' : '';?> ></script>
        <?php endforeach; ?>
      <?php endif; ?>
    <!--end::Page Snippets -->
  </body>
  <!-- end::Body -->
</html>
