<!-- Begin: Nav Menu -->
<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--align-push" data-dropdown-toggle="click" aria-expanded="true">
  <a href="#" class="dropdown-toggle m-dropdown__toggle btn btn-outline-metal m-btn  m-btn--icon m-btn--pill">
    <span>
      <?php echo !is_null($this->uri->segment(1)) ? ucwords($this->uri->segment(1)) : 'Dashboard'; ?>
    </span>
  </a>
  <div class="m-dropdown__wrapper">
    <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
    <div class="m-dropdown__inner">
      <div class="m-dropdown__body">
        <div class="m-dropdown__content">
          <ul class="m-nav">
            <li class="m-nav__section m-nav__section--first m--hide">
              <span class="m-nav__section-text">
                Menu
              </span>
            </li>
            <li class="m-nav__item">
              <a href="<?=site_url();?>" class="m-nav__link">
                <i class="m-nav__link-icon flaticon-share"></i>
                <span class="m-nav__link-text">
                  Dashboard
                </span>
              </a>
            </li>
            <li class="m-nav__separator m-nav__separator--fit"></li>
            <li class="m-nav__item">
              <a href="<?=site_url('article');?>" class="m-nav__link">
                <i class="m-nav__link-icon flaticon-share"></i>
                <span class="m-nav__link-text">
                  Articles
                </span>
              </a>
            </li>
            <li class="m-nav__item">
              <a href="<?=site_url('comments');?>" class="m-nav__link">
                <i class="m-nav__link-icon flaticon-chat-1"></i>
                <span class="m-nav__link-text">
                  Comments
                </span>
              </a>
            </li>
            <li class="m-nav__separator m-nav__separator--fit"></li>
            <li class="m-nav__item">
              <a href="<?=site_url('gallery');?>" class="m-nav__link">
                <i class="m-nav__link-icon flaticon-info"></i>
                <span class="m-nav__link-text">
                  Media Gallery
                </span>
              </a>
            </li>
            <li class="m-nav__separator m-nav__separator--fit"></li>
            <li class="m-nav__item">
              <a href="<?=site_url('social_media');?>" class="m-nav__link">
                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                <span class="m-nav__link-text">
                  Social Media
                </span>
              </a>
            </li>
            <li class="m-nav__separator m-nav__separator--fit"></li>
            <li class="m-nav__item">
              <a href="<?=site_url('user_management');?>" class="m-nav__link">
                <i class="m-nav__link-icon flaticon-chat-1"></i>
                <span class="m-nav__link-text">
                  User &amp; Roles
                </span>
              </a>
            </li>
            <li class="m-nav__item">
              <a href="<?=site_url('setting');?>" class="m-nav__link">
                <i class="m-nav__link-icon flaticon-chat-1"></i>
                <span class="m-nav__link-text">
                  Site Settings
                </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End: Nav Menu -->