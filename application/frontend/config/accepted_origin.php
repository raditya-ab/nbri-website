<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['accepted_origin'] = array(
	'http://localhost/',
	'http://127.0.0.1/',
	'::1',
	'https://www.n-bri.org',
	'http://www.n-bri.org',
    'http://n-bri.local'
);
 ?>