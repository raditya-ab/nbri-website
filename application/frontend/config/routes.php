<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
require_once( BASEPATH .'database/DB'. EXT );

$route['default_controller'] = 'pages/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Page Navigation (Homepage, About, Contact)
$route['home'] = 'pages/home';
$route['about'] = 'pages/about';
$route['contact'] = 'pages/contact';
$route['expert_panel'] = 'pages/expert_panel';


// Research Navigation
$route['research'] = 'pages/research';

$db =& DB();
$select = array(
    'r.id',
    'r.research_category',
    'rc.category_name',
    'r.research_name',
    'rc.slug as category_slug',
    'r.slug',
    'r.filename',
    'r.img_thumb',
    'r.content'
);
$db->select($select);
$db->from('research r');
$db->join('research_category rc', 'rc.id = r.research_category', 'left');

$result = $db->get()->result();

foreach( $result as $row )
{
    $route[ 'research/'.$row->category_slug.'/'.$row->slug ] = 'pages/research_detail/'.$row->category_slug.'/'.$row->slug;
    // $route[ 'product/category/'.$row->gender.'/(:num)/(:any)' ] = 'product/detail/$1/$2';
}

$db->select('slug');
$result = $db->get('research_category')->result();

foreach($result as $row){
    $route['research/'.$row->slug] = 'pages/research_sub/'.$row->slug;
}


// Articles Navigation (News, Event, Education and Skill, Opportunities)

$route['news'] = 'articles/news/index';

$db->select('slug');
$db->from('articles');
$db->where('category', '1');

$result = $db->get()->result();

foreach($result as $row)
{
    $route['news/'.$row->slug] = 'articles/news/detail/'.$row->slug;
}

// Event
$route['events'] = 'articles/events/index';

$db->select('slug');
$db->from('articles');
$db->where('category', '2');

$result = $db->get()->result();

foreach($result as $row)
{
    $route['events/'.$row->slug] = 'articles/events/detail/'.$row->slug;
}

// Education and Skill
$route['education-and-skill'] = 'articles/education/index';

$db->select('slug');
$db->from('articles');
$db->where('category', '4');

$result = $db->get()->result();

foreach($result as $row)
{
    $route['education-and-skill/'.$row->slug] = 'articles/education/detail/'.$row->slug;
}

// Opportunities
$route['opportunities'] = 'articles/opportunities/index';

// $db->select('a.slug,s.slug as subcategory_slug');
// $db->from('articles a');
// $db->join('subcategories s','a.subcategory = s.id', 'left');
// $db->where('a.category', '3');
$query = "SELECT a.slug FROM articles a where a.category = 3 AND a.id NOT IN (SELECT b.subcategory FROM articles b WHERE subcategory IS NOT NULL AND b.category = 3)";

$result = $db->query($query)->result();

foreach($result as $row)
{
    $route['opportunities/'.$row->slug] = 'articles/opportunities/detail/'.$row->slug;
    // $route['opportunities/sub/'.$row->slug] = 'articles/opportunities/index_subcategory/'.$row->slug;

    // $route['opportunities/'.$row->subcategory_slug] = 'articles/opportunities/sub_index/'.$row->subcategory_slug;
}

$db->select('a.slug, b.slug as subcategory_slug');
$db->from('articles a');
$db->join('articles b','a.subcategory = b.id');

$result = $db->get()->result();
// echo $db->last_query();

foreach($result as $row)
{
    $route['opportunities/'.$row->subcategory_slug] = 'articles/opportunities/index_subcategory/'.$row->subcategory_slug;
    $route['opportunities/'.$row->subcategory_slug.'/'.$row->slug] = 'articles/opportunities/detail/'.$row->slug;

    // $route['opportunities/'.$row->subcategory_slug] = 'articles/opportunities/sub_index/'.$row->subcategory_slug;
}

$db->select('id,slug');
$db->from('categories');
$db->where('id NOT IN (1,2,3,4)',NULL);
$result = $db->get()->result();

foreach($result as $row)
{
    $route['event/'.$row->slug] = 'pages/event/index/'.$row->id.'/'.$row->slug;
    $route['event/'.$row->slug.'/news'] = 'pages/event/news_index/'.$row->id.'/'.$row->slug;
    $route['event/'.$row->slug.'/news/(:any)'] = 'pages/event/news_detail/'.$row->id.'/'.$row->slug.'/$1';
    $route['event/'.$row->slug.'/(:any)'] = 'pages/event/pages/'.$row->id.'/'.$row->slug.'/$1';
}

$route['news'] = 'articles/news/index';

$db->select('slug');
$db->from('articles');
$db->where('category', '1');

$result = $db->get()->result();

foreach($result as $row)
{
    $route['news/'.$row->slug] = 'articles/news/detail/'.$row->slug;
}

//=============== DONE ===================//


$route['publication/catalogue'] = 'publication/catalogue';
$route['publication/(:any)'] = 'publication/detail/$1';

$route['nbri-onemap'] = 'pages/maps';

$route['membership/individual'] = 'membership/index/4';
$route['membership/institution'] = 'membership/index/5';
$route['membership/corporate'] = 'membership/index/6';

$route['login'] = 'auth/login/index';
$route['register'] = 'auth/register/index';

// $route['event/icb-rev-2021'] = 'event/icb-rev-2021/homepage';
// $route['event/icb-rev-2021/agenda'] = 'event/icbrev2021/agenda';
// $route['event/icb-rev-2021/conference'] = 'event/icbrev2021/conference';
// $route['event/icb-rev-2021/school'] = 'event/icbrev2021/school';
// $route['event/icb-rev-2021/exhibition'] = 'event/icbrev2021/exhibition';
// $route['event/icb-rev-2021/registration'] = 'event/icbrev2021/registration';
// $route['event/icb-rev-2021/speakers'] = 'event/icbrev2021/speakers';
// $route['event/icb-rev-2021/publication'] = 'event/icbrev2021/publication';
// $route['event/icb-rev-2021/program-book'] = 'event/icbrev2021/program_book';
// $route['event/icb-rev-2021/organization'] = 'event/icbrev2021/organization';
// $route['event/icb-rev-2021/registration/summary'] = 'event/icbrev2021/summary';
// $route['event/icb-rev-2021/registration/checkout'] = 'event/icbrev2021/checkout';
