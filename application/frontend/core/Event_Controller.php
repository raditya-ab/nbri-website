<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property $template
 * @property $uri
 */
class Event_Controller extends Public_Controller
{
	protected $folder_path;

	public function __construct()
	{
		parent::__construct();

		$this->data['event_date'] = [
			'icb-rev_2022' => date('Y-m-d H:i:s', strtotime('2022-06-21 00:00:00')),
			'ibs_2022' => date('Y-m-d H:i:s', strtotime('2022-05-24 00:00:00')),
			'nbri-yic_2022' => date('Y-m-d H:i:s', strtotime('2022-06-21 00:00:00')),
			'isrus_2022' => date('Y-m-d H:i:s', strtotime('2022-08-02 00:00:00')),
		];

		$this->data['active_event'] = [
			'event_name' => 'The International Conference on  Battery <br>for Renewable Energy and Electric Vehicles 2022',
			'event_date' => $this->data['event_date']['icb-rev_2022'],
		];

		$this->data['important_dates'] = [
			'icb-rev-2021' => [
				[
					'name' => 'Abstract Submission Deadline',
					'value' => 'June 12, 2021',
					'short_value' => [
						12, 'June'
					]
				],
				[
					'name' => 'Early Bird Registration Deadline',
					'value' => 'June 14, 2021',
					'short_value' => [
						14, 'June'
					]
				],
				[
					'name' => 'Abstract Acceptance Notification',
					'value' => 'June 15,2021',
					'short_value' => [
						15, 'June'
					]
				],
				[
					'name' => 'Full Paper Submission Deadline',
					'value' => 'June 24, 2021',
					'short_value' => [
						24, 'June'
					]
				],
				[
					'name' => 'Conference Date',
					'value' => 'June 22 - 24, 2021',
					'short_value' => [
						'22 - 24', 'June'
					]
				]
			],
			'icb-rev-2022' => [
				[
					'name' => 'Abstract Submission Deadline',
					'value' => '16 June 2022',
					'short_value' => [
						'16', 'June'
					]
				],
				[
					'name' => 'Abstract Acceptance Notification',
					'value' => '18 June 2022',
					'short_value' => [
						'18', 'June'
					]
				],
				[
					'name' => 'Early Bird Registration Deadline',
					'value' => '16 June 2022',
					'short_value' => [
						'16', 'June'
					]
				],
				[
					'name' => 'Full Paper Submission Deadline',
					'value' => '20 June 2022',
					'short_value' => [
						'20', 'June'
					]
				],
				[
					'name' => 'Conference Date',
					'value' => '21-23 June 2022',
					'short_value' => [
						'21 - 23', 'June'
					]
				]
			]
		];

		$this->data['early_bird_date'] = strtotime('2022-06-10 00:00:00');

		$this->template->set_partial('banner_hero', 'partials/event_partial/banner_hero', $this->data);
		$this->template->set_partial('navbar', 'partials/event_partial/' . $this->uri->segment(2) . '/navbar', $this->data);
		$this->template->set_partial('sponsors', 'partials/event_partial/' . $this->uri->segment(2) . '/sponsors', $this->data);
		//$this->template->set_partial('email_dropbox', 'partials/event_partial/' . $this->uri->segment(2) . '/email_dropbox', $this->data);
		$this->template->set_partial('footer', 'partials/event_partial/' . $this->uri->segment(2) . '/footer_event', $this->data);
		$this->template->set_layout('default_template_event');
		$this->folder_path = $this->uri->segment(2) . '/';
	}

	/**
	 * @param String $pagename
	 *
	 * @return void
	 */
	protected function _build(string $pagename): void
	{
		if (!isset($pagename)) {
			show_404();
		}

		$this->template->build($this->folder_path . $pagename, $this->data);
	}

	protected function _build_plain(string $pagename): void
	{
		if (!isset($pagename)) {
			show_404();
		}

		$this->template->set_layout('default_template_plain');
		$this->template->build($this->folder_path . $pagename, $this->data);
	}

	protected function get_rates($amount, $currency_from = 'USD', $currency_dest = 'IDR')
	{
		$url = 'https://openexchangerates.org/api/latest.json?app_id=15ee3ba79e1e41fbbfcd0818004560ec&base=' . $currency_from;
		$data = file_get_contents($url);

		$rates = json_decode($data, FALSE);

		if (!empty($rates)) {
			$rate = ceil($rates->rates->{$currency_dest});
			$total = $amount * $rate;
			return array(
				'disclaimer' => $rates->disclaimer,
				'license' => $rates->license,
				'usd_to_idr' => $rate,
				'usd_to_idr_total' => $total,
				'timestamp' => $rates->timestamp
			);
		}

		return FALSE;
	}
}
