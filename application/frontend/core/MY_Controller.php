<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {
	protected $data;

	public function __construct()
	{
		parent::__construct();

		if(!array_key_exists('website_var', $_SESSION)){
			$session_data = set_website_variables(); // get website setting from json file, put setting value into container
			$this->session->set_userdata( $session_data ); // put setting value into session
		} else {
			if(is_null($_SESSION['website_var'])){
				$session_data = set_website_variables(); // get website setting from json file, put setting value into container
				$this->session->set_userdata( $session_data ); // put setting value into session
			}
		}
		$this->data['website_var'] = $_SESSION['website_var'];

		$this->data['current_segment'] = $this->uri->segment(1);
        $this->load->model('site_m');

        $this->data['menu']['research'] = $this->site_m->get_research_menu();

		// $this->template->set_partial('logo','partials/logo');

		// $this->template->set_partial('global_js','partials/global_js');
		// $this->template->set_partial('scripts','partials/scripts');
        $this->template->set_partial('topbar','partials/topbar');
		$this->template->set_partial('navbar','partials/navbar', $this->data);
		$this->template->set_partial('header','partials/header');
		$this->template->set_partial('slider','partials/slider',$this->data);
		$this->template->set_partial('footer','partials/footer');

		$this->template->set_theme('default');
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/frontend/core/MY_Controller.php */