<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	public function is_active($field,$value,$table_name){
		$this->db->where('status', 1);
		$this->db->where($field, $value);
		return ($this->db->count_all_results($table_name) > 0) ? TRUE : FALSE;
	}

	public function is_inactive($field,$value,$table_name){
		$this->db->where('status', 0);
		$this->db->where($field, $value);
		return ($this->db->count_all_results($table_name) > 0) ? TRUE : FALSE;
	}

	public function get_lens_option(){
		$this->db->select('id,name,slug,price');
		$this->db->from('lens_option');
		$this->db->where('status', 1);

		return $this->db->get()->result_array();
	}

	public function get_brands()
	{
		$this->db->select('brand_name, slug');
		$this->db->from('brands');
		$this->db->where('status', 1);

		return $this->db->get()->result_array();
	}

	public function send_email(Array $param = []){
        $message = (isset($param['message'])) ? $param['message']:'';
        $destination = (isset($param['destination'])) ? $param['destination']:'admin@n-bri.org';
        $subject = (isset($param['subject'])) ? $param['subject']:'Greetings from NBRI';

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'smtp_user'=> ICB-REV2021_EMAIL,
            'smtp_pass'=> ICB-REV2021_EMAILPASS,
            'mailtype' => 'html',
            'charset' => 'utf8',
            'priority' => 2,
            'wordwrap' => TRUE,
            'newline' => "\r\n"
        );

        $this->load->library('email', $config);
        $this->email->from(ICB-REV2021_EMAIL, 'ICB-REV 2021 NBRI');
        $this->email->to($destination);
        $this->email->bcc(ADMIN-NBRI_EMAIL);
        $this->email->subject($subject);
        $this->email->message($message);

        return $this->email->send();
    }

}

/* End of file MY_Model.php */
/* Location: ./application/frontend/core/MY_Model.php */