<?php

class Main_event_controller extends Event_Controller
{
	public const CLOSED_EVENT = [
		'icb-rev-2021'
	];

	public const CLOSED_METHOD = [
		// 'registration'
	];

	public function __construct()
	{
		parent::__construct();

		if (in_array($this->uri->segment(2), static::CLOSED_EVENT)) {
			show_404();
		}
		if (in_array($this->uri->segment(3), static::CLOSED_METHOD)) {
			echo "This event is still locked. Contact admin for more info.";
			echo "<br /><br />";
			echo "<a href='javascript:history.go(-1)'><< Go Back</a>";
			die;
		}

		$this->data['event_details'] = [
			'icb-rev-2022' => [
				'icb-rev_2022' => [
					'description' => 'International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2022',
					'package_detail' => [
						'international' => [
							'student' => [
								'presenter' => [
									'price' => [
										'early_bird' => [
											'amount' => 250,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-06-16 23:59:59'))
										],
										'normal' => [
											'amount' => 300,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Participate all conference programs',
										'Oral presentation',
										'International certificate',
										'International publication'
									]
								],
								'participant' => [
									'price' => [
										'early_bird' => [
											'amount' => 50,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-06-16 23:59:59'))
										],
										'normal' => [
											'amount' => 100,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Attend all conference programs',
										'International certificate'
									]
								],
							],
							'regular' => [
								'presenter' => [
									'price' => [
										'early_bird' => [
											'amount' => 350,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-06-16 23:59:59'))
										],
										'normal' => [
											'amount' => 450,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Participate all conference programs',
										'Oral presentation',
										'International certificate',
										'International publication'
									]

								],
								'participant' => [
									'price' => [
										'early_bird' => [
											'amount' => 100,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-06-16 23:59:59'))
										],
										'normal' => [
											'amount' => 150,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Attend all conference programs',
										'International certificate'
									]

								],
							],
						],
						'indonesian' => [
							'student' => [
								'presenter' => [
									'price' => [
										'early_bird' => [
											'amount' => 2500000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-06-16 23:59:59'))
										],
										'normal' => [
											'amount' => 3000000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Participate all conference programs',
										'Oral presentation',
										'International certificate',
										'International publication'
									]

								],
								'participant' => [
									'price' => [
										'early_bird' => [
											'amount' => 500000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-06-16 23:59:59'))
										],
										'normal' => [
											'amount' => 1000000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Attend all conference programs',
										'International certificate'
									]

								],
							],
							'regular' => [
								'presenter' => [
									'price' => [
										'early_bird' => [
											'amount' => 3500000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-06-16 23:59:59'))
										],
										'normal' => [
											'amount' => 4500000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Participate all conference programs',
										'Oral presentation',
										'International certificate',
										'International publication'
									]

								],
								'participant' => [
									'price' => [
										'early_bird' => [
											'amount' => 1000000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-06-16 23:59:59'))
										],
										'normal' => [
											'amount' => 1500000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Attend all conference programs',
										'International certificate'
									]
								],
							],
						]
					],
					'event_detail' => [
						'scopes' => [
							'ADVANCED BATTERY TECHNOLOGY FROM UPSTREAM (RAW MATERIALS), MIDSTREAM (CELL FABRICATION), AND DOWNSTREAM (APPLICATIONS AND RECYCLING).',
							'ENERGY STORAGE TECHNOLOGY FOR RENEWABLE ENERGIES (SOLAR, WIND, BIOMASS, GEOTHERMAL, ETC)',
							'ELECTRIC VEHICLES ECOSYSTEM (BATTERY, ELECTRIC MOTOR, CHARGING STATION, ETC)',
							'OTHER RELATED TOPICS (POLICY, REGULATION, STANDARDIZATION, INDUSTRY, ETC)',
						]
					],
					'event_date' => [
						'start_date' => date('Y-m-d H:i:s', strtotime('2022-06-21 00:00:00')),
						'end_date' => date('Y-m-d H:i:s', strtotime('2022-06-21 00:00:00'))
					],
					'speakers' => [
						'homepage' => [
							'title' => 'Homepage Speakers',
							'content' => [
								[
									'slug' => 'toto_nugraha_pranatyasto_homepage',
									'name' => 'Toto Nugroho Pranatyasto',
									'subtitle' => 'President Director of Indonesia Battery Corporation, Indonesia',
									'pic' => base_url('assets/images/speakers/toto_nugraha_pranatyasto_homepage.png'),
									'description' => 'Toto Nugroho Pranatyasto is a President Director of Indonesia Battery Corporation (IBC) Indonesia. He is former manager for New & Renewable Energy Business Development at Pertamina. He received bachelor science of chemical engineering from University of Indonesia (1986 - 1991). Then, he continued to The University of Texas Austin for Master on Chemical Engineering. Mr. Toto also enrolled in Leadership Executive Program, Business Administration and Management by INSEAD (2013- 2014).'
								],
								[
									'slug' => 'rene_schroeder_homepage',
									'name' => 'Rene Schroeder',
									'subtitle' => 'Executive Director of European Automotive and Industrial Battery Manufacturers (EUROBAT), Belgium',
									'pic' => base_url('assets/images/speakers/rene_schroeder_homepage.png'),
									'description' => 'Rene Schroeder is an Executive Director of European Automotive and Industrial Battery Manufacturers (EUROBAT), Belgium since 2017. EUROBAT is the association for the European manufacturers of automotive, industrial and energy storage batteries. EUROBAT has more than 50 members from across the continent comprising more than 90% of the automotive and industrial battery industry in Europe. He received his bachelor degree in University of Nebraska at Kearney (2001), then Master degree on History/Political Science from Universität Rostock (1997-2002) and Master degree on European Studies/Civilization from College of Europe in Natolin (2003-2004).'
								],
								[
									'slug' => 'prof_rodrigo_martins_homepage',
									'name' => 'Prof. Rodrigo Martins',
									'subtitle' => 'President of IUMRS and European Academy of Science (EurASc), Portugal',
									'pic' => base_url('assets/images/speakers/prof_rodrigo_martins_homepage.png'),
									'description' => 'Prof. Rodrigo Martins is full professor in Materials Science Department of Faculty of Science and Technology of New University of Lisbon, a fellow of the Portuguese Engineering Academy since 2009 and a member of the European Academy of Science since 2016. He was decorated with the gold medal of merit and distinction by the Almada Municipality for his R&D achievements. Prof. Rodrigo has been involved in the pioneer European research on Amorphous silicon semiconductors and pioneer with is group worldwide activity related to passive and active oxides, the so called transparent electronics and it is one of the inventors of the so-called paper electronics, where paper is exploit not only as asubstrate but also as a functional component in active devices. He published over 700 papers and during the last 10 years got more than 14 international and national prizes and distinctions for his work (e.g. Lisbon Energy Live Expo, Innovation Prize, 2012 Solar Tiles, European Patent Office Innovation nomination 2016, etc.'
								],
								[
									'slug' => 'prof__neeraj_sharma_homepage',
									'name' => 'Prof. Neeraj Sharma',
									'subtitle' => 'Director of Australian Battery Society (ABS), Australia',
									'pic' => base_url('assets/images/speakers/prof__neeraj_sharma_homepage.png'),
									'description' => "Professor Neeraj Sharma is a Director of Australian Battery Society (ABS), Australia. He also Associate Professor of University of New South Wales (UNSW), Australia. Neeraj completed his Ph.D. at the University of Sydney then moved to the Bragg Institute at Australian Nuclear Science and Technology Organisation (ANSTO) for a postdoc. He started at the School of Chemistry, UNSW on a Australian Institute of Nuclear Science and Engineering (AINSE) Research Fellowship followed by an Australian Research Council (ARC) Discovery Early Career Research Award (DECRA). He is currently an Associate Professor and ARC Future Fellow. Neeraj has been the Royal Australian Chemical Institute (RACI) Nyholm Youth Lecturer (2013/2014) and has won the NSW Premier's Prize for Science and Engineering (Early Career Researcher in Physical Sciences, 2019), Australian Synchrotron Research Award (2018), RACI Rennie Memorial Medal for Chemical Science (2018), UNSW Postgraduate Supervisor Award (2017) and a NSW Young Tall Poppy Award (2014). Neeraj has over 165 publications and has been invited to present his work at over 30 conferences. Neeraj’s research interests are based on solid state chemistry, designing new materials and investigating their structure-property relationships. He loves to undertake in situ or operando experiments of materials inside full devices, especially batteries, in order to elucidate the structural subtleties that lead to superior performance parameters. Neeraj’s projects are typically highly collaborative working with colleagues from all over the world with a range of skill sets."
								],
								// [
								// 	'slug' => 'prof_bambang_permadi_soemantri_brodjonegoro',
								// 	'name' => 'Prof. Bambang Permadi Soemantri Brodjonegoro',
								// 	'subtitle' => 'Co-Chair T20 of G20 Indonesia Presidency and Former Minister of Research and Technology/National Research and Innovation Agency of Republic of Indonesia',
								// 	'pic' => base_url('assets/images/speakers/prof__bambang_brodjonegoro.jpg'),
								// 	'description' => "Prof. Bambang Permadi Soemantri Brodjonegoro is a Co-Chair T20 of G20 Indonesia Presidency. He was the Minister of Research and Technology/National Research and Innovation Agency of Republic of Indonesia. Previously, he was the Minister of National Development Planning of Indonesia, taking office after a cabinet reshuffle by President Joko Widodo. From 27 October 2014 to 27 July 2016, he was the Finance Minister of Indonesia in the Working Cabinet also under President Joko Widodo's administration. Under Susilo Bambang Yudhoyono's administration, he was the Deputy of Finance Minister in the Second United Indonesia Cabinet. Professor Bambang Brodjonegoro completed his study at Economics Faculty of Universitas Indonesia in 1990. The next year, Bambang studied for a master's degree at the University of Illinois at Urbana–Champaign, United States, continuing on to doctoral program completed in 1997. On 3 July 2021, Bandung Institute of Technology (ITB) awarded honorary degree on occasion of 101 years anniversary event to Bambang Brodjonegoro. He was one of three people awarded by ITB in auspicious occasion. He awarded with doctor honoris causa degree in Urban Planning."
								// ],
								[
									'slug' => 'prof__jacqui_murray_homepage',
									'name' => 'Prof. Jacqui Murray',
									'subtitle' => 'Deputy Director of Faraday Battery Challenge Innovate UK, United Kingdom',
									'pic' => base_url('assets/images/speakers/prof__jacqui_murray_homepage.png'),
									'description' => 'Prof. Jacqui Murray is a Deputy Director of Faraday Battery Challenge Innovate UK, United Kingdom. As Deputy Director of the £318m million Faraday Battery Challenge Jacqui helps lead government investment to develop batteries that are cost-effective, long range, fast charging, durable, safe and recyclable. Her background in the steel industry, environmental regulation and advanced materials innovation provides industrial relevance in her role as a Visiting Professor (Royal Academy of Engineering) at the University of Leicester, where she enhances the engineering program. A keen STEM ambassador, Jacqui takes an active leadership approach on diversity and inclusion throughout her work. Named as one of Autocar’s Top 100 Women in Automotive based on Seniority and Influence in 2020.'
								],
								[
									'slug' => 'dr__laksana_tri_handoko_homepage',
									'name' => 'Dr. Laksana Tri Handoko',
									'subtitle' => 'Chairman of National Research and Innovation Agency (BRIN), Indonesia',
									'pic' => base_url('assets/images/speakers/dr__laksana_tri_handoko_homepage.png'),
									'description' => 'Dr. Laksana Tri Handoko is an Indonesian scientist and public official specializing in theoretical and particle physics. He formerly served as the deputy head of science and technology for the Indonesian Institute of Sciences from 2014 to 2018. And he became Chairman of the institute start from 2018. He appointed as the second (but first independent) holder of Head of National Research and Innovation Agency.'
								],
								[
									'slug' => 'danny_kennedy_homepage',
									'name' => 'Danny Kennedy',
									'subtitle' => 'Chief Energy Officer of New Energy Nexus, United States',
									'pic' => base_url('assets/images/speakers/danny_kennedy_homepage.png'),
									'description' => "Danny Kennedy is the Chief Energy Officer for New Energy Nexus, an international organization that supports clean energy entrepreneurs with funds, accelerators and networks. Launched in California, New Energy Nexus now operates programs in China, India, Southeast Asia, and East Africa. Leading up to his role at New Energy Nexus, Danny worked at the California Clean Energy Fund, connecting entrepreneurs everywhere to capital to build an abundant clean energy economy that benefits all. In 2016, CalCEF launched New Energy Nexus. Kennedy is also the  President of CalCharge, a public-private partnership with DoE National Labs and universities in California, unions and companies, working to advance energy storage. Kennedy co-founded Sungevity in 2007, the company that created remote solar design, and Powerhouse, a smart energy incubator and accelerator. He was the first backer of Mosaic in 2011,  the $2B solar loan provider, and remains on the Board of Powerhive, a  solar mini-utility in Kenya and Sunergise, a solar-as-a-service business out of Fiji and EnergyLabAustralia. He is also a Director of the organizations VoteSolar, Power for All and Confluence Philanthropy and  adviser to SolarPhilippines. Kennedy authored the book Rooftop Revolution: How Solar Power Can Save Our Economy–and Our Planet–from  Dirty Energy in 2012. Prior to being an entrepreneur, he worked at  Greenpeace and other NGOs on climate and energy issues for 20+ years."
								],
								// [
								// 	'slug' => 'dr_agus_gumiwang_kartasasmita',
								// 	'name' => 'Dr. Agus Gumiwang Kartasasmita',
								// 	'subtitle' => 'Minister of Industry Republic of Indonesia, Indonesia',
								// 	'pic' => base_url('assets/images/speakers/dr__agus_gumiwang_kartasasmita.png'),
								// 	'description' => 'Dr. Agus Gumiwang Kartasasmita is an Indonesian politician. He is the current Minister of Industry in the Republic of Indonesia, appointed on 24 August 2019. He attended Canisius High School in 1984 and left in 1985 to Knox High School in New York. In 1991, he went to Pacific Western University where he studied Commercial Science and graduated in 1994 with a BSc in Commercial science. In 2007, he enrolled into University of Pasundan where he graduated with Master of Public Administration in 2009, and obtained a PhD in Administration in 2014. On October 23 2019, president elect Jokowi announced that Agus Gumiwang would make a return to his Indonesia Onward Cabinet as Minister of Industry replacing Gumiwang\'s fellow Golkar Party politician, Airlangga Hartarto.'
								// ],
							],
							'mentioned_in_speakers_page' => false
						],
						'opening_remarks' => [
							'title' => 'Opening Remarks',
							'content' => [
								[
									'slug' => 'prof__evvy_kartini_opening_remarks',
									'name' => 'Prof. Dr. rer. nat. Evvy Kartini',
									'subtitle' => 'Founder of National Battery Research Institute and President of MRS-INA',
									'pic' => base_url('assets/images/speakers/prof__evvy_kartini_opening_remarks.png'),
									'description' => 'Evvy Kartini holds the title of Prof. Dr. rer. nat. She is an expert on the neutron scattering and respected internationally. Her international reputation in the field of neutron scattering and solid state ionics, has been well established. She has been represented as a leader (president) of the Indonesian Neutron Scattering Society (INSS) since 2013. She is one of the executive committee of the Asian Oceania Neutron Scattering Soceity (AONSA). Besides neutron scattering, Evvy Kartini has expertise on the materials science, especially on lithium ion battery research. Evvy Kartini, has became one of the International Board Member of the Asian Socety of Solid State Ionics and International Society of Solid State Ionics. Recently, Evvy Kartini has attended the 20th International Conference on Solid State Ionics (ICSSI) in Keystone, Colorado, USA 2015. She was there not only for presenting her research work on battery researchers, but also represented Asia-Australia region as councilor of the International Society of Solid State Ionics. She was one of the International Advisory Board, and also chaired the session of Research on Battery Materials. She also presented her recent topic on development of new lithium solid electrolyte, which is explored by the inelastic spectrometer instrument at Japan Proton Accelerator Research Compex (J-PARC), the foremost leading spallation neutron source in the world.'
								],
								[
									'slug' => 'prof__bvr_chowdari_opening_remarks',
									'name' => 'Prof. B.V.R. Chowdari',
									'subtitle' => 'Head Officer of IUMRS, Singapore',
									'pic' => base_url('assets/images/speakers/prof__bvr_chowdari_opening_remarks.png'),
									'description' => 'Prof. Chowdary is a Head officer of International Union of Material Research Society (IUMRS), Singapore. Prof. Chowdari has established research laboratory, first of its kind in NUS and the region, for carrying out research on synthesis and characterisation of materials for potential applications in solid state batteries. The laboratory is instrumental in developing several research groups in Singapore and the region. His current research interest lie in the area of materials science, specialising in the synthesis of materials using different methods, characterising them by variety of techniques, and finding applications in energy storage devices more specifically Lithium ion batteries.'
								],
								[
									'slug' => 'dr__laksana_tri_handoko_opening_remarks',
									'name' => 'Dr. Laksana Tri Handoko',
									'subtitle' => 'Chairman of National Research and Innovation Agency (BRIN), Indonesia',
									'pic' => base_url('assets/images/speakers/dr__laksana_tri_handoko_opening_remarks.png'),
									'description' => 'Dr. Laksana Tri Handoko is an Indonesian scientist and public official specializing in theoretical and particle physics. He formerly served as the deputy head of science and technology for the Indonesian Institute of Sciences from 2014 to 2018. And he became Chairman of the institute start from 2018. He appointed as the second (but first independent) holder of Head of National Research and Innovation Agency.'
								],
								[
									'slug' => 'prof__colin_grant_opening_remarks',
									'name' => 'Prof. Colin Grant',
									'subtitle' => 'Vice Principal (International) of Queen Mary University of London',
									'pic' => base_url('assets/images/speakers/prof__colin_grant_opening_remarks.png'),
									'description' => 'Professor Colin Grant is a Vice Principal (International) of Queen Mary University of London (QMUL), United Kingdom. He studied modern languages, international organisations, literature and European Studies in Edinburgh, Nantes, Leipzig, Bath and Berlin and had two DAAD postdoctoral fellowships at the University of Siegen (Germany). Professor Colin was an exchange student at the universities of Leipzig and Nantes and a visiting PhD student at the Humboldt/Zentralinstitut fuer Literaturgeschichte in Berlin. He was Head of Department, Head of School, Dean and inaugural Pro-Vice-Chancellor for International Relations at the University of Surrey and inaugural PVC (International) at the University of Bath and inaugural Vice President (International) at the University of Southampton. Professor Colin was Visiting Professor at the Federal University of Rio de Janeiro and at Paris 4. He has taught across many different areas at all levels: European Studies, international organisations, German and French literature, social philosophy, translation studies, media and communication theory. Professor Colin’s PhD students and postdoctoral fellows have worked on various aspects of political communication, mental health and environmental communication, the public sphere and uncertainty and dialogism, media and communication, Islamophobia and communication philosophy.'
								],
							],
							'mentioned_in_speakers_page' => true
						],
						'plenary_speakers' => [
							'title' => 'Plenary Speakers',
							'content' => [
								[
									'slug' => 'toto_nugraha_pranatyasto',
									'name' => 'Toto Nugroho Pranatyasto',
									'subtitle' => 'President Director of Indonesia Battery Corporation, Indonesia',
									'pic' => base_url('assets/images/speakers/toto_nugraha_pranatyasto.png'),
									'description' => 'Toto Nugroho Pranatyasto is a President Director of Indonesia Battery Corporation (IBC) Indonesia. He is former manager for New & Renewable Energy Business Development at Pertamina. He received bachelor science of chemical engineering from University of Indonesia (1986 - 1991). Then, he continued to The University of Texas Austin for Master on Chemical Engineering. Mr. Toto also enrolled in Leadership Executive Program, Business Administration and Management by INSEAD (2013- 2014).'
								],
								[
									'slug' => 'rene_schroeder',
									'name' => 'Rene Schroeder',
									'subtitle' => 'Executive Director of European Automotive and Industrial Battery Manufacturers (EUROBAT), Belgium',
									'pic' => base_url('assets/images/speakers/rene_schroeder.png'),
									'description' => 'Rene Schroeder is an Executive Director of European Automotive and Industrial Battery Manufacturers (EUROBAT), Belgium since 2017. EUROBAT is the association for the European manufacturers of automotive, industrial and energy storage batteries. EUROBAT has more than 50 members from across the continent comprising more than 90% of the automotive and industrial battery industry in Europe. He received his bachelor degree in University of Nebraska at Kearney (2001), then Master degree on History/Political Science from Universität Rostock (1997-2002) and Master degree on European Studies/Civilization from College of Europe in Natolin (2003-2004).'
								],
								[
									'slug' => 'prof__satish_patil',
									'name' => 'Prof. Satish Patil',
									'subtitle' => 'Professor of Solid State and Structural Chemistry Unit Indian Institute of Science, India',
									'pic' => base_url('assets/images/speakers/prof__satish_patil.png'),
									'description' => 'Prof. Satish Patil is a Professor of Solid State and Structural Chemistry Unit Indian Institute of Science, India. He obtained his Ph.D of polymer chemistry from Bergische University of Wuppertal, Germany. Professor Patil has a research interest in synthesis of organic semiconductors for singlet fission, thermally allowed delayed emission, organic solar cell, and organic electrochemical transistors. He already published more than 100 papers in peer reviewed journals.'
								],
								[
									'slug' => 'prof_rodrigo_martins',
									'name' => 'Prof. Rodrigo Martins',
									'subtitle' => 'President of IUMRS and European Academy of Science (EurASc), Portugal',
									'pic' => base_url('assets/images/speakers/prof__rodrigo_martins.png'),
									'description' => 'Prof. Rodrigo Martins is full professor in Materials Science Department of Faculty of Science and Technology of New University of Lisbon, a fellow of the Portuguese Engineering Academy since 2009 and a member of the European Academy of Science since 2016. He was decorated with the gold medal of merit and distinction by the Almada Municipality for his R&D achievements. Prof. Rodrigo has been involved in the pioneer European research on Amorphous silicon semiconductors and pioneer with is group worldwide activity related to passive and active oxides, the so called transparent electronics and it is one of the inventors of the so-called paper electronics, where paper is exploit not only as asubstrate but also as a functional component in active devices. He published over 700 papers and during the last 10 years got more than 14 international and national prizes and distinctions for his work (e.g. Lisbon Energy Live Expo, Innovation Prize, 2012 Solar Tiles, European Patent Office Innovation nomination 2016, etc.'
								],
								[
									'slug' => 'prof__roberto_m__torresi',
									'name' => 'Prof. Roberto M. Torresi',
									'subtitle' => 'Professor at the Fundamental Chemistry Department Institute of Chemistry - University of São Paulo',
									'pic' => base_url('assets/images/speakers/prof__roberto_m__torresi.png'),
									'description' => 'Professor Roberto M. Torresi is a Professor at the Fundamental Chemistry Department Institute of Chemistry - University of São Paulo, Brazil. Professor Roberto is also an associate editor of the Journal of the Brazilian Chemical Society. He obtained his Bachelor in Chemical Physics (1980) and PhD in Chemical Sciences (1986) from the National University of Córdoba, Córdoba, Argentina. He did his postdoctoral work at Pierre et Marie Curie University, Paris, France between 1988 and 1990 working with Claude Gabrielli and Michel Keddam. Between 1990 and 1993 he was a visiting professor at the Physics Institute of UNICAMP, Campinas, and from 1994 onwards he became a professor at the São Carlos Institute of Chemistry (USP), São Carlos, where he remained until 2002. He has broad experience in Chemistry, with emphasis on Electrochemistry, working mainly on the following subjects: ionic liquids at room temperature, quartz crystal microbalance, modified electrodes with inorganic, organic or hybrid materials, lithium-ion batteries and electro-intercalation.'
								],
								[
									'slug' => 'prof_neeraj_sharma',
									'name' => 'Prof. Neeraj Sharma',
									'subtitle' => 'Director of Australian Battery Society (ABS), Australia',
									'pic' => base_url('assets/images/speakers/prof__neeraj_sharma.png'),
									'description' => "Professor Neeraj Sharma is a Director of Australian Battery Society (ABS), Australia. He also Associate Professor of University of New South Wales (UNSW), Australia. Neeraj completed his Ph.D. at the University of Sydney then moved to the Bragg Institute at Australian Nuclear Science and Technology Organisation (ANSTO) for a postdoc. He started at the School of Chemistry, UNSW on a Australian Institute of Nuclear Science and Engineering (AINSE) Research Fellowship followed by an Australian Research Council (ARC) Discovery Early Career Research Award (DECRA). He is currently an Associate Professor and ARC Future Fellow. Neeraj has been the Royal Australian Chemical Institute (RACI) Nyholm Youth Lecturer (2013/2014) and has won the NSW Premier's Prize for Science and Engineering (Early Career Researcher in Physical Sciences, 2019), Australian Synchrotron Research Award (2018), RACI Rennie Memorial Medal for Chemical Science (2018), UNSW Postgraduate Supervisor Award (2017) and a NSW Young Tall Poppy Award (2014). Neeraj has over 165 publications and has been invited to present his work at over 30 conferences. Neeraj’s research interests are based on solid state chemistry, designing new materials and investigating their structure-property relationships. He loves to undertake in situ or operando experiments of materials inside full devices, especially batteries, in order to elucidate the structural subtleties that lead to superior performance parameters. Neeraj’s projects are typically highly collaborative working with colleagues from all over the world with a range of skill sets."
								],
								[
									'slug' => 'prof_jacqui_murray',
									'name' => 'Prof. Jacqui Murray',
									'subtitle' => 'Deputy Director of Faraday Battery Challenge Innovate UK, United Kingdom',
									'pic' => base_url('assets/images/speakers/prof__jacqui_murray.png'),
									'description' => 'Prof. Jacqui Murray is a Deputy Director of Faraday Battery Challenge Innovate UK, United Kingdom. As Deputy Director of the £318m million Faraday Battery Challenge Jacqui helps lead government investment to develop batteries that are cost-effective, long range, fast charging, durable, safe and recyclable. Her background in the steel industry, environmental regulation and advanced materials innovation provides industrial relevance in her role as a Visiting Professor (Royal Academy of Engineering) at the University of Leicester, where she enhances the engineering program. A keen STEM ambassador, Jacqui takes an active leadership approach on diversity and inclusion throughout her work. Named as one of Autocar’s Top 100 Women in Automotive based on Seniority and Influence in 2020.'
								],
								[
									'slug' => 'prof_alan_j_drew',
									'name' => 'Prof. Alan J Drew',
									'subtitle' => 'Co-founder of NBRI and Director of the Materials Research Institute, Queen Mary University of London (QMUL)',
									'pic' => base_url('assets/images/prof_alan_j_drew_1.png'),
									'description' => 'Prof. Alan Drew was appointed Leverhulme Fellow in the Centre of Condensed Matter & Materials Physics in 2008. He was rapidly promoted to Senior Lecturer (2011) and then Reader (2012), and in 2018 was promoted to Professor. Prof. Drew has been awarded a number of prestigious fellowships and awards over his career, starting with a Fellowship of the Royal Commission of the Exhibition of 1851 (2014), Leverhulme Fellow (2008), European Research Council (ERC) fellow (2012), Talent 1000 Scholar of the Chinese Ministry of Education (2014) and Changjiang Distinguished Professor at Sichuan University (2015). He is currently Head of the CCMMP Research Centre, and Director of the Materials Research Institute. His main research interests are using spin sensitive and structural probes situated at central facilities to characterize and understand the fundamental properties of materials, backed up with laboratory-based techniques (e.g. magnetic, structural and electrical characterization, thin film growth, Raman/IR spectroscopy, electro/photo luminescence). His main interests are understanding spin and charge carrier dynamics in organic and biological materials, the properties of materials with novel quantum mechanical states, structure function relationship in biomass derived conductors.'
								],
								[
									'slug' => 'dr__m__v__reddy',
									'name' => 'Dr. M.V. Reddy',
									'subtitle' => 'Senior Researcher of Institute of Research Hydro-Québec, Center of Excellence in Energy Storage and Transportation Electrification, Montreal, Canada',
									'pic' => base_url('assets/images/speakers/dr__m__v__reddy.png'),
									'description' => 'Dr. M.V. Reddy obtained his Ph. D (2003) in the area of  Materials Science and Engineering (with highest distinction) from the University of Bordeaux, France. From July 2003 to May 2019, he worked at the Department of Materials Science and Engineering, Chemistry and Physics, National University of Singapore (NUS). Singapore.  From June 2019 to Aug 2021, he worked at the Center of Excellence in Transportation Electrification and Energy Storage, Hydro-Québec, Canada. Currently working as a Senior Professional Researcher and group leader in Energy storage Technology at Nouveau Monde Graphite (New graphite world) (NMG), Quebec, Montreal, Canada. Over the past 21 years, he has conducted leading research on Materials for Energy Storage (cathodes, anodes, supercapacitors and electrolytes), Materials processing & characterization, and the development of in situ techniques for Energy storage renewable technologies. Dr. Reddy has published 220 papers in various international journals.  He has obtained an h-index of 68 with over 17200 citations. These have recently placed him within the top 2% highly cited researchers in Energy (world Ranking the 1002nd out of 186500 researchers) and Highly cited Researcher in Materials Science in Canada (National ranking:39) Dr. Reddy is serving as an editorial advisory board member in Materials Research Bulletin and Journal of Energy Storage (Elsevier, Scopus journal) as well as several open access journals Awards: Outstanding Science Mentorship Award (2010- 2018), and Inspiring Research Mentor Award (2011 to 2019), 2021 Battery Materials electrochemistry award from the Electrochemical Society of India, Indian Institute of Science (IISc), Bangalore, India. He was also Invited life member in ICDD USA and given 150 talks at various conferences and workshops & FDP. Dr. Reddy also became Invited committee member in various international Research proposals, theses and conference organizations and visiting Professor at various Universities.'
								],
								[
									'slug' => 'dr__jong_yeon_kim',
									'name' => 'Dr. Jong Yeon Kim',
									'subtitle' => 'CEO of TAOS Company, South Korea',
									'pic' => base_url('assets/images/speakers/dr__jong_yeon_kim.png'),
									'description' => 'Dr. Jong Yeon Kim is a CEO of TAOS Company, South Korea. TAOS Co., Ltd – Korea provides services that prioritize customer value in the fields of IoT, Cloud, Big Data and Mobile in preparation for the fourth industrial revolution era. In addition, Taos offers connected car services based on cloud platform technology. Dr. Kim earned his bachelor\'s degree in Plant Genetic Engineering, Catholic University of Daegu in 2005. He continued his Master Degree in Cohesion Materials Physics, Daegu Catholic University and graduated in 2008. Then, Dr. Kim completed his Ph.D. in Materials Science and Chemical Engineering, Daegu Catholic University in 2018. Dr. Kim has various expertise in many fields in particular Electric Vehicles, such as Signal Detection and Analysis Device of Ultrasonic Sensor for Vehicle, Vehicle engine sound generation system, Monitorable secondary battery pack system for electric vehicles, Electric car care system using battery status information, Automobile collision warning system linked with user portable terminal, Electric car battery monitoring system firmware version_2.0, Electric Vehicle Battery Monitoring System (Hybrid_Web), Sensor interlocking firmware using gyro sensor and servo motor, to Monitoring device and method for managing the getting on and off of a vehicle.'
								],
								[
									'slug' => 'dr_agus_gumiwang_kartasasmita',
									'name' => 'Dr. Agus Gumiwang Kartasasmita',
									'subtitle' => 'Minister of Industry Republic of Indonesia, Indonesia',
									'pic' => base_url('assets/images/speakers/dr__agus_gumiwang_kartasasmita.png'),
									'description' => 'Dr. Agus Gumiwang Kartasasmita is an Indonesian politician. He is the current Minister of Industry in the Republic of Indonesia, appointed on 24 August 2019. He attended Canisius High School in 1984 and left in 1985 to Knox High School in New York. In 1991, he went to Pacific Western University where he studied Commercial Science and graduated in 1994 with a BSc in Commercial science. In 2007, he enrolled into University of Pasundan where he graduated with Master of Public Administration in 2009, and obtained a PhD in Administration in 2014. On October 23 2019, president elect Jokowi announced that Agus Gumiwang would make a return to his Indonesia Onward Cabinet as Minister of Industry replacing Gumiwang\'s fellow Golkar Party politician, Airlangga Hartarto.'
								],
								[
									'slug' => 'danny_kennedy',
									'name' => 'Danny Kennedy',
									'subtitle' => 'Chief Energy Officer of New Energy Nexus, United States',
									'pic' => base_url('assets/images/speakers/danny_kennedy.png'),
									'description' => "Danny Kennedy is the Chief Energy Officer for New Energy Nexus, an international organization that supports clean energy entrepreneurs with funds, accelerators and networks. Launched in California, New Energy Nexus now operates programs in China, India, Southeast Asia, and East Africa. Leading up to his role at New Energy Nexus, Danny worked at the California Clean Energy Fund, connecting entrepreneurs everywhere to capital to build an abundant clean energy economy that benefits all. In 2016, CalCEF launched New Energy Nexus. Kennedy is also the  President of CalCharge, a public-private partnership with DoE National Labs and universities in California, unions and companies, working to advance energy storage. Kennedy co-founded Sungevity in 2007, the company that created remote solar design, and Powerhouse, a smart energy incubator and accelerator. He was the first backer of Mosaic in 2011,  the $2B solar loan provider, and remains on the Board of Powerhive, a  solar mini-utility in Kenya and Sunergise, a solar-as-a-service business out of Fiji and EnergyLabAustralia. He is also a Director of the organizations VoteSolar, Power for All and Confluence Philanthropy and  adviser to SolarPhilippines. Kennedy authored the book Rooftop Revolution: How Solar Power Can Save Our Economy–and Our Planet–from  Dirty Energy in 2012. Prior to being an entrepreneur, he worked at  Greenpeace and other NGOs on climate and energy issues for 20+ years."
								],
								[
									'slug' => 'prof__ayi_bahtiar',
									'name' => 'Prof. Ayi Bahtiar',
									'subtitle' => 'Head of Advanced Materials Laboratory at University of Padjadjaran, Indonesia',
									'pic' => base_url('assets/images/speakers/prof__ayi_bahtiar.png'),
									'description' => 'Prof. Ayi Bahtiar is a Head of Advanced Materials Laboratory at University of Padjadjaran, Indonesia. He has served as a lecturer and researcher for University of Padjajaran in the past 25 years. His research interests are polymer solar, cells hybrid organic-metal oxide, perovskite solar cells, graphene to photonics with 604 citations and h-index score on 11.'
								],
								// [
								// 	'slug' => 'prof_khalil_amine',
								// 	'name' => 'Prof. Khalil Amine',
								// 	'subtitle' => 'Argonne Distinguished Fellow, Argonne National Laboratory, USA',
								// 	'pic' => base_url('assets/images/speakers/prof__khalil_amine.jpeg'),
								// 	'description' => 'Prof. Khalil Amine is Argonne Distinguished Fellow, Argonne National Laboratory, USA. He is an Argonne distinguished fellow and leader of the Advanced Lithium Battery Technology Group at the US Department of Energy’s Argonne National Laboratory. His team is focused on the research and development of advanced battery systems with applications in HEV, PHEV, and EV. Amine is an adjunct professor at Stanford University.'
								// ],
								// [
								// 	'slug' => 'rene_schroeder',
								// 	'name' => 'Rene Schroeder',
								// 	'subtitle' => 'Executive Director of European Automotive and Industrial Battery Manufacturers (EUROBAT), Belgium',
								// 	'pic' => base_url('assets/images/speakers/rene_schroeder.png'),
								// 	'description' => 'Rene Schroeder is an Executive Director of European Automotive and Industrial Battery Manufacturers (EUROBAT), Belgium since 2017. EUROBAT is the association for the European manufacturers of automotive, industrial and energy storage batteries. EUROBAT has more than 50 members from across the continent comprising more than 90% of the automotive and industrial battery industry in Europe. He received his bachelor degree in University of Nebraska at Kearney (2001), then Master degree on History/Political Science from Universität Rostock (1997-2002) and Master degree on European Studies/Civilization from College of Europe in Natolin (2003-2004).'
								// ],

								// [
								// 	'slug' => 'prof_jacqui_murray',
								// 	'name' => 'Prof. Eklas Hossain',
								// 	'subtitle' => 'Associate Professor of Electrical Engineering and Renewable Energy, Oregon Tech',
								// 	'pic' => base_url('assets/images/speakers/prof__eklas_hossain.jpg'),
								// 	'description' => 'Prof. Eklas Hossain is an Associate Professor of Electrical Engineering and Renewable Energy, Oregon Tech, United States. Prof. Hossain earned his PhD from the college of Engineering and Applied Science at University of Wisconsin Milwaukee, received his MS in Mechatronics and Robotics Engineering from International Islamic University of Malaysia, and finished his BS in Electrical & Electronic Engineering from Khulna University of Engineering and Technology, Bangladesh. Professor Hossain has more than 10 years of research experience in Renewable Energy/Energy Systems & Advanced Control Systems. He joined the Electrical Engineering and Renewable Energy (EERE) department at Oregon Tech in 2015, where he is a tenured Associate Professor. He has completed several industrial funded projects before and currently. He is currently working with Portland General Electric Company (PGE) and Oregon Renewable Energy Center (OREC) as an industry collaboration with their research projects. Besides the industrial experience, he has more than 23 research papers.'
								// ],
								// [
								// 	'slug' => 'prof_bruno_scrosati',
								// 	'name' => 'Prof. Bruno Scrosati',
								// 	'subtitle' => 'Top 1% the Most Cited on Material Research, University of Rome La Sapienza., Italy',
								// 	'pic' => base_url('assets/images/speakers/prof__bruno_scrosati.jpg'),
								// 	'description' => 'Professor Bruno Scrosati is an Italian electrochemist. From 1980 to 2012 he was Professor of Electrochemistry at La Sapienza University in Rome. He is known as the editor of several books and through a large number of research publications, especially in the field of electrochemical energy storage, especially lithium-ion batteries . For example, he researched electrode materials for use in batteries that were optimized with the help of nanotechnology. He received the title of Doctor in Science "honoris causa”,from the University of St. Andrews in Scotland and from the Chalmers University in Sweden. He is European Editor of the "Journal of Power Sources” and author of more than 450 scientific publications. He also becomes Top 1% the Most Cited on Material Research.'
								// ],
								// [
								// 	'slug' => 'prof_bambang_permadi_soemantri_brodjonegoro',
								// 	'name' => 'Prof. Bambang Permadi Soemantri Brodjonegoro',
								// 	'subtitle' => 'Co-Chair T20 of G20 Indonesia Presidency and Former Minister of Research and Technology/National Research and Innovation Agency of Republic of Indonesia',
								// 	'pic' => base_url('assets/images/speakers/prof__bambang_brodjonegoro.jpg'),
								// 	'description' => "Prof. Bambang Permadi Soemantri Brodjonegoro is a Co-Chair T20 of G20 Indonesia Presidency. He was the Minister of Research and Technology/National Research and Innovation Agency of Republic of Indonesia. Previously, he was the Minister of National Development Planning of Indonesia, taking office after a cabinet reshuffle by President Joko Widodo. From 27 October 2014 to 27 July 2016, he was the Finance Minister of Indonesia in the Working Cabinet also under President Joko Widodo's administration. Under Susilo Bambang Yudhoyono's administration, he was the Deputy of Finance Minister in the Second United Indonesia Cabinet. Professor Bambang Brodjonegoro completed his study at Economics Faculty of Universitas Indonesia in 1990. The next year, Bambang studied for a master's degree at the University of Illinois at Urbana–Champaign, United States, continuing on to doctoral program completed in 1997. On 3 July 2021, Bandung Institute of Technology (ITB) awarded honorary degree on occasion of 101 years anniversary event to Bambang Brodjonegoro. He was one of three people awarded by ITB in auspicious occasion. He awarded with doctor honoris causa degree in Urban Planning."
								// ],
								// [
								// 	'slug' => 'prof_jun_liu',
								// 	'name' => 'Prof. Jun Liu',
								// 	'subtitle' => 'Director of Battery 500 Consortium, United States',
								// 	'pic' => base_url('assets/images/speakers/prof__jun_liu.jpg'),
								// 	'description' => "Prof. Jun Liu is a Battelle Fellow at PNNL, holds a joint appointment with the University of Washington as the Campbell Chair Professor, and also serves as the Director for the Battery500 Consortium, a large DOE multi-institute program supported by DOE to develop future energy storage technologies. Prof. Liu has more than 450 peer reviewed publications, has received more than 60 U.S. patents. He has been a Highly Cited Researcher in the world in the fields of chemistry and materials sciences since 2014. He was named a Distinguished Inventor of Battelle in 2007, and was selected as PNNL's Inventor of the Year in 2012 and 2016. He has received the Electrochemical Society Battery Division Technology Award, the DOE EERE Exceptional Achievement Award, and PNNL Lifetime Achievement Award."
								// ],
								// [
								// 	'slug' => 'prof_jun_liu',
								// 	'name' => 'Prof. Jun Liu',
								// 	'subtitle' => 'Director of Battery 500 Consortium, United States',
								// 	'pic' => base_url('assets/images/speakers/prof__jun_liu.jpg'),
								// 	'description' => "Prof. Jun Liu is a Battelle Fellow at PNNL, holds a joint appointment with the University of Washington as the Campbell Chair Professor, and also serves as the Director for the Battery500 Consortium, a large DOE multi-institute program supported by DOE to develop future energy storage technologies. Prof. Liu has more than 450 peer reviewed publications, has received more than 60 U.S. patents. He has been a Highly Cited Researcher in the world in the fields of chemistry and materials sciences since 2014. He was named a Distinguished Inventor of Battelle in 2007, and was selected as PNNL's Inventor of the Year in 2012 and 2016. He has received the Electrochemical Society Battery Division Technology Award, the DOE EERE Exceptional Achievement Award, and PNNL Lifetime Achievement Award."
								// ],
								// [
								// 	'slug' => 'jennifer_f_scanlon',
								// 	'name' => 'Jennifer F. Scanlon',
								// 	'subtitle' => 'President and CEO of Underwriters Laboratory (UL), United States',
								// 	'pic' => base_url('assets/images/speakers/jennifer_f_scanlon.jpg'),
								// 	'description' => 'Jennifer F. Scanlon is president and chief executive officer (CEO) of UL. Scanlon’s deep commitment to science, technology, engineering and math shape her approach to advancing UL’s leadership as a trust-ingredient brand, driving confidence and trust in the next generation of innovation. Prior to becoming CEO in September 2019, she served as president and CEO of USG Corporation, a manufacturer of innovative building products. During her 16 years at USG, she also served as chief information officer (CIO), and head of Corporate Strategy. She led the company through a major international expansion, transformed the role of technology in driving operational efficiency and customer satisfaction, and invested in research to develop sustainable building materials. Data analytics, machine learning and artificial intelligence were key drivers in the transformation of USG’s understanding of and ability to serve its customers. She began her career at IBM. Her experience will be central to building new offerings, opening new markets, and creating the next generation of solutions at UL. Scanlon serves on the board of Norfolk Southern Corporation and the board and executive committee of the Chicago Council on Global Affairs. She is a member of The Chicago Network, the Economic Club of Chicago and the Commercial Club of Chicago. She graduated from the University of Notre Dame with dual degrees in government and computer applications and a Master of Business Administration in finance and marketing from the University of Chicago Booth School of Business.'
								// ],
								// [
								// 	'slug' => 'dr_fatih_birol',
								// 	'name' => 'Dr. Fatih Birol',
								// 	'subtitle' => 'Executive Director of International Energy Agency (IEA), Turkey',
								// 	'pic' => base_url('assets/images/speakers/dr__fatih_birol.png'),
								// 	'description' => "Dr Fatih Birol has served as Executive Director of the International Energy Agency since 2015. Under his leadership, the IEA has moved to the forefront of global efforts to reach international climate goals while ensuring that the social and economic impacts of clean energy transitions are at the heart of policy-making and energy security is safeguarded. After taking office, Dr. Birol led the IEA in its first comprehensive modernization program since its creation in 1974. These efforts focused on “opening the doors” of the IEA to major emerging economies including Brazil, China, India and South Africa; making the IEA the global hub for clean energy transitions; and broadening the IEA’s energy security focus beyond oil to also cover electricity, natural gas, renewables and the critical minerals needed in many of today's clean energy technologies. With new governments joining the IEA, under his tenure the Agency’s share of global energy demand has risen from 40% to 75%."
								// ],
								// [
								// 	'slug' => 'francesco_la_camera',
								// 	'name' => 'Francesco La Camera',
								// 	'subtitle' => 'Director General of International Renewable Energy Agency (IRENA), Italy',
								// 	'pic' => base_url('assets/images/speakers/francesco_la_camera.jpg'),
								// 	'description' => 'Francesco La Camera is the Director-General of the International Renewable Energy Agency (IRENA). He was appointed at the Ninth Assembly of IRENA, the ultimate decision-making body of the Agency. Mr. La Camera took office on 4 April 2019 and brings more than thirty years of experience in the fields of climate, sustainability, and international cooperation. In his role, Mr. La Camera is responsible for leading the delivery of IRENA’s work program and strategy in cooperation with the Agency’s member states. At a critical time for climate change and the achievement of the Sustainable Development Goals, Mr. La Camera is tasked with redefining the structure and operations of the Agency in response to the urgent needs of its members. Under his leadership the Agency has forged a series of new strategic partnerships with UN organizations including UNDP, UNFCCC and Green Climate Fund among others. A key priority of his tenure is to implement a more action-oriented approach to the Agency’s work. Previously, Mr. La Camera served as Director-General of Sustainable Development, Environmental Damage, EU and International Affairs at the Italian Ministry of Environment, Land & Sea since 2014. In this capacity, he developed cooperation and partnership agreements with a wide range of countries, particularly developing countries including Small Island Developing States (SIDS). Mr. La Camera held number of roles at the Italian Ministry of Environment, Land and Sea. As the national coordinator for climate, environment, resource efficiency and circular economy, he led the Italian delegation to UNFCCC’s COP 21 to 24 and the EU Presidency at COP 20. He was responsible for the preparation and organisation of Italy’s G7 Environment Presidency in 2017. He was a leading member of the Italian delegation to the G7 Environment Ministers meetings in 2016 and 2018.'
								// ],
								// [
								// 	'slug' => 'dr_rainer_janssen',
								// 	'name' => 'Dr. Rainer Janssen',
								// 	'subtitle' => 'President of European Renewable Energy Research Centers Association, Germany',
								// 	'pic' => base_url('assets/images/speakers/dr__rainer_janssen.jpg'),
								// 	'description' => 'Dr. Rainer Janssen is a President of European Renewable Energy Research Centers Association, Germany. He also heads of the Biomass Department at WIP Renewable Energies and Senior Expert in Biomass. He specializes in the production, distribution and market penetration of biomass energy (solid biomass, biogas) and biofuels for transport (bioethanol, biodiesel, vegetable oil) with special emphasis on innovative technologies, research and innovation policies, market research, public awareness as well as the development of supportive framework conditions and policy regulations in the EU and emerging and developing economies. He graduated in Physics (Dr. rer.nat.) at the Technical University of Munich, Walter Schottky Institute, Germany and performed studies at the University of Toronto, Canada. Dr Janssen coordinated various international and European bioenergy projects and is invited expert for the European Commission (DG RTD, DG ENER), IEA (International Energy Agency) Bioenergy, and GIZ (German Development Cooperation). Since 2009, he is member of the Steering Committee of the Biomass Panel of the RHC TP (Technology Platform on Renewable Heating and Cooling) and member of working group 4 on “Policy and Sustainability” of the EBTP (European Biofuels Technology Platform). Since 2012 Dr Janssen is biomass expert for the GIZ on national and international developments in the area of biomass and biofuels and Vice-President of EUREC, the Association of European Renewable Energy Research Centres. In 2014, he participated in the expert Working Group for the elaboration of the SET-Plan Integrated Roadmap on behalf of the RHC TP.'
								// ],


							],
							'mentioned_in_speakers_page' => true
						],
						'keynote_speakers' => [
							'title' => 'Keynote Speakers',
							'content' => [
								[
									'slug' => 'prof__worawat_meevasana',
									'name' => 'Prof. Worawat Meevasana',
									'subtitle' => 'Assistant Professor of Suranaree Institute of Technology, Thailand',
									'pic' => base_url('assets/images/speakers/prof__worawat_meevasana.png'),
									'description' => 'Prof. Worawat Meevasana is an Assistant Professor of Suranaree Institute of Technology, Thailand. He is also Head of School of Physics, Suranaree University of Technology, Thailand from 2015 to present. Professor Meevasana holds a Bachelor of Science in Physics from University of California, Santa Barbara (UCSB), USA. He graduated in 2022 with the highest honor. Then, Prof. Meevasana continued his study in Master of Physics, Stanford University and graduated in 2007. He completed his Ph.D. in Physics, Stanford University back in 2009. In 2015, he was awarded the Outstanding Research Award from National Research Council of Thailand. Prof. Meevasana has various research expertise & interests, such as Synchrotron Radiation (SR) which can produce very bright light at various wavelengths, usually from infrared up to X-ray. Due to this brightness and the wavelength tunability, SR can have a wide range of applications, including many research fields (e.g. physics, material science, biology, chemistry, environmental study and engineering). Realizing much use of SR techniques, our group has much interest in exploiting SR techniques available at the Thai institute (Synchrotron Light Research Institute, SLRI) and the laboratories abroad (e.g. ALS and SSRL) to various research topics. Our current research interests are on 1) transition-metal oxides/ dichalcogenides, 2) carbon-based materials, 3) physical phenomena at low temperature. Professor Meevasana is one of the leading and most famous Physicists both in Thailand and at the international level and his research on batteries and EV vehicles is well-known.'
								],
								// [
								// 	'slug' => 'richard_tandiono',
								// 	'name' => 'Richard Tandiono',
								// 	'subtitle' => 'President Director of PT. Trinitan Metals and Minerals, Indonesia',
								// 	'pic' => base_url('assets/images/speakers/richard_tandiono.jpg'),
								// 	'description' => 'Richard Tandiono is a President Director of PT. Trinitan Metals and Minerals, Indonesia. He obtained his Bachelor of Science in Industrial and Systems Engineering from the University of Southern California in California, USA in 2002 and obtained his Master of Science in Engineering Management, from the University of Southern California in California, USA in 2004. Previously, he also President Commissioner of PT. Sky Energy Indonesia Tbk. '
								// ],
								// [
								// 	'slug' => 'pratjojo_dewo_sridadi',
								// 	'name' => 'Pratjojo Dewo Sridadi',
								// 	'subtitle' => 'President Director of PT. Komatsu Indonesia, Indonesia',
								// 	'pic' => base_url('assets/images/speakers/pratjojo_dewo.jpg'),
								// 	'description' => 'Pratjojo Dewo Sridadi is a President Director of PT. Komatsu Indonesia. The Komatsu Nature of Business are manufacturing of construction & mining equipment, machineries and its components & steel and iron casting.'
								// ],
								[
									'slug' => 'prof__arief_s_budiman',
									'name' => 'Prof. Arief S Budiman',
									'subtitle' => 'Director of Oregon Renewable Energy Center), USA',
									'pic' => base_url('assets/images/speakers/prof__arief_s_budiman.png'),
									'description' => 'Professor Arief S Budiman is a Director of Oregon Renewable Energy Center), United States of America. He finished his doctoral at Stanford University in 2008, taking material science and engineering as his field. He received several research awards and contributed to several high-impact journal publications, such as the prestigious Los Alamos National Laboratory (LANL) Director\'s Research Fellow Award in 2009 and received a Science Highlight from the famed Berkeley Lab for the technological breakthrough that has attraced wide industrial reception.'
								],
								[
									'slug' => 'prof__giichiro_uchida',
									'name' => 'Prof. Giichiro Uchida',
									'subtitle' => 'Professor of Faculty of Electrical and Electronic Engineering, Meijo University, Japan',
									'pic' => base_url('assets/images/speakers/prof__giichiro_uchida.png'),
									'description' => 'Prof. Giichiro Uchida is a Professor at the Faculty of Science and Technology, Meijo University Japan since April 2019. He obtained his Bachelor of Science from School of Engineering, Tohoku University, Japan in March 1994. He continued his learning journey through a Master Degree in the Graduate School of Engineering, Tohoku University, Japan in March 1998. Then, Professor Uchida obtained his Ph.D. at the same faculty and university in March 2001. Professor Uchida has received various prestigious awards since December 2007. Recently, He has been awarded the Best Presentation Award from the 13th International Symposium on Advanced Plasma Science and its Applications for Nitrides in March 2021. Prof. Uchida has published 102 papers, with 57 invited presentations and 12 patents in total.'
								],
								[
									'slug' => 'dr__eng__budi_prawara',
									'name' => 'Dr. Eng. Budi Prawara',
									'subtitle' => 'Chairman of Research Organization for Electronics and Informatics, National Research and Innovation Agency (BRIN)',
									'pic' => base_url('assets/images/speakers/dr__eng__budi_prawara.png'),
									'description' => 'Dr. Eng. Budi Prawara is the Chairman of Research Organization for Electronics and Informatics, National Research and Innovation Agency (BRIN), Indonesia. He obtained his PhD and Master degree from University of Ryukyus, Japan on material engineering.'
								],
								[
									'slug' => 'muhammad_fakhrudin__st',
									'name' => 'Muhammad Fakhrudin, ST.',
									'subtitle' => 'Assistant Manager of Research, Development and Innovation, National Battery Research Institute.',
									'pic' => base_url('assets/images/speakers/muhammad_fakhrudin__st.png'),
									'description' => 'Muhammad Fakhrudin, S.T. is a researcher of National Research and Innovation Agency (BRIN). He is also a senior researcher of National Battery Research Institute. He graduated from Chemical Engineering, Institute Technology of Bandung. His research experience on “Production of Advanced Biomaterials Cellulose Nano Crystallines (CNC) from Palm Oil Bunches as Pt/Rh/Ce Catalyst Buffer Material for Catalytic Converters” 2017 - 2018. He also has scientific articles on “Preparation of cellulose nanocrystals from empty fruit bunch of palm oil by using phosphotungstic acid” IOP Conference Series: Earth and Environmental Science Vol 105(1) 2018.'
								],
								[
									'slug' => 'muhammad_firmansyah_se',
									'name' => 'Muhammad Firmansyah, SE.',
									'subtitle' => 'CEO of PT. Infiniti Energi Indonesia',
									'pic' => base_url('assets/images/speakers/muhammad_firmansyah__se.png'),
									'description' => 'Muhammad Firmansyah completed his undergraduate education from Padjajaran University Bandung in 2015 majoring International Business Management. Then, He deepened his knowledge in entrepreneurship. Firman began his career at PT. HM. Sampoerna Tbk in 2015. Initially he got a position as a graduate trainee, then as a Consumer Engagement Supervisor. In 2016, he gained a position as Area Supervisor for Multitasking. In 2018, he held the position of Area Analyst and later became Area Manager. In the same year he founded his consulting company named PT. Infiniti Energi Indonesia and became President Director. In addition, he also became a commissioner of CV. Tridaya Cavali.'
								],
								// [
								// 	'slug' => 'prof_takashi_kamiyama',
								// 	'name' => 'Prof. Takashi Kamiyama',
								// 	'subtitle' => 'Spallation Neutron Source, China',
								// 	'pic' => base_url('assets/images/speakers/prof__takashi_kamiyama.jpg'),
								// 	'description' => 'Prof. Kamiyama is an Associate Professor at the Divison of Quantum Science aand Engineering of Faculty of Engineering, Hokkaido University. He has been interested in topics including dynamical structure of amorphous materials, neutron scattering and imaging using pulsed sources, and neutron devices for detecting and beam control. He received his bachelor’s, master’s, and Ph.D degrees from Tohoku University.'
								// ],
								[
									'slug' => 'dr__haznan_abimanyu',
									'name' => 'Dr. Haznan Abimanyu',
									'subtitle' => 'Chairman of Research Organization for Energy and Manufacture, National Research and Innovation Agency (BRIN), Indonesia',
									'pic' => base_url('assets/images/speakers/dr__haznan_abimanyu.png'),
									'description' => 'Dr. Haznan Abimanyu is a Chairman of the Research Organization for Energy and Manufacture, National Research and Innovation Agency (BRIN), Indonesia. Dr. Haznan started his research career in 1997 at the Research Center for Chemistry at the Indonesian Institute of Science (LIPI). He pursued his PhD in University of Science and Technology, South Korea. His research scope related to energy, catalyst, atsiri, and oleochemistry.'
								],
								[
									'slug' => 'commissioner_general_of_police__ret__nanan_soekarna',
									'name' => 'Commissioner General of Police (Ret) Nanan Soekarna',
									'subtitle' => 'Chairman of Indonesian Nickel Mining Association (APNI), Indonesia',
									'pic' => base_url('assets/images/speakers/commissioner_general_of_police__ret__nanan_soekarna.png'),
									'description' => 'Commissioner General of Police (Ret) Nanan Soekarna is a Chairman of Indonesian Nickel Mining Association (APNI), Indonesia. He is a retired Indonesian National Police officer with the last rank as Commissioner General of Police. Graduated from the Police Academy in 1978, the College of Police Science (PTIK) in 1986, SESPIMPOL in 1995, SESKOGAB in 1999 and LEMHANNAS in 2005. He started his career as Dan Unit Patko Sabhara Polda Metro Jaya in 1979 and was later assigned to several police units in several parts of Indonesia. Previously served as Wakapolda Metro Jaya (2003-2004), Kapolda West Kalimantan (2004-2006), Kapolri Social Political Expert Staff (2006-2008), Kapolda North Sumatra (2008-2009), Inspector General Supervision of Police (2009-2011) and Deputy Chief of Police of the Republic of Indonesia (2011-2013). Also, part of UN peacekeeping forces, including UNTAG in Namibia, South Africa, in 1990 and UNTAC in Cambodia in 1992; as well as attending various police training and seminars abroad.'
								],
								// [
								// 	'slug' => 'jeff_pratt',
								// 	'name' => 'Jeff Pratt',
								// 	'subtitle' => 'Managing Director of UK Battery Industrialization Centre (UKBIC), United Kingdom',
								// 	'pic' => base_url('assets/images/speakers/jeff_prat.jpg'),
								// 	'description' => 'Jeff Pratt is a Managing Director of UK Battery Industrialization Centre (UKBIC), United Kingdom. It is £80 million UK Battery Industrialization Centre which is part of the Government’s Faraday Battery Challenge – a £246 million commitment over the next four years on battery development for the automotive sector. Jeff has extensive experience in Production Engineering and Facility Engineering as well as more than 20 years’ senior management experience. He has had responsibility for delivering a range of new model and facility projects as well as managing daily operations in part of the biggest car manufacturing facility in the UK. Jeff, who has a BEng (Hons) in Electrical Engineering and MEng, said: “I feel privileged and excited to be appointed to lead the introduction of the UK Battery Industrialization Centre.'
								// ],
								// [
								// 	'slug' => 'prof_jacqui_murray',
								// 	'name' => 'Prof. Jacqui Murray',
								// 	'subtitle' => 'Deputy Director of Faraday Battery Challenge Innovate UK, United Kingdom',
								// 	'pic' => base_url('assets/images/speakers/prof__jacqui_murray.png'),
								// 	'description' => 'Prof. Jacqui Murray is a Deputy Director of Faraday Battery Challenge Innovate UK, United Kingdom. As Deputy Director of the £318m million Faraday Battery Challenge Jacqui helps lead government investment to develop batteries that are cost-effective, long range, fast charging, durable, safe and recyclable. Her background in the steel industry, environmental regulation and advanced materials innovation provides industrial relevance in her role as a Visiting Professor (Royal Academy of Engineering) at the University of Leicester, where she enhances the engineering program. A keen STEM ambassador, Jacqui takes an active leadership approach on diversity and inclusion throughout her work. Named as one of Autocar’s Top 100 Women in Automotive based on Seniority and Influence in 2020.'
								// ],

								// [
								// 	'slug' => 'prof_stefan_adams',
								// 	'name' => 'Prof. Stefan Adams',
								// 	'subtitle' => 'Associate Professor of National University of Singapore (NUS, Singapore',
								// 	'pic' => base_url('assets/images/speakers/prof__stefan_adams.png'),
								// 	'description' => 'Prof. Adams is an Associate Professor at Department of Materials Science and Engineering, National University of Singapore since 2011. His work is to combine in situ characterization by electrochemical, neutron, and x-ray diffraction methods with computational approaches to promote the understanding of charge and mass transport in solids and the underlying structure-property correlations. His studies also encompass a wide range of materials for sustainable energy applications such as fast ion conducting solids and mixed conducting cathode materials for lithium batteries with higher power or energy density, and ceramic fuel cells operating at moderate temperatures, nanocomposites for chemica and electrochemical energy storage.'
								// ],
								// [
								// 	'slug' => 'prof_vanessa_peterson',
								// 	'name' => 'Prof. Vanessa Peterson',
								// 	'subtitle' => 'Principal Research of ANSTO, Australia',
								// 	'pic' => base_url('assets/images/speakers/prof__vanessa_peterson.png'),
								// 	'description' => 'Prof. Peterson is a principal research and neutron scattering instrument scientist. She is a leader of Energy Materials Research Project Neutron Instrument Scientist, Powder Diffraction (Wombat and Echidna instruments) ANSTO. She also a Honorary Professional Fellow, Institute for Superconducting and Electronic Materials, University of Wollongong. She has an expertise in understanding materials function by relating atomic-scale structure and dynamics to material properties. Also condensed matter materials of interest include porous coordination framework materials, energy-industry relevant materials such as batteries, gas-separation and storage materials, and fuel-cells, as well as cement.'
								// ],
								// [
								// 	'slug' => 'diyanto_imam',
								// 	'name' => 'Diyanto Imam',
								// 	'subtitle' => 'Director of New Energy Nexus Indonesia',
								// 	'pic' => base_url('assets/images/speakers/diyanto_imam.jpg'),
								// 	'description' => 'Diyanto Imam is a Director of New Energy Nexus Indonesia. He is an impact-focused professional who delivers creative business solutions with significant and extensive experience in hardware-based startups and impact startups and impact enterprise development in Indonesia. He pursued his bachelor degree in Management at Queensland University of Technology (QUT) 1998 and master’s degree on International Business at Swinburne University of Technology in 2000.'
								// ],

							],
							'mentioned_in_speakers_page' => true
						],
					]
				],
				'ibs_2022' => [
					'description' => 'International Battery School (IBS) 2022',
					'package_detail' => [
						'international' => [
							'regular' => [
								'Industry_and_Government' => [
									'price' => [
										'early_bird' => [
											'amount' => 140,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-05-24 00:00:00'))
										],
										'normal' => [
											'amount' => 250,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
								'University_and_R&D' => [
									'price' => [
										'early_bird' => [
											'amount' => 75,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-05-24 00:00:00'))
										],
										'normal' => [
											'amount' => 150,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
							],
						],
						'indonesian' => [
							'regular' => [
								'Industry_and_Government' => [
									'price' => [
										'early_bird' => [
											'amount' => 2000000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-05-24 00:00:00'))
										],
										'normal' => [
											'amount' => 3000000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
								'University_and_R&D' => [
									'price' => [
										'early_bird' => [
											'amount' => 1000000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-05-24 00:00:00'))
										],
										'normal' => [
											'amount' => 2000000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
							],
						],
					],
					'detail' => [
						'lecturers' => [
							[
								'name' => 'Prof. Dr. Mohammad Zaki Mubarok',
								'pic' => 'prof__zaki_mubarok.jpg',
								'title' => 'Professor of Institute Technology of Bandung, Indonesia',
								'description' => 'Prof. Dr. mont. M. Zaki Mubarok, ST., MT. Is a head of Hydro-Electrometallurgy Laboratory of ITB. He pursued Doctoral degree from Department of Nonferrous Metallurgy, University of Leoben, Austria 2005. He also published many research papers and various award. In 2020, he received high-impact research article awards from National Research and Innovation Agency.'
							],
							[
								'name' => 'Prof. Dr. rer. nat. Evvy Kartini',
								'pic' => 'prof__evvy_kartini.png',
								'title' => 'Founder of National Battery Research Institute (NBRI), President of Material Research Society Indonesia (MRS-INA), and Research Professor of National Research and Innovation Agency (BRIN), Indonesia',
								'description' => 'Evvy Kartini holds the title of Prof. Dr. rer. nat. She is an expert on neutron scattering and respected internationally. Her international reputation in the field of neutron scattering and solid state ionics, has been well established. She has been represented as a leader (president) of the Indonesian Neutron Scattering Society (INSS) since 2013. She is one of the executive committees of the Asian Oceania Neutron Scattering Society (AONSA). Besides neutron scattering, Evvy Kartini has expertise on the materials science, especially on lithium ion battery research. Evvy Kartini, has become one of the International Board Members of the Asian Society of Solid-State Ionics and International Society of Solid-State Ionics. Recently, Evvy Kartini has attended the 20th International Conference on Solid State Ionics (ICSSI) in Keystone, Colorado, USA 2015. She was there not only for presenting her research work on battery researchers, but also represented the Asia-Australia region as councilor of the International Society of Solid-State Ionics. She was one of the International Advisory Board, and also chaired the session of Research on Battery Materials. She also presented her recent topic on development of new lithium solid electrolyte, which is explored by the inelastic spectrometer instrument at Japan Proton Accelerator Research Complex (J-PARC), the foremost leading spallation neutron source in the world.'
							],
							[
								'name' => 'Prof. Alan J Drew',
								'pic' => 'prof_alan_j_drew.png',
								'title' => 'Professor and Director of the Materials Research Institute, Queen Mary University of London (QMUL), United Kingdom',
								'description' => 'Professor Alan Drew was appointed Leverhulme Fellow in the Centre for Condensed Matter & Materials Physics in 2008. He was rapidly promoted to Senior Lecturer (2011) and then Reader (2012), and in 2018 was promoted to Professor. Prof. Drew has been awarded a number of prestigious fellowships and awards over his career, starting with a Fellowship of the Royal Commission of the Exhibition of 1851 (2004), Leverhulme Fellow (2008), European Research Council (ERC) Fellow (2012), Talent 1000 Scholar of the Chinese Ministry of Education (2014) and Changjiang Distinguished Professor at Sichuan University (2015). He is currently Head of the CCMMP Research Centre, and Director of the Materials Research Institute. His main research interests are using spin sensitive and structural probes situated at central facilities to characterize and understand the fundamental properties of materials, backed up with laboratory based techniques (e.g magnetic, structural and electrical characterisation, thin film growth, raman/IR spectroscopy, electro/photo luminescence). His main interests are understanding spin and charge carrier dynamics in organic and biological materials, the properties of materials with novel quantum mechanical states, structure function relationships in biomass derived conductors.'
							],
							// [
							// 	'name' => 'Prof. Pooi See Lee',
							// 	'pic' => 'prof__pooi_see_lee.png',
							// 	'title' => 'Professor of Nanyang Technological University (NTU), Singapore',
							// 	'description' => 'Pooi See Lee received her Ph.D. from National University of Singapore in 2002. She joined the School of Materials Science and Engineering, Nanyang Technological University as an Assistant Professor in 2004. She was promoted to tenured Associate Professor in 2009 and full Professor in 2015. Her research focuses on nanomaterials for energy and electronics applications, ﬂexible and stretchable devices, electrochemical inspired devices, and human-machine interface. Pooi See is a recipient of the prestigious NRF Investigatorship 2015. From 2016 to 2019, she served as the Associate Dean in College of Engineering. She is appointed as the Dean of Graduate College in 2020. She receives the Nanyang Research Excellence Award 2016 and Nanyang Entrepreneurship and Innovation Award 2018. She is interested in harnessing its multi-functionality through understanding of the structural-property characteristics. She has developed high energy capacitors, energy saving electrochromic coatings, novel transparent conductors, flexible and stretchable devices. Currently, she is the Senior Editor of ACS Energy Letters. She is elected as the National Academy of Inventors (NAI) Fellow in 2020, the highest professional distinction accorded solely to academic inventors.'
							// ],
							[
								'name' => 'Muhammad Fakhrudin, ST.',
								'pic' => 'muhammad_fakhrudin,_st.png',
								'title' => 'Researcher of National Research and Innovation Agency (BRIN), Indonesia',
								'description' => 'Muhammad Fakhrudin, S.T. is a researcher of National Research and Innovation Agency (BRIN). He is also a senior researcher of National Battery Research Institute. He has graduated from Chemical Engineering, Institute Technology of Bandung. His research experience on “Production of Advanced Biomaterials Cellulose Nano Crystallines (CNC) from Palm Oil Bunches as Pt/Rh/Ce Catalyst Buffer Material for Catalytic Converters” 2017 - 2018. He also has scientific articles on “Preparation of cellulose nanocrystals from empty fruit bunch of palm oil by using phosphotungstic acid” IOP Conference Series: Earth and Environmental Science Vol 105(1) 2018.'
							],
							[
								'name' => 'Ir. Chairul Hudaya, S.T., M.Eng., Ph.D, IPM',
								'pic' => 'ir__chairul_hudaya__s_t___m_eng___ph_d__ipm.jpg',
								'title' => 'Rector of University of Technology Sumbawa (UTS), Indonesia',
								'description' => 'Dr. Hudaya was born in a small village, Bandar Harapan, District of Terbanggi Besar,  Province of Lampung, Sumatra Island, Indonesia. He earned his bachelor degree at the Department of Electrical Engineering, Universitas Indonesia in July 2006. After graduation, he served as a lecturer assistant at the Division of Electric Power Engineering Universitas Indonesia for 1 semester. He received a scholarship funded by the Korean Government Undergraduate Scholarship for excellent Foreign Students (GSFS) Program at Department of Energy Systems Engineering, Seoul National University, Korea. He focused his research interest on Nuclear Safety Analysis and completed the program in February 2009. He continued his doctoral degree at the Department of Energy and Environmental Engineering, University of Science and Technology at Korea Institute of Science and Technology. He took his engineer (Ir.) in June 2016 on Persatuan Insinyur Indonesia (PII) as Insinyur Profesional Madya. Currently, he serves as Rector of University of Technology of Sumbawa (UTS), Indonesia.'
							],
							// [
							// 	'name' => 'Prof. Ir. Muhammad Nizam, ST., MT., PhD.',
							// 	'pic' => 'prof__ir__muhammad_nizam,_st_,_mt_,_ph_d_.jpeg',
							// 	'title' => 'Former Coordinator of National Research Priority Mandatory on Energy Storage, Sebelas Maret University (UNS), Indonesia',
							// 	'description' => 'Prof. Ir. Muhammad Nizam is presently working as a senior lecturer in Universitas Sebelas Maret in Surakarata. He achieved his bachelor and master’s degree from Universitas Gadjah Mada majoring in Electrical engineering while the PhD of Electrical, Electronics and Systems Engineering was obtained from Universiti Kebangsaan Malaysia in 2008. Prof. Nizam put his research interest on Power Systems, Renewable Energy, Energy Management, Energy Storage Systems, and Power Quality. Recently, Prof. Nizam serves as Head of National Research Priority of Indonesia on lithium battery and fast charging station.'
							// ],
							[
								'name' => 'Mohammad Ridho Nugraha, ST.',
								'pic' => 'mohammad_ridho_nugraha,_st.png',
								'title' => 'Supervisor of Battery Testing and Standardization, National Battery Research Institute, Indonesia',
								'description' => 'Mohammad Ridho Nugraha, S.T. is a Supervisor of Battery Testing and Standardization of National Battery Research Institute in Laboratory and Operation Department. He graduated from Metallurgy & Materials Engineering, Universitas Indonesia in 2019. He has become a Battery Laboratory Technician at Material Energy Laboratory, Faculty of Engineering, Universitas Indonesia (2018 – 2020). He also has three years experiences in Lithium-ion battery research (including LTO and LFP material synthesis for performance optimization, Battery Cell Fabrication, Material Characterization, Battery Performance Testing, and Battery pack assembly), 10 International Publication in IOP, AIP, and IJTECH about Lithium-ion Batteries (2018–2021), and three times presenter in International Conferences (2018). He also mastering product prototyping design and drafting using computer-aided design and drafting (CADD) software.'
							],
							[
								'name' => 'Muhammad Firmansyah, SE.',
								'pic' => 'muhammad_firmansyah,_se.jpg',
								'title' => 'CEO of PT. Infiniti Energi Indonesia',
								'description' => 'Muhammad Firmansyah completed his undergraduate education from Padjadjaran University in Bandung in 2015 majoring in International Business Management. He then deepened his knowledge in entrepreneurship. Firman began his career at PT. HM. Sampoerna Tbk in 2015. Initially he got a position as a graduate trainne, then as a Consumer Engagement supervisor. In 2016 he gained a position as Area Supervisor for Multitasking. In 2018, he held the position of Area Analystist and later became Area Manager. In the same year he founded his consulting company named PT. Infiniti Energi Indonesia and became President Director. In addition, he also became a commissioner of CV. Tridaya Cavali. He has gone through various training such as the Future Market and Financial Analyst in 2015, Commercial Business in 2017 and Human Resource Development in 2018. He was very interested in developing lithium ion batteries, biomass, industry and financial research and business plan research so he joined the National Battery Research Institute (NBRI) team as a Vice Leader along with Prof. Dr. rer nat Evvy Kartini, Prof. Dr. Alan J. Drew and other NBRI teams.'
							],
							[
								'name' => 'Meidy Katrin Lengkey',
								'pic' => 'meidy_katrin_lengkey.png',
								'title' => 'General Secretary of Association of Nickel Miners Indonesia (APNI), Indonesia',
								'description' => 'Meidy Katrin Lengkey is a General Secretary of Association of Nickel Miners Indonesia (APNI). She serves on the Investment Committee at the Ministry of Investment/ Indonesia Investment Coordinating Board (BKPM). She also became a Country Manager for Indonesia, JTA International Qatar Holding Company. Mrs. Meidy has various business experiences as Director and commissioner. Such as the Director of PT. Sinergy Metal Mineral, Director of PT. Indonesian Nickel Cakrawala, Director of CV Tiga Utama, Commissioner of PT. Yakin Sukses Makmur, President Director of PT. Digital Synergy Indonesia, Director of PT. Madisindo Dinamika, and Director of PT. Masindo Dinamika.'
							],
							[
								'name' => 'Yoga Mugiyo Pratama, S.T.',
								'pic' => 'yoga_mugiyo_pratama__s_t_.jpg',
								'title' => 'Head of R&D Project Strom supported by Hyundai Kefico',
								'description' => 'Yoga Mugiyo Pratama, S.T. is a Head of R&D Project Strom supported by Hyundai Kefico. He received his bachelor degree on Materials and Metallurgical Engineering from Institute Technology of Sepuluh Nopember (ITS), Surabaya. Then, he continues education journey in ITS through the Engineering Profession Program at present. Mr. Yoga has broad experiences on electric vehicles (EV). He is a former Head of R&D PT. Gesits Technologies Indonesia (GESITS) from 2019-2021. Previously, he also served as National Electric Vehicle Researcher at PUI SKO ITS (Indonesia Center of Excellence in Automotive Control System (2016 - 2019). And from 2015 until now, Mr. Yoga has been actively involved as Advisor and Driver of Hydrogen Car Research Competition for Antasena ITS Team.'
							],
							// [
							// 	'name' => 'Viar Motor Indonesia',
							// 	'pic' => 'viar_motor_indonesia.png',
							// 	'title' => 'Viar Motor Indonesia',
							// 	'description' => 'Established in 2000, PT Triangle Motorindo as Principal of the Viar Motor Indonesia brand is here with the determination to become one of the largest two-wheeled and commercial motorcycle manufacturers in Indonesia. This is evidenced by the company\'s commitment to always produce Viar motorcycles with high quality and affordable prices for the people of Indonesia. Owning a 20 hectares production plant located in Bukit Semarang Baru, PT. Triangle Motorindo has been able to produce motorcycles and commercial motorcycles with a production capacity of up to 1000 units per day, making it one of the largest automotive factories in Indonesia. Not only that, the company also continues to increase the downstreaming of domestic industrial components to become part of the Viar supply chain. In order to make it easier for people to get excellent after-sales service, currently PT. Triangle Motorindo already has more than 700 networks spread to districts and will continue to grow. In addition, to make it easier for the public to have products from Viar Motor, the company understands various financial institutions, such as: ADIRA Finance, BCA Multifinance, MEGA Finance, etc.'
							// ]
						],
						'what-you-will-learn' => [
							[
								'name' => 'The untapping Indonesia’s mineral resources for battery technology',
								'content' => [
									'Introduction to local mineral resources (nickel, manganese, cobalt, etc.)',
									'Introduction to rare earth elements',
									'Global Supply chain of battery materials industry',
									'Mining processing to gain battery raw materials'
								]
							],
							[
								'name' => 'All about lithium ion batteries',
								'content' => [
									'Lithium ion batteries introduction',
									'Battery components materials structure and how it works',
									'Battery cell performance testing introduction',
									'Global battery market landscape and Indonesia battery road map'
								]
							],
							[
								'name' => 'The scintillating of future battery technology',
								'content' => [
									'The prospective of global battery innovation and its challenges',
									'Future battery technology introduction (Solid state battery, Sodium-ion battery, Lithium-Sulphur battery, Graphene, Silicon Nanowires, Composite Solid Electrolytes)',
									'Indonesia’s current battery technology development',
									'The value chain of future battery technology'
								]
							],
							[
								'name' => 'The development of battery material with the high standard',
								'content' => [
									'Development of battery materials & its characterization',
									'Introduction to basic battery material characterization (XRD, SEM, DTA, etc.)',
									'X-Ray Diffraction Introduction',
									'XRD data analysis demonstration'
								]
							],
							[
								'name' => 'All about cylindrical battery cell',
								'content' => [
									'Cylindrical battery cell introduction',
									'The prospect of cylindrical cell and its challenge',
									'From laboratory to industry',
									'Battery and BMS application'
								]
							],
							[
								'name' => 'Get to know battery regulation & standardization',
								'content' => [
									'Why is battery standardization critical?',
									'Introduction to battery standard (IEC, UL, SNI, etc.)',
									'Battery cell and pack safety testing',
									'Current battery regulation & standardization'
								]
							],
							[
								'name' => 'Prospect of Energy Storage for Solar PV and Renewable Energy',
								'content' => [
									'Renewable Energy Engineering Procurement Construction (EPC) introduction',
									'The importance of lithium ion batteries on renewable energy',
									'Lithium ion batteries business prospect and its challenge',
									'Battery supply chain'
								]
							],
							[
								'name' => 'Battery in Electric Vehicles and Its Ecosystem',
								'content' => [
									'The role of battery in electric vehicles',
									'The benefit and challenge of battery in EV ecosystem',
									'The current condition of battery application for EV',
									'The current condition of EV Ecosystem'
								]
							],
						],
						'what-you-will-get' => [
							'Learning Module from the Experts',
							'International E-Certificate (Recognized by Queen Mary University of London)'
						],
					],
					'event_date' => [
						'start_date' => date('Y-m-d H:i:s', strtotime('2022-05-24 00:00:00')),
						'end_date' => date('Y-m-d H:i:s', strtotime('2022-05-25 00:00:00'))
					]
				],
				'isrus_2022' => [
					'description' => 'International Workshop on Solar Rooftop Residential and Utilities Scale (ISRUS) 2022',
					'package_detail' => [
						'international' => [
							'regular' => [
								'Student' => [
									'price' => [
										'early_bird' => [
											'amount' => 50,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-08-03 00:00:00'))
										],
										'normal' => [
											'amount' => 100,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
								'Industry_and_Government' => [
									'price' => [
										'early_bird' => [
											'amount' => 200,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-08-03 00:00:00'))
										],
										'normal' => [
											'amount' => 250,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
								'University_and_R&D' => [
									'price' => [
										'early_bird' => [
											'amount' => 100,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-08-03 00:00:00'))
										],
										'normal' => [
											'amount' => 150,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
							],
						],
						'indonesian' => [
							'regular' => [
								'Student' => [
									'price' => [
										'early_bird' => [
											'amount' => 500000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-08-03 00:00:00'))
										],
										'normal' => [
											'amount' => 1000000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
								'Industry_and_Government' => [
									'price' => [
										'early_bird' => [
											'amount' => 2000000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-08-03 00:00:00'))
										],
										'normal' => [
											'amount' => 2500000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
								'University_and_R&D' => [
									'price' => [
										'early_bird' => [
											'amount' => 1000000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-08-03 00:00:00'))
										],
										'normal' => [
											'amount' => 1500000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Learning Module from the Experts',
										'International E-Certificate (Recognized by Queen Mary University of London)'
									]
								],
							],
						],
					],
					'detail' => [
						'lecturers' => [
							[
								'slug' => 'fabby_tumiwa',
								'name' => 'Fabby Tumiwa',
								'subtitle' => 'Chairman of Indonesia Solar Energy Association (ISEA) and Executive Director of Institute for Essential Services Reform (IESR)',
								'pic' => base_url('assets/uploads/' . $this->uri->segment(2) . '/images/isrus_2022/lecturers/fabby_tumiwa.jpg'),
								'description' => 'Fabby Tumiwa is one of the founders of the Indonesia Solar Energy Association (ISEA) in 2016, then serves as vice Chairman, and since 2021 he has been leading the association as the Chairman. He was on the executive committee on institutional cooperation of the Indonesia Energy Efficiency and Energy Conservation Society. Also, Fabby Tumiwa is an energy transition strategist and the Executive Director of the Institute for Essential Services Reform (IESR), an Indonesia think tank in energy policy and environment. IESR advocates for the low carbon energy transition in Indonesia. Fabby has been working on energy policy and regulation for more than 20 years, as well as a practitioner of renewable energy. He advises various Indonesian government agencies, businesses, and NGOs, as well as multilateral development organizations in electricity regulation, renewable energy, and energy efficiency policy and regulation, and climate change policy. From 2006 to 2017, he was a member of the Indonesia negotiator on climate change negotiation. Fabby is a 2007 LEAD Fellow and 2009 Eisenhower Fellow. By 2021, Fabby has been appointed as a member of the Sustainable Advisory Council of GoJek Indonesia and serves as an advisory board member of Indonesia Energy Transition Partnership (ETP).  Fabby studied Electronic Engineering at Satya Wacana Christian University.'
							],
							[
								'slug' => 'tony_susandy__st___mba',
								'name' => 'Tony Susandy, S.T., M.BA.',
								'subtitle' => 'Senior Policy Analyst at Directorate General of New, Renewable Energy, and Energy Conservation, Ministry of Energy and Mineral Resources, Republic of Indonesia',
								'pic' => base_url('assets/uploads/' . $this->uri->segment(2) . '/images/isrus_2022/lecturers/tony_susandy__st___mba.jpg'),
								'description' => 'Tony Susandy, S.T, M.BA is a professional in the renewable energy and energy conservation sector. He currently serves as Coordinator of Various New and Renewable Energy Programs, Ministry of Energy and Mineral Resources. He graduated from Master of Energy management in 2009 and is continuing his PhD on Management Science in Decision Making and Strategic Negotiation in 2013 from Institut Teknologi Bandung.'
							],
							[
								'slug' => 'prof__dr__ir__arno_h_m__smets',
								'name' => 'Prof. Dr. Ir. Arno H.M. Smets',
								'subtitle' => 'Professor at Delft University of Technology, Netherlands',
								'pic' => base_url('assets/uploads/' . $this->uri->segment(2) . '/images/isrus_2022/lecturers/prof__dr__ir__arno_h_m__smets.jpg'),
								'description' => 'Prof. Dr. Arno H.M. Smets is Professor in Solar Energy in the Photovoltaics Material and Devices group at the faculty of Electrical Engineering, Mathematics and Computer Science, Delft University of Technology. From 2005-2010 he worked at the Research Center for Photovoltaics at the National Institute of Advanced Industrial Science and Technology (AIST) in Tsukuba, Japan. His research work is focused on processing of thin silicon films, innovative materials and new concepts for photovoltaic applications. He is lecturer for BSc and MSc courses on Photovoltaics and Sustainable Energy at TU Delft. His online edX course on Solar Energy attracted over 150.000 students worldwide. He is co-author of the book "Solar Energy. The physics and engineering of photovoltaic conversion technologies and systems."'
							],
							[
								'slug' => 'muhammad_firmansyah__se',
								'name' => 'Muhammad Firmansyah, SE.',
								'subtitle' => 'CEO of PT. Infiniti Energi Indonesia',
								'pic' => base_url('assets/uploads/' . $this->uri->segment(2) . '/images/isrus_2022/lecturers/muhammad_firmansyah__se.jpg'),
								'description' => 'Muhammad Firmansyah completed his undergraduate education from Padjadjaran University in Bandung in 2015 majoring in International Business Management. He then deepened his knowledge in entrepreneurship. Firman began his career at PT. HM. Sampoerna Tbk in 2015. Initially he got a position as a graduate trainee, then as a Consumer Engagement supervisor. In 2016 he gained a position as Area Supervisor for Multitasking. In 2018, he held the position of Area Analysts and later became Area Manager. In the same year he founded his consulting company named PT. Infiniti Energi Indonesia and became President Director. In addition, he also became a commissioner of CV. Tridaya Cavali. He has gone through various training such as the Future Market and Financial Analyst in 2015, Commercial Business in 2017 and Human Resource Development in 2018. He was very interested in developing lithium ion batteries, biomass, industry and financial research and business plan research so he joined the National Battery Research Institute (NBRI) team as a Vice Leader along with Prof. Dr. rer nat Evvy Kartini, Prof. Dr. Alan J. Drew and other NBRI teams.'
							],
							[
								'slug' => 'adit_tri_wiguno__se',
								'name' => 'Adit Tri Wiguno, SE.',
								'subtitle' => 'PV Consultant of PT. Infiniti Energi Indonesia',
								'pic' => base_url('assets/uploads/' . $this->uri->segment(2) . '/images/isrus_2022/lecturers/adit_tri_wiguno__se.jpg'),
								'description' => 'Adit Tri Wiguno is a PV Consultant of PT. Infiniti Energi Indonesia. He graduated from Prasetiya Mulya University of Business and Economics with a major in Business in 2016. Adit then continued the Level 1 Chartered Financial Analyst (CFA) short course at Bina Nusantara University. Adit\'s early career was as an Industry and Financial Analyst at PT. Noble Capital which started from March 2016 to October 2016. He then continued his work in October 2016 to October 2018 at PT. Bahtera Karimun Sejahtera (Subsidiary of PT. Noble Capital) as Finance. He then continued his work as Corporate Finance at PT. Trimitra Jaya Mineral (Subsidiary of PT. Noble Capital) from October 2018 until now. Adit has gone through various training programs such as How to Find a Super Gain Pearl Stock from the Billy Budiman Course in 2013. In 2015 he did his internship at PT. Dewras Iksasy as Equity Analyst Intern. He then continued his courses at the Sharia Capital Market Schools level 1 and 2 of the Indonesia Stock Exchange in 2016. Since the beginning he was very interested in developing lithium ion batteries, biomass, industry and financial research and business plan research so he joined the National Battery Research Institute (NBRI) team as a Finance Director along with Prof. Dr. rer nat Evvy Kartini, Prof. Dr. Alan J. Drew and other NBRI teams.'
							],
							[
								'slug' => 'dhiko_rosanda__st_',
								'name' => 'Dhiko Rosanda, ST.',
								'subtitle' => 'Senior PV Engineer of PT. Infiniti Energi Indonesia',
								'pic' => base_url('assets/uploads/' . $this->uri->segment(2) . '/images/isrus_2022/lecturers/dhiko_rosanda__st_.jpg'),
								'description' => 'Dhiko Rosanda, ST. is a Senior PV Engineer of PT. Infiniti Energi Indonesia. He has graduated from Institute Technology of PLN in 2021 with bachelor of Electrical Engineering. He also has completed an Internsip in PT. Wedosolar Indonesia as Junior Design Engineering. He is renewable energy enthusiast with analytic and problem-solving mindset. Dhiko also curious on something new and able to gransp new concepts quickly and strong team collaboration skills. As Senior PV Engineer, he masters various software, such as Helioscope, Autocad, Sketch Up, ETAP, PvSyst, etc.'
							],
						],
						'what-you-will-learn' => [
							[
								'name' => 'Solar Energy System Introduction',
								'content' => [
									'Introduction to Renewable Energy',
									'Global value chain and challenge of renewable energy',
									'The potency of solar energy',
									'Introduction to solar energy system'
								]
							],
							[
								'name' => 'Understanding Indonesian Solar PV Policies',
								'content' => [
									'Current status of Indonesian Solar PV',
									'Fundamental regulation',
									'Future projection of Solar PV in Indonesia',
									'Challenges of implementing Solar PV policies'
								]
							],
							[
								'name' => 'The Emerging Solar Energy Technology',
								'content' => [
									'The development of solar energy technology',
									'Challenges of implementing solar energy technology',
									'Current state of solar energy technology',
									'Future prospect of solar energy technology'
								]
							],
							[
								'name' => 'Prospect of Solar PV and business analysis',
								'content' => [
									'Prospects and benefits of installing Solar PV',
									'All about smart solar PV installation',
									'The importance of lithium ion batteries on solar system',
									'Business Analysis'
								]
							],
							[
								'name' => 'All about the preparation of solar PV installation',
								'content' => [
									'Introduction to solar PV installation component',
									'Site analysis',
									'Choosing the best installation component',
									'Software utilities introduction'
								]
							],
							[
								'name' => 'How to Design Solar PV',
								'content' => [
									'How to read a data sheet of any component used the PV solar system',
									'Data sheet analysis',
									'Protection devices',
									'Wiring foundation'
								]
							],
							[
								'name' => 'Understanding Helioscope Software',
								'content' => [
									'Introduction to Helioscope',
									'User interface and its function',
									'Case study',
									'Helioscope Demonstration'
								]
							],
							[
								'name' => 'Sketch Up 101',
								'content' => [
									'Introduction to Sketch Up 101',
									'Tools and Shortcuts',
									'Case study',
									'Sketch Up Demonstration'
								]
							],
						],
						'what-you-will-get' => [
							'Learning Module from the Experts',
							'International E-Certificate (Recognized by Queen Mary University of London)'
						]
					],
					'event_date' => [
						'start_date' => date('Y-m-d H:i:s', strtotime('2022-08-02 00:00:00')),
						'end_date' => date('Y-m-d H:i:s', strtotime('2022-08-03 23:59:00'))
					]
				],
				'nbri-yic_2022' => [
					'description' => 'NBRI Youth Ideas Competition (NBRI-YIC) 2022',
					'package_detail' => [
						'international' => [
							'student' => [
								'Paper_Competition' => [
									'price' => [
										'early_bird' => [
											'amount' => 10,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-05-21 00:00:00'))
										],
										'normal' => [
											'amount' => 10,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Total prize up to IDR 10.000.000',
										'Possibility to publish on Journal of Batteries for Renewable Energy and Electric Vehicles (JBREV) and internship at NBRI'
									],
									'scopes' => [
										'Advanced battery technology from upstream (raw materials), midstream (cell fabrication), and downstream (applications and recycling)',
										'Energy storage technology for renewable energies (solar, wind, biomass, geothermal, etc.)',
										'Electric vehicles ecosystem (battery, electric motor, charging station, etc.)',
										'Other related topics (policy, regulation, standardization, industry, etc.)'
									]
								],
								'Microblog_Competition' => [
									'price' => [
										'early_bird' => [
											'amount' => 5,
											'currency' => 'usd',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-05-21 00:00:00'))
										],
										'normal' => [
											'amount' => 6,
											'currency' => 'usd'
										]
									],
									'benefit' => [
										'Total prize up to IDR 10.000.000',
										'Possibility to publish on Journal of Batteries for Renewable Energy and Electric Vehicles (JBREV) and internship at NBRI'
									],
									'scopes' => [
										'Technology for battery and energy storage (materials, methods, system, etc.)',
										'Role of renewable energy and electric vehicles (potential, importance, etc.)',
										'Progress of decarbonization and energy transition (policy, regulations, standardization, etc.)',
										'Concept of green economy and climate change issue (business, industry, investment, prospective, etc.)',
										'Multidisciplinary studies for the energy sector (artificial intelligence, machine learning, etc.)'
									]
								],
							],
						],
						'indonesian' => [
							'student' => [
								'Paper_Competition' => [
									'price' => [
										'early_bird' => [
											'amount' => 100000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-05-17 00:00:00'))
										],
										'normal' => [
											'amount' => 100000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Total prize up to IDR 10.000.000',
										'Possibility to publish on Journal of Batteries for Renewable Energy and Electric Vehicles (JBREV) and internship at NBRI'
									],
									'scopes' => [
										'Advanced battery technology from upstream (raw materials), midstream (cell fabrication), and downstream (applications and recycling)',
										'Energy storage technology for renewable energies (solar, wind, biomass, geothermal, etc.)',
										'Electric vehicles ecosystem (battery, electric motor, charging station, etc.)',
										'Other related topics (policy, regulation, standardization, industry, etc.)'
									]
								],
								'Microblog_Competition' => [
									'price' => [
										'early_bird' => [
											'amount' => 50000,
											'currency' => 'idr',
											'expired_time' => date('Y-m-d H:i:s', strtotime('2022-05-17 00:00:00'))
										],
										'normal' => [
											'amount' => 60000,
											'currency' => 'idr'
										]
									],
									'benefit' => [
										'Total prize up to IDR 10.000.000',
										'Possibility to publish on Journal of Batteries for Renewable Energy and Electric Vehicles (JBREV) and internship at NBRI'
									],
									'scopes' => [
										'Technology for battery and energy storage (materials, methods, system, etc.)',
										'Role of renewable energy and electric vehicles (potential, importance, etc.)',
										'Progress of decarbonization and energy transition (policy, regulations, standardization, etc.)',
										'Concept of green economy and climate change issue (business, industry, investment, prospective, etc.)',
										'Multidisciplinary studies for the energy sector (artificial intelligence, machine learning, etc.)'
									]
								],
							],
						],
					],
					'event_detail' => [
						'lecturers' => [
							[
								'name' => 'Fabby Tumiwa',
								'pic' => 'fabby_tumiwa.jpg',
								'title' => 'Chairman of Indonesia Solar Energy Association (ISEA) and Executive Director of Institute for Essential Services Reform (IESR)',
								'description' => 'Fabby Tumiwa is one of the founders of the Indonesia Solar Energy Association (ISEA) in 2016, then serves as vice Chairman, and since 2021 he has been leading the association as the Chairman. He was on the executive committee on institutional cooperation of the Indonesia Energy Efficiency and Energy Conservation Society. Also, Fabby Tumiwa is an energy transition strategist and the Executive Director of the Institute for Essential Services Reform (IESR), an Indonesia think tank in energy policy and environment. IESR advocates for the low carbon energy transition in Indonesia. Fabby has been working on energy policy and regulation for more than 20 years, as well as a practitioner of renewable energy. He advises various Indonesian government agencies, businesses, and NGOs, as well as multilateral development organizations in electricity regulation, renewable energy, and energy efficiency policy and regulation, and climate change policy. From 2006 to 2017, he was a member of the Indonesia negotiator on climate change negotiation. Fabby is a 2007 LEAD Fellow and 2009 Eisenhower Fellow. By 2021, Fabby has been appointed as a member of the Sustainable Advisory Council of GoJek Indonesia and serves as an advisory board member of Indonesia Energy Transition Partnership (ETP).  Fabby studied Electronic Engineering at Satya Wacana Christian University.'
							],
							[
								'name' => 'Tony Susandy, S.T., M.BA.',
								'pic' => 'tony_susandy,_st_,_mba.jpg',
								'title' => 'Senior Policy Analyst at Directorate General of New, Renewable Energy, and Energy Conservation, Ministry of Energy and Mineral Resources, Republic of Indonesia',
								'description' => 'Tony Susandy, S.T, M.BA is a professional in the renewable energy and energy conservation sector. He currently serves as Coordinator of Various New and Renewable Energy Programs, Ministry of Energy and Mineral Resources. He graduated from Master of Energy management in 2009 and is continuing his PhD on Management Science in Decision Making and Strategic Negotiation in 2013 from Institut Teknologi Bandung.'
							],
							[
								'name' => 'Prof. Dr. Ir. Arno H.M. Smets',
								'pic' => 'prof__dr__ir__arno_h_m__smets.jpg',
								'title' => 'Professor at Delft University of Technology, Netherlands',
								'description' => 'Prof. Dr. Arno H.M. Smets is Professor in Solar Energy in the Photovoltaics Material and Devices group at the faculty of Electrical Engineering, Mathematics and Computer Science, Delft University of Technology. From 2005-2010 he worked at the Research Center for Photovoltaics at the National Institute of Advanced Industrial Science and Technology (AIST) in Tsukuba, Japan. His research work is focused on processing of thin silicon films, innovative materials and new concepts for photovoltaic applications. He is lecturer for BSc and MSc courses on Photovoltaics and Sustainable Energy at TU Delft. His online edX course on Solar Energy attracted over 150.000 students worldwide. He is co-author of the book "Solar Energy. The physics and engineering of photovoltaic conversion technologies and systems."'
							],
							[
								'name' => 'Muhammad Firmansyah, SE.',
								'pic' => 'muhammad_firmansyah,_se.jpg',
								'title' => 'CEO of PT. Infiniti Energi Indonesia',
								'description' => 'Muhammad Firmansyah completed his undergraduate education from Padjadjaran University in Bandung in 2015 majoring in International Business Management. He then deepened his knowledge in entrepreneurship. Firman began his career at PT. HM. Sampoerna Tbk in 2015. Initially he got a position as a graduate trainee, then as a Consumer Engagement supervisor. In 2016 he gained a position as Area Supervisor for Multitasking. In 2018, he held the position of Area Analysts and later became Area Manager. In the same year he founded his consulting company named PT. Infiniti Energi Indonesia and became President Director. In addition, he also became a commissioner of CV. Tridaya Cavali. He has gone through various training such as the Future Market and Financial Analyst in 2015, Commercial Business in 2017 and Human Resource Development in 2018. He was very interested in developing lithium ion batteries, biomass, industry and financial research and business plan research so he joined the National Battery Research Institute (NBRI) team as a Vice Leader along with Prof. Dr. rer nat Evvy Kartini, Prof. Dr. Alan J. Drew and other NBRI teams.'
							],
							[
								'name' => 'Adit Tri Wiguno, SE.',
								'pic' => 'adit_tri_wiguno,_se.jpg',
								'title' => 'PV Consultant of PT. Infiniti Energi Indonesia',
								'description' => 'Adit Tri Wiguno is a PV Consultant of PT. Infiniti Energi Indonesia. He graduated from Prasetiya Mulya University of Business and Economics with a major in Business in 2016. Adit then continued the Level 1 Chartered Financial Analyst (CFA) short course at Bina Nusantara University. Adit\'s early career was as an Industry and Financial Analyst at PT. Noble Capital which started from March 2016 to October 2016. He then continued his work in October 2016 to October 2018 at PT. Bahtera Karimun Sejahtera (Subsidiary of PT. Noble Capital) as Finance. He then continued his work as Corporate Finance at PT. Trimitra Jaya Mineral (Subsidiary of PT. Noble Capital) from October 2018 until now. Adit has gone through various training programs such as How to Find a Super Gain Pearl Stock from the Billy Budiman Course in 2013. In 2015 he did his internship at PT. Dewras Iksasy as Equity Analyst Intern. He then continued his courses at the Sharia Capital Market Schools level 1 and 2 of the Indonesia Stock Exchange in 2016. Since the beginning he was very interested in developing lithium ion batteries, biomass, industry and financial research and business plan research so he joined the National Battery Research Institute (NBRI) team as a Finance Director along with Prof. Dr. rer nat Evvy Kartini, Prof. Dr. Alan J. Drew and other NBRI teams.'
							],
							[
								'name' => 'Dhiko Rosanda, ST.',
								'pic' => 'dhiko_rosanda,_st_.jpg',
								'title' => 'Senior PV Engineer of PT. Infiniti Energi Indonesia',
								'description' => 'Dhiko Rosanda, ST. is a Senior PV Engineer of PT. Infiniti Energi Indonesia. He has graduated from Institute Technology of PLN in 2021 with bachelor of Electrical Engineering. He also has completed an Internsip in PT. Wedosolar Indonesia as Junior Design Engineering. He is renewable energy enthusiast with analytic and problem-solving mindset. Dhiko also curious on something new and able to gransp new concepts quickly and strong team collaboration skills. As Senior PV Engineer, he masters various software, such as Helioscope, Autocad, Sketch Up, ETAP, PvSyst, etc.'
							],
						],
						'what-you-will-learn' => [
							[
								'name' => 'Solar Energy System Introduction',
								'content' => [
									'Introduction to Renewable Energy',
									'Global value chain and challenge of renewable energy',
									'The potency of solar energy',
									'Introduction to solar energy system'
								]
							],
							[
								'name' => 'Understanding Indonesian Solar PV Policies',
								'content' => [
									'Current status of Indonesian Solar PV',
									'Fundamental regulation',
									'Future projection of Solar PV in Indonesia',
									'Challenges of implementing Solar PV policies'
								]
							],
							[
								'name' => 'The Emerging Solar Energy Technology',
								'content' => [
									'The development of solar energy technology',
									'Challenges of implementing solar energy technology',
									'Current state of solar energy technology',
									'Future prospect of solar energy technology'
								]
							],
							[
								'name' => 'Prospect of Solar PV and business analysis',
								'content' => [
									'Prospects and benefits of installing Solar PV',
									'All about smart solar PV installation',
									'The importance of lithium ion batteries on solar system',
									'Business Analysis'
								]
							],
							[
								'name' => 'All about the preparation of solar PV installation',
								'content' => [
									'Introduction to solar PV installation component',
									'Site analysis',
									'Choosing the best installation component',
									'Software utilities introduction'
								]
							],
							[
								'name' => 'How to Design Solar PV',
								'content' => [
									'How to read a data sheet of any component used the PV solar system',
									'Data sheet analysis',
									'Protection devices',
									'Wiring foundation'
								]
							],
							[
								'name' => 'Understanding Helioscope Software',
								'content' => [
									'Introduction to Helioscope',
									'User interface and its function',
									'Case study',
									'Helioscope Demonstration'
								]
							],
							[
								'name' => 'Sketch Up 101',
								'content' => [
									'Introduction to Sketch Up 101',
									'Tools and Shortcuts',
									'Case study',
									'Sketch Up Demonstration'
								]
							],
						],
						'what-you-will-get' => [
							'Learning Module from the Experts',
							'International E-Certificate (Recognized by Queen Mary University of London)'
						],
						'scopes' => [
							'Paper_Competition' => [
								'Advanced battery technology from upstream (raw materials), midstream (cell fabrication), and downstream (applications and recycling)',
								'Energy storage technology for renewable energies (solar, wind, biomass, geothermal, etc.)',
								'Electric vehicles ecosystem (battery, electric motor, charging station, etc.)',
								'Other related topics (policy, regulation, standardization, industry, etc.)'
							], 'Microblog_Competition' => [
								'Technology for battery and energy storage (materials, methods, system, etc.)',
								'Role of renewable energy and electric vehicles (potential, importance, etc.)',
								'Progress of decarbonization and energy transition (policy, regulations, standardization, etc.)',
								'Concept of green economy and climate change issue (business, industry, investment, prospective, etc.)',
								'Multidisciplinary studies for the energy sector (artificial intelligence, machine learning, etc.)'
							]
						]
					],
					'event_date' => [
						'start_date' => date('Y-m-d H:i:s', strtotime('2022-06-13 00:00:00')),
						'end_date' => date('Y-m-d H:i:s', strtotime('2022-06-13 00:00:00'))
					]
				]
			]
		];
	}
}
