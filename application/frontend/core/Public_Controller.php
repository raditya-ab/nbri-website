<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Public_Controller extends MY_Controller {
	protected $data;

	public function __construct()
	{
		parent::__construct();

        $website_setting = get_website_setting();

        if($website_setting == NULL || !$website_setting){
            $website_setting = $website_setting = set_website_variables();

        }
		if(is_null($this->session->userdata('website_var'))){
			$website_setting = set_website_variables();
			$this->data['website_var'] = array(
				'website_name' => $website_setting['website_var']['website_name'],
				'address' => $website_setting['website_var']['address'],
				'lat' => $website_setting['website_var']['lat'],
				'lng' => $website_setting['website_var']['lng'],
				'email' => $website_setting['website_var']['email'],
				'phone' => $website_setting['website_var']['phone'],
				'fax' => $website_setting['website_var']['fax'],
				'facebook_url' => $website_setting['website_var']['facebook_url'],
				'instagram_url' => $website_setting['website_var']['instagram_url'],
				'google_url' => $website_setting['website_var']['google_url'],
				'twitter_url' => $website_setting['website_var']['twitter_url'],
				'youtube_url' => $website_setting['website_var']['youtube_url'],
				'biglogo' => $website_setting['website_var']['biglogo'],
				'logo' => $website_setting['website_var']['logo'],
				'favicon' => $website_setting['website_var']['favicon']
			);
		} else {
			$this->data['website_var'] = $this->session->userdata('website_var');;
		}

		// $this->data['current_segment'] = $this->uri->segment(1);

		// // $this->template->set_partial('logo','partials/logo');

		// // $this->template->set_partial('global_js','partials/global_js');
		// $this->template->set_partial('scripts','partials/scripts');
		// $this->template->set_partial('topbar','partials/topbar');
		// $this->template->set_partial('navbar','partials/navbar');
		// $this->template->set_partial('slider','partials/slider',$this->data);
		// $this->template->set_partial('footer','partials/footer');
		// $this->template->set_layout('skeleton');
		// $this->template->set_theme('frontend');
	}

	public function send_email(Array $param = []){
        $message = (isset($param['message'])) ? $param['message']:'';
        $destination = (isset($param['destination'])) ? $param['destination']:'admin@n-bri.org';
        $subject = (isset($param['subject'])) ? $param['subject']:'Greetings from NBRI';

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'smtp_user'=> ICB-REV2021_EMAIL,
            'smtp_pass'=> ICB-REV2021_EMAILPASS,
            'mailtype' => 'html',
            'charset' => 'utf8',
            'priority' => 2,
            'wordwrap' => TRUE,
            'newline' => "\r\n"
        );

        $this->load->library('email', $config);
        $this->email->from(ICB-REV2021_EMAIL, 'ICB-REV 2021 NBRI');
        $this->email->to($destination);
        $this->email->bcc(ADMIN-NBRI_EMAIL);
        $this->email->subject($subject);
        $this->email->message($message);

        return $this->email->send();
    }
}

/* End of file Public_controller.php */
/* Location: ./application/core/Public_controller.php */
