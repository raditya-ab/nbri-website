<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

	public function checkIsSlugAvailable($slug, $table = 'articles', $id = NULL, $field){
        if(!is_null($field)) {
            $this->db->where($field, $slug);
        } else {
    		$this->db->where('slug', $slug);
        }

		if(!is_null($id)){
			$this->db->where('id != ', $id);
		}

		$result = $this->db->count_all_results($table);

		return ($result > 0) ? FALSE : TRUE;
	}

	public function setting_been_set()
	{
		return ($this->db->count_all('website_setting') > 0) ? TRUE : FALSE;
	}

	public function get_website_setting()
	{
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);
		return $this->db->get('website_setting')->row_array();
	}


}

/* End of file Common_m.php */
/* Location: ./application/backend/models/Common_m.php */