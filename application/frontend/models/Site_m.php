<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_m extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_research_menu()
    {
        $this->db->select('category_name as name,slug');
        $this->db->from('research_category');
        $this->db->where('status', '1');

        return $this->db->get()->result_array();
    }

}

/* End of file Site_m.php */
/* Location: ./application/frontend/models/Site_m.php */