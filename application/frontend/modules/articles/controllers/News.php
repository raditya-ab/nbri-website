<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends Public_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('articles_m');
        $this->template->set_layout('default_template');
    }

    public function index()
    {
        $this->data['news_list'] = $this->articles_m->get_articles(1);
        $this->data['featured_news'] = $this->articles_m->get_featured_articles(1);
        $this->data['page_js'] = array(
            array(
                'url' => '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js'
            )
        );

        $this->template->set('page_title', 'News');
        $this->template->set('page_banner', base_url('assets/images/news_banner.jpg'));
        $this->template->build('news_index', $this->data);
    }

    public function detail($slug)
    {
        $this->load->model('author_m');
        $this->data['news_detail'] = $this->articles_m->get_article_detail($slug,1);
        $this->data['author_detail'] = $this->author_m->get_author_profile($this->data['news_detail']['author']);
        $this->data['news_list'] = $this->articles_m->get_related_articles($slug,$this->data['news_detail']['category']);
        $this->template->build('news_detail', $this->data);
    }

    public function articlesdetail($category_slug, $news_slug)
    {

        $this->load->model('news/news_m','news_m');

        if(!$this->news_m->news_is_exist($category_slug, $news_slug)){
            show_404();
        }

        $this->data['news_detail'] = $this->news_m->get_detail($category_slug,$news_slug);

        if(is_null($this->data['news_detail']['img']) ||empty($this->data['news_detail']['img'])){
          $this->data['news_detail']['bgimg'] = base_url('assets/images/'.$this->session->userdata('website_var')['logo']);
        } else {
          $this->data['news_detail']['bgimg'] = base_url('assets/images/article/'.$this->data['news_detail']['img']);
        }

        // $this->data['slider'] = [
        //  'title' => $this->data['news_detail']['title'],
        //  'background' => base_url('assets/images/article/'.$this->data['news_detail']['bgimg'])
        // ];

        $this->template->title($this->data['news_detail']['title'],'Articles');
        $this->template->set('page_subtitle',$this->data['news_detail']['category_name']);

        $this->template->build('articles_detail', $this->data);
    }

}

/* End of file Articles.php */
/* Location: ./application/frontend/modules/articles/controllers/Articles.php */