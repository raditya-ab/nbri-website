<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articles_m extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_id_from_slug($slug){
        return $this->db->select('id')
                ->where('slug',$slug)
                ->limit(1)
                ->get('articles')->row_array();
    }
    public function get_articles($category = NULL,$subcategory_value = NULL,$excluded_category = 5)
    {
        $subcategories = '';
        // if($subcategory_value == NULL){
        //     $subcategory_list = $this->get_subcategories();
    
    
        //     if(count($subcategory_list) > 0){
        //         $a = array_keys($subcategory_list);
        //         foreach($a as $key => $subcategory){
        //             $subcategories .= "'$subcategory'";
        //             if($subcategory != end($a)){
        //                 $subcategories .= ',';
        //             }
        //         }
        //         // $subcategories .= 'NULL';
        //     }
        // }

        $select = array(
            'a.*',
            'c.slug as category_slug',
        );

        $this->db->join('categories c', 'a.category = c.id', 'left');
        // $this->db->join('subcategories s', 'a.subcategory = s.id', 'left');
        $this->db->from('articles a');

        if(!empty($subcategories)){
            $this->db->where("a.id IN($subcategories)", NULL, FALSE);
        }

        if($category != NULL && !empty($category)){
            $this->db->where('a.category', $category);
        }
        
        if($excluded_category != NULL && !empty($excluded_category)){
            $this->db->where("a.category <> $excluded_category", NULL);
        }

        if($subcategory_value != NULL && !empty($subcategory_value)){
            $select[] = 'b.slug as subcategory_slug';
            $select[] = 'b.title as subcategory_title';
            $this->db->join('articles b','a.subcategory = b.id');

            $this->db->where('a.subcategory', $subcategory_value);
        } else {
            $this->db->where("a.subcategory IS NULL");
        }

        $this->db->select($select);

        $this->db->order_by('a.modified', 'desc');
        $this->db->order_by('a.created', 'desc');

        return $this->db->get()->result_array();
    }

    public function get_featured_articles($category = NULL)
    {
        $select = array(
            'a.*',
            'c.slug as category_slug'
        );

        $this->db->select($select);
        $this->db->join('categories c', 'a.category = c.id', 'left');

        if($category != NULL && !empty($category)){
            $this->db->where('a.category', $category);
        }

        $this->db->where('a.featured', '1');

        $this->db->order_by('modified', 'desc');
        $this->db->order_by('created', 'desc');

        return $this->db->get('articles a')->result_array();
    }

    public function get_related_articles($slug,$category)
    {
        $select = array(
            'a.*',
            'c.slug as category_slug'
        );

        $this->db->select($select);
        $this->db->join('categories c', 'a.category = c.id', 'left');
        $this->db->where('a.category', $category);
        $this->db->where('a.slug !=',$slug);
        $this->db->limit(3);
        $this->db->order_by('created', 'desc');

        return $this->db->get('articles a')->result_array();
    }

    public function get_article_detail($slug, $category)
    {
        $select = array(
            'a.*',
            'c.category_name',
            'c.slug as category_slug',
        );

        $this->db->select($select);
        $this->db->from('articles a');
        $this->db->join('categories c', 'a.category = c.id', 'left');

        $this->db->where('a.slug', $slug);
        $this->db->where('a.category', $category);
        $this->db->limit(1);

        return $this->db->get('articles')->row_array();
    }

    public function get_subcategories()
    {
        $select = array(
            'DISTINCT(a.subcategory)',
            'a.slug as subcategory_slug'
        );

        $this->db->select($select);
        $this->db->from('articles a');
        $this->db->where('a.subcategory IS NOT NULL');

        $query = "SELECT a.id as subcategory_id, a.slug as subcategory_slug FROM articles a WHERE id IN (SELECT DISTINCT(b.subcategory) FROM articles b WHERE b.subcategory IS NOT NULL)";
        $result = $this->db->query($query)->result_array();
        $this->db->reset_query();

        $subcategory_list = array();
        if(count($result) > 0){
            foreach($result as $subcategory){
                $subcategory_list[$subcategory['subcategory_id']] = $subcategory['subcategory_slug'];
            }
        }

        return $subcategory_list;
    }

}

/* End of file Articles_m.php */
/* Location: ./application/frontend/modules/articles/models/Articles_m.php */