<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Author_m extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_author_profile($user_id)
    {
        $select = array(
            'u.id',
            'u.username',
            'up.first_name',
            'up.last_name',
            'up.img_profile',
            'up.bio'
        );
        $this->db->select($select);
        $this->db->from('user_profiles up');
        $this->db->join('users u', 'up.user_id = u.id', 'left');

        $this->db->where('u.id', $user_id);

        return $this->db->get()->row_array();
    }

}

/* End of file Author_m.php */
/* Location: ./application/frontend/modules/articles/models/Author_m.php */