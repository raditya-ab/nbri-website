<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(<?=$page_banner;?>), url(<?=base_url('assets/images/nbri_new_logo.png');?>">
                <h1 class="page-title"><?=$page_title;?></h1>
            </div>
        </div>
    </div>
</section>
<section id="page-subtitle" class="bg-green">
    <div class="container">
        <div class="row">
            <div class="col-md-1 text-center d-flex align-items-center">
                <button class="btn btn-transparent" type="button" id="btn-text-slider-left">
                    <i class="fas fa-chevron-left fa-3x"></i>
                </button>
            </div>
            <div class="col-md-10 d-flex justify-content-center">
                <div class="text-slide-wrapper" id="quotes_slider" style="max-width: 844px">
                    <div class="text-slide-item text-center">
                        <p style="font-family: 'Sailec Medium'; font-size: 20px">"An investment in knowledge pays the best interest."
                            <br>
                            <br>
                        - Benjamin Franklin -</p>
                    </div>
                    <div class="text-slide-item text-center">
                        <p style="font-family: 'Sailec Medium'; font-size: 20px">"Research is to see what everybody else has seen and to think what nobody else has thought."
                            <br>
                            <br>
                        - Albert Szent – Gyorgyi -</p>
                    </div>
                    <div class="text-slide-item text-center">
                        <p style="font-family: 'Sailec Medium'; font-size: 20px">"No research without action, no action without research."
                            <br>
                            <br>
                        - Kurt Lewin -</p>
                    </div>
                    <div class="text-slide-item text-center">
                        <p style="font-family: 'Sailec Medium'; font-size: 20px">"Electric power is everywhere present in unlimited quantities and can drive the world's machinery without the need of coal, oil, gas or any other of the common fuel."
                            <br>
                            <br>
                        - Nikola Tesla -</p>
                    </div>
                    <div class="text-slide-item text-center">
                        <p style="font-family: 'Sailec Medium'; font-size: 20px">"Science knows no country because knowledge belongs to humanity and is the torch which illuminates the world."
                            <br>
                            <br>
                        - Louis Pasteur -</p>
                    </div>
                </div>

            </div>
            <div class="col-md-1 text-center d-flex align-items-center">
                <button class="btn btn-transparent" type="button" id="btn-text-slider-right">
                    <i class="fas fa-chevron-right fa-3x"></i>
                </button>
            </div>
        </div>
    </div>
</section>

<?php if(!empty($news_list)): ?>
    <section class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center font-color-navy" sty>
                    <button class="btn btn-primary btn-lg" id="btn-expert-prev" style="position: absolute;top: 231px;left: -40px;z-index: 1; color: #181d62;background-color: transparent;border: none"><i class="fas fa-2x fa-chevron-left"></i></button>
                    <div id="article-featured-slider">
                        <?php foreach($featured_news as $slider): ?>
                            <?php
                                $content = strip_tags(stripslashes(html_entity_decode($slider['content'])));
                                $img = ($slider['img'] != NULL && $slider['img'] != '') ? base_url('assets/images/articles/'.$slider['img']) : base_url('assets/images/nbri_new_logo.png');
                            ?>
                            <div class="featured-slider-item">
                                <div class="img-featured" style="background-image: url(<?=$img;?>), url(<?=base_url('assets/images/nbri_new_logo.png');?>"></div>
                                <h3 class="section-title font-bold font-24 mb-3"><?=word_limiter($slider['title'],7);?></h3>
                                <p class="section-description "><?=word_limiter($content,30);?></p>
                                <a href="<?=site_url($slider['category_slug'].'/'.$slider['slug']);?>" class="btn btn-readmore btn-navy mt-3">Read More</a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <button class="btn btn-primary btn-lg" id="btn-expert-next" style="position: absolute;top: 231px;right: -40px;z-index: 1; color: #181d62;background-color: transparent;border: none"><i class="fas fa-2x fa-chevron-right"></i></button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-wrapper">
                    <?php foreach($news_list as $news): ?>
                        <?php
                            $content = strip_tags(stripslashes(html_entity_decode($news['content'])));
                            $img = ($news['img'] != NULL && $news['img'] != '') ? base_url('assets/images/articles/'.$news['img']) : base_url('assets/images/nbri_new_logo.png');
                        ?>
                        <div class="grid-article-wrapper">
                            <div class="img-wrapper" style="background-image: url(<?=$img;?>)"></div>
                            <div class="caption-wrapper text-left">
                                <h4 class="article-title"><?=word_limiter($news['title'],7);?></h4>
                                <p class="article-description"><?=word_limiter($content,15);?></p>
                            </div>
                            <div class="readmore-wrapper">
                                <a href="<?=site_url($news['category_slug'].'/'.$news['slug']);?>" class="readmore" title="">
                                    <span>Read More</span>
                                    <i class="fas fa-arrow-right"></i>
                                </a>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<?php else: ?>
    <section class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center font-color-navy">
                    <h3 class="section-title">
                        There's no content yet
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-wrapper">
                    <?php foreach($news_list as $news): ?>
                        <?php
                            $content = strip_tags(stripslashes(html_entity_decode($news['content'])));
                            $img = ($news['img'] != NULL && $news['img'] != '') ? base_url('assets/images/articles/'.$news['img']) : base_url('assets/images/nbri_new_logo.png');
                        ?>
                        <div class="grid-article-wrapper">
                            <div class="img-wrapper" style="background-image: url(<?=$img;?>)"></div>
                            <div class="caption-wrapper text-left">
                                <h4 class="article-title"><?=word_limiter($news['title'],7);?></h4>
                                <p class="article-description"><?=word_limiter($content,15);?></p>
                            </div>
                            <div class="readmore-wrapper">
                                <a href="<?=site_url($news['category_slug'].'/'.$news['slug']);?>" class="readmore" title="">
                                    <span>Read More</span>
                                    <i class="fas fa-arrow-right"></i>
                                </a>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>