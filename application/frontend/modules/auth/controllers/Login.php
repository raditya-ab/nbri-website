<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_layout('default_template');
    }

    public function index()
    {
        $this->load->view('auth/auth/login_form');
    }
}

/* End of file Login.php */
/* Location: ./application/frontend/modules/auth/controllers/Login.php */