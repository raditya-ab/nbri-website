<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_layout('default_template');
    }

    public function index()
    {
        $this->template->build('auth/auth/register', $this->data);
    }

}

/* End of file Register.php */
/* Location: ./application/frontend/modules/auth/controllers/Register.php */