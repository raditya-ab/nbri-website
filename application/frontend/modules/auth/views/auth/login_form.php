<div class="column one-third column_column" style="margin-top: 20px">
    <div class="login-box" style="border: 1px solid #DDD; padding: 20px;">
        <h4><b>Login</b></h4><hr>
        <div class="form-group">
            <label for="" class="control-label">Username</label>
            <input type="text" class="form-control" name="">
        </div>
        <div class="form-group">
            <label for="" class="control-label">Password</label>
            <input type="text" class="form-control" name="">
        </div>
        <div class="items_group clearfix">
            <div class="column one-second" style="margin-bottom: 0">
                Didn't have an account?<br>
                <a href="<?=site_url('register');?>" title="Register">Register</a>
            </div>
            <div class="column one-second" style="text-align: right; margin-bottom: 0">
                <button type="button" class="btn btn-default">Login</button>
            </div>
        </div>
    </div>
</div>