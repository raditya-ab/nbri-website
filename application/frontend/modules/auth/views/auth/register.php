<div class="section equal-height " style="padding-top:90px; padding-bottom:50px; ">
    <div class="section_wrapper clearfix">
        <div class="items_group clearfix">
            <div class="column two-third column_column">
                <h3>Join NBRI</h3><hr>
                <h5>Please fill your data correctly. We will send you an activation account to your email.</h5>
                <div class="items_group clearfix" style="margin-top: 50px">
                    <div class="column one-second column_column ">
                        <div class="form-group">
                            <label class="control-label required">Salutation</label>
                            <select class="form-control" required>
                                <option value="">Select Salutation</option>
                                <option value="Prof.">Prof.</option>
                                <option value="Dr.">Dr.</option>
                                <option value="Mr.">Mr.</option>
                                <option value="Mrs.">Mrs.</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label required">First Name</label>
                            <input type="text" class="form-control" name="" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Last Name</label>
                            <input type="text" class="form-control" name="">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label required">Email</label>
                            <input type="text" class="form-control" name="" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Phone</label>
                            <input type="text" class="form-control" name="">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label required">Institution</label>
                            <input type="text" class="form-control" name="" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Address</label>
                            <textarea name="" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Research Focus Area</label>
                            <span><small><i>(Please separate with a comma if more than one)</i></small></span>
                            <textarea name="" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Expertise</label>
                            <input type="text" class="form-control" name="">
                        </div>
                    </div>

                    <div class="column one-second column_column ">
                        <div class="form-group">
                            <label for="" class="control-label required">Username</label>
                            <span><small><i>Only lowercase letters, numbers, and hyphens/underscores.</i></small></span>
                            <input type="text" class="form-control" name="" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label required">Password</label>
                            <span><small><i>At least 6 characters, combination of number, lowercase and uppercase characters</i></small></span>
                            <input type="text" class="form-control" name="" required>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label required">Password Confirmation</label>
                            <input type="text" class="form-control" name="" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label required">Category</label>
                            <select class="form-control" required>
                                <option value="">Select Category</option>
                                <option value="Prof.">International</option>
                                <option value="Prof.">Local</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="">
                                    By registering, I Agree to the <a href=""><u>Terms and conditions</u></a>
                                </label>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary g-recaptcha" data-sitekey="6LfGoLkZAAAAALzwkjzyC99qGxTyAFGcOHw04CIF" style="display: inline-block">Submit</button>
                    </div>
                </div>
            </div>
            <?php echo modules::run('auth/login/index'); ?>
        </div>
    </div>
</div>

</div>

