<?php

$route['event/icb-rev-2021'] = 'icb-rev-2021/homepage';

// Compabilty
$route['event/icb-rev-2021/conference'] = 'icb-rev-2021/agenda/conference';
$route['event/icb-rev-2021/school'] = 'icb-rev-2021/agenda/school';
$route['event/icb-rev-2021/exhibition'] = 'icb-rev-2021/agenda/exhibition';
$route['event/icb-rev-2021/program-book'] = 'icb-rev-2021/program_book';

$route['event/icb-rev-2022'] = 'icb-rev-2022/homepage/index/icb-rev-2022';

// Compabilty
$route['event/icb-rev-2022/conference'] = 'icb-rev-2022/agenda/conference';
$route['event/icb-rev-2022/agenda/ibs-2022'] = 'icb-rev-2022/agenda/ibs';
$route['event/icb-rev-2022/agenda/nbri-yic-2022'] = 'icb-rev-2022/agenda/nbri_yic';
$route['event/icb-rev-2022/agenda/isrus-2022'] = 'icb-rev-2022/agenda/isrus';
$route['event/icb-rev-2022/program-book'] = 'icb-rev-2022/program_book';
$route['event/icb-rev-2022/registration/step1'] = 'icb-rev-2022/registration/index';
$route['event/icb-rev-2022/registration/finish'] = 'icb-rev-2022/registration/finish';
$route['event/icb-rev-2022/registration/finish/(:any)'] = 'icb-rev-2022/registration/finish/$1';