<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends Public_Controller {
    public function __construct()
    {
        parent::__construct();
    }
    public function send_mail()
    {
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_crypto'=>'ssl',
            'smtp_user'=>'icb-rev2021@n-bri.org',
            'smtp_pass'=>'Nbri2021',
            'mailtype' => 'text',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->from('icb-rev2021@n-bri.org', 'ICB REV 2021 NBRI');
        $this->email->to('wahyusyafiul@gmail.com');
        $this->email->cc('admin@n-bri.org');
        $this->email->subject('Testing');
        $this->email->message('Testingsdsdssdsds');
        $this->email->send();

    }

    public function index($id,$slug,$pageslug = 'climate-challenge-workshop')
    {
        $this->data['news_list'] = $this->event_m->get_articles($id);
        $this->data['banner_details'] = array(
            'img' => $pageslug.'.jpg',
            'caption' => ucwords(preg_replace('/\-/i', ' ', $pageslug)),
            'subcaption' => array(
                array(
                    'text' => 'Delivering a battery revolution - reducing the drivers of climate change in Indonesia'
                ),
                array(
                    'text' => '12 - 14<sup>th</sup> July 2021'
                ),
                array(
                    'text' => 'Total Prize &#163; 30.000',
                    'font-size' => '24px'
                ),
            )
        );

        $this->template->set_partial('left_sidebar','partials/event_partial/left_sidebar', $this->data);
        $this->template->set_partial('right_sidebar','partials/event_partial/right_sidebar');
        $this->template->set_partial('banner_hero','partials/banner_hero', $this->data);


        $this->template->set_layout('event_template_3-grid');


        $this->template->build($slug.'/index', $this->data);

    }

    public function pages()
    {

    }


}

/* End of file Event.php */
/* Location: ./application/modules/event/controllers/Event.php */