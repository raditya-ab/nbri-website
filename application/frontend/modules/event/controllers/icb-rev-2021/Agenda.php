<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Agenda extends Main_event_controller
{
    protected $folder_path;

    public function __construct()
    {
        parent::__construct();
        $this->data['event_date'] = date('Y-m-d H:i:s', strtotime('2022-06-21 00:00:00'));
    }

    public function index()
    {
        $this->data['banner_title'] = 'Agenda';
        $this->_build('agenda');
    }

    public function conference()
    {
        $this->data['banner_title'] = 'Conference';

        $this->_build('conference');
    }

    public function school()
    {
        $this->data['banner_title'] = 'School';

        $this->_build('school');
    }

    public function sponsors()
    {
        $this->data['banner_title'] = 'Platinum Sponsors';

        $this->_build('sponsors');
    }
}
