<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Program_book extends Main_event_controller
{
    protected $folder_path;

    public function __construct()
    {
        parent::__construct();
        $this->data['event_date'] = date('Y-m-d H:i:s', strtotime('2022-06-21 00:00:00'));
    }

    public function index()
    {
        $this->data['banner_title'] = 'Program Book';

        $this->_build('program-book');
    }
}
