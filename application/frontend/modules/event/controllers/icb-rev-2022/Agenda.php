<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends Main_event_controller
{
    protected $folder_path;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['banner_title'] = 'Agenda';
		$this->data['page_banner'] = '2__header_page_agenda.jpg';
        $this->_build('agenda');
    }

    public function conference()
    {
        $this->data['banner_title'] = 'ICB-REV 2022';
	    $this->data['page_banner'] = '2_1_header_subpage_icb-rev_2022.jpg';
        $this->_build('conference');
    }

    public function ibs()
    {
        $this->data['banner_title'] = 'International Battery School (IBS)';
        $this->data['banner_subtitle'] = 'Battery Technology from Upstream to Downstream';
	    $this->data['page_banner'] = '2_2_header_subpage_ibs_2022.jpg';
        $this->_build('school');
    }

    public function nbri_yic()
    {
        $this->data['banner_title'] = 'NBRI Youth Ideas Competition (NBRI-YIC)';
		$this->data['banner_subtitle'] = 'NBRI Youth Ideas Competition 2022';
	    $this->data['page_banner'] = '2_4_header_subpage_nbri-yic_2022.jpg';
        $this->_build('nbri_yic');
    }
	public function isrus()
	{
		$this->data['banner_title'] = 'International Workshop on Solar Rooftop Residential and Utilities Scale';
        $this->data['banner_subtitle'] = 'Accelerating Indonesia Clean Energy Transition through Rooftop Solar Photovoltaic';
		$this->data['page_banner'] = '2_3_header_subpage_isrus_2022.jpg';
		$this->_build('isrus');
	}
}