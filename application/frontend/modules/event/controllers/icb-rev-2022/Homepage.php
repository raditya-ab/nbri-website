<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends Main_event_controller {

    public $member_data;

    public function __construct()
    {
        parent::__construct();
    }

    public function index($pageslug = 'icb-rev-2021')
    {
        $this->data['banner_details'] = array(
            'img' => $pageslug.'.jpg',
            'title' => 'The International Conference on  Battery <br>for Renewable Energy and Electric Vehicles 2022',
            'subtitle' => 'Accelerating global energy transition agenda through revolutionary battery technology, renewable energy and electric vehicles',
            'caption' => ucwords(preg_replace('/\-/i', ' ', $pageslug)),
            'subcaption' => array(
                array(
                    'text' => 'Delivering a battery revolution - reducing the drivers of climate change in Indonesia'
                ),
                array(
                    'text' => '12 - 14<sup>th</sup> July 2021'
                ),
                array(
                    'text' => 'Total Prize &#163; 30.000',
                    'font-size' => '24px'
                ),
            ),
            'event_date' => 'Conducting Online. 21st - 23rd, June 2022'
        );

        $this->template->set_partial('banner_hero','partials/event_partial/banner_hero_fullheight', $this->data);
        $this->template->set_partial('navbar','partials/event_partial/'.$pageslug.'/navbar', $this->data);


        $this->template->set_layout('default_template_event');


        $this->template->build($pageslug.'/index', $this->data);

    }

    public function a(){
        $this->form_validation->set_rules('h-captcha-response', 'Captcha', 'trim|required');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|strip_tags|addslashes|htmlentities|xss_clean|valid_email');
        $this->form_validation->set_rules('event_name', 'Event Referer', 'trim|strip_tags|addslashes|htmlentities|xss_clean');

        if ($this->form_validation->run($this) == FALSE) {
            $output = array(
                'status' => 0,
                'message' => array(
                    'text' => validation_errors()
                )
            );
        } else {
            $input = $this->input->post();

            $data = array(
                'secret' => "0x86bB06eC6032aB32647F38a426a68Eadb16b61A9",
                'response' => $input['h-captcha-response']
            );
            echo '<pre>';
            print_r(http_build_query($data));
            echo '</pre>';

            $verify = curl_init();
            curl_setopt($verify, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/x-www-form-urlencoded',
                'Content-Length: ' . strlen(http_build_query($data))
                )
            );
            curl_setopt($verify, CURLOPT_URL, "https://hcaptcha.com/siteverify");
            curl_setopt($verify, CURLOPT_POST, true);
            curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($verify);

            echo '<pre>';
            print_r($response);
            echo '</pre>';
            die;

            $responseData = json_decode($response);
            if($responseData->success) {
                // $output = $this->membership_m->update($user_data['id'],$merged_data);
                $output = array(
                    'status' => 1
                );

                $output['message'] = array(
                    'text' => 'Email submitted, you\'ll contacted soon!'
                );
            }
            else {
                $output = array(
                    'status' => 0
                );

                $output['message'] = array(
                    'text' => 'Error'
                );
            }

        }

        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

}

/* End of file Event.php */
/* Location: ./application/modules/event/controllers/Event.php */