<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization extends Main_event_controller
{
    protected $folder_path;

    public function __construct()
    {
        parent::__construct();
        

    }

    public function index()
    {
        $this->data['banner_title'] = 'Organization';
	    $this->data['page_banner'] = '6__header_page_organization.jpg';
        
        $this->_build('organization');
    }
}