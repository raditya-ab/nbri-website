<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program_book extends Main_event_controller
{
    protected $folder_path;

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['banner_title'] = 'Program Book';
	    $this->data['page_banner'] = '5__header_page_program_book.jpg';

        $this->_build('program-book');
    }
}