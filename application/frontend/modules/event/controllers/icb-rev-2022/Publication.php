<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publication extends Main_event_controller
{
    protected $folder_path;

    public function __construct()
    {
        parent::__construct();
        

    }

    public function index()
    {
        $this->data['banner_title'] = 'Publication';
	    $this->data['page_banner'] = '4__header_page_publication.jpg';
       
        $this->_build('publication');
    }
}