<?php

use Midtrans\Config;
use Midtrans\Snap;

require APPPATH . 'third_party/midtrans/Midtrans.php';

defined('BASEPATH') or exit('No direct script access allowed');

class Registration extends Main_event_controller
{

    private $event = null;

    public function __construct()
    {
        parent::__construct();
        $this->data['banner_title'] = 'Registration';

        $this->event = !is_null($this->input->get('event', TRUE)) ? $this->input->get('event', TRUE) : $this->session->userdata('event_category');

        if (!in_array($this->router->fetch_method(), ['index', 'process_step', 'finish'])) {
            if ($this->session->userdata('registration_data') == null) {
                $this->session->flashdata('error', 'No registration data found!');
                redirect('event/' . $this->uri->segment(2) . '/registration');
            }
        }

        if ($this->event != null && in_array($this->event, array_keys($this->data['event_details']['icb-rev-2022']))) {
            $this->session->set_userdata('event_category', $this->event);
        } else if ($this->event != null && !in_array($this->event, array_keys($this->data['event_details']['icb-rev-2022']))) {
            show_404();
        }

        if ($this->event != null) {
            $this->event_param = http_build_query(['event' => $this->event]);
        };

        $this->data['current_url'] = site_url($this->input->server('REQUEST_URI'));
    }

    public function process_step()
    {
        if (count($this->input->post()) == 0 || strlen($this->input->server('HTTP_REFERER')) == 0) {
            redirect('event/' . $this->uri->segment(2) . '/registration');
        }

        $current_step = $this->session->userdata('current_step');

        $has_symposia = false;

        if ($current_step == 1) {
            $this->form_validation->set_rules('event_category_name', 'Event Category', 'required');
            $this->form_validation->set_rules('event_category', 'Event Category', 'required');
        } else if ($current_step == 2) {
            $this->form_validation->set_rules('nationality', 'Nationality', 'required');
            $this->form_validation->set_rules('occupation', 'Occupation', 'required');
            $this->form_validation->set_rules('event_package', 'Event Package', 'required');

            if (
                ($this->input->post('event_category_name', true) == 'icb-rev_2022' && stripos($this->input->post('event_package'), 'presenter') !== false) ||
                ($this->input->post('event_category_name', true) == 'nbri-yic_2022')
            ) {
                $has_symposia = true;
                $this->form_validation->set_rules('symposia', 'Symposia', 'required');
                $this->form_validation->set_rules('abstract_title', 'Abstract Title', 'required');
            }
        } else if ($current_step == 3) {
            $this->form_validation->set_rules('salutation', 'Salutation', 'required');
            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('suffix', 'Suffix', 'required');
            $this->form_validation->set_rules('affiliation', 'Affiliation', 'required');
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('phone', 'Phone No.', 'required');
            $this->form_validation->set_rules('referer', 'Referer', 'required');
        }

        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->input->server('HTTP_REFERER'));
        } else {
            $input = array_map(array($this->security, 'xss_clean'), $this->input->post());
            if ($current_step == 1) {
                $registration_data = $this->session->userdata('registration_data');
                $registration_data['event_category_name'] = $input['event_category_name'];
                $registration_data['event_category'] = $input['event_category'];

                $this->session->set_userdata('step1_complete', true);

                $this->session->set_userdata('registration_data', $registration_data);

                redirect('/event/' . $this->uri->segment(2) . '/registration/step2?event=' . $input['event_category']);
            } else if ($current_step == 2) {
                $registration_data = $this->session->userdata('registration_data');
                $registration_data['nationality'] = $input['nationality'];
                $registration_data['occupation'] = $input['occupation'];
                $registration_data['event_package'] = $input['event_package'];

                sscanf($input['event_package'], "%s %s %s", $registration_data['package_type'], $registration_data['occupation'], $registration_data['nationality']);

                $package_detail = $this->data['event_details'][$this->uri->segment(2)][$this->event]['package_detail'][$registration_data['nationality']][$registration_data['occupation']][$registration_data['package_type']];

                $early_bird_date = $package_detail['price']['early_bird']['expired_time'];
                $registration_data['early_bird_promo'] = (time() < strtotime($early_bird_date)) ? true : false;

                $package_price = $registration_data['early_bird_promo']  ? $package_detail['price']['early_bird'] : $package_detail['price']['normal'];

                $normal_package_price = $package_detail['price']['normal'];
                $early_bird_package_price = $package_detail['price']['early_bird'];

                $registration_data['event_price'] = $registration_data['early_bird_promo'] ? $early_bird_package_price['amount'] : $normal_package_price['amount'];
                $registration_data['total_event_price'] = $registration_data['event_price'];
                $registration_data['event_price_currency'] = $package_price['currency'];

                $registration_data['normal_event_price'] = $normal_package_price['amount'];
                $registration_data['early_bird_event_price'] = $registration_data['early_bird_promo'] ? $early_bird_package_price['amount'] : 0;

                $registration_data['additional_discount'] = 0;

                if ($has_symposia) {
                    $registration_data['symposia'] = $input['symposia'];
                    $registration_data['abstract_title'] = $input['abstract_title'];
                }

                $this->session->set_userdata('step2_complete', true);
                $this->session->set_userdata('registration_data', $registration_data);

                redirect('/event/' . $this->uri->segment(2) . '/registration/step3?event=' . $this->event);
            } else if ($current_step == 3) {
                $registration_data = $this->session->userdata('registration_data');

                $registration_data['salutation'] = $input['salutation'];
                $registration_data['first_name'] = $input['first_name'];
                $registration_data['last_name'] = $input['last_name'];
                $registration_data['suffix'] = $input['suffix'];
                $registration_data['affiliation'] = $input['affiliation'];
                $registration_data['email'] = $input['email'];
                $registration_data['address'] = $input['address'];
                $registration_data['phone'] = $input['phone'];
                $registration_data['referer'] = $input['referer'];

                $this->session->set_userdata('step3_complete', true);
                $this->session->set_userdata('registration_data', $registration_data);

                redirect('/event/' . $this->uri->segment(2) . '/registration/step4?event=' . $this->event);
            } else {
            }
        }
    }

    public function index()
    {
        $this->session->set_userdata('current_step', 1);

        $this->_build('registration/step1');
    }

    public function step2()
    {
        if ($this->session->userdata('step1_complete')) {
            $this->session->set_userdata('current_step', 2);

            if ($this->event != 'nbri-yic_2022') {
                $this->_build('registration/step2');
            } else {
                $this->_build('registration/step2_nbriyic');
            }

        } else {
            redirect('/event/' . $this->uri->segment(2) . '/registration/');
        }
    }

    public function step3()
    {
        if ($this->session->userdata('step1_complete') && $this->session->userdata('step2_complete')) {
            $this->session->set_userdata('current_step', 3);
            $this->_build('registration/step3');
        } else {
            redirect('/event/' . $this->uri->segment(2) . '/registration/');
        }
    }

    public function step4()
    {
        if ($this->session->userdata('step1_complete') && $this->session->userdata('step2_complete') && $this->session->userdata('step3_complete')) {
            $this->session->set_userdata('current_step', 4);

            $registration_data = $this->session->userdata('registration_data');
            $registration_data['additional_discount_amount'] = 0;

            if ($this->aauth->is_loggedin()) {
                $registration_data['additional_discount'] = 0.1;
                $registration_data['additional_discount_amount'] = $registration_data['event_price'] * $registration_data['additional_discount'];
                $registration_data['total_event_price'] = $registration_data['event_price'] - $registration_data['additional_discount_amount'];
            }

            if ($registration_data['event_price_currency'] == 'usd') {
                $rates_conversion = $this->get_rates($registration_data['total_event_price']);

                $registration_data['conversion_rates'] = [
                    'license' => $rates_conversion['license'],
                    'idr_rates' => $rates_conversion['usd_to_idr'],
                    'timestamp' => $rates_conversion['timestamp'],
                    'converted_price' => ceil($rates_conversion['usd_to_idr_total'])
                ];
            }

            $this->session->set_userdata('registration_data', $registration_data);

            $this->_build('registration/step4');
        } else {
            redirect('/event/' . $this->uri->segment(2) . '/registration/');
        }
    }

    public function finish($status = 'success')
    {
        $registration_data = $this->session->userdata('registration_data');

        if (empty($registration_data) || is_null($registration_data)) {
            show_404();
        }

        if ($status != 'error') {
            $registration_data['state'] = ENVIRONMENT;

            $registration_data['order_id'] = $this->input->get('order_id', true);
            $registration_data['root_event'] = $this->uri->segment(2);
            $registration_data['event_category'] = $registration_data['event_category'];
            $registration_data['event_name'] = $registration_data['event_category_name'];
            $registration_data['package'] = $registration_data['event_package'];
            $registration_data['nationality'] = $registration_data['nationality'];
            $registration_data['is_early_bird'] = $registration_data['early_bird_promo'];
            $registration_data['is_member'] = $this->aauth->is_loggedin();
            $registration_data['member_id'] = $this->aauth->is_loggedin() ? $this->aauth->get_user_id() : null;
            $registration_data['currency'] = $registration_data['event_price_currency'];
            $registration_data['total_billed_in_idr'] = ($registration_data['event_price_currency'] == 'usd') ? $registration_data['conversion_rates']['converted_price'] : $registration_data['total_event_price'];
            $registration_data['json_value'] = json_encode($this->session->userdata('registration_data'));
            $registration_data['created_at'] = date('Y-m-d H:i:s', time());
            $registration_data['status_code'] = $this->input->get('status_code', true);
            $registration_data['transaction_status'] = $this->input->get('transaction_status', true);

            $this->session->set_userdata('registration_data', $registration_data);

            $event_registration_data = [
                'order_id' => $this->input->get('order_id', true),
                'root_event' => $this->uri->segment(2),
                'event_category' => $registration_data['event_category'],
                'event_name' => $registration_data['event_category_name'],
                'package' => $registration_data['event_package'],
                'nationality' => $registration_data['nationality'],
                'is_early_bird' => $registration_data['early_bird_promo'],
                'is_member' => $this->aauth->is_loggedin(),
                'member_id' => $this->aauth->is_loggedin() ? $this->aauth->get_user_id() : null,
                'currency' => $registration_data['event_price_currency'],
                'total_billed_in_idr' => ($registration_data['event_price_currency'] == 'usd') ? $registration_data['conversion_rates']['converted_price'] : $registration_data['total_event_price'],
                'json_value' => json_encode($this->session->userdata('registration_data')),
                'created_at' => date('Y-m-d H:i:s', time()),
                'status_code' => $this->input->get('status_code', true),
                'transaction_status' => $this->input->get('transaction_status', true),
            ];

            $registration_data['order'] = $event_registration_data;

            $email_sent = $this->checkout();

            $event_registration_data['email_sent'] = $email_sent;

            $this->db->insert('event_registration', $event_registration_data);

            unset($_SESSION['registration_data']);
            unset($_SESSION['current_step']);
            unset($_SESSION['step1_complete']);
            unset($_SESSION['step2_complete']);
            unset($_SESSION['step3_complete']);
        }

        $this->data['status'] = $status;
        $this->_build_plain('registration/finish');
    }

    public function generate_order_token()
    {
        $registration_data = $this->session->userdata('registration_data');
        $package_id = implode('_', [
            $registration_data['nationality'],
            $registration_data['occupation'],
            $registration_data['package_type']
        ]);

        Config::$serverKey = MIDTRANS_SERVER_KEY;
        // Uncomment for production environment
        Config::$isProduction = (ENVIRONMENT == 'production') ? TRUE : FALSE;
        // Enable sanitization
        Config::$isSanitized = true;
        // 	// Enable 3D-Secure
        Config::$is3ds = true;

        // Required
        $transaction_details = array(
            'order_id' => $registration_data['event_category'] . '-' . time(),
            'gross_amount' => ($registration_data['event_price_currency'] == 'usd') ? $registration_data['conversion_rates']['converted_price'] : $registration_data['total_event_price'], // no decimal allowed for creditcard
        );

        // Optional
        $item_details = array();

        // Add courier detail
        $item_details[] = array(
            'id' => $registration_data['event_category'] . '-' . $package_id,
            'name' => strtoupper($this->uri->segment(2)) . ' - ' . ucwords($registration_data['nationality']) . ' ' . ucwords($registration_data['package_type']) . ' (' . ucwords($registration_data['occupation']) . ')',
            'quantity' => 1,
            'price' => ($registration_data['event_price_currency'] == 'usd') ? $registration_data['conversion_rates']['converted_price'] : $registration_data['total_event_price'],
        );

        // Optional
        $billing_address = array(
            'first_name'    => $registration_data['first_name'],
            'last_name'     => $registration_data['last_name'],
            'address'       => $registration_data['address'],
            'phone'         => $registration_data['phone'],
        );

        $customer_details = array(
            'first_name'    => $registration_data['first_name'],
            'last_name'     => $registration_data['last_name'],
            'email'         => $registration_data['email'],
            'phone'         => $registration_data['phone'],
            'billing_address' => $billing_address
        );

        // Fill transaction details
        $transaction = array(
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_details,
        );

        $snapToken = Snap::getSnapToken($transaction);

        if ($snapToken) {
            $output = array(
                'status' => 1,
                'token' => $snapToken
            );
        } else {
            $output = array(
                'status' => 0,
                'message' => 'Cannot retrieve snap token'
            );
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

    public function checkout()
    {
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => 'radityalagi@gmail.com',
            'smtp_pass' => '@Raditya1993radityalagi',
            'smtp_port' => 465,
            'smtp_crypto' => 'ssl',
            'mailtype' => 'html',
            'charset' => 'utf8',
            'wordwrap' => TRUE
        );

        $message = $this->load->view($this->uri->segment(2) . '/email_template', NULL, TRUE);

        $this->load->library('email', $config);
        $this->email->from('radityalagi@gmail.com', 'ICB REV 2022 NBRI');
        $this->email->to($this->session->userdata('registration_data')['email']);
        // $this->email->bcc('admin@n-bri.org');
        $this->email->subject('Registration & Purchase Detail of ICB-REV 2022');
        $this->email->message($message);

        if (!$this->email->send()) {
            return false;
        }

        return true;
    }
}
