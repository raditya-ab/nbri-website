<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Speakers extends Main_event_controller
{
	protected $folder_path;

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->data['banner_title'] = 'Speakers';
		$this->data['page_banner'] = '3__header_page_speakers.jpg';

		$this->_build('speakers');
	}
}
