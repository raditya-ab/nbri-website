<?php

use Midtrans\Config;
use Midtrans\Snap;

require APPPATH.'third_party/midtrans/Midtrans.php';

defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends Main_event_controller
{
    private $closed_at;

    public function __construct()
    {
        parent::__construct();

        $this->closed_at = new DateTime($this->data['event_date']['icb-rev-2022']);
        $now = new DateTime('NOW');

        if ($now > $this->closed_at) {
            show_404();
        }

        if(!isset($_SESSION['current_step'])){
            $_SESSION['current_step'] = 1;
        }
        $this->load->model('registration_m');
    }

    public function index()
    {
        $this->data['banner_title'] = 'Registration';

        $step = ($this->input->get('step', TRUE));

        if(isset($step)){
            if($step != 1 || $step != 2 || $step != 3){
                $_SESSION['current_step'] = $step;

                if($step === '1'){
                    $this->_build('registration/step1');
                } else if($step === '2'){
                    if(!isset($_SESSION['registration_data']['step1'])){
                        $this->session->set_flashdata('error','You haven\'t finish step 1');
                        redirect(site_url('event/'.$this->folder_path.'registration?step=1'));
                    } else {
                        $this->_build('registration/step2');
                    }
                } else {
                    if(!isset($_SESSION['registration_data']['step2'])){
                        $this->session->set_flashdata('error','You haven\'t finish step 2');
                        redirect(site_url('event/'.$this->folder_path.'registration?step=2'));
                    } else {
                        $this->data['package_info'] = $this->registration_m->get_package_info(array(
                            'package_slug' => $_SESSION['registration_data']['step1']['event_package_type'],
                            'event_category' => $_SESSION['registration_data']['step1']['event_category'],
                            'package_domain' => $_SESSION['registration_data']['step1']['nationality'],
                            'package_scope' => $_SESSION['registration_data']['step1']['occupation']
                        ));
                        if(!empty($this->data['package_info'])){
                            $subtotal = $this->data['package_info']['price'];

                            if(!empty($this->data['package_info']['earlybird_exp_date']) && !empty($this->data['package_info']['earlybird_price'])){

                                $early_bird_exp = DateTime::createFromFormat("Y-m-d H:i:s", $this->data['package_info']['earlybird_exp_date']);
                                $early_bird_exp_time = $early_bird_exp->getTimestamp();

                                if(time() < $early_bird_exp_time){
                                    $subtotal = $this->data['package_info']['earlybird_price'];
                                }
                            }

                            if($this->aauth->is_loggedin()){
                                $this->data['package_info']['login_status'] = 1;
                                $this->data['package_info']['member_discount_amount'] = (($subtotal)*($this->data['package_info']['member_discount']/100));
                                $subtotal = ceil($subtotal * ((100-$this->data['package_info']['member_discount'])/100));
                            } else {
                                $this->data['package_info']['login_status'] = 0;
                            }

                            $this->data['package_info']['total'] = $subtotal;
                            $this->data['package_info']['grand_total'] = $subtotal;

                            if($this->data['package_info']['price_currency'] == 'USD'){

                                $conversion_result = $this->get_rates($subtotal);
                                $this->data['package_info']['conversion'] = $conversion_result;
                                $this->data['package_info']['grand_total'] = $conversion_result['usd_to_idr_total'];
                            }

                            $_SESSION['registration_data']['step3'] = $this->data['package_info'];

                            $this->_build('registration/step3');
                        } else {
                            show_404();
                        }
                    }
                }
            } else {
                show_404();
            }
        } else {
            $this->_build('registration/step1');
        }
    }

    public function verify()
    {
        $this->load->model('registration_m');
        if($this->input->post()){

            $step = ($this->input->post('current_step', TRUE));

            $validation = $this->registration_m->validate_step($step);

            if($validation === false){
                $output = array(
                    'status' => 0,
                    'error' => array(
                        'text' => validation_errors()
                    )
                );
                $this->session->set_flashdata($output);
                redirect(site_url('event/'.$this->folder_path.'registration?step='.$step));
            } else {
                if($step == 1){
                    $_SESSION['registration_data']['step1'] = $this->input->post();
                    $_SESSION['current_step'] = 2;
                    redirect(site_url('event/'.$this->folder_path.'registration?step=2'));
                } else if($step == 2){
                    $_SESSION['registration_data']['step2'] = $this->input->post();
                    $_SESSION['current_step'] = 3;
                    redirect(site_url('event/'.$this->folder_path.'registration?step=3'));
                } else if($step == 3){

                } else {
                    show_404();
                }
            }

        } else {
            show_404();
        }
    }

    public function generate_order_token()
	{
        // $midtrans = new Midtrans();
		//Set Your server key
            //Veritrans_Config::$serverKey = "Mid-server-hTsqaxVzMxUVyLhEAMIlkNuF";
            // Veritrans_Config::$serverKey = "SB-Mid-server-1JLCX1ZWjxkjzoX3SKC9mM8K";
            Config::$serverKey = MIDTRANS_SERVER_KEY;
			// Uncomment for production environment
			 Config::$isProduction = (ENVIRONMENT == 'production') ? TRUE : FALSE;
			// Enable sanitization
		 	Config::$isSanitized = true;
		// 	// Enable 3D-Secure
		 	Config::$is3ds = true;

		// 	$input = $_SESSION['registration_data'];
		// 	// $_SESSION['temp_post'] = $input;

		// 	// $courier_data = explode('#', $input['courier']);
		// 	// $address = $_SESSION['address_temp'];
		// 	// $cart_data = $_SESSION['cart'];

		// 	$total_order = 0;
		// 	// for($i=0;$i<count($input['product_id']);$i++){
		// 	// 	$total_order += (int)$cart_data[$i]['total_price'];

		// 	// }

		 	// Required
		 	$transaction_details = array(
		 	  'order_id' => $_SESSION['registration_data']['step1']['event_category'].'-'.time(),
		 	  'gross_amount' => $_SESSION['registration_data']['step3']['grand_total'], // no decimal allowed for creditcard
		 	);
		 	// Optional
		 	$item_details = array();

		 	// Add courier detail
		 	$item_details[] = array(
		 		'id' => $_SESSION['registration_data']['step1']['event_category'].'-'.$_SESSION['registration_data']['step1']['event_package'],
                'name' => $_SESSION['registration_data']['step1']['event_category'].'-'.$_SESSION['registration_data']['step1']['event_package'],
//                'name' => $_SESSION['registration_data']['step3']['event_category_name'].' - '.ucwords($_SESSION['registration_data']['step3']['package_domain'].' '.$_SESSION['registration_data']['step3']['package_scope'].' '.$_SESSION['registration_data']['step3']['package_slug']),
		 		'quantity' => 1,
		 		'price' => $_SESSION['registration_data']['step3']['grand_total'],
		 	);

		 	// // Optional
		 	// $billing_address = array(
		 	//   'first_name'    => "Andri",
		 	//   'last_name'     => "Litani",
		 	//   'address'       => "Mangga 20",
		 	//   'city'          => "Jakarta",
		 	//   'postal_code'   => "16602",
		 	//   'phone'         => "081122334455",
		 	//   'country_code'  => 'IDN'
		 	// );
		 	// Optional
		 	$billing_address = array(
		 	  'first_name'    => $_SESSION['registration_data']['step2']['first_name'],
		 	  'last_name'     => $_SESSION['registration_data']['step2']['last_name'],
		 	  'address'       => $_SESSION['registration_data']['step2']['address'],
		 	  'phone'         => $_SESSION['registration_data']['step2']['phone'],
		 	);
		 	// Optional
		 	$customer_details = array(
		 	  'first_name'    => $_SESSION['registration_data']['step2']['first_name'],
		 	  'last_name'     => $_SESSION['registration_data']['step2']['last_name'],
		 	  'email'         => $_SESSION['registration_data']['step2']['email'],
		 	  'phone'         => $_SESSION['registration_data']['step2']['phone'],
		 	  'billing_address' => $billing_address
		 	);
		 	// Optional, remove this to display all available payment methods
		 	// $enable_payments = array('credit_card','cimb_clicks','mandiri_clickpay','echannel');
		 	// Fill transaction details
		 	$transaction = array(
		 	  // 'enabled_payments' => $enable_payments,
		 	  'transaction_details' => $transaction_details,
		 	  'customer_details' => $customer_details,
		 	  'item_details' => $item_details,
		 	);

		 	$snapToken = Snap::getSnapToken($transaction);

		 	if($snapToken){
		 		$output = array(
		 			'status' => 1,
		 			'token' => $snapToken
		 		);
		 	} else {
		 		$output = array(
		 			'status' => 0,
		 			'message' => 'Cannot retrieve snap token'
		 		);
		 	}

		 $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

    public function finish($status = 'success')
    {
        // unset($_SESSION['registration_data']);
        if($status == 'success'){
            $_SESSION['registration_data']['state'] = ENVIRONMENT;
            $this->db->insert('registration_temp',array(
                'event_name' => strtoupper($this->uri->segment(2)),
                'json_value' => json_encode($_SESSION['registration_data'])
            ));
            unset($_SESSION['registration_data']);
        }

        $this->data['status'] = $status;
        $this->_build_plain('registration/finish');
    }

    public function verify_registration()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('currency','Currency','required');
        $this->form_validation->set_rules('currency_code','Currency','required');
        $this->form_validation->set_rules('normal_price','Price','required');

        $this->form_validation->set_rules('early_bird_price','Early Bird Price','required');

        $this->form_validation->set_rules('currency_code','Currency','required');
        $this->form_validation->set_rules('conference-package','Package','required');

        if(stripos($this->input->post('conference_package'),'participant') !== FALSE){
            $this->form_validation->set_rules('symposia','Symposia','required');
            $this->form_validation->set_rules('conference_abstract-title','Abstract Title','required');
        }
        $this->form_validation->set_rules('salutation','Prefix','required');
        $this->form_validation->set_rules('salutation-other','Prefix','trim');
        $this->form_validation->set_rules('first_name','First Name','required');
        $this->form_validation->set_rules('last_name','Last Name','trim');
        $this->form_validation->set_rules('suffixes','Suffix','trim');
        $this->form_validation->set_rules('email','Email','required');
        $this->form_validation->set_rules('affiliation','Affiliation','required');
        $this->form_validation->set_rules('address','Address','required|trim');
        $this->form_validation->set_rules('phone','Phone','required|trim');
        $this->form_validation->set_rules('referer','Referer','required|trim');
        $this->form_validation->set_rules('referer_other','Referer','trim');

        if ($this->form_validation->run($this) == FALSE) {
            $output = array(
                'status' => 0,
                'message' => array(
                    'text' => validation_errors()
                )
            );
        } else {
            $this->member_data = $this->input->post();

            if($this->member_data['currency_code'] == 'USD'){

                $rates_conversion = $this->get_rates('USD','IDR');

                $this->member_data['idr_rates'] = $rates_conversion['idr_rates'];
                $this->member_data['timestamp'] = $rates_conversion['timestamp'];
            }

            if($this->member_data['event_category'] == 'conference'){
                $this->member_data['event_category_text'] = 'International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2022';
            } else if($this->member_data['event_category'] == 'school'){
                $this->member_data['event_category_text'] = 'International Battery School (IBS) 2022';
            } else {
                $this->member_data['event_category_text'] = 'International Workshop on Solar Rooftop Residential and Utilities Scale (ISRUS) 2022';
            }

            $this->session->set_userdata('registration_data', $this->member_data);
            $this->db->insert('registration_temp',[
                'session_id' => $this->session->session_id,
                'json_value' => json_encode($this->member_data)
            ]);

            $output = array(
                'status' => 1,
                'message' => array(
                    'text' => "Preparing to checkout...",
                ),
                'redirect_url' => site_url('event/icbrev2021/summary')
            );
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

    public function summary($slug = 'summary', $pageslug = 'icb-rev-2021')
    {
        $this->data['banner_title'] = 'Registration';
        $this->template->set_partial('banner_hero','partials/event_partial/banner_hero', $this->data);
        $this->template->set_partial('navbar','partials/event_partial/icb-rev-2021/navbar', $this->data);

        $this->db->where('session_id', $this->session->session_id);
        $this->data['registration_data'] = $this->db->get('registration_temp')->row_array();

        $this->template->set_layout('default_template_event');

        $this->template->build($pageslug.'/'.$slug, $this->data);
    }


    public function checkout()
    {
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user'=>'icb-rev2021@n-bri.org',
            'smtp_pass'=>'Nbri2021',
            'smtp_port' => 465,
            'smtp_crypto'=>'ssl',
            'mailtype' => 'html',
            'charset' => 'utf8',
            'wordwrap' => TRUE
        );

        $message = $this->load->view('icb-rev-2021/email_template', NULL, TRUE);

        $this->load->library('email', $config);
        $this->email->from('icb-rev2021@n-bri.org', 'ICB REV 2021 NBRI');
        $this->email->to($this->session->userdata('registration_data')['email']);
        $this->email->bcc('admin@n-bri.org');
        $this->email->subject('Registration & Payment Detail of ICB-REV 2021');
        $this->email->message($message);

        if(!$this->email->send()){
            $output = array(
                'status' => 0,
                'message' => array(
                    'text' => 'Error when submitting your data'
                )
            );
        } else {
            unset($_SESSION['registration_data']);
            $output = array(
                'status' => 1,
                'message' => array(
                    'text' => "Your data has been registered, please check your email inbox/spam folder for further info.",
                ),
                'redirect_url' => site_url('event/icb-rev-2021')
            );
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }
}