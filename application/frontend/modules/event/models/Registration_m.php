<?php

class Registration_m extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_package_info(Array $filter){
        $this->db->select([
            'ec.event_category_name',
            'p.*'
        ]);
        $this->db->from('event_activity_package p');
        $this->db->join('activity_event_category ec','ec.id = p.event_category_id');
        $this->db->where('ec.event_category_field_slug',$filter['event_category']);
        $this->db->where('p.package_slug',$filter['package_slug']);
        $this->db->where('p.package_scope',$filter['package_scope']);
        $this->db->where('p.package_domain',$filter['package_domain']);
        $this->db->where('p.status',1);
        $this->db->where('ec.status',1);
        
        return $this->db->get()->row_array();
    }
    public function validate_step($step)
    {
        $this->load->library('form_validation');
        $rules = [];

        if($step == 1){
            $rules = [
                [
                    'field' => 'event_category',
                    'label' => 'Event Category',
                    'rules' => 'required'
                ],
                [
                    'field' => 'event_package',
                    'label' => 'Event Package',
                    'rules' => 'required'
                ],
                [
                    'field' => 'current_step',
                    'label' => 'Step',
                    'rules' => 'required'
                ],
                [
                    'field' => 'event_package_type',
                    'label' => 'Event Package',
                    'rules' => 'required'
                ]
                
            ];
    
            if($this->input->post('event_category', TRUE) == 'conference'){
                $rules[] = [
                    'field' => 'nationality',
                    'label' => 'Nationality',
                    'rules' => 'required'
                ];
                $rules[] = [
                    'field' => 'occupation',
                    'label' => 'Occupation',
                    'rules' => 'required'
                ];
                if($this->input->post('event_package_type', TRUE) == 'presenter'){
                    $rules[] = [
                        'field' => 'symposia',
                        'label' => 'Symposia',
                        'rules' => 'required'
                    ];
                    $rules[] = [
                        'field' => 'abstract_title',
                        'label' => 'Abstract Title',
                        'rules' => 'required'
                    ];
                }
            }
        } else if($step == 2){
            $rules = [
                [
                    'field' => 'salutation',
                    'label' => 'Salutation',
                    'rules' => 'required'
                ],
                [
                    'field' => 'first_name',
                    'label' => 'First Name',
                    'rules' => 'required'
                ],
                [
                    'field' => 'last_name',
                    'label' => 'Last Name',
                    'rules' => 'required'
                ],
                [
                    'field' => 'suffix',
                    'label' => 'Suffix',
                    'rules' => 'required'
                ],
                [
                    'field' => 'email',
                    'label' => 'Email Address',
                    'rules' => 'required'
                ],
                [
                    'field' => 'address',
                    'label' => 'Address',
                    'rules' => 'required'
                ],
                [
                    'field' => 'phone',
                    'label' => 'Phone Number',
                    'rules' => 'required'
                ],
                [
                    'field' => 'referer',
                    'label' => 'Event Referer',
                    'rules' => 'required'
                ]
                
            ];
    
            if(in_array($this->input->post('event_category', TRUE),['conference','school'])){
                $rules[] = [
                    'field' => 'affiliation',
                    'label' => 'Affiliation/Institution',
                    'rules' => 'required'
                ];
            } else {
                $rules[] = [
                    'field' => 'affiliation',
                    'label' => 'Institution',
                    'rules' => 'required'
                ];
            }
        }

        $this->form_validation->set_rules($rules);

        return $this->form_validation->run($this);
    }
    
}