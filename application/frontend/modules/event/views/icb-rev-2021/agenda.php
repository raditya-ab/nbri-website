<section id="countdown-section" class="page-section py-80 text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="section-title">Register Now</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="countdown"></div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5">
                <a class="btn btn-custom bg-navy text-yellow" href="<?php echo site_url('event/icb-rev-2021/registration');?>">Register Now</a>
            </div>  
        </div>
    </div>
</section>
<section id="scope-section" class="page-section bg-yellow py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Agenda</h2>
            </div>
            <div class="col-md-12 grid-card-wrapper grid-3 align-items-center justify-content-between">
                <div class="card">
                    <a href="<?php echo site_url('event/icb-rev-2021/conference');?>">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/conference.png');?>" alt="International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2021">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2021</h3>
                            <!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="<?php echo site_url('event/icb-rev-2021/school');?>">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/school.png');?>" alt="">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">International School of Battery Technology</h3>
                            <!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="<?php echo site_url('event/icb-rev-2021/exhibition');?>">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/exhibition.png');?>" alt="">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Exhibitions (Research Centers, Universities, and Industries)</h3>
                            <!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="calendar-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Mark the calendar</h2>
            </div>
            <div class="col-md-12 accordion-tab-wrapper ">
                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">12</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Abstract Submission Deadline</div>
                </div>
                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">14</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Early bird registration deadline</div>
                </div>
                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">15</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Abstract Acceptance Notification</div>
                </div>
                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">24</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Full paper submission deadline</div>
                </div>
                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">22 - 24</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Conference date</div>
                </div>
                
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5" style="flex-wrap: wrap">
                <!-- <a class="btn btn-custom bg-yellow text-navy mx-3"><i class="fab fa-google"></i> Add events to Google Calendar</a> -->
                <a href="<?php echo base_url('assets/icb-rev-2021.ics');?>" download class="btn btn-custom bg-navy text-green mx-3"><i class="far fa-calendar-alt"></i> Add to calendar</a>
            </div>
        </div>
    </div>
</section>