<section id="abstract-section" class="page-section py-80">
    <div class="container">
        <div class="row">
        <div class="col-md-12">
                <h2 class="section-title text-yellow">Abstract and Paper Submission</h2>
                <p class="text-navy font-light font-size-medium">
                    Please read the Abstract and Paper instruction carefully. Abstract and papers submitted to the ICB-REV 2021 must be prepared in accordance with the template. Please download the <a class="font-light font-size-medium text-yellow" download href="<?php echo base_url('assets/file/Abstract_Template_of_ICB-REV_2021.docx');?>">abstract template</a> and <a class="font-light font-size-medium text-yellow" download href="<?php echo base_url('assets/file/Paper_Template_of_ICB-REV_2021.docx');?>">paper template</a>.
                </p>
                <ol class="font-size-medium text-navy font-light">
                    <li>Abstract and papers must be submitted through <a class="font-light font-size-medium text-yellow" href="https://easychair.org/conferences/?conf=icbrev2021" target="_blank">Easychair submission system</a> for ICB-REV 2021. A guideline on how to submit full paper conference is available <a download href="<?php echo base_url('assets/file/Easychair_ICB-REV_2021_Guideline.pdf');?>" class="font-light font-size-medium text-yellow">Guideline for Easychair</a>.</li>
                    <li>Papers must be submitted in a .pdf file format.</li>
                    <li>File size cannot exceed 50MB. Check image resolution or compress images before saving the file as .pdf.</li>
                    <li>Check that all fonts are converted properly when the .pdf is created. Pay close attention to special font types and symbols used in equations.</li>
                    <li>You may submit the Conference Paper in advance of the June 24, 2021 deadline.</li>
                </ol>
                <p class="text-navy font-light font-size-medium">
                    The presenting author should update any changes from the final paper; such as the title and/or author information. This can be done directly via easychair.
                </p>
            </div>
        </div>
    </div>
</section>
<section id="tentative-agenda-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-green">Tentative Agenda</h2>
                <p class="text-navy font-light font-size-medium">
                The International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2021 will be as in the file below:
                </p>
            </div>
        </div>
        <!-- <style>
            .table-custom,
            .table-custom th,
            .table-custom td{
                border: 5px solid #181D62;
                font-size: 14px;
            }

            .table-custom thead th,
            .table-custom td[rowspan],
            .table-custom td[colspan] {
                font-family: 'Sailec Bold';
            }

            .table-custom thead {
                background-color: #F7AF00;
            }

            .table-custom td.table-break {
                background-color: #F7AF00;
            }
        </style> -->
        <div class="row mt-3">
            <div class="col-12">
                <a class="btn btn-custom bg-green text-navy" href="<?=base_url('assets/file/tentative_agend_icb-rev_2021.pdf');?>" target="_blank">View PDF Agenda</a>
                <!-- <div class="table-responsive">
                    <table class="table text-center table-custom bg-green">
                        <thead>
                            <tr>
                                <th>Sessions</th>
                                <th>Time</th>
                                <th><?php echo date('l, F jS', strtotime('2021-06-22'));?></th>
                                <th><?php echo date('l, F jS', strtotime('2021-06-23'));?></th>
                                <th><?php echo date('l, F jS', strtotime('2021-06-24'));?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowspan="6">Morning Session <br><b>(GMT +07.00)</b></td>
                                <td>08.30 – 09.00</td>
                                <td>Opening</td>
                                <td rowspan="2">Plenary Session (3)</td>
                                <td rowspan="2">Plenary Session (5)</td>
                            </tr>
                            <tr>
                                <td>09.00 – 09.40</td>
                                <td>Plenary Session (1)</td>
                            </tr>
                            <tr>
                                <td>09.40 – 09.45</td>
                                <td class="table-break" colspan="3">Room Transition</td>
                            </tr>
                            <tr>
                                <td>09.45 – 10.35</td>
                                <td>Keynote Speech (1)</td>
                                <td>Keynote Speech (3)</td>
                                <td>Keynote Speech (5)</td>
                            </tr>
                            <tr>
                                <td>10.35 – 12.00</td>
                                <td>Parallel Session (1)</td>
                                <td>Parallel Session (3)</td>
                                <td>Parallel Session (5)</td>
                            </tr>
                            <tr>
                                <td>12.00 – 13.00</td>
                                <td class="table-break" colspan="3">Break</td>
                            </tr>
                            <tr>
                                <td rowspan="6">Afternoon Session <br><b>(GMT +07.00)</b></td>
                            </tr>
                            <tr>
                                <td>13.00 - 13.40</td>
                                <td>Plenary Session (2)</td>
                                <td>Plenary Session (4)</td>
                                <td>Plenary Session (6)</td>
                            </tr>
                            <tr>
                                <td>13.40 – 13.45</td>
                                <td class="table-break" colspan="3">Room Transition</td>
                            </tr>
                            <tr>
                                <td>13.45 – 14.35</td>
                                <td>Keynote Speech (2)</td>
                                <td>Keynote Speech (4)</td>
                                <td>Keynote Speech (6)</td>
                            </tr>
                            <tr>
                                <td>14.35 – 16.00</td>
                                <td>Parallel Session (2)</td>
                                <td>Parallel Session (4)</td>
                                <td>Parallel Session (6)</td>
                            </tr>
                            <tr>
                                <td>16.00 - End</td>
                                <td class="table-break" colspan="3">Closing</td>
                            </tr>
                        </tbody>
                    </table>
                </div> -->
            </div>
        </div>
    </div>
</section>
<section id="register-agenda-section" class="page-section bg-navy py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-green">Registration</h2>
                <p class="text-yellow font-light font-size-medium">
                All participants will join the ICB-REV 2021 coming from R&D Institutes, Universities, and Industries from all over the world. Registration shall be made as follow.
                </p>
            </div>
            <div class="col-md-12">

               
                <div class="nav nav-tabs no-border d-flex justify-content-center" id="nav-tab" role="tablist">
                    <a class="nav-item no-radius nav-link active btn btn-custom bg-yellow text-navy mx-3" id="international-tab" data-toggle="tab" href="#nav-international"
                        role="tab" aria-controls="nav-home" aria-selected="true">International</a>
                    <a class="nav-item no-radius nav-link btn btn-custom bg-yellow text-navy mx-3" id="indonesia-tab" data-toggle="tab" href="#nav-indonesia"
                        role="tab" aria-controls="nav-profile" aria-selected="false">Indonesia</a>
                </div>
                <div class="tab-content mt-5" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-international" role="tabpanel" aria-labelledby="international-tab">
                        <div class="row">
                            <div class="col-md-12 d-flex justify-content-around flex-wrap">
                                <div class="price-card bg-green p-5 text-center mb-3">
                                    <div class="card-title-container mb-5">
                                        <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                    </div>
                                    <div class="price-container mb-5">
                                        <span class="normal-price font-size-tiny"><del>US$ 300</del></span>
                                        <br/>
                                        <span class="discount-price font-size-biggest font-bold">US$ 250<sup>*</sup></span>
                                    </div>
                                    <div class="price-description">
                                        <small>*before June 14th, 2021</small>
                                        <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Select</a>
                                        <hr>
                                        <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                            <li>Participate all conference programs</li>
                                            <li>Oral presentation</li>
                                            <li>International certificate</li>
                                            <li>International publication</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="price-card bg-green p-5 text-center mb-3">
                                    <div class="card-title-container mb-5">
                                        <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                    </div>
                                    <div class="price-container mb-5">
                                        <span class="normal-price font-size-tiny"><del>US$ 400</del></span>
                                        <br/>
                                        <span class="discount-price font-size-biggest font-bold">US$ 350<sup>*</sup></span>
                                    </div>
                                    <div class="price-description">
                                        <small>*before June 14th, 2021</small>
                                        <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                        <hr>
                                        <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                            <li>Participate all conference programs</li>
                                            <li>Oral presentation</li>
                                            <li>International certificate</li>
                                            <li>International publication</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="price-card bg-green p-5 text-center mb-3">
                                    <div class="card-title-container mb-5">
                                        <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                    </div>
                                    <div class="price-container mb-5">
                                        <br/>
                                        <span class="discount-price font-size-biggest font-bold">US$ 100</span>
                                    </div>
                                    <div class="price-description">
                                        <small>&nbsp;</small>

                                        <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                        <hr>
                                        <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                            <li>Attend all conference programs</li>
                                            <li>International certificate</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="price-card bg-green p-5 text-center mb-3">
                                    <div class="card-title-container mb-5">
                                        <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                    </div>
                                    <div class="price-container mb-5">
                                        <br>
                                        <span class="discount-price font-size-biggest font-bold">US$ 150</span>
                                    </div>
                                    <div class="price-description">
                                        <small>&nbsp;</small>

                                        <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                        <hr>
                                        <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                            <li>Attend all conference programs</li>
                                            <li>International certificate</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-indonesia" role="tabpanel" aria-labelledby="indonesia-tab">
                        <div class="row">
                            <div class="col-md-12 d-flex justify-content-around flex-wrap">
                                <div class="price-card bg-green p-5 text-center mb-3">
                                    <div class="card-title-container mb-5">
                                        <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                    </div>
                                    <div class="price-container mb-5">
                                        <span class="normal-price font-size-tiny"><del>Rp 2.500.000</del></span>
                                        <br>
                                        <span class="discount-price font-size-biggest font-bold">Rp 2.000.000<sup>*</sup></span>
                                    </div>
                                    <div class="price-description">
                                        <small>*before June 14th, 2021</small>
                                        <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                        <hr>
                                        <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                            <li>Participate all conference programs</li>
                                            <li>Oral presentation</li>
                                            <li>International certificate</li>
                                            <li>International publication</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="price-card bg-green p-5 text-center mb-3">
                                    <div class="card-title-container mb-5">
                                        <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                    </div>
                                    <div class="price-container mb-5">
                                        <span class="normal-price font-size-tiny"><del>Rp 3.000.000</del></span>
                                        <br>
                                        <span class="discount-price font-size-biggest font-bold">Rp 2.500.000<sup>*</sup></span>
                                    </div>
                                    <div class="price-description">
                                        <small>*before June 14th, 2021</small>
                                        <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                        <hr>
                                        <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                            <li>Participate all conference programs</li>
                                            <li>Oral presentation</li>
                                            <li>International certificate</li>
                                            <li>International publication</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="price-card bg-green p-5 text-center mb-3">
                                    <div class="card-title-container mb-5">
                                        <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                    </div>
                                    <div class="price-container mb-5">
                                        <br>
                                        <span class="discount-price font-size-biggest font-bold">Rp 500.000</span>
                                    </div>
                                    <div class="price-description">
                                        <small>&nbsp;</small>
                                        <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                        <hr>
                                        <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                            <li>Attend all conference programs</li>
                                            <li>International certificate</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="price-card bg-green p-5 text-center mb-3">
                                    <div class="card-title-container mb-5">
                                        <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                    </div>
                                    <div class="price-container mb-5">
                                        <br>
                                        <span class="discount-price font-size-biggest font-bold">Rp 1.500.000</span>
                                    </div>
                                    <div class="price-description">
                                        <small>&nbsp;</small>
                                        <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                        <hr>
                                        <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                            <li>Attend all conference programs</li>
                                            <li>International certificate</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="countdown-section" class="page-section py-80 text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="section-title">Register Now</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="countdown"></div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5">
                <a class="btn btn-custom bg-navy text-yellow" href="#register-agenda-section" class="page-section bg-navy py-80">Register Now</a>
            </div>  
        </div>
    </div>
</section>