<?php
    $registered_user_data = $this->session->userdata('registration_data');
?>
<h1>Thank you for registering the ICB-REV 2021</h1>
<p>Your registration details:</p>

<ul>
    <li>Fullname: <?php echo $registered_user_data['salutation'].' '.$registered_user_data['first_name'].' '.$registered_user_data['last_name'].$registered_user_data['suffixes'];?></li>
    <li>Email address: <?=$registered_user_data['email'];?></li>
    <li>Address: <?=$registered_user_data['address'];?></li>
    <li>Phone number: <?=$registered_user_data['phone'];?></li>
    <li>Institution / Affiliation: <?=$registered_user_data['affiliation'];?></li>
    <li>Event Category: <?=$registered_user_data['event_category_text'];?></li>
    <li>Package: <?=$registered_user_data['conference-package-text'];?></li>
    <li>Nationality: <?=ucwords($registered_user_data['nationality']);?></li>
    <?php if($registered_user_data['event_category'] == 'conference'):?>
        <?php if(stripos($registered_user_data['conference_package'],'participant') !== FALSE):?>
            <li>Symposia: <?=$registered_user_data['symposia'];?></li>
            <li>Abstract Title: <?=$registered_user_data['conference_abstract-title'];?></li>
        <?php endif;?>
    <?php endif;?>
    <?php if(1623689999 >= strtotime('now')):?>
        <li>Amount Transfer: <b><?=$registered_user_data['currency'].$registered_user_data['early_bird_price'];?></b></li>
    <?php else:?>
        <li>Amount Transfer: <b><?=$registered_user_data['currency'].$registered_user_data['normal_price'];?></b></li>
    <?php endif;?>
</ul>
<p><i>Please read the payment instruction carefully</i></p>
<ul>
    <li>Please transfer your payment to below account <b>ONLY</b>, please note that we don’t accept any other type of payment or any different account.<br>
        Acc.No: <b>899 288 0502</b><br>
        Bank: <b>PT Bank Central Asia Tbk (BCA)</b> <br>
        Recepient: <b>YAYASAN PUSAT UNGGULAN INOVASI BATERAI DAN ENERGI TERBARUKAN</b>
        <?php if($registered_user_data['currency_code'] == 'USD'):?>
            Currency Code: <b>IDR</b><br>
            Swift code: <b>CENAIDJA</b><br>
            Bank Adress: <b>Wisma BCA BSD City, Lot. 1.3, Jl. Kapten Soebijanto Djojohadikusumo</b><br>
            <b>No.Kav. CDB, Lengkong Gudang,</b><br>
            <b>Kec. Serpong, Kota Tangerang Selatan</b><br>
            <b>Banten 15310</b><br>
            PIC: <b>Adit Tri Wiguno (+62 896 8700 5142)</b>
        <?php endif;?>
    </li>
    <li>
    After you have done with your payment, please email the scanned transfer receipt (with a clear and readable information in a pdf format) to <a href="mailto:icb-rev2021@n-bri.org">icb-rev2021@n-bri.org</a> with subject <b>RECEIPT_ICBREV2021_NAME</b> (eg: RECEIPT_ICBREV2021_WAHYU SYAFIUL)
    </li>
    <li>If you are already registered as member of NBRI, please reply this email first with your registered email address at n-bri.org to get your 10% discount.</li>
</ul>