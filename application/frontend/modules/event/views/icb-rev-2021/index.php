<section id="about-section" class="page-section bg-yellow py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <p>
                The latest report from the International Governmental Panel on Climate Change (IPCC) captures the full scale of the threat to human life in a heating world. The surge of carbon emissions in the last decade lead mother earth to the climate crisis. Various initiatives have taken to curb the climate catastrophe. From global treaty policy (Paris Agreement 2015 to Glasgow Climate Pact 2021) to technology intervention (renewable energy to battery electric vehicles innovation). Even, Indonesia G20 presidency makes the energy transition as one of priority agenda. Because energy transition follows the Paris Agreement, which targets net-zero emissions by 2060.
                <br><br>
                Based on the current situation, the National Battery Research Institute (NBRI) will conduct the International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2022 in collaboration with Queen Mary University of London (QMUL) United Kingdom, International Union of Material Research Societies (IUMRS) and Material Research Society Indonesia (MRS-INA). This conference will bring together scientists, academicians, industry partners, the government and all stakeholders that focus on battery technology for both Electric Vehicles and Renewable Energy. The ICB-REV 2022 will be the insightful space for all related stakeholders to disseminate their innovations, exchange their ideas & perspectives and also open international networking for bolstering the global energy transition agenda.
                <br><br>
                Since its establishment on December 07th 2020, the NBRI has performed various events covering the battery technology, renewable energy, and electric vehicles topics. NBRI has successfully conducted the ICB-REV 2021 and International Conference on Advanced Material and Technology (ICAMT) 2021 with 154 distinguished speakers across 16 countries. There were more than 150 selected articles published in AIP Proceedings, IONICS journal, and Progress on Natural Science and Material International (PNSMI) journal. In addition, NBRI has also organized International Battery School (IBS) 2021, Climate Challenge Workshop 2021, International School of Battery in Electric Vehicles (ISBEV) 2021, International Workshop on Material and Advanced Characterization (IMAC) 2021 and International Workshop on Solar Rooftop Residential and Utilities Scale (ISRUS) 2021.
                <br><br>
                The main concern of ICB-REV 2022 is focused on advanced battery technology from raw materials to cell fabrication, energy storage for renewable energy, and electric vehicles battery and charging station that support energy transition. This should be accomplished by the presence of invited world-class speakers, international participants, global industry players and international battery associations. The selected articles will be published in AIP international proceedings and other reputable journal Scopus indexed.
                <br><br>
                Following ICB-REV 2022, the consecutive events will be organized as complementary to broaden audiences such as:
                <ul>
                    <li>International Battery School (IBS) on May 24th-25th, 2022</li>
                    <li>NBRI Youth Ideas Competition (NBRI-YIC) on June 21th, 2022</li>
                    <li>International Workshop on Solar Rooftop Residential and Utilities Scale (ISRUS) on July 13th-14th, 2022</li>
                </ul>
                <br><br>
                Due to the present situation of COVID-19, the format of presentation will be conducted online. We sincerely hope that you will enjoy the ICB-REV 2022 and have a pleasant experience.
                <br><br>
                Indonesia, 01 April 2022
                <br><br>
                <br><br>
                <b><u>Prof. Dr. rer. nat. Evvy Kartini</u></b><br>
                <i>Chair of the ICB-REV 2022</i>
                </p>
            </div>
            <div class="col-md-3">
                <img src="<?php echo base_url('assets/images/evvy.png');?>" alt="">
            </div>

        </div>
    </div>
</section>
<section id="scope-section" class="page-section my-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Scopes</h2>
            </div>
            <div class="col-md-12 grid-card-wrapper grid-4 align-items-center justify-content-between">
                <div class="card">
                    <!-- <a href=""> -->
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/scopes1.png');?>" alt="Advanced battery technology from raw materials to cell fabrication">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Advanced battery technology from raw materials to applications and recycling</h3>
                            <!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
                        </div>
                    <!-- </a> -->
                </div>
                <div class="card">
                    <!-- <a href=""> -->
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/scopes2.png');?>" alt="">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Energy storage for renewable energy (solar, wind, biomass, geothermal, etc)</h3>
                            <!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
                        </div>
                    <!-- </a> -->
                </div>
                <div class="card">
                    <!-- <a href=""> -->
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/scopes3.png');?>" alt="">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Battery electric vehicles, battery swap, and charging station</h3>
                            <!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
                        </div>
                    <!-- </a> -->
                </div>
                <div class="card">
                    <!-- <a href=""> -->
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/scopes4.png');?>" alt="">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Other related topics (policy, regulation, standardization, industry, etc)</h3>
                            <!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
                        </div>
                    <!-- </a> -->
                </div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5">
                <a href="<?php echo site_url('event/icb-rev-2021/agenda');?>" class="btn btn-custom bg-navy text-yellow">Register Now</a>
            </div>
        </div>
    </div>
</section>
<section id="calendar-section" class="page-section bg-green py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Mark the calendar</h2>
            </div>
            <div class="col-md-12 accordion-tab-wrapper ">
                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">12</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Abstract Submission Deadline</div>
                </div>
                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">14</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Early bird registration deadline</div>
                </div>
                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">15</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Abstract Acceptance Notification</div>
                </div>

                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">24</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Full paper submission deadline</div>
                </div>
                <div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
                    <div class="event-date text-center">
                        <div class="date">22 - 24</div>
                        <div class="month text-green">June</div>
                    </div>
                    <div class="event-description">Conference date</div>
                </div>

            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5" style="flex-wrap: wrap">
                <!-- <a class="btn btn-custom bg-yellow text-navy mx-3"><i class="fab fa-google"></i> Add events to Google Calendar</a> -->
                <a href="<?php echo base_url('assets/icb-rev-2021.ics');?>" download class="btn btn-custom bg-navy text-green mx-3"><i class="far fa-calendar-alt"></i> Add to calendar</a>
            </div>
        </div>
    </div>
</section>
<section id="speakers-section" class="page-section bg-navy py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title">Speakers</h2>
            </div>
            <div class="col-md-12 list-card-wrapper">
                <div class="card-list right d-flex mb-5">
                    <div class="card-caption mr-5">
                        <h3 class="card-title">Luhut Binsar Pandjaitan</h3>
                        <p>Coordinating Minister of Maritime and Investment Affairs of Indonesia</p>
                        <p class="card-description">Luhut Binsar Pandjaitan is an Indonesian politician, businessman and retired four-star Army general, serving as Coordinating Minister for Maritime Affairs and Investment since October 2019. He previously served as Coordinating Minister for Maritime Affairs from July 2016 to October 2019, Coordinating Minister for Political, Legal, and Security Affairs from August 2015 to July 2016 and Chief of Staff to President Joko Widodo. He was also Minister of Trade and Industry in President Abdurrahman Wahid's cabinet and Indonesian Ambassador to Singapore from 1999 to 2000.</p>
                    </div>
                    <div class="card-image image-stack bg-yellow">
                        <div class="image-stack-item image-stack-item__top">
                            <img class="img-responsive image-stack-item image-stack-item__top" src="<?php echo base_url('assets/images/prof_b_john_goodenough.png');?>" alt="">
                        </div>
                        <div class="image-stack-item image-stack-item__bottom bg-yellow" ></div>

                    </div>
                </div>
                <div class="card-list left d-flex mb-5">
                    <div class="card-caption">
                        <h3 class="card-title">Prof. Bambang Permadi Soemantri Brodjonegoro</h3>
                        <p>Co-Chair T20 of G20 Indonesia Presidency and Former Minister of Research and Technology/National Research and Innovation Agency of Republic of Indonesia</p>
                        <p class="card-description">Prof. Bambang Permadi Soemantri Brodjonegoro is a Co-Chair T20 of G20 Indonesia Presidency. He was the Minister of Research and Technology/National Research and Innovation Agency of Republic of Indonesia. Previously, he was the Minister of National Development Planning of Indonesia, taking office after a cabinet reshuffle by President Joko Widodo. From 27 October 2014 to 27 July 2016, he was the Finance Minister of Indonesia in the Working Cabinet also under President Joko Widodo's administration. Under Susilo Bambang Yudhoyono's administration, he was the Deputy of Finance Minister in the Second United Indonesia Cabinet.</p>
                    </div>
                    <div class="card-image image-stack bg-yellow mr-5">
                        <div class="image-stack-item image-stack-item__top">
                            <img class="img-responsive image-stack-item image-stack-item__top" src="<?php echo base_url('assets/images/prof_ying_shirley_meng.png');?>" alt="">
                        </div>
                        <div class="image-stack-item image-stack-item__bottom bg-yellow" ></div>

                    </div>

                </div>
                <div class="card-list right d-flex mb-5">
                    <div class="card-caption mr-5">
                        <h3 class="card-title">Prof. Rodrigo Martins</h3>
                        <p>President of International Union of Material Research Societies (IUMRS)</p>
                        <p class="card-description">Prof. Rodrigo Martins is full professor in Materials Science Department of Faculty of Science and Technology of New University of Lisbon, a fellow of the Portuguese Engineering Academy since 2009 and a member of the European Academy of Science since 2016. He was decorated with the gold medal of merit and distinction by the Almada Municipality for his R&D achievements. He also President of International Union of Material Research Societies (IUMRS).</p>
                    </div>
                    <div class="card-image image-stack bg-yellow">
                        <div class="image-stack-item image-stack-item__top">
                            <img class="img-responsive image-stack-item image-stack-item__top" src="<?php echo base_url('assets/images/prof_jun_liu.png');?>" alt="">
                        </div>
                        <div class="image-stack-item image-stack-item__bottom bg-yellow" ></div>

                    </div>
                </div>
                <div class="card-list left d-flex mb-5">
                    <div class="card-caption">
                        <h3 class="card-title">Prof. Neeraj Sharma</h3>
                        <p>Director of Australian Battery Society (ABS), Australia</p>
                        <p class="card-description">Professor Neeraj Sharma is a Director of Australian Battery Society (ABS), Australia. He also Associate Professor of University of New South Wales (UNSW), Australia. Neeraj completed his Ph.D. at the University of Sydney then moved to the Bragg Institute at Australian Nuclear Science and Technology Organization (ANSTO) for a postdoc. He started at the School of Chemistry, UNSW on Australian Institute of Nuclear Science and Engineering (AINSE) Research Fellowship followed by an Australian Research Council (ARC) Discovery Early Career Research Award (DECRA). He is currently an Associate Professor and ARC Future Fellow.</p>
                    </div>
                    <div class="card-image image-stack bg-yellow mr-5">
                        <div class="image-stack-item image-stack-item__top">
                            <img class="img-responsive image-stack-item image-stack-item__top" src="<?php echo base_url('assets/images/prof_laurence_hardwick.png');?>" alt="">
                        </div>
                        <div class="image-stack-item image-stack-item__bottom bg-yellow" ></div>

                    </div>

                </div>
                <div class="card-list right d-flex mb-5">
                    <div class="card-caption mr-5">
                        <h3 class="card-title">Prof. Jun Liu</h3>
                        <p>Director of Battery 500 Consortium, United States</p>
                        <p class="card-description">Prof. Jun Liu is a Battelle Fellow at PNNL, holds a joint appointment with the University of Washington as the Campbell Chair Professor, and also serves as the Director for the Battery500 Consortium, a large DOE multi-institute program supported by DOE to develop future energy storage technologies. Prof. Liu has more than 450 peer reviewed publications, has received more than 60 U.S. patents. He has been a Highly Cited Researcher in the world in the fields of chemistry and materials sciences since 2014.</p>
                    </div>
                    <div class="card-image image-stack bg-yellow">
                        <div class="image-stack-item image-stack-item__top">
                            <img class="img-responsive image-stack-item image-stack-item__top" src="<?php echo base_url('assets/images/dr_laksana_tri_handoko.png');?>" alt="">
                        </div>
                        <div class="image-stack-item image-stack-item__bottom bg-yellow" ></div>

                    </div>
                </div>
                <div class="card-list left d-flex mb-5">
                    <div class="card-caption">
                        <h3 class="card-title">Francesco La Camera</h3>
                        <p>Director General of International Renewable Energy Agency (IRENA), Italy</p>
                        <p class="card-description">Francesco La Camera is the Director-General of the International Renewable Energy Agency (IRENA). He was appointed at the Ninth Assembly of IRENA, the ultimate decision-making body of the Agency. Mr. La Camera took office on 4 April 2019 and brings more than thirty years of experience in the fields of climate, sustainability, and international cooperation. In his role, Mr. La Camera is responsible for leading the delivery of IRENA’s work program and strategy in cooperation with the Agency’s member states. At a critical time for climate change and the achievement of the Sustainable Development Goals, Mr. La Camera is tasked with redefining the structure and operations of the Agency in response to the urgent needs of its members. Under his leadership the Agency has forged a series of new strategic partnerships with UN organizations including UNDP, UNFCCC and Green Climate Fund among others.</p>
                    </div>
                    <div class="card-image image-stack bg-yellow mr-5">
                        <div class="image-stack-item image-stack-item__top">
                            <img class="img-responsive image-stack-item image-stack-item__top" src="<?php echo base_url('assets/images/yi_ke_phd.png');?>" alt="">
                        </div>
                        <div class="image-stack-item image-stack-item__bottom bg-yellow" ></div>

                    </div>

                </div>
                <div class="card-list right d-flex mb-5">
                    <div class="card-caption mr-5">
                        <h3 class="card-title">Rene Schroeder</h3>
                        <p>Executive Director of European Automotive and Industrial Battery Manufacturers (EUROBAT), Belgium</p>
                        <p class="card-description">Rene Schroeder is an Executive Director of European Automotive and Industrial Battery Manufacturers (EUROBAT), Belgium since 2017. EUROBAT is the association for the European manufacturers of automotive, industrial and energy storage batteries. EUROBAT has more than 50 members from across the continent comprising more than 90% of the automotive and industrial battery industry in Europe. He received his bachelor degree in University of Nebraska at Kearney (2001), then Master degree on History/Political Science from Universität Rostock (1997-2002) and Master degree on European Studies/Civilization from College of Europe in Natolin (2003-2004).</p>
                    </div>
                    <div class="card-image image-stack bg-yellow">
                        <div class="image-stack-item image-stack-item__top">
                            <img class="img-responsive image-stack-item image-stack-item__top" src="<?php echo base_url('assets/images/prof_tim_white_1.png');?>" alt="">
                        </div>
                        <div class="image-stack-item image-stack-item__bottom bg-yellow" ></div>

                    </div>
                </div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5">
                <a href="<?php echo site_url('event/icb-rev-2021/speakers');?>" class="btn btn-custom bg-yellow text-navy">View All Speakers</a>
            </div>
        </div>
    </div>
</section>
<section id="countdown-section" class="page-section py-80 text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="section-title">Register Now</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="countdown"></div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5">
                <a href="<?php echo site_url('event/icb-rev-2021/agenda');?>" class="btn btn-custom bg-navy text-yellow">Register Now</a>
            </div>
        </div>
    </div>
</section>
