<section id="abstract-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-yellow">Steering Comittee</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Alan J. Drew</div>
            <div class="col font-size-medium"><i>Co-Founder of National Battery Research Institute</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. rer. nat. Evvy Kartini</div>
            <div class="col font-size-medium"><i>Founder of National Battery Research Institute</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Y.F. Han</div>
            <div class="col font-size-medium"><i>President of IUMRS</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. M. Nizam, Ph.D.</div>
            <div class="col font-size-medium"><i>Coordinator of the National Research Priority Mandatory</i></div>
        </div>

        <div class="row">
            <div class="col-md-12 mt-5">
                <h2 class="section-title text-yellow">International Advisory Board</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Rodrigo Martins</div>
            <div class="col font-size-medium"><i>President of IUMRS</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. rer. nat. Evvy Kartini</div>
            <div class="col font-size-medium"><i>President of MRS-INA</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Yafang Han</div>
            <div class="col font-size-medium"><i>Immediate past president, IUMRS</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. B.V.R. Chowdary</div>
            <div class="col font-size-medium"><i>(IUMRS HO Officer), Singapore</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Yuan Ping Feng</div>
            <div class="col font-size-medium"><i>(IUMRS Officer), Singapore</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Hideo Hosono</div>
            <div class="col font-size-medium"><i>MRS-Japan</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Jow-Lay Huang</div>
            <div class="col font-size-medium"><i>MRS-Taiwan</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Woo-Gwang Jung</div>
            <div class="col font-size-medium"><i>MRS-Korea</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Soo Wohn Lee</div>
            <div class="col font-size-medium"><i>(IUMRS Officer), Korea</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Santi Maensiri</div>
            <div class="col font-size-medium"><i>MRS-Thailand</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Osvaldo Novais de Oliveira Jr</div>
            <div class="col font-size-medium"><i>(IUMRS Officer), Brazil</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Alan J Drew</div>
            <div class="col font-size-medium"><i>Queen Mary University of London, UK and Co-founder of NBRI</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Dr. Christian Nielsen</div>
            <div class="col font-size-medium"><i>Queen Mary University of London, UK</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Dr. Ana Jorge Sobrido</div>
            <div class="col font-size-medium"><i>Queen Mary University of London, UK</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Ir. Anne Zulfia, M.Sc.</div>
            <div class="col font-size-medium"><i>University of Indonesia, Indonesia</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Risdiana, M.Eng</div>
            <div class="col font-size-medium"><i>Padjajaran University, Indonesia</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Ir. I Made Dana Tangkas M.Si, IPU, ASEAN Eng.</div>
            <div class="col font-size-medium"><i>Indonesian Automotive Institute Indonesia</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Dr. Edvin Aldrian, B.Eng., M.Sc.</div>
            <div class="col font-size-medium"><i>Agency for the Assesment and Application of Technology, Indonesia</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Bendjamin Benny Louhenapessy</div>
            <div class="col font-size-medium"><i>National Standarization Agency Indonesia</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Ir. Iswan Prahastono M.Phil</div>
            <div class="col font-size-medium"><i>PLN Indonesia</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Dr. Mohamad Mustafa Sarinanto, IPU</div>
            <div class="col font-size-medium"><i>Agency for the Assessment and Application of Technology (BPPT) Indonesia</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Ir. Chairul Hudaya, Ph.D.</div>
            <div class="col font-size-medium"><i>Sumbawa University of Technology Indonesia</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Dr. Alexey Glushenkov</div>
            <div class="col font-size-medium"><i>Australia National University (ANU) Australia</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Prof. Poo See Lee</div>
            <div class="col font-size-medium"><i>Nanyang Technological University, Singapore</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Arief S. Budiman, Ph.D.</div>
            <div class="col font-size-medium"><i>BINUS University, Indonesia</i></div>
        </div>

        <div class="row">
            <div class="col-md-12 mt-5">
                <h2 class="section-title text-yellow">Organizing Comittee</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Chair</div>
            <div class="col font-size-medium"><i>Prof. Dr. rer. nat. Evvy Kartini</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Vice Chair</div>
            <div class="col font-size-medium"><i>Prof. Alan J Drew</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Secretary</div>
            <div class="col font-size-medium"><i>Shinta Widyaningrum, S.Sos.</i></div>
        </div>
        <div class="row">
            <div class="col offset-md-5 font-size-medium"><i>Moh. Wahyu Syafi’ul Mubarok</i></div>
        </div>
        <div class="row">
            <div class="col offset-md-5 font-size-medium"><i>Yualina Riastuti Partiwi</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Treasury</div>
            <div class="col font-size-medium"><i>Adit Tri Wiguno, SE</i></div>
        </div>
        <div class="row">
            <div class="col offset-md-5 font-size-medium"><i>Noer'aida</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Coordinator of Conference</div>
            <div class="col font-size-medium"><i>Dr. Alfian Ferdiansyah Madsuha</i></div>
        </div>
        <div class="row">
            <div class="col offset-md-5 font-size-medium"><i>Dr. Teguh Yulius Pancaputra</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Coordinator of Workshop and Exhibition</div>
            <div class="col font-size-medium"><i>Cipta Panghegar</i></div>
        </div>
        <div class="row">
            <div class="col offset-md-5 font-size-medium"><i>M. Fakhrudin</i></div>
        </div>
        <div class="row">
            <div class="col offset-md-5 font-size-medium"><i>Muhammad Ridho Nugraha</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Coordinator for Journal Publication</div>
            <div class="col font-size-medium"><i>Rida Nurul Shelni Rofika</i></div>
        </div>
        <div class="row">
            <div class="col offset-md-5 font-size-medium"><i>Rizka Ayu Puspita</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">International Relation</div>
            <div class="col font-size-medium"><i>M. Firmansyah, S.E.</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Press Conference</div>
            <div class="col font-size-medium"><i>Subhan Al-kiana</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Easychair</div>
            <div class="col font-size-medium"><i>Dr. Nofrijon Sofyan</i></div>
        </div>
        <div class="row">
            <div class="col offset-md-5 font-size-medium"><i>Dr. Sudaryanto</i></div>
        </div>
        <div class="row">
            <div class="col-md-5 font-size-medium font-bold">Webmaster</div>
            <div class="col font-size-medium"><i>Raditya Baskoro</i></div>
        </div>
    </div>
</section>