<section id="abstract-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="text-navy font-light font-size-medium">
                  Please find the detailed agenda as well as the presentation schedule in the program book. You may download it <a download href="<?=base_url('assets/file/icb-rev-2021_program-book.pdf')?>" class="font-bold text-yellow">here</a>.
                </p>
            </div>
        </div>
    </div>
</section>