<section id="abstract-section" class="page-section py-80">
    <div class="container">
        <div class="row">
        <div class="col-md-12">
                <!-- <h2 class="section-title text-yellow">Abstract and Paper Submission</h2> -->
                <p class="text-navy font-light font-size-medium">
                All paper works presented in the oral presentations after reviewed wil be published in one of these publications:
                </p>

                <ol class="font-size-medium text-navy font-light">
                    <li>Progress in Natural Science: Materials International (Q1, Scopus, Impact Factor (IF) 1.753, 5 year IF 2.113). (<a class="text-yellow font-light" href="https://www.journals.elsevier.com/progress-in-natural-science-materials-international/">https://www.journals.elsevier.com/progress-in-natural-science-materials-international/</a>)</li>
                    <li>IONICS (Q2, Scopus, IF 2.119). (<a class="text-yellow font-light" href="https://link.springer.com/journal/11581">https://link.springer.com/journal/11581</a>)</li>
                    <!-- <li>Atom Indonesia Journal (Scopus, Accreditation A by LIPI & Ristekdikti). (<a class="text-yellow font-light" href="http://aij.batan.go.id/">http://aij.batan.go.id/</a> )</li> -->
                    <li>International Proceeding (Indexed by Scopus):
                        <ul>
                            <li>American Institute of Physics (AIP) Conference Proceedings (<a class="text-yellow font-light" href="https://aip.scitation.org/">https://aip.scitation.org/</a>)</li>
                        </ul>
                    </li>
                </ol>
                <p class="text-navy font-light font-size-medium">
                    The presenting author should update any changes from the final paper; such as the title and/or author information. This can be done directly via <a class="font-light font-size-medium text-yellow" href="https://easychair.org/conferences/?conf=icbrev2021" target="_blank">Easychair</a>.
                </p>
            </div>
        </div>
    </div>
</section>