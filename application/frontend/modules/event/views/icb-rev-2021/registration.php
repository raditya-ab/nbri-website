<style>
    .control-label {
        font-size: 18px;
    }
</style>
<section id="registration-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div id="smartwizard" class="col-md-12">
                <ul class="nav">
                    <li>
                        <a class="nav-link font-size-small" href="#step-1">
                            Event Category
                        </a>
                    </li>
                    <li>
                        <a class="nav-link font-size-small" href="#step-2">
                            Fill your details
                        </a>
                    </li>
                </ul>

                <?php echo form_open(site_url('event/icbrev2021/verify_registration'),array('id'=>'form-registration-event','method'=>'post'));?>
                    <input type="hidden" name="currency">
                    <input type="hidden" name="currency_code">
                    <input type="hidden" name="normal_price">
                    <input type="hidden" name="early_bird_price">
                    <input type="hidden" name="member_discount" value="0">
                    <input type="hidden" name="conference-package-text" id="conference-package-text">


                    <div class="tab-content">
                        <div id="step-1" class="tab-pane" role="tabpanel">
                            <div class="row">
                                <div class="form-group col-md-12 required-field">
                                    <label class="control-label required">Select Category</label>
                                    <select name="event_category" id="event_category" class="form-control" required>
                                        <option value="">Select event</option>
                                        <option value="conference">International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2021</option>
                                        <!-- <option value="school">International School of Battery Technology</option> -->
                                        <!-- <option value="exhibition">Exhibitions (Research Centers, Universities, and Industries)</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="row registration-form_field conference-form_field" style="display: none">
                                <div class="form-group col-md-12">
                                    <label class="control-label required">Select Nationality</label>
                                    <select name="nationality" id="nationality" class="form-control" required>
                                        <option value="">Select nationality</option>
                                        <option value="indonesian">Indonesian</option>
                                        <option value="international">International</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">Select Occupation</label>
                                    <select name="occupation" id="occupation" class="form-control">
                                        <option value="">Select occupation</option>
                                        <option value="student">Student</option>
                                        <option value="regular">Regular</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 required-field">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label required">Select package</label>
                                        </div>
                                        <div class="col-md-12 d-flex justify-content-start flex-wrap mb-3">
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option international-option student-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <span class="normal-price font-size-tiny"><del>US$ 300</del></span>
                                                    <br/>
                                                    <span class="discount-price font-size-biggest font-bold">US$ 250<sup>*</sup></span>
                                                </div>
                                                <div class="price-description">
                                                    <small>*before June 14th, 2021</small>
                                                    <label for="presenter_student-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Participate all conference programs</li>
                                                        <li>Oral presentation</li>
                                                        <li>International certificate</li>
                                                        <li>International publication</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="presenter_student-international" value="presenter_student-international" data-text-value="Presenter (Student)" data-currency_price="$" data-normal_price="300" data-early_bird_price="250">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option international-option student-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <br/>
                                                    <span class="discount-price font-size-biggest font-bold">US$ 100</span>
                                                </div>
                                                <div class="price-description">
                                                    <small>&nbsp;</small>
                                                    <label for="participant_student-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Attend all conference programs</li>
                                                        <li>International certificate</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="participant_student-international" value="participant_student-international" data-text-value="Participant (Student)" data-currency_price="$" data-normal_price="100" data-early_bird_price="100">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option international-option regular-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <span class="normal-price font-size-tiny"><del>US$ 400</del></span>
                                                    <br/>
                                                    <span class="discount-price font-size-biggest font-bold">US$ 350<sup>*</sup></span>
                                                </div>
                                                <div class="price-description">
                                                    <small>*before June 14th, 2021</small>
                                                    <label for="presenter_regular-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Participate all conference programs</li>
                                                        <li>Oral presentation</li>
                                                        <li>International certificate</li>
                                                        <li>International publication</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="presenter_regular-international" value="presenter_regular-international" data-text-value="Presenter (Regular)" data-currency_price="$" data-normal_price="400" data-early_bird_price="350">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option international-option regular-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">US$ 150</span>
                                                </div>
                                                <div class="price-description">
                                                    <small>&nbsp;</small>

                                                    <label for="participant_regular-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Attend all conference programs</li>
                                                        <li>International certificate</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="participant_regular-international" value="participant_regular-international" data-text-value="Participant (Regular)" data-currency_price="$" data-normal_price="150" data-early_bird_price="150">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option indonesian-option student-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <span class="normal-price font-size-tiny"><del>Rp 2.500.000</del></span>
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">Rp 2.000.000<sup>*</sup></span>
                                                </div>
                                                <div class="price-description">
                                                    <small>*before June 14th, 2021</small>
                                                    <label for="presenter_student-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Participate all conference programs</li>
                                                        <li>Oral presentation</li>
                                                        <li>International certificate</li>
                                                        <li>International publication</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="presenter_student-indonesian" value="presenter_student-indonesian" data-text-value="Presenter (Student)" data-currency_price="Rp" data-normal_price="2500000" data-early_bird_price="2000000">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option indonesian-option student-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">Rp 500.000</span>
                                                </div>
                                                <div class="price-description">
                                                    <small>&nbsp;</small>
                                                    <label for="participant_student-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Attend all conference programs</li>
                                                        <li>International certificate</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="participant_student-indonesian" value="participant_student-indonesian" data-text-value="Participant (Student)" data-currency_price="Rp" data-normal_price="500000" data-early_bird_price="500000">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option indonesian-option regular-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <span class="normal-price font-size-tiny"><del>Rp 3.000.000</del></span>
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">Rp 2.500.000<sup>*</sup></span>
                                                </div>
                                                <div class="price-description">
                                                    <small>*before June 14th, 2021</small>
                                                    <label for="presenter_regular-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Participate all conference programs</li>
                                                        <li>Oral presentation</li>
                                                        <li>International certificate</li>
                                                        <li>International publication</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="presenter_regular-indonesian" value="presenter_regular-indonesian" data-text-value="Presenter (Regular)" data-currency_price="Rp" data-normal_price="3000000" data-early_bird_price="2500000">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option indonesian-option regular-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">Rp 1.500.000</span>
                                                </div>
                                                <div class="price-description">
                                                    <small>&nbsp;</small>
                                                    <label for="participant_regular-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Attend all conference programs</li>
                                                        <li>International certificate</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="participant_regular-indonesian" value="participant_regular-indonesian" data-text-value="Participant (Regular)" data-currency_price="Rp" data-normal_price="1500000" data-early_bird_price="1500000">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <p>(Member discount will be applied at total, not a member? <a class="font-medium text-yellow" href="">Register here</a>)</p>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <!-- <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Modal title</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                            </div>
                                            <div class="modal-body">
                                                Body
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-12 form-group required-field conference-field-only">
                                    <label for="symposia" class="control-label required">Symposia <a href="#" data-toggle="modal" data-target="#modelId" title="Hints"><i class="fas fa-question-circle"></i></a></label>
                                    <select class="form-control" name="symposia" id="symposia" required>
                                        <option value="">Select option</option>
                                        <option value="Advanced battery technology from raw materials to cell fabrication">Advanced battery technology from raw materials to cell fabrication</option>
                                        <option value="Energy storage for renewable energy (solar, wind, biomass, etc)">Energy storage for renewable energy (solar, wind, biomass, etc)</option>
                                        <option value="Battery electric vehicles, battery swap, and charging station">Battery electric vehicles, battery swap, and charging station</option>
                                        <option value="Other related topics (policy, regulation, standardization, industry, etc)">Other related topics (policy, regulation, standardization, industry, etc)</option>
                                    </select>
                                </div>
                                <div class="col-12 form-group required-field conference-field-only">
                                    <label for="abstract-title" class="control-label required">Abstract Title</label>
                                    <input type="text" name="conference_abstract-title" id="conference_abstract-title" class="form-control" required>
                                </div>
                            </div>
                            <!-- <div class="row registration-form_field school-form_field" style="display: none">
                                <div class="form-group col-md-12">
                                    <label class="control-label required">Select Nationality</label>
                                    <select name="nationality" id="nationality" class="form-control" required>
                                        <option value="">Select nationality</option>
                                        <option value="indonesian">Indonesian</option>
                                        <option value="international">International</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">Select Occupation</label>
                                    <select name="occupation" id="occupation" class="form-control">
                                        <option value="">Select occupation</option>
                                        <option value="student">Student</option>
                                        <option value="regular">Regular</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 required-field">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="control-label required">Select package</label>
                                        </div>
                                        <div class="col-md-12 d-flex justify-content-start flex-wrap mb-3">
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option international-option student-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <span class="normal-price font-size-tiny"><del>US$ 300</del></span>
                                                    <br/>
                                                    <span class="discount-price font-size-biggest font-bold">US$ 250<sup>*</sup></span>
                                                </div>
                                                <div class="price-description">
                                                    <small>*before June 14th, 2021</small>
                                                    <label for="presenter_student-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Participate all conference programs</li>
                                                        <li>Oral presentation</li>
                                                        <li>International certificate</li>
                                                        <li>International publication</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="presenter_student-international" value="presenter_student-international" data-text-value="Presenter (Student)" data-currency_price="$" data-normal_price="300" data-early_bird_price="250">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option international-option student-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <br/>
                                                    <span class="discount-price font-size-biggest font-bold">US$ 100</span>
                                                </div>
                                                <div class="price-description">
                                                    <small>&nbsp;</small>
                                                    <label for="participant_student-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Attend all conference programs</li>
                                                        <li>International certificate</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="participant_student-international" value="participant_student-international" data-text-value="Participant (Student)" data-currency_price="$" data-normal_price="100" data-early_bird_price="100">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option international-option regular-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <span class="normal-price font-size-tiny"><del>US$ 400</del></span>
                                                    <br/>
                                                    <span class="discount-price font-size-biggest font-bold">US$ 350<sup>*</sup></span>
                                                </div>
                                                <div class="price-description">
                                                    <small>*before June 14th, 2021</small>
                                                    <label for="presenter_regular-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Participate all conference programs</li>
                                                        <li>Oral presentation</li>
                                                        <li>International certificate</li>
                                                        <li>International publication</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="presenter_regular-international" value="presenter_regular-international" data-text-value="Presenter (Regular)" data-currency_price="$" data-normal_price="400" data-early_bird_price="350">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option international-option regular-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">US$ 150</span>
                                                </div>
                                                <div class="price-description">
                                                    <small>&nbsp;</small>

                                                    <label for="participant_regular-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Attend all conference programs</li>
                                                        <li>International certificate</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="participant_regular-international" value="participant_regular-international" data-text-value="Participant (Regular)" data-currency_price="$" data-normal_price="150" data-early_bird_price="150">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option indonesian-option student-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <span class="normal-price font-size-tiny"><del>Rp 2.500.000</del></span>
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">Rp 2.000.000<sup>*</sup></span>
                                                </div>
                                                <div class="price-description">
                                                    <small>*before June 14th, 2021</small>
                                                    <label for="presenter_student-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Participate all conference programs</li>
                                                        <li>Oral presentation</li>
                                                        <li>International certificate</li>
                                                        <li>International publication</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="presenter_student-indonesian" value="presenter_student-indonesian" data-text-value="Presenter (Student)" data-currency_price="Rp" data-normal_price="2500000" data-early_bird_price="2000000">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option indonesian-option student-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">Rp 500.000</span>
                                                </div>
                                                <div class="price-description">
                                                    <small>&nbsp;</small>
                                                    <label for="participant_student-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Attend all conference programs</li>
                                                        <li>International certificate</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="participant_student-indonesian" value="participant_student-indonesian" data-text-value="Participant (Student)" data-currency_price="Rp" data-normal_price="500000" data-early_bird_price="500000">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option indonesian-option regular-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <span class="normal-price font-size-tiny"><del>Rp 3.000.000</del></span>
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">Rp 2.500.000<sup>*</sup></span>
                                                </div>
                                                <div class="price-description">
                                                    <small>*before June 14th, 2021</small>
                                                    <label for="presenter_regular-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Participate all conference programs</li>
                                                        <li>Oral presentation</li>
                                                        <li>International certificate</li>
                                                        <li>International publication</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="presenter_regular-indonesian" value="presenter_regular-indonesian" data-text-value="Presenter (Regular)" data-currency_price="Rp" data-normal_price="3000000" data-early_bird_price="2500000">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option indonesian-option regular-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                                </div>
                                                <div class="price-container mb-5">
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold">Rp 1.500.000</span>
                                                </div>
                                                <div class="price-description">
                                                    <small>&nbsp;</small>
                                                    <label for="participant_regular-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <li>Attend all conference programs</li>
                                                        <li>International certificate</li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="conference-package" type="radio" id="participant_regular-indonesian" value="participant_regular-indonesian" data-text-value="Participant (Regular)" data-currency_price="Rp" data-normal_price="1500000" data-early_bird_price="1500000">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <p>(Member discount will be applied at total, not a member? <a class="font-medium text-yellow" href="">Register here</a>)</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 form-group required-field conference-field-only">
                                    <label for="symposia" class="control-label required">Symposia <a href="#" data-toggle="modal" data-target="#modelId" title="Hints"><i class="fas fa-question-circle"></i></a></label>
                                    <select class="form-control" name="symposia" id="symposia" required>
                                        <option value="">Select option</option>
                                        <option value="Advanced battery technology from raw materials to cell fabrication">Advanced battery technology from raw materials to cell fabrication</option>
                                        <option value="Energy storage for renewable energy (solar, wind, biomass, etc)">Energy storage for renewable energy (solar, wind, biomass, etc)</option>
                                        <option value="Battery electric vehicles, battery swap, and charging station">Battery electric vehicles, battery swap, and charging station</option>
                                        <option value="Other related topics (policy, regulation, standardization, industry, etc)">Other related topics (policy, regulation, standardization, industry, etc)</option>
                                    </select>
                                </div>
                                <div class="col-12 form-group required-field conference-field-only">
                                    <label for="abstract-title" class="control-label required">Abstract Title</label>
                                    <input type="text" name="conference_abstract-title" id="conference_abstract-title" class="form-control" required>
                                </div>
                            </div> -->
                        </div>
                        <div id="step-2" class="tab-pane" role="tabpanel">
                            <div class="row">
                                <div class="col-md-10 form-group required-field">
                                    <label for="salutation" class="control-label required">Prefixes</label>
                                    <select class="form-control" name="salutation" id="salutation" required>
                                        <option value="">Select option</option>
                                        <option value="Prof.">Prof.</option>
                                        <option value="Dr.">Dr.</option>
                                        <option value="PhD">PhD</option>
                                        <option value="Mr.">Mr</option>
                                        <option value="Mrs.">Mrs</option>
                                        <option value="Ms.">Ms.</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <div class="col-md-2 form-group required-field" id="prefix_other" style="display: none">
                                    <label for="salutation-other" class="control-label required">Enter Prefix</label>
                                    <input type="text" name="salutation-other" id="salutation-other" class="form-control" disabled>
                                </div>
                                <div class="col-md-6 form-group required-field">
                                    <label for="first_name" class="control-label required">Firstname</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="last_name" class="control-label">Lastname</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control">
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="suffixes" class="control-label">Suffixes</label>
                                    <input type="text" name="suffixes" id="suffixes" class="form-control">
                                </div>
                                <div class="col-md-12 form-group required-field">
                                    <label for="email" class="control-label required">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" required>
                                </div>
                                <div class="col-md-12 form-group required-field registration-form_field conference-form_field" style="display: none">
                                    <label for="affiliation" class="control-label required">Affiliation / Institution</label>
                                    <input type="text" name="affiliation" id="affiliation-conference" class="form-control" required>
                                </div>
                                <div class="col-md-12 form-group required-field registration-form_field school-form_field" style="display: none">
                                    <label for="affiliation" class="control-label required">Affiliation / Institution</label>
                                    <input type="text" name="affiliation" id="affiliation-school" class="form-control" required>
                                </div>
                                <div class="col-md-12 form-group required-field registration-form_field exhibition-form_field" style="display: none">
                                    <label for="affiliation" class="control-label required">Affiliation / Institution</label>
                                    <select class="form-control" name="affiliation" id="affiliation-exhibition" required>
                                        <option value="">Select option</option>
                                        <option value="Research Center">Research Center</option>
                                        <option value="University">University</option>
                                        <option value="Industry">Industry</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group required-field">
                                    <label for="address" class="control-label required">Address</label>
                                    <input type="text" name="address" id="address" class="form-control" required>
                                </div>
                                <div class="col-md-12 form-group required-field">
                                    <label for="phone" class="control-label" required>Phone</label>
                                    <input type="text" name="phone" id="phone" class="form-control" required>
                                </div>
                                <div class="col-md-12 form-group required-field">
                                    <label for="address" class="control-label required">How do you know about this event</label>
                                    <select class="form-control" name="referer" id="referer" required>
                                        <option value="">Select option</option>
                                        <option value="NBRI Website">NBRI Website</option>
                                        <option value="NBRI Social Media">NBRI Social Media</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group required-field" id="referer_other" style="display: none">
                                    <label for="address" class="control-label required">Enter Referer</label>
                                    <input type="text" name="referer_other" id="referer_other" class="form-control" required disabled>
                                </div>
                            </div>
                        </div>
                        
                        <!-- <div id="step-3" class="tab-pane" role="tabpanel">
                            
                        </div> -->
                    </div>
                    
                    <div class="row mt-5">
                        <div class="col d-flex justify-content-between">
                            <div class="form-group">
                                <button class="btn btn-custom text-navy" type="button" style="background: #DDD">Reset
                                    Form</button>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-custom bg-navy text-yellow sw-btn-prev" type="button">Previous</button>
                                    <button class="btn btn-custom bg-navy text-yellow sw-btn-next" type="button">Next</button>
                                    <button class="btn btn-custom bg-green text-navy" id="btn-checkout" type="submit" style="display: none">Proceed to Checkout</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php echo form_close();?>
        </div>
        <script type="text/javascript">
            // var currentTab = 0; // Current tab is set to be the first tab (0)
            // showTab(currentTab); // Display the current tab

            // function showTab(n) {
            //     // This function will display the specified tab of the form ...
            //     var x = document.getElementsByClassName("tab");
            //     x[n].style.display = "block";
            //     // ... and fix the Previous/Next buttons:
            //     if (n == 0) {
            //         document.getElementById("prevBtn").style.display = "none";
            //     } else {
            //         document.getElementById("prevBtn").style.display = "inline";
            //     }
            //     if (n == (x.length - 1)) {
            //         document.getElementById("nextBtn").innerHTML = "Submit";
            //     } else {
            //         document.getElementById("nextBtn").innerHTML = "Next";
            //     }
            //     // ... and run a function that displays the correct step indicator:
            //     fixStepIndicator(n)
            // }

            // function nextPrev(n) {
            //     // This function will figure out which tab to display
            //     var x = document.getElementsByClassName("tab");
            //     // Exit the function if any field in the current tab is invalid:
            //     if (n == 1 && !validateForm()) return false;
            //     // Hide the current tab:
            //     x[currentTab].style.display = "none";
            //     // Increase or decrease the current tab by 1:
            //     currentTab = currentTab + n;
            //     // if you have reached the end of the form... :
            //     if (currentTab >= x.length) {
            //         //...the form gets submitted:
            //         document.getElementById("regForm").submit();
            //         return false;
            //     }
            //     // Otherwise, display the correct tab:
            //     showTab(currentTab);
            // }

            // function validateForm() {
            //     // This function deals with validation of the form fields
            //     var x, y, i, valid = true;
            //     x = document.getElementsByClassName("tab");
            //     y = x[currentTab].getElementsByTagName("input");
            //     // A loop that checks every input field in the current tab:
            //     for (i = 0; i < y.length; i++) {
            //         // If a field is empty...
            //         if (y[i].value == "") {
            //             // add an "invalid" class to the field:
            //             y[i].className += " invalid";
            //             // and set the current valid status to false:
            //             valid = false;
            //         }
            //     }
            //     // If the valid status is true, mark the step as finished and valid:
            //     if (valid) {
            //         document.getElementsByClassName("step")[currentTab].className += " finish";
            //     }
            //     return valid; // return the valid status
            // }

            // function fixStepIndicator(n) {
            //     // This function removes the "active" class of all steps...
            //     var i, x = document.getElementsByClassName("step");
            //     for (i = 0; i < x.length; i++) {
            //         x[i].className = x[i].className.replace(" active", "");
            //     }
            //     //... and adds the "active" class to the current step:
            //     x[n].className += " active";
            // }

        </script>
    </div>
</section>