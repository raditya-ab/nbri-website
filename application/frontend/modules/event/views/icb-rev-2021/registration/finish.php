<section id="checkout-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 d-flex justify-content-center align-items-center" style="height: 60vh">
                <?php if($status == 'success'):?>
                <div class="success-message text-center">
                    <i class="far fa-5x fa-check-circle text-green mb-3"></i>
                    <h2 class="font-bold font-size-biggest">REGISTRATION SUCCESS</h2>
                    <p>You'll be redirected to event homepage in 3 seconds</p>
                    <a href="#">Click here if auto redirect is not working</a>
                </div>
                <?php else:?>
                <div class="success-message text-center">
                    <i class="far fa-5x fa-check-circle mb-3" style="color: red"></i>
                    <h2 class="font-bold font-size-biggest">REGISTRATION FAILED</h2>
                    <p>You'll be redirected to event homepage in 3 seconds</p>
                    <a href="#">Click here if auto redirect is not working</a>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>


<script>

    setTimeout(function(){
        window.location.href = "<?php echo site_url('event/icb-rev-2021');?>";
    }, 3000)
</script>