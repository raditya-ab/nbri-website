
<style>
    .control-label {
        font-size: 18px;
    }
</style>
<section id="registration-section" class="page-section py-80">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 font-bold bg-yellow" href="#">Choose your event</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 bg-navy" href="?step=2">Fill in your details</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 bg-navy" href="?step=3">Checkout &amp; Payment</a>
                    </li>
                </ul>
            </div>
            <?php if(array_key_exists('error',$_SESSION)):?>
                <div class="col-md-12 mt-5">
                    <div class="alert alert-danger p-5 alert-dismissible fade show font-size-medium" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong>Error!</strong> <br><?php echo $_SESSION['error']['text'];?>
                    </div>
                </div>
            <?php endif;?>
        </div>
        <?php echo form_open(site_url('event/icb-rev-2021/registration/verify'),[
                'id' => 'form-registration-event',
                'class' => 'form',
                'method' => 'post'
            ], [
                'current_step' => $_SESSION['current_step'],
                'event_category_name' => '',
                'event_package_type' => ''
            ]);
        ?>
        <div class="row mt-5">
            <div class="col-md-12">
                

                <div class="form-group">
                    <label for="" class="control-label">Select Event Category</label>
                    <div class="grid-card-wrapper grid-3 align-items-center justify-content-between">
                        <div class="card">
                            <label class="form-check-label" for="conference">
                                <div class="card-image">
                                    <img src="<?php echo base_url('assets/images/conference.png');?>"
                                        alt="International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2021">
                                </div>
                                <div class="card-caption text-center">
                                    <h3 class="card-title">International Conference on Battery for Renewable
                                        Energy and Electric Vehicles (ICB-REV) 2021</h3>
                                    <div class="form-check">
                                        <input class="form-check-input" name="event_category" id="conference" type="radio" value="conference" data-text="International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2021">
                                    </div>
                                </div>
                            </label>
                        </div>
                        <div class="card">
                            <label class="form-check-label" for="school">
                                <div class="card-image">
                                    <img src="<?php echo base_url('assets/images/school.png');?>" alt="">
                                </div>
                                <div class="card-caption text-center">
                                    <h3 class="card-title">International School of Battery Technology</h3>
                                    <div class="form-check">
                                        <input class="form-check-input" name="event_category" id="school" type="radio" value="school" data-text="International School of Battery Technology">
                                    </div>
                                </div>
                            </label>
                        </div>
<!--                        <div class="card">-->
<!--                            <label class="form-check-label" for="exhibition">-->
<!--                                <div class="card-image">-->
<!--                                    <img src="--><?php //echo base_url('assets/images/exhibition.png');?><!--" alt="">-->
<!--                                </div>-->
<!--                                <div class="card-caption text-center">-->
<!--                                    <h3 class="card-title">Exhibitions (Research Centers, Universities, and-->
<!--                                        Industries)</h3>-->
<!--                                    <div class="form-check">-->
<!--                                        <input class="form-check-input" name="event_category" id="exhibition" type="radio" value="exhibition" data-text="Exhibitions (Research Centers, Universities, and Industries)">-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </label>-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label required">Select Nationality</label>
                    <select name="nationality" id="nationality" class="form-control" required>
                        <option value="">Select nationality</option>
                        <option value="indonesian">Indonesian</option>
                        <option value="international">International</option>
                    </select>
                </div>
                <div id="conference_form_field" class="form_field_group" style="display: none">
                    <div class="form-group">
                        <label class="control-label">Select Occupation</label>
                        <select name="occupation" id="conference_occupation" class="form-control">
                            <option value="">Select occupation</option>
                            <option value="student">Student</option>
                            <option value="regular">Regular</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="" class="control-label">Select Event Package</label>
                        <div class="grid-card-wrapper grid-4 align-items-stretch justify-content-around mb-5 package_list" id="conference_package_list">
                            <div class="price-card bg-green p-5 text-center mb-3 package-option presenter-option international-option student-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <span class="normal-price font-size-tiny"><del>US$ 300</del></span>
                                    <br/>
                                    <span class="discount-price font-size-biggest font-bold">US$ 250<sup>*</sup></span>
                                </div>
                                <div class="price-description">
                                    <small>*before June 14th, 2021</small>
                                    <label for="presenter_student-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Participate all conference programs</li>
                                        <li>Oral presentation</li>
                                        <li>International certificate</li>
                                        <li>International publication</li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="presenter_student-international" value="presenter_student-international" data-type="presenter" data-text-value="Presenter (Student)" data-currency_price="$" data-normal_price="300" data-early_bird_price="250">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option participant-option international-option student-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <br/>
                                    <span class="discount-price font-size-biggest font-bold">US$ 100</span>
                                </div>
                                <div class="price-description">
                                    <small>&nbsp;</small>
                                    <label for="participant_student-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Attend all conference programs</li>
                                        <li>International certificate</li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="participant_student-international" value="participant_student-international" data-type="participant" data-text-value="Participant (Student)" data-currency_price="$" data-normal_price="100" data-early_bird_price="100">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option presenter-option international-option regular-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <span class="normal-price font-size-tiny"><del>US$ 400</del></span>
                                    <br/>
                                    <span class="discount-price font-size-biggest font-bold">US$ 350<sup>*</sup></span>
                                </div>
                                <div class="price-description">
                                    <small>*before June 14th, 2021</small>
                                    <label for="presenter_regular-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Participate all conference programs</li>
                                        <li>Oral presentation</li>
                                        <li>International certificate</li>
                                        <li>International publication</li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="presenter_regular-international" value="presenter_regular-international" data-type="presenter" data-text-value="Presenter (Regular)" data-currency_price="$" data-normal_price="400" data-early_bird_price="350">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option participant-option international-option regular-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <br>
                                    <span class="discount-price font-size-biggest font-bold">US$ 150</span>
                                </div>
                                <div class="price-description">
                                    <small>&nbsp;</small>

                                    <label for="participant_regular-international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Attend all conference programs</li>
                                        <li>International certificate</li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="participant_regular-international" value="participant_regular-international" data-type="participant" data-text-value="Participant (Regular)" data-currency_price="$" data-normal_price="150" data-early_bird_price="150">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option presenter-option indonesian-option student-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <span class="normal-price font-size-tiny"><del>Rp 2.500.000</del></span>
                                    <br>
                                    <span class="discount-price font-size-biggest font-bold">Rp 2.000.000<sup>*</sup></span>
                                </div>
                                <div class="price-description">
                                    <small>*before June 14th, 2021</small>
                                    <label for="presenter_student-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Participate all conference programs</li>
                                        <li>Oral presentation</li>
                                        <li>International certificate</li>
                                        <li>International publication</li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="presenter_student-indonesian" value="presenter_student-indonesian" data-type="presenter" data-text-value="Presenter (Student)" data-currency_price="Rp" data-normal_price="2500000" data-early_bird_price="2000000">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option participant-option indonesian-option student-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <br>
                                    <span class="discount-price font-size-biggest font-bold">Rp 500.000</span>
                                </div>
                                <div class="price-description">
                                    <small>&nbsp;</small>
                                    <label for="participant_student-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Attend all conference programs</li>
                                        <li>International certificate</li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="participant_student-indonesian" value="participant_student-indonesian" data-type="participant" data-text-value="Participant (Student)" data-currency_price="Rp" data-normal_price="500000" data-early_bird_price="500000">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option presenter-option indonesian-option regular-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <span class="normal-price font-size-tiny"><del>Rp 3.000.000</del></span>
                                    <br>
                                    <span class="discount-price font-size-biggest font-bold">Rp 2.500.000<sup>*</sup></span>
                                </div>
                                <div class="price-description">
                                    <small>*before June 14th, 2021</small>
                                    <label for="presenter_regular-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Participate all conference programs</li>
                                        <li>Oral presentation</li>
                                        <li>International certificate</li>
                                        <li>International publication</li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="presenter_regular-indonesian" value="presenter_regular-indonesian" data-type="presenter" data-text-value="Presenter (Regular)" data-currency_price="Rp" data-normal_price="3000000" data-early_bird_price="2500000">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option participant-option indonesian-option regular-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <br>
                                    <span class="discount-price font-size-biggest font-bold">Rp 1.500.000</span>
                                </div>
                                <div class="price-description">
                                    <small>&nbsp;</small>
                                    <label for="participant_regular-indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Attend all conference programs</li>
                                        <li>International certificate</li>
                                    </ul>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="participant_regular-indonesian" value="participant_regular-indonesian" data-type="participant" data-text-value="Participant (Regular)" data-currency_price="Rp" data-normal_price="1500000" data-early_bird_price="1500000">
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="conference_presenter_field" style="display: none">
                        <div class="form-group required-field">
                            <label for="symposia" class="control-label required">Symposia <sup><a class="text-navy" href="#" data-toggle="modal" data-target="#modelId" title="Helpn  "><i class="fas fa-question-circle"></i></a></sup></label>
                            <select class="form-control no-radius" name="symposia" id="symposia" disabled>
                                <option value="">Select option</option>
                                <option value="Advanced battery technology from raw materials to cell fabrication">Advanced battery technology from raw materials to cell fabrication</option>
                                <option value="Energy storage for renewable energy (solar, wind, biomass, etc)">Energy storage for renewable energy (solar, wind, biomass, etc)</option>
                                <option value="Battery electric vehicles, battery swap, and charging station">Battery electric vehicles, battery swap, and charging station</option>
                                <option value="Other related topics (policy, regulation, standardization, industry, etc)">Other related topics (policy, regulation, standardization, industry, etc)</option>
                            </select>
                        </div>
                        <div class="form-group required-field">
                            <label for="abstract-title" class="control-label required">Abstract Title</label>
                            <input type="text" name="abstract_title" id="abstract_title" class="form-control" disabled>
                        </div>
                    </div>
                </div>
                <div id="school_form_field" class="form_field_group" style="display: none">
                    <div class="form-group">
                        <label class="control-label">Select Occupation</label>
                        <select name="occupation" id="school_occupation" class="form-control">
                            <option value="">Select occupation</option>
                            <option value="student-and-rnd">Student and R&amp;D</option>
                            <option value="industry">Industries</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Select Event Package</label>
                        <div class="grid-card-wrapper grid-4 align-items-stretch justify-content-around mb-5 package_list" id="school_package_list">
                            <div class="price-card bg-green p-5 text-center mb-3 package-option school-option international-option student-and-rnd-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">International <br>Student and R&D</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <span class="normal-price font-size-tiny">&nbsp;</span>
                                    <br/>
                                    <span class="discount-price font-size-biggest font-bold">US$ 100</span>
                                </div>
                                <div class="price-description">
                                    <small>&nbsp;</small>
                                    <label for="student-and-rnd_international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <!-- <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Participate all conference programs</li>
                                        <li>Oral presentation</li>
                                        <li>International certificate</li>
                                        <li>International publication</li>
                                    </ul> -->
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="student-and-rnd_international" value="student-and-rnd_international" data-type="school" data-text-value="International Student and R&D" data-currency_price="$" data-normal_price="100" data-early_bird_price="100">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option school-option international-option industry-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">International <br>Industry</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <span class="normal-price font-size-tiny">&nbsp;</span>
                                    <br/>
                                    <span class="discount-price font-size-biggest font-bold">US$ 200</span>
                                </div>
                                <div class="price-description">
                                    <small>&nbsp;</small>
                                    <label for="industry_international" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <!-- <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Participate all conference programs</li>
                                        <li>Oral presentation</li>
                                        <li>International certificate</li>
                                        <li>International publication</li>
                                    </ul> -->
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="industry_international" value="industry_international" data-type="school" data-text-value="International Industries" data-currency_price="$" data-normal_price="200" data-early_bird_price="200">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option school-option indonesian-option student-and-rnd-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Indonesian <br>Student and R&D</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <span class="normal-price font-size-tiny">&nbsp;</span>
                                    <br/>
                                    <span class="discount-price font-size-biggest font-bold">Rp 1.000.000</span>
                                </div>
                                <div class="price-description">
                                    <small>&nbsp;</small>
                                    <label for="student-and-rnd_indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <!-- <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Participate all conference programs</li>
                                        <li>Oral presentation</li>
                                        <li>International certificate</li>
                                        <li>International publication</li>
                                    </ul> -->
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="student-and-rnd_indonesian" value="student-and-rnd_indonesian" data-type="school" data-text-value="Indonesian Student and R&D" data-currency_price="Rp" data-normal_price="1000000" data-early_bird_price="1000000">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="price-card bg-green p-5 text-center mb-3 package-option school-option indonesian-option industry-option">
                                <div class="card-title-container mb-5">
                                    <h4 class="font-size-big font-weight-medium">Indonesian <br>Industry</h4>
                                </div>
                                <div class="price-container mb-5">
                                    <span class="normal-price font-size-tiny">&nbsp;</span>
                                    <br/>
                                    <span class="discount-price font-size-biggest font-bold">Rp 2.000.000</span>
                                </div>
                                <div class="price-description">
                                    <small>&nbsp;</small>
                                    <label for="industry_indonesian" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                    <hr>
                                    <!-- <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                        <li>Participate all conference programs</li>
                                        <li>Oral presentation</li>
                                        <li>International certificate</li>
                                        <li>International publication</li>
                                    </ul> -->
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" name="event_package" type="radio" id="industry_indonesian" value="industry_indonesian" data-type="school" data-text-value="Indonesian Industries" data-currency_price="Rp" data-normal_price="2000000" data-early_bird_price="2000000">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 d-flex justify-content-between">
                <div class="sub-container">
                    <a class="btn btn-custom bg-green text-navy">Reset</a>
                </div>
                <div class="sub-container">
                    <button class="btn btn-custom bg-navy text-yellow" type="submit">Next</button>
                </div>
            </div>
        </div>
        <?php echo form_close();?>
    </div>
</section>
