
<style>
    .control-label {
        font-size: 18px;
    }
</style>

<section id="registration-section" class="page-section py-80">
    <div class="container">
        <div class="row mb-5">
           <div class="col-md-12">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 bg-navy" href="?step=1">Choose your event</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 font-bold bg-yellow" href="#">Fill in your details</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 bg-navy" href="?step=3">Checkout &amp; Payment</a>
                    </li>
                </ul>
           </div>
           <?php if(array_key_exists('error',$_SESSION)):?>
                <div class="col-md-12 mt-5">
                    <div class="alert alert-danger p-5 alert-dismissible fade show font-size-medium" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong>Error!</strong> <br><?php echo $_SESSION['error']['text'];?>
                    </div>
                </div>
            <?php endif;?>
        </div>
        <?php echo form_open(site_url('event/icb-rev-2021/registration/verify'),[
                'id' => 'form-registration-event',
                'class' => 'form',
                'method' => 'post'
            ], [
                'current_step' => $_SESSION['current_step']
            ]);
        ?>
        <div class="row mt-5">
            <div class="col-md-12">
                
                <div class="form-group">
                    <label for="salutation" class="control-label required">Salutation</label>
                    <select class="form-control" name="salutation" id="prefix" required>
                        <option value="">Select option</option>
                        <option value="Prof.">Prof.</option>
                        <option value="Dr.">Dr.</option>
                        <option value="PhD">PhD</option>
                        <option value="Mr.">Mr</option>
                        <option value="Mrs.">Mrs</option>
                        <option value="Ms.">Ms.</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="form-group" id="prefix_other" style="display: none">
                    <label for="salutation" class="control-label required">Specify your Salutation</label>
                    <input type="text" name="salutation" class="form-control" disabled>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="salutation" class="control-label required">Firstname</label>
                        <input type="text" name="first_name" id="first_name" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="salutation" class="control-label required">Lastname</label>
                        <input type="text" name="last_name" id="last_name" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="salutation" class="control-label required">Suffix</label>
                    <input type="text" name="suffix" id="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="salutation" class="control-label required">Email</label>
                    <input type="email" name="email" id="email" class="form-control">
                </div>
                <?php if($_SESSION['registration_data']['step1'] == 'exhibition'):?>
                    <div class="form-group">
                        <label for="salutation" class="control-label required">Affiliation</label>
                        <select class="form-control" name="affiliation" id="affiliation-exhibition" required>
                            <option value="">Select option</option>
                            <option value="Research Center">Research Center</option>
                            <option value="University">University</option>
                            <option value="Industry">Industry</option>
                        </select>
                    </div>
                <?php else:?>
                    <div class="form-group">
                        <label for="salutation" class="control-label required">Affiliation / Institution</label>
                        <input type="text" name="affiliation" id="affiliation" class="form-control">
                    </div>
                <?php endif;?>
                <div class="form-group">
                    <label for="salutation" class="control-label required">Address</label>
                    <textarea name="address" id="address" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label for="salutation" class="control-label required">Phone No.</label>
                    <input type="text" name="phone" id="phone" class="form-control">
                </div>
                <div class="form-group">
                    <label for="salutation" class="control-label required">How do you know about this event?</label>
                    <select class="form-control" name="referer" id="referer" required>
                        <option value="">Select option</option>
                        <option value="NBRI Website">NBRI Website</option>
                        <option value="NBRI Social Media">NBRI Social Media</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="form-group" id="referer_other" style="display: none">
                    <label for="address" class="control-label required">Enter Referer</label>
                    <input type="text" name="referer_other" class="form-control" required disabled>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 d-flex justify-content-between">
                <div class="sub-container">
                    <button class="btn btn-custom bg-green text-navy">Reset</button>
                </div>
                <div class="sub-container">
                    <button class="btn btn-custom bg-navy text-yellow" type="submit">Next</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>

