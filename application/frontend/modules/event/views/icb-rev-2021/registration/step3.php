<section id="checkout-section" class="page-section py-80">
    <div class="container">
    <div class="row mb-5">
           <div class="col-md-12">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 bg-navy" href="?step=1">Choose your event</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 bg-navy" href="?step=2">Fill in your details</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 font-bold bg-yellow" href="3">Checkout &amp; Payment</a>
                    </li>
                </ul>
           </div>
    </div>
        <div class="row">
            <div class="col-md-12 mb-5">
                <h2>Checkout</h2>
            </div>
            <div class="col-md-8 pr-5">
                <?php
                    $registered_user_data = $this->session->userdata('registration_data');
                ?>
                <div class="form-group">
                    <label for="" class="font-size-small">Full name</label>
                    <p class="font-size-medium">
                        <?php echo $registered_user_data['step2']['salutation'].' '.$registered_user_data['step2']['first_name'].' '.$registered_user_data['step2']['last_name'].((isset($registered_user_data['step2']['suffix'])) ? ', '.$registered_user_data['step2']['suffix'] : '');?>
                    </p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Email address</label>
                    <p class="font-size-medium"><?php echo $registered_user_data['step2']['email'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Address</label>
                    <p class="font-size-medium"><?=$registered_user_data['step2']['address'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Phone number</label>
                    <p class="font-size-medium"><?=$registered_user_data['step2']['phone'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Institution / Affiliation</label>
                    <p class="font-size-medium"><?=$registered_user_data['step2']['affiliation'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Event Category</label>
                    <p class="font-size-medium">International Conference on Battery for Renewable Energy and Electric
                        Vehicles (ICB-REV) 2021</p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Package</label>
                    <p class="font-size-medium"><?=$package_info['package_name'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Nationality</label>
                    <p class="font-size-medium"><?=ucwords($registered_user_data['step1']['nationality']);?></p>
                </div>
                <?php if(stripos($registered_user_data['step1']['event_package_type'],'presenter')):?>
                <div class="form-group">
                    <label for="" class="font-size-small">Symposia</label>
                    <p class="font-size-medium"><?=$registered_user_data['step1']['symposia'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Abstract Title</label>
                    <p class="font-size-medium"><?=$registered_user_data['step1']['conference_abstract-title'];?></p>
                </div>
                <?php endif;?>
            </div>
            <div class="col-md-4 bg-navy p-5">
                <h2>Summary</h2>
                <hr class="border-green">
                <div class="row">
                    <div class="col-md-7">
                        <p class="font-size-medium font-thin">Subtotal</p>
                    </div>
                    <div class="col-md-5 text-right">
                        <p class="font-size-medium font-thin"><span id="currency"><?=$package_info['price_currency_symbol'];?> </span><span
                                id="subtotal"><?php echo number_format($package_info['price'],0,'','.');?></span></p>
                    </div>
                </div>
                <?php if(!empty($package_info['earlybird_exp_date']) && !empty($package_info['earlybird_price'])):?>
                    <div class="row">
                        <div class="col-md-7">
                            <p class="font-size-medium font-thin">Early-bird discount</p>
                        </div>
                        <div class="col-md-5 text-right">
                            <p class="font-size-medium font-thin">- <span id="currency"><?=$package_info['price_currency_symbol'];?> </span><span
                                    id="subtotal"><?php echo number_format(($package_info['price']-$package_info['earlybird_price']),0,'','.');?></span></p>
                        </div>
                    </div>
                <?php endif;?>
                <?php if($this->aauth->is_loggedin()):?>
                    <div class="row">
                        <div class="col-md-7">
                            <p class="font-size-medium font-thin">Additional discount</p>
                        </div>
                        <div class="col-md-5 text-right">
                            <p class="font-size-medium font-thin">- <span id="currency"><?=$package_info['price_currency_symbol'];?> </span><span
                                    id="subtotal"><?php echo number_format($package_info['member_discount_amount'],0,'','.');?></span></p>
                        </div>
                    </div>
                <?php else:?>
                    <div class="row">
                        <div class="col-md-7">
                            <p class="font-size-medium font-thin">Additional discount</p>
                        </div>
                        <div class="col-md-5 text-right">
                            <p class="font-size-medium font-thin">- <span id="currency"><?=$package_info['price_currency_symbol'];?> </span><span
                                    id="subtotal"><?php echo number_format(0,0,'','.');?></span></p>
                        </div>
                    </div>
                <?php endif;?>
                <hr class="border-green">
                <div class="row">
                    <div class="col-md-7">
                        <p class="font-size-medium font-medium">Total</p>
                    </div>
                    <div class="col-md-5 text-right">
                        <p class="font-size-medium font-medium"><span id="currency"><?=$package_info['price_currency_symbol'];?> </span><span
                                id="subtotal"><?php echo number_format($package_info['total'],0,'','.');?></span></p>
                    </div>
                </div>
                <?php if(array_key_exists('conversion', $package_info)):?>
                    <div class="row">
                        <div class="col-md-7">
                            <p class="font-size-medium font-medium">Total in IDR</p>
                            <a class="text-white" href="<?=$package_info['conversion']['license'];?>" target="_blank">
                                <span class="font-size-small font-regular">US$ 1 = IDR
                                    <?php echo number_format($package_info['conversion']['usd_to_idr'],2,',','.');?></span>
                                <sup class="text-yellow" title="rates from openexchangerates.org (per <?=date('F j, Y H:i:s',$package_info['conversion']['timestamp']);?> UTC)"><i
                                        class="fas fa-question-circle"></i></sup>

                            </a>
                        </div>
                        <div class="col-md-5 text-right">
                            <p class="font-size-medium font-medium"><span id="currency">Rp </span><span
                                    id="subtotal"><?php echo number_format($package_info['conversion']['usd_to_idr_total'],0,'','.');?></span></p>
                        </div>
                    </div>
                <?php endif;?>
                
                <div class="row mt-5">
                    <?php if($package_info['login_status']):?>
                    <div class="col-md-12">
                        <button class="btn btn-custom btn-block bg-yellow text-navy btn-checkout" type="submit">Checkout</button>
                    </div>
                    <?php else:?>
                    <div class="col-md-12">
                        <button class="btn btn-custom btn-block bg-yellow text-navy btn-checkout" type="submit">Guest Checkout</button>
                        <a class="btn btn-custom btn-block bg-yellow text-navy" href="<?=site_url('membership/login');?>">Member Checkout</a>
                    </div>
                    <div class="col-md-12 mt-3">
                        Not a member? <a href="<?=site_url('membership/login');?>" class="text-yellow">Click here to get 10% additional discount!</a>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</section>

<script src='<?php echo MIDTRANS_SNAP_JS_URL;?>' data-client-key='<?php echo MIDTRANS_CLIENT_KEY;?>>'></script>