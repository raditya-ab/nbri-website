<style>
    .card .card-image {
        background-color: #FFF;
    }
</style>
<section id="school-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-navy text-center">Lecturers</h2>
            </div>
            <div class="col-md-12 grid-card-wrapper grid-5 align-items-stretch justify-content-center">
                <div class="card mx-2">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_rer_nat_evvy_kartini.png');?>" alt="">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. rer. nat. Evvy Kartini</h3>
                            <p class="card-description">Founder of National Battery Research Institute and President of MRS-INA</p>
                        </div>
                    </a>
                </div>
                <div class="card mx-2">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_ir_anne_zulfia_syahrial_msc_1.png');?>" alt="Prof. Dr. Ir. Anne Zulfia Syahrial, M.Sc.">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. Ir. Anne Zulfia Syahrial, M.Sc.</h3>
                            <p class="card-description">Universitas Indonesia</p>
                        </div>
                    </a>
                </div>
                <div class="card mx-2">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_eng_agus_purwanto_st_mt_1.png');?>" alt="Prof. Dr. Eng. Agus Purwanto, S.T., M.T. ">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. Eng. Agus Purwanto, S.T., M.T. </h3>
                            <p class="card-description">Professor at Department of Chemical Engineering, Universitas Negeri Sebelas Maret, Indonesia</p>
                        </div>
                    </a>
                </div>
                <div class="card mx-2">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/ir_i_made_dana_tangkas_m_si_ipu_asean_eng_1.png');?>" alt="Ir. I Made Dana Tangkas, M.Si., IPU, ASEAN Eng">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Ir. I Made Dana Tangkas, M.Si., IPU, ASEAN Eng</h3>
                            <p class="card-description">Indonesia Automotive Institute </p>
                        </div>
                    </a>
                </div>
                <div class="card mx-2">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/arief_s_budiman_ph_d_1.png');?>" alt="Arief S. Budiman, Ph.D">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Arief S. Budiman, Ph.D</h3>
                            <p class="card-description">BINUS/NTU</p>
                        </div>
                    </a>
                </div>
                <div class="card mx-2">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/dr_alexey_glushenkov_1.png');?>" alt="Dr. Alexey Glushenkov">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Dr. Alexey Glushenkov</h3>
                            <p class="card-description">Australia National University (ANU), Australia</p>
                        </div>
                    </a>
                </div>
                
                
                
                <div class="card mx-2">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/ir_chairul_hudaya_st_meng_phd_ipm_1.png');?>" alt="Ir. Chairul Hudaya, ST, M.Eng., Ph.D, IPM">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Ir. Chairul Hudaya, ST, M.Eng., Ph.D, IPM</h3>
                            <p class="card-description">Universitas Indonesia</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <h2 class="section-title text-navy text-center">Mentors</h2>
            </div>
            <div class="col-md-12 grid-card-wrapper grid-5 align-items-stretch justify-content-center">  
                <div class="card mx-2">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/achmad_subhan_st_mt_1.png');?>" alt="Achmad Subhan, S.T., M.T., ">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Achmad Subhan, S.T., M.T., </h3>
                            <p class="card-description">Researcher at Indonesian Institute of Science – Research Center of Physics</p>
                        </div>
                    </a>
                </div>
                <div class="card mx-2">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/cipta_panghegar_supriadi_mt_1.png');?>" alt="Cipta Panghegar Supriadi, M.T.">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Cipta Panghegar Supriadi, M.T.</h3>
                            <p class="card-description">Senior Researcher at National Battery Research Institute</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row mt-5 text-navy">
            <div class="col-md-6">
                <h2 class="section-title text-navy text-center">What you will <b class="font-bold font-size-heading2">LEARN</b></h2>
                <div class="section-part">
                    <p class="font-bold font-size-small">LECTURES</p>
                    <ol>
                        <li class="font-size-small font-regular">Understanding How Battery Works</li>
                        <li class="font-size-small font-regular">Battery Cell Fabrication</li>
                        <li class="font-size-small font-regular">Battery Module Assembly</li>
                        <li class="font-size-small font-regular">Application</li>
                    </ol>
                </div>
                <div class="section-part">
                    <p class="font-bold font-size-small">DEMONSTRATION</p>
                    <ul>
                        <li class="font-size-small font-regular">Battery Cell Assembly and Performance Testing</li>
                    </ul>
                </div>
                
            </div>
            <div class="col-md-6">
                <h2 class="section-title text-navy text-center">What you will <b class="font-bold font-size-heading2">GET</b></h2>
                <div class="section-part">
                    <ol>
                        <li class="font-size-small font-regular">Learning Module</li>
                        <li class="font-size-small font-regular">E-Certificate</li>
                        <li class="font-size-small font-regular">NBRI Membership</li>
                        <li class="font-size-small font-regular">Free 10-Minutes consultation with the Experts</li>
                    </ol>
                </div>
                <div class="section-part mt-5">
                    <h2 class="section-title text-navy text-center">Venue</h2>
                    <div class="section-part">
                        <p class="font-bold font-size-small">Online Zoom Meeting</p>
                    </div>
                </div>
                <div class="section-part mt-5">
                    <h2 class="section-title text-navy text-center">Participant Fee</h2>
                    <ul>
                        <li class="font-size-small font-regular"><b class="font-size-small font-bold">IDR 2.000.000</b> for Participants from Industry</li>
                        <li class="font-size-small font-regular"><b class="font-size-small font-bold">IDR 1.000.000</b> for Participants from R&amp;D Institution</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="countdown-section" class="page-section py-80 text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="section-title">Register Now</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="countdown"></div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5">
                <a class="btn btn-custom bg-navy text-yellow" href="<?php echo site_url('event/icb-rev-2021/registration');?>">Register Now</a>
            </div>  
        </div>
    </div>
</section>