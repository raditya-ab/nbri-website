<style>
    .card .card-image {
        background-color: #FFF;
    }
</style>
<section id="speakers-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-navy">Opening Remarks</h2>
            </div>
            <div class="col-md-12 grid-card-wrapper grid-5 align-items-stretch justify-content-between">
                <div class="card">
                    <a href="javascript:void(0)" data-target="#dr_laksana_tri_handoko_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/dr_laksana_tri_handoko_1.png');?>" alt="Dr. Laksana Tri Handoko">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Dr. Laksana Tri Handoko</h3>
                            <p class="card-description">Head of National Research and Innovation Agency</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                <a href="javascript:void(0)" data-target="#prof_dr_b_v_r_chowdary_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_b_v_r_chowdary_1.png');?>" alt="Prof. Dr. B.V.R. Chowdary">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. B.V.R. Chowdary</h3>
                            <p class="card-description">Head of IUMRS</p>
                        </div>
                    </a>
                </div>
                <!-- <div class="card">
                    <a href="">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/erick_thohir_mba.png');?>" alt="">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Erick Thohir, M.B.A</h3>
                            <p class="card-description">Minister for State Owned Enterprises Republic of Indonesia</p>
                        </div>
                    </a>
                </div> -->
                <div class="card">
                <a href="javascript:void(0)" data-target="#prof_colin_gareth_bailey" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_colin_gareth_bailey.png');?>" alt="">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Colin Gareth Bailey</h3>
                            <p class="card-description">President and Principal of Queen Mary University of London</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_dr_rer_nat_evvy_kartini" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_rer_nat_evvy_kartini.png');?>" alt="">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. rer. nat. Evvy Kartini</h3>
                            <p class="card-description">Founder of National Battery Research Institute and President of MRS-INA</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-navy">Plenary Speakers</h2>
            </div>
            <div class="col-md-12 grid-card-wrapper grid-5 align-items-stretch justify-content-around">
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_john_b_goodenough" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_john_b_goodenough.png');?>" alt="Prof. John B. Goodenough">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. John B. Goodenough</h3>
                            <p class="card-description">The Nobel Prize in Chemistry 2019</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_tim_white_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_tim_white_1.png');?>" alt="Prof. Tim White">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Tim White</h3>
                            <p class="card-description">President of Material Research Societies (MRS) Singapore</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#ir_agus_tjahajana_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/ir_agus_tjahajana_1.png');?>" alt="Ir. Agus Tjahajana">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Ir. Agus Tjahajana</h3>
                            <p class="card-description">President Commissioner of MIND ID</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_alan_j_drew_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_alan_j_drew_1.png');?>" alt="Prof. Alan J Drew">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Alan J Drew</h3>
                            <p class="card-description">QMUL/NBRI</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_eniya_listiani_dewi" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_eniya_listiani_dewi.jpg');?>" alt="Prof. Dr. Eng. Eniya Listiani Dewi, M.Eng">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. Eng. Eniya Listiani Dewi, M.Eng</h3>
                            <p class="card-description">Deputy for Information, Energy, and Materials Technology, Agency for the Assessment and Application of Technology (BPPT)</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#dr_ir_taufik_bawazier_m_si_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/dr_ir_taufik_bawazier_m_si_1.png');?>" alt="Dr.Ir. Taufik Bawazier, M.Si">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Dr.Ir. Taufik Bawazier, M.Si</h3>
                            <p class="card-description">General Director of The Ministry of Industry Indonesia</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_dr_ir_anhar_riza_antariksawan" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_ir_anhar_riza_antariksawan.png');?>" alt="Prof. Dr. Ir. Anhar Riza Antariksawan">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. Ir. Anhar Riza Antariksawan</h3>
                            <p class="card-description">Head of BATAN, Indonesia</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_rodrigo_martins_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_rodrigo_martins_1.png');?>" alt="Prof. Rodrigo Martins">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Rodrigo Martins</h3>
                            <p class="card-description">President of IUMRS and European Academy of Science (EurASc)</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#yi_ke_phd" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/yi_ke_phd.png');?>" alt="Yi Ke, Ph.D.">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Yi Ke, Ph.D.</h3>
                            <p class="card-description">Energy Storage Program Manager – New Energy Nexus Global</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_laurence_hardwick" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_laurence_hardwick.png');?>" alt="Prof. Laurence Hardwick">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Laurence Hardwick</h3>
                            <p class="card-description">Director of the Stephenson Institute for Renewable Energy, University of Liverpool</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_ying_shirley_meng_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_ying_shirley_meng_1.png');?>" alt="Prof. Ying Shirley Meng">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Ying Shirley Meng</h3>
                            <p class="card-description">Research Award of International Battery Material Association 2019</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_jun_liu_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_jun_liu_1.png');?>" alt="Prof. Jun Liu">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Jun Liu</h3>
                            <p class="card-description">Director of the Innovation centre for Battery 500 Consortium</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#dr_ana_jorge_sobrido" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/dr_ana_jorge_sobrido.png');?>" alt="Dr. Ana Jorge Sobrido, Ph.D.">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Dr. Ana Jorge Sobrido, Ph.D.</h3>
                            <p class="card-description">UKRI Future Leaders Fellow, QMUL</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-navy">Keynote Speakers</h2>
            </div>
            <div class="col-md-12 grid-card-wrapper grid-5 align-items-stretch justify-content-between">
                <div class="card">
                    <a href="javascript:void(0)" data-target="#diyanto_imam" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/diyanto_imam.png');?>" alt="Diyanto Imam">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Diyanto Imam</h3>
                            <p class="card-description">Program Director of New Energy Nexus Indonesia</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_dr_vanessa_peterson_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_vanessa_peterson_1.png');?>" alt="Prof. Dr. Vanessa Peterson">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. Vanessa Peterson</h3>
                            <p class="card-description">ANSTO, Australia</p>
                        </div>
                    </a>
                </div> 
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_dr_stefan_adams_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_stefan_adams_1.png');?>" alt="Prof. Dr. Stefan Adams">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. Stefan Adams</h3>
                            <p class="card-description">NUS, Singapore</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_dr_takashi_kamiyama_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_takashi_kamiyama_1.png');?>" alt="Prof. Dr. Takashi Kamiyama">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. Takashi Kamiyama</h3>
                            <p class="card-description">Spallation Neutron Source, China</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_m_nizam_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_m_nizam_1.png');?>" alt="Prof. M. Nizam">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. M. Nizam</h3>
                            <p class="card-description">Coordinator of the National Research Priority</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#prof_santi_maensiri_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_santi_maensiri_1.png');?>" alt="Prof. Dr. Santi Maensiri">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. Santi Maensiri</h3>
                            <p class="card-description">President of MRS-Thailand</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#dr_haznan_abimanyu_p_hd" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/dr_haznan_abimanyu_p_hd.png');?>" alt="Dr. Haznan Abimanyu, PhD.">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Dr. Haznan Abimanyu, PhD.</h3>
                            <p class="card-description">Director of Research Centre for Electrical Power and Mechatronics at Indonesian Institute of Sciences (LIPI)</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                  <a href="javascript:void(0)" data-target="#prof_dr_m_zaki_mubarok_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/prof_dr_m_zaki_mubarok_1.png');?>" alt="Prof. Dr. M. Zaki Mubarok">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Prof. Dr. M. Zaki Mubarok</h3>
                            <p class="card-description">Institute of Technology Bandung, Indonesia</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                  <a href="javascript:void(0)" data-target="#dr_alexey_glushenkov_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/dr_alexey_glushenkov_1.png');?>" alt="Dr. Alexey Glushenkov">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Dr. Alexey Glushenkov</h3>
                            <p class="card-description">Australia National University (ANU), Australia</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#ir_i_made_dana_tangkas_m_si_ipu_asean_eng_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/ir_i_made_dana_tangkas_m_si_ipu_asean_eng_1.png');?>" alt="Ir. I Made Dana Tangkas, M.Si., IPU, ASEAN Eng">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Ir. I Made Dana Tangkas, M.Si., IPU, ASEAN Eng</h3>
                            <p class="card-description">Indonesia Automotive Institute </p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#dr_mohammad_mustafa_sarinanto_ipu_1" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/dr_mohammad_mustafa_sarinanto_ipu_1.png');?>" alt="Dr. Mohammad Mustafa Sarinanto, IPU">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">Dr. Mohammad Mustafa Sarinanto, IPU</h3>
                            <p class="card-description">BPPT</p>
                        </div>
                    </a>
                </div>
                <div class="card">
                    <a href="javascript:void(0)" data-target="#m_firmansyah" data-toggle="modal">
                        <div class="card-image">
                            <img src="<?php echo base_url('assets/images/m_firmansyah.png');?>" alt="M. Firmansyah, SE">
                        </div>
                        <div class="card-caption">
                            <h3 class="card-title">M. Firmansyah, SE</h3>
                            <p class="card-description">CEO at PT. Infiniti Energi Indonesia</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- #dr_laksana_tri_handoko -->
<div class="modal modal-member fade" id="dr_laksana_tri_handoko_1" tabindex="-1" role="dialog" aria-labelledby="dr_laksana_tri_handoko_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/dr_laksana_tri_handoko_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                    Dr. Laksana Tri Handoko
                </h3>
                <p class="subtitle">Head of National Research and Innovation Agency</p>
            </div>
            <div class="col-md-8">
                <p class="description">Dr. Laksana Tri Handoko is an Indonesian scientist and public official specializing in theoretical and particle physics. He formerly served as the deputy head of science and technology for the Indonesian Institute of Sciences from 2014 to 2018. And he became Chairman of the institute start from 2018. He appointed as the second (but first independent) holder of Head of National Research and Innovation Agency.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_dr_b_v_r_chowdary_1 -->
<div class="modal modal-member fade" id="prof_dr_b_v_r_chowdary_1" tabindex="-1" role="dialog" aria-labelledby="prof_dr_b_v_r_chowdary_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_dr_b_v_r_chowdary_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Dr. B.V.R. Chowdary
                </h3>
                <p class="subtitle">Head of IUMRS</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Chowdary is a president of Material Research Societes (MRS) Singapore. Prof. Chowdari has established research laboratory, first of its kind in NUS and the region, for carrying out research on synthesis and characterisation of materials for potential applications in solid state batteries. The laboratory is instrumental in developing several research groups in Singapore and the region. His current research interest lie in the area of materials science, specialising in the synthesis of materials using different methods, characterising them by variety of techniwues, and finding applications in energy storage devices more specifically Lithium ion batteries.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_colin_gareth_bailey -->
<div class="modal modal-member fade" id="prof_colin_gareth_bailey" tabindex="-1" role="dialog" aria-labelledby="prof_colin_gareth_bailey" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_colin_gareth_bailey.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Colin Gareth Bailey
                </h3>
                <p class="subtitle">President and Principal of Queen Mary University of London</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Bailey is is a researcher in structural engineering, who became the President and Principal of Queen Mary University of London in September 2017. Prior to that, Bailey was Deputy President and Deputy Vice-Chancellor at the University of Manchester. He is a Fellow of the Royal Academy of Engineering, the Institution of Civil Engineers, the Institution of Structural Engineers and a member of the Institution of Fire Engineers</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_colin_gareth_bailey -->
<div class="modal modal-member fade" id="prof_colin_gareth_bailey" tabindex="-1" role="dialog" aria-labelledby="prof_colin_gareth_bailey" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_colin_gareth_bailey.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Colin Gareth Bailey
                </h3>
                <p class="subtitle">President and Principal of Queen Mary University of London</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Bailey is is a researcher in structural engineering, who became the President and Principal of Queen Mary University of London in September 2017. Prior to that, Bailey was Deputy President and Deputy Vice-Chancellor at the University of Manchester. He is a Fellow of the Royal Academy of Engineering, the Institution of Civil Engineers, the Institution of Structural Engineers and a member of the Institution of Fire Engineers</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_dr_rer_nat_evvy_kartini -->
<div class="modal modal-member fade" id="prof_dr_rer_nat_evvy_kartini" tabindex="-1" role="dialog" aria-labelledby="prof_dr_rer_nat_evvy_kartini" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_dr_rer_nat_evvy_kartini.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Dr. rer. nat. Evvy Kartini
                </h3>
                <p class="subtitle">Founder of National Battery Research Institute and President of MRS-INA</p>
            </div>
            <div class="col-md-8">
                <p class="description">Evvy Kartini holds the title of Prof. Dr. rer. nat. She is an expert on the neutron scattering and respected internationally. Her international reputation in the field of neutron scattering and solid state ionics, has been well established. She has been represented as a leader (president) of the Indonesian Neutron Scattering Society (INSS) since 2013. She is one of the executive committee of the Asian Oceania Neutron Scattering Soceity (AONSA). Besides neutron scattering, Evvy Kartini has expertise on the materials science, especially on lithium ion battery research. Evvy Kartini, has became one of the International Board Member of the Asian Socety of Solid State Ionics and International Society of Solid State Ionics. Recently, Evvy Kartini has attended the 20th International Conference on Solid State Ionics (ICSSI) in Keystone, Colorado, USA 2015. She was there not only for presenting her research work on battery researchers, but also represented Asia-Australia region as councilor of the International Society of Solid State Ionics. She was one of the International Advisory Board, and also chaired the session of Research on Battery Materials. She also presented her recent topic on development of new lithium solid electrolyte, which is explored by the inelastic spectrometer instrument at Japan Proton Accelerator Research Compex (J-PARC), the foremost leading spallation neutron source in the world</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_john_b_goodenough -->
<div class="modal modal-member fade" id="prof_john_b_goodenough" tabindex="-1" role="dialog" aria-labelledby="prof_john_b_goodenough" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_john_b_goodenough.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. John B. Goodenough
                </h3>
                <p class="subtitle">The Nobel Prize in Chemistry 2019</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. John B. Goodenough is an American physicist who won the 2019 Nobel Prize for Chemistry for his work on developing lithium-ion batteries. Goodenough became a professor at the University of Texas at Austin in 1986 in the departments of mechanical engineering and electrical and computer engineering. He has been honoured with the National Medal of Science (2011), the Charles Stark Draper Prize (2014), and the Copley Medal (2019). He wrote Magnetism and the Chemical Bond (1963), SolidOxide Fuel Cell Technology: Principles, Performance and Operations (2009, with Kevin Huang), and an autobiography, Witness to Grace (2008)”</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_tim_white_1 -->
<div class="modal modal-member fade" id="prof_tim_white_1" tabindex="-1" role="dialog" aria-labelledby="prof_tim_white_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_tim_white_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Tim White
                </h3>
                <p class="subtitle">President of Material Research Societies (MRS) Singapore</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Tim White is the president’s Chair in Materials Science & Engineering and Associate Vice President (Infrastructure & Programmes) in the President’s Office at the Nanyang Technological University, Singapore. He has over 40 years research experience at national laboratories and universities in Australia and Singapore in materials science and engineering, minerals processing, nuclear waste treatment and environmental management. These appointments included group leader at The Australian Atomic Energy Commission and Multiplex Professor of Environmental Technology. Prof. Tim White has thirty years of experience in the design and demonsration of advanced materials for environmental, superconducting, ionic conductivity and hydrogen storage applications. His particular interests lie in tailoring cermaics at the atomic scale to develop or enhance particular properties. Theses studies have been supported and facilitated through the use of advanced characterization methods, including atomic resolution electron microscopy, crystal refinement using X-ray and neutron diffraction, and synchrotron-based surface analysis for the investigation of chemical states and molecular environments</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #ir_agus_tjahajana_1 -->
<div class="modal modal-member fade" id="ir_agus_tjahajana_1" tabindex="-1" role="dialog" aria-labelledby="ir_agus_tjahajana_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/ir_agus_tjahajana_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Ir. Agus Tjahajana
                </h3>
                <p class="subtitle">President Commissioner of MIND ID</p>
            </div>
            <div class="col-md-8">
                <p class="description">Dr. Ir. Agus Tjahajana Wirakusumah is a President Commissioner of MIND ID (Indonesian Mining Holding Industry). Dr. Agus graduated from Institut Teknologi Bandung (ITB) majoring Mechanical Engineering in 1979. He also received Bachelor of Economic from Universitas Indonesia (1988). Dr. Agus pursued his master degree on Industrial System Engineering University of Florida, United States (1991). Along his career in private sector, Dr. Agus often becomes the commissioner in many companies. Like General Manager at PT Rekadaya Sarana (1981-1982), Commissioner at PT Semen Baturaja (1995-2002), President Commissioner at PT Semen Baturaja (2002-2006), President Commissioner at PT Boma Bisma Indra (2008-2011), to President Commissioner at PT Rekayasa Industri (2011-2014). In public sector, he also has several core position. Dr. Agus is a former General Director of International Industrial Cooperation Ministry of Industry (2010-2015).</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_alan_j_drew_1 -->
<div class="modal modal-member fade" id="prof_alan_j_drew_1" tabindex="-1" role="dialog" aria-labelledby="prof_alan_j_drew_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_alan_j_drew_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Alan J Drew
                </h3>
                <p class="subtitle">QMUL/NBRI</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Alan Drew was appointed Leverhulme Fellow in the Centre of Condensed Matter & Materials Physics in 2008. He was rapidly promoted to Senior Lecturer (2011) and then Reader (2012), and in 2018 was promoted to Professor. Prof. Drew has been awarded a number of prestigious fellowships and awards over his career, starting with a Fellowship of the Royal Commission of the Exhibition of of 1851 (2014), Leverhulme Fellow (2008), European Research Council (ERC) fellow (2012), Talent 1000 Scholar of the Chinese Ministry of Education (2014) and Changjiang Distinguished Professor at Sichuan University (2015). He is currently Head of the CCMMP Research Centre, and Director of the Materials Research Institute. His main research interests are using spin sensitive and structural probes situated at central facilities to characterize and understand the fundamental properties of materials,backed up with laboratory based techniques (e.g. magnetic, structural and electrical characterisation, thin film growth, raman/IR spectroscopy, electro/photo luminescence). His main interests are understanding spin and charge carrier dynamics in organic and biological materials, the properties of materials with novel quantum mechanical states, structure function relationship in biomass derived conductors.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_eniya_listiani_dewi -->
<div class="modal modal-member fade" id="prof_eniya_listiani_dewi" tabindex="-1" role="dialog" aria-labelledby="prof_eniya_listiani_dewi" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_eniya_listiani_dewi.jpg');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Dr. Eng. Eniya Listiani Dewi, M.Eng
                </h3>
                <p class="subtitle">Deputy for Information, Energy, and Materials Technology, Agency for the Assessment and Application of Technology (BPPT)</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Eniya Listiani Dewi is Professor Electrochemical Process at Agency for the Assessment and Application of Technology (BPPT). She is also deputy of Information, Energy, and Materials Technology at BPPT. Prof. Eniya had finished Bachelor of Engineering at Applied Chemistry of Waseda University at 1998, then completed Doctor of Engineering on the advanced research institute for science and engineering, engineering faculty, department of applied chemistry, Waseda University, Tokyo, Japan at 2003, as DC1 JSPS special researcher. Her interest are on electron transfer phenomenon on the nanocatalyst, hydrocarbon polymer materials, PEM-fuel cell, zinc-air fuel cell batteries as well as hydrogen production from biomass and PEM-electrolyser. Her activity were awarded from many institutions, such as Mizuno Award, Koukenkai Award, Asia Excellent Award, Best Scientist Award, Engineering Award, Medco-Energy Research Award, Patent Innovation Award, The Habibie Award, etc.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #dr_ir_taufik_bawazier_m_si_1 -->
<div class="modal modal-member fade" id="dr_ir_taufik_bawazier_m_si_1" tabindex="-1" role="dialog" aria-labelledby="dr_ir_taufik_bawazier_m_si_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/dr_ir_taufik_bawazier_m_si_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Dr.Ir. Taufik Bawazier, M.Si
                </h3>
                <p class="subtitle">General Director of The Ministry of Industry Indonesia</p>
            </div>
            <div class="col-md-8">
                <p class="description">Dr. Ir. Taufik Bawazier, M.Si. is a general director of ILMATE The Ministry of Industry, Indonesia. He obtained bachelor degree of Engineering from Institute Technology of Sepuluh Nopember. He pursued his educational journey on Magister of Economic University of Indonesia. Then, he got Doctoral degree of Political Science from University of Indonesia in 2011.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_dr_ir_anhar_riza_antariksawan -->
<div class="modal modal-member fade" id="prof_dr_ir_anhar_riza_antariksawan" tabindex="-1" role="dialog" aria-labelledby="prof_dr_ir_anhar_riza_antariksawan" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_dr_ir_anhar_riza_antariksawan.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Dr. Ir. Anhar Riza Antariksawan
                </h3>
                <p class="subtitle">Head of BATAN, Indonesia</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Anhar Riza Antariksawan is a Head of National Nuclear Energy Agency (BATAN). He obtained bachelor degree of Nuclear Engineering from Gadjah Mada University in 1986. Then, he pursued his master and doctoral degrees from Energy Physics in Institute of National Polytechnique de Grenoble, France in 1989 and 1993. </p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_rodrigo_martins_1 -->
<div class="modal modal-member fade" id="prof_rodrigo_martins_1" tabindex="-1" role="dialog" aria-labelledby="prof_rodrigo_martins_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_rodrigo_martins_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Rodrigo Martins
                </h3>
                <p class="subtitle">President of IUMRS and European Academy of Science (EurASc)</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Rodrigo Martins is full professor in Materials Science Department of Faculty of Science and Technology of New University of Lisbon, a fellow of the Portuguese Engineering Academy since 2009 and a member of the European Academy of Science since 2016. He was decorated with the gold medal of merit and distinction by the Almada Municipality for his R&D achievements. Prof. Rodrigo has been involved in the pioneer European research on Amorphous silicon semiconductors and pioneer with is group worldwide activity related to passive and active oxides, the so called transparent electronics and it is one of the inventors of the so-called paper electronics, where paper is exploit not only as asubstrate but also as a functional component in active devices. He published over 700 papers and during the last 10 years got more than 14 international and national prizes and distinctions for his work (e.g. Lisbon Energy Live Expo, Innovation Prize, 2012 Solar Tiles, European Patent Office Innovation nomination 2016, etc.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #yi_ke_phd -->
<div class="modal modal-member fade" id="yi_ke_phd" tabindex="-1" role="dialog" aria-labelledby="yi_ke_phd" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/yi_ke_phd.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Yi Ke, Ph.D.
                </h3>
                <p class="subtitle">Energy Storage Program Manager – New Energy Nexus Global</p>
            </div>
            <div class="col-md-8">
                <p class="description">Yi Ke, Ph.D. is a material science Ph.D. and strategic consultant in energy. She lead the energy storage program at New Energy Nexus (previously known as California Clean Energy Fund). They aim to support diverse entrepreneurs to drive innovation in the energy storage and battery sector with funds, accelerators, and networks. Projects under their energy storage program include CalCharge, EV and Battery Challenge (with LG Chem, Hyundai Motor, and Kia Motor), LG Chem Battery Challenge, Bay Area Battery Summit. While working at Rocky Mountain Institute, She provided strategy consulting service to government and utilities, on topics from long-term energy system planning to short-term power market designto create clean, prosperous, and secure low-carbon energy future for all. Her works were used as a reference to the US-China Joint Presidential Statement on Climate Change in 2015 by the White House and China’s 13th Five Year Plan by NDRC. </p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_laurence_hardwick -->
<div class="modal modal-member fade" id="prof_laurence_hardwick" tabindex="-1" role="dialog" aria-labelledby="prof_laurence_hardwick" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_laurence_hardwick.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Laurence Hardwick
                </h3>
                <p class="subtitle">Director of the Stephenson Institute for Renewable Energy, University of Liverpool</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Laurence Hardwick is Professor of Electrochemistry and Director of the Stephenson Institute for Renewable Energy within the Department of Chemistry at the University of Liverpool. He received his Mchem in Chemistry in 2003 from the University of Southampton and PhD in Chemistry from ETH-Zurich in 2006. Before joining Liverpool in 2011, he spent his postdoctoral time working at the Lawrence Berkeley National Laboratory and at the University of St Andrews Investigating Li-ion battery electrode degradation mechanism, lithium diffusion pathways through carbon and the chemical and electrochemical process in Li-air cells. His recent work has focused on the development of advanced in situ electrochemical surface enhanced infrared and Raman methodologies that examine electrochemical reaction mechanisms on variety of electrodes interfaces which assist in our understanding on the function of metal-air and li-ion batteries</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_jun_liu_1 -->
<div class="modal modal-member fade" id="prof_jun_liu_1" tabindex="-1" role="dialog" aria-labelledby="prof_jun_liu_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_jun_liu_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Jun Liu
                </h3>
                <p class="subtitle">Director of the Innovation centre for Battery 500 Consortium</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Jun Liu is a Battelle Fellow at PNNL, holds a joint appointment with the University of Washington as the Campbell Chair Professor, and also serves as the Director for the Battery500 Consortium, a large DOE multi-institute program supported by DOE to develop future energy storage technologies. Prof. Liu has more than 450 peer reviewed publications, has received more than 60 U.S. patents. He has been a Highly Cited Researcher in the world in the fields of chemistry and materials sciences since 2014. He was named a Distinguished Inventor of Battelle in 2007, and was selected as PNNL's Inventor of the Year in 2012 and 2016. He has received the Electrochemical Society Battery Division Technology Award, the DOE EERE Exceptional Achievement Award, and PNNL Lifetime Achievement Award.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_ying_shirley_meng_1 -->
<div class="modal modal-member fade" id="prof_ying_shirley_meng_1" tabindex="-1" role="dialog" aria-labelledby="prof_ying_shirley_meng_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_ying_shirley_meng_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Ying Shirley Meng
                </h3>
                <p class="subtitle">Research Award of International Battery Material Association 2019</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Ying Shirley Meng is a materials scientist and professor of the department of Nanoengineering and the Materials Science Program at University of California, San Diego. She currently holds the Zable Endowed Chair Professor in Energy Technologies. She is also the founding Director of Sustainable Power and Energy Center. She achieved several prestigious awards including International Battery Association IBA Research Award in 2019 and Chancellor’s Associates Faculty Research Excellence Award 2019.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #dr_ana_jorge_sobrido -->
<div class="modal modal-member fade" id="dr_ana_jorge_sobrido" tabindex="-1" role="dialog" aria-labelledby="dr_ana_jorge_sobrido" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/dr_ana_jorge_sobrido.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Dr. Ana Jorge Sobrido, Ph.D.
                </h3>
                <p class="subtitle">UKRI Future Leaders Fellow, QMUL</p>
            </div>
            <div class="col-md-8">
                <p class="description">Dr. Ana Jorge Sobrido, Ph.D. is a senior lecturer in Sustainable Energy Materials UKRI Future Leaders Fellow Programme Director for Sustainable Energy Engineering of Queen Mary University of London. After finishing her degree in chemistry (2004), she moved to Barcelona to conduct a PhD in Materials Science at he Autonomous University of Barcelona and the Institute of Materials Science, being awarded a Prize for an outstanding PhD thesis (2009). From 2011 to 2016 she worked as a PDRA at UCL in Chemistry and Chemical Engineering Departments, where she researched metal-free energy materials for photocatalysis, batteries and water electrolyses. In 2016, she took up a position as Academic Fellow at QMUL. In 2019, Dr. Ana became a Lecturer in Energy Materials at QMUL. Dr. Ana’s group studies the design of high-performing sustainable electroactive materials using easy-to-scale processing techniques for application in energy storage and conversion. She is a member of the UK Redox Flow Battery Committee, STFC Battery Steering Committee and Director of the London Energy Materials & Devices Hub.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #diyanto_imam -->
<div class="modal modal-member fade" id="diyanto_imam" tabindex="-1" role="dialog" aria-labelledby="diyanto_imam" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/diyanto_imam.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Diyanto Imam
                </h3>
                <p class="subtitle">Program Director of New Energy Nexus Indonesia</p>
            </div>
            <div class="col-md-8">
                <p class="description">Diyanto Imam is a Program Director of New Energy Nexus Indonesia. He is an impact-focused professional who delivers creative business solutions with significant and extensive experience in hardware-based startups and impact startups and impact enterprise development in Indonesia. He pursued his bachelor degree on Management at Queensland University of Technology (QUT) 1998 and master’s degree on International Business at Swinburne University of Technology in 2000.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_dr_vanessa_peterson_1 -->
<div class="modal modal-member fade" id="prof_dr_vanessa_peterson_1" tabindex="-1" role="dialog" aria-labelledby="prof_dr_vanessa_peterson_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_dr_vanessa_peterson_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Dr. Vanessa Peterson
                </h3>
                <p class="subtitle">ANSTO, Australia</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Peterson is a principal research and neutron scattering instrument scientist. She is a leader of Energy Materials Research Project Neutron Instrument Scientist, Powder Diffraction (Wombat and Echidna instruments) ANSTO. She also a Honorary Professional Fellow, Institute for Superconducting and Electronic Materials, University of Wollongong. She has an expertise in understanding materials function by relating atomic-scale structure and dynamics to material properties. Also condensed matter materials of interest include porous coordination framework materials, energy-industry relevant materials such as batteries, gas-separation and storage materials, and fuel-cells, as well as cement.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_dr_stefan_adams_1 -->
<div class="modal modal-member fade" id="prof_dr_stefan_adams_1" tabindex="-1" role="dialog" aria-labelledby="prof_dr_stefan_adams_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_dr_stefan_adams_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Dr. Stefan Adams
                </h3>
                <p class="subtitle">NUS, Singapore</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Adams is an Associate Professor at Department of Materials Science and Engineering, National University of Singapore since 2011. His work is to combine in situ characterization by electrochemical, neutron, and x-ray diffraction methods with computational approaches to promote the understanding of charge and mass transport in solids and the underlying structure-property correlations. His studies also encompass a wide range of materials for sustainable energy applications such as fast ion conducting solids and mixed conducting cathode materials for lithium batteries with higher power or energy density, and ceramic fuel cells operating at moderate temperatures, nanocomposites for chemica and electrochemical energy storage.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_dr_takashi_kamiyama_1 -->
<div class="modal modal-member fade" id="prof_dr_takashi_kamiyama_1" tabindex="-1" role="dialog" aria-labelledby="prof_dr_takashi_kamiyama_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_dr_takashi_kamiyama_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Dr. Takashi Kamiyama
                </h3>
                <p class="subtitle">Spallation Neutron Source, China</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Kamiyama is an Associate Professor at the Divison of Quantum Science aand Engineering of Faculty of Engineering, Hokkaido University. He has been interested in topics including dynamical structure of amorphous materials, neutron scattering and imaging using pulsed sources, and neutron devices for detecting and beam control. He received his bachelor’s, master’s, and Ph.D degrees from Tohoku University.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_m_nizam_1 -->
<div class="modal modal-member fade" id="prof_m_nizam_1" tabindex="-1" role="dialog" aria-labelledby="prof_m_nizam_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_m_nizam_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. M. Nizam
                </h3>
                <p class="subtitle">Coordinator of the National Research Priority</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Nizam is a coordiantor of the National Research Priority of Indonesia on lithium battery and fast charging station. He is presently working as a senior lecture in Universitas Sebelas Maret in Surakarta. He achieved his bachelor and master’s degree from Universitas Gadjah Mada majoring in Electrical Engineering while the PhD of electrical, electronic, and systems engineering was obtained from Universiti Kebangsaan Malaysia in 2008. Prof. Nizam put his research interest on power systems, renewable energy, energy management, energy storage system, and power quality. 

</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #dr_alexey_glushenkov_1 -->
<div class="modal modal-member fade" id="dr_alexey_glushenkov_1" tabindex="-1" role="dialog" aria-labelledby="dr_alexey_glushenkov_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/dr_alexey_glushenkov_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Dr. Alexey Glushenkov
                </h3>
                <p class="subtitle">Australia National University (ANU), Australia</p>
            </div>
            <div class="col-md-8">
                <p class="description">Dr. Glushenkov was awarded his PhD ten years ago in December 2009. He has not experienced significant academic interruption. He has contributed significantly to the international research on battery materials, including high impact work in materials for supercapacitors, sodium-ion, and potassium-ion batteries. He has published 69 journal papers (cited >3000 times) and his current h-index is 31. Dr. Glushenkov has estabished international collaborations with Japan, Ireland, Russia, China, Germany, and the US and won a number of prizes and awards. He has been awarded an Award for Research Excellence by Deakin University (early career researcher) in 2014 and he was an Emerging Investigator of Journal of Materials Chemistry A in 2017.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #ir_i_made_dana_tangkas_m_si_ipu_asean_eng_1 -->
<div class="modal modal-member fade" id="ir_i_made_dana_tangkas_m_si_ipu_asean_eng_1" tabindex="-1" role="dialog" aria-labelledby="ir_i_made_dana_tangkas_m_si_ipu_asean_eng_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/ir_i_made_dana_tangkas_m_si_ipu_asean_eng_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Ir. I Made Dana Tangkas, M.Si., IPU, ASEAN Eng
                </h3>
                <p class="subtitle">Indonesia Automotive Institute</p>
            </div>
            <div class="col-md-8">
                <p class="description">Ir. I Made Dana Tangkas earned Bachelor of Industrial Engineering from Bandung Institute of Technology in 1989 and Master of Economic Development studies form University of Pembangunan Nasional-Veteran in 2014. Joined Toyota Indonesia since 1990 and in 1994 practices Lean Industry 1 year in Toyota Motor Corp Japan, since 2009 as Toyota Indonesia’s Director and 2010-2012 as Vice President of Toyota Motor Asia Pacific as well as Indonesia Country Leader, then since 2013 back to Indonesia as director. He has held various essential positions throughout his career among others. Director of Corporate & External Affairs and also Production & Logistics Control, Production Engineering, Quality Assurance & Engineering Technical Services. The other important activities are as President of Idnonesia Automotive Institute since 2016 and also in 2018 as founder & CEO of IBIMA Indonesia.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #dr_mohammad_mustafa_sarinanto_ipu_1 -->
<div class="modal modal-member fade" id="dr_mohammad_mustafa_sarinanto_ipu_1" tabindex="-1" role="dialog" aria-labelledby="dr_mohammad_mustafa_sarinanto_ipu_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/dr_mohammad_mustafa_sarinanto_ipu_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Dr. Mohammad Mustafa Sarinanto, IPU
                </h3>
                <p class="subtitle">BPPT</p>
            </div>
            <div class="col-md-8">
                <p class="description">Ir. I Made Dana Tangkas earned Bachelor of Industrial Engineering from Bandung Institute of Technology in 1989 and Master of Economic Development studies form University of Pembangunan Nasional-Veteran in 2014. Joined Toyota Indonesia since 1990 and in 1994 practices Lean Industry 1 year in Toyota Motor Corp Japan, since 2009 as Toyota Indonesia’s Director and 2010-2012 as Vice President of Toyota Motor Asia Pacific as well as Indonesia Country Leader, then since 2013 back to Indonesia as director. He has held various essential positions throughout his career among others. Director of Corporate & External Affairs and also Production & Logistics Control, Production Engineering, Quality Assurance & Engineering Technical Services. The other important activities are as President of Idnonesia Automotive Institute since 2016 and also in 2018 as founder & CEO of IBIMA Indonesia.</p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #m_firmansyah -->
<div class="modal modal-member fade" id="m_firmansyah" tabindex="-1" role="dialog" aria-labelledby="m_firmansyah" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/m_firmansyah.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                M. Firmansyah, SE
                </h3>
                <p class="subtitle">CEO at PT. Infiniti Energi Indonesia</p>
            </div>
            <div class="col-md-8">
                <p class="description">Muhammad Firmansyah completed his undergraduate education from Padjajaran University Bandung in 2015 majoring International Business Management. Then, He deepened his knowledge in entrepreneurship. Firman began his career at PT. HM. Sampoerna Tbk in 2015. Initially he got a position as a graduate trainee, then as a Consumer Engagement Supervisor. In 2016, he gained a position as Area Supervisor for Multitasking. In 2018, he held the position of Area Analyst and later became Area Manager. In the same year he founded his consulting company named PT. Infiniti Energi Indonesia and became President Director. In addition, he also became a commissioner of CV. Tridaya Cavali. 
                </p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_dr_m_zaki_mubarok_1 -->
<div class="modal modal-member fade" id="prof_dr_m_zaki_mubarok_1" tabindex="-1" role="dialog" aria-labelledby="prof_dr_m_zaki_mubarok_1" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
        <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
            <div class="col-md-4">
                <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_dr_m_zaki_mubarok_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                </div>
                <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                Prof. Dr. M. Zaki Mubarok
                </h3>
                <p class="subtitle">Institute of Technology Bandung, Indonesia</p>
            </div>
            <div class="col-md-8">
                <p class="description">Prof. Dr. mont. M. Zaki Mubarok, ST., MT. Is a head of Hydro-Electrometallurgy Laboratory of ITB. He pursued Doctoral degree from Department of Nonferrous Metallurgy, University of Leoben, Austria 2005. He also published many research papers and various award. In 2020, he received high-impact research article awards from National Research and Innovation Agency. 
                </p>
            </div>
        </div>
        </div>
        <div class="modal-footer no-border p-0">
            <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
        </div>
    </div>
    </div>
</div>

<!-- #prof_santi_maensiri_1 -->
<div class="modal modal-member fade" id="prof_santi_maensiri_1" tabindex="-1" role="dialog" aria-labelledby="prof_santi_maensiri_1" aria-hidden="true">
  <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
      <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
          <div class="col-md-4">
            <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof_santi_maensiri_1.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
            </div>
            <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
            Prof. Dr. Santi Maensiri
            </h3>
            <p class="subtitle">President of MRS-Thailand</p>
          </div>
          <div class="col-md-8">
            <p class="description">Prof. Dr. Santi Maensiri is the Director of SUT Center of Excellence in Advanced Functional Materials (SUT-AFM) and Vice-Rector for Academic Affairs and Internationalization, Suranaree University of Technology, Nakhon Ratchasima, Thailand. He is the author of over 225 papers with citations over 3400 times and h-index of 31 in ISI journals. His research of interest is in the fields of materials physics and nanostructured materials, which focus mainly on the fabrication, properties, and applications of materials. The materials of interest include: 
            <ol>
                <li>functional nanostructured materials,</li>
                <li>diluted magnetic semiconductors and magnetic nanoparticles,</li>
                <li>giant dielectric ceramics and nanocomposites, and</li>
                <li>electrospun nanofibers of ceramics, polymers, and nanocomposites for electronic device, environmental, and energy applications.</li>
            </ol>
            Prof. Maensiri received his BSc. Degree (Physics) from Khon Kaen University, Thailand, M.Sc. degree (Ceramic Processing) from Leeds University, and D.Phil. degree (Materials) from Oxford University, U.K. He received many research awards including Thai Young Scientist Award 2007 (Physics), TWAS Prize 2009 for Young Scientists in Thailand in Physics, TRF-CHE-SCOPUS Researcher Award 2010 (Physical Sciences), Thailand Research Fund (TRF) Senior Research Scholar 2013,  and Thailand National Outstanding Researcher Awards 2013 (NRCT). Prof. Maensiri is the President of Materials Research Society of Thailand (MRS-Thailand).
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer no-border p-0">
        <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
      </div>
    </div>
  </div>
</div>

<!-- #dr_haznan_abimanyu_p_hd -->
<div class="modal modal-member fade" id="dr_haznan_abimanyu_p_hd" tabindex="-1" role="dialog" aria-labelledby="dr_haznan_abimanyu_p_hd" aria-hidden="true">
  <div class="modal-dialog modal-full modal-dialog-centered" role="document">
    <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
      <div class="modal-body p-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">
                <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
            </span>
        </button>
        <div class="row">
          <div class="col-md-4">
            <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/dr_haznan_abimanyu_p_hd.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
            </div>
            <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
            Dr. Haznan Abimanyu, PhD.
            </h3>
            <p class="subtitle">Director of Research Centre for Electrical Power and Mechatronics at Indonesian Institute of Sciences (LIPI)</p>
          </div>
          <div class="col-md-8">
            <p class="description">Dr. Haznan Abimanyu is a Director of Research Centre for Electrical Power and Mechatronics at Indonesian Institute of Sciences (LIPI). Dr. Haznan started his research career in 1997 at the Research Centre for Chemistry Indonesian Institute of Science. He pursued his PhD in University of Science and Technology, South Korea. His research scope related to energy, catalyst, atsiri, and oleochemistry.
            </p>
          </div>
        </div>
      </div>
      <div class="modal-footer no-border p-0">
        <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
      </div>
    </div>
  </div>
</div>
