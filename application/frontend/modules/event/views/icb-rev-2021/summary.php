<section id="checkout-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <h2>Summary</h2>
            </div>
            <div class="col-md-12">
            <?php
                $registered_user_data = $_SESSION['registration_data'];
            ?>
                <div class="form-group">
                    <label for="" class="font-size-small">Full name</label>
                    <p class="font-size-medium"><?php echo $registered_user_data['salutation'].' '.$registered_user_data['first_name'].' '.$registered_user_data['last_name'].$registered_user_data['suffixes'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Email address</label>
                    <p class="font-size-medium"><?php echo $registered_user_data['email'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Address</label>
                    <p class="font-size-medium"><?=$registered_user_data['address'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Phone number</label>
                    <p class="font-size-medium"><?=$registered_user_data['phone'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Institution / Affiliation</label>
                    <p class="font-size-medium"><?=$registered_user_data['affiliation'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Event Category</label>
                    <p class="font-size-medium">International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2021</p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Package</label>
                    <p class="font-size-medium"><?=$registered_user_data['conference-package-text'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Nationality</label>
                    <p class="font-size-medium"><?=ucwords($registered_user_data['nationality']);?></p>
                </div>
                <?php if(stripos($registered_user_data['conference-package'],'participant')):?>
                <div class="form-group">
                    <label for="" class="font-size-small">Symposia</label>
                    <p class="font-size-medium"><?=$registered_user_data['symposia'];?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Abstract Title</label>
                    <p class="font-size-medium"><?=$registered_user_data['conference_abstract-title'];?></p>
                </div>
                <?php endif;?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <label for="" class="font-size-regular">Price</label>
            </div>
            <div class="col-md-6 text-right">
                <p class="font-size-big">
                    <span class="font-size-medium" id="currency"><?=$registered_user_data['currency'];?></span>
                    <?=$registered_user_data['normal_price'];?>
                </p>
            </div>
            <div class="col-md-6">
                <label for="" class="font-size-regular">Discount</label>
            </div>
            <div class="col-md-6 text-right">
                <p class="font-size-big">
                    <span class="font-size-medium" id="currency"><?=$registered_user_data['currency'];?></span>
                    -<?=$registered_user_data['normal_price']-$registered_user_data['early_bird_price'];?>
                </p>
            </div>
            <div class="col-md-6">
                <label for="" class="font-size-regular font-bold">Total</label>
            </div>
            <div class="col-md-6 text-right">
                <p class="font-size-biggest font-bold">
                    <span class="font-size-big" id="currency"><?=$registered_user_data['currency'];?></span>
                    <?=$registered_user_data['normal_price']-($registered_user_data['normal_price']-$registered_user_data['early_bird_price']);?>
                </p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 d-flex justify-content-end">
                <button type="button" class="btn btn-custom bg-navy text-yellow" id="btn-checkout-fix">Check Out</button>
                <!-- <button class="btn btn-custom bg-navy text-yellow mr-3">Guest Checkout</button>
                <button class="btn btn-custom bg-navy text-yellow">Member Checkout</button> -->
            </div>
        </div>
    </div>
</section>