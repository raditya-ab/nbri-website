<section id="countdown-section" class="page-section py-80 text-center">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="section-title">Register Now</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="countdown" data-year="<?= date('Y', strtotime($event_date['icb-rev_2022'])); ?>"
						 data-month="<?= date('m', strtotime($event_date['icb-rev_2022'])); ?>" data-day="<?= date('d', strtotime($event_date['icb-rev_2022'])); ?>"
						 data-hours="<?= date('H', strtotime($event_date['icb-rev_2022'])); ?>"></div>
			</div>
			<div class="col-md-12 d-flex justify-content-center mt-5">
				<a class="btn btn-custom bg-navy text-yellow"
					 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/registration'); ?>">Register Now</a>
			</div>
		</div>
	</div>
</section>
<section id="scope-section" class="page-section bg-yellow py-80">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">Agenda</h2>
			</div>
			<div class="col-md-12 grid-card-wrapper grid-4 align-items-center justify-content-between">
				<div class="card">
					<a href="<?php echo site_url('event/' . $this->uri->segment(2) . '/conference'); ?>">
						<div class="card-image">
							<img src="<?php echo base_url('assets/uploads/'.$this->uri->segment(2).'/images/icb-rev_2022.jpg'); ?>"
									 alt="International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2022">
						</div>
						<div class="card-caption">
							<h3 class="card-title">International Conference on Battery for Renewable Energy and Electric Vehicles
								(ICB-REV) 2022</h3>
						</div>
					</a>
				</div>
				<div class="card">
					<a href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/ibs-2022'); ?>">
						<div class="card-image">
							<img src="<?php echo base_url('assets/uploads/'.$this->uri->segment(2).'/images/ibs_2022.jpg'); ?>" alt="">
						</div>
						<div class="card-caption">
							<h3 class="card-title">International Battery School (IBS) 2022</h3>
							<!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
						</div>
					</a>
				</div>
				<div class="card">
					<a href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/nbri-yic-2022'); ?>">
						<div class="card-image">
							<img src="<?php echo base_url('assets/uploads/'.$this->uri->segment(2).'/images/nbri-yic_2022.jpg'); ?>" alt="">
						</div>
						<div class="card-caption">
							<h3 class="card-title">NBRI Youth Ideas Competition (NBRI-YIC) 2022</h3>
							<!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
						</div>
					</a>
				</div>
				<div class="card">
					<a href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/isrus-2022'); ?>">
						<div class="card-image">
							<img src="<?php echo base_url('assets/uploads/'.$this->uri->segment(2).'/images/isrus_2022.jpg'); ?>" alt="">
						</div>
						<div class="card-caption">
							<h3 class="card-title">International Workshop on Solar Rooftop Residential and Utilities Scale (ISRUS)
								2022</h3>
							<!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="calendar-section" class="page-section py-80">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">Mark the calendar</h2>
			</div>
			<div class="col-md-12 accordion-tab-wrapper ">
		  <?php foreach ($event_details[$this->uri->segment(2)]['icb-rev_2022']['detail']['important-dates'] as $date) : ?>
						<div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
							<div class="event-date text-center">
								<div class="date"><?= $date['short_value'][0] ?></div>
								<div class="month text-green"><?= $date['short_value'][1] ?></div>
							</div>
							<div class="event-description"><?= $date['name']; ?></div>
						</div>
		  <?php endforeach; ?>
			</div>
			<div class="col-md-12 d-flex justify-content-center mt-5" style="flex-wrap: wrap">
				<!-- <a class="btn btn-custom bg-yellow text-navy mx-3"><i class="fab fa-google"></i> Add events to Google Calendar</a> -->
				<a href="<?php echo base_url('assets/uploads/' . $this->uri->segment(2) . '/documents/' . $this->uri->segment(2) . '.ics'); ?>" download
					 class="btn btn-custom bg-navy text-green mx-3"><i class="far fa-calendar-alt"></i> Add to calendar</a>
			</div>
		</div>
	</div>
</section>