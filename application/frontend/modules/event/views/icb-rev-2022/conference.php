<section id="abstract-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-yellow">Abstract and Paper Submission</h2>
                <p class="text-navy font-light font-size-medium">
                    Please read the Abstract and Paper instruction carefully. Abstract and papers submitted to the ICB-REV 2022 must be prepared in accordance with the template. Please download the <a class="font-light font-size-medium text-yellow" download href="<?php echo base_url('assets/uploads/' . $this->uri->segment(2) . '/documents/icb-rev_2022_abstract_template.docx'); ?>">abstract template</a> and <a class="font-light font-size-medium text-yellow" download href="<?php echo base_url('assets/uploads/' . $this->uri->segment(2) . '/documents/icb-rev_2022_paper_template.docx'); ?>">paper template</a>.
                </p>
                <ol class="font-size-medium text-navy font-light">
                    <li>Abstract and papers must be submitted through <a class="font-light font-size-medium text-yellow" href="https://easychair.org/conferences/?conf=icbrev2022" target="_blank">Easychair submission system</a> for ICB-REV 2022. A guideline on how to submit full paper conference is available <a download href="<?php echo base_url('assets/uploads/' . $this->uri->segment(2) . '/documents/guideline_for_easychair_icb-rev_2022.pdf'); ?>" class="font-light font-size-medium text-yellow">Guideline for Easychair</a>.</li>
                    <li>Papers must be submitted in a .pdf file format.</li>
                    <li>File size cannot exceed 50MB. Check image resolution or compress images before saving the file as .pdf.</li>
                    <li>Check that all fonts are converted properly when the .pdf is created. Pay close attention to special font types and symbols used in equations.</li>
                    <li>Please submit the Conference Paper in advance of the June 14, 2022 deadline.</li>
                </ol>
                <p class="text-navy font-light font-size-medium">
                    The presenting author should update any changes from the final paper; such as the title and/or author information. This can be done directly via <a class="font-light font-size-medium text-yellow" href="https://easychair.org/conferences/?conf=icbrev2022" target="_blank">Easychair</a>.
                </p>
            </div>
        </div>
    </div>
</section>
<section id="tentative-agenda-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-green">Tentative Agenda</h2>
                <p class="text-navy font-light font-size-medium">
                    The International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2022 will be as in the file below:
                </p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <a class="btn btn-custom bg-green text-navy" href="<?= base_url('assets/uploads/' . $this->uri->segment(2) . '/documents/tentative_agenda_of_icb-rev_2022.pdf'); ?>" target="_blank">View PDF Agenda</a>
            </div>
        </div>
    </div>
</section>
<section id="register-agenda-section" class="page-section bg-navy py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-green">Registration</h2>
                <p class="text-yellow font-light font-size-medium">
                    The ICB-REV 2022 participants will come from R&D Institutes, Universities, and Industries from all over the world. Registration shall be made as follow.
                </p>
            </div>
            <div class="col-md-12">
                <div class="nav nav-tabs no-border d-flex justify-content-center" id="nav-tab" role="tablist">
                    <?php $nationality = array_keys($event_details[$this->uri->segment(2)]['icb-rev_2022']['package_detail']); ?>
                    <?php $first_elem = $nationality[0]; ?>
                    <?php foreach ($nationality as $nation) : ?>
                        <a class="nav-item no-radius nav-link <?= $nation == $first_elem ? 'active' : ''; ?> btn btn-custom bg-yellow text-navy mx-3" id="<?= $nation; ?>-tab" data-toggle="tab" href="#nav-<?= $nation; ?>" role="tab" aria-controls="nav-home" aria-selected="true"><?= ucwords($nation); ?></a>
                    <?php endforeach; ?>
                </div>
                <div class="tab-content mt-5" id="nav-tabContent">
                    <?php foreach ($nationality as $nation) : ?>
                        <div class="tab-pane fade <?= $nation == $first_elem ? 'show active' : ''; ?>" id="nav-<?= $nation; ?>" role="tabpanel" aria-labelledby="<?= $nation; ?>-tab">
                            <div class="row">
                                <div class="col-md-12 d-flex justify-content-center flex-wrap">
                                    <?php foreach ($event_details[$this->uri->segment(2)]['icb-rev_2022']['package_detail'][$nation] as $occupation => $detail) : ?>
                                        <?php foreach ($detail as $type => $package_detail) : ?>
                                            <?php $currency_symbol = $package_detail['price']['normal']['currency'] == 'usd' ? 'US$' : 'Rp'; ?>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option <?= $type; ?>-option <?= $nation; ?>-option <?= $occupation; ?>-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium"><?= ucwords(str_replace('_', ' ', $type)); ?><br>(<?= ucwords($occupation); ?>)</h4>
                                                </div>
                                                <?php if (time() > strtotime($package_detail['price']['early_bird']['expired_time'])) : ?>
                                                    <div class="price-container mb-5">
                                                        <br>
                                                        <span class="discount-price font-size-biggest font-bold"><?= $currency_symbol . ' ' . number_format($package_detail['price']['normal']['amount']); ?></span>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="price-container mb-5">
                                                        <span class="normal-price font-size-tiny"><del><?= $currency_symbol . ' ' . number_format($package_detail['price']['normal']['amount']); ?></del></span>
                                                        <br />
                                                        <span class="discount-price font-size-biggest font-bold"><?= $currency_symbol . ' ' . number_format($package_detail['price']['early_bird']['amount']); ?><sup>*</sup></span>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="price-description">
                                                    <small>
                                                        <?php if (time() > strtotime($package_detail['price']['early_bird']['expired_time'])) : ?>
                                                            &nbsp;
                                                        <?php else : ?>
                                                            *before <?= date('d F, Y', strtotime($package_detail['price']['early_bird']['expired_time'])); ?>
                                                        <?php endif; ?>
                                                    </small>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <?php foreach ($package_detail['benefit'] as $benefit) : ?>
                                                            <li><?= $benefit; ?></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="countdown-section" class="page-section py-80 text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="section-title">Register Now</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="countdown" data-year="<?= date('Y', strtotime($event_date['icb-rev_2022'])); ?>" data-month="<?= date('m', strtotime($event_date['icb-rev_2022'])); ?>" data-day="<?= date('d', strtotime($event_date['icb-rev_2022'])); ?>" data-hours="<?= date('H', strtotime($event_date['icb-rev_2022'])); ?>"></div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5">
                <a class="btn btn-custom bg-navy text-yellow" href="<?= site_url('event/' . $this->uri->segment(2) . '/registration'); ?>">Register Now</a>
            </div>
        </div>
    </div>
</section>