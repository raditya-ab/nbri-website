<?php
$registered_user_data = $this->session->userdata('registration_data');
?>
<h1>Thank you for registering the ICB-REV 2022</h1>
<p>Your purchase details:</p>
<ul>
    <li>Order #: <?php echo $registered_user_data['order']['order_id']; ?></li>
    <li>Fullname: <?php echo $registered_user_data['salutation'] . ' ' . $registered_user_data['first_name'] . ' ' . $registered_user_data['last_name'] . $registered_user_data['suffixes']; ?></li>
    <li>Email address: <?= $registered_user_data['email']; ?></li>
    <li>Address: <?= $registered_user_data['address']; ?></li>
    <li>Phone number: <?= $registered_user_data['phone']; ?></li>
    <li>Institution / Affiliation: <?= $registered_user_data['affiliation']; ?></li>
    <li>Event Category: <?= $registered_user_data['event_category_name']; ?></li>
    <li>Package: <?= ucwords(str_replace('_', ' ', $registered_user_data['package_type'])); ?><br>(<?= ucwords($registered_user_data['occupation']); ?>)</li>
    <li>Nationality: <?= ucwords($registered_user_data['nationality']); ?></li>
    <?php if (in_array($registered_user_data['event_category'], ['ibc-rev_2022', 'nbri-yic_2022'])) : ?>
        <?php if (stripos($registered_user_data['conference_package'], 'participant') !== FALSE) : ?>
            <li>Symposia: <?= $registered_user_data['symposia']; ?></li>
            <li>Abstract Title: <?= $registered_user_data['conference_abstract-title']; ?></li>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($registered_user_data['order']['currency'] == 'idr') : ?>
        <li>Amount Transfer: Rp <b><?= $registered_user_data['order']['total_billed_in_idr']; ?></b></li>
    <?php else : ?>
        <li>Amount Transfer: US$ <b><?= $registered_user_data['total_event_price']; ?> / Rp <?= $registered_user_data['order']['total_billed_in_idr']; ?> (US$ 1 = <?= $registered_user_data['conversion_rates']['idr_rates']; ?>)</b></li>
    <?php endif; ?>
    <?php if ($registered_user_data['order']['transaction_status'] != 'capture'):?>
        <li>Please check further email for your payment instructions.</li>
    <?php endif;?>
</ul>
<br>
<br>
<h4>Thank you</h4>