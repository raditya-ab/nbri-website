<section id="about-section" class="page-section bg-yellow py-80">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<p>
					The latest report from the International Governmental Panel on Climate Change (IPCC) captures the full scale
					of the threat to human life in a heating world. The surge of carbon emissions in the last decade lead mother
					earth to the climate crisis. Various initiatives have taken to curb the climate catastrophe. From global
					treaty policy (Paris Agreement 2015 to Glasgow Climate Pact 2021) to technology intervention (renewable energy
					to battery electric vehicles innovation). Even, Indonesia G20 presidency makes the energy transition as one of
					priority agenda. Because energy transition follows the Paris Agreement, which targets net-zero emissions by
					2060.
					<br><br>
					Based on the current situation, the National Battery Research Institute (NBRI) will conduct the International
					Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2022 in collaboration with Queen
					Mary University of London (QMUL) United Kingdom, International Union of Material Research Societies (IUMRS)
					and Material Research Society Indonesia (MRS-INA). This conference will bring together scientists,
					academicians, industry partners, the government and all stakeholders that focus on battery technology for both
					Electric Vehicles and Renewable Energy. The ICB-REV 2022 will be the insightful space for all related
					stakeholders to disseminate their innovations, exchange their ideas & perspectives and also open international
					networking for bolstering the global energy transition agenda.
					<br><br>
					Since its establishment on December 07th 2020, the NBRI has performed various events covering the battery
					technology, renewable energy, and electric vehicles topics. NBRI has successfully conducted the <a class="text-white" href="<?= base_url('assets/uploads/' . $this->uri->segment(2) . '/documents/report_book_of_icb-rev_2021.pdf'); ?>">ICB-REV 2021</a>
					and <a class="text-white" href="<?= base_url('assets/uploads/' . $this->uri->segment(2) . '/documents/report_book_of_icamt_2021.pdf'); ?>">International Conference on Advanced Material and Technology (ICAMT) 2021</a> with 154 distinguished speakers
					across 16 countries. There were more than 150 selected articles published in AIP Proceedings, IONICS journal,
					and Progress on Natural Science and Material International (PNSMI) journal. In addition, NBRI has also
					organized International Battery School (IBS) 2021, Climate Challenge Workshop 2021, International School of
					Battery in Electric Vehicles (ISBEV) 2021, <a class="text-white" href="<?= base_url('assets/uploads/' . $this->uri->segment(2) . '/documents/report_book_of_imac_2021.pdf'); ?>">International Workshop on Material and Advanced Characterization
						(IMAC) 2021</a> and International Workshop on Solar Rooftop Residential and Utilities Scale (ISRUS) 2021.
					<br><br>
					The main concern of ICB-REV 2022 is focused on advanced battery technology from raw materials to cell
					fabrication, energy storage for renewable energy, and electric vehicles battery and charging station that
					support energy transition. This should be accomplished by the presence of invited world-class speakers,
					international participants, global industry players and international battery associations. The selected
					articles will be published in AIP international proceedings and other reputable journal Scopus indexed.
					<br><br>
					Following ICB-REV 2022, the consecutive events will be organized as complementary to broaden audiences such
					as:
				<ul>
					<li>International Battery School (IBS) on May 24th-25th, 2022</li>
					<li>NBRI Youth Ideas Competition (NBRI-YIC) on June 21th, 2022</li>
					<li>International Workshop on Solar Rooftop Residential and Utilities Scale (ISRUS) on July 13th-14th, 2022
					</li>
				</ul>
				<br><br>
				Due to the present situation of COVID-19, the format of presentation will be conducted online. We sincerely hope
				that you will enjoy the ICB-REV 2022 and have a pleasant experience.
				<br><br>
				Indonesia, 01 April 2022
				<br><br>
				<br><br>
				<b><u>Prof. Dr. rer. nat. Evvy Kartini</u></b><br>
				<i>Chair of the ICB-REV 2022</i>
				</p>
			</div>
			<div class="col-md-3">
				<img src="<?php echo base_url('assets/images/evvy.png'); ?>" alt="">
			</div>

		</div>
	</div>
</section>
<section id="scope-section" class="page-section my-80">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">Scopes</h2>
			</div>
			<div class="col-md-12 grid-card-wrapper grid-4 align-items-center justify-content-between">
				<div class="card">
					<!-- <a href=""> -->
					<div class="card-image">
						<img src="<?php echo base_url('assets/uploads/' . $this->uri->segment(2) . '/images/advanced_battery_technology_from_upstream_(raw_materials),_midstream_(cell_fabrication),_and_downstream_(applications_and_recycling).jpg'); ?>" alt="Advanced battery technology from raw materials to cell fabrication">
					</div>
					<div class="card-caption">
						<h3 class="card-title">Advanced battery technology from upstream (raw materials), midstream (cell fabrication), and downstream (applications and recycling).</h3>
						<!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
					</div>
					<!-- </a> -->
				</div>
				<div class="card">
					<!-- <a href=""> -->
					<div class="card-image">
						<img src="<?php echo base_url('assets/uploads/' . $this->uri->segment(2) . '/images/energy_storage_technology_for_renewable_energies_(solar,_wind,_biomass,_geothermal,_etc).jpg'); ?>" alt="">
					</div>
					<div class="card-caption">
						<h3 class="card-title">Energy storage technology for renewable energies (solar, wind, biomass, geothermal, etc)</h3>
						<!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
					</div>
					<!-- </a> -->
				</div>
				<div class="card">
					<!-- <a href=""> -->
					<div class="card-image">
						<img src="<?php echo base_url('assets/uploads/' . $this->uri->segment(2) . '/images/electric_vehicles_ecosystem_(battery,_electric_motor,_charging_station,_etc).jpg'); ?>" alt="">
					</div>
					<div class="card-caption">
						<h3 class="card-title">Electric vehicles ecosystem (battery, electric motor, charging station, etc)</h3>
						<!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
					</div>
					<!-- </a> -->
				</div>
				<div class="card">
					<!-- <a href=""> -->
					<div class="card-image">
						<img src="<?php echo base_url('assets/uploads/' . $this->uri->segment(2) . '/images/other_related_topics_(policy,_regulation,_standardization,_industry,_etc).jpg'); ?>" alt="">
					</div>
					<div class="card-caption">
						<h3 class="card-title">Other related topics (policy, regulation, standardization, industry, etc)</h3>
						<!-- <p class="card-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur, quae!</p> -->
					</div>
					<!-- </a> -->
				</div>
			</div>
			<div class="col-md-12 d-flex justify-content-center mt-5">
				<a href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda'); ?>" class="btn btn-custom bg-navy text-yellow">Register Now</a>
			</div>
		</div>
	</div>
</section>
<section id="calendar-section" class="page-section bg-green py-80">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">Mark the calendar</h2>
			</div>
			<div class="col-md-12 accordion-tab-wrapper ">
				<?php foreach ($important_dates[$this->uri->segment(2)] as $date) : ?>
					<div class="accordion-tab d-flex justify-content-between align-items-center bg-navy text-yellow">
						<div class="event-date text-center">
							<div class="date"><?= $date['short_value'][0] ?></div>
							<div class="month text-green"><?= $date['short_value'][1] ?></div>
						</div>
						<div class="event-description"><?= $date['name']; ?></div>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="col-md-12 d-flex justify-content-center mt-5" style="flex-wrap: wrap">
				<!-- <a class="btn btn-custom bg-yellow text-navy mx-3"><i class="fab fa-google"></i> Add events to Google Calendar</a> -->
				<a href="<?php echo base_url('assets/' . $this->uri->segment(2) . '.ics'); ?>" download class="btn btn-custom bg-navy text-green mx-3"><i class="far fa-calendar-alt"></i> Add to calendar</a>
			</div>
		</div>
	</div>
</section>
<section id="speakers-section" class="page-section bg-navy py-80">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title">Speakers</h2>
			</div>
			<div class="col-md-12 list-card-wrapper">
				<?php $i = 1; ?>
				<?php foreach ($event_details[$this->uri->segment(2)]['icb-rev_2022']['speakers']['homepage']['content'] as $speaker) : ?>
					<div class="card-list <?= $i % 2 == 0 ? 'right' : 'left'; ?> d-flex mb-5">
						<div class="card-caption <?= $i % 2 == 0 ? 'mr-5' : 'ml-5'; ?>">
							<h3 class="card-title"><?= $speaker['name']; ?></h3>
							<p><?= $speaker['subtitle']; ?></p>
							<p class="card-description"><?= $speaker['description']; ?></p>
						</div>
						<div class="card-image image-stack bg-yellow">
							<div class="image-stack-item image-stack-item__top">
								<img class="img-responsive image-stack-item image-stack-item__top" style="max-height: 400px" src="<?php echo $speaker['pic']; ?>" alt="">
							</div>
							<div class="image-stack-item image-stack-item__bottom bg-yellow"></div>

						</div>
					</div>
					<?php $i++; ?>
				<?php endforeach; ?>
			</div>
			<div class="col-md-12 d-flex justify-content-center mt-5">
				<a href="<?php echo site_url('event/' . $this->uri->segment(2) . '/speakers'); ?>" class="btn btn-custom bg-yellow text-navy">View All Speakers</a>
			</div>
		</div>
	</div>
</section>
<section id="countdown-section" class="page-section py-80 text-center">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="section-title">Register Now</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="countdown" data-year="<?= date('Y', strtotime($event_date['icb-rev_2022'])); ?>" data-month="<?= date('m', strtotime($event_date['icb-rev_2022'])); ?>" data-day="<?= date('d', strtotime($event_date['icb-rev_2022'])); ?>" data-hours="<?= date('H', strtotime($event_date['icb-rev_2022'])); ?>"></div>
			</div>
			<div class="col-md-12 d-flex justify-content-center mt-5">
				<a href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda'); ?>" class="btn btn-custom bg-navy text-yellow">Register Now</a>
			</div>
		</div>
	</div>
</section>