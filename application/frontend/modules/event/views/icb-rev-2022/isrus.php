<style>
    .card .card-image {
        background-color: #FFF;
    }
</style>
<section id="school-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-title text-navy text-center">Lecturers</h2>
            </div>
            <div class="col-md-12 grid-card-wrapper grid-5 align-items-stretch justify-content-center">
                <?php foreach ($event_details[$this->uri->segment(2)]['isrus_2022']['detail']['lecturers'] as $i => $lecturer) : ?>
                    <div class="card mx-2">
                        <a href="javascript:void(0)" data-target="#<?= $lecturer['slug'] . '-' . $i; ?>" data-toggle="modal">
                            <div class="card-image" style="background-image: url('<?= $lecturer['pic']; ?>'); background-size: cover; background-position: center; min-height: 242px; width: 100%;">

                            </div>
                            <div class="card-caption">
                                <h3 class="card-title"><?= $lecturer['name']; ?></h3>
                                <p class="card-description"><?= $lecturer['subtitle']; ?></p>
                            </div>
                        </a>
                    </div>
                    <!-- #<?= $lecturer['slug']; ?> modal -->
                    <div class="modal modal-member fade" id="<?= $lecturer['slug'] . '-' . $i; ?>" tabindex="-1" role="dialog" aria-labelledby="<?= $lecturer['slug']; ?>" aria-hidden="true">
                        <div class="modal-dialog modal-full modal-dialog-centered" role="document">
                            <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
                                <div class="modal-body p-0">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">
                                            <img src="<?= base_url('assets/images/clear_24px.svg'); ?>" alt="">
                                        </span>
                                    </button>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?= $lecturer['pic']; ?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                                            </div>
                                            <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                                                <?= $lecturer['name']; ?>
                                            </h3>
                                            <p class="subtitle"><?= $lecturer['subtitle']; ?></p>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="description"><?= $lecturer['description']; ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer no-border p-0">
                                    <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<style>
    #wywl-wrap .slick-arrow {
        color: #fff;
        z-index: 1000
    }

    #wywl-wrap .slick-prev {
        left: 0
    }

    #wywl-wrap .slick-next {
        right: 0
    }
</style>
<section class="page-section py-80 bg-yellow">
    <div class="container">
        <div class="row mt-5 text-navy">
            <div class="col-md-5 flex">
                <h2 class="section-title text-navy p-0 m-0 mb-5">What you will <br /><b class="font-bold" style="font-size: 64px">LEARN.</b></h2>
            </div>
            <div class="col-md-7" id="wywl-wrapper">
                <div id="wywl-wrap" class="overflow-hidden mx-auto">
                    <?php foreach ($event_details[$this->uri->segment(2)]['isrus_2022']['detail']['what-you-will-learn'] as $wywl) : ?>
                        <div class="card-word bg-navy text-green p-5 mx-3" style="min-width: 300px">
                            <h3><?= $wywl['name']; ?></h3>
                            <p>
                            <ul class="p-4">
                                <?php foreach ($wywl['content'] as $content) : ?>
                                    <li class="font-size-small"><?= $content; ?></li>
                                <?php endforeach; ?>
                            </ul>
                            </p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page-section py-80">
    <div class="container">
        <div class="row mt-5 text-navy">
            <div class="col-md-12 text-center">
                <h2 class="section-title text-navy p-0 m-0 mb-5">What you will <br><b class="font-bold" style="font-size: 64px">GET.</b></h2>
                <p class="font-size-medium">
                    <?php foreach ($event_details[$this->uri->segment(2)]['ibs_2022']['detail']['what-you-will-get'] as $wywg) : ?>
                        <?= $wywg; ?> <br />
                    <?php endforeach; ?>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="page-section py-80 bg-green">
    <div class="container">
        <div class="row mt-5 text-navy">
            <div class="col-md-12 text-center">
                <h2 class="section-title text-navy p-0 m-0 mb-5"><b class="font-bold" style="font-size: 64px">Time &amp; Venue</b></h2>
                <p class="font-size-big text-bold">
                    <?php
                        $start_date = $event_details[$this->uri->segment(2)]['isrus_2022']['event_date']['start_date'];
                        $end_date = $event_details[$this->uri->segment(2)]['isrus_2022']['event_date']['end_date'];
                        if($start_date != $end_date) {
                            echo date('j', strtotime($start_date)) . ' - ' . date('j F Y', strtotime($end_date));
                        } else {
                            echo date('j', strtotime($start_date));
                        }
                        ?>
                </p>
                <p class="font-size-medium">
                    Online Zoom Meeting
                </p>
            </div>
        </div>
    </div>
</section>
<section class="page-section py-80 bg-navy">
    <div class="container">
        <div class="row mt-5 text-navy">
            <div class="col-md-12 text-center">
                <h2 class="section-title text-yellow p-0 m-0 mb-5"><b class="font-bold" style="font-size: 64px">Participant Fee</b></h2>
                <div class="nav nav-tabs no-border d-flex justify-content-center" id="nav-tab" role="tablist">
                    <?php $nationality = array_keys($event_details[$this->uri->segment(2)]['isrus_2022']['package_detail']); ?>
                    <?php $first_elem = $nationality[0]; ?>
                    <?php foreach ($nationality as $nation) : ?>
                        <a class="nav-item no-radius nav-link <?= $nation == $first_elem ? 'active' : ''; ?> btn btn-custom bg-yellow text-navy mx-3" id="<?= $nation; ?>-tab" data-toggle="tab" href="#nav-<?= $nation; ?>" role="tab" aria-controls="nav-home" aria-selected="true"><?= ucwords($nation); ?></a>
                    <?php endforeach; ?>
                </div>
                <div class="tab-content mt-5" id="nav-tabContent">
                    <?php foreach ($nationality as $nation) : ?>
                        <div class="tab-pane fade <?= $nation == $first_elem ? 'show active' : ''; ?>" id="nav-<?= $nation; ?>" role="tabpanel" aria-labelledby="<?= $nation; ?>-tab">
                            <div class="row">
                                <div class="col-md-12 d-flex justify-content-center flex-wrap">
                                    <?php foreach ($event_details[$this->uri->segment(2)]['isrus_2022']['package_detail'][$nation] as $occupation => $detail) : ?>
                                        <?php foreach ($detail as $type => $package_detail) : ?>
                                            <?php $currency_symbol = $package_detail['price']['normal']['currency'] == 'usd' ? 'US$' : 'Rp'; ?>
                                            <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option <?= $type; ?>-option <?= $nation; ?>-option <?= $occupation; ?>-option">
                                                <div class="card-title-container mb-5">
                                                    <h4 class="font-size-big font-weight-medium"><?= ucwords(str_replace('_', ' ', $type)); ?><br>(<?= ucwords($occupation); ?>)</h4>
                                                </div>
                                                <?php if (time() > strtotime($package_detail['price']['early_bird']['expired_time'])) : ?>
                                                    <div class="price-container mb-5">
                                                        <br>
                                                        <span class="discount-price font-size-biggest font-bold"><?= $currency_symbol . ' ' . number_format($package_detail['price']['normal']['amount']); ?></span>
                                                    </div>
                                                <?php else : ?>
                                                    <div class="price-container mb-5">
                                                        <span class="normal-price font-size-tiny"><del><?= $currency_symbol . ' ' . number_format($package_detail['price']['normal']['amount']); ?></del></span>
                                                        <br />
                                                        <span class="discount-price font-size-biggest font-bold"><?= $currency_symbol . ' ' . number_format($package_detail['price']['early_bird']['amount']); ?><sup>*</sup></span>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="price-description">
                                                    <small>
                                                        <?php if (time() > strtotime($package_detail['price']['early_bird']['expired_time'])) : ?>
                                                            &nbsp;
                                                        <?php else : ?>
                                                            *before <?= date('d F, Y', strtotime($package_detail['price']['early_bird']['expired_time'])); ?>
                                                        <?php endif; ?>
                                                    </small>
                                                    <hr>
                                                    <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                        <?php foreach ($package_detail['benefit'] as $benefit) : ?>
                                                            <li><?= $benefit; ?></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="countdown-section" class="page-section py-80 text-center">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="section-title">Register Now</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="countdown" data-year="<?= date('Y', strtotime($event_date['isrus_2022'])); ?>" data-month="<?= date('m', strtotime($event_date['isrus_2022'])); ?>" data-day="<?= date('d', strtotime($event_date['isrus_2022'])); ?>" data-hours="<?= date('H', strtotime($event_date['isrus_2022'])); ?>"></div>
            </div>
            <div class="col-md-12 d-flex justify-content-center mt-5">
                <a class="btn btn-custom bg-navy text-yellow" href="<?= site_url('event/' . $this->uri->segment(2) . '/registration'); ?>">Register Now</a>
            </div>
        </div>
    </div>
</section>