<section id="abstract-section" class="page-section py-80">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title text-yellow">Steering Comittee</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Dr. rer. nat. Evvy Kartini</div>
			<div class="col font-size-medium"><i>Founder of National Battery Research Institute</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Alan J. Drew</div>
			<div class="col font-size-medium"><i>Co-Founder of National Battery Research Institute</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Rodrigo Martins</div>
			<div class="col font-size-medium"><i>President of IUMRS</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Ir. Muhammad Nizam, ST., MT., P.hD.</div>
			<div class="col font-size-medium"><i>Former Coordinator of National Research Priority Mandatory on Energy
					Storage.</i></div>
		</div>

		<div class="row">
			<div class="col-md-12 mt-5">
				<h2 class="section-title text-yellow">International Advisory Board</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Rodrigo Martins</div>
			<div class="col font-size-medium"><i>President of IUMRS, Portugal</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Dr. rer. nat. Evvy Kartini</div>
			<div class="col font-size-medium"><i>President of MRS-INA, Indonesia</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Dr. B.V.R. Chowdary</div>
			<div class="col font-size-medium"><i>IUMRS HO Officer, Singapore</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Dr. Yuan Ping Feng</div>
			<div class="col font-size-medium"><i>IUMRS Officer, Singapore</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Dr. Hideo Hosono</div>
			<div class="col font-size-medium"><i>MRS-Japan</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Dr. Jow-Lay Huang</div>
			<div class="col font-size-medium"><i>MRS-Taiwan</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Dr. Woo-Gwang Jung</div>
			<div class="col font-size-medium"><i>MRS-South Korea</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Dr. Soo Wohn Lee</div>
			<div class="col font-size-medium"><i>IUMRS Officer, South Korea</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Dr. Santi Maensiri</div>
			<div class="col font-size-medium"><i>President of MRS-Thailand</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. YF Han</div>
			<div class="col font-size-medium"><i>MRS-China</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Tim J White</div>
			<div class="col font-size-medium"><i>President of MRS-Singapore</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Alan J Drew</div>
			<div class="col font-size-medium"><i>Director of the Materials Research Institute of QMUL, United Kingdom</i>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Dr. Ana Jorge Sobrido</div>
			<div class="col font-size-medium"><i>Queen Mary University of London, UK</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Dr. Alexey Glushenkov</div>
			<div class="col font-size-medium"><i>Australia National University (ANU), Australia</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Prof. Pooi See Lee</div>
			<div class="col font-size-medium"><i>Nanyang Technological University, Singapore</i></div>
		</div>

		<div class="row">
			<div class="col-md-12 mt-5">
				<h2 class="section-title text-yellow">Organizing Comittee</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Chair</div>
			<div class="col font-size-medium"><i>Prof. Dr. rer. nat. Evvy Kartini</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Vice Chair</div>
			<div class="col font-size-medium"><i>Prof. Alan J Drew</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Secretary</div>
			<div class="col font-size-medium"><i>Shinta Widyaningrum, S.Sos.</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Treasury</div>
			<div class="col font-size-medium"><i>Adit Tri Wiguno, SE</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Coordinator of ICB-REV 2021</div>
			<div class="col font-size-medium"><i>Moh. Wahyu Syafi’ul Mubarok, S.Si. </i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Coordinator for Journal Publication</div>
			<div class="col font-size-medium"><i>Muhammad Fakhrudin, ST. </i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Coordinator of IBS 2022</div>
			<div class="col font-size-medium"><i>Muhammad Ridho Nugraha, S.T. </i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Coordinator of ISRUS 2022</div>
			<div class="col font-size-medium"><i>Dhiko Rosanda, ST.</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">International and National Relations</div>
			<div class="col font-size-medium"><i>Muhammad Firmansyah, SE.</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Press Conference & Media Partners</div>
			<div class="col font-size-medium"><i>Muhammad Subhan Alkyana, S.IP.</i></div>
		</div>
		<div class="row">
			<div class="col-md-5 font-size-medium font-bold">Social Media</div>
			<div class="col font-size-medium"><i>Shafira Ramadhani</i></div>
		</div>
	</div>
</section>