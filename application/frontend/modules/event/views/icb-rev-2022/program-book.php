<section id="abstract-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="text-navy font-light font-size-medium">
									<!-- Coming Soon.. -->
                  Please find the detailed agenda as well as the presentation schedule in the program book. You may download it <a download href="<?=base_url('assets/uploads/icb-rev-2022/documents/icb-rev_2022_program_book.pdf');?>" class="font-bold text-yellow">here</a>.
                </p>
            </div>
        </div>
    </div>
</section>