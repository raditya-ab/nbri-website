<section id="supporter-section" class="page-section bg-green text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="supporting-title">Supported by:</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img class="supporters" src="<?=base_url('assets/images/footer-assets/iumrs.png');?>" alt="" height="75">
            </div>
            <div class="col-md-4">
                <img class="supporters" src="<?=base_url('assets/images/footer-assets/qmul.png');?>" alt="" height="75">
            </div>
            <div class="col-md-4">
                <img class="supporters" src="<?=base_url('assets/images/footer-assets/mrs-ina.png');?>" alt="" height="75">
            </div>
        </div>
    </div>
</section>
<style>
    .control-label {
        font-size: 18px;
    }
</style>
<section id="abstract-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div id="smartwizard" class="col-md-12">
                <ul class="nav">
                    <li>
                        <a class="nav-link font-size-small" href="#step-1">
                            Step 1
                        </a>
                    </li>
                    <li>
                        <a class="nav-link font-size-small" href="#step-2">
                            Step 2
                        </a>
                    </li>
                    <li>
                        <a class="nav-link font-size-small" href="#step-3">
                            Step 3
                        </a>
                    </li>
                    <li>
                        <a class="nav-link font-size-small" href="#step-4">
                            Step 4
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="step-1" class="tab-pane" role="tabpanel">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">Select Category</label>
                            </div>
                            <div class="form-group col-md-12 grid-card-wrapper grid-3 align-items-center justify-content-between mb-5">
                                <div class="card">
                                    <label class="form-check-label" for="conference">
                                        <div class="card-image">
                                            <img src="<?php echo base_url('assets/images/conference.png');?>"
                                                alt="International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2021">
                                        </div>
                                        <div class="card-caption text-center">
                                            <h3 class="card-title">International Conference on Battery for Renewable
                                                Energy and Electric Vehicles (ICB-REV) 2021</h3>
                                            <div class="form-check">
                                                <input class="form-check-input" name="event_category" id="conference" type="radio" value="conference">
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                <div class="card">
                                    <label class="form-check-label" for="school">
                                        <div class="card-image">
                                            <img src="<?php echo base_url('assets/images/school.png');?>" alt="">
                                        </div>
                                        <div class="card-caption text-center">
                                            <h3 class="card-title">International School of Battery Technology</h3>
                                            <div class="form-check">
                                                <input class="form-check-input" name="event_category" id="school" type="radio" value="school">
                                            </div>
                                        </div>
                                    </label>
                                </div>
                                <div class="card">
                                    <label class="form-check-label" for="exhibition">
                                        <div class="card-image">
                                            <img src="<?php echo base_url('assets/images/exhibition.png');?>" alt="">
                                        </div>
                                        <div class="card-caption text-center">
                                            <h3 class="card-title">Exhibitions (Research Centers, Universities, and
                                                Industries)</h3>
                                            <div class="form-check">
                                                <input class="form-check-input" name="event_category" id="exhibition" type="radio" value="exhibition">
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div id="conference_form" class="row registration-form-field">
                            <div class="form-group col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Select Nationality</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="nationality" id="" value="indonesian">
                                            Indonesian
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="nationality" id="" value="international">
                                            International
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Select Occupation</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="occupation" id="" value="student">
                                            Student
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="occupation" id="" value="regular">
                                            Regular
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="control-label">Select package</label>
                                    </div>
                                    <div id="international_student-option" class="col-md-12 d-flex justify-content-start flex-wrap mb-3">
                                        <div class="price-card bg-green p-5 text-center mb-3 mr-3">
                                            <div class="card-title-container mb-5">
                                                <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                            </div>
                                            <div class="price-container mb-5">
                                                <span class="normal-price font-size-tiny"><del>US$ 300</del></span>
                                                <br/>
                                                <span class="discount-price font-size-biggest font-bold">US$ 250<sup>*</sup></span>
                                            </div>
                                            <div class="price-description">
                                                <small>*before June 14th, 2021</small>
                                                <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Select</a>
                                                <hr>
                                                <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                    <li>Participate all conference programs</li>
                                                    <li>Oral presentation</li>
                                                    <li>International certificate</li>
                                                    <li>International publication</li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input class="form-check-input" name="conference-package" type="radio" value="international_regular-present">
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-card bg-green p-5 text-center mb-3 mr-3">
                                            <div class="card-title-container mb-5">
                                                <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                            </div>
                                            <div class="price-container mb-5">
                                                <br/>
                                                <span class="discount-price font-size-biggest font-bold">US$ 100</span>
                                            </div>
                                            <div class="price-description">
                                                <small>&nbsp;</small>

                                                <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                                <hr>
                                                <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                    <li>Attend all conference programs</li>
                                                    <li>International certificate</li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input class="form-check-input" name="conference-package" type="radio" value="international_regular-present">
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="international_regular-option" class="col-md-12 d-flex justify-content-start flex-wrap mb-3">
                                        <div class="price-card bg-green p-5 text-center mb-3 mr-3">
                                            <div class="card-title-container mb-5">
                                                <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                            </div>
                                            <div class="price-container mb-5">
                                                <span class="normal-price font-size-tiny"><del>US$ 400</del></span>
                                                <br/>
                                                <span class="discount-price font-size-biggest font-bold">US$ 350<sup>*</sup></span>
                                            </div>
                                            <div class="price-description">
                                                <small>*before June 14th, 2021</small>
                                                <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                                <hr>
                                                <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                    <li>Participate all conference programs</li>
                                                    <li>Oral presentation</li>
                                                    <li>International certificate</li>
                                                    <li>International publication</li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input class="form-check-input" name="conference-package" type="radio" value="international_regular-present">
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-card bg-green p-5 text-center mb-3 mr-3">
                                            <div class="card-title-container mb-5">
                                                <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                            </div>
                                            <div class="price-container mb-5">
                                                <br>
                                                <span class="discount-price font-size-biggest font-bold">US$ 150</span>
                                            </div>
                                            <div class="price-description">
                                                <small>&nbsp;</small>

                                                <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                                <hr>
                                                <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                    <li>Attend all conference programs</li>
                                                    <li>International certificate</li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input class="form-check-input" name="conference-package" type="radio" value="international_regular-present">
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="indonesian_student-option" class="col-md-12 d-flex justify-content-start flex-wrap">
                                        <div class="price-card bg-green p-5 text-center mb-3 mr-3">
                                            <div class="card-title-container mb-5">
                                                <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
                                            </div>
                                            <div class="price-container mb-5">
                                                <span class="normal-price font-size-tiny"><del>Rp 2.500.000</del></span>
                                                <br>
                                                <span class="discount-price font-size-biggest font-bold">Rp 2.000.000<sup>*</sup></span>
                                            </div>
                                            <div class="price-description">
                                                <small>*before June 14th, 2021</small>
                                                <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                                <hr>
                                                <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                    <li>Participate all conference programs</li>
                                                    <li>Oral presentation</li>
                                                    <li>International certificate</li>
                                                    <li>International publication</li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input class="form-check-input" name="conference-package" type="radio" value="international_regular-present">
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="price-card bg-green p-5 text-center mb-3 mr-3">
                                            <div class="card-title-container mb-5">
                                                <h4 class="font-size-big font-weight-medium">Participant<br/>(Student)</h4>
                                            </div>
                                            <div class="price-container mb-5">
                                                <br>
                                                <span class="discount-price font-size-biggest font-bold">Rp 1.000.000</span>
                                            </div>
                                            <div class="price-description">
                                                <small>&nbsp;</small>
                                                <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                                <hr>
                                                <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                    <li>Attend all conference programs</li>
                                                    <li>International certificate</li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input class="form-check-input" name="conference-package" type="radio" value="international_regular-present">
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="indonesian_regular-option" class="col-md-12 d-flex justify-content-start flex-wrap">
                                        <div class="price-card bg-green p-5 text-center mb-3 mr-3">
                                            <div class="card-title-container mb-5">
                                                <h4 class="font-size-big font-weight-medium">Presenter<br>(Regular)</h4>
                                            </div>
                                            <div class="price-container mb-5">
                                                <span class="normal-price font-size-tiny"><del>Rp 3.000.000</del></span>
                                                <br>
                                                <span class="discount-price font-size-biggest font-bold">Rp 2.500.000<sup>*</sup></span>
                                            </div>
                                            <div class="price-description">
                                                <small>*before June 14th, 2021</small>
                                                <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                                <hr>
                                                <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                    <li>Participate all conference programs</li>
                                                    <li>Oral presentation</li>
                                                    <li>International certificate</li>
                                                    <li>International publication</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="price-card bg-green p-5 text-center mb-3 mr-3">
                                            <div class="card-title-container mb-5">
                                                <h4 class="font-size-big font-weight-medium">Participant<br/>(Regular)</h4>
                                            </div>
                                            <div class="price-container mb-5">
                                                <br>
                                                <span class="discount-price font-size-biggest font-bold">Rp 1.500.000</span>
                                            </div>
                                            <div class="price-description">
                                                <small>&nbsp;</small>
                                                <a href="#" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small mt-3">Register</a>
                                                <hr>
                                                <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                    <li>Attend all conference programs</li>
                                                    <li>International certificate</li>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                    <input class="form-check-input" name="conference-package" type="radio" value="international_regular-present">
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div id="step-2" class="tab-pane" role="tabpanel">
                        <input type="hidden" name="occupation">
                        <input type="hidden" name="occupation">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label required">Choose event category</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-4">
                                <div class="form-check">
                                    <label class="form-check-label font-size-small">
                                    <input type="radio" class="form-check-input" name="event_category" id="">
                                    International Conference on Battery for Renewable Energy and Electric Vehicles (ICB-REV) 2021
                                </label>
                                </div>
                            </div>
                            <div class="form-group col-4">
                                <div class="form-check">
                                    <label class="form-check-label font-size-small">
                                    <input type="radio" class="form-check-input" name="event_category" id="">
                                    International School of Battery Technology 2021
                                </label>
                                </div>
                            </div>
                            <div class="form-group col-4">
                                <div class="form-check">
                                    <label class="form-check-label font-size-small">
                                    <input type="radio" class="form-check-input" name="event_category" id="">
                                    Exhibition (Research Centers, Universities, and Industries)
                                </label>
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-10 form-group">
                                <label for="prefix" class="control-label required">Prefixes</label>
                                <select class="form-control" name="" id="">
                                    <option>Select option</option>
                                    <option value="">Prof.</option>
                                    <option value="">Dr.</option>
                                    <option value="PhD">PhD</option>
                                    <option value="">Mr</option>
                                    <option value="">Mrs</option>
                                    <option value="">Ms.</option>
                                    <option value="">Other</option>
                                  </select>
                            </div>
                            <div class="col-2 form-group" id="prefix-other-container">
                                <label for="prefix-other" class="control-label required">Enter Prefix</label>
                                <input type="text" name="prefix-other" id="prefix-other" class="form-control">
                            </div>
                            <div class="col-6 form-group">
                                <label for="full-name" class="control-label required">Firstname</label>
                                <input type="text" name="full-name" id="full-name" class="form-control">
                            </div>
                            <div class="col-6 form-group">
                                <label for="full-name" class="control-label required">Lastname</label>
                                <input type="text" name="full-name" id="full-name" class="form-control">
                            </div>
                            <div class="col-12 form-group">
                                <label for="suffixes" class="control-label">Suffixes</label>
                                <input type="text" name="suffixes" id="suffixes" class="form-control">
                            </div>
                            <div class="col-12 form-group">
                                <label for="email" class="control-label required">Email</label>
                                <input type="email" name="email" id="email" class="form-control">
                            </div>
                            <div class="col-12 form-group">
                                <label for="affiliation" class="control-label required+">Affiliation / Institution</label>
                                <input type="text" name="affiliation" id="affiliation" class="form-control">
                            </div>
                            <div class="col-12 form-group">
                                <label for="address" class="control-label required">Address</label>
                                <input type="text" name="address" id="address" class="form-control">
                            </div>
                            <div class="col-12 form-group">
                                <label for="phone" class="control-label">Phone</label>
                                <input type="text" name="phone" id="phone" class="form-control">
                            </div>
                        </div>
                        <div id="conference-form mw-100">
                            <div class="row">
                                <div class="col-12 form-group">
                                    <label for="symposia" class="control-label required">Occupation</label>
                                    <select class="form-control" name="" id="">
                                        <option>Select option</option>
                                        <option value="">International Regular</option>
                                        <option value="">Indonesia Regular</option>
                                        <option value="PhD">International Student</option>
                                        <option value="">Indonesia Student</option>
                                      </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 form-group">
                                    <label for="symposia" class="control-label required">Participation</label>
                                    <select class="form-control" name="" id="">
                                        <option>Select option</option>
                                        <option value="">Conference Speaker</option>
                                        <option value="">Participant Only</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 form-group">
                                    <label for="symposia" class="control-label required">Symposia</label>
                                    <select class="form-control" name="" id="">
                                        <option>Select option</option>
                                        <option value="">Advanced battery technology from raw materials to cell fabrication</option>
                                        <option value="">Energy storage for renewable energy</option>
                                        <option value="">Electric vehicles battery and charging station</option>
                                        <option value="">Other related topics</option>
                                    </select>
                                </div>
                                <div class="col-12 form-group">
                                    <label for="abstract-title" class="control-label required">Abstract Title</label>
                                    <input type="text" name="conference_abstract-title" id="conference_abstract-title" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div id="school-form mw-100">
                            <div class="row">
                                <div class="col-12 form-group">
                                    <label for="symposia" class="control-label">Symposia</label>
                                    <input type="text" name="conference_symposia" id="conference_symposia" class="form-control">
                                </div>
                                <div class="col-12 form-group">
                                    <label for="abstract-title" class="control-label">Abstract Title</label>
                                    <input type="text" name="conference_abstract-title" id="conference_abstract-title" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div id="exhibition-form mw-100">
                            <div class="row">
                                <div class="col-12 form-group">
                                    <label for="symposia" class="control-label">Symposia</label>
                                    <input type="text" name="conference_symposia" id="conference_symposia" class="form-control">
                                </div>
                                <div class="col-12 form-group">
                                    <label for="abstract-title" class="control-label">Abstract Title</label>
                                    <input type="text" name="conference_abstract-title" id="conference_abstract-title" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-3" class="tab-pane" role="tabpanel">
                        Step content
                    </div>
                    <div id="step-4" class="tab-pane" role="tabpanel">
                        Step content
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col d-flex justify-content-between">
                        <div class="form-group">
                            <button class="btn btn-custom text-navy" type="button" style="background: #DDD">Reset
                                Form</button>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-custom bg-navy text-yellow sw-btn-prev"
                                type="button">Previous</button>
                            <button class="btn btn-custom bg-navy text-yellow sw-btn-next" type="button">Next</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            // var currentTab = 0; // Current tab is set to be the first tab (0)
            // showTab(currentTab); // Display the current tab

            // function showTab(n) {
            //     // This function will display the specified tab of the form ...
            //     var x = document.getElementsByClassName("tab");
            //     x[n].style.display = "block";
            //     // ... and fix the Previous/Next buttons:
            //     if (n == 0) {
            //         document.getElementById("prevBtn").style.display = "none";
            //     } else {
            //         document.getElementById("prevBtn").style.display = "inline";
            //     }
            //     if (n == (x.length - 1)) {
            //         document.getElementById("nextBtn").innerHTML = "Submit";
            //     } else {
            //         document.getElementById("nextBtn").innerHTML = "Next";
            //     }
            //     // ... and run a function that displays the correct step indicator:
            //     fixStepIndicator(n)
            // }

            // function nextPrev(n) {
            //     // This function will figure out which tab to display
            //     var x = document.getElementsByClassName("tab");
            //     // Exit the function if any field in the current tab is invalid:
            //     if (n == 1 && !validateForm()) return false;
            //     // Hide the current tab:
            //     x[currentTab].style.display = "none";
            //     // Increase or decrease the current tab by 1:
            //     currentTab = currentTab + n;
            //     // if you have reached the end of the form... :
            //     if (currentTab >= x.length) {
            //         //...the form gets submitted:
            //         document.getElementById("regForm").submit();
            //         return false;
            //     }
            //     // Otherwise, display the correct tab:
            //     showTab(currentTab);
            // }

            // function validateForm() {
            //     // This function deals with validation of the form fields
            //     var x, y, i, valid = true;
            //     x = document.getElementsByClassName("tab");
            //     y = x[currentTab].getElementsByTagName("input");
            //     // A loop that checks every input field in the current tab:
            //     for (i = 0; i < y.length; i++) {
            //         // If a field is empty...
            //         if (y[i].value == "") {
            //             // add an "invalid" class to the field:
            //             y[i].className += " invalid";
            //             // and set the current valid status to false:
            //             valid = false;
            //         }
            //     }
            //     // If the valid status is true, mark the step as finished and valid:
            //     if (valid) {
            //         document.getElementsByClassName("step")[currentTab].className += " finish";
            //     }
            //     return valid; // return the valid status
            // }

            // function fixStepIndicator(n) {
            //     // This function removes the "active" class of all steps...
            //     var i, x = document.getElementsByClassName("step");
            //     for (i = 0; i < x.length; i++) {
            //         x[i].className = x[i].className.replace(" active", "");
            //     }
            //     //... and adds the "active" class to the current step:
            //     x[n].className += " active";
            // }

        </script>
    </div>
</section>

<section id="drop-email-section" class="page-section py-80 bg-yellow">
    <div class="container">
        <?php echo form_open('event/icbrev2021/a',array('class' => 'form w-100','id' => 'form_drop_email','method'=>'post'),array('event_name' => 'icb-rev-2021','y-captcha-response'=>''));?>
            <div class="row">
                <div class="col d-flex flex-column align-items-stretch">
                    <h2 class="section-title mb-0">Need more info?</h2>
                    <p class="text-left">Drop us your email</p>
                </div>
                <div class="col-md-7 d-flex align-items-center">
                    <input type="text" name="email" class="custom-input no-border text-big text-navy">
                </div>
                <div class="col d-flex align-items-center justify-content-center mt-3">
                    <button type="button" data-sitekey="ae5242f9-574b-48bd-bd78-3055d070f1cc" data-callback="submitForm" class="btn btn-custom bg-navy text-green h-captcha"><i class="fas fa-paper-plane"></i> Submit</button>
                </div>
            </div>
        <?php echo form_close();?>
    </div>
</section>  