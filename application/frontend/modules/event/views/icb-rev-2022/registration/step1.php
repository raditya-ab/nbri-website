<style>
    .control-label {
        font-size: 18px;
    }

    .btn-custom.bg-navy:hover {
        color: #f7af00
    }

    .hover-underline:hover {
        text-decoration: underline;
    }
</style>

<section id="registration-section" class="page-section py-80">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <?php $queryParams = strlen($this->input->server('QUERY_STRING')) > 0 ? '?' . $this->input->server('QUERY_STRING') : ''; ?>
                <ul class="nav justify-content-center flex-nowrap">
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 1) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 1) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step1'); ?>">Choose event</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 2) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 2) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step2' . $queryParams); ?>">Choose event package</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 3) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 3) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step3' . $queryParams); ?>">Fill the details</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 4) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 4) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step4' . $queryParams); ?>">Checkout &amp; Payment</a>
                    </li>
                </ul>
            </div>
            <?php if (array_key_exists('error', $_SESSION)) : ?>
                <div class="col-md-12 mt-5">
                    <div class="alert alert-danger p-5 alert-dismissible fade show font-size-medium" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong>Error!</strong> <br><?php print_r($_SESSION['error']); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php echo form_open(site_url('event/' . $this->uri->segment(2) . '/registration/process_step'), [
            'id' => 'form-registration-event',
            'class' => 'form',
            'method' => 'post'
        ], [
            'current_step' => 1,
            'event_category_name' => '',
        ]);
        ?>
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="" class="control-label">Select Event Category</label>
                    <div class="grid-card-wrapper grid-3 align-items-center justify-content-between">
                        <?php foreach ($event_details[$this->uri->segment(2)] as $event_name => $event) : ?>
                            <?php if(time() < strtotime($event_date[$event_name])):?>
                            <div class="card">
                                <label class="form-check-label" for="rbo_<?= $event_name; ?>">
                                    <div class="card-image">
                                        <img src="<?php echo base_url('assets/uploads/' . $this->uri->segment(2) . '/images/' . $event_name . '.jpg'); ?>" alt="<?= $event['description']; ?>">
                                    </div>
                                    <div class="card-caption text-center">
                                        <h3 class="card-title"><?= $event['description']; ?></h3>
                                        <div class="form-check">
                                            <input class="form-check-input" name="event_category" id="rbo_<?= $event_name; ?>" type="radio" value="<?= $event_name; ?>" data-text="<?= $event['description']; ?>">
                                        </div>
                                    </div>
                                </label>
                            </div>
                            <?php endif;?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 d-flex justify-content-between">
                <div class="sub-container">
                    <a class="btn btn-custom bg-green text-navy">Reset</a>
                </div>
                <div class="sub-container">
                    <button class="btn btn-custom bg-navy text-yellow" type="submit">Next</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>