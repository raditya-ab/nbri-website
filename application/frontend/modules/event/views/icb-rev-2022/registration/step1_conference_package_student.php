<?php if($this->input->post('nationality') == 'international'):?>
    <div class="price-card bg-green p-5 text-center mb-3 package-option international-option student-option">
        <div class="card-title-container mb-5">
            <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
        </div>
        <div class="price-container mb-5">
            <span class="normal-price font-size-tiny"><del>US$ 300</del></span>
            <br />
            <span class="discount-price font-size-biggest font-bold">US$ 250<sup>*</sup></span>
        </div>
        <div class="price-description">
            <small>*before June 14th, 2021</small>
            <label for="presenter_student-international"
                class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
            <hr>
            <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                <li>Participate all conference programs</li>
                <li>Oral presentation</li>
                <li>International certificate</li>
                <li>International publication</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="conference-package" type="radio"
                        id="presenter_student-international" value="presenter_student-international"
                        data-text-value="Presenter (Student)" data-currency_price="$" data-normal_price="300"
                        data-early_bird_price="250">
                </label>
            </div>
        </div>
    </div>
    <div class="price-card bg-green p-5 text-center mb-3 package-option international-option student-option">
        <div class="card-title-container mb-5">
            <h4 class="font-size-big font-weight-medium">Participant<br />(Student)</h4>
        </div>
        <div class="price-container mb-5">
            <br />
            <span class="discount-price font-size-biggest font-bold">US$ 100</span>
        </div>
        <div class="price-description">
            <small>&nbsp;</small>
            <label for="participant_student-international"
                class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
            <hr>
            <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                <li>Attend all conference programs</li>
                <li>International certificate</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="conference-package" type="radio"
                        id="participant_student-international" value="participant_student-international"
                        data-text-value="Participant (Student)" data-currency_price="$" data-normal_price="100"
                        data-early_bird_price="100">
                </label>
            </div>
        </div>
    </div>
<?php else if($this->input->post('nationality') == 'indonesian'):?>
    <div class="price-card bg-green p-5 text-center mb-3 package-option indonesian-option student-option">
        <div class="card-title-container mb-5">
            <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
        </div>
        <div class="price-container mb-5">
            <span class="normal-price font-size-tiny"><del>Rp 2.500.000</del></span>
            <br>
            <span class="discount-price font-size-biggest font-bold">Rp 2.000.000<sup>*</sup></span>
        </div>
        <div class="price-description">
            <small>*before June 14th, 2021</small>
            <label for="presenter_student-indonesian"
                class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
            <hr>
            <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                <li>Participate all conference programs</li>
                <li>Oral presentation</li>
                <li>International certificate</li>
                <li>International publication</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="conference-package" type="radio" id="presenter_student-indonesian"
                        value="presenter_student-indonesian" data-text-value="Presenter (Student)" data-currency_price="Rp"
                        data-normal_price="2500000" data-early_bird_price="2000000">
                </label>
            </div>
        </div>
    </div>
    <div class="price-card bg-green p-5 text-center mb-3 package-option indonesian-option student-option">
        <div class="card-title-container mb-5">
            <h4 class="font-size-big font-weight-medium">Participant<br />(Student)</h4>
        </div>
        <div class="price-container mb-5">
            <br>
            <span class="discount-price font-size-biggest font-bold">Rp 500.000</span>
        </div>
        <div class="price-description">
            <small>&nbsp;</small>
            <label for="participant_student-indonesian"
                class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
            <hr>
            <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                <li>Attend all conference programs</li>
                <li>International certificate</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="conference-package" type="radio"
                        id="participant_student-indonesian" value="participant_student-indonesian"
                        data-text-value="Participant (Student)" data-currency_price="Rp" data-normal_price="500000"
                        data-early_bird_price="500000">
                </label>
            </div>
        </div>
    </div>
<?php else:?>
    <div class="price-card bg-green p-5 text-center mb-3 package-option international-option student-option">
        <div class="card-title-container mb-5">
            <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
        </div>
        <div class="price-container mb-5">
            <span class="normal-price font-size-tiny"><del>US$ 300</del></span>
            <br />
            <span class="discount-price font-size-biggest font-bold">US$ 250<sup>*</sup></span>
        </div>
        <div class="price-description">
            <small>*before June 14th, 2021</small>
            <label for="presenter_student-international"
                class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
            <hr>
            <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                <li>Participate all conference programs</li>
                <li>Oral presentation</li>
                <li>International certificate</li>
                <li>International publication</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="conference-package" type="radio"
                        id="presenter_student-international" value="presenter_student-international"
                        data-text-value="Presenter (Student)" data-currency_price="$" data-normal_price="300"
                        data-early_bird_price="250">
                </label>
            </div>
        </div>
    </div>
    <div class="price-card bg-green p-5 text-center mb-3 package-option international-option student-option">
        <div class="card-title-container mb-5">
            <h4 class="font-size-big font-weight-medium">Participant<br />(Student)</h4>
        </div>
        <div class="price-container mb-5">
            <br />
            <span class="discount-price font-size-biggest font-bold">US$ 100</span>
        </div>
        <div class="price-description">
            <small>&nbsp;</small>
            <label for="participant_student-international"
                class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
            <hr>
            <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                <li>Attend all conference programs</li>
                <li>International certificate</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="conference-package" type="radio"
                        id="participant_student-international" value="participant_student-international"
                        data-text-value="Participant (Student)" data-currency_price="$" data-normal_price="100"
                        data-early_bird_price="100">
                </label>
            </div>
        </div>
    </div>
    <div class="price-card bg-green p-5 text-center mb-3 package-option indonesian-option student-option">
        <div class="card-title-container mb-5">
            <h4 class="font-size-big font-weight-medium">Presenter<br>(Student)</h4>
        </div>
        <div class="price-container mb-5">
            <span class="normal-price font-size-tiny"><del>Rp 2.500.000</del></span>
            <br>
            <span class="discount-price font-size-biggest font-bold">Rp 2.000.000<sup>*</sup></span>
        </div>
        <div class="price-description">
            <small>*before June 14th, 2021</small>
            <label for="presenter_student-indonesian"
                class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
            <hr>
            <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                <li>Participate all conference programs</li>
                <li>Oral presentation</li>
                <li>International certificate</li>
                <li>International publication</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="conference-package" type="radio" id="presenter_student-indonesian"
                        value="presenter_student-indonesian" data-text-value="Presenter (Student)" data-currency_price="Rp"
                        data-normal_price="2500000" data-early_bird_price="2000000">
                </label>
            </div>
        </div>
    </div>
    <div class="price-card bg-green p-5 text-center mb-3 package-option indonesian-option student-option">
        <div class="card-title-container mb-5">
            <h4 class="font-size-big font-weight-medium">Participant<br />(Student)</h4>
        </div>
        <div class="price-container mb-5">
            <br>
            <span class="discount-price font-size-biggest font-bold">Rp 500.000</span>
        </div>
        <div class="price-description">
            <small>&nbsp;</small>
            <label for="participant_student-indonesian"
                class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
            <hr>
            <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                <li>Attend all conference programs</li>
                <li>International certificate</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" name="conference-package" type="radio"
                        id="participant_student-indonesian" value="participant_student-indonesian"
                        data-text-value="Participant (Student)" data-currency_price="Rp" data-normal_price="500000"
                        data-early_bird_price="500000">
                </label>
            </div>
        </div>
    </div>
<?php endif;?>