<?php $nationality = array_keys($event_details[$this->uri->segment(2)][$this->session->userdata('registration_data')['event_category']]['package_detail']); ?>
<?php $registration_data = $this->session->userdata('registration_data'); ?>
<style>
    .control-label {
        font-size: 18px;
    }

    .btn-custom.bg-navy:hover {
        color: #f7af00
    }

    .hover-underline:hover {
        text-decoration: underline;
    }
</style>
<section id="registration-section" class="page-section py-80">
    <div class="container">

        <div class="row mb-5">
            <div class="col-md-12">
                <?php $queryParams = strlen($this->input->server('QUERY_STRING')) > 0 ? '?' . $this->input->server('QUERY_STRING') : ''; ?>
                <ul class="nav justify-content-center flex-nowrap">
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 1) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 1) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step1'); ?>">Choose event</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 2) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 2) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step2' . $queryParams); ?>">Choose event package</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 3) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 3) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step3' . $queryParams); ?>">Fill the details</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 4) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 4) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step4' . $queryParams); ?>">Checkout &amp; Payment</a>
                    </li>
                </ul>
            </div>
            <?php if (array_key_exists('error', $_SESSION)) : ?>
                <div class="col-md-12 mt-5">
                    <div class="alert alert-danger p-5 alert-dismissible fade show font-size-medium" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong>Error!</strong> <br><?php print_r($_SESSION['error']); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php echo form_open(site_url('event/' . $this->uri->segment(2) . '/registration/process_step'), [
            'id' => 'form-registration-event',
            'class' => 'form',
            'method' => 'post'
        ], [
            'current_step' => $this->session->userdata('current_step'),
            'event_category_name' => $this->input->get('event', true),
            'occupation' => 'regular',
        ]);
        ?>
        <div class="row mb-5">
            <div class="col-md-12">
                <div id="icb-rev_2022_form_field" class="form_field_group">
                    <div class="form-group">
                        <label class="control-label required">Select nationality</label>
                        <select name="nationality" id="nationality" class="form-control">
                            <option value="">Select nationality</option>
                            <?php foreach ($nationality as $nation) : ?>
                                <option value="<?= $nation; ?>"><?= ucfirst($nation); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label required">Select occupation</label>
                        <select name="occupation" id="occupation" class="form-control" readonly>
                            <option value="student">Student</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label required">Select Event Package</label>
                        <div class="grid-card-wrapper grid-4 align-items-stretch justify-content-start mb-5" id="package_list">
                            <?php foreach ($nationality as $nation) : ?>
                                <?php foreach ($event_details[$this->uri->segment(2)][$this->session->userdata('registration_data')['event_category']]['package_detail'][$nation] as $occupation => $detail) : ?>
                                    <?php foreach ($detail as $type => $package_detail) : ?>
                                        <?php $currency_symbol = $package_detail['price']['normal']['currency'] == 'usd' ? 'US$' : 'Rp'; ?>
                                        <div class="price-card bg-green p-5 text-center mb-3 mr-3 package-option <?= $type; ?>-option <?= $nation; ?>-option <?= $occupation; ?>-option">
                                            <div class="card-title-container mb-5">
                                                <h4 class="font-size-big font-weight-medium"><?= ucwords(str_replace('_', ' ', $type)); ?><br>(<?= ucwords($occupation); ?>)</h4>
                                            </div>
                                            <?php if (time() > strtotime($package_detail['price']['early_bird']['expired_time']) || $package_detail['price']['early_bird']['amount'] == $package_detail['price']['normal']['amount']) : ?>
                                                <div class="price-container mb-5">
                                                    <br>
                                                    <span class="discount-price font-size-biggest font-bold"><?= $currency_symbol . ' ' . number_format($package_detail['price']['normal']['amount']); ?></span>
                                                </div>
                                            <?php else : ?>
                                                <div class="price-container mb-5">
                                                    <span class="normal-price font-size-tiny"><del><?= $currency_symbol . ' ' . number_format($package_detail['price']['normal']['amount']); ?></del></span>
                                                    <br />
                                                    <span class="discount-price font-size-biggest font-bold"><?= $currency_symbol . ' ' . number_format($package_detail['price']['early_bird']['amount']); ?><sup>*</sup></span>
                                                </div>
                                            <?php endif; ?>
                                            <div class="price-description">
                                                <small>
                                                    <?php if (time() > strtotime($package_detail['price']['early_bird']['expired_time'])) : ?>
                                                        &nbsp;
                                                    <?php else : ?>
                                                        *before <?= date('d F, Y', strtotime($package_detail['price']['early_bird']['expired_time'])); ?>
                                                    <?php endif; ?>
                                                </small>
                                                <label for="<?= $type . '_' . $occupation . '-' . $nation; ?>" class="btn btn-default bg-navy text-yellow btn-block no-radius font-size-small btn-package mt-3">Select</label>
                                                <hr>
                                                <ul class="m-0 p-0 font-size-small`" style="list-style: none">
                                                    <?php foreach ($package_detail['benefit'] as $benefit) : ?>
                                                        <li>- <?= $benefit; ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input <?= $type; ?>" data-class="<?= $type; ?>" name="event_package" type="radio" id="<?= $type . '_' . $occupation . '-' . $nation; ?>" value="<?= $type . ' ' . $occupation . ' ' . $nation; ?>" data-type="<?= $type; ?>" data-text-value="<?= ucwords($type) . ' (' . ucwords($occupation) . ')'; ?>)" data-currency_price="<?= $currency_symbol; ?>" data-normal_price="<?= $package_detail['price']['normal']['amount']; ?>" data-early_bird_price="<?= $package_detail['price']['early_bird']['amount']; ?>">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="form-group required-field nbri-yic_2022_field" style="display: none">
                        <label for="symposia" class="control-label required">Symposia <sup><a class="text-navy" href="#" data-toggle="modal" data-target="#modelId" title="Helpn  "><i class="fas fa-question-circle"></i></a></sup></label>
                        <select class="form-control no-radius nbri-yic_2022_competition" name="symposia" id="Paper_Competition_symposia" style="display: none" disabled>
                            <option value="">Select option</option>
                            <?php foreach ($event_details[$this->uri->segment(2)]['nbri-yic_2022']['event_detail']['scopes']['Paper_Competition'] as $scope) : ?>
                                <option value="<?= $scope; ?>"><?= $scope; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select class="form-control no-radius nbri-yic_2022_competition" name="symposia" id="Microblog_Competition_symposia" style="display: none" disabled>
                            <option value="">Select option</option>
                            <?php foreach ($event_details[$this->uri->segment(2)]['nbri-yic_2022']['event_detail']['scopes']['Microblog_Competition'] as $scope) : ?>
                                <option value="<?= $scope; ?>"><?= $scope; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group required-field">
                        <label for="abstract-title" class="control-label required">Abstract Title</label>
                        <input type="text" name="abstract_title" id="abstract_title" class="form-control" disabled>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 d-flex justify-content-between">
                <div class="sub-container">
                    <a class="btn btn-custom bg-green text-navy">Reset</a>
                </div>
                <div class="sub-container">
                    <button class="btn btn-custom bg-navy text-yellow" type="submit">Next</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>