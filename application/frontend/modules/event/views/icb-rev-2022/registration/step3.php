<style>
    .control-label {
        font-size: 18px;
    }
</style>
<?php
$is_loggedin = $this->aauth->is_loggedin();

$salutation = $is_loggedin ? $this->aauth->get_user_var('salutation') : null;
$first_name = $is_loggedin ? $this->aauth->get_user_var('first_name') : null;
$last_name = $is_loggedin ? $this->aauth->get_user_var('last_name') : null;
$suffix = $is_loggedin ? $this->aauth->get_user_var('suffix') : null;
$email = $is_loggedin ? $this->aauth->get_user()->email : null;
$affiliation = $is_loggedin ? $this->aauth->get_user_var('affiliation') : null;
$address = $is_loggedin ? $this->aauth->get_user_var('address') : null;
$phone = $is_loggedin ? $this->aauth->get_user_var('phone') : null;
?>
<section id="registration-section" class="page-section py-80">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <?php $queryParams = strlen($this->input->server('QUERY_STRING')) > 0 ? '?' . $this->input->server('QUERY_STRING') : ''; ?>
                <ul class="nav justify-content-center flex-nowrap">
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 1) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 1) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step1'); ?>">Choose event</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 2) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 2) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step2' . $queryParams); ?>">Choose event package</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 3) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 3) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step3' . $queryParams); ?>">Fill the details</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 4) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 4) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step4' . $queryParams); ?>">Checkout &amp; Payment</a>
                    </li>
                </ul>
            </div>
            <?php if (array_key_exists('error', $_SESSION)) : ?>
                <div class="col-md-12 mt-5">
                    <div class="alert alert-danger p-5 alert-dismissible fade show font-size-medium" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <strong>Error!</strong> <br><?php echo $_SESSION['error']['text']; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php echo form_open(site_url('event/' . $this->uri->segment(2) . '/registration/process_step'), [
            'id' => 'form-registration-event',
            'class' => 'form',
            'method' => 'post'
        ], [
            'current_step' => $this->session->userdata('current_step'),
            'event_category_name' => $this->input->get('event', true),
        ]);
        ?>
        <div class="row mt-5">
            <div class="col-md-12">

                <div class="form-group">
                    <label for="salutation" class="control-label required">Salutation</label>
                    <?= form_dropdown(
                        'salutation',
                        [
                            '' => 'Select Option',
                            'Mr.' => 'Mr.',
                            'Mrs.' => 'Mrs.',
                            'Ms.' => 'Ms.',
                            'Dr.' => 'Dr.',
                            'Prof.' => 'Prof.',
                            'Rev.' => 'Rev.',
                            'Other' => 'Other'
                        ],
                        $salutation,
                        [
                            'class' => 'form-control',
                            'id' => 'prefix',
                            'required' => 'required'
                        ]
                    ); ?>
                </div>
                <div class="form-group" id="prefix_other" style="display: none">
                    <label for="salutation" class="control-label required">Specify your Salutation</label>
                    <input type="text" name="salutation" class="form-control" disabled>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="salutation" class="control-label required">Firstname</label>
                        <?= form_input('first_name', $first_name, [
                            'class' => 'form-control',
                            'id' => 'first_name',
                            'required' => 'required'
                        ]); ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="salutation" class="control-label required">Lastname</label>
                        <?= form_input('last_name', $last_name, [
                            'class' => 'form-control',
                            'id' => 'last_name',
                            'required' => 'required'
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="salutation" class="control-label required">Suffix</label>
                    <?= form_input('suffix', $suffix, [
                        'class' => 'form-control',
                        'id' => 'suffix',
                        'required' => 'required'
                    ]); ?>
                </div>
                <div class="form-group">
                    <label for="salutation" class="control-label required">Email</label>
                    <?= form_input('email', $email, [
                        'class' => 'form-control',
                        'id' => 'email',
                        'required' => 'required'
                    ]); ?>
                </div>
                <?php if ($this->session->userdata('event_category') == 'isrus_2022') : ?>
                    <div class="form-group">
                        <label for="salutation" class="control-label required">Affiliation</label>
                        <?= form_dropdown(
                            'affiliation',
                            [
                                '' => 'Select Option',
                                'Research Centre' => 'Research Center',
                                'University' => 'University',
                                'Industry' => 'Industry',
                            ],
                            $affiliation,
                            [
                                'class' => 'form-control',
                                'id' => 'affiliation',
                                'required' => 'required'
                            ]
                        ); ?>
                    </div>
                <?php else : ?>
                    <div class="form-group">
                        <label for="affiliation" class="control-label required">Affiliation / Institution</label>
                        <?= form_input('affiliation', $affiliation, [
                            'class' => 'form-control',
                            'id' => 'affiliation',
                            'required' => 'required'
                        ]); ?>
                    </div>
                <?php endif; ?>
                <div class="form-group">
                    <label for="salutation" class="control-label required">Address</label>
                    <?= form_textarea('address', $address, [
                        'class' => 'form-control',
                        'id' => 'address',
                        'required' => 'required',
                        'cols' => '30',
                        'rows' => '10'
                    ]); ?>
                </div>
                <div class="form-group">
                    <label for="salutation" class="control-label required">Phone No.</label>
                    <?= form_input('phone', $phone, [
                        'class' => 'form-control',
                        'id' => 'phone',
                        'required' => 'required'
                    ]); ?>
                </div>
                <div class="form-group">
                    <label for="salutation" class="control-label required">How do you know about this event?</label>
                    <?= form_dropdown(
                        'referer',
                        [
                            '' => 'Select Option',
                            'NBRI Website' => 'NBRI Website',
                            'NBRI Social Media' => 'NBRI Social Media',
                            'Other' => 'Other'
                        ],
                        null,
                        [
                            'class' => 'form-control',
                            'id' => 'referer',
                            'required' => 'required'
                        ]
                    ); ?>
                </div>
                <div class="form-group" id="referer_other" style="display: none">
                    <label for="address" class="control-label required">Enter Referer</label>
                    <input type="text" name="referer_other" class="form-control" required disabled>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12 d-flex justify-content-between">
                <div class="sub-container">
                    <?= form_reset('btn-reset', 'Reset', [
                        'class' => 'btn btn-custom bg-green text-navy'
                    ]); ?>
                </div>
                <div class="sub-container">
                    <button class="btn btn-custom bg-navy text-yellow" type="submit">Next</button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</section>