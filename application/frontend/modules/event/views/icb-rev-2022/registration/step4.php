<style>
    .control-label {
        font-size: 18px;
    }

    .btn-custom.bg-navy:hover {
        color: #f7af00
    }

    .hover-underline:hover {
        text-decoration: underline;
    }
</style>
<section id="checkout-section" class="page-section py-80">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <?php $queryParams = strlen($this->input->server('QUERY_STRING')) > 0 ? '?' . $this->input->server('QUERY_STRING') : ''; ?>
                <ul class="nav justify-content-center flex-nowrap">
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 1) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 1) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step1'); ?>">Choose event</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 2) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 2) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step2' . $queryParams); ?>">Choose event package</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom  font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 3) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 3) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step3' . $queryParams); ?>">Fill the details</a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-custom font-size-medium mx-3 <?= ($this->session->userdata('current_step') == 4) ? "font-bold bg-yellow" : "bg-navy"; ?>" href="<?= ($this->session->userdata('current_step') == 4) ? "javascript:void(0);" : site_url('event/' . $this->uri->segment(2) . '/registration/step4' . $queryParams); ?>">Checkout &amp; Payment</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-5">
                <h2>Checkout</h2>
            </div>
            <div class="col-md-8 pr-5">
                <?php
                $registered_user_data = $this->session->userdata('registration_data');
                ?>
                <div class="form-group">
                    <label for="" class="font-size-small">Full name</label>
                    <p class="font-size-medium">
                        <?php echo $registered_user_data['salutation'] . ' ' . $registered_user_data['first_name'] . ' ' . $registered_user_data['last_name'] . ((isset($registered_user_data['suffix'])) ? ', ' . $registered_user_data['suffix'] : ''); ?>
                    </p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Email address</label>
                    <p class="font-size-medium"><?php echo $registered_user_data['email']; ?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Address</label>
                    <p class="font-size-medium"><?= $registered_user_data['address']; ?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Phone number</label>
                    <p class="font-size-medium"><?= $registered_user_data['phone']; ?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Institution / Affiliation</label>
                    <p class="font-size-medium"><?= $registered_user_data['affiliation']; ?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Event Category</label>
                    <p class="font-size-medium"><?= $registered_user_data['event_category_name']; ?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Package</label>
                    <p class="font-size-medium"><?= ucwords($registered_user_data['nationality']) . ' ' . ucwords($registered_user_data['package_type']) . ' (' . ucwords($registered_user_data['occupation']) . ')'; ?></p>
                </div>
                <div class="form-group">
                    <label for="" class="font-size-small">Nationality</label>
                    <p class="font-size-medium"><?= ucwords($registered_user_data['nationality']); ?></p>
                </div>
                <?php if ($registered_user_data['event_category'] = 'icb-rev_2022' && stripos($registered_user_data['package_type'], 'presenter') !== false) : ?>
                    <div class="form-group">
                        <label for="" class="font-size-small">Symposia</label>
                        <p class="font-size-medium"><?= $registered_user_data['symposia']; ?></p>
                    </div>
                    <div class="form-group">
                        <label for="" class="font-size-small">Abstract Title</label>
                        <p class="font-size-medium"><?= $registered_user_data['abstract_title']; ?></p>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-4 bg-navy p-5">
                <h2>Summary</h2>
                <hr class="border-green">
                <div class="row">
                    <div class="col-md-7">
                        <p class="font-size-medium font-thin">Subtotal</p>
                    </div>
                    <div class="col-md-5 text-right">
                        <p class="font-size-medium font-thin"><span id="currency"><?= $registered_user_data['event_price_currency'] == 'usd' ? 'US$' : 'Rp'; ?> </span><span id="subtotal"><?php echo number_format($registered_user_data['normal_event_price'], 0, '', '.'); ?></span></p>
                    </div>
                </div>
                <?php if ($registered_user_data['early_bird_promo']) : ?>
                    <div class="row">
                        <div class="col-md-7">
                            <p class="font-size-medium font-thin">Early-bird discount</p>
                        </div>
                        <div class="col-md-5 text-right">
                            <p class="font-size-medium font-thin">(<span id="currency"><?= $registered_user_data['event_price_currency'] == 'usd' ? 'US$' : 'Rp'; ?> </span><span id="subtotal"><?php echo number_format(($registered_user_data['normal_event_price'] - $registered_user_data['early_bird_event_price']), 0, '', '.'); ?>)</span></p>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-7">
                        <p class="font-size-medium font-thin">Additional discount</p>
                    </div>
                    <div class="col-md-5 text-right">
                        <p class="font-size-medium font-thin">
                            (<span id="currency"><?= $registered_user_data['event_price_currency'] == 'usd' ? 'US$' : 'Rp'; ?> </span>
                            <span id="subtotal">
                                <?php echo number_format($registered_user_data['additional_discount_amount'], 1, ',', '.'); ?>)</span>
                        </p>
                    </div>
                </div>
                <?php if (!$this->aauth->is_loggedin()) : ?>
                    <div class="row">
                        <div class="col-md-12 mt-3">
                            Not a member? <a href="<?= site_url('membership/login'); ?>" class="text-yellow text-medium hover-underline animate__animated animate__flash animate__infinite">Login/Register HERE and get 10% additional discount!</a>
                        </div>
                    </div>
                <?php endif; ?>
                <hr class="border-green">
                <div class="row">
                    <div class="col-md-7">
                        <p class="font-size-medium font-medium">Total</p>
                    </div>
                    <div class="col-md-5 text-right">
                        <p class="font-size-medium font-medium"><span id="currency"><?= $registered_user_data['event_price_currency'] == 'usd' ? 'US$' : 'Rp'; ?> </span><span id="subtotal"><?php echo number_format($registered_user_data['total_event_price'], 1, ',', '.'); ?></span></p>
                    </div>
                </div>
                <?php if ($registered_user_data['event_price_currency'] == 'usd') : ?>
                    <div class="row">

                        <div class="col-md-7">
                            <p class="font-size-medium font-medium">Total in IDR</p>
                            <a class="text-white" href="<?= $registered_user_data['conversion_rates']['license']; ?>" target="_blank">
                                <span class="font-size-small font-regular">US$ 1 = IDR
                                    <?php echo number_format($registered_user_data['conversion_rates']['idr_rates'], 2, ',', '.'); ?></span>
                                <sup class="text-yellow" title="rates from openexchangerates.org (per <?= date('F j, Y H:i:s', $registered_user_data['conversion_rates']['timestamp']); ?> UTC)"><i class="fas fa-question-circle"></i></sup>

                            </a>
                        </div>
                        <div class="col-md-5 text-right">
                            <p class="font-size-medium font-medium"><span id="currency">Rp </span><span id="subtotal"><?php echo number_format($registered_user_data['conversion_rates']['converted_price'], 0, '', '.'); ?></span></p>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="row mt-5">
                    <div class="col-md-12">
                        <button class="btn btn-custom btn-block bg-yellow text-navy btn-checkout" type="button">Checkout</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src='<?php echo MIDTRANS_SNAP_JS_URL; ?>' data-client-key='<?php echo MIDTRANS_CLIENT_KEY; ?>>'></script>