<style>
	.card .card-image {
		background-color: #FFF;
	}
</style>
<section id="speakers-section" class="page-section py-80">
	<div class="container">
		<?php foreach ($event_details[$this->uri->segment(2)]['icb-rev_2022']['speakers'] as $speaker) : ?>
			<?php if (!$speaker['mentioned_in_speakers_page']) continue;?>
			<div class="row">
				<div class="col-md-12">
					<h2 class="section-title text-navy"><?=ucwords($speaker['title']);?></h2>
				</div>
				<div class="col-md-12 grid-card-wrapper grid-4 align-items-stretch justify-content-center">
					<?php foreach ($speaker['content'] as $i => $person) : ?>
						<div class="card mx-2">
							<a href="javascript:void(0)" data-target="#<?= $person['slug'] . '-' . $i; ?>" data-toggle="modal">
								<div class="card-image" style="background-image: url('<?= $person['pic']; ?>'); background-size: cover; background-position: center; min-height: 242px; width: 100%;">

								</div>
								<div class="card-caption">
									<h3 class="card-title"><?= $person['name']; ?></h3>
									<p class="card-description"><?= $person['subtitle']; ?></p>
								</div>
							</a>
						</div>
						<!-- #<?= $person['slug']; ?> modal -->
						<div class="modal modal-member fade" id="<?= $person['slug'] . '-' . $i; ?>" tabindex="-1" role="dialog" aria-labelledby="<?= $person['slug']; ?>" aria-hidden="true">
							<div class="modal-dialog modal-full modal-dialog-centered" role="document">
								<div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
									<div class="modal-body p-0">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												<img src="<?= base_url('assets/images/clear_24px.svg'); ?>" alt="">
											</span>
										</button>
										<div class="row">
											<div class="col-md-4">
												<div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?= $person['pic']; ?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
												</div>
												<h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
													<?= $person['name']; ?>
												</h3>
												<p class="subtitle"><?= $person['subtitle']; ?></p>
											</div>
											<div class="col-md-8">
												<p class="description"><?= $person['description']; ?></p>
											</div>
										</div>
									</div>
									<div class="modal-footer no-border p-0">
										<button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>