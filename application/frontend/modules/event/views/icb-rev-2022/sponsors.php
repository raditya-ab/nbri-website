<section id="abstract-section" class="page-section py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-navy">
							Coming soon
<!--                <h2 class="section-title text-yellow">Platinum Sponsor</h2>-->
                <!--<div class="my-5 text-center">
                    <img src="<?/*=base_url('assets/images/footer-assets/telkom.png');*/?>" alt="Telkom Sponsor NBRI ICB-REV 2021" style="width:80%">
                </div>
                <p class="font-size-medium">
                    PT Telkom Indonesia (Persero) Tbk (Telkom) is a state-owned information and communications
                    technology enterprise and telecommunications network in Indonesia. The Government of Indonesia is
                    the majority shareholder with 52.09 percent shares while the remaining 47.91 percent shares belong
                    to public shareholders. Telkom’s shares are traded on the Indonesian Stock Exchange (IDX) where it
                    is listed as “TLKM” and on the New York Stock Exchange (NYSE), which lists it as “TLK”.
                </p>
                <p class="font-size-medium">
                    As it transforms to become a digital telecommunication company, TelkomGroup implements a
                    customer-oriented business and company operational strategy. The transformation aims to trim down
                    TelkomGroup’s organization to be leaner and more agile in adapting the fast-changing nature of
                    telecommunications industry. The new organization is expected to be able to improve efficiency and
                    be more effective in producing a quality customer experience.
                </p>
                <p class="font-size-medium">
                    TelkomGroup’s activities grow and change in accordance to the development of new technology,
                    information and digitalization, but still within the corridor of telecommunications and information
                    technology. This is evident in the newly developed business lines, which complements the company’s
                    existing legacy business.
                </p>
                <p class="font-size-medium">
                    Telkom now categorize its portfolio into 3 Digital Bussiness Domain: 
                    <ol class="font-size-medium">
                        <li>
                            <b>Digital Connectivity</b>: Fiber to the x (FTTx), 5G, Software Defined Networking (SDN)/
                            Network Function Virtualization (NFV)/ Satellite
                        </li>
                        <li>
                            <b>Digital Platform</b>: Data Center, Cloud, Internet of Things (IoT), Big Data/ Artificial
                            Intelligence (AI), Cybersecurity
                        </li>
                        <li>
                            <b>Digital Services</b>: Enterprise, Consumer
                        </li>
                    </ol>
                </p>
                
                <p class="font-size-medium">
                    For responding the industry shift to Digital, for supporting National Digitization and for
                    internalizing the transformation agenda Telkom has redefined its Purpose, Vision, and Missions.
                </p>
                <p class="font-size-medium">
                    <b>Purpose</b>: To build a more prosperous and competitive nation as well as deliver the best value
                    to our stakeholders. <br>
                    <b>Vision</b>: To be the most preferred digital telco to empower the society. <br>
                    <b>Mission</b>:
                    <ol class="font-size-medium">
                        <li>Advance rapid buildout of sustainable intelligent digital infrastructure and platforms tat
                            is affordable and accessible to all.</li>
                        <li>Nature best-in-class digital talent that helps develop nation's digital capabillities and
                            increase digital adoption.</li>
                        <li>Orchestrate digital ecosystem to deliver superior customer experience.</li>
                    </ol>
                </p>
                <p class="font-size-medium mt-5">
                    Visit their website: <a class="text-yellow" href="https://www.telkom.co.id/sites"><b><i>https://www.telkom.co.id/sites</i></b></a>
                </p>-->
            </div>
        </div>
    </div>
</section>