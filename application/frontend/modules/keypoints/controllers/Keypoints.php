<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keypoints extends Public_Controller {
    public function __construct()
    {
        parent::__construct();
    }

    public function slider()
    {
        $this->load->model('slider_m');
        $data['slider'] = array(
            'slides' => $this->slider_m->get_slider()
        );

        $this->load->view('slider', $data);
    }

    public function news_slider()
    {
        $this->load->model('news/news_m', 'news_m');

        $data['news_slider'] = $this->news_m->get_news();

        $this->load->view('news_slider', $data, FALSE);
    }

    public function member_slider()
    {
        $this->load->module('member');

        $this->member->show_member_slider(FALSE);
    }

    public function sidebar_result()
    {
        $data = [];

        $this->load->model('restaurant_model2','restaurant');
        $this->load->library('array_operation');

        if($this->input->post('markers'))
        {
            // $markers = $this->input->post('markers');
            // $latitude = $this->input->post('latitude');
            // $longitude = $this->input->post('longitude');

            // for($i = 0; $i < count($markers); $i++)
            //     $data[] = $this->restaurant->get_restaurant_around($markers[$i], $latitude, $longitude, FALSE);

            // $data['data'] = $this->array_operation->object_orderby($data, 'distance', SORT_ASC, 'rating', SORT_DESC);

            $this->load->view('sidebar_results', $this->input->post());
        }
    }

}

/* End of file Keypoints.php */
/* Location: ./application/modules/keypoints/controllers/Keypoints.php */