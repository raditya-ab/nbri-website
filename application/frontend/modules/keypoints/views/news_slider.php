<div class="section full-width sections_style_0 ">
    <div class="section_wrapper clearfix">
        <div class="items_group clearfix">
            <!-- One full width row-->
            <div class="column one column_slider_plugin ">
                <!-- Revolution slider area-->
                <div class="mfn-main-slider">
                    <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                    <link href="http://fonts.googleapis.com/css?family=Roboto:900,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                    <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#fff;padding:0px;margin-top:0px;margin-bottom:0px;">
                        <div id="rev_slider_4_1" class="rev_slider fullwidthabanner" data-version="5.0.6">
                            <ul>
                                <?php foreach($news_slider as $i => $news): ?>
                                    <?php if($i>=5) break; ?>
                                    <li data-index="rs-<?=$i;?>" data-transition="parallaxvertical" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<?php echo base_url("assets/images/articles/".$news['img']); ?>" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="<?php echo $news['title']; ?>" data-param1="<?php echo $news['category_name']; ?>" data-description="<?php echo strip_tags($news['content']); ?>">
                                        <img onerror="this.src='https://via.placeholder.com/100x<?=$i+=50;?>'" src="<?php echo base_url('assets/images/articles/'.$news['img']) ?>" alt="" width="1920" height="1280" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                        <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" id="slide-2279-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on" style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 1.00);"></div>
                                        <div class="tp-caption Newspaper-Title tp-resizeme rs-parallaxlevel-0" id="slide-2279-layer-1" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['165','135','105','130']" data-fontsize="['50','50','50','30']" data-lineheight="['55','55','55','35']" data-fontweight="['700','400','400','400']" data-width="['600','600','600','420']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal; font-weight: 700;">
                                            <?php echo $news['title']; ?>
                                        </div>
                                        <div class="tp-caption Newspaper-Subtitle tp-resizeme rs-parallaxlevel-0" id="slide-2279-layer-2" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['140','110','80','100']" data-fontweight="['400','900','900','900']" data-color="['rgba(255, 255, 255, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);text-transform:uppercase;">
                                            <?php echo date('d F, Y', strtotime($news['created'])); ?>
                                        </div>
                                        <a class="tp-caption Newspaper-Button rev-btn rs-parallaxlevel-0" href="content/space/item-1.html" target="_self" id="slide-2279-layer-5" data-x="['left','left','left','left']" data-hoffset="['53','53','53','30']" data-y="['top','top','top','top']" data-voffset="['361','331','301','245']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);cursor:pointer;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="on" data-responsive="off" style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Read more </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <div class="tp-static-layers"></div>
                            <div class="tp-bannertimer tp-bottom flv_viz_hid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>