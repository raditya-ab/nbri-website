<section id="homepage-slider" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php for ($i = 0; $i < count($slider['slides']); $i++) : ?>
            <li data-target="#homepage-slider-<?= $i; ?>" data-slide-to="<?php echo $i; ?>" class="<?php echo $i == 0 ? 'active' : ''; ?>"></li>
        <?php endfor; ?>
    </ol>
    <div class="carousel-inner">
        <?php foreach ($slider['slides'] as $i => $slide) : ?>
            <?php
            $url = $slide['url'] ? $slide['url'] : '#';
            ?>
            <div id="homepage-slider-<?= $i; ?>" class="carousel-item <?= ($i == 0) ? 'active' : ''; ?>" style="background-image: url(assets/images/slider/<?= $slide['filename']; ?>)" onclick="window.location.href='<?= $url; ?>'">
                <?php if ($slide['title'] || $slide['subtitle']) : ?>
                    <div class="carousel-caption d-none d-md-block" style="background-color: rgba(0,0,0,0.4)">
                        <?php if (!empty($slide['url']) && $slide['url'] !== NULL) : ?>
                            <a href="<?php echo $slide['url'] ?>" title="<?php echo @$slide['title'] ?>">
                                <h5><?php echo @$slide['title'] ?></h5>
                                <p><?php echo @$slide['subtitle'] ?></p>
                            </a>
                        <?php else : ?>
                            <h5><?php echo @$slide['title'] ?></h5>
                            <p><?php echo @$slide['subtitle'] ?></p>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <a class="carousel-control-prev" href="#homepage-slider" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#homepage-slider" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</section>