<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends Public_Controller {

    public function show_member_slider($as_variable = FALSE)
    {
        $this->load->model('member_m');

        $data['member_list'] = $this->member_m->get_member();

        $this->load->view('member_slider', $data, $as_variable);
    }

    public function show_member_section($as_variable = FALSE)
    {
        $this->load->model('member_m');
        $data['member_list'] = $this->member_m->get_member();
        $this->load->view('member_grid', $data, $as_variable);
    }

}

/* End of file Member.php */
/* Location: ./application/modules/member/controllers/Member.php */