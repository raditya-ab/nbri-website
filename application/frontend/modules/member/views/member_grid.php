<section id="member-section" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h3 class="section-title" style="margin-bottom: 65px">Partner of NBRI</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="icons-grid">
                    <?php foreach($member_list as $i => $member): ?>
                    <div class="icon-wrapper">
                        <img class="icon-image" src="<?php echo base_url('assets/images/member/'.$member['img']) ?>" onerror="this.src='https://via.placeholder.com/120x120'" alt="" >
                        <!-- <h5 class="research-title">NBRI One Map</h5> -->
                    </div>
                    <?php if($i%4 == 0 && $i > 0): ?> <div class="break" style="flex-basis: 100%;height: 65px"></div><?php   endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>