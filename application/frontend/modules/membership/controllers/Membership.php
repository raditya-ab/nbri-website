<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership extends Public_Controller{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('membership_m');
        $this->template->set_layout('default_template');
    }

    public function index($group = 4)
    {
        $this->data['member_list'] = $this->membership_m->get_members($group);
        switch($group){
            case 4: $page_title = 'Individual';
                break;
            case 5: $page_title = 'Institute';
                break;
            case 6: $page_title = 'Corporate';
                break;
            default: $page_title = 'All';
                break;
        }

        $this->template->set('page_title', $page_title);
        $this->template->build('index', $this->data);
    }

    function detail($hashed_username)
    {
        $user_data = $this->membership_m->get_hashed_username_id($hashed_username);

        if($user_data !== FALSE){
            $this->data['detail'] = $this->membership_m->get_detail($user_data['id'], $user_data['username']);
            $this->template->build('detail', $this->data);
        } else {
            show_404();
        }
    }

    function profile($hashed_username)
    {
        $user_data = $this->membership_m->get_hashed_username_id($hashed_username);

        if($user_data !== FALSE){
            $this->data['detail'] = $this->membership_m->get_detail($user_data['id'], $user_data['username']);
            $this->template->build('profile', $this->data);
        } else {
            show_404();
        }
    }

    function edit_profile($hashed_username)
    {
        if($hashed_username != sha1($_SESSION['username'])){
            show_404();
        }
        $user_data = $this->membership_m->get_hashed_username_id($hashed_username);
        if($user_data !== FALSE){
            $this->data['detail'] = $this->membership_m->get_detail($user_data['id'], $user_data['username']);
            $this->data['hashed_username'] = $hashed_username;
            $this->template->build('edit_profile', $this->data);
        } else {
            show_404();
        }
    }

    function save_profile($hashed_username)
    {
        if($hashed_username != sha1($_SESSION['username'])){
            show_404();
        }
        $this->load->library('form_validation');
        $document_filename = NULL;
        $message = NULL;

        $user_data = $this->membership_m->get_hashed_username_id($hashed_username);
       
        if($user_data == FALSE){
           show_404();
        }
        
        if($this->input->is_ajax_request()){
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('salutation', 'Salutation', 'trim|max_length[100]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('affiliation', 'Institution', 'trim|required|max_length[200]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|min_length[5]|addslashes');
            $this->form_validation->set_rules('research_focus', 'Research Focus', 'trim|required|max_length[200]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('expertise', 'Expertise', 'trim|required|max_length[200]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('website', 'Website', 'trim|max_length[200]|strip_tags|addslashes|htmlentities|xss_clean');
            $this->form_validation->set_rules('description', 'Short Bio', 'trim|min_length[5]|addslashes');
            
            if ($this->form_validation->run($this) == FALSE) {
                $output = array(
                    'status' => 0,
                    'message' => array(
                        'text' => validation_errors()
                    )
                );
            } else {
                $post = $this->input->post();
                $detail = $this->membership_m->get_detail($user_data['id'], $user_data['username']);

                $merged_data = array_merge($detail,$post);
                foreach($merged_data as $key => $data){
                    if(!is_array($merged_data)){
                        $temp = trim($merged_data[$key]);
                    } else {
                        $temp = $merged_data[$key];
                    }

                    if(empty($temp) || $temp == null || strlen($temp) == 0){
                        unset($merged_data[$key]);
                    }
                }
                
                if(!empty($_FILES['inputfile']['name'])){
                    $this->load->library('upload');
                    $upload_config = array(
                        'upload_path' => 'assets/images/',
                        'allowed_types' => 'gif|jpg|png|jpeg',
                        'max_size' => '3196',
                        'overwrite' => TRUE,
                        'remove_spaces' => TRUE,
                        'encrypt_name' => TRUE,
                        'file_ext_tolower' => TRUE
                    );

                    $this->upload->initialize($upload_config);

                    if ($this->upload->do_upload('inputfile')){
                        $document_filename = $this->upload->data()['file_name'];
                        $merged_data ['profile_pic'] = $document_filename;
                        $message .= 'Profile Picture uploaded';
                    } else {
                        $message .= 'Profile Picture not uploaded';
                    }
                }

                $output = $this->membership_m->update($user_data['id'],$merged_data);
                $output['message'] = array(
                    'text' => 'Profile updated! '.$message
                );
                
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($output));
        } else {
            show_404();
        }
    }

    function login()
    {
        if($this->aauth->is_loggedin())
            redirect(site_url(),'refresh');
        $this->template->build('login', $this->data);
    }

    function register()
    {
        if($this->aauth->is_loggedin())
            redirect(site_url(),'refresh');
        $this->template->build('register', $this->data);
    }

    function logout()
    {
        $this->aauth->logout();
        redirect(site_url(),'refresh');
    }

    function delete($user_id)
    {
        $this->aauth->delete_user($user_id);
    }

    // public function delete_empty_value($row, $key)
    // {
    //     $new_val = 
    // }

}