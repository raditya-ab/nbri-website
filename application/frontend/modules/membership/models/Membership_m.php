<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membership_m extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function get_members($group)
    {
        $this->db->select('u.id,u.username');
        $this->db->from('aauth_users u');
        $this->db->join('aauth_user_to_group ug', 'u.id = ug.user_id', 'left');
        $this->db->join('aauth_groups g', 'ug.group_id = g.id', 'left');
        $this->db->where('g.id', $group);

        $result = $this->db->get()->result_array();

        $member_list = array();
        foreach($result as $member){
            $member_list[] = array(
                'id' => $member['id'],
                'username' => $member['username'],
                'first_name' => $this->aauth->get_user_var('first_name',$member['id']),
                'last_name' => $this->aauth->get_user_var('last_name',$member['id']),
                'salutation' => $this->aauth->get_user_var('salutation',$member['id']),
                'affiliation' => $this->aauth->get_user_var('affiliation',$member['id']),
                'research_focus' => $this->aauth->get_user_var('research_focus',$member['id']),
                'expertise' => $this->aauth->get_user_var('expertise',$member['id'])
            );
        }

        return $member_list;


    }

    public function get_hashed_username_id($hashed_username){
        $this->db->select('id, username');
        $this->db->from('aauth_users');
        $this->db->where('sha1(username)', $hashed_username);

        $result =  $this->db->get()->row_array();

        if(!empty($result)){
            return $result;
        }

        return FALSE;
    }

    public function get_detail($user_id, $username)
    {
        $user_detail = $this->aauth->get_user($user_id);
        
        $member_list = array(
            'id' => $user_id,
            'username' => $username,
            'email' => $user_detail->email,
            'first_name' => $this->aauth->get_user_var('first_name',$user_id),
            'last_name' => $this->aauth->get_user_var('last_name',$user_id),
            'profile_pic' => $this->aauth->get_user_var('profile_pic', $user_id),
            'salutation' => $this->aauth->get_user_var('salutation',$user_id),
            'affiliation' => $this->aauth->get_user_var('affiliation',$user_id),
            'research_focus' => $this->aauth->get_user_var('research_focus',$user_id),
            'expertise' => $this->aauth->get_user_var('expertise',$user_id),
            'website' => $this->aauth->get_user_var('website', $user_id),
            'address' => $this->aauth->get_user_var('address', $user_id),
            'description' => $this->aauth->get_user_var('description', $user_id),
            'publication' => json_decode($this->aauth->get_user_var('publication', $user_id)),
        );

        return $member_list;
    }

    public function update($user_id, $updated_data)
    {
        unset($updated_data['id']);
        unset($updated_data['username']);
        unset($updated_data['email']);

        $this->db->trans_strict(TRUE);
        $this->db->trans_begin();
        foreach($updated_data as $key => $val){
            $this->db->where('user_id', $user_id);
            $this->db->where('data_key', $key);
            $this->db->set('value', $val);
            $this->db->update('aauth_user_variables');
        }

        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $status = 0;
        } else {
            $this->db->trans_commit();
            $status = 1;
        }

        $output = array(
            'status' => $status
        );

        $this->db->trans_strict(FALSE);
        return $output;
    }


}

/* End of file Membership_m.php */
/* Location: ./application/frontend/modules/membership/models/Membership_m.php */