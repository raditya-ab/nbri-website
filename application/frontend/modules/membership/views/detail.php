<!-- <section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container">
                <h1 class="page-title font-color-navy"><?php echo $detail['salutation']." ".$detail['first_name']." ".$detail['last_name'];?></h1>
            </div>
        </div>
    </div>
</section> -->
<section class="content-section">
    <div class="container">

        <div class="row pt-5">
            <div class="col-md-12 bg-navy p-5">
                <div class="row">
                    <div class="col-md-3">
                    <?php
                        $profile_pic = 'https://via.placeholder.com/300x300';

                        // echo is_file('./assets/images/'.$detail['profile_pic']);
                        // echo './assets/images/'.$detail['profile_pic'];
                        // exit();
                        if($detail['profile_pic'] != null && !empty($detail['profile_pic'])){
                            if(file_exists('assets/images/'.$detail['profile_pic'])){
                                $profile_pic = base_url('assets/images/'.$detail['profile_pic']);
                            }
                        }
                    ?>
                        <div class="img-profile" style="background-image: url(<?php echo $profile_pic;?>)"></div>
                        <ul class="list-unstyled mt-5 text-white">
                            <?php if(!empty($detail['publication'])): ?>
                                <?php foreach($detail['publication'] as $publication): ?>
                                    <li class="pt-1 pb-1"><a class="btn no-radius bg-green btn-block font-color-navy" href="<?php echo $publication['link'];?>"><?php echo $publication['portal'] ?></a></li>
                                <?php endforeach ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="font-color-green"><?php echo $detail['salutation']." ".$detail['first_name']." ".$detail['last_name'];?></h4>
                                <div class="table-responsive">
                                    <table class="table table-hover font-color-green mt-5" style="border: 0 impor !important">
                                        <tbody>
                                            <tr>
                                                <td width="200" class="font-weight-bold">Name</td>
                                                <td><?php echo $detail['salutation']." ".$detail['first_name']." ".$detail['last_name'];?></td>
                                            </tr>
                                            <tr>
                                                <td width="200" class="font-weight-bold">Email</td>
                                                <td><?php echo $detail['email'];?></td>
                                            </tr>
                                            <tr>
                                                <td width="200" class="font-weight-bold">Institution</td>
                                                <td><?php echo (!empty($detail['affiliation'])) ? $detail['affiliation']:'-';?></td>
                                            </tr>
                                            <tr>
                                                <td width="200" class="font-weight-bold">Address</td>
                                                <td><?php echo (!empty($detail['institution'])) ? $detail['institution']:'-';?></td>
                                            </tr>
                                            <tr>
                                                <td width="200" class="font-weight-bold">Research Focus</td>
                                                <td><?php echo (!empty($detail['research_focus'])) ? $detail['research_focus']:'-';?></td>
                                            </tr>
                                            <tr>
                                                <td width="200" class="font-weight-bold">Expertise</td>
                                                <td><?php echo (!empty($detail['expertise'])) ? $detail['expertise']:'-';?></td>
                                            </tr>
                                            <tr>
                                                <td width="200" class="font-weight-bold">Website</td>
                                                <td><?php echo (!empty($detail['website'])) ? $detail['website']:'-';?></td>
                                            </tr>
                                            <tr>
                                                <td width="200" class="font-weight-bold">Profile</td>
                                                <td><?php echo (!empty($detail['description'])) ? $detail['description']:'-';?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo site_url('membership');?>" class="btn bg-yellow font-color-navy no-radius mt-5 float-right">Back to Member List</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>