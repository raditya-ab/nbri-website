<!-- <section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container">
                <h1 class="page-title font-color-navy"><?php echo $detail['salutation']." ".$detail['first_name']." ".$detail['last_name'];?></h1>
            </div>
        </div>
    </div>
</section> -->
<section class="content-section">
    <div class="container">

        <div class="row pt-5">
            <?php echo form_open_multipart('membership/save_profile/'.$hashed_username, array('method' => 'post', 'id'=>'form_edit_profile', 'data-identifier'=>$hashed_username));?>
                <div class="col-md-12 bg-navy p-5">
                    <div class="row">
                        <div class="col-md-3">
                        <?php
                        $profile_pic = 'https://via.placeholder.com/300x300';

                        // echo is_file('./assets/images/'.$detail['profile_pic']);
                        // echo './assets/images/'.$detail['profile_pic'];
                        // exit();
                        if($detail['profile_pic'] != null && !empty($detail['profile_pic'])){
                            if(file_exists('assets/images/'.$detail['profile_pic'])){
                                $profile_pic = base_url('assets/images/'.$detail['profile_pic']);
                            }
                        }
                    ?>
                        <div class="img-profile" id="img-profile" style="background-image: url(<?php echo $profile_pic;?>)"></div>
                            <center>
                                <input type="file" accept="image/*" name="inputfile" id="photo" onChange="readURL(this);" style="display: none">
                                <label for="photo" class="btn bg-green font-color-navy no-radius mt-3">Change Photo</label>
                            </center>
                            <ul class="list-unstyled mt-5 text-white">
                                <?php if(!empty($detail['publication'])): ?>
                                    <?php foreach($detail['publication'] as $publication): ?>
                                        <li class="pt-1 pb-1"><a class="btn no-radius bg-green btn-block font-color-navy" href="<?php echo $publication['link'];?>"><?php echo $publication['portal'] ?></a></li>
                                    <?php endforeach ?>
                                <?php else: ?>
                                        <li class="pt-1 pb-1">No published publication yet.</li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="font-color-green"><?php echo $detail['salutation']." ".$detail['first_name']." ".$detail['last_name'];?></h4>
                                    <div class="table-responsive">
                                        <table class="table table-hover font-color-green mt-5" style="border: 0 !important">
                                            <tbody>
                                                <tr>
                                                    <td width="200" class="font-weight-bold">First Name*</td>
                                                    <td>
                                                        <input type="text" class="form-control no-radius" name="first_name" value="<?php echo $detail['first_name'];?>" placeholder="Insert your first name" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200" class="font-weight-bold">Last Name</td>
                                                    <td>
                                                        <input type="text" class="form-control no-radius" name="last_name" value="<?php echo $detail['last_name'];?>" placeholder="Insert your last name with suffix" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200" class="font-weight-bold">Salutation</td>
                                                    <td>
                                                        <input type="text" class="form-control no-radius" name="salutation" value="<?php echo $detail['salutation'];?>" placeholder="Insert your salutation title (Mr.,Mrs.,Ms.,Prof.)">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200" class="font-weight-bold">Institution*</td>
                                                    <td>
                                                        <input type="text" class="form-control no-radius" name="affiliation" value="<?php echo (!empty($detail['affiliation'])) ? $detail['affiliation']:'-';?>" placeholder="Insert your institution" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200" class="font-weight-bold">Address*</td>
                                                    <td>
                                                        <textarea name="address" id="address"  rows="5" class="form-control no-radius" placeholder="Your address" required>
                                                        <?php
                                                            if(strlen(trim($detail['address'])) > 0){
                                                                echo $detail['address'];
                                                            }
                                                        ?>
                                                        </textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200" class="font-weight-bold">Research Focus*</td>
                                                    <td>
                                                        <input type="text" class="form-control no-radius" name="research_focus" value="<?php echo (!empty($detail['research_focus'])) ? $detail['research_focus']:'-';?>" placeholder="Your research focus" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200" class="font-weight-bold">Expertise*</td>
                                                    <td>
                                                        <input type="text" class="form-control no-radius" name="expertise" value="<?php echo (!empty($detail['expertise'])) ? $detail['expertise']:'-';?>" placeholder="Your expertise" required>


                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200" class="font-weight-bold">Website</td>
                                                    <td>
                                                        <input type="text" class="form-control no-radius" name="website" value="<?php echo (!empty($detail['website'])) ? $detail['website']:'';?>" placeholder="Your portofolio/publication website address">
                                                        <small><i>Insert your publication websites, separated with comma ",". eg: https://independent.academia.edu/JohnDoe</i></small>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="200" class="font-weight-bold">Short Bio</td>
                                                    <td>
                                                        <textarea name="description" id="description"  rows="5" class="form-control no-radius" placeholder="Put brief description about yourself">
                                                            <?php echo (trim($detail['description']) != NULL && !empty(trim($detail['description'])))? $detail['description'] : '';?>
                                                        </textarea>
                                                        <small><i>Max 800 character</i></small>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <span><i>fields with (*) are required.</i></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn bg-yellow font-color-navy no-radius mt-5 float-right">Save Profile Data</button>
                                    <a href="<?php echo site_url('membership');?>" class="btn bg-green font-color-navy no-radius mt-5 mr-3 float-right">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php echo form_close();?>
        </div>
    </div>
</section>