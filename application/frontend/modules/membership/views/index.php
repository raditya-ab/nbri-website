<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container">
                <h1 class="page-title font-color-navy">Our Members</h1>
            </div>
        </div>
    </div>
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="section-title font-bold text-center"><?=$page_title;?> NBRI Members</h3>
            </div>
        </div>
        <div class="row pt-5 pb-5" style="border-top: 1px solid #333;border-bottom: 1px solid #333">
            <div class="col-md-2 d-flex align-items-center">
                <span>Search Member</span>
            </div>
            <div class="col-md-8">
                <input type="text" class="form-control no-radius" style="height: 100%">
            </div>
            <div class="col-md-2 text-right">
                <button class="btn btn-w-165 no-radius bg-navy m-0 font-color-green btn-hover-yellow" style="padding: 10px 25px">Search Member</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="table-responsive">
                    <table class="table table-hover table-inverse" id="member_table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Institution</th>
                                <th>Research Focus</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($member_list as $no => $member):?>
                                <?php $random_string = sha1($member['username']); ?>
                            <tr>
                                <td><?=++$no;?></td>
                                <td><?=$member['salutation'].' '.$member['first_name'].' '.$member['last_name'];?></td>
                                <td><?=$member['affiliation'];?></td>
                                <td><?=$member['research_focus'];?></td>
                                <td align="right"><a href="<?php echo site_url('membership/detail/'.$random_string);?>" class="btn no-radius btn-w-165 bg-navy m-0 font-color-green btn-hover-yellow" style="padding: 10px 25px">View Detail</a></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>