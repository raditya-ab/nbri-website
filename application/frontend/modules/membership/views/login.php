<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container">
                <h1 class="page-title font-color-navy">Login Membership</h1>
            </div>
        </div>
    </div>
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php echo form_open(site_url('user/login/verify_login'), array('id' => 'form_login','method'=>'post')); ?>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label for="">Email</label>
                        <input type="email" name="email" class="form-control flat-corner" placeholder="Please put your registered email" required>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control flat-corner" placeholder="Password" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <div class="row">
                            <div class="col-md-5">
                                <button type="submit" class="btn bg-navy btn-block flat-corner"><b>Login</b></button>
                            </div>
                            <div class="col-md-7">
                                <a href="#" title="">Forgot Password</a>
                            </div>
                        </div>

                    </div>
                </div>
                <?php form_close(); ?>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label for="">Register</label>
                        <a href="<?=site_url('membership/register');?>" class="btn bg-green btn-block flat-corner"><b>I'm not a member. Register Now</b></a>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>