<style type="text/css" media="screen">
    .radio-button-big input[type="radio"] {
      opacity: 0;
      position: fixed;
      width: 0;
    }
    .radio-button-big label {
        display: flex;
        background-color: #ddd;
        height: 100%;
        width: 100%;
        justify-content: center;
        align-items: center;
        font-family: 'Sailec Medium';
        font-size: 28px;
        border-radius: 0;
    }
    .radio-button-big input[type="radio"]:checked + label {
        background-color:#bfb;
        border-color: #4c4;
    }
    .radio-button-big input[type="radio"]:focus + label {
        border: 2px dashed #444;
    }
    .radio-button-big label:hover {
      background-color: #dfd;
    }
</style>

<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(https://faraday.ac.uk/wp-content/uploads/2019/04/Alireza-Rastegarpanah-2-resized.jpg)">
                <h1 class="page-title">Become a Member</h1>
            </div>
        </div>
    </div>
</section>

<?php echo form_open(site_url('user/register'), array('id'=>'form_register','method'=>'post')); ?>
<section class="content-section">
    <div class="container">

        <div class="radio-button-big">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-title text-center font-bold">Membership Type*</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4" style="height: 228px;">
                    <input type="radio" id="individual_rbo" name="membership_type" value="4">
                    <label for="individual_rbo">Individual</label>
                </div>
                <div class="col-md-4" style="height: 228px;">
                    <input type="radio" id="institute_rbo" name="membership_type" value="5">
                    <label for="institute_rbo">Research Institute</label>
                </div>
                <div class="col-md-4" style="height: 228px;">
                    <input type="radio" id="corporate_rbo" name="membership_type" value="6">
                    <label for="corporate_rbo">Corporate</label>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-12">
                    <h3 class="section-title text-center font-bold">Nationality*</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="height: 228px;">
                    <input type="radio" id="domestic_rbo" name="domain" value="domestic">
                    <label for="domestic_rbo">Indonesia</label>
                </div>
                <div class="col-md-6" style="height: 228px;">
                    <input type="radio" id="international_rbo" name="domain" value="international">
                    <label for="international_rbo">International</label>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-12">
                    <h3 class="section-title text-center font-bold">Occupation*</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4" style="height: 228px;">
                    <input type="radio" id="student_rbo" name="membership_role" value="7">
                    <label for="student_rbo">Student</label>
                </div>
                <div class="col-md-4" style="height: 228px;">
                    <input type="radio" id="researcher_rbo" name="membership_role" value="8">
                    <label for="researcher_rbo">Researcher</label>
                </div>
                <div class="col-md-4" style="height: 228px;">
                    <input type="radio" id="private_rbo" name="membership_role" value="9">
                    <label for="private_rbo">Private</label>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-12">
                    <h3 class="section-title text-center font-bold">Membership Plan*</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="height: 228px;">
                    <input type="radio" id="basic_rbo" name="membership_plan" value="0">
                    <label for="basic_rbo">Basic Membership</label>
                </div>
                <div class="col-md-6" style="height: 228px;">
                    <input type="radio" id="full_rbo" name="membership_plan" value="1">
                    <label for="full_rbo">Full Membership</label>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="section-title text-center font-bold">Fill Your Information</h3>
            </div>
        </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="">Email*</label>
                    <input type="email" name="email" class="form-control flat-corner" placeholder="Email" required>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Username*</label>
                    <input type="text" name="username" class="form-control flat-corner" placeholder="Username" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="">Password*</label>
                    <input type="password" name="password" class="form-control flat-corner" placeholder="Password" required>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Confirm Password*</label>
                    <input type="password" name="password_confirmation" class="form-control flat-corner" placeholder="Confirm Password" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="">First Name*</label>
                    <input type="text" name="first_name" class="form-control flat-corner" placeholder="First Name" required>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Last Name*</label>
                    <input type="text" name="last_name" class="form-control flat-corner" placeholder="Last Name" required>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="">Salutation</label>
                    <input type="text" name="salutation" class="form-control flat-corner" placeholder="Prof./Dr./Mr./Mrs./Ms.">
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Affiliation</label>
                    <input type="text" name="affiliation" class="form-control flat-corner" placeholder="Your Institution (e.g &ldquo;BATAN&rdquo;)">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group">
                    <label for="">Research Focus Area*</label>
                    <input type="text" name="research_focus" class="form-control flat-corner" placeholder="Insert your research focus (separate with comma)" required>
                </div>
                <div class="col-md-6 form-group">
                    <label for="">Expertise</label>
                    <input type="text" name="expertise" class="form-control flat-corner" placeholder="Your Expertise">
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <label>Field with (*) is required</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 form-group">
                    <button type="submit" class="btn btn-block bg-navy flat-corner">Register</button>
                </div>
            </div>

    </div>
</section>
<?php echo form_close(); ?>
