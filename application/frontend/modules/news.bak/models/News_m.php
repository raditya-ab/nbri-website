<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News_m extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_news($category = NULL){
		$select = array(
			'a.id',
			'a.title',
			'a.img',
			'a.slug as news_slug',
            'a.content',
			'a.status',
            'a.created',
			'c.category_name',
			'c.slug as category_slug',
			'GROUP_CONCAT(t.tag_name) as tags'
		);

		if(!is_null($category) && !empty($category)){
			$this->db->where('a.category', $category);
		}

		$this->db->select($select);
		$this->db->from('articles a');
		$this->db->join('article_tags at', 'a.id = at.article_id', 'left');
		$this->db->join('categories c', 'a.category = c.id', 'left');
		$this->db->join('tags t', 'at.tag_id = t.id', 'left');
		$this->db->where('a.status', 1);
		$this->db->order_by('a.featured', 'desc');
		$this->db->order_by('a.created', 'desc');
		$this->db->group_by('a.id');
		return $this->db->get()->result_array();
	}

	public function get_detail($category_slug, $news_slug){
		$select = array(
			'a.id',
			'a.title',
			'a.img',
			'a.created',
			'c.category_name',
			'a.content',
		);

		$this->db->select($select);
		$this->db->from('articles a');
		$this->db->join('categories c', 'a.category = c.id', 'left');
		$this->db->where('a.slug', $news_slug);
		$this->db->where('c.slug', $category_slug);
		$this->db->where('a.status', 1);
		return $this->db->get()->row_array();
	}

	public function news_is_exist($category_slug, $news_slug){
		$this->db->where('a.slug', $news_slug);
		$this->db->where('c.slug', $category_slug);
		$this->db->where('a.status', 1);
		$this->db->from('articles a');
		$this->db->join('categories c', 'a.category = c.id', 'left');
		return ($this->db->count_all_results() > 0) ? TRUE : FALSE;
	}

	public function get_categories(){
		return $this->db->get('categories')->result_array();
	}

	public function get_category_id($category_name){
		return $this->db->where('category_name', $category_name)->limit(1)->get('categories')->row_array();
	}

}

/* End of file News_m.php */
/* Location: ./application/frontend/modules/news/models/News_m.php */