
<div class="section full-width" style="background-color:#C5011A">
    <div class="section-wrapper clearfix">
        <div class="items_group">
            <form action="" class="form form-horizontal">
                <div class="form-group">
                    <style>
                        input.form-control::placeholder {
                            color: #FFF;
                        }

                        select.form-control:focus,
                        input.form-control:focus
                        {
                            background-color: #C5011A !important;
                        }
                    </style>

                    <select name="" id="" class="form-control" style="margin-bottom: 0; font-size: 1.3em; display: inline-block; background: transparent;border: none;box-shadow: none; color: #FFF">
                        <option value="" style="color: #FFF; background-color:#C5011A">Filter By</option>
                        <option value="" style="color: #FFF; background-color:#C5011A">Event</option>
                        <option value="" style="color: #FFF; background-color:#C5011A">Blogs</option>
                        <option value="" style="color: #FFF; background-color:#C5011A">Press Release</option>
                        <option value="" style="color: #FFF; background-color:#C5011A">News</option>
                    </select>
                    <select name="" id="" class="form-control" style="margin-bottom: 0; font-size: 1.3em; display: inline-block; background: transparent;border: none;box-shadow: none; color: #FFF">
                        <option value="">Sort By</option>
                        <option value="">Title (A-Z)</option>
                        <option value="">Title (Z-A)</option>
                        <option value="">Date (Newest)</option>
                        <option value="">Date (Oldest)</option>
                    </select>

                    <input type="text" class="form-control" placeholder="Search..." style="margin-bottom: 0; font-size: 1.3em; display: inline-block;float:right; background: transparent;border: none;box-shadow: none; color: #FFF">
                </div>
            </form>
        </div>

    </div>
</div>
<div class="section" style="padding-top:70px; padding-bottom:30px;">
    <div class="section_wrapper clearfix">
        <?php foreach($news as $i => $news_item): ?>
        <?php if($i%4==0): ?>
        <div class="items_group clearfix equal-height">
        <?php endif; ?>
            <div class="column one-fourth column_column">
                <div class="photo_box" style="background: #FFF; padding: 15px;-webkit-box-shadow: 0px 13px 31px -8px rgba(0,0,0,0.55);-moz-box-shadow: 0px 13px 31px -8px rgba(0,0,0,0.55);box-shadow: 0px 13px 31px -8px rgba(0,0,0,0.55);">
                    <h4><b><?php echo $news_item['title']; ?></b></h4>

                    <div class="image_frame">
                        <a href="<?=site_url('articles/'.$news_item['category_slug'].'/'.$news_item['news_slug']);?>" title="<?php echo $news_item['title']; ?>">
                            <img class="scale-with-grid" src="<?php echo base_url('assets/images/articles/'.$news_item['img']); ?>" onerror="this.src='https://via.placeholder.com/200x200'">
                        </a>
                    </div>

                    <!-- <div class="desc">
                        Vitae adipiscing turpis. Aenean ligula nibh in, molestie id viverra a,
                        dapibus at dolor.
                    </div> -->
                </div>
            </div>
            <?php $index = $i+1; ?>
            <?php if($index>0 && $index%4==0): ?>
        </div>
        <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
<div class="section" style="padding-bottom: 70px;">
    <center>
        <button class="btn" style="border-radius:0;background-color: #C5011A"><span class="big">Load more</span></button>
    </center>
</div>