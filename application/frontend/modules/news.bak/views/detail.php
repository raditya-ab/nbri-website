<style>
   /*  a .projects-card:hover .photo
    {
        -webkit-box-shadow: -3px 16px 37px 4px rgba(0,0,0,0.50); 
        box-shadow: -3px 16px 37px 4px rgba(0,0,0,0.50);
    } */

    ::selection {
      background: #EC452A; /* WebKit/Blink Browsers */
    }
    ::-moz-selection {
      background: #EC452A; /* Gecko Browsers */
    }
    .project-detail-data
    {
        padding: 20px 50px;
        background-color: #EC452A;
        color: #FFF;
        font-family: 'Montserrat', sans-serif;
    }
</style>
<div class="client">
    <div class="section">
        <div class="container-fluid" style="padding: 0">
            <div class="project-detail-image" style="height: 65vh; width: 100%; background: url('<?=$news_detail['bgimg'];?>') no-repeat center center; background-size: cover">
                
            </div>
            <div class="project-detail-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="detail-title text-center" style="margin-bottom: 10px; font-weight: 800">Date Published</div>
                        <div class="detail-value text-center"><?=date('M d, Y', strtotime($news_detail['created']));?></div>
                    </div>
                    <div class="col-md-6">
                        <div class="detail-title text-center" style="margin-bottom: 10px; font-weight: 800">Category</div>
                        <div class="detail-value text-center"><?=$news_detail['category_name'];?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section" style="padding:50px 0">
        <div class="container">
            <div class="row">
                <div class="col-md-12 project-description">
                    <?=html_entity_decode(addslashes($news_detail['content']));?>
                </div>
            </div>
        </div>
    </div>
</div>
