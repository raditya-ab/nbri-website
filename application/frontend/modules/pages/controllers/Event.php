<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends Public_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('event_m');

        $this->template->set_partial('navbar','partials/event_partial/navbar_event', $this->data);
		$this->template->set_partial('header','partials/header');
        $this->template->set_partial('footer_event','partials/event_partial/footer_event');

        $this->template->set_layout('default_template');

    }

    public function index($id,$slug,$pageslug = 'climate-challenge-workshop')
    {
        $this->data['news_list'] = $this->event_m->get_articles($id);
        $this->data['banner_details'] = array(
            'img' => $pageslug.'.jpg',
            'caption' => ucwords(preg_replace('/\-/i', ' ', $pageslug)),
            'subcaption' => array(
                array(
                    'text' => 'Delivering a battery revolution - reducing the drivers of climate change in Indonesia'
                ),
                array(
                    'text' => '12 - 14<sup>th</sup> July 2021'
                ),
                array(
                    'text' => 'Total Prize &#163; 30.000',
                    'font-size' => '24px'
                ),
            )
        );

        $this->template->set_partial('left_sidebar','partials/event_partial/left_sidebar', $this->data);
        $this->template->set_partial('right_sidebar','partials/event_partial/right_sidebar');
        $this->template->set_partial('banner_hero','partials/banner_hero', $this->data);


        $this->template->set_layout('event_template_3-grid');

        
        $this->template->build($slug.'/index', $this->data);
        
    }

    public function news_index($id,$slug)
    {
        $this->data['news_list'] = $this->event_m->get_articles($id);
                // echo $this->db->last_query();
        // exit();
        $this->data['featured_news'] = $this->event_m->get_featured_articles($id);
        $this->data['page_js'] = array(
            array(
                'url' => '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js'
            )
        );

        $this->template->set('page_title', 'News');
        $this->template->set('page_banner', base_url('assets/images/news_banner.jpg'));
        $this->template->build($slug.'/'.'news_index', $this->data);
    }

    public function news_detail($id,$category_slug,$news_slug)
    {
        $this->load->model('author_m');
        $this->data['news_detail'] = $this->event_m->get_article_detail($news_slug,$id);
        $this->data['author_detail'] = $this->author_m->get_author_profile($this->data['news_detail']['author']);
        $this->data['news_list'] = $this->event_m->get_related_articles($news_slug,$this->data['news_detail']['category']);
        $this->template->build($category_slug.'/'.'news_detail', $this->data);
    }

    public function pages($id,$slug,$pageslug)
    {
        $this->data['banner_details'] = array(
            'img' => $pageslug.'.jpg',
            'caption' => ($pageslug == 'faq') ? 'Frequently Asked Questions' : ucwords(preg_replace('/\-/i', ' ', $pageslug))
        );

        if($pageslug == 'climate-challenge-workshop'){
            $this->data['banner_details']['subcaption'] = array(
                array(
                    'text' => 'Delivering a battery revolution - reducing the drivers of climate change in Indonesia'
                ),
                array(
                    'text' => '12 - 14<sup>th</sup> July 2021'
                ),
                array(
                    'text' => 'Total Prize &#163; 30.000',
                    'font-size' => '24px'
                )
            );
        } else if($pageslug == 'research-prize'){
            $this->data['banner_details']['subcaption'] = array(
                array(
                    'text' => 'Delivering a battery revolution - reducing the drivers of climate change in Indonesia'
                ),
                array(
                    'text' => '12 - 14<sup>th</sup> July 2021'
                ),
                array(
                    'text' => 'Total Prize &#163; 20.000',
                    'font-size' => '24px'
                )
            );
        } else if($pageslug == 'dissemination-awards'){
            $this->data['banner_details']['subcaption'] = array(
                array(
                    'text' => 'Delivering a battery revolution - reducing the drivers of climate change in Indonesia'
                ),
                array(
                    'text' => '12 - 14<sup>th</sup> July 2021'
                ),
                array(
                    'text' => 'Total Prize &#163; 10.000',
                    'font-size' => '24px'
                )
            );
        }

        $this->template->set_partial('banner_hero','partials/banner_hero', $this->data);

        if(in_array($pageslug,array('about','registration-application','climate-challenge-workshop'))){
            $this->data['news_list'] = $this->event_m->get_articles($id);

            $this->template->set_partial('left_sidebar','partials/event_partial/left_sidebar', $this->data);
            $this->template->set_partial('right_sidebar','partials/event_partial/right_sidebar');

            $this->template->set_layout('event_template_3-grid');
        } else {
            $this->template->set_partial('right_sidebar','partials/event_partial/right_sidebar');
            $this->template->set_layout('event_template_2-grid');

        }



        $this->template->build($slug.'/'.$pageslug, $this->data);
    }
}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */