<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->template->set_layout('default_template');
    }

    public function climate_challenge_workshop()
    {
        $this->template->build('climate_change', $this->data);

    }

    public function home()
    {
        $this->load->model('research_m');
        $this->load->model('pages_m');
        $this->load->model('articles/articles_m','articles_m');

        $this->template->set_layout('homepage');
        
        $this->data['subheader_img'] = NULL;
        $this->data['research_list'] = $this->research_m->get_research();
        $this->data['article_list'] = $this->articles_m->get_articles();


        $this->data['pages_section'] = array(
            'section-1' => $this->pages_m->get_page_section('homepage','section-1'),
            'section-2' => $this->pages_m->get_page_section('homepage','section-2'),
            'homepage-about' => $this->pages_m->get_page_section('homepage','homepage-about')
        );
        $this->template->build('homepage', $this->data);
    }

    public function about()
    {
        $this->load->model('pages_m');
        $this->data['pages_section'] = array(
            'about-description-1' => $this->pages_m->get_page_section('about','about-description-1'),
            'about-description-2' => $this->pages_m->get_page_section('about','about-description-2'),
            'about-what-we-do' => $this->pages_m->get_page_section('about','about-what-we-do'),
            'about-our-values' => $this->pages_m->get_page_section('about','about-our-values'),
            'about-our-teams' => $this->pages_m->get_page_section('about','about-our-teams'),
            'team-member-list' => $this->pages_m->get_team_member(),
            'expert-member-list' => $this->pages_m->get_expert_member()
        );
        $this->template->build('about', $this->data);
    }

    public function contact()
    {
        $this->template->build('contact', $this->data);
    }

    public function expert_panel()
    {
        $this->load->view('expertpanel');
    }

    public function research()
    {
        $this->load->model('research_m');


        $this->data['research_list'] = $this->research_m->get_research();
        $this->template->build('research', $this->data);
        // $this->load->view('research');
    }

    public function research_sub($slug = NULL)
    {
        $this->load->model('research_m');

        $this->data['category_detail'] = $this->research_m->get_research_detail($slug);
        $this->data['research_list'] = $this->research_m->get_subresearch($slug);

        if(empty($this->data['research_list'])){
            $this->data['research_detail'] = $this->data['category_detail'];
            $this->data['research_detail']['img'] = (!empty($this->data['research_detail']['thumb_img'])) ? base_url('assets/images/research/'.$this->data['research_detail']['thumb_img']) : base_url('assets/images/nbri_new_logo.png');
            $this->template->build('research_detail', $this->data);
        } else {
            $this->data['category_detail']['img'] = (!empty($this->data['category_detail']['thumb_img'])) ?  base_url('assets/images/research/'.$this->data['category_detail']['thumb_img']) : base_url('assets/images/nbri_new_logo.png');
            $this->template->build('research_sub', $this->data);
        }
    }

    public function research_detail($slug = NULL, $sub_slug = NULL)
    {
        $this->load->model('research_m');

        $this->data['category_detail'] = $this->research_m->get_research_detail($slug);
        $this->data['research_detail'] = $this->research_m->get_subresearch_detail($sub_slug);
        $this->data['research_detail']['img'] = (!empty($this->data['research_detail']['img_thumb'])) ?  base_url('assets/images/subresearch/'.$this->data['research_detail']['img_thumb']) : base_url('assets/images/nbri_new_logo.png');
        $this->template->build('research_detail', $this->data);
    }

    public function publication()
    {
        $this->template->build('publication', $this->data);
    }

    public function opportunities()
    {
        $this->load->model('opportunities_m');
        $this->template->build('opportunities', $this->data);
    }
    public function opportunities_sub()
    {
        $this->load->model('opportunities_m');
        $this->template->build('opportunities_sub', $this->data);
    }

    public function opportunities_detail($slug = NULL)
    {
        $this->template->build('opportunities_detail', $this->data);
    }

    public function educationskill()
    {
        $this->template->build('educationskill', $this->data);
    }



    public function membership()
    {
        $this->template->build('membership', $this->data);
    }

    public function maps()
    {
        $this->template->build('map_index');
    }

}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */