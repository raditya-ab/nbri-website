<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_m extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    /*----------  Store Data  ----------*/

    public function add($filename = NULL)
    {
        $input = $this->input->post(NULL, TRUE);
        $pages_data = array(
            'name' => $input['name'],
            'slug' => $input['slug'],
            'description' => $input['content'],
            'status' => $input['status'],
            'img' => $filename
        );

        $this->db->insert('pages', $pages_data);

        if($this->db->affected_rows() == 0){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $pages_id = $this->db->insert_id();
            $output = array(
                'status' => 1,
                'message' => array(
                    'pages_id' => $pages_id,
                    'pages_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    public function update($id, $filename = NULL)
    {
        $pages_id = $id;
        $input = $this->input->post(NULL, TRUE);
        $pages_data = array(
            'name' => $input['name'],
            'slug' => $input['slug'],
            'description' => $input['content'],
            'status' => $input['status'],
        );


        if(!is_null($filename) && !empty($filename)) $pages_data['img'] = $filename;

        $this->db->where('id', $pages_id);
        $this->db->update('pages', $pages_data);

        $this->db->trans_complete();

        if($this->db->affected_rows() <= 0){
            $output = array(
                'status' => 0,
                'message' => array(
                    'error' => $this->db->error()
                )
            );
        } else {
            $output = array(
                'status' => 1,
                'message' => array(
                    'pages_id' => $pages_id,
                    'pages_slug' => $input['slug']
                )
            );
        }

        return $output;
    }

    /*----------  Delete Data  ----------*/

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('pages');

        return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
    }

    /*----------  Get All Data  ----------*/

	public function get_pages_list()
	{
		$input    = $this->input->post();
		$query    = array();
		$page     = (int)$this->input->post('pagination',TRUE)['page'];
		$per_page = (int)$this->input->post('pagination',TRUE)['perpage'];
		$offset   = ($page * $per_page) - $per_page;
        $search_flag = FALSE;
        $filter_field = array();


        $order  = $this->input->post('sort', true);

        $query_search = $this->input->post('query', TRUE);

        if(!is_null($query_search) && !empty($query_search)){
            if(is_array($query_search)) {
                foreach($query_search as $field => $search_field){
                    $filter_field[$field] = trim($search_field);
                }
            } else {
                $filter_field[] = trim($query_search);
            }
        }

        $this->db->start_cache();

        $select = array(
            'p.id',
            'p.page_name',
            'p.page_slug',
            'GROUP_CONCAT(s.section_name) "section_name"',
            'p.status',
            'p.created',
            'p.modified'
        );

        $this->db->select($select);
        $this->db->from('pages p');

        $query['totalRecords'] = $this->db->count_all_results();

        if(!is_null($filter_field) && !empty($filter_field)){
            foreach($filter_field as $field => $search) {
                if($field != 'status') {
                    $this->db->or_like('LOWER(page_name)', $search);
                    // $search_select = array('section_name','page_name');
                    // $this->db->group_start();
                    // foreach($search_select as $s)
                    // {
                    //     $this->db->or_like('LOWER('.$s.')',$search);
                    // }
                    // $this->db->group_end();
                } else {
                    $this->db->group_start();
                    $this->db->or_like('status', $search, 'BOTH');
                    $this->db->group_end();
                }
            }
        }

        $this->db->stop_cache();

        $this->db->group_by('p.id');
        $this->db->join('pages_section s', 'p.id = s.pages_id', 'left');
        $this->db->order_by($order['field'], $order['sort']);

        $this->db->limit($per_page,$offset);

        $query['records'] = $this->db->get()->result_array();
        $query['log'] = $this->db->last_query();
	    $query['filterRecords'] = $this->db->count_all_results();

	    $this->db->flush_cache();

		return $query;
	}

    public function get_team_member()
    {
        return $this->db->get('team')->result_array();
    }

     public function get_expert_member()
    {
        return $this->db->get('expert')->result_array();
    }

    public function get_section_list()
    {
        $this->db->select('id,section_name,section_slug,page_name,page_slug');
    }

    /*----------  Get Specific Data  ----------*/

    public function get_pages_detail($id)
    {
        $this->db->where('id', $id);

        $result = $this->db->get('pages')->row_array();

        return $result;
    }

    public function get_page_section($page_slug, $section_slug)
    {
        $this->db->select('ps.content');
        $this->db->where('ps.section_slug', $section_slug);
        $this->db->where('p.page_slug', $page_slug);

        $this->db->from('pages_section ps');
        $this->db->join('pages p', 'p.id = ps.pages_id', 'left');

        return $this->db->get()->row_array()['content'];
    }

    /*----------  Checking  ----------*/

    public function pages_exist($id)
    {
        $this->db->where('id', $id);

        return ($this->db->count_all_results('pages') > 0) ? TRUE : FALSE;
    }

    public function pages_slug_exist($slug)
    {
        $this->db->where('page_slug', $slug);

        return ($this->db->count_all_results('pages') > 0) ? TRUE : FALSE;
    }

}

/* End of file Article_m.php */