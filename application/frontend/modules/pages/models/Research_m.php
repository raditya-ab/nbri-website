<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Research_m extends CI_Model {
    public function get_research()
    {
        return $this->db->get('research_category')->result_array();
    }

    public function get_research_detail($slug)
    {
        $this->db->where('slug', $slug);
        $this->db->limit(1);
        return $this->db->get('research_category')->row_array();
    }

    public function get_subresearch($category_slug){
        $select = array(
            'r.id',
            'r.research_category',
            'rc.category_name',
            'r.research_name',
            'rc.slug as category_slug',
            'r.slug',
            'r.filename',
            'r.img_thumb',
            'r.content'
        );
        $this->db->select($select);
        $this->db->from('research r');
        $this->db->join('research_category rc', 'rc.id = r.research_category', 'left');
        $this->db->where('rc.slug', $category_slug);
        $this->db->order_by('r.created','desc');

        return $this->db->get()->result_array();
    }

    public function get_subresearch_detail($slug)
    {
        $this->db->where('slug', $slug);
        $this->db->limit(1);
        return $this->db->get('research')->row_array();
    }


}

/* End of file Research_m.php */
/* Location: ./application/frontend/modules/pages/models/Research_m.php */