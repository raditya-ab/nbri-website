<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(https://faraday.ac.uk/wp-content/uploads/2019/04/Alireza-Rastegarpanah-2-resized.jpg)">
                <h1 class="page-title">About NBRI</h1>
            </div>
        </div>
    </div>
</section>

<section class="about-section content-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12 text-navy">
                <?php
                    $content = stripslashes(html_entity_decode($pages_section['about-description-1']));

                    echo $content;
                ?>
                <!-- <img class="img-hero" src="<?=base_url('assets/images/about-us.jpeg');?>" alt="NBRI - National Battery Research Institute" style="width: 100%">

                <h3 class="section-title">National Battery Research Institute<br>(NBRI)</h3>
                <p class="section-description">The National Battery Research Institute (NBRI) was founded by E.Kartini in 2014. Launched in October 2019, the National Battery Research Institute (NBRI) is the Indonesia’s independent institute for electrochemical energy storage science and technology, supporting research, training, and analysis.</p>

                <p class="section-description">Bringing together expertise from universities and industry, As part of the national Battery Challenge, the NBRI endeavours to make the Indonesia the go-to place for the research, development, manufacture and production of new electrical storage technologies for both the electronics, automotive industries and the wider.</p>

                <p class="section-description">The NBRI bench marked of the Faraday Institution in UK. Its establishing is supported by the Global Challenge Research Fund (GCRF), UK, through the cooperation with QMUL.</p>

                <p class="section-description">NBRI aims to contribute to the overall research capacity and training environment in Indonesia in battery research. We bring together scientists and industry partners on research projects to reduce battery cost, weight, and volume; to improve performance and reliability; and to develop whole-life strategies including recycling and reuse.</p>

                <div class="spacer-40"> &nbsp;</div>
                <img class="img-hero" src="<?=base_url('assets/images/about-us.jpeg');?>" alt="NBRI - National Battery Research Institute" style="width: 100%">

                <h3 class="section-title">What We Do</h3>
                <ul style="list-style: none">
                    <li>• Mapping of Indonesian activities: Human resources and capabilities, research facilities, local material/mineral resources, and industrial activities.</li>
                    <li>• Develop a batteries manufacturing industry using locally sourced resources, which will enable Indonesia to be independent in energy.</li>
                    <li>• As a forum to help build networking with other parties with similar projects throughout the world.</li>
                </ul> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-navy">
                <?php
                    $content = stripslashes(html_entity_decode($pages_section['about-description-2']));

                    echo $content;
                ?>
            </div>
        </div>
    </div>
</section>
<section class="about-section content-section bg-navy" id="our-values">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="color: #b9dc00">
                <h3 class="section-title" style="text-transform: uppercase;">Our Values</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <img src="<?=base_url('assets/images/we_collaborate.jpg');?>" alt="" class="img-hero" style="max-width: 100%">
            </div>
            <div class="col-md-6 align-bottom">
                <h3 class="section-title font-regular">We Collaborate</h3>
                <p class="section-description">We actively collaborate to achieve shared and focused objectives on International Networking.<br><br>We build connections within and between project teams, and externally with industry, institution, government and researcher. We work together to generate a research environment that encourages international collaboration that is interdisciplinary and cross-sectoral, which would thus build research capacity.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 order-md-2">
                <img src="<?=base_url('assets/images/we_pioneering.jpg');?>" alt="" class="img-hero" style="max-width: 100%">
            </div>
            <div class="col-md-6 align-bottom order-md-1">
                <h3 class="section-title font-regular">We are Pioneering</h3>
                <p class="section-description">We are pioneering, innovative and resilient.<br><br>We are the first institute in Indonesia that seeks to unite parties related to batteries to create an independent Indonesia in terms of industry and strive to provide the latest innovations related to batteries.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <img src="<?=base_url('assets/images/we_transparent.jpg');?>" alt="" class="img-hero" style="max-width: 100%">
            </div>
            <div class="col-md-6 align-bottom">
                <h3 class="section-title font-regular">We are Transparent</h3>
                <p class="section-description">We are driven to leave a legacy.<br><br>We give priority to honesty and transparency in terms of delivering data on our website. We try to display data as accurately as possible so that the data we have is the latest data.</p>
            </div>
        </div>
    </div>
</section>
<section class="content-section about-member">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="section-title font-bold">Our Teams</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 grid-wrapper">
                <?php foreach($pages_section['team-member-list'] as $i => $team_member): ?>
                    <?php if($i == 2): ?><div style="flex-basis: 100%; height: 40px;"></div><?php endif; ?>
                    <div class="grid-member-wrapper">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#<?=$team_member['slug'];?>">
                            <div class="img-wrapper" style="background-image: url(<?=base_url('assets/images/'.$team_member['img']);?>">
                                <div class="overlay"></div>
                            </div>
                            <div class="caption-wrapper">
                                <h3 class="member-name"><?=$team_member['name'];?></h3>
                                <p class="subtitle"><?=$team_member['title'];?></p>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
<section class="content-section about-expert">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="section-title font-bold">Expert Panel</h3>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 grid-wrapper" style="position: relative">
                <button class="btn btn-primary btn-lg" id="btn-expert-prev" style="position: absolute;top: 120px;left: -40px;z-index: 1; color: #181d62;background-color: transparent;border: none"><i class="fas fa-2x fa-chevron-left"></i></button>
                <div id="expert-slider">
                    <?php foreach($pages_section['expert-member-list'] as $i => $expert_member): ?>
                        <div class="grid-member-wrapper">
                            <a href="javascript:void(0);" class="font-color-navy" data-toggle="modal" data-target="#expert-<?=$expert_member['slug'];?>">
                                <div class="img-wrapper" style="background-image: url(<?=base_url('assets/images/'.$expert_member['img']);?>)"></div>
                                <div class="caption-wrapper">
                                    <h3 class="member-name"><?=$expert_member['name'];?></h3>
                                    <p class="subtitle" style="white-space: pre-wrap;"><?=$expert_member['subtitle'];?></p>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
                <button class="btn btn-primary btn-lg" id="btn-expert-next" style="position: absolute;top: 120px;right: -40px;z-index: 1; color: #181d62;background-color: transparent;border: none"><i class="fas fa-2x fa-chevron-right"></i></button>
            </div>
        </div>
    </div>
</section>


<!-- Modal -->
<?php foreach($pages_section['team-member-list'] as $i => $team_member): ?>
    <div class="modal modal-member fade" id="<?=$team_member['slug'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-full modal-dialog-centered" role="document">
        <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
          <div class="modal-body p-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">
                  <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
              </span>
            </button>
            <div class="row">
                <div class="col-md-4">
                    <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?=base_url('assets/images/'.$team_member['img']);?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                    </div>
                    <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                        <?=$team_member['name'];?>
                    </h3>
                    <p class="subtitle"><?=$team_member['title'];?></p>
                </div>
                <div class="col-md-8">
                    <p class="description"><?=html_entity_decode(stripslashes($team_member['description']));?></p>
                </div>
            </div>
          </div>
          <div class="modal-footer no-border p-0">
              <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
          </div>
        </div>
      </div>
    </div>
<?php endforeach; ?>

<?php foreach($pages_section['expert-member-list'] as $i => $expert_member): ?>
    <div class="modal modal-member fade" id="expert-<?=$expert_member['slug'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-full modal-dialog-centered" role="document">
        <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
          <div class="modal-body p-0">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">
                  <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
              </span>
            </button>
            <div class="row">
                <div class="col-md-4">
                    <div class="image-modal" style="height: 250px; width: 250px; background-image: url(<?=base_url('assets/images/'.$expert_member['img']);?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                    </div>
                    <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                        <?=$expert_member['name'];?>
                    </h3>
                    <p class="subtitle"><?=$expert_member['subtitle'];?></p>
                </div>
                <div class="col-md-8">
                    <p class="description"><?=html_entity_decode(stripslashes($expert_member['description']));?></p>
                </div>
            </div>
          </div>
          <div class="modal-footer no-border p-0">
              <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
          </div>
        </div>
      </div>
    </div>
<?php endforeach; ?>
