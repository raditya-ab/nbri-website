<div class="content_wrapper clearfix">
    <div class="sections_group">
        <div class="entry-content">
            <div class="section" style="padding-top:70px; padding-bottom:30px; ">
                <div class="section_wrapper clearfix">
                    <div class="items_group clearfix">
                        <!-- One Second (1/2) Column -->
                        <div class="column one column_column">
                            <?php echo stripslashes(html_entity_decode($news_detail['content'])) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section the_content no_content">
                <div class="section_wrapper">
                    <div class="the_content_wrapper"></div>
                </div>
            </div>
        </div>
    </div>
</div>
