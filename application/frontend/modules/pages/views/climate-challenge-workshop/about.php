<h2 class="section-title">Background</h2>
<div class="col-md-12 text-justify">This Workshop was funded as part of the many activities the British Council is
    delivering in the run up to COP26 (the 26th United Nations Climate Change Conference of the Parties) which takes
    place in Glasgow in November 2021. The aim is to harness the power of people all over the world – particularly young
    people and those most vulnerable to the effects of climate change – to connect and collaborate through culture and
    education to combat climate change.
    <br>
    Addressing the challenges of climate change is critical for promoting global sustainable development. Climate
    Challenge Workshop grants are designed to provide financial support to bring together a UK and Indonesia bilateral
    cohort of early career researchers (ECRs) to take part in virtual interdisciplinary research workshops. The
    workshops offer a platform to stimulate and discuss innovative research ideas and encourage sustainable
    collaborations across disciplines.
    <br>
    It will deliver a balanced programme specifically aimed at early career researchers (ECRs) from Indonesia and the UK, with the following objectives:
    <br>
    <ul class="list-style">
        <li>Enable networking and mutual research exchange between ECRs from the UK and Indonesia, via the effective use of online platforms (online profiles, speed networking, break-out sessions) and post-Workshop activities.</li>
        <li>Deliver a climate change workshop containing a mixed programme of ECR talks, plenaries, breakout sessions and training sessions</li>
        <li>Train ECRs in contemporary issues around climate change, with a balanced programme including the underlying scientific causes and effects of climate change, potential technological solutions and the broader socioeconomic consequences.</li>
        <li>Identify the top climate-range ECRs in Indonesia and the UK, and award them <a href="<?=site_url('event/climate-challenge-workshop/research-prize');?>">Research Prize</a> and <a href="<?=site_url('event/climate-challenge-workshop/dissemination-awards');?>">Dissemination Awards</a></li>
        <li>Mentor ECRs prior to the workshop to upskill and increase their capacity to bid for the Research Prizes, and continue that mentor relationship during the delivery of the projects.</li>
    </ul>
</div>
<br>
<h2 class="section-title">Objectives</h2>
<div class="col-md-12 text-justify">The objectives of this workshop are to increase capacity, undertake training and
    promote research cooperation and impact, achieved through plenary lectures and training sessions given by experts,
    as well as fund dissemination activities and joint research projects. In general, the objectives are:
    <br>
    <ol type="i">
        <li>Explain the causes and the role that Indonesia has on climate change, locally and globally, across different
            economic sectors (e.g transport, energy, industry, tourism, household).</li>
        <li>Explain the social and economic impact that climate change will have on Indonesia.</li>
        <li>Explain potential solutions to reducing the causes of Indonesian driven climate change, such as battery
            backed renewable energy or electrification of transport.</li>
        <li>Identify scalable battery and renewable energy technology that can tackle climate change.</li>
        <li>Elucidate the wider environmental impact of solutions to climate change (e.g pollution from mining,
            deforestation and displacement of indigenous people for hydroelectric power, carbon capture etc).</li>
        <li>Ensure that ECRs understand the proposed solutions must be of net benefit to climate change and the
            environment, by taking into account the full environmental cost.</li>
    </ol>
</div>
<br>
<h2 class="section-title">Thematic Areas</h2>
<div class="col-md-12 text-justify">
    <ol type="i">
        <li>Climate change specific to Indonesia - causes and effects: This will cover all potential causes (SDG13),
            such as deforestation (SDG13, SDG15), transport (SDG9, SDG11), tourism, cooking and household emissions
            (SDG3), electricity generation (SDG7, SDG11,SDG12), industry (SDG9), and future potential economic growth
            predictions(SDG1, SDG8, ). We note that the major drivers of climate change in Indonesia are electricity
            generation, industry, burning of biomass and transportation, but others exist and we will consider all.
            Effects will cover different localities (urban, rural, coastal: SDG14, SDG15), economic status and
            employment sector (SDG8), natural environment (SDF14, SDG15), gender equality (SDG5) etc.</li>
        <li>Potential technological solutions to climate change specific to Indonesia: This will heavily emphasise
            technological solutions to renewable energy (solar, wind, hydro) backed up with batteries for storage when
            renewables isn't generating (SDG13, SDG7), the infrastructure and solutions needed to support electrified
            transport (SDG9) in Indonesia, and the potential for carbon capture.</li>
        <li>Socioeconomic, policy and financial barriers to climate change solutions in Indonesia: There are many
            barriers, often financial and political, to sustainable growth in Indonesia. Government policy has presided
            over a significant expansion of energy use, most of which has come from fossil fuels, as a result of
            abundant sources of coal and oil in Indonesian territory. The political, financial, social and economic
            battlers will be fundamental to the workshop, particularly in assessing how they may link in with
            technological solutions.</li>
    </ol>
</div>
<br>
<h2 class="section-title">Organizers</h2>
<section class="content-section about-member">
    <div class="container">
        <div class="row">
            <div class="col-md-12 grid-wrapper">
                <div class="grid-member-wrapper">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#prof-dr-alan-j-drew">
                        <div class="img-wrapper"
                            style="background-image: url(<?php echo base_url('assets/images/acfeb6747f6309444ecba5d240856720.png');?>">
                            <div class="overlay"></div>
                        </div>
                        <div class="caption-wrapper">
                            <h3 class="member-name">Prof. Dr. Alan J. Drew</h3>
                            <p class="subtitle">Chair</p>
                        </div>
                    </a>
                </div>
                <div class="grid-member-wrapper">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#prof-dr-rer-nat-evvy-kartini">
                        <div class="img-wrapper"
                            style="background-image: url(<?php echo base_url('assets/images/46b8d33bc9a0a01dbe8d86f8a040b33e.png');?>">
                            <div class="overlay"></div>
                        </div>
                        <div class="caption-wrapper">
                            <h3 class="member-name">Prof. Dr. Evvy Kartini</h3>
                            <p class="subtitle">Vice Chair</p>
                        </div>
                    </a>
                </div>

                <div class="modal modal-member fade" id="prof-dr-rer-nat-evvy-kartini" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
                        <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
                            <div class="modal-body p-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">
                                        <img src="<?php echo base_url('assets/images/clear_24px.svg" alt="">');?>">
                                    </span>
                                </button>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="image-modal"
                                            style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/46b8d33bc9a0a01dbe8d86f8a040b33e.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                                        </div>
                                        <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                                            Prof. Dr. rer nat Evvy Kartini </h3>
                                        <p class="subtitle">Vice Chair</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p class="description">
                                            <p>Evvy Kartini holds the title of Prof. Dr. rer nat.&nbsp;She
                                                is an expert on the neutron scattering and respected
                                                internationally. Her international reputation in the field
                                                of neutron scattering and solid state ionics, has been well
                                                established.</p>
                                            <p>She has been represented as a leader (President) of the
                                                Indonesian Neutron Scattering Society (INSS) since 2013. She
                                                is one of the executive committee of the Asian Oceania
                                                Neutron Scattering Society (AONSA).</p>
                                            <p>Besides neutron scattering, Evvy Kartini has expertise on the
                                                materials science, especially on lithium ion battery
                                                research. Evvy Kartini, has became one of the International
                                                Board Member of the Asian Society of Solid State Ionics and
                                                International Society of Solid State Ionics. Recently, Evvy
                                                Kartini has attended the 20th International Conference on
                                                Solid State Ionics (ICSSI) in Keystone, Colorado, USA, 2015.
                                                She was there not only for presenting her research work on
                                                battery researches, but also represented Asia-Australia
                                                region as councilor of the International Society of Solid
                                                State Ionics.</p>
                                            <p>She was one of the International Advisory Board, and also
                                                chaired the session of Research on Battery Materials. She
                                                also presented her recent topic on development of new
                                                lithium solid electrolyte, which is explored by the
                                                inelastic spectrometer instrument at Japan Proton
                                                Accelerator Research Complex (J-PARC), the foremost leading
                                                spallation neutron source in the world.</p>
                                            <p><a href="/backend/\&quot;/backend/\\&quot;/nbri_website/public/backend/\\\\&quot;http:/www.batan.go.id/index.php/en/staff-profile-evvy\\\\&quot;\\&quot;\&quot;"
                                                    target="\&quot;\\&quot;\\\\&quot;_blank\\\\&quot;\\&quot;\&quot;"
                                                    rel="\&quot;\\&quot;\\\\&quot;noopener\\\\&quot;\\&quot;\&quot;">http://www.batan.go.id/index.php/en/staff-profile-evvy</a>
                                            </p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer no-border p-0">
                                <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal modal-member fade" id="prof-dr-alan-j-drew" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
                        <div class="modal-content bg-navy font-color-green" style="padding: 50px; position: relative;">
                            <div class="modal-body p-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">
                                        <img src="<?php echo base_url('/assets/images/clear_24px.svg" alt="">');?>">
                                    </span>
                                </button>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="image-modal"
                                            style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/acfeb6747f6309444ecba5d240856720.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                                        </div>
                                        <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                                            Prof. Dr. Alan J. Drew </h3>
                                        <p class="subtitle">Chair</p>
                                    </div>
                                    <div class="col-md-8">
                                        <p class="description">
                                            <p>Professor Alan Drew was&nbsp;appointed Leverhulme Fellow in
                                                the Centre for Condensed Matter &amp; Materials
                                                Physics&nbsp;in 2008. He was rapidly promoted to Senior
                                                Lecturer (2011) and then Reader (2012), and in 2018 was
                                                promoted to Professor.</p>
                                            <p>Prof. Drew has been awarded a number of prestigious
                                                fellowships and awards over his career, starting with a
                                                Fellowship of the Royal Commission of the Exhibition of 1851
                                                (2004), Leverhulme Fellow (2008), European Research Council
                                                (ERC) Fellow (2012), Talent 1000 Scholar of the Chinese
                                                Ministry of Education (2014) and Changjiang Distribguished
                                                Professor at Sichuan University (2015). He is currently Head
                                                of the CCMMP Research Centre, and Director of the Materials
                                                Research Institute.</p>
                                            <p>His main research interests are using spin sensitive and
                                                structural probes situated at central facilities to
                                                characterise and understand the fundamental properties of
                                                materials, backed up with laboratory based
                                                techniques&nbsp;(e.g magnetic, structural and electrical
                                                characterisation, thin film growth, raman/IR spectroscopy,
                                                electro/photo luminescence). His main interests are
                                                understanding spin and charge carrier dynamics in organic
                                                and biological materials, the properties of materials with
                                                novel quantum mechanical states, structure function
                                                relationships in biomass derived conductors.</p>
                                            <p><a href="/backend/\&quot;/backend/\\&quot;/backend/\\\\&quot;https:/www.qmul.ac.uk/spa/people/academics/profiles/ajdrew.html\\\\&quot;\\&quot;\&quot;"
                                                    target="\&quot;\\&quot;\\\\&quot;_blank\\\\&quot;\\&quot;\&quot;"
                                                    rel="\&quot;\\&quot;\\\\&quot;noopener\\\\&quot;\\&quot;\&quot;">https://www.qmul.ac.uk/spa/people/academics/profiles/ajdrew.html</a>
                                            </p>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer no-border p-0">
                                <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
                            </div>
                        </div>
                    </div>
                </div>
</section>
