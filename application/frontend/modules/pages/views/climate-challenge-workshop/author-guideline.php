<div class="row">
    <div class="col-md-12">
        <h2 class="section-title">Research Prize Applicant Guideline</h2>
        <p>
            Please find below the proposal guideline for research prize applicant. Please note that proposal must follow
            these template and guideline to pass the eligibility process
        </p>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-12 text-center">
        <p class="mb-0">
            <b>Download Research Prize Applicant Guideline Form</b>
        </p>
        <a class="btn btn-navy btn-readmore m-2"
            href="<?php echo base_url('assets/file/research-prize-applicant-guidelines.pdf');?>" download>Download PDF
            Form</a>
    </div>
</div>