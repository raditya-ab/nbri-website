<h2 class="section-title">Background</h2>
<p class="text-justify">
    The Prizes of up to £8,000 each are available, and we expect to be able to fund around 4 prizes. The first winner prize will get £8.000, and £4.000 for the second to fourth winners. These challenge research prizes are aimed at enabling ECRs to undertake a piece of original research involving batteries, climate change and Indonesia. Further details on remit can be found in the About section [link]. This is open to teams of up to 3 ECRs, where the primary investigator (PI) must be from Indonesia (with a preferred international team) and be employed (i.e the PI cannot be a PhD student, but a PhD student can be on the team). The process is as follows:
    <ol>
        <li>Register your interest <a target="_blank" href="http://bit.ly/ClimateChallengeWorkshop">Here</a>. You will then receive an invite to a networking event in early May, that will help you form teams of people to apply for the fund.</li>
        <li>Please note that your personal details and research interests will be circulated to everyone who has expressed an interest to attend the networking event; if you do not want this to happen, please ensure you indicate this on the form. Whilst it is still possible to apply for funding without attending the networking event or having your details circulated, we note that this might lower the quality of the application and therefore lower the chances of success.</li>
        <li>Once the networking event is finished, you will be sent an application form to fill out, along with further instructions on how to make the full application for funding.</li>
        <li>The proposal should be submitted before the 22nd June 2021 deadline, using the instructions and application supplied after the networking event.</li>
        <li>Further details on remit can be found in the About section <a href="<?=site_url('event/climate-challenge-workshop/about');?>">here</a> areas that we will consider for assessment are listed below and <a href="<?=site_url('event/climate-challenge-workshop/faq');?>">terms & conditions</a>.</li>
        <li>You will be informed of being selected to give a talk at the workshop to present your ideas for a research project (and guidelines for speaking) on the 4th July 2021.</li>
        <li>You will give a presentation about your proposed project at the workshop following the guidelines. A panel of experts will then select the projects to be funded from those that gave the presentation. Candidates will be informed at a ceremony at the end of the workshop, and be expected to start the work no later than 1st September 2021. The projects must be finished by 31st March 2022 (i.e all monies spent and claimed for)</li>
        <li>You will be assigned a mentor, whose role will be to track the project to ensure delivery, and ensure finances are on track (not over or under spent).</li>
        <li>You will be responsible for preparing quarterly reports on progress (i.e end of December and end of March).</li>
        <li>You will be responsible for ensuring that financial regulations are adhered to; please see the end of this page for eligible and ineligible costs <a href="<?=site_url('event/climate-challenge-workshop/faq');?>">here</a>. If you are  successful, please check with Prof. Alan Drew if you are unsure.</li>
        <li>When drawing up the budget for the proposal, please see the <a href="<?=site_url('event/climate-challenge-workshop/faq');?>">FAQs</a> for eligible and ineligible costs</li>
        <li>In  order to be eligible for funding, you must attend the workshop and give a presentation on your proposed research. This is a vital part of the assessment process and you will be ineligible if you do not attend.</li>
    </ol>
</p>
<p class="text-justify">The aim of this Climate Challenge Workshop is to harness the power of people
    all over the world – particularly young people and those most vulnerable to the effects of climate change –
    to connect and collaborate through culture and education to combat climate change.
    This workshop will explain the causes and the role that Indonesia has on climate change,
    locally and globally, across different economic sectors (e.g transport, energy, industry, tourism, household)
    as well as the social and economic impact that climate change will have on Indonesia.
    It will address the solutions, whether technological or socioeconomic, by addressing the following themes:
    <br>
    <ol>
        <li>Climate change specific to Indonesia - causes and effects.</li>
        <li>Technological solutions that combine renewable energy (solar, wind, hydro) with batteries,
            put into the context of a battery transport revolution in Indonesia </li>
        <li>Socioeconomic, policy and financial barriers to climate change solutions in Indonesia</li>
    </ol>
    It will deliver a balanced programme specifically aimed at early career researchers (ECRs)
    from Indonesia and the UK, with the following objectives:
    <ol>
        <li>Enable networking and mutual research exchange between ECRs from the UK and Indonesia,
            via the effective use of online platforms (online profiles, speed networking, break-out sessions)
            and post-Workshop activities.</li>
        <li>Deliver a climate change workshop containing a mixed programme of ECR talks, plenaries, breakout
            sessions and training sessions</li>
        <li>Train ECRs in contemporary issues around climate change, with a balanced programme including the
            underlying scientific causes and effects of climate change, potential technological solutions and
            the broader socioeconomic consequences.</li>
        <li>Identify the top climate-range ECRs in Indonesia and the UK,
            and award them Dissemination Awards and Research Prizes (see below).</li>
        <li>Mentor ECRs prior to the workshop to upskill and increase their capacity
            to bid for the Research Prizes, and continue that mentor relationship during the delivery of the projects.
        </li>
    </ol>
</p>
<br>
<h2 class="section-title">Programme</h2>
<div class="col-md-12 text-justify">The Workshop programme will be split into four sections:
    <br>
    <b>Dissemination Awards :</b> ECRs will be able to submit an abstract to present their research at the Workshop.
    Up to 5 speakers will then be selected for funding to attend a conference to present their research work (relevant
    to climate change, batteries and Indonesia).
    Example conferences that could be considered are COP26 fringe meetings, or the Asian-Pacific Climate Change Week.
    All ECRs are eligible. For further information, please see <a href="<?=site_url('event/climate-challenge-workshop/registration-application');?>">here.</a>
    <br>
    <b>Research Prizes :</b> Prizes of up to £8,000 each are available to be applied for, which are aimed at enabling
    ECRs to undertake a piece of original research involving batteries, climate change and Indonesia. This is open to
    teams of up to 3 ECRs, where the primary investigator (PI) must be from Indonesia (with a preferred international
    team) and be employed (i.e the PI cannot be a PhD student, but a PhD student can be on the team). A virtual
    partnering/networking solution will be implemented - we are currently seeking expressions of interest from
    individuals to attend this virtual networking event, with the expectation that proposals will be submitted once
    teams are formed. Projects will be selected, and PIs will be asked to present their proposals to a panel of experts
    at the workshop. Successful applicants will be assigned a mentor to help them deliver on the project. For further
    information, please see <a href="<?=site_url('event/climate-challenge-workshop/registration-application');?>">here.</a>
    <br>
    <b>Lectures :</b> Capacity building and training objectives will achieved through plenary lectures and training
    sessions given by experts. Details to be announced in due course. This will be open to anyone.
    <br>
    <b>Breakout sessions :</b> to enable participants to network informally after the formal has finished for the day.
</div>