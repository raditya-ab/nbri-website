<h2 class="section-title">Climate Challenge Dissemination Awards</h2>
<div class="col-md-12">
    <p>
        This research program is designed for Early Career Researcher who hold citizens and resident of United Kingdom or Indonesia. The participant must be PhD student or a research staff with maximum of 5 years post PhD experience or staff qualified to Master level or equivalent with maximum of 8 years experiences. The total prize of £10,000 are available, We expect to fund around 5 Dissemination Awards each with a maximum value £2,000, but we note this is cap and not a target. 
    </p>
    <p>
        ECRs will be able to submit an abstract to present their research at the Workshop, which will be used to select the speakers at the Workshop. The best talks will then be selected for funding to attend a relevant conference of their choice to present their research work (i.e present the same research as presented at the workshop. We encourage applicants to consider presenting at COP26 fringe meetings, or the Asian-Pacific Climate Change Week, but any relevant conference will be considered for funding. All ECRs are eligible. The process is as follows:
    </p>
    <ol>
        <li>We note that we have applied to exhibit at COP26 Green Zone (open to the general public and for exhibitors) and will find out in May of we are successful, and so if you wish to attend COP26 it may be sensible to submit an application after we have been informed if we are successful.</li>
        <li>Research can be on anything that is in remit of <a href="<?=site_url('event/climate-challenge-workshop/about');?>">the Workshop</a> areas that we will consider for assessment are listed below and <a href="<?=site_url('event/climate-challenge-workshop/faq');?>">terms & conditions</a></li>
        <li>You will be informed of being selected to give a talk at the workshop to present your research (and guidelines for speaking). You will then be considered for funding to attend the conference chosen on the abstract form, based on the talk you give. A panel of experts will select the individuals to be funded from those that gave the presentation. Candidates will be informed at a ceremony at the end of the workshop. All expenses must be completed by 31st March 2022 (i.e all monies spent and claimed for).</li>
        <li>You will be responsible for preparing a final report before the end of March.</li>
        <li>5.You will be responsible for ensuring that financial regulations are adhered to; please see the end of this page for <a href="<?=site_url('event/climate-challenge-workshop/faq');?>">eligible and ineligible costs</a>. Please check with Prof. Alan Drew if you are unsure.</li>
    </ol>
    <p class="mt-3">
        Register your interest <a
            href="<?=site_url('event/climate-challenge-workshop/registration-application');?>">here</a>
    </p>
</div>
<!-- <p>
        Call for Participant: May 2021 <br>
        Abstract Deadline: 15th June 2021 <br>
        Candidate Announcement: 4 July 2021 <br>
        Workshop Event: 12 - 14 July 2021 <br>
        <br>
        ECRs will be able to submit an abstract to present their research at the Workshop, which will be used to select the speakers at the Workshop. The best talks will then be selected for funding to attend a relevant conference of their choice to present their research work (i.e present the same research as presented at the workshop. We encourage applicants to consider presenting at COP26 fringe meetings, or the Asian-Pacific Climate Change Week, but any relevant conference will be considered for funding. We expect to fund a minimum of 5 speakers to present at conferences, with a maximum budget of £2,000 per person, but we note this is a cap and not a target. All ECRs are eligible. The process is as follows:
        <ol>
            <li>Upload an abstract, budget and details of the conference you wish to attend <a target="_blank" href="http://bit.ly/ClimateChallengeWorkshop">here</a>, by the 15th June 2021.</li>
            <li> We note that we have applied to exhibit at COP26 Green Zone (open to the general public and for exhibitors) and will find out in May of we are successful, and so if you wish to attend COP26 it may be sensible to submit an application after we have been informed if we are successful.</li>
            <li> Research can be on anything that is in remit of <a href="<?=site_url('event/climate-challenge-workshop/about');?>">the Workshop</a> areas that we will consider for assessment are listed below and <a href="<?=site_url('event/climate-challenge-workshop/faq');?>">terms & conditions</a>.</li>
            <li> You will be informed of being selected to give a talk at the workshop to present your research (and guidelines for speaking). You will then be considered for funding to attend the conference chosen on the abstract form, based on the talk you give. A panel of experts will select the individuals to be funded from those that gave the presentation. Candidates will be informed at a ceremony at the end of the workshop. All expenses must be completed by 31st March 2022 (i.e all monies spent and claimed for).</li>
            <li> You will be responsible for preparing a final report before the end of March.</li>
            <li> You will be responsible for ensuring that financial regulations are adhered to; please see the end of this page for <a href="<?=site_url('event/climate-challenge-workshop/faq');?>">eligible and ineligible costs</a>. Please check with Prof. Alan Drew if you are unsure.</li>
        </ol>
    </p>
<br> -->

<!-- <h2 class="section-title">Lectures</h2>
<p>
    Capacity building and training objectives will achieved through plenary lectures and training sessions given by experts. Details and a registration link to be announced in due course. This will be open to anyone.
</p> -->