<h2 class="section-title">FAQs</h2>
<div class="col-md-12 text-justify">
    <div class="accordion" id="accordion_list" style="max-width: 100%; min-width: 100%">
        <div class="card block-card">
            <div class="card-header bg-navy" id="publication-0"
                style="display: flex; justify-content: space-between;align-items: center">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#card-0"
                        aria-expanded="true" aria-controls="card-0">
                        What are the assessment criteria for Research Prizes and Dissemination Awards? </button>
                </h2>
                <h2 class="mb-0">
                    <button class="btn btn-link btn-arrow" type="button" data-toggle="collapse" data-target="#card-0"
                        aria-expanded="true" aria-controls="card-0">
                        <i class="fas fa-chevron-down"></i>
                    </button>
                </h2>

            </div>
            <div id="card-0" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body bg-yellow pb-5">
                    <p>Abstracts and projects will be assessed on:</p>
                    <ul>
                        <li>Potential impact (during the length of the project and on a 10 year horizon)</li>
                        <li>Research quality</li>
                        <li>Activities balance</li>
                        <li>Track record</li>
                        <li>Interdisciplinarity</li>
                        <li>Gender, social, economic, sectoral impact</li>
                        <li>ODA eligibility (<a href="#card-1" data-target="#card-1" data-toggle="collapse">see below</a> for more details)</li>
                        <li>Relevance for climate change</li>
                        <li>An existing problem caused by climate change</li>
                        <li>Collaboration between Indonesia and the UK</li>
                    </ul>
                    Further details will be provided in due course.
                </div>
            </div>
        </div>
        <div class="card block-card">
            <div class="card-header bg-navy" id="publication-1"
                style="display: flex; justify-content: space-between;align-items: center">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#card-1"
                        aria-expanded="true" aria-controls="card-1">
                        What is ODA compliance? </button>
                </h2>
                <h2 class="mb-0">
                    <button class="btn btn-link btn-arrow" type="button" data-toggle="collapse" data-target="#card-1"
                        aria-expanded="true" aria-controls="card-1">
                        <i class="fas fa-chevron-down"></i>
                    </button>
                </h2>

            </div>
            <div id="card-1" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body bg-yellow pb-5">
                    Projects and abstracts/talks must be ODA compliant, but there is no single answer (it is complex).
                    When
                    considering ODA compliance, try to answer the following questions:
                    <ul>
                        <li>Is the project addressing the economic development and welfare of the country in question?
                        </li>
                        <li>Are the countries involved on the DAC List of ODA Recipients (the Development Assistant
                            Committee of
                            the OECD)</li>
                        <li>Is there a development need that my project or activity is addressing?</li>
                        <li>Is this credible or is there evidence of the need?</li>
                        <li>How would this project or activity be applied in the country?</li>
                        <li>What would the impact of my project or activity be, and who would benefit?</li>
                        <li>How does my project or activity contribute to sustainable development?</li>
                        <li>Would this lead to a reduction in poverty in a developing country?</li>
                        <li>What would success for this activity look like?</li>
                        <li>How would success or impact be measured?</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card block-card">
            <div class="card-header bg-navy" id="publication-2"
                style="display: flex; justify-content: space-between;align-items: center">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#card-2"
                        aria-expanded="true" aria-controls="card-2">
                        What is an ECR? </button>
                </h2>
                <h2 class="mb-0">
                    <button class="btn btn-link btn-arrow" type="button" data-toggle="collapse" data-target="#card-2"
                        aria-expanded="true" aria-controls="card-2">
                        <i class="fas fa-chevron-down"></i>
                    </button>
                </h2>

            </div>
            <div id="card-2" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body bg-yellow pb-5">
                    ECRs are defined as:
                    <ul>
                        <li>PhD students and research staff with up to 5 years of experience post PhD</li>
                        <li>Staff, qualified to Masters level or equivalent, with up to 8 years of experience.</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card block-card">
            <div class="card-header bg-navy" id="publication-2"
                style="display: flex; justify-content: space-between;align-items: center">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#card-3"
                        aria-expanded="true" aria-controls="card-3">
                        What background are you looking for? </button>
                </h2>
                <h2 class="mb-0">
                    <button class="btn btn-link btn-arrow" type="button" data-toggle="collapse" data-target="#card-3"
                        aria-expanded="true" aria-controls="card-3">
                        <i class="fas fa-chevron-down"></i>
                    </button>
                </h2>

            </div>
            <div id="card-3" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body bg-yellow pb-5">
                    We are targeting both UK and Indonesian based ECRs in order to foster future links between
                    researchers in
                    the two countries. Those bidding for funding must therefore be a resident of the United Kingdom or
                    Indonesia. The background of the participants will be in energy generation and storage and how this
                    impacts
                    on electrification of transport, as well as climate change specific to the region. This includes
                    those in
                    the social and economic sciences, policy work and the broader climate change area, as we aim to
                    operate a
                    balanced programme. We will target those with a research background (in universities, research
                    facilities
                    and industry), and those working in industry, government and NGOs.
                </div>
            </div>
        </div>
        <div class="card block-card">
            <div class="card-header bg-navy" id="publication-2"
                style="display: flex; justify-content: space-between;align-items: center">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#card-4"
                        aria-expanded="true" aria-controls="card-4">
                        How much is available? </button>
                </h2>
                <h2 class="mb-0">
                    <button class="btn btn-link btn-arrow" type="button" data-toggle="collapse" data-target="#card-4"
                        aria-expanded="true" aria-controls="card-4">
                        <i class="fas fa-chevron-down"></i>
                    </button>
                </h2>

            </div>
            <div id="card-4" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body bg-yellow pb-5">
                    <p class="text-justify">
                        The total prize is GBP 30,000 which will be distributed to all the awards/prizes. We  expect roughly the split to be 1/3 Dissemination  Awards and 2/3 Research Prizes, but there is some degree of flexibility. We expect to fund around 5 Dissemination Awards each with a maximum value £2,000, but we note this is cap and not a  target. We also expect to fund 4 Research Prizes with a maximum value £8,000, but it is  likely that only one prize will be awarded at this level.
                    </p>
                </div>
            </div>
        </div>
        <div class="card block-card">
            <div class="card-header bg-navy" id="publication-2"
                style="display: flex; justify-content: space-between;align-items: center">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#card-5"
                        aria-expanded="true" aria-controls="card-5">
                        When will the Workshop be? </button>
                </h2>
                <h2 class="mb-0">
                    <button class="btn btn-link btn-arrow" type="button" data-toggle="collapse" data-target="#card-5"
                        aria-expanded="true" aria-controls="card-5">
                        <i class="fas fa-chevron-down"></i>
                    </button>
                </h2>

            </div>
            <div id="card-5" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body bg-yellow pb-5">
                    12th – 14th July 2021, at a time suitable for both time zones (i.e morning -> mid-afternoon UK;
                    mid-afternoon -> evening Indonesia). The precise programme will be announced in due course.
                </div>
            </div>
        </div>
        <div class="card block-card">
            <div class="card-header bg-navy" id="publication-2"
                style="display: flex; justify-content: space-between;align-items: center">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#card-6"
                        aria-expanded="true" aria-controls="card-6">
                        What are eligible costs? </button>
                </h2>
                <h2 class="mb-0">
                    <button class="btn btn-link btn-arrow" type="button" data-toggle="collapse" data-target="#card-6"
                        aria-expanded="true" aria-controls="card-6">
                        <i class="fas fa-chevron-down"></i>
                    </button>
                </h2>

            </div>
            <div id="card-6" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body bg-yellow pb-5">
                    The money must be spent on activities that address development challenges and therefore be
                    ODA-eligible. The
                    following costs are eligible:
                    <ul>
                        <li>Costs for research and development.</li>
                        <li>Operational costs: Travel costs for visiting and post-workshop activities, Visa fees,
                            vaccinations,
                            and medical insurance for travel essential to collaboration, to the UK and partner
                            countries.</li>
                        <li>Publication costs directly related to the project and the workshop (we particularly
                            encourage open
                            access publishing). </li>
                        <li>Use of telecommunications such as video / audio / web conferencing.</li>
                        <li>New equipment: The purchase of equipment must be subsidiary to the aims of the follow on
                            work, any
                            equipment above the value of £5,000 will need to be signed off by the British Council before
                            any
                            award can be made. Criteria are normally:</li>
                        <ul type="disk">
                            <li>Equipment is essential to delivery of the project and cannot be expected to be provided
                                by
                                institutions.</li>
                            <li>Equipment will be used in the partner country and will remain there on project
                                completion.</li>
                        </ul>
                        <li>Additional consumables for the research.</li>
                    </ul>
                    The British Council does not wish to place any other criteria on eligible costs. Applicants should
                    be clear
                    about what exactly they intend to spend the prize money on; this will be taken into account when
                    reviewing
                    applications.
                    <br>
                    If in doubt, please check before spending or committing to spend money.
                </div>
            </div>
        </div>
        <div class="card block-card">
            <div class="card-header bg-navy" id="publication-2"
                style="display: flex; justify-content: space-between;align-items: center">
                <h2 class="mb-0">
                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#card-7"
                        aria-expanded="true" aria-controls="card-7">
                        What are ineligible costs? </button>
                </h2>
                <h2 class="mb-0">
                    <button class="btn btn-link btn-arrow" type="button" data-toggle="collapse" data-target="#card-7"
                        aria-expanded="true" aria-controls="card-7">
                        <i class="fas fa-chevron-down"></i>
                    </button>
                </h2>

            </div>
            <div id="card-7" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body bg-yellow pb-5">
                    They are:
                    <ul>
                        <li>Costs of staff based in commercial or for-profit organisations.</li>
                        <li>Costs of permanent staff in any organisation.</li>
                        <li>Purchase or rental of standard office equipment (except specialist equipment essential to
                            the
                            research). This includes IT hardware – laptops, personal computers, tablets, smart phones,
                            Mac
                            workstations, computer parts and peripherals, etc. Any standard hardware which would
                            routinely be
                            used by researchers and academics will not be funded.</li>
                        <li>Standard office software.</li>
                        <li>Mobile phone rental or purchase / roaming charges.</li>
                        <li>Tuition Fees.</li>
                        <li>Bench Fees (for example PhD, Masters or Undergraduate study).</li>
                        <li>Attendance at conferences or other events unless this is to present outputs and outcomes of
                            the
                            project (note: Dissemination Awards are eligible).</li>
                        <li>Patents costs.</li>
                        <li>Costs relating to the construction, procurement, or rental of physical infrastructure, (e.g.
                            office
                            buildings, laboratory facilities).</li>
                        <li>Entertainment costs such as gifts or alcohol, excessive restaurant costs, excessive taxi
                            fares</li>
                        <li>Institutional overhead costs</li>
                        <li>Activities which may lead to civil unrest.</li>
                        <li>Activities which discriminate against any group on the basis of age, gender reassignment,
                            disability, race, colour, ethnicity, sex and sexual orientation, pregnancy and maternity,
                            religion
                            or belief.</li>
                        <li>Interest payments or service charge payments for finance leases</li>
                        <li>Statutory fines, criminal fines or penalties.</li>
                        <li>Bad debts to related parties.</li>
                        <li>Payments for unfair dismissal or other compensation.</li>
                        <li>To replace or refund any funds lost to fraud, corruption, bribery, theft, terrorist
                            financing or
                            other misuse of funds.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>