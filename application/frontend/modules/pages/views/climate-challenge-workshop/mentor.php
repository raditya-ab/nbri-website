<h2 class="section-title">Mentors</h2>
<div class="col-md-12 text-justify">
    Mentors will be assigned to projects and help ECRs deliver. This involves turning a 1-page 'good idea' written by
    the ECRs, as part of the application process, into a viable and implementable project, ensuring delivery and impact
    is maximized in the short all the way through the long term.
    <br>
    <section class="content-section about-member">
        <div class="container">
            <div class="row">
                <div class="col-md-12 grid-wrapper">
                    <div class="grid-member-wrapper">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#prof-elizabeth-tanner">
                            <div class="img-wrapper"
                                style="background-image: url(<?php echo base_url('assets/images/prof-elizabeth-tanner.png');?>">
                                <div class="overlay"></div>
                            </div>
                            <div class="caption-wrapper">
                                <h3 class="member-name"> Prof. Elizabeth Tanner OBE FRSE, FRENG</h3>
                                <p class="subtitle">Professor of Materials Science, QMUL</p>
                            </div>
                        </a>
                    </div>
                    <div class="grid-member-wrapper">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#prof-sir-colin-humphreys">
                            <div class="img-wrapper"
                                style="background-image: url(<?php echo base_url('assets/images/prof-sir-colin-humphreys.png');?>">
                                <div class="overlay"></div>
                            </div>
                            <div class="caption-wrapper">
                                <h3 class="member-name">Prof. Sir Colin Humphreys CBE FRS FREng FIMMM FInstP</h3>
                                <p class="subtitle">Professor of Materials Science, QMUL</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-wrapper">
                    
                    <div class="grid-member-wrapper">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#prof-edwin-aldrian">
                            <div class="img-wrapper"
                                style="background-image: url(<?php echo base_url('assets/images/prof-edwin-aldrian.png');?>">
                                <div class="overlay"></div>
                            </div>
                            <div class="caption-wrapper">
                                <h3 class="member-name"> Prof. Edwin Aldrian</h3>
                                <p class="subtitle">Chief Scientist, Agency of Assessment Application
                                    Technology</p>
                            </div>
                        </a>
                    </div>
                    <div class="grid-member-wrapper">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#prof-dr-ir-anne-zulfia">
                            <div class="img-wrapper"
                                style="background-image: url(<?php echo base_url('assets/images/prof-dr-ir-anne-zulfia.png');?>">
                                <div class="overlay"></div>
                            </div>
                            <div class="caption-wrapper">
                                <h3 class="member-name">Prof. Dr. Ir. Anne Zulfia, M.Sc </h3>
                                <p class="subtitle">University of Indonesia</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal modal-member fade" id="prof-elizabeth-tanner" tabindex="-1" role="dialog" aria-labelledby="prof-elizabeth-tanner" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
        <div class="modal-content bg-navy font-color-green"
            style="padding: 50px; position: relative;">
            <div class="modal-body p-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="<?php echo base_url('assets/images/clear_24px.svg" alt="">');?>">
                    </span>
                </button>
                <div class="row">
                    <div class="col-md-4">
                        <div class="image-modal"
                            style="height: 250px; width: 250px; background-image: url(<?=base_url('assets/images/prof-elizabeth-tanner.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                        </div>
                        <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                            Prof. Elizabeth Tanner OBE FRSE, FRENG</h3>
                        <p class="subtitle">Professor of Materials Science, QMUL</p>
                    </div>
                    <div class="col-md-8">
                        <p class="description">
                            <p>Her research focusses is on biomedical engineering solutions to a
                                variety of problems facing developed and developing nations, with
                                interests in international development and climate change. She is a
                                Fellow Royal Academy of Engineering (FREng), Fellow of Royal Society
                                of Edinburgh (FRSE), Fellow of the Institution of Mechanical
                                Engineers (FIMechE), Fellow of the Institute of Materials, Minerals
                                and Mining (FIMMM), Fellow of the Institution of Physics a Chartered
                                Engineer (CEng), Chartered Scientist (CSci) and Trustee of the Royal
                                Academy of Engineering.
                            </p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer no-border p-0">
                <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-member fade" id="prof-sir-colin-humphreys" tabindex="-1" role="dialog" aria-labelledby="prof-sir-colin-humphreys" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
        <div class="modal-content bg-navy font-color-green"
            style="padding: 50px; position: relative;">
            <div class="modal-body p-0">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="<?php echo base_url('/assets/images/clear_24px.svg" alt="">');?>">
                    </span>
                </button>
                <div class="row">
                    <div class="col-md-4">
                        <div class="image-modal"
                            style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof-sir-colin-humphreys.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                        </div>
                        <h3 class="member-name mb-4" style="font-family: 'Sailec Regular'">
                            Prof. Sir Colin Humphreys CBE FRS FREng FIMMM FInstP</h3>
                        <p class="subtitle">Professor of Materials Science, QMUL</p>
                    </div>
                    <div class="col-md-8">
                        <p class="description">
                            <p>His research focuses on materials science of carbons, gallium
                                nitrides and aerospace engineering. He has a long and distinguished
                                career, including many awards and prizes, such as: 1996: elected as
                                a Fellow of the Royal Academy of Engineering; 2000: IOP Kelvin Medal
                                and Prize; 2001: A. A. Griffith Medal and Prize; 2003: CBE for
                                services to science as a researcher and communicator; 2010: knighted
                                in Birthday Honours; 2011 elected a Fellow of the Royal Society;
                                2015: elected as an Honorary Fellow of the Royal Microscopical
                                Society.
                            </p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer no-border p-0">
                <button class="btn btn-readmore bg-yellow m-0" data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-member fade" id="prof-edwin-aldrian" tabindex="-1" role="dialog" aria-labelledby="prof-edwin-aldrian" aria-hidden="true">
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
        <div class="modal-content bg-navy font-color-green"
            style="padding: 50px; position: relative;">
            <div class="modal-body p-0">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <img
                            src="<?php echo base_url('assets/images/clear_24px.svg" alt="">');?>">
                    </span>
                </button>
                <div class="row">
                    <div class="col-md-4">
                        <div class="image-modal"
                            style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof-edwin-aldrian.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                        </div>
                        <h3 class="member-name mb-4"
                            style="font-family: 'Sailec Regular'">
                            Prof. Edwin Aldrian</h3>
                        <p class="subtitle">Chief Scientist, Agency of Assessment
                            Application Technology</p>
                    </div>
                    <div class="col-md-8">
                        <p class="description">
                            <p>He has research expertise in Meteorology and Climatology
                                specific to the SE-Asian region. He is IPCC WG1 Vice
                                Chair representing Indonesia and countries in the
                                Southwest Pacific region and part of IPCC Outreach
                                programme in South Asia and Southeast Asia.
                            </p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer no-border p-0">
                <button class="btn btn-readmore bg-yellow m-0"
                    data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-member fade" id="prof-dr-ir-anne-zulfia" tabindex="-1" role="dialog" aria-labelledby="prof-dr-ir-anne-zulfia" aria-hidden="true">n
    <div class="modal-dialog modal-full modal-dialog-centered" role="document">
        <div class="modal-content bg-navy font-color-green"
            style="padding: 50px; position: relative;">
            <div class="modal-body p-0">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <img
                            src="<?php echo base_url('/assets/images/clear_24px.svg" alt="">');?>">
                    </span>
                </button>
                <div class="row">
                    <div class="col-md-4">
                        <div class="image-modal"
                            style="height: 250px; width: 250px; background-image: url(<?php echo base_url('assets/images/prof-dr-ir-anne-zulfia.png');?>); background-repeat: no-repeat; background-size: cover; background-position: center; margin-bottom: 15px">
                        </div>
                        <h3 class="member-name mb-4"
                            style="font-family: 'Sailec Regular'">
                            Prof. Dr. Ir. Anne Zulfia, M.Sc</h3>
                        <p class="subtitle">University of Indonesia</p>
                    </div>
                    <div class="col-md-8">
                        <p class="description">
                            <p>Prof. Dr. Anne Zulfia, MSc is one of the experts in the
                                field of metalurgy who currently works as a lecturer at
                                the University of Indonesia who is interested in the
                                field of battery development.
                                She has research interests in composite materials,
                                lithium ion batteries and sustainable energy specific to
                                Indonesia.
                            </p>
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer no-border p-0">
                <button class="btn btn-readmore bg-yellow m-0"
                    data-dismiss="modal">Back</button>
            </div>
        </div>
    </div>
</div>