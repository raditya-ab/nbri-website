<section id="page-hero" class="news-detail">
    <div class="container-fluid">
        <div class="row">
            <?php
                $img = ($news_detail['img'] != NULL && $news_detail['img'] != '') ? base_url('assets/images/articles/'.$news_detail['img']) : base_url('assets/images/nbri_new_logo.png');
            ?>
            <div class="col-md-12 hero-container" style="background: url(<?=$img;?>), url(<?=base_url('assets/images/nbri_new_logo.png');?>) center / contain no-repeat;">
            </div>
        </div>
    </div>
</section>
<section class="content-section news-detail-section">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1 class="page-title news-detail"><?=$news_detail['title'];?></h1>
                <?=stripslashes(html_entity_decode($news_detail['content']));?>
            </div>
            <div class="col-md-3">
                <table>
                    <tbody>
                        <tr>
                            <td width="110">Date</td>
                            <td>:</td>
                            <td><?=date('d F Y', strtotime(@$news_detail['created']));?></td>
                        </tr>
                        <tr rowspan="2">
                            <td width="110">Written by</td>
                            <td>:</td>
                            <td><?=@$author_detail['first_name'].' '.@$author_detail['last_name'];?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Share Article</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="share-wrapper" style="margin-top: 10px;width: 100%">
                                    <a target="_blank" class="mr-2" href="https://www.facebook.com/sharer/sharer.php?u=<?=urlencode(is_https() ? "https://" : "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/facebook.png');?>" alt="" width="25"></a>
                                    <a target="_blank" class="mr-2"href="mailto:?subject=Check this publication article from NBRI about <?=(array_key_exists('title', $news_detail)) ? $news_detail['title'] : $news_detail['category_name'];?>&body=This is the url: <?php echo is_https() ? "https://" : "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"><img class="social-img" src="<?=base_url('assets/images/footer-assets/mail.png');?>" alt="" width="25"></a>
                                    <a target="_blank" class="mr-2" href="http://twitter.com/share?text=Check this publication article from NBRI about <?=(array_key_exists('title', $news_detail)) ? $news_detail['title'] : $news_detail['category_name'];?>&url=<?php echo is_https() ? "https://" : "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"><img class="social-img" src="<?=base_url('assets/images/footer-assets/twitter.png');?>" alt="" width="25"></a>
                                    <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo urlencode(is_https() ? "https://" : "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);?>"><img class="social-img" src="<?=base_url('assets/images/footer-assets/linkedin.png');?>" width="25"></a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12"><hr></div>
    </div>
</div>
<div class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-title">Other Articles</h2>
            </div>
        </div>
        <div class="row">
            <?php if(!empty($news_list)): ?>
            <div class="col-md-12 grid-wrapper justify-content-around">
                <?php foreach($news_list as $news): ?>
                    <?php
                        $content = strip_tags(stripslashes(html_entity_decode($news['content'])));
                        $img = ($news['img'] != NULL && $news['img'] != '') ? base_url('assets/images/articles/'.$news['img']) : base_url('assets/images/nbri_new_logo.png');
                    ?>
                    <div class="grid-article-wrapper">
                        <div class="img-wrapper" style="background-image: url(<?=$img;?>), url(<?=base_url('assets/images/nbri_new_logo.png');?>)"></div>
                        <div class="caption-wrapper text-left">
                            <h4 class="article-title"><?=word_limiter($news['title'],7);?></h4>
                            <p class="article-description"><?=word_limiter($content,15);?></p>
                        </div>
                        <div class="readmore-wrapper">
                            <a href="<?=site_url('event/'.$news['category_slug'].'/news/'.$news['slug']);?>" class="readmore" title="">
                                <span>Read More</span>
                                <i class="fas fa-arrow-right"></i>
                            </a>

                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php else: ?>
                <div class="col-md-12">
                    <h4>There's no other article yet.</h4>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>