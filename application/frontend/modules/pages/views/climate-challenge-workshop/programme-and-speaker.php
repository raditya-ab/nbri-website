<p>We are currently arranging the programme  and this section will be updated once this has been completed.</p>
<h2 class="section-title">Programme</h2>
<div class="row">
    <div class="col-md-12">
    <p class="text-justify">
        The workshop will deliver a balanced programme specifically aimed at early career researchers (ECRs) from Indonesia and the UK, and involve sessions in four areas:
        <ol>
            <li>Research Prizes : ECRs will be able  to submit research proposals to bid for prizes of up to £8,000, to undertake a piece of original research involving batteries, climate change and Indonesia. For further information please see <a href="#">Research Prizes</a></li>
            <li>Dissemination Awards : ECRs will be able to submit an abstract to present their research at the Workshop and bid for funding to attend a climate change conference. For further information please see <a href="#">Dissemination Awards</a>.</li>
            <li>Lectures : Capacity building and training will achieved through plenary lectures and training sessions given by experts.</li>
            <li>Breakout sessions : to enable participants to network informally after the formal has finished for the day.</li>
        </ol>
    </p>
    </div>
</div>
<!-- <h2 class="section-title">Background</h2> -->
<!-- <div class="col-md-12 text-justify">The aim of this Climate Challenge Workshop is to harness the power of people
    all over the world – particularly young people and those most vulnerable to the effects of climate change –
    to connect and collaborate through culture and education to combat climate change.
    This workshop will explain the causes and the role that Indonesia has on climate change,
    locally and globally, across different economic sectors (e.g transport, energy, industry, tourism, household)
    as well as the social and economic impact that climate change will have on Indonesia.
    It will address the solutions, whether technological or socioeconomic, by addressing the following themes:
    <br>
    <ol>
        <li>Climate change specific to Indonesia - causes and effects.</li>
        <li>Technological solutions that combine renewable energy (solar, wind, hydro) with batteries,
            put into the context of a battery transport revolution in Indonesia </li>
        <li>Socioeconomic, policy and financial barriers to climate change solutions in Indonesia</li>
    </ol>
    It will deliver a balanced programme specifically aimed at early career researchers (ECRs)
    from Indonesia and the UK, with the following objectives:
    <ol>
        <li>Enable networking and mutual research exchange between ECRs from the UK and Indonesia,
            via the effective use of online platforms (online profiles, speed networking, break-out sessions)
            and post-Workshop activities.</li>
        <li>Deliver a climate change workshop containing a mixed programme of ECR talks, plenaries, breakout
            sessions and training sessions</li>
        <li>Train ECRs in contemporary issues around climate change, with a balanced programme including the
            underlying scientific causes and effects of climate change, potential technological solutions and
            the broader socioeconomic consequences.</li>
        <li>Identify the top climate-range ECRs in Indonesia and the UK,
            and award them Dissemination Awards and Research Prizes (see below).</li>
        <li>Mentor ECRs prior to the workshop to upskill and increase their capacity
            to bid for the Research Prizes, and continue that mentor relationship during the delivery of the projects.
        </li>
    </ol>
</div> -->