<div class="row">
    <div class="col-md-12">
        <h2 class="section-title">Introduction</h2>
        <p>
            The workshop is divided into three types of participants - those applying for Research Prizes (an
            independent research prize; presentation at the Workshop), those applying for a Dissemination Award (a
            travel grant to attend a Climate Change conference; presentation at the Workshop) and to attend the lectures
            (no presentation). These, along with assessment criteria and FAQs, are outlined below.
        </p>
    </div>
</div>
<div class="row mt-3">
    <div class="col">
        <h5>Research Prizes</h5>
        <p>
            Template CV: <a download href="<?php echo base_url('assets/file/template-cv-climate-challenge_rev.pdf');?>">Download here</a><br>
            Author Guidelines: <a download href="<?php echo base_url('assets/file/author-guidelines.pdf');?>">Download here</a><br>
            Register Now: <a href="http://bit.ly/CCResearchPrize" target="_blank">Go to registration page</a><br>
            Proposal Template: <a download href="<?php echo base_url('assets/file/climate-challenge-proposal-template.pdf');?>">Download here</a><br>
    </div>
</div>
<div class="row mt-3">
    <div class="col">
        <h5>Dissemination Awards</h5>
        <p>
            Register Now: <a href="http://bit.ly/CCDisseminationAward" target="_blank">Go to registration page</a><br>
    </div>
</div>
<div class="row mt-3">
    <div class="col">
        <h5>Join the Conference</h5>
        <p>
            Register Now: <a href="https://bit.ly/CCW_Attendance" target="_blank">Go to registration page</a><br>
    </div>
</div>
<!-- <div class="row mt-3">
    <h5>Dissemination Awards</h5>
    <div class="col-md-12 text-center">
        <div class="row">
            <div class="col">
                <p class="mb-0">
                    <b>Download Research Prize Application Form</b>
                </p>
                <a class="btn btn-navy btn-readmore m-2" href="<?php echo base_url('assets/file/research-prize-application-form.pdf');?>" download>Download PDF Form</a>
            </div>
            <div class="col">
                <p class="mb-0">
                    <b>Download Dissemination Awards Application Form</b>
                </p>
                <a class="btn btn-navy btn-readmore m-2" href="<?php echo base_url('assets/file/dissemination-prize-application-form.pdf');?>" download>Download PDF Form</a>
            </div>
            <div class="col">
                <p class="mb-0">
                    <b>Download Template CV Form</b>
                </p>
                <a class="btn btn-navy btn-readmore m-2" href="<?php echo base_url('assets/file/template-cv-climate-challenge.pdf');?>" download>Download PDF Form</a>
            </div>
        </div>
    </div>
</div> -->
<!-- <div class="row mt-3">
    <div class="col-md-4 text-center">
        <p class="mb-0">
            <b>Download Template CV Form</b>
        </p>
    </div>
    <div class="col-md-4 text-center">
        
        <a class="btn btn-navy btn-readmore m-2"
            href="<?php echo base_url('assets/file/template-cv-climate-challenge.pdf');?>" download>Download PDF
            Form</a>
    </div>
    
</div> -->