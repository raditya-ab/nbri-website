<h2 class="section-title">Climate Challenge Research Prizes</h2>
<div class="col-md-12 text-justify">
    <p>
        This research program is designed for Early Career Researcher who hold citizens and resident of United Kingdom or Indonesia. The participant must be PhD student or a research staff with maximum of 5 years post PhD experience or staff qualified to Master level or equivalent with maximum of 8 years experiences. The Prizes of up to £8,000 each are available, and we expect to be able to fund around 4 prizes.
    </p>
    <p>
        These challenge research prizes are aimed at enabling ECRs to undertake a piece of original research involving batteries, climate change and Indonesia. Further details on remit can be found in the About section <a
                href="<?=site_url('event/climate-challenge-workshop/about');?>">here</a>. This is open to teams of up to 3 ECRs, where the primary investigator (PI) must be from Indonesia (with a preferred international team) and be employed (i.e the PI cannot be a PhD student, but a PhD student can be on the team). The process is as follows:
    </p>
    <br>
    <ol>
        <li>Please note that your personal details and research interests will be circulated to everyone who has
            expressed an interest to attend the networking event; if you do not want this to happen, please ensure you
            indicate this on the form. Whilst it is still possible to apply for funding without attending the networking
            event or having your details circulated, we note that this might lower the quality of the application and
            therefore lower the chances of success.</li>
        <li>You will then receive an invitation to a networking event in early May, that will help you form teams of
            people to apply for the fund.</li>
        <li>Once the networking event is finished, you will be sent an application form to fill out, along with further
            instructions on how to make the full application for funding.</li>
        <li> The proposal should be submitted before the <b>22th June 2021</b> deadline, using the instructions and
            application supplied after the networking event.</li>
        <li> Further details on remit can be found in the About section <a
                href="<?=site_url('event/climate-challenge-workshop/about');?>">here</a> areas that we will consider for
            assessment are listed below and <a href="<?=site_url('event/climate-challenge-workshop/faq');?>">terms &
                conditions</a>.</li>
        <li> You will be informed of being selected to give a talk at the workshop to present your ideas for a research
            project (and guidelines for speaking) on the 4th July 2021.</li>
        <li> You will give a presentation about your proposed project at the workshop following the guidelines. A panel
            of experts will then select the projects to be funded from those that gave the presentation. Candidates will
            be informed at a ceremony at the end of the workshop, and be expected to start the work no later than 1st
            September 2021. The projects must be finished by 31st March 2022 (i.e all monies spent and claimed for).
        </li>
        <li> You will be assigned a mentor, whose role will be to track the project to ensure delivery, and ensure
            finances are on track (not over or under spent).</li>
        <li> You will be responsible for preparing quarterly reports on progress (i.e end of December and end of March).
        </li>
        <li> You will be responsible for ensuring that financial regulations are adhered to; please see the end of this
            page for <a href="<?=site_url('event/climate-challenge-workshop/faq');?>">eligible and ineligible costs</a>.
            If you are successful, please check with Prof. Alan Drew if you are unsure.</li>
        <li>When drawing up the budget for the proposal, please see the <a
                href="<?=site_url('event/climate-challenge-workshop/faq');?>">FAQs</a> for eligible and ineligible costs
        </li>
        <li>In order to be eligible for funding, you must attend the workshop and give a presentation on your proposed
            research. This is a vital part of the assessment process and you will be ineligible if you do not attend.
        </li>
    </ol>
    <p class="mt-3">
        Register your interest <a
            href="<?=site_url('event/climate-challenge-workshop/registration-application');?>">here</a>
    </p>
</div>