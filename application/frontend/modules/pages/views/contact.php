<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(https://faraday.ac.uk/wp-content/uploads/2019/04/Alireza-Rastegarpanah-2-resized.jpg)">
                <h1 class="page-title">About NBRI</h1>
            </div>
        </div>
    </div>
</section>

<section class="content-section" style="padding-bottom: 20px">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div style="width: 100%"><iframe width="100%" height="482px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=500&amp;hl=en&amp;q=<?php echo $website_var['lat'].','.$website_var['lng'];?>&amp;t=&amp;z=16&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe></div>
            </div>
            <div class="col-md-6 d-flex align-items-end">
                <div class="wrapper font-color-navy">
                    <h3 class="section-title font-bold font-24"><?=$website_var['website_name'];?></h3>
                    
                    <ul class="p-0 list-none">
                        <li>
                            <address>
                                <?=$website_var['address'];?>
                                <br>
                                <br>
                            </address>
                        </li>
                        <li>Tel: <?=$website_var['phone'];?></li>
                        <li>Fax: <?=$website_var['phone'];?></li>
                        <li>Email: <?=$website_var['email'];?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12"> <hr>    </div>
    </div>
</div>
<section class="content-section" style="padding-top: 40px">
    <style>
        .contact-container{
            max-width: 624px;
        }
        .form-contact input.form-control
        {
            height: 42px;
            border-radius: 0;
        }
    </style>
    <div class="container contact-container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="section-title mb-4">Email Us</h3>
                <p class="section-subtitle">Lorem ipsum dolor sit amet consectetur adipisicing elit. Non, minus?</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="" method="POST" role="form" class="form-contact mt-5">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="" placeholder="Input field">
                        </div>
                         <div class="form-group col-md-6">
                            <input type="text" class="form-control" id="" placeholder="Input field">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <input type="text" class="form-control" id="" placeholder="Input field">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <textarea name="    " id="input    " class="form-control" rows="10" required="required"></textarea>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-readmore bg-navy m-0">Submit</button>
                </form>
            </div>
        </div>
    </div>
</section>
