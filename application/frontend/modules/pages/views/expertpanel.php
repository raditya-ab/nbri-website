<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7 "> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Be</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- FONTS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,700italic,900'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Orbitron:300,400,400italic,700,700italic,900'>

    <!-- CSS -->
    <link rel='stylesheet' href='assets/css/global.css'>
    <link rel='stylesheet' href='assets/css/structure.css'>
    <link rel='stylesheet' href='assets/css/space.css'>
    <link rel='stylesheet' href='assets/css/custom.css'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>

</head>
<body class="layout-full-width color-custom header-stack minimalist-header sticky-header sticky-white no-content-padding subheader-both-center footer-copy-center">
<!-- <body class="page-parent layout-full-width mobile-tb-left button-stroke no-content-padding header-transparent header-fw minimalist-header sticky-header sticky-white ab-hide subheader-both-center menuo-right menuo-no-borders footer-copy-center"> -->
    <!-- Main Theme Wrapper -->
    <div id="Wrapper">
        <!-- Header Wrapper -->
        <div id="Header_wrapper" style="background-image:url(assets/images/home_space_subheader2.jpg);">
            <!-- Header -->
            <header id="Header" style="background: #FFF !important">
                <!-- Header Top -  Info Area -->
                <!-- <div id="Action_bar">
                    <div class="container">
                        <div class="column one">

                            <ul class="contact_details">
                                <li class="slogan">
                                    Have any questions?
                                </li>
                                <li class="phone">
                                    <i class="icon-phone"></i><a href="tel:+61383766284">+61 383 766 284</a>
                                </li>
                                <li class="mail">
                                    <i class="icon-mail-line"></i><a href="mailto:noreply@envato.com">noreply@envato.com</a>
                                </li>
                            </ul>

                            <ul class="social">
                                <li class="skype">
                                    <a href="#" title="Skype"><i class="icon-skype"></i></a>
                                </li>
                                <li class="facebook">
                                    <a href="https://www.facebook.com/Beantown-Themes-653197714728193" title="Facebook"><i class="icon-facebook"></i></a>
                                </li>
                                <li class="googleplus">
                                    <a href="https://plus.google.com/" title="Google+"><i class="icon-gplus"></i></a>
                                </li>
                                <li class="twitter">
                                    <a href="#" title="Twitter"><i class="icon-twitter"></i></a>
                                </li>
                                <li class="vimeo">
                                    <a href="https://vimeo.com/" title="Vimeo"><i class="icon-vimeo"></i></a>
                                </li>
                                <li class="youtube">
                                    <a href="#" title="YouTube"><i class="icon-play"></i></a>
                                </li>
                                <li class="flickr">
                                    <a href="https://www.flickr.com/" title="Flickr"><i class="icon-flickr"></i></a>
                                </li>
                                <li class="pinterest">
                                    <a href="https://www.pinterest.com/" title="Pinterest"><i class="icon-pinterest"></i></a>
                                </li>
                                <li class="dribbble">
                                    <a href="https://dribbble.com" title="Dribbble"><i class="icon-dribbble"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <!-- Header -  Logo and Menu area -->
                <div id="Top_bar" style="background:#FFF !important">
                    <div class="container">
                        <div class="column one">
                            <div class="top_bar_left clearfix">
                                <!-- Logo-->
                                <div class="logo">
                                    <a id="logo" href="<?=base_url();?>" title="National Battery Research Institute"><img class="scale-with-grid" src='<?=base_url("assets/images/logo_size_invert.jpg");?>' alt="National Battery Research Institute" />
                                    </a>
                                </div>
                                <!-- Main menu-->
                                <div class="menu_wrapper">
                                    <nav id="menu">
                                        <ul id="menu-main-menu" class="menu">
                                            <!-- <li class="current-menu-item"> -->
                                            <li>
                                                <a href="<?=site_url('research');?>"><span>RESEARCH</span></a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="<?=site_url('research/lithiumion');?>"><span>Lithium Ion</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('research/beyondlithiumion');?>"><span>Beyond Lithium Ion</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('research/industrysprints');?>"><span>Industry Sprints</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('batterycharacterisation');?>"><span>Battery Characterisation</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('research/community');?>"><span>Research Community</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('research/highlight');?>"><span>Research Highlights</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="<?=site_url('publication');?>"><span>PUBLICATION</span></a>
                                            </li>
                                            <li>
                                                <a href="<?=site_url('opportunities');?>"><span>OPPORTUNITIES</span></a>
                                            </li>
                                            <li>
                                                <a href="<?=site_url('educationskill');?>"><span>EDUCATION &amp; SKILLS</span></a>
                                            </li>
                                            <li>
                                                <a href="<?=site_url('articles');?>"><span>NEWS ARTICLES</span></a>
                                            </li>
                                        </ul>
                                    </nav><a class="responsive-menu-toggle" href="#"><i class="fas fa-bars"></i></a>
                                </div>
                                <!-- Secondary menu area - only for certain pages -->
                                <div class="secondary_menu_wrapper">
                                    <nav id="secondary-menu" class="menu-secondary-menu-container">
                                        <ul id="menu-secondary-menu" class="secondary-menu">
                                            <li class="menu-item-1568">
                                                <a href="<?=base_url();?>"><u>HOME</u></a>
                                            </li>
                                            <li class="menu-item-1574">
                                                <a href="<?=site_url('about');?>">ABOUT</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item-1569">
                                                        <a href="<?=site_url('about');?>#team">Team</a>
                                                    </li>
                                                    <li class="menu-item-1570">
                                                        <a href="<?=site_url('about');?>#values">Our Values</a>
                                                    </li>
                                                    <li class="menu-item-1571">
                                                        <a href="<?=site_url('expert_panel');?>">Expert Panel</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-1583">
                                                <a target="_blank" href="<?=site_url('contact');?>">CONTACT</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>

                                <!-- Header Searchform area-->
                                <div class="search_wrapper">
                                    <form method="get" action="#">
                                        <i class="icon_search icon-search"></i><a href="#" class="icon_close"><i class="fas fa-search"></i></a>
                                        <input type="text" class="field" name="s" placeholder="Enter your search" />
                                    </form>
                                </div>
                            </div>
                            <div class="top_bar_right">
                                <div class="top_bar_right_wrapper">
                                    <a id="search_button" href="#"><i class="fas fa-search"></i> &nbsp;</a>
                                    <!--wpml flags selector-->
                                    <div class="wpml-languages enabled">
                                        <a class="active tooltip" href="#" data-tooltip="No translations available for this page">
                                            <img src='<?=base_url("assets/images/flags/en.png");?>' alt="English" />&nbsp;<i class="fas fa-caret-down"></i>
                                        </a>
                                        <ul class="wpml-lang-dropdown">
                                            <li>
                                                <a href="#"><img src='<?=base_url("assets/images/flags/id.png");?>' alt="Bahasa" />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!--Subheader area - only for certain pages -->
            <div id="Subheader">
                <div class="container">
                    <div class="column one">
                        <h1 class="title">Expert Panel</h1>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main Content -->
        <div id="Content">
            <div class="content_wrapper clearfix">
                <div class="sections_group">
                    <div class="entry-content">
                        <div class="section" style="padding-top:70px; padding-bottom:30px; ">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- One Second (1/2) Column -->
                                    <div class="column one column_column">
                                        <h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum fugit sequi, nostrum ad vero sed aspernatur magnam at dolorum corrupti.</h5>
                                        <p class="big">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis magni blanditiis aliquid corporis, odio sit. Neque ut, amet fugiat fuga cupiditate, omnis recusandae nisi enim explicabo dolores incidunt asperiores voluptates veritatis minus rem atque. Recusandae doloremque libero illum, ut odit nesciunt quae quas incidunt maiores laboriosam esse ad deserunt, enim nemo quidem! Minima sapiente quo esse magnam fuga molestias maxime, corrupti ullam facilis ad voluptates, ipsa excepturi, quia suscipit facere asperiores! Repudiandae mollitia officiis fugit odit harum nemo, illum expedita porro, cupiditate asperiores incidunt dolores, eos voluptas natus possimus autem totam laudantium. Rerum temporibus, minus perferendis voluptatum quisquam quae tempore ipsum ea. Esse earum voluptatem, vitae expedita dolor ipsum odit quasi nisi maxime officiis velit, voluptatum. Nostrum nam iure dicta illum, quaerat accusantium mollitia voluptatum alias veniam dolorem quas necessitatibus rerum ad amet commodi quos ratione eligendi et ex facilis rem excepturi fugiat delectus eos quo. Laboriosam consectetur beatae obcaecati saepe nisi itaque vitae vero magni, error, cum atque fugiat impedit illo perspiciatis facere, rerum iusto. Qui, veritatis? Eius, accusamus, modi. Atque blanditiis voluptas quasi cum, non voluptates aut iure totam quibusdam ad. Quo ipsum tenetur, eius enim iusto atque iure dolorum consectetur quibusdam asperiores velit facilis doloremque. Quam distinctio unde expedita beatae dolores, deserunt consequuntur debitis voluptas id, commodi optio nostrum in maiores? Minus sapiente ut aut dolorum, voluptatibus sed quia suscipit aliquid pariatur tempore commodi, earum eveniet soluta quos, molestiae, adipisci impedit enim omnis nisi explicabo! Atque fugiat tempore debitis ut, eveniet repellendus officia voluptates qui quisquam recusandae nam dicta maxime, ea ipsam odit consequuntur minus impedit. Tempora dolor laboriosam nihil quibusdam fugit quod illum eos impedit, soluta doloribus architecto, officia numquam fugiat, non ea. Placeat saepe amet, eius architecto laborum molestias non itaque quod pariatur sit totam, assumenda fugiat dolorem expedita facilis. Aliquid unde rem magni iure.</p>
                                    </div>
                                </div>
                                <div class="items_group clearfix">
                                    <div class="column one-fourth column_column">
                                        <div class="photo_box">
                                            <div class="image_frame">
                                                <div class="image_wrapper">
                                                    <a href="#">
                                                        <div class="mask"></div><img class="scale-with-grid" src="https://via.placeholder.com/200x200" alt="img">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="desc">
                                                Vitae adipiscing turpis. Aenean ligula nibh in, molestie id viverra a, dapibus at dolor.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column one-fourth column_column">
                                        <div class="photo_box">
                                            <div class="image_frame">
                                                <div class="image_wrapper">
                                                    <a href="#">
                                                        <div class="mask"></div><img class="scale-with-grid" src="https://via.placeholder.com/200x200" alt="img">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="desc">
                                                Vitae adipiscing turpis. Aenean ligula nibh in, molestie id viverra a, dapibus at dolor.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column one-fourth column_column">
                                        <div class="photo_box">
                                            <div class="image_frame">
                                                <div class="image_wrapper">
                                                    <a href="#">
                                                        <div class="mask"></div><img class="scale-with-grid" src="https://via.placeholder.com/200x200" alt="img">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="desc">
                                                Vitae adipiscing turpis. Aenean ligula nibh in, molestie id viverra a, dapibus at dolor.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column one-fourth column_column">
                                        <div class="photo_box">
                                            <div class="image_frame">
                                                <div class="image_wrapper">
                                                    <a href="#">
                                                        <div class="mask"></div><img class="scale-with-grid" src="https://via.placeholder.com/200x200" alt="img">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="desc">
                                                Vitae adipiscing turpis. Aenean ligula nibh in, molestie id viverra a, dapibus at dolor.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="section the_content no_content">
                            <div class="section_wrapper">
                                <div class="the_content_wrapper"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            a.social-icon{
                padding-left: 5px;
                padding-right: 5px;
            }
        </style>
        <!-- Footer-->
        <footer id="Footer" class="clearfix">
            <!-- Footer - First area -->
            <div class="footer_action">
                <div class="container">
                    <!-- One full width row-->
                    <div class="column one column_column">
                        <h4 style="color: #a6a6a8;">News Feed / Social Media</h4>
                    </div>
                    <div class="column one column_column" style="padding-top: 0">
                        <div class="items_group clearfix">
                            <a href="" class="social-icon" title=""><i class="fas fa-2x fa-envelope-square"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-facebook-square"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-instagram-square"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-twitter-square"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-linkedin"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-youtube-square"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widgets_wrapper">
                <div class="container">
                    <!-- One Fourth (1/4) Column -->
                    <div class="column one-second">
                        <!-- Meta Links Area -->
                        <img class="scale-with-grid" src="assets/images/logo_size_invert.jpg" alt="BeTheme - Best Html Theme Ever" />
                        <br><br>
                        <h4>National Battery Research Institute (NBRI)</h4>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum sequi a magni cupiditate maiores hic perferendis iure eum amet asperiores! Tempora deserunt accusantium pariatur dolore aliquid ut impedit assumenda molestiae.</p>
                    </div>
                    <div class="column one-fourth"> &nbsp;</div>
                    <!-- One Fourth (1/4) Column -->
                    <div class="column one-fourth">
                        <h4>Subcribe</h4>
                        <form class="form" action="homepage_submit" method=" " accept-charset="utf-8">
                            <input type="text" class="form-control" name="" value="" placeholder="youremail@domain.com" style="background-color: transparent;border-bottom: 1px solid">
                            <button class="btn btn-custom" style="background-color: #D0011B"> Subscribe</button>
                        </form>
                        <ul>
                            <li><i class="fas fa-home"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, asperiores.</li>
                            <li><i class="fas fa-phone"></i> 020 202002020</li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
            <style>
                .footer-nav-link a{
                    padding-left: 5%;
                    padding-right: 5%;
                }
            </style>
            <div class="widgets_wrapper">
                <div class="container footer-nav-link" style="text-align: center">
                    <a href="" class="footer-nav-link" title="">Home</a>
                    <a href="" class="footer-nav-link" title="">About</a>
                    <a href="" class="footer-nav-link" title="">Contact</a>
                </div>
            </div>
            <!-- Footer copyright-->
            <div class="footer_copy">
                <div class="container">
                    <div class="column one">

                        <div class="copyright">
                            &copy; 2020 National Battery Research Institute - HTML by <a target="_blank" rel="nofollow" href="http://www.raditya.site/">Raditya</a>
                        </div>
                        <!--Social info area-->
                        <ul class="social"></ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- JS -->
    <script src="assets/js/jquery-2.1.4.min.js"></script>

    <script src="assets/js/mfn.menu.js"></script>
    <script src="assets/js/jquery.plugins.js"></script>
    <script src="assets/js/jquery.jplayer.min.js"></script>
    <script src="assets/js/animations/animations.js"></script>
    <script src="assets/js/scripts.js"></script>

    <script>
        jQuery(window).load(function() {
            var retina = window.devicePixelRatio > 1 ? true : false;
            if (retina) {
                var retinaEl = jQuery("#logo img.logo-main");
                var retinaLogoW = retinaEl.width();
                var retinaLogoH = retinaEl.height();
                retinaEl.attr("src", "images/retina-space.png").width(retinaLogoW).height(retinaLogoH);
                var stickyEl = jQuery("#logo img.logo-sticky");
                var stickyLogoW = stickyEl.width();
                var stickyLogoH = stickyEl.height();
                stickyEl.attr("src", "images/retina-space.png").width(stickyLogoW).height(stickyLogoH);
                var mobileEl = jQuery("#logo img.logo-mobile");
                var mobileLogoW = mobileEl.width();
                var mobileLogoH = mobileEl.height();
                mobileEl.attr("src", "images/retina-space-responsive.png").width(mobileLogoW).height(mobileLogoH);
            }
        });
    </script>
</body>

</html>