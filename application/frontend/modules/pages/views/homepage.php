<section id="homepage-content-1" class="content-section bg-yellow">
    <div class="container">
        <div class="row">
            <div class="col-md-6 pb-5">
                <h2 class="section-title title-lg">About NBRI</h2>
                <!-- <p class="section-subtitle">
                    <strong>
                        National Battery Research Indonesia (NBRI) is Indonesia independent institute for electrochemical energy storage research and skills development.
                    </strong>
                </p>
                <p>
                    NBRI aims to contribute to the overall research capacity and training environment in Indonesia in battery research. We bring together scientists and industry partners on research projects to reduce battery cost, weight, and volume; to improve performance and reliability; and to develop whole-life strategies including recycling and reuse.
                </p>
                <p>
                    The National Battery Research Institute (NBRI) was founded by E.Kartini in 2012. Launched in October 2019 during the ICAMT-ICMR conference 2019 in Bogor held by the Material Research Society of Indonesia (MRS-INA). Bringing together expertise from universities and industry, the NBRI endeavours to make the Indonesia the go-to place for the research, development, manufacture and production of new electrical storage technologies for both the electronics, automotive industries and the wider. The NBRI bench marked of the Faraday Institution in UK. Its establishing is supported by the Global Challenge Research Fund (GCRF), UK, through the cooperation with Queen Mary University of London (QMUL) in 2020.

                </p> -->
                <?php
                    $content = stripslashes(html_entity_decode($pages_section['homepage-about']));

                    echo $content;
                ?>
                <!-- <p>
                The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education. NBRI aims to contribute to the overall research capacity and training environment in Indonesia in Battery Research. NBRI is a platform that brings together scientists, academicians, industry partners, the government and all stakeholders that focus on battery technology. The main goal of NBRI is to encourage and support a battery manufacturing industry using locally resources, which will enable Indonesia to be independent in energy. The NBRI was supported by the UK Government’s Global Challenge Research Fund (GCRF), as part of the Queen Mary University of London QR allocation.
                </p> -->
                <a href="<?php echo site_url('about') ?>" class="btn btn-readmore btn-navy">Read More</a>
            </div>
            <div class="col-md-6 pb-5">
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <div class="img-figure" style="background-image: url(<?php echo base_url('assets/images/home-about.JPG');?>)"></div>
                    </div>
                </div>
                <div class="row what-we-do">
                    <div class="col-md-5">
                        <h2 class="section-title title-sm">What We Do</h2>
                    </div>
                    <div class="col">
                        <ul>
                            <li>Mapping of Indonesian activities: Human resources and capabilities, research facilities, local material/mineral resources, and industrial activities.</li>
                            <li>Develop a batteries manufacturing industry using locally sourced resources, which will enable Indonesia to be independent in energy.</li>
                            <li>As a forum to help build networking with other parties with similar projects throughout the world.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="article-slider-section" class="content-section bg-image">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mt-5">
                <div id="headline-article">
                    <span id="headline-date"><?=date('d F Y', strtotime($article_list[0]['created']));?></span>
                    <h2 id="headline-title"><?=strip_tags(stripslashes($article_list[0]['title']));?></h2>
                    <p id="headline-content" class="headline-content"><?=word_limiter(strip_tags(stripslashes(html_entity_decode($article_list[0]['content']))),20);?></p>

                    <a href="<?=site_url($article_list[0]['category_slug'].'/'.$article_list[0]['slug']);?>" class="btn btn-readmore btn-yellow">Read More</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="article-wrapper bg-navy p-5">
                    <?php foreach($article_list as $i => $article): ;?>
                        <?php if($i == 0) continue; ?>
                            <div class="article-list" style="min-height: 74px; margin-bottom: 30px">
                                <a href="<?=site_url($article['category_slug'].'/'.$article['slug']);?>">
                                    <span class="article-date"><?=date('d F Y', strtotime($article['created']));?></span>
                                    <h2 class="article-title"><?=strip_tags(stripslashes($article['title']));?></h2>
                                    <p class="article-content"><?=word_limiter(strip_tags(stripslashes(html_entity_decode($article['content']))),20);?></p>
                                </a>
                            </div>
                            <?php if($i == 3) break; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="counter-section" class="content-section">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h3 class="section-title">Indonesia Primed for a Battery Revolution</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 text-center">
                <h4 class="counter-digit counter">14</h4>
                <span class="counter-title">Countries</span>
                <!-- <p class="counter-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, sed.</p> -->
            </div>
            <div class="col-md-3 text-center">
                <h4 class="counter-digit counter">10</h4>
                <span class="counter-title">Articles</span>
                <!-- <p class="counter-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, hic.</p> -->
            </div>
            <div class="col-md-3 text-center">
                <h4 class="counter-digit counter">6</h4>
                <span class="counter-title">Projects</span>
                <!-- <p class="counter-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, eos.</p> -->
            </div>
            <div class="col-md-3 text-center">
                <h4 class="counter-digit counter">7</h4>
                <span class="counter-title">Years of Experience</span>
                <!-- <p class="counter-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident, eos.</p> -->
            </div>
        </div>
    </div>
</section>
<section id="one-map-section" class="content-section bg-image">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h3 class="section-title">NBRI One Map</h3>
                <div style="width: 100%; margin-top: 50px;margin-bottom: 50px">
                    <img src="<?=base_url('assets/images/icon_map.png');?>" alt="NBRI - One Map" width="100">
                </div>
                <!-- <p class="section-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. At consectetur sequi velit sapiente sed hic explicabo officiis magni minima nesciunt?</p> -->
                <a href="<?=site_url('nbri-onemap');?>" class="btn btn-readmore bg-green">Open NBRI One Map</a>
            </div>
        </div>
    </div>
</section>
<section id="research-section" class="content-section bg-yellow">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h3 class="section-title" style="margin-bottom: 65px">Research Projects</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="icons-grid">
                    <?php $index = 1; ?>
                    <?php foreach($research_list as $i => $research): ?>
                        <?php $img = ($research['thumb_img'] != NULL && !empty($research['thumb_img'])) ? base_url('assets/images/research/'.$research['thumb_img']) : 'https://via.placeholder.com/120x120'; ?>
                    <div class="icon-wrapper" style="width: 150px">
                        <a href="<?php echo site_url('research/'.$research['slug']); ?>">
                            <div class="icon-image icon-div" style="background-image: url(<?=$img;?>)"></div>
                            <h5 class="research-title"><?=$research['category_name'];?></h5>
                        </a>
                    </div>
                    <?php if($index>0 && ($index%4 == 0)): ?> <div class="break"></div><?php   endif; ?>
                    <?php $index++; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo modules::run('member/show_member_section'); ?>