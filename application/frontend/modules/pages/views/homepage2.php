<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7 "> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>NRBI - National Battery Research Institute | Homepage</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="<?=base_url('assets/images/favicon.ico');?>">

    <!-- FONTS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,700italic,900'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Orbitron:300,400,400italic,700,700italic,900'>

    <!-- CSS -->
    <link rel='stylesheet' href="<?=base_url('assets/css/global.css');?>">
    <link rel='stylesheet' href="<?=base_url('assets/css/structure.css');?>">
    <link rel='stylesheet' href="<?=base_url('assets/css/space.css');?>">
    <link rel='stylesheet' href="<?=base_url('assets/css/custom.css');?>">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />

    <!-- Revolution Slider -->
    <link rel="stylesheet" href='<?=base_url("assets/plugins/rs-plugin/css/settings.css");?>'>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>

</head>

<body class="page-parent template-slider color-custom layout-full-width header-stack header-left subheader-transparent sticky-header sticky-white subheader-title-left footer-copy-center">
    <!-- Main Theme Wrapper -->
    <div id="Wrapper">
        <!-- Header Wrapper -->
        <div id="Header_wrapper">
            <!-- Header -->
            <header id="Header" style="background: #FFF !important">
                <!-- Header Top -  Info Area -->
                <!-- <div id="Action_bar">
                    <div class="container">
                        <div class="column one">

                            <ul class="contact_details">
                                <li class="slogan">
                                    Have any questions?
                                </li>
                                <li class="phone">
                                    <i class="icon-phone"></i><a href="tel:+61383766284">+61 383 766 284</a>
                                </li>
                                <li class="mail">
                                    <i class="icon-mail-line"></i><a href="mailto:noreply@envato.com">noreply@envato.com</a>
                                </li>
                            </ul>

                            <ul class="social">
                                <li class="skype">
                                    <a href="#" title="Skype"><i class="icon-skype"></i></a>
                                </li>
                                <li class="facebook">
                                    <a href="https://www.facebook.com/Beantown-Themes-653197714728193" title="Facebook"><i class="icon-facebook"></i></a>
                                </li>
                                <li class="googleplus">
                                    <a href="https://plus.google.com/" title="Google+"><i class="icon-gplus"></i></a>
                                </li>
                                <li class="twitter">
                                    <a href="#" title="Twitter"><i class="icon-twitter"></i></a>
                                </li>
                                <li class="vimeo">
                                    <a href="https://vimeo.com/" title="Vimeo"><i class="icon-vimeo"></i></a>
                                </li>
                                <li class="youtube">
                                    <a href="#" title="YouTube"><i class="icon-play"></i></a>
                                </li>
                                <li class="flickr">
                                    <a href="https://www.flickr.com/" title="Flickr"><i class="icon-flickr"></i></a>
                                </li>
                                <li class="pinterest">
                                    <a href="https://www.pinterest.com/" title="Pinterest"><i class="icon-pinterest"></i></a>
                                </li>
                                <li class="dribbble">
                                    <a href="https://dribbble.com" title="Dribbble"><i class="icon-dribbble"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <!-- Header -  Logo and Menu area -->
                <div id="Top_bar" style="background:#FFF !important">
                    <div class="container">
                        <div class="column one">
                            <div class="top_bar_left clearfix">
                                <!-- Logo-->
                                <div class="logo">
                                    <a id="logo" href="<?=base_url();?>" title="National Battery Research Institute"><img class="scale-with-grid" src='<?=base_url("assets/images/logo_size_invert.jpg");?>' alt="National Battery Research Institute" />
                                    </a>
                                </div>
                                <!-- Main menu-->
                                <div class="menu_wrapper">
                                    <nav id="menu">
                                        <ul id="menu-main-menu" class="menu">
                                            <!-- <li class="current-menu-item"> -->
                                            <li>
                                                <a href="<?=site_url('research');?>"><span>RESEARCH</span></a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="<?=site_url('research/lithiumion');?>"><span>Lithium Ion</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('research/beyondlithiumion');?>"><span>Beyond Lithium Ion</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('research/industrysprints');?>"><span>Industry Sprints</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('batterycharacterisation');?>"><span>Battery Characterisation</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('research/community');?>"><span>Research Community</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?=site_url('research/highlight');?>"><span>Research Highlights</span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="<?=site_url('publication');?>"><span>PUBLICATION</span></a>
                                            </li>
                                            <li>
                                                <a href="<?=site_url('opportunities');?>"><span>OPPORTUNITIES</span></a>
                                            </li>
                                            <li>
                                                <a href="<?=site_url('educationskill');?>"><span>EDUCATION &amp; SKILLS</span></a>
                                            </li>
                                            <li>
                                                <a href="<?=site_url('articles');?>"><span>NEWS ARTICLES</span></a>
                                            </li>
                                        </ul>
                                    </nav><a class="responsive-menu-toggle" href="#"><i class="fas fa-bars"></i></a>
                                </div>
                                <!-- Secondary menu area - only for certain pages -->
                                <div class="secondary_menu_wrapper">
                                    <nav id="secondary-menu" class="menu-secondary-menu-container">
                                        <ul id="menu-secondary-menu" class="secondary-menu">
                                            <li class="menu-item-1568">
                                                <a href="<?=base_url();?>"><u>HOME</u></a>
                                            </li>
                                            <li class="menu-item-1574">
                                                <a href="<?=site_url('about');?>">ABOUT</a>
                                                <ul class="sub-menu">
                                                    <li class="menu-item-1569">
                                                        <a href="<?=site_url('about');?>#team">Team</a>
                                                    </li>
                                                    <li class="menu-item-1570">
                                                        <a href="<?=site_url('about');?>#values">Our Values</a>
                                                    </li>
                                                    <li class="menu-item-1571">
                                                        <a href="<?=site_url('expert_panel');?>">Expert Panel</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-1583">
                                                <a target="_blank" href="<?=site_url('contact');?>">CONTACT</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>

                                <!-- Header Searchform area-->
                                <div class="search_wrapper">
                                    <form method="get" action="#">
                                        <i class="icon_search icon-search"></i><a href="#" class="icon_close"><i class="fas fa-search"></i></a>
                                        <input type="text" class="field" name="s" placeholder="Enter your search" />
                                    </form>
                                </div>
                            </div>
                            <div class="top_bar_right">
                                <div class="top_bar_right_wrapper">
                                    <a id="search_button" href="#"><i class="fas fa-search"></i> &nbsp;</a>
                                    <!--wpml flags selector-->
                                    <div class="wpml-languages enabled">
                                        <a class="active tooltip" href="#" data-tooltip="No translations available for this page">
                                            <img src='<?=base_url("assets/images/flags/en.png");?>' alt="English" />&nbsp;<i class="fas fa-caret-down"></i>
                                        </a>
                                        <ul class="wpml-lang-dropdown">
                                            <li>
                                                <a href="#"><img src='<?=base_url("assets/images/flags/id.png");?>' alt="Bahasa" />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        <!-- Main Content -->
        <div id="Content">
            <div class="content_wrapper clearfix">
                <div class="sections_group">
                    <div class="entry-content">
                        <div id="slider-nrbi">
                            <div class="section full-screen section-border-bottom " style="padding-top:40px; padding-bottom:0px; background-image:url(https://faraday.ac.uk/wp-content/uploads/2019/04/faraday-day-2-19-resized-1.jpg); background-repeat:no-repeat; background-position:-60% 40% !important;background-size:cover !important; -webkit-background-size:cover;">
                                <div class="section_wrapper clearfix">
                                    <div class="items_group clearfix">
                                        <div class="column two-third column_placeholder">
                                            <div class="placeholder">
                                                &nbsp;
                                            </div>
                                        </div>
                                        <div class="column one-third column_column" style="background: rgba(255,255,255,.7) !important">
                                            <div class="column_attr slider-caption animate fadeInRight">
                                                <div style="padding: 40px; box-sizing: border-box; -webkit-box-sizing: border-box;">
                                                    <h2 style="margin-right: 10%; font-size: 45px">Education
                                                        <br>
                                                        and Skills Development</h2>
                                                    <hr class="no_line hrmargin_b_30" />
                                                    <div style="position: relative; margin-left: -20%;"><img class="scale-with-grid" src='<?=base_url("assets/images/home_space_hr.png");?>' alt="">
                                                    </div>
                                                    <hr class="no_line hrmargin_b_30" />
                                                    <h6 style="margin-right: 30%;">The race to electrification needs young people, interested in science and engineering, with the drive to play a part in changing all our futures. Join our programmes.</h6>
                                                    <hr class="no_line hrmargin_b_40" />
                                                    <a class="button button_large button_theme button_js" href="about.html"><span class="button_label">Read more</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="section full-screen full-width highlight-right section-border-bottom " style="padding-top:0px; padding-bottom:0px; background-image:url(https://faraday.ac.uk/wp-content/uploads/2020/03/Castle-Bromwich-Manufacturing-5-30.jpg); background-repeat:no-repeat; background-position:-70% 40% !important;background-size:contain !important; overflow: hidden; -webkit-background-size:contain;">>
                                <div class="section_wrapper clearfix">
                                    <div class="items_group clearfix">
                                        <!-- One Second (1/2) Column -->
                                        <div class="column one-second column_placeholder">
                                            <div class="placeholder">
                                                &nbsp;
                                            </div>
                                        </div>
                                        <!-- One Second (1/2) Column -->
                                        <div class="column one-second column_column ">
                                            <div class="column_attr slider-caption animate fadeInRight">
                                                <div style="padding: 40px; box-sizing: border-box; -webkit-box-sizing: border-box;">
                                                    <!-- <h5 style="letter-spacing: 4px;">New at Be Space</h5> -->
                                                    <h2 style="margin-right: 10%;">UK EV and Battery Production Potential 2040</h2>
                                                    <hr class="no_line hrmargin_b_30" />
                                                    <div style="position: relative; margin-left: -20%;"><img class="scale-with-grid" src='<?=base_url("assets/images/home_space_hr.png");?>' alt="">
                                                    </div>
                                                    <hr class="no_line hrmargin_b_30" />
                                                    <h6 style="margin-right: 30%;">Revised projections: demand for 7 UK gigafactories of 20 GWh p.a. by 2040.</h6>
                                                    <hr class="no_line hrmargin_b_40" />
                                                    <a class="button button_large button_theme button_js" href="about.html"><span class="button_label">Read more</span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="section equal-height " style="padding-top:90px; padding-bottom:50px; ">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- One Second (1/2) Column -->
                                    <div class="column one-second column_column ">
                                        <div class="column_attr" style=" padding:0 8% 0 0;">
                                            <hr class="no_line" style="margin: 0 auto 50px;" />
                                            <h3>New Way of Collaboration</h3>
                                            <hr class="no_line hrmargin_b_30" />
                                            <h6> <b>National Battery Research Indonesia (NBRI)</b> is Indonesia independent institute for electrochemical energy storage research and skills development.</h6>
                                            <hr class="no_line hrmargin_b_30" />
                                            <p>
                                                NBRI aims to contribute to the overall research capacity and training environment in Indonesia in battery research. We bring together scientists and industry partners on research projects to reduce battery cost, weight, and volume; to improve performance and reliability; and to develop whole-life strategies including recycling and reuse.
                                            </p>
                                            <hr class="no_line hrmargin_b_30" />
                                            <p>
                                                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dooluptatem.
                                            </p>
                                            <hr class="no_line hrmargin_b_30" />
                                            <a class="button button_large button_theme button_js" href="technology.html"><span class="button_label">Read more</span></a>
                                        </div>
                                    </div>
                                    <!-- One Second (1/2) Column -->
                                    <div class="column one-second column_column space-border">
                                        <div class="column_attr" style=" padding:0 0 0 9%;">
                                            <hr class="no_line hrmargin_b_40" />
                                            <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                <div class="image_wrapper"><img class="scale-with-grid" src="https://faraday.ac.uk/wp-content/uploads/2019/04/Alireza-Rastegarpanah-2-resized.jpg" alt="" width="442" height="313" />
                                                </div>
                                            </div>
                                            <hr class="no_line hrmargin_b_30" />
                                            <h3>What we do</h3>
                                            <ul class="list_check" style="font-size: 13px;">
                                                <li style="background: url(assets/images/home_space_list.png) no-repeat; padding-left: 30px;">
                                                    Mapping of Indonesian activities: Human resources and capabilities,
                                                    research facilities, local material/mineral resources, and industrial
                                                    activities.
                                                </li>
                                                <li style="background: url(assets/images/home_space_list.png) no-repeat; padding-left: 30px;">
                                                     Develop a batteries manufacturing industry using locally sourced
                                                    resources, which will enable Indonesia to be independent in energy
                                                </li>
                                                <li style="background: url(assets/images/home_space_list.png) no-repeat; padding-left: 30px;">
                                                    As a forum to help build networking with other parties with similar
                                                    projects throughout the world
                                                </li>
                                            </ul>
                                            <hr class="no_line hrmargin_b_20" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section full-width sections_style_0 ">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- One full width row-->
                                    <div class="column one column_slider_plugin ">
                                        <!-- Revolution slider area-->
                                        <div class="mfn-main-slider">
                                            <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                                            <link href="http://fonts.googleapis.com/css?family=Roboto:900,700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                                            <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#fff;padding:0px;margin-top:0px;margin-bottom:0px;">
                                                <div id="rev_slider_4_1" class="rev_slider fullwidthabanner" data-version="5.0.6">
                                                    <ul>
                                                        <li data-index="rs-2279" data-transition="parallaxvertical" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/home_space_blog2-100x50.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Quisque lorem tortor fringilla" data-param1="Universe" data-description="Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non, consectetuer lobortis quis, varius in, purus. Integer ultrices posuere cubilia Curae, Nulla ipsum dolor lacus, suscipit adipiscing. Cum sociis natoque penatibus et ultrices volutpat.">
                                                            <img src='<?=base_url("assets/images/home_space_blog2.jpg");?>' alt="" width="1920" height="1280" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                                            <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" id="slide-2279-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on" style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 1.00);"></div>
                                                            <div class="tp-caption Newspaper-Title tp-resizeme rs-parallaxlevel-0" id="slide-2279-layer-1" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['165','135','105','130']" data-fontsize="['50','50','50','30']" data-lineheight="['55','55','55','35']" data-fontweight="['700','400','400','400']" data-width="['600','600','600','420']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal; font-weight: 700;">
                                                                Quisque lorem tortor fringilla
                                                            </div>
                                                            <div class="tp-caption Newspaper-Subtitle tp-resizeme rs-parallaxlevel-0" id="slide-2279-layer-2" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['140','110','80','100']" data-fontweight="['400','900','900','900']" data-color="['rgba(255, 255, 255, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);text-transform:uppercase;">
                                                                May 6, 2014
                                                            </div>
                                                            <a class="tp-caption Newspaper-Button rev-btn rs-parallaxlevel-0" href="<?=base_url('articles/loremipsum');?>" target="_self" id="slide-2279-layer-5" data-x="['left','left','left','left']" data-hoffset="['53','53','53','30']" data-y="['top','top','top','top']" data-voffset="['361','331','301','245']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);cursor:pointer;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="on" data-responsive="off" style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Read more </a>
                                                        </li>
                                                        <li data-index="rs-2281" data-transition="parallaxvertical" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/home_space_blog4-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Vivamus sit amet metus sem uspendisse pellen" data-param1="Technology" data-description="Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis.">
                                                            <img src='<?=base_url("assets/images/home_space_blog4.jpg");?>' alt="" width="1920" height="1280" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                                            <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" id="slide-2281-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on" style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 1.00);"></div>
                                                            <div class="tp-caption Newspaper-Title tp-resizeme rs-parallaxlevel-0" id="slide-2281-layer-1" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['165','135','105','130']" data-fontsize="['50','50','50','30']" data-lineheight="['55','55','55','35']" data-fontweight="['700','400','400','400']" data-width="['600','600','600','420']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal; font-weight: 700;">
                                                                Vivamus sit amet metus sem uspendisse pellen
                                                            </div>
                                                            <div class="tp-caption Newspaper-Subtitle tp-resizeme rs-parallaxlevel-0" id="slide-2281-layer-2" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['140','110','80','100']" data-fontweight="['400','900','900','900']" data-color="['rgba(255, 255, 255, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);text-transform:uppercase;">
                                                                May 7, 2014
                                                            </div>
                                                            <a class="tp-caption Newspaper-Button rev-btn rs-parallaxlevel-0" href="<?=base_url('articles/loremipsum');?>" target="_self" id="slide-2281-layer-5" data-x="['left','left','left','left']" data-hoffset="['53','53','53','30']" data-y="['top','top','top','top']" data-voffset="['361','331','301','245']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);cursor:pointer;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="on" data-responsive="off" style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Read more </a>
                                                        </li>
                                                        <li data-index="rs-2277" data-transition="parallaxvertical" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/home_space_blog1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Vestibulum commodo volutpat laoreet" data-param1="Planets" data-description="Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.">
                                                            <img src='<?=base_url("assets/images/home_space_blog1.jpg");?>' alt="" width="1920" height="1280" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                                            <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" id="slide-2277-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on" style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 1.00);"></div>
                                                            <div class="tp-caption Newspaper-Title tp-resizeme rs-parallaxlevel-0" id="slide-2277-layer-1" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['165','135','105','130']" data-fontsize="['50','50','50','30']" data-lineheight="['55','55','55','35']" data-fontweight="['700','400','400','400']" data-width="['600','600','600','420']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal; font-weight: 700;">
                                                                Vestibulum commodo volutpat laoreet
                                                            </div>
                                                            <div class="tp-caption Newspaper-Subtitle tp-resizeme rs-parallaxlevel-0" id="slide-2277-layer-2" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['140','110','80','100']" data-fontweight="['400','900','900','900']" data-color="['rgba(255, 255, 255, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);text-transform:uppercase;">
                                                                May 8, 2014
                                                            </div>
                                                            <a class="tp-caption Newspaper-Button rev-btn rs-parallaxlevel-0" href="<?=base_url('articles/loremipsum');?>" target="_self" id="slide-2277-layer-5" data-x="['left','left','left','left']" data-hoffset="['53','53','53','30']" data-y="['top','top','top','top']" data-voffset="['361','331','301','245']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);cursor:pointer;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="on" data-responsive="off" style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Read more </a>
                                                        </li>
                                                        <li data-index="rs-2275" data-transition="parallaxvertical" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="images/home_space_blog3-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Vestibulum ante ipsum primis" data-param1="Earth" data-description="Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.">
                                                            <img src='<?=base_url("assets/images/home_space_blog3.jpg");?>' alt="" width="1920" height="1280" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                                            <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0" id="slide-2275-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on" style="z-index: 5;background-color:rgba(0, 0, 0, 0.35);border-color:rgba(0, 0, 0, 1.00);"></div>
                                                            <div class="tp-caption Newspaper-Title tp-resizeme rs-parallaxlevel-0" id="slide-2275-layer-1" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['165','135','105','130']" data-fontsize="['50','50','50','30']" data-lineheight="['55','55','55','35']" data-fontweight="['700','400','400','400']" data-width="['600','600','600','420']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal; font-weight: 700;">
                                                                Vestibulum ante ipsum primis
                                                            </div>
                                                            <div class="tp-caption Newspaper-Subtitle tp-resizeme rs-parallaxlevel-0" id="slide-2275-layer-2" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['140','110','80','100']" data-fontweight="['400','900','900','900']" data-color="['rgba(255, 255, 255, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)','rgba(168, 216, 238, 1.00)']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);text-transform:uppercase;">
                                                                May 9, 2014
                                                            </div>
                                                            <a class="tp-caption Newspaper-Button rev-btn rs-parallaxlevel-0" href="<?=base_url('articles/loremipsum');?>" target="_self" id="slide-2275-layer-5" data-x="['left','left','left','left']" data-hoffset="['53','53','53','30']" data-y="['top','top','top','top']" data-voffset="['361','331','301','245']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);cursor:pointer;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-actions='' data-responsive_offset="on" data-responsive="off" style="z-index: 8; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Read more </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tp-static-layers"></div>
                                                    <div class="tp-bannertimer tp-bottom flv_viz_hid"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section" style="padding-top:90px; padding-bottom:50px; ">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_column ">
                                        <div class="column_attr" style=" padding:0 8% 0 0;">
                                            <hr class="no_line hrmargin_b_40" />
                                            <h3>Meet Our Founder</h3>
                                            <hr class="no_line hrmargin_b_30" />
                                            <a class="button button_large button_theme button_js" href="stuff.html"><span class="button_label">Read more</span></a>
                                        </div>
                                    </div>
                                    <!-- Three Fourth (3/4) Column -->
                                    <div class="column three-fourth column_column space-border">
                                        <div class="column_attr" style=" padding:0 0 0 8%;">
                                            <hr class="no_line hrmargin_b_40" />
                                            <div class="column one">
                                                <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse.</h6>
                                            </div>
                                            <!-- One Third (1/3) Column -->
                                            <div class="column one-second">
                                                <div style="margin-right: 20%;">
                                                    <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                        <div class="image_wrapper"><img class="scale-with-grid" src='<?=base_url("assets/images/prof_evvy.jpg");?>' alt="" width="380" height="342" />
                                                        </div>
                                                    </div>
                                                    <hr class="no_line hrmargin_b_20" />
                                                    <h5>Prof. Dr. rer nat Evvy Kartini </h5>
                                                    <p><i>Founder</i></p>
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolorsi architecto beatae vitae dicta sunt explicabo.
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- One Third (1/3) Column -->
                                            <div class="column one-second">
                                                <div style="margin-right: 20%;">
                                                    <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                        <div class="image_wrapper"><img class="scale-with-grid" src="https://static.wixstatic.com/media/ae3692b18d64410dafc8014b30a9982f.jpg/v1/crop/x_861,y_0,w_3397,h_3413/fill/w_300,h_296,al_c,q_80,usm_0.66_1.00_0.01/Businessman.webp" alt="" width="380" height="342" />
                                                        </div>
                                                    </div>
                                                    <hr class="no_line hrmargin_b_20" />
                                                    <h5>Prof. Dr. Alan J. Drew</h5>
                                                    <p><i>Co-Founder</i></p>
                                                    <p>
                                                        Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolorsi architecto beatae vitae dicta sunt explicabo.
                                                    </p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section full-width clearfix" style="background-color:#eef1ec; color: #D10922; padding:60px 40px 70px">
                            <style type="text/css" media="screen">
                                .column_quick_fact .desc {
                                    color: #000
                                }
                            </style>
                            <div class="items_group clearfix">
                                <!-- One full width row-->
                                <div class="column one column_column">
                                    <div class="column_attr align-center">
                                        <center>
                                            <h3>Indonesia is Primed for a Battery Revolution</h3>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="items_group clearfix">
                                <!-- One Fourth (1/4) Column -->
                                <div class="column one-fourth column_quick_fact">
                                    <!-- Counter area-->
                                    <div class="quick_fact animate-math">
                                        <div class="number" data-to="35">
                                            0 </div>
                                        <h3 class="title">countries</h3>
                                        <hr class="hr_narrow" />
                                        <div class="desc">
                                            Donec vestibulum justo a diam ultricies pel lentesque. Quisque mattis diam vel lac. </div>
                                    </div>
                                </div>
                                <!-- One Fourth (1/4) Column -->
                                <div class="column one-fourth column_quick_fact">
                                    <!-- Counter area-->
                                    <div class="quick_fact animate-math">
                                        <div class="number" data-to="142">
                                            0 </div>
                                        <h3 class="title">articles</h3>
                                        <hr class="hr_narrow" />
                                        <div class="desc">
                                            Donec vestibulum justo a diam ultricies pel lentesque. Quisque mattis diam vel lac. </div>
                                    </div>
                                </div>
                                <!-- One Fourth (1/4) Column -->
                                <div class="column one-fourth column_quick_fact">
                                    <!-- Counter area-->
                                    <div class="quick_fact animate-math">
                                        <div class="number" data-to="89">
                                            0 </div>
                                        <h3 class="title">projects</h3>
                                        <hr class="hr_narrow" />
                                        <div class="desc">
                                            Donec vestibulum justo a diam ultricies pel lentesque. Quisque mattis diam vel lac. </div>
                                    </div>
                                </div>
                                <!-- One Fourth (1/4) Column -->
                                <div class="column one-fourth column_quick_fact">
                                    <!-- Counter area-->
                                    <div class="quick_fact animate-math">
                                        <div class="number" data-to="9">
                                            0 </div>
                                        <h3 class="title">years of experience</h3>
                                        <hr class="hr_narrow" />
                                        <div class="desc">
                                            Donec vestibulum justo a diam ultricies pel lentesque. Quisque mattis diam vel lac. </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section clearfix" style="padding:60px 40px 70px">
                            <div class="section_wrapper">

                                <div class="items_group clearfix">
                                    <!-- One full width row-->
                                    <div class="column one column_column">
                                        <div class="column_attr align-center">
                                            <center>
                                                <h3>Research Projects</h3>
                                            </center>
                                        </div>
                                    </div>
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_icon_box">
                                        <div class="icon_box icon_position_top has_border">
                                            <a  href="#">
                                                <div class="icon_wrapper">
                                                    <div class="icon">
                                                        <i class="icon-heart-line"></i>
                                                    </div>
                                                </div>
                                                <div class="desc_wrapper">
                                                    <h4>Aenean fermen</h4>
                                                    <div class="desc">
                                                        Vitae adipiscing turpis. Aenean ligula nibo molestie id vivide.
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_icon_box">
                                        <div class="icon_box icon_position_top has_border">
                                            <a  href="#">
                                                <div class="icon_wrapper">
                                                    <div class="icon">
                                                        <i class="icon-heart-line"></i>
                                                    </div>
                                                </div>
                                                <div class="desc_wrapper">
                                                    <h4>Aenean fermen</h4>
                                                    <div class="desc">
                                                        Vitae adipiscing turpis. Aenean ligula nibo molestie id vivide.
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_icon_box">
                                        <div class="icon_box icon_position_top has_border">
                                            <a  href="#">
                                                <div class="icon_wrapper">
                                                    <div class="icon">
                                                        <i class="icon-heart-line"></i>
                                                    </div>
                                                </div>
                                                <div class="desc_wrapper">
                                                    <h4>Aenean fermen</h4>
                                                    <div class="desc">
                                                        Vitae adipiscing turpis. Aenean ligula nibo molestie id vivide.
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_icon_box">
                                        <div class="icon_box icon_position_top has_border">
                                            <a  href="#">
                                                <div class="icon_wrapper">
                                                    <div class="icon">
                                                        <i class="icon-heart-line"></i>
                                                    </div>
                                                </div>
                                                <div class="desc_wrapper">
                                                    <h4>Aenean fermen</h4>
                                                    <div class="desc">
                                                        Vitae adipiscing turpis. Aenean ligula nibo molestie id vivide.
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_icon_box">
                                        &nbsp;
                                    </div>
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_icon_box">
                                        <div class="icon_box icon_position_top has_border">
                                            <a  href="#">
                                                <div class="icon_wrapper">
                                                    <div class="icon">
                                                        <i class="icon-heart-line"></i>
                                                    </div>
                                                </div>
                                                <div class="desc_wrapper">
                                                    <h4>Aenean fermen</h4>
                                                    <div class="desc">
                                                        Vitae adipiscing turpis. Aenean ligula nibo molestie id vivide.
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_icon_box">
                                        <div class="icon_box icon_position_top has_border">
                                            <a  href="#">
                                                <div class="icon_wrapper">
                                                    <div class="icon">
                                                        <i class="icon-heart-line"></i>
                                                    </div>
                                                </div>
                                                <div class="desc_wrapper">
                                                    <h4>Aenean fermen</h4>
                                                    <div class="desc">
                                                        Vitae adipiscing turpis. Aenean ligula nibo molestie id vivide.
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <style type="text/css" media="screen">
                                .column_quick_fact .desc {
                                    color: #000
                                }
                            </style>
                        </div>
                        <div class="section" style="background-color:#eef1ec; color: #D10922; padding:60px 40px 0px">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- Three Fourth (3/4) Column -->
                                    <div class="column three-fourth column_column">
                                        <div class="column_attr" style=" padding:0 0 0 8%;">
                                            <!-- <hr class="no_line hrmargin_b_40" />
                                            <div class="column one">
                                                <h4>Government Institutions</h4>
                                            </div> -->
                                            <div class="wrapper-member">
                                                <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <!-- One Third (1/3) Column -->
                                                <div class="column one-fourth">
                                                    <div style="margin-right: 20%;">
                                                        <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                                            <div class="image_wrapper"><img class="scale-with-grid" src="https://via.placeholder.com/100x100" alt="" width="380" height="342" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_column space-border">
                                        <div class="column_attr" style=" padding:0 0 0 8%;">
                                            <hr class="no_line hrmargin_b_40" />
                                            <h3>Member of NBRI</h3>
                                            <hr class="no_line hrmargin_b_30" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            a.social-icon{
                padding-left: 5px;
                padding-right: 5px;
            }
        </style>
        <!-- Footer-->
        <footer id="Footer" class="clearfix">
            <!-- Footer - First area -->
            <div class="footer_action">
                <div class="container">
                    <!-- One full width row-->
                    <div class="column one column_column">
                        <h4 style="color: #a6a6a8;">News Feed / Social Media</h4>
                    </div>
                    <div class="column one column_column" style="padding-top: 0">
                        <div class="items_group clearfix">
                            <a href="" class="social-icon" title=""><i class="fas fa-2x fa-envelope-square"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-facebook-square"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-instagram-square"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-twitter-square"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-linkedin"></i></a>
                            <a href="" class="social-icon" title=""><i class="fab fa-2x fa-youtube-square"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widgets_wrapper">
                <div class="container">
                    <!-- One Fourth (1/4) Column -->
                    <div class="column one-second">
                        <!-- Meta Links Area -->
                        <img class="scale-with-grid" src='<?=base_url("assets/images/logo_size_invert.jpg");?>' alt="BeTheme - Best Html Theme Ever" />
                        <br><br>
                        <h4>National Battery Research Institute (NBRI)</h4>
                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum sequi a magni cupiditate maiores hic perferendis iure eum amet asperiores! Tempora deserunt accusantium pariatur dolore aliquid ut impedit assumenda molestiae.</p>
                    </div>
                    <div class="column one-fourth"> &nbsp;</div>
                    <!-- One Fourth (1/4) Column -->
                    <div class="column one-fourth">
                        <h4>Subcribe</h4>
                        <form class="form" action="homepage_submit" method=" " accept-charset="utf-8">
                            <input type="text" class="form-control" name="" value="" placeholder="youremail@domain.com" style="background-color: transparent;border-bottom: 1px solid">
                            <button class="btn btn-custom" style="background-color: #D0011B"> Subscribe</button>
                        </form>
                        <ul>
                            <li><i class="fas fa-home"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam, asperiores.</li>
                            <li><i class="fas fa-phone"></i> 020 202002020</li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            </div>
            <style>
                .footer-nav-link a{
                    padding-left: 5%;
                    padding-right: 5%;
                }
            </style>
            <div class="widgets_wrapper">
                <div class="container footer-nav-link" style="text-align: center">
                    <a href="" class="footer-nav-link" title="">Home</a>
                    <a href="" class="footer-nav-link" title="">About</a>
                    <a href="" class="footer-nav-link" title="">Contact</a>
                </div>
            </div>
            <!-- Footer copyright-->
            <div class="footer_copy">
                <div class="container">
                    <div class="column one">

                        <div class="copyright">
                            &copy; 2020 National Battery Research Institute - HTML by <a target="_blank" rel="nofollow" href="http://www.raditya.site/">Raditya</a>
                        </div>
                        <!--Social info area-->
                        <ul class="social"></ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- JS -->
    <script src='<?=base_url("assets/js/jquery-2.1.4.min.js");?>'></script>

    <script src='<?=base_url("assets/js/mfn.menu.js");?>'></script>
    <script src='<?=base_url("assets/js/jquery.plugins.js");?>'></script>
    <script src='<?=base_url("assets/js/jquery.jplayer.min.js");?>'></script>
    <script src='<?=base_url("assets/js/animations/animations.js");?>'></script>
    <script src='<?=base_url("assets/js/scripts.js");?>'></script>

    <script src='<?=base_url("assets/plugins/rs-plugin/js/jquery.themepunch.tools.min.js");?>'></script>
    <script src='<?=base_url("assets/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js");?>'></script>
    <script src='<?=base_url("assets/plugins/rs-plugin/js/extensions/revolution.extension.video.min.js");?>'></script>
    <script src='<?=base_url("assets/plugins/rs-plugin/js/extensions/revolution.extension.slideanims.min.js");?>'></script>
    <script src='<?=base_url("assets/plugins/rs-plugin/js/extensions/revolution.extension.actions.min.js");?>'></script>
    <script src='<?=base_url("assets/plugins/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js");?>'></script>
    <script src='<?=base_url("assets/plugins/rs-plugin/js/extensions/revolution.extension.kenburn.min.js");?>'></script>
    <script src='<?=base_url("assets/plugins/rs-plugin/js/extensions/revolution.extension.navigation.min.js");?>'></script>
    <script src='<?=base_url("assets/plugins/rs-plugin/js/extensions/revolution.extension.migration.min.js");?>'></script>
    <script src='<?=base_url("assets/plugins/rs-plugin/js/extensions/revolution.extension.parallax.min.js");?>'></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

    <script>
        $(document).ready(function() {
            $("#slider-nrbi").slick({
                arrows: false,
                dots: false,
                adaptiveHeight: true
            });

            if ($('.slick-slide').hasClass('slick-active')) {
                $('.slider-caption').addClass('animated fadeInRight');
            } else {
                $('.slider-caption').removeClass('animated fadeInRight');
            }

            $("#slider-nrbi").on("beforeChange", function() {

            $('.slider-caption').removeClass('animated fadeInRight').hide();
            setTimeout(() => {
                $('.slider-caption').addClass('animated fadeInRight').show();

                }, 500);

            });

            $(".wrapper-member").slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                dots: false,
                prevArrow: false,
                nextArrow: false
            });
        });
    </script>

    <script>
        function revslider_showDoubleJqueryError(sliderID) {
            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option: <strong><b>Put JS Includes To Body</b></strong> option to true.";
            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>"
            jQuery(sliderID).show().html(errorMessage);
        }

        var tpj = jQuery;
        var revapi4;
        tpj(document).ready(function() {
            if (tpj("#rev_slider_4_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_4_1");
            } else {
                revapi4 = tpj("#rev_slider_4_1").show().revolution({
                    sliderType: "standard",
                    sliderLayout: "fullwidth",
                    dottedOverlay: "none",
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "on",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        onHoverStop: "off",
                        touch: {
                            touchenabled: "on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "gyges",
                            enable: true,
                            hide_onmobile: false,
                            hide_over: 778,
                            hide_onleave: false,
                            tmp: '',
                            left: {
                                h_align: "right",
                                v_align: "bottom",
                                h_offset: 40,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "bottom",
                                h_offset: 0,
                                v_offset: 0
                            }
                        },
                        tabs: {
                            style: "hebe",
                            enable: true,
                            width: 250,
                            height: 100,
                            min_width: 250,
                            wrapper_padding: 0,
                            wrapper_color: "transparent",
                            wrapper_opacity: "0",
                            tmp: '<div class="tp-tab-title">{{param1}}</div><div class="tp-tab-desc">{{title}}</div>',
                            visibleAmount: 3,
                            hide_onmobile: true,
                            hide_under: 778,
                            hide_onleave: false,
                            hide_delay: 200,
                            direction: "vertical",
                            span: false,
                            position: "inner",
                            space: 10,
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        }
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [500, 450, 400, 350],
                    lazyType: "none",
                    parallax: {
                        type: "scroll",
                        origo: "enterpoint",
                        speed: 400,
                        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
                    },
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "on",
                    stopAfterLoops: 0,
                    stopAtSlide: 1,
                    shuffle: "off",
                    autoHeight: "off",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    startWithSlide: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
        });
        /*]]>*/
    </script>

    <script>
        jQuery(window).load(function() {
            var retina = window.devicePixelRatio > 1 ? true : false;
            if (retina) {
                var retinaEl = jQuery("#logo img.logo-main");
                var retinaLogoW = retinaEl.width();
                var retinaLogoH = retinaEl.height();
                retinaEl.attr("src", "images/retina-space.png").width(retinaLogoW).height(retinaLogoH);
                var stickyEl = jQuery("#logo img.logo-sticky");
                var stickyLogoW = stickyEl.width();
                var stickyLogoH = stickyEl.height();
                stickyEl.attr("src", "images/retina-space.png").width(stickyLogoW).height(stickyLogoH);
                var mobileEl = jQuery("#logo img.logo-mobile");
                var mobileLogoW = mobileEl.width();
                var mobileLogoH = mobileEl.height();
                mobileEl.attr("src", "images/retina-space-responsive.png").width(mobileLogoW).height(mobileLogoH);
            }
        });
    </script>
</body>

</html>