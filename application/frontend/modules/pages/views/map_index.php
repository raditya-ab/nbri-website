<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(https://faraday.ac.uk/wp-content/uploads/2019/04/Alireza-Rastegarpanah-2-resized.jpg)">
                <h1 class="page-title">NBRI One Map</h1>
            </div>
        </div>
    </div>
</section>

<section class="content-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <div id="map-slider-focus">
                    <div class="img-focus-item">
                        <div class="img-wrapper" style="background-image: url(<?=base_url('assets/images/upstream_map.jpg');?>)"></div>
                        <div class="caption-wrapper">
                            <h2>RESOURCES</h2>
                            <p>NBRI One Map RESOURCES - provide information about the industries that produces raw material for the battery </p>
                        </div>
                        <div class="readmore-wrapper">
                            <a target="_blank" href="<?=site_url('maps/resources');?>" class="btn-navy btn-readmore">Open Map</a>
                        </div>
                    </div>
                    <div class="img-focus-item">
                        <div class="img-wrapper" style="background-image: url(<?=base_url('assets/images/downstream_map.jpg');?>)"></div>
                        <div class="caption-wrapper">
                            <h2>END USERS</h2>
                            <p>NBRI One Map END USERS - provide information about the industries that utilize battery for their application </p>
                        </div>
                        <div class="readmore-wrapper">
                            <a target="_blank" href="<?=site_url('maps/end-users');?>" class="btn-navy btn-readmore">Open Map</a>
                        </div>
                    </div>
                    <div class="img-focus-item">
                        <div class="img-wrapper" style="background-image: url(<?=base_url('assets/images/battery_cell_map.jpg');?>)"></div>
                        <div class="caption-wrapper">
                            <h2>BATTERY MANUFACTURERS</h2>
                            <p>NBRI One Map BATTERY MANUFACTURERS - provide information about the industries that produce battery </p>
                        </div>
                        <div class="readmore-wrapper">
                            <a target="_blank" href="<?=site_url('maps/battery-manufacturers');?>" class="btn-navy btn-readmore">Open Map</a>
                        </div>
                    </div>
                    <div class="img-focus-item">
                        <div class="img-wrapper" style="background-image: url(<?=base_url('assets/images/research_facility_map.jpg');?>)"></div>
                        <div class="caption-wrapper">
                            <h2>RESEARCH FACILITIES</h2>
                            <p>NBRI One Map RESEARCH FACILITIES - provide information of the facility and human resources related to battery research and development </p>
                        </div>
                        <div class="readmore-wrapper">
                            <a target="_blank" href="<?=site_url('maps/research-facilities');?>" class="btn-navy btn-readmore">Open Map</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div id="maps-slider-nav" class="col-md-12" style="justify-content: space-between">
                <div class="img-grid">
                    <div class="img-wrapper bg-navy" style="background-image: url(<?=base_url('assets/images/upstream_map.jpg');?>)"></div>
                    <div class="caption-wrapper" >
                        RESOURCES
                    </div>
                </div>
                <div class="img-grid" style="">
                    <div class="img-wrapper bg-navy" style="background-image: url(<?=base_url('assets/images/downstream_map.jpg');?>)"></div>
                    <div class="caption-wrapper" >
                        END USERS
                    </div>
                </div>
                <div class="img-grid" style="">
                    <div class="img-wrapper bg-navy" style="background-image: url(<?=base_url('assets/images/battery_cell_map.jpg');?>)"></div>
                    <div class="caption-wrapper" >
                        BATTERY MANUFACTURERS
                    </div>
                </div>
                <div class="img-grid" style="">
                    <div class="img-wrapper bg-navy" style="background-image: url(<?=base_url('assets/images/research_facility_map.jpg');?>)"></div>
                    <div class="caption-wrapper" >
                        RESEARCH FACILITIES
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-md-12 btn-wrapper d-flex justify-content-between">
                <button class="btn btn-readmore btn-navy" id="btn-left">Prev</button>
                <button class="btn btn-readmore btn-navy" id="btn-right">Next</button>
            </div>
        </div> -->
    </div>
</section>