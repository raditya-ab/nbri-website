<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(https://faraday.ac.uk/wp-content/uploads/2019/04/Alireza-Rastegarpanah-2-resized.jpg)">
                <h1 class="page-title">Research</h1>
            </div>
        </div>
    </div>
</section>
<section id="page-subtitle" class="bg-yellow">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>In the near term, as released by the Presidential regulation No.55/2019 about the transportation in Indonesia to use 30% the electric vehicles (EV) in 2030, based on lithium-ion battery (LIB) technology. In order to accelerate the drive towards electric vehicles (EVs), it requires the optimisation of lithium-ion battery technology. So far, the performance of current LIB has several limitations, and they need some improvements for example the battery materials, battery chemistries, life cycle, energy density, cost and safety. So, in the medium to long term, the innovation of new battery chemistries and the commercialisation of new materials from the local resources will be very crucial. Due to this reason, the National Battery Research Institute (NBRI) exits to gather all the resources to achieve that goal.</p>
            </div>
        </div>
    </div>
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-title">Our Portfolio</h2>
                <p class="section-description">The NBRI portfolio of large scale, will become a mission-driven projects which selected after consultation with academic and industrial stakeholders across the country, with due consideration of the potential impact they could make to the Indonesia.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 grid-wrapper">
                <?php foreach($research_list as $i => $research):?>
                    <?php $content = strip_tags(stripslashes(html_entity_decode($research['content']))); ?>
                <div class="img-with-caption-wrapper">
                    <a href="<?=site_url("research/".$research['slug']);?>">
                        <div class="img-wrapper bg-yellow" style="background-image: url(<?=base_url('assets/images/research/'.$research['thumb_img']);?>)">
                            <h3 class="img-title"><?=$research['category_name'];?></h3>
                        </div>
                        <div class="caption-wrapper bg-navy">
                            <span><?=($content != '' && $content != NULL) ? word_limiter($content,10):'...';?></span>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>