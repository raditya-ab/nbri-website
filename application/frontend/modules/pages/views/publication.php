<div class="content_wrapper clearfix">
    <div class="sections_group">
        <div class="entry-content">
            <div class="section" style="padding-top:70px; padding-bottom:30px; ">
                <div class="section_wrapper clearfix">
                    <div class="items_group clearfix">
                        <!-- One Second (1/2) Column -->
                        <div class="column one column_column">
                            <h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum fugit sequi, nostrum ad vero sed aspernatur magnam at dolorum corrupti.</h5>
                            <p class="big">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim est totam, ab eligendi rerum. Corporis eaque aperiam fugit, maiores eos eligendi temporibus odio earum velit beatae quod dolorem deleniti quasi natus! Id deserunt modi, earum ducimus quo aperiam debitis excepturi iure at velit provident porro! Eaque quasi ipsam esse eligendi enim tempora quibusdam quaerat, laboriosam velit sint aspernatur maxime, saepe recusandae magni eum beatae nostrum excepturi accusamus fuga molestias. Eum excepturi incidunt, iure laboriosam rerum fuga ipsam autem. Culpa natus deserunt corporis, qui sunt, quisquam modi facilis eveniet similique illum quis fuga deleniti dolor aperiam perferendis aliquid esse ab maiores.</p>
                        </div>
                    </div>
                    <div class="items_group clearfix">
                        <div class="column one column_column">
                            <div class="accordion">
                                <div class="mfn-acc accordion_wrapper open1st toggle">

                                    <div class="question active">
                                        <div class="title">
                                                <h5 style="margin-bottom:0">Sed est elit posuere ac semper hendrerit neque</h5>
                                        </div>
                                    <div class="answer" style="display: block;">
                                        <div class="column three-fourth">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem.</p>
                                            <p>Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id. In viverra ipsum ac eros tristique dignissim. Donec aliquam velit vitae mi dictum.</p>
                                            <br>
                                            <br>
                                            <a href="#" title="Download Insights">Download Insights</a>
                                        </div>
                                        <div class="column one-fourth">
                                            <img src="https://via.placeholder.com/200x200" alt="" style="float:right">
                                        </div>

                                    </div>

                                </div>

                                <div class="question">
                                    <div class="title">

                                            <h5 style="margin-bottom:0">Aenean ligula nibh, molestie id viverra a</h5>
                                    </div>

                                    <div class="answer">
                                        <div class="column three-fourth">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem.</p>
                                            <p>Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id. In viverra ipsum ac eros tristique dignissim. Donec aliquam velit vitae mi dictum.</p>
                                            <br>
                                            <br>
                                            <a href="#" title="Download Insights">Download Insights</a>
                                        </div>
                                        <div class="column one-fourth">
                                            <img src="https://via.placeholder.com/200x200" alt="" style="float:right">
                                        </div>

                                    </div>
                                </div>

                                <div class="question">
                                    <div class="title">

                                            <h5 style="margin-bottom:0">Aenean ligula nibh, molestie id viverra a</h5>
                                    </div>

                                    <div class="answer">
                                        <div class="column three-fourth">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit tortor, dictum in gravida nec, aliquet non lorem.</p>
                                            <p>Donec vestibulum justo a diam ultricies pellentesque. Quisque mattis diam vel lacus tincidunt elementum. Sed vitae adipiscing turpis. Aenean ligula nibh, molestie id viverra a, dapibus at dolor. In iaculis viverra neque, ac eleifend ante lobortis id. In viverra ipsum ac eros tristique dignissim. Donec aliquam velit vitae mi dictum.</p>
                                            <br>
                                            <br>
                                            <a href="#" title="Download Insights">Download Insights</a>
                                        </div>
                                        <div class="column one-fourth">
                                            <img src="https://via.placeholder.com/200x200" alt="" style="float:right">
                                        </div>

                                    </div>
                                </div>

                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section the_content no_content">
                <div class="section_wrapper">
                    <div class="the_content_wrapper"></div>
                </div>
            </div>
        </div>
    </div>
</div>