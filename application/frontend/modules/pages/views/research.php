<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(<?=base_url('assets/images/research_banner.jpg');?>)">
                <h1 class="page-title">Research</h1>
            </div>
        </div>
    </div>
</section>
<section id="page-subtitle" class="bg-yellow">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>The Presidential regulation No.55/2019 states that the transportation in Indonesia will be transform into 30% the electric vehicles (EV) in 2030, which based on lithium-ion battery (LIB) technology.  In order to accelerate the drive towards electric vehicles (EVs), it requires the optimisation of lithium-ion battery technology. So far, the performance of current LIB has several limitations, and they need some improvements for example the battery materials, battery chemistries, life cycle, energy density, cost and safety. So, in the medium to long term, the innovation of new battery chemistries  and the commercialisation of new materials  from the local resources will be very crucial. Due to this reason, the National Battery Research Institute (NBRI) exits to gather all the resources to achieve that goal.</p>
            </div>
        </div>
    </div>
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-title">Battery Insight</h2>
                <p class="section-description">One of the NBRI missions is to introduce the Battery Research and Technology to worldwide society.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 grid-wrapper">
                <?php foreach($research_list as $i => $research):?>
                    <?php 
                        $content = strip_tags(stripslashes(html_entity_decode($research['content'])));
                        $img = (!empty($research['thumb_img'])) ? base_url('assets/images/research/'.$research['thumb_img']) : base_url('assets/images/nbri_new_logo.png');
                    ?>
                <div class="img-with-caption-wrapper">
                    <a href="<?=site_url("research/".$research['slug']);?>">
                        <div class="img-wrapper bg-yellow" style="background-image: url(<?=$img;?>)">
                            <h3 class="img-title"><?=$research['category_name'];?></h3>
                        </div>
                        <div class="caption-wrapper bg-navy">
                            <span><?=($content != '' && $content != NULL) ? word_limiter($content,10):'...';?></span>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>