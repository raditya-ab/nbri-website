<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(<?php echo $research_detail['img'];?>)">
                <h1 class="page-title">
                    <?=(array_key_exists('research_name', $research_detail)) ? $research_detail['research_name'] : $research_detail['category_name'];?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <?=stripslashes(html_entity_decode($research_detail['content']));?>
            </div>
            <div class="col-md-3">
                <table>
                    <tbody>
                        <tr>
                            <td width="110">Date</td>
                            <td>:</td>
                            <td><?=date('d F Y', strtotime($research_detail['created']));?></td>
                        </tr>
                        <tr rowspan="2">
                            <td width="110">Written by</td>
                            <td>:</td>
                            <td>NBRI</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Share Article</td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div class="share-wrapper" style="margin-top: 10px;width: 100%">
                                    <a target="_blank" class="mr-2" href="https://www.facebook.com/sharer/sharer.php?u=<?=urlencode(is_https() ? "https://" : "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/facebook.png');?>" alt="" width="25"></a>
                                    <a target="_blank" class="mr-2"href="mailto:?subject=Check this publication article from NBRI about <?=(array_key_exists('research_name', $research_detail)) ? $research_detail['research_name'] : $research_detail['category_name'];?>&body=This is the url: <?php echo is_https() ? "https://" : "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"><img class="social-img" src="<?=base_url('assets/images/footer-assets/mail.png');?>" alt="" width="25"></a>
                                    <a target="_blank" class="mr-2" href="http://twitter.com/share?text=Check this publication article from NBRI about <?=(array_key_exists('research_name', $research_detail)) ? $research_detail['research_name'] : $research_detail['category_name'];?>&url=<?php echo is_https() ? "https://" : "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>"><img class="social-img" src="<?=base_url('assets/images/footer-assets/twitter.png');?>" alt="" width="25"></a>
                                    <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo urlencode(is_https() ? "https://" : "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);?>"><img class="social-img" src="<?=base_url('assets/images/footer-assets/linkedin.png');?>" width="25"></a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>