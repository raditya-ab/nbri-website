<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(<?=$category_detail['img'];?>)">
                <h1 class="page-title"><?=$category_detail['category_name'];?></h1>
            </div>
        </div>
    </div>
</section>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="section-description"><?=stripslashes(html_entity_decode($category_detail['content']));?></p>
            </div>
        </div>
        <div class="row" style="margin-top: 30px">
            <div class="col-md-12 grid-wrapper">
                <?php foreach($research_list as $i => $research):?>
                    <?php
                        $content = strip_tags(stripslashes(html_entity_decode($research['content'])));
                        $img = (!empty($research['img_thumb'])) ? base_url('assets/images/subresearch/'.$research['img_thumb']) : base_url('assets/images/nbri_new_logo.png');
                    ?>
                <div class="img-with-caption-wrapper">
                    <a href="<?=site_url("research/".$research['category_slug'].'/'.$research['slug']);?>">
                        <div class="img-wrapper bg-yellow" style="background-image: url(<?=$img;?>)">
                            <h3 class="img-title"><?=$research['research_name'];?></h3>
                        </div>
                        <div class="caption-wrapper bg-navy">
                            <span><?=($content != '' && $content != NULL) ? word_limiter($content,10):'...';?></span>
                        </div>
                    </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>