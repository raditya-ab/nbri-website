<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Press extends Public_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('press_m');
        $this->template->set_layout('default_template');
    }

    public function index()
    {
        $this->data['publication_list'] = $this->press_m->get_all();

        $this->template->set('page_title', 'Publication');
        $this->template->set('page_banner', base_url('assets/images/publication_banner.jpg'));
        $this->template->build('index', $this->data);
    }

    public function detail($slug)
    {
        $this->template->set('page_title', 'Publication');
        $this->template->set('page_banner', base_url('assets/images/publication_banner.jpg'));

        $this->data['detail'] = $this->press_m->get_detail($slug);
        $this->data['publication_list'] = $this->press_m->get_all_related($slug);
        $this->template->build('detail', $this->data);
    }

    public function articlesdetail($category_slug, $news_slug)
    {

        $this->load->model('news/news_m','news_m');

        if(!$this->news_m->news_is_exist($category_slug, $news_slug)){
            show_404();
        }

        $this->data['news_detail'] = $this->news_m->get_detail($category_slug,$news_slug);

        if(is_null($this->data['news_detail']['img']) ||empty($this->data['news_detail']['img'])){
          $this->data['news_detail']['bgimg'] = base_url('assets/images/'.$this->session->userdata('website_var')['logo']);
        } else {
          $this->data['news_detail']['bgimg'] = base_url('assets/images/article/'.$this->data['news_detail']['img']);
        }

        // $this->data['slider'] = [
        //  'title' => $this->data['news_detail']['title'],
        //  'background' => base_url('assets/images/article/'.$this->data['news_detail']['bgimg'])
        // ];

        $this->template->title($this->data['news_detail']['title'],'Articles');
        $this->template->set('page_subtitle',$this->data['news_detail']['category_name']);

        $this->template->build('articles_detail', $this->data);
    }

}

/* End of file Articles.php */
/* Location: ./application/frontend/modules/articles/controllers/Articles.php */