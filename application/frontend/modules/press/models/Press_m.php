<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Press_m extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get_all() {
        $this->db->select('publication_press.*, GROUP_CONCAT(DISTINCT tags.tag_name SEPARATOR ", ") as tags');
        $this->db->from('publication_press');
        $this->db->join('press_tags', 'publication_press.id = press_tags.press_id', 'left');
        $this->db->join('tags', 'press_tags.tag_id = tags.id', 'left');
        $this->db->group_by('publication_press.id');
        $this->db->order_by('publication_press.created', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_detail($slug) {
        $this->db->select('publication_press.*, tags.tag_name');
        $this->db->from('publication_press');
        $this->db->join('press_tags', 'publication_press.id = press_tags.press_id', 'left');
        $this->db->join('tags', 'press_tags.tag_id = tags.id', 'left');
        $this->db->where('publication_press.slug', $slug);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function get_all_related($slug) {
      $this->db->select('publication_press.*, GROUP_CONCAT(DISTINCT tags.tag_name SEPARATOR ", ") as tags');
      $this->db->from('publication_press');
      $this->db->join('press_tags', 'publication_press.id = press_tags.press_id', 'left');
      $this->db->join('tags', 'press_tags.tag_id = tags.id', 'left');
      $this->db->group_by('publication_press.id');
      $this->db->order_by('publication_press.created', 'desc');
      $this->db->where('publication_press.status', 1);
      $this->db->where('publication_press.slug !=', $slug);
      $query = $this->db->get();
      return $query->result_array();
  }
}