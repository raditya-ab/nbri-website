<?php
$img = ($detail['img'] != NULL && $detail['img'] != '') ? base_url('assets/images/publication/' . $detail['img']) : base_url('assets/images/nbri_new_logo.png');
?>
<section id="page-hero">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 hero-container" style="background-image: url(<?= $page_banner; ?>), url(<?= base_url('assets/images/nbri_new_logo.png'); ?>">
        <h2 class="page-title"><?= $page_title; ?></h2>
      </div>
    </div>
  </div>
</section>
<section class="content-section news-detail-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3 offset-md-1">
        <img src="<?php echo $img; ?>" alt="" style="max-width: 100%">
      </div>
      <div class="col-md-5">
        <h2 class="page-title news-detail"><?= $detail['title']; ?></h2>
        <br><br><br>
        <p class="font-bold font-size-regular">Sinopsis</p>
        <p class="font-size-small"><?= stripslashes(html_entity_decode($detail['synopsis'])); ?></p>
        <br><br><br><br><br><br>
        <p class="font-bold font-size-regular">Detail Buku</p>
        <div class="row">
          <div class="col-md-3 font-bold font-size-regular">Judul Buku</div>
          <div class="col-md-9 font-size-small"><?= $detail['title'] ? $detail['title'] : '-'; ?></div>
        </div>
        <div class="row">
          <div class="col-md-3 font-bold font-size-regular">Nama Penulis</div>
          <div class="col-md-9 font-size-small"><?= $detail['author'] ? $detail['author'] : '-'; ?></div>
        </div>
        <div class="row">
          <div class="col-md-3 font-bold font-size-regular">Nama Penerbit</div>
          <div class="col-md-9 font-size-small"><?= $detail['publisher'] ? $detail['publisher'] : '-'; ?></div>
        </div>
        <div class="row">
          <div class="col-md-3 font-bold font-size-regular">Jumlah Halaman</div>
          <div class="col-md-9 font-size-small"><?= $detail['page_number'] ? $detail['page_number'] : '-'; ?></div>
        </div>
        <div class="row">
          <div class="col-md-3 font-bold font-size-regular">Dimensi Buku</div>
          <div class="col-md-9 font-size-small"><?= $detail['dimension'] ? $detail['dimension'] : '-'; ?></div>
        </div>
        <div class="row">
          <div class="col-md-3 font-bold font-size-regular">Harga Buku</div>
          <div class="col-md-9 font-size-small">Rp<?= $detail['price'] ? number_format($detail['price'], 0, ',', '.') : '-'; ?></div>
        </div>
        <div class="row">
          <div class="col-md-3 font-bold font-size-regular">ISBN</div>
          <div class="col-md-9 font-size-small"><?= $detail['isbn'] ? $detail['isbn'] : '-'; ?></div>
        </div>
        <!-- <div class="row">
                    <div class="col-md-3">Judul Buku</div>
                    <div class="col-md-6"><?= $detail['title']; ?></div>
                </div> -->
        <br>
        <p class="font-bold font-size-regular">Order</p>
        <p class="font-size-small"><a target="_blank" class="btn bg-yellow btn-lg font-bold p-3" href="https://wa.me/+6281181251717?text=Halo%2C%20saya%20mau%20pesan%20buku%20<?= urlencode($detail['title']); ?>">Order via WA</a></p>

      </div>
      <div class="col-md-3">
        <table>
          <tbody>
            <tr>
              <td width="110">Date</td>
              <td>:</td>
              <td><?= date('d F Y', strtotime(@$detail['created'])); ?></td>
            </tr>
            <tr rowspan="2">
              <td width="110">Written by</td>
              <td>:</td>
              <td><?= @$detail['author']; ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Share Publication</td>
            </tr>
            <tr>
              <td colspan="3">
                <div class="share-wrapper" style="margin-top: 10px;width: 100%">
                  <a target="_blank" class="mr-2" href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode(is_https() ? "https://" : "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>" target="_blank"><img class="social-img" src="<?= base_url('assets/images/footer-assets/facebook.png'); ?>" alt="" width="25"></a>
                  <a target="_blank" class="mr-2" href="mailto:?subject=Check this publication article from NBRI about <?= (array_key_exists('title', $detail)) ? $detail['title'] : $detail['category_name']; ?>&body=This is the url: <?php echo is_https() ? "https://" : "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><img class="social-img" src="<?= base_url('assets/images/footer-assets/mail.png'); ?>" alt="" width="25"></a>
                  <a target="_blank" class="mr-2" href="http://twitter.com/share?text=Check this publication article from NBRI about <?= (array_key_exists('title', $detail)) ? $detail['title'] : $detail['category_name']; ?>&url=<?php echo is_https() ? "https://" : "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"><img class="social-img" src="<?= base_url('assets/images/footer-assets/twitter.png'); ?>" alt="" width="25"></a>
                  <a target="_blank" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo urlencode(is_https() ? "https://" : "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']); ?>"><img class="social-img" src="<?= base_url('assets/images/footer-assets/linkedin.png'); ?>" width="25"></a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <hr>
    </div>
  </div>
</div>
<div class="content-section">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="section-title">Other Publication</h2>
      </div>
    </div>
    <div class="row">
      <?php if (!empty($publication_list)) : ?>
        <div class="col-md-10 offset-md-1 grid-wrapper" style="justify-content: flex-start; flex-wrap: no-wrap">
          <?php foreach ($publication_list as $publication) : ?>
            <?php
            $content = strip_tags(stripslashes(html_entity_decode($publication['synopsis'])));
            $img = ($publication['img'] != NULL && $publication['img'] != '') ? base_url('assets/images/publication/' . $publication['img']) : base_url('assets/images/nbri_new_logo.png');
            ?>
            <div class="grid-article-wrapper" style="height: auto !important">
              <div class="img-wrapper d-flex justify-content-end flex-column" style="background-image: url(<?= $img; ?>); min-height: 476px;">
                <?php if (!empty($publication['tags'])) : ?>
                  <div class="tag-container d-flex justify-content-end flex-row mb-3 mr-3">
                    <?php foreach ($tags as $tag) : ?>
                      <span class="font-size-small bg-yellow p-2 rounded ml-2" style=""><?= $tag; ?></span>
                    <?php endforeach; ?>
                  </div>
                <?php endif; ?>
              </div>
              <div class="caption-wrapper text-left" style="height: auto">
                <h4 class="article-title"><?= word_limiter($publication['title'], 7); ?></h4>
                <p class="article-description font-size-small"><?= word_limiter($content, 15); ?></p>
              </div>
              <div class="readmore-wrapper">
                <a href="<?= site_url('press/detail/' . $publication['slug']); ?>" class="readmore" title="">
                  <span>Read More</span>
                  <i class="fas fa-arrow-right"></i>
                </a>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      <?php else : ?>
        <div class="col-md-12">
          <h4>There's no other publication yet.</h4>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>