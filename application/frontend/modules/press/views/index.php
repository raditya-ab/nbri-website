<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(<?= $page_banner; ?>), url(<?= base_url('assets/images/nbri_new_logo.png'); ?>">
                <h1 class="page-title"><?= $page_title; ?></h1>
            </div>
        </div>
    </div>
</section>

<?php if (!empty($publication_list)) : ?>
    <section class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 grid-wrapper">
                    <?php foreach ($publication_list as $publication) : ?>
                        <?php
                        $content = strip_tags(stripslashes(html_entity_decode($publication['synopsis'])));
                        $img = ($publication['img'] != NULL && $publication['img'] != '') ? base_url('assets/images/publication/' . $publication['img']) : base_url('assets/images/nbri_new_logo.png');
                        $tags = $publication['tags'] ? explode(',', $publication['tags']) : array();
                        ?>
                        <div class="grid-article-wrapper" style="height: auto !important">
                            <div class="img-wrapper d-flex justify-content-end flex-column" style="background-image: url(<?= $img; ?>); min-height: 476px;">
                                <?php if(!empty($publication['tags'])) : ?>
                                    <div class="tag-container d-flex justify-content-end flex-row mb-3 mr-3">
                                        <?php foreach($tags as $tag):?>
                                            <span class="font-size-small bg-yellow p-2 rounded ml-2" style=""><?=$tag;?></span>
                                        <?php endforeach;?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="caption-wrapper text-left" style="height: auto">
                                <h4 class="article-title"><?= word_limiter($publication['title'], 7); ?></h4>
                                <p class="article-description font-size-small"><?= word_limiter($content, 15); ?></p>
                            </div>
                            <div class="readmore-wrapper">
                                <a href="<?= site_url('press/detail/' . $publication['slug']); ?>" class="readmore" title="">
                                    <span>Read More</span>
                                    <i class="fas fa-arrow-right"></i>
                                </a>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
<?php else : ?>
    <section class="content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center font-color-navy">
                    <h3 class="section-title">
                        There's no content yet
                    </h3>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>