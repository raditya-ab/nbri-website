<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publication extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('publication_m');
        $this->template->set_layout('default_template');
    }

    public function index()
    {
        $this->template->set('page_title', 'Publication');
        $this->template->set('page_banner', base_url('assets/images/publication_banner.jpg'));
        $this->template->build('index', $this->data);
    }

    public function catalogue()
    {
        $this->data['publication_list'] = $this->publication_m->get_publications();

        $this->template->set('page_title', 'Publication');
        $this->template->set('page_banner', base_url('assets/images/publication_banner.jpg'));
        $this->template->build('publication', $this->data);
    }
}

/* End of file Publication.php */
/* Location: ./application/frontend/modules/publication/controllers/Publication.php */
