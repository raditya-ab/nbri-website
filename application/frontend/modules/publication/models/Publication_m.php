<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publication_m extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_publications()
    {
        $this->db->order_by('created', 'DESC');
        $this->db->where('status', '1');
        return $this->db->get('publication')->result_array();
    }

}

/* End of file Publication_m.php */
/* Location: ./application/frontend/modules/publication/models/Publication_m.php */