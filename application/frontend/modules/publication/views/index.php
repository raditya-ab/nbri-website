<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(<?=$page_banner;?>)">
                <h1 class="page-title"><?=$page_title;?></h1>
            </div>
        </div>
    </div>
</section>

<section class="bg-green d-flex" style="min-height: 25em; padding: 25px 0">
    <div class="container d-flex" style="min-height: 100%">
        <div class="row d-flex align-items-stretch">
            <div class="col-md-5" tyle="min-height: 100%">
                <div class="image-hero" style="min-height: 300px;background: url(<?php echo site_url('assets/images/jbrev.png');?>) center no-repeat; background-size: contain">&nbsp;</div>
            </div>
            <div class="col-md-7 align-self-center">
                <p class="text-justify font-size-regular ">
                JBREV is established in 2022 by the National Battery Research Institute (NBRI) in collaboration with the Queen Mary University of London, Material Research Society Indonesia (MRS-INA) and International Union of Materia Research Societies (IUMRS). The JBREV is devoted to publish new and original research, article review relaton to battery materials, science &amp; engineering that applicable to renewable energy and electric vehicles.
                </p>
                <br>
                <br>
                <a href="https://journal.n-bri.org/" target="_blank" class="btn btn-default btn-block font-size-medium" style="background-color: #FFF; color: #181d62">Submit your paper today!</a>
            </div>

        </div>
    </div>
</section>
<section class="d-flex" style="min-height: 25em; padding: 25px 0">
    <div class="container d-flex" style="min-height: 100%">
        <div class="row d-flex align-items-stretch">
            <div class="col-md-5" tyle="min-height: 100%">
                <div class="image-hero" style="min-height: 300px;background: url(<?php echo site_url('assets/images/nbri_press.png');?>) center no-repeat; background-size: contain">&nbsp;</div>
            </div>
            <div class="col-md-7 align-self-center">
                <p class="text-justify font-size-regular ">
                NBRI Press or "Penerbit Yayasan Pusat Unggulan Inovasi Baterai dan Energi Terbarukan" is a publishing house under National Battery Research Institute. NBRI publishes JBREV, scientific &amp; popular book, module and handbook focusing on battery technology, renewable energy and electric vehicles. NBRI Press also covers various topics as long as it is beneficial for society.
                </p>
                <br>
                <br>
                <a href="<?=site_url('press');?>" class="btn btn-yellow btn-block font-size-medium">Check our catalogues</a>
            </div>

        </div>
    </div>
</section>
<section class="bg-navy d-flex" style="min-height: 25em; padding: 25px 0;">
    <div class="container d-flex" style="min-height: 100%">
        <div class="row d-flex align-items-stretch">
            <div class="col-md-5" tyle="min-height: 100%">
                <div class="image-hero" style="min-height: 300px;background: url(<?php echo site_url('assets/images/pub.jpeg');?>) center no-repeat; background-size: contain">&nbsp;</div>
            </div>
            <div class="col-md-7 align-self-center">
                <p class="text-justify font-size-regular ">
                As a leading think-thank in battery technology, NBRI provides various research project from upstream to downstream of battery supply chain, as well as its application for renewable energy and electric vehicles. Some research has been published and indexed by Scopus.
                </p>
                <br>
                <br>
                <a href="<?php echo base_url('publication/catalogue');?>" class="btn bg-green btn-block font-size-medium text-navy">Check our research publication</a>
            </div>

        </div>
    </div>
</section>