<section id="page-hero">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 hero-container" style="background-image: url(<?=$page_banner;?>)">
                <h1 class="page-title"><?=$page_title;?></h1>
            </div>
        </div>
    </div>
</section>

<style type="text/css" media="screen">
    .block-card
    {
        margin-bottom: 10px;
        border: none;
        border-radius: none;
    }


</style>
<section class="content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 d-flex justify-content-center">
                <?php if(!empty($publication_list)): ?>
                <div class="accordion" id="accordion_list" style="width: 100%;">
                    <?php foreach($publication_list as $i=> $publication): ?>
                        <div class="card block-card">
                            <div class="card-header bg-navy" id="<?='publication-'.$i;?>" style="display: flex; justify-content: space-between;align-items: center">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#<?='card-'.$i;?>" aria-expanded="true" aria-controls="<?='card-'.$i;?>">
                                        <?=$publication['name'];?>
                                    </button>
                                </h2>
                                <h2 class="mb-0">
                                    <button class="btn btn-link btn-arrow" type="button" data-toggle="collapse" data-target="#<?='card-'.$i;?>" aria-expanded="true" aria-controls="<?='card-'.$i;?>">
                                        <i class="fas fa-chevron-down"></i>
                                    </button>
                                </h2>

                            </div>
                            <div id="<?='card-'.$i;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body bg-yellow pb-5">
                                    <?=stripslashes(html_entity_decode($publication['description']));?>
                                    <br>
                                    
                                    <a href="<?=base_url('assets/images/publication/'.$publication['filename']);?>" title="">Download Publication Here.</a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php else: ?>
                    <h3 class="section-title">There's no publication yet.</h3>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>