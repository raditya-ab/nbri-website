<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
    public function __construct()
    {
        parent::__construct();
    }
    public function verify_login()
    {
        if(!$this->aauth->is_loggedin()){
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|min_length[5]|trim|strip_tags|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|trim|strip_tags|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $output = array(
                    'status' => 0,
                    'message' => array(
                        'text' => validation_errors()
                    )
                );
            } else {
                $email = $this->input->post('email', TRUE);
                $password = $this->input->post('password', TRUE);

                if($this->aauth->login($email, $password)){
                    $output = array(
                        'status' => 1,
                        'message' => array(
                            'text' => 'Login Successful, redirecting you back to homepage in 3 seconds..')
                    );
                } else {
                    $output = array(
                        'status' => 0,
                        'message' => array(
                            'text' => $this->aauth->print_errors()
                        )
                    );
                }
            }

            $this->output->set_content_type('application/json')->set_output(json_encode($output));

        } else {
            redirect('membership/login','refresh');
        }
    }
}

/* End of file Login.php */
/* Location: ./application/backend/modules/user/controllers/Login.php */