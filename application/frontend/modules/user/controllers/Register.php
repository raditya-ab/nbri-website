<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MX_Controller {
    public function __construct()
    {
        parent::__construct();
        if($this->aauth->is_loggedin())
            redirect(site_url().'refresh');
    }

    public function index()
    {
        $this->form_validation->set_rules('membership_type', 'Membership Type', 'trim|required|in_list[4,5,6]|strip_tags|xss_clean');
        $this->form_validation->set_rules('domain', 'Domain', 'trim|required|in_list[domestic,international]|strip_tags|xss_clean');
        $this->form_validation->set_rules('membership_role', 'Role', 'trim|required|in_list[7,8,9]|strip_tags|xss_clean');
        $this->form_validation->set_rules('membership_plan', 'Membership Plan', 'trim|required|in_list[0,1]|strip_tags|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[aauth_users.email]|valid_email|strip_tags|xss_clean');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[aauth_users.username]|min_length[5]|max_length[12]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[20]|strip_tags|callback_password_check');
        $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[1]');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|max_length[50]|strip_tags|xss_clean');
        $this->form_validation->set_rules('salutation', 'Salutation', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('affiliation', 'Affiliation', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('research_focus', 'Research Focus Area', 'trim|strip_tags|xss_clean');
        $this->form_validation->set_rules('expertise', 'Expertise', 'trim|strip_tags|xss_clean');

        if ($this->form_validation->run($this) == FALSE) {
            $output = array(
                'status' => 0,
                'message' => array(
                    'text' => validation_errors()
                )
            );
        } else {
            $input = array_map(array($this,'cleanse_input'), $this->input->post());
            if($user_id = $this->aauth->create_user($input['email'],$input['password'], $input['username'])){
                $this->aauth->add_member($user_id,$input['membership_type']);
                $this->aauth->add_member($user_id,$input['membership_role']);

                $this->aauth->set_user_var('domain',$input['domain'], $user_id);
                $this->aauth->set_user_var('membership_plan',$input['membership_plan'], $user_id);
                $this->aauth->set_user_var('first_name',$input['first_name'], $user_id);
                $this->aauth->set_user_var('last_name',$input['last_name'], $user_id);
                $this->aauth->set_user_var('salutation',$input['salutation'], $user_id);
                $this->aauth->set_user_var('affiliation',$input['affiliation'], $user_id);
                $this->aauth->set_user_var('research_focus',$input['research_focus'], $user_id);
                $this->aauth->set_user_var('expertise',$input['expertise'], $user_id);
                $this->aauth->set_user_var('address', NULL, $user_id);
                $this->aauth->set_user_var('website', NULL, $user_id);
                $this->aauth->set_user_var('description', NULL, $user_id);
                $this->aauth->set_user_var('publication', json_encode(array()), $user_id);
                $this->aauth->set_user_var('profile_pic', NULL, $user_id);

                $output = array(
                    'status' => 1,
                    'message' => array(
                        'text' => 'Register User Success, redirecting you back to homepage in 3 seconds..'
                    )
                );

                $this->aauth->login_fast($user_id);
            } else {
                $output = array(
                    'status' => 0,
                    'message' => array(
                        'text' => $this->aauth->print_errors()
                    )
                );
            }

        }
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
    }

    public function cleanse_input($input)
    {
        return $this->security->xss_clean($input);
    }

    public function password_check($str)
    {
        $flag = 0;
        $message = '';

        if(trim($str) != '' || $str != NULL)
        {
            if(strlen($str) >= 8 && strlen($str) <= 20){
                $flag++;
            } else {
                $message .= 'Password Length must be 8-20 characters<br>';
            }

            if(preg_match('/\d/',$str)){
                $flag++;
            } else {
                $message .= 'Password must contain at least one number<br>';
            }

            if(preg_match('/[a-z]/',$str)){
                $flag++;
            } else {
                $message .= 'Password must contain at least one lowercase letter<br>';
            }

            if(preg_match('/[A-Z]/',$str)){
                $flag++;
            } else {
                $message .= 'Password must contain at least one uppercase letter<br>';
            }

            if(preg_match('/[\=\+*.,_\-!@#$%^&]/',$str)){
                $flag++;
            } else {
                $message .= 'Password must contain at least one symbol (!@#$%^&*-+=_)<br>';
            }
        }

        if($flag >= 5)
        {
            return TRUE;
        } else {
            $this->form_validation->set_message('password_check',$message);
            return FALSE;
        }
    }
}

/* End of file Register.php */
/* Location: ./application/frontend/modules/user/controllers/Register.php */