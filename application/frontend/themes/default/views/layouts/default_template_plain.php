<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">

    <!-- Search Engine -->
    <meta name="description" content="The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education.">
    <meta name="image" content="<?=base_url('assets/images/082742d33ff1187bd3f12ebb5b000e77.png');?>">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="<?=@$template['title'];?> - National Battery Research Institute">
    <meta itemprop="description" content="The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education.">
    <meta itemprop="image" content="<?=base_url('assets/images/082742d33ff1187bd3f12ebb5b000e77.png');?>">

     <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="NBRI - National Battery Research Institute">
    <meta property="og:url" content="http://n-bri.org/">
    <meta property="og:locale" content="en_US">
    <meta property="og:title" content="<?=@$template['title'];?> - National Battery Research Institute">
    <meta property="og:description" content="The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education.">
    <meta property="og:image" content="<?=base_url('assets/images/082742d33ff1187bd3f12ebb5b000e77.png');?>">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="http://n-bri.org/">
    <meta property="twitter:title" content="<?=@$template['title'];?> - National Battery Research Institute">
    <meta property="twitter:description" content="The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education.">
    <meta property="twitter:site" content="@NBRI_Indonesia">
    <meta property="twitter:creator" content="@NBRI_Indonesia">
    <meta property="twitter:image:src" content="<?=base_url('assets/images/082742d33ff1187bd3f12ebb5b000e77.png');?>">
    <!-- Bootstrap CSS -->
    <title><?=@$template['title'];?> - National Battery Research Institute</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css' integrity='sha512-P5MgMn1jBN01asBgU0z60Qk4QxiXo86+wlFahKrsQf37c9cro517WzVSPPV1tDKzhku2iJ2FVgL67wG03SGnNA==' crossorigin='anonymous'/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.0.2/sweetalert2.min.css" integrity="sha512-NU255TKQ55xzDS6UHQgO9HQ4jVWoAEGG/lh2Vme0E2ymREox7e8qwIfn6BFem8lbahhU9E2IQrHZlFAxtKWH2Q==" crossorigin="anonymous" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/three-dots/0.2.0/three-dots.min.css" integrity="sha512-mWd7xkyVPqynhBSpiU8IRd15BjgbAJGNo24BuboWM1f290funR3WYPYWNrDx8yMAwOuJpOF5v8Kt3nYTyFjr0Q==" crossorigin="anonymous" />
    <link href="<?=base_url('assets\vendors\jquery-smartwizard-master\dist\css\smart_wizard_all.min.css');?>" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/navbar.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/event.css');?>">

    
    <script src="https://kit.fontawesome.com/e7c6f13eeb.js" crossorigin="anonymous"></script>

    <?php if(ENVIRONMENT != NULL):?>
        <?php $active_env = strtolower(ENVIRONMENT); ?>
        <?php if($active_env == 'production'): ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178037418-1"></script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-178037418-1');
            </script>
        <?php endif; ?>
    <?php endif; ?>
</head>

<body>
    <?php echo @$template['partials']['header']; ?>
    <?php echo $template['body']; ?>
    <?php echo @$template['partials']['sponsors'];?>
    <?php echo @$template['partials']['email_dropbox'];?>
    <?php echo @$template['partials']['footer']; ?>
    <!-- Addons -->
    <a href="#" id="btn-date-sticky" class="btn bg-green text-navy d-flex align-items-center">
        <svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M33.7477 31.143L31.7188 29.1195V22.7812C31.7169 21.5889 31.4155 20.4162 30.8421 19.3708C30.2688 18.3254 29.4419 17.4409 28.4375 16.7984V4.1875C28.4375 4.04246 28.3799 3.90336 28.2773 3.8008C28.1748 3.69824 28.0357 3.64062 27.8906 3.64062H25.1562V2.54688C25.1562 2.40183 25.0986 2.26273 24.9961 2.16018C24.8935 2.05762 24.7544 2 24.6094 2H21.3281C21.1831 2 21.044 2.05762 20.9414 2.16018C20.8389 2.26273 20.7812 2.40183 20.7812 2.54688V3.64062H8.75V2.54688C8.75 2.40183 8.69238 2.26273 8.58982 2.16018C8.48727 2.05762 8.34817 2 8.20312 2H4.92188C4.77683 2 4.63774 2.05762 4.53518 2.16018C4.43262 2.26273 4.375 2.40183 4.375 2.54688V3.64062H1.64062C1.49558 3.64062 1.35648 3.69824 1.25393 3.8008C1.15137 3.90336 1.09375 4.04246 1.09375 4.1875V27.7031C1.09375 27.8482 1.15137 27.9873 1.25393 28.0898C1.35648 28.1924 1.49558 28.25 1.64062 28.25H17.5V29.1195L15.4711 31.143C15.3952 31.2199 15.3438 31.3175 15.3234 31.4236C15.3029 31.5297 15.3144 31.6395 15.3562 31.7391C15.3965 31.8395 15.466 31.9255 15.5557 31.986C15.6454 32.0464 15.7512 32.0785 15.8594 32.0781H21.3773C21.5059 32.8424 21.9011 33.5364 22.4928 34.0369C23.0845 34.5373 23.8344 34.812 24.6094 34.812C25.3844 34.812 26.1343 34.5373 26.726 34.0369C27.3177 33.5364 27.7129 32.8424 27.8414 32.0781H33.3594C33.4676 32.0785 33.5734 32.0464 33.6631 31.986C33.7528 31.9255 33.8223 31.8395 33.8625 31.7391C33.9044 31.6395 33.9158 31.5297 33.8954 31.4236C33.8749 31.3175 33.8235 31.2199 33.7477 31.143ZM21.875 3.09375H24.0625V5.28125C24.0625 5.57133 23.9473 5.84953 23.7421 6.05465C23.537 6.25977 23.2588 6.375 22.9688 6.375C22.6787 6.375 22.4005 6.25977 22.1954 6.05465C21.9902 5.84953 21.875 5.57133 21.875 5.28125V3.09375ZM5.46875 3.09375H7.65625V5.28125C7.65625 5.57133 7.54102 5.84953 7.3359 6.05465C7.13078 6.25977 6.85258 6.375 6.5625 6.375C6.27242 6.375 5.99422 6.25977 5.7891 6.05465C5.58398 5.84953 5.46875 5.57133 5.46875 5.28125V3.09375ZM2.1875 4.73438H4.375V5.28125C4.375 5.86141 4.60547 6.41781 5.0157 6.82805C5.42594 7.23828 5.98234 7.46875 6.5625 7.46875C7.14266 7.46875 7.69906 7.23828 8.1093 6.82805C8.51953 6.41781 8.75 5.86141 8.75 5.28125V4.73438H20.7812V5.28125C20.7812 5.86141 21.0117 6.41781 21.422 6.82805C21.8322 7.23828 22.3886 7.46875 22.9688 7.46875C23.5489 7.46875 24.1053 7.23828 24.5155 6.82805C24.9258 6.41781 25.1562 5.86141 25.1562 5.28125V4.73438H27.3438V8.5625H2.1875V4.73438ZM2.1875 27.1562V9.65625H27.3438V15.6719C27.3438 14.9467 27.0557 14.2512 26.5429 13.7384C26.0301 13.2256 25.3346 12.9375 24.6094 12.9375C23.8842 12.9375 23.1887 13.2256 22.6759 13.7384C22.1631 14.2512 21.875 14.9467 21.875 15.6719V16.2188C20.5807 16.7604 19.4751 17.6721 18.6969 18.8396C17.9186 20.007 17.5022 21.3782 17.5 22.7812V27.1562H2.1875ZM26.25 15.6719V15.8688C25.172 15.6063 24.0467 15.6063 22.9688 15.8688V15.6719C22.9688 15.2368 23.1416 14.8195 23.4493 14.5118C23.757 14.2041 24.1743 14.0312 24.6094 14.0312C25.0445 14.0312 25.4618 14.2041 25.7695 14.5118C26.0771 14.8195 26.25 15.2368 26.25 15.6719ZM24.6094 33.7188C24.1256 33.7172 23.6561 33.5553 23.2741 33.2585C22.8922 32.9616 22.6194 32.5465 22.4984 32.0781H26.7203C26.5994 32.5465 26.3266 32.9616 25.9446 33.2585C25.5627 33.5553 25.0931 33.7172 24.6094 33.7188ZM17.1773 30.9844L18.4352 29.732C18.5359 29.6278 18.5927 29.4887 18.5938 29.3438V22.7812C18.5938 21.1858 19.2275 19.6557 20.3557 18.5276C21.4838 17.3994 23.0139 16.7656 24.6094 16.7656C26.2048 16.7656 27.7349 17.3994 28.8631 18.5276C29.9912 19.6557 30.625 21.1858 30.625 22.7812V29.3438C30.6261 29.4887 30.6829 29.6278 30.7836 29.732L32.0414 30.9844H17.1773Z" fill="#181D62"/>
            <path d="M8.20312 10.75H4.92188C4.77683 10.75 4.63774 10.8076 4.53518 10.9102C4.43262 11.0127 4.375 11.1518 4.375 11.2969V14.5781C4.375 14.7232 4.43262 14.8623 4.53518 14.9648C4.63774 15.0674 4.77683 15.125 4.92188 15.125H8.20312C8.34817 15.125 8.48727 15.0674 8.58982 14.9648C8.69238 14.8623 8.75 14.7232 8.75 14.5781V11.2969C8.75 11.1518 8.69238 11.0127 8.58982 10.9102C8.48727 10.8076 8.34817 10.75 8.20312 10.75ZM7.65625 14.0312H5.46875V11.8438H7.65625V14.0312Z" fill="#181D62"/>
            <path d="M12.0313 10.75C11.5986 10.75 11.1757 10.8783 10.8159 11.1187C10.4562 11.359 10.1758 11.7007 10.0103 12.1004C9.8447 12.5001 9.80138 12.9399 9.88578 13.3643C9.97019 13.7886 10.1785 14.1784 10.4845 14.4843C10.7904 14.7902 11.1802 14.9986 11.6045 15.083C12.0288 15.1674 12.4687 15.1241 12.8684 14.9585C13.2681 14.7929 13.6097 14.5125 13.8501 14.1528C14.0905 13.7931 14.2188 13.3701 14.2188 12.9375C14.2188 12.3573 13.9883 11.8009 13.578 11.3907C13.1678 10.9805 12.6114 10.75 12.0313 10.75ZM12.0313 14.0312C11.8149 14.0312 11.6035 13.9671 11.4236 13.8469C11.2437 13.7267 11.1035 13.5559 11.0208 13.3561C10.938 13.1562 10.9163 12.9363 10.9585 12.7241C11.0007 12.512 11.1049 12.3171 11.2579 12.1641C11.4108 12.0111 11.6057 11.907 11.8179 11.8648C12.03 11.8226 12.25 11.8442 12.4498 11.927C12.6497 12.0098 12.8205 12.15 12.9407 12.3298C13.0609 12.5097 13.125 12.7212 13.125 12.9375C13.125 13.2276 13.0098 13.5058 12.8046 13.7109C12.5995 13.916 12.3213 14.0312 12.0313 14.0312Z" fill="#181D62"/>
            <path d="M19.7043 12.9206C19.7043 12.4879 19.5761 12.065 19.3357 11.7053C19.0953 11.3456 18.7537 11.0652 18.354 10.8996C17.9543 10.734 17.5144 10.6907 17.0901 10.7751C16.6658 10.8595 16.276 11.0679 15.9701 11.3738C15.6641 11.6797 15.4558 12.0695 15.3714 12.4938C15.287 12.9182 15.3303 13.358 15.4959 13.7577C15.6614 14.1574 15.9418 14.4991 16.3015 14.7394C16.6613 14.9798 17.0842 15.1081 17.5168 15.1081C18.0968 15.1074 18.6528 14.8767 19.0628 14.4666C19.4729 14.0565 19.7036 13.5005 19.7043 12.9206ZM16.4231 12.9206C16.4231 12.7043 16.4872 12.4928 16.6074 12.3129C16.7276 12.1331 16.8984 11.9929 17.0983 11.9101C17.2981 11.8273 17.5181 11.8057 17.7302 11.8479C17.9424 11.8901 18.1373 11.9942 18.2902 12.1472C18.4432 12.3002 18.5474 12.495 18.5896 12.7072C18.6318 12.9194 18.6101 13.1393 18.5273 13.3392C18.4446 13.539 18.3044 13.7098 18.1245 13.83C17.9446 13.9502 17.7332 14.0143 17.5168 14.0143C17.2268 14.0143 16.9486 13.8991 16.7434 13.694C16.5383 13.4889 16.4231 13.2107 16.4231 12.9206Z" fill="#181D62"/>
            <path d="M12.0313 16.2188C11.5986 16.2188 11.1757 16.347 10.8159 16.5874C10.4562 16.8278 10.1758 17.1694 10.0103 17.5691C9.8447 17.9688 9.80138 18.4087 9.88578 18.833C9.97019 19.2573 10.1785 19.6471 10.4845 19.953C10.7904 20.259 11.1802 20.4673 11.6045 20.5517C12.0288 20.6361 12.4687 20.5928 12.8684 20.4272C13.2681 20.2617 13.6097 19.9813 13.8501 19.6216C14.0905 19.2618 14.2188 18.8389 14.2188 18.4062C14.2188 17.8261 13.9883 17.2697 13.578 16.8595C13.1678 16.4492 12.6114 16.2188 12.0313 16.2188ZM12.0313 19.5C11.8149 19.5 11.6035 19.4359 11.4236 19.3157C11.2437 19.1955 11.1035 19.0247 11.0208 18.8248C10.938 18.625 10.9163 18.405 10.9585 18.1929C11.0007 17.9807 11.1049 17.7858 11.2579 17.6329C11.4108 17.4799 11.6057 17.3757 11.8179 17.3335C12.03 17.2913 12.25 17.313 12.4498 17.3958C12.6497 17.4785 12.8205 17.6187 12.9407 17.7986C13.0609 17.9785 13.125 18.1899 13.125 18.4062C13.125 18.6963 13.0098 18.9745 12.8046 19.1796C12.5995 19.3848 12.3213 19.5 12.0313 19.5Z" fill="#181D62"/>
            <path d="M8.20312 16.2188H4.92188C4.77683 16.2188 4.63774 16.2764 4.53518 16.3789C4.43262 16.4815 4.375 16.6206 4.375 16.7656V20.0469C4.375 20.1919 4.43262 20.331 4.53518 20.4336C4.63774 20.5361 4.77683 20.5938 4.92188 20.5938H8.20312C8.34817 20.5938 8.48727 20.5361 8.58982 20.4336C8.69238 20.331 8.75 20.1919 8.75 20.0469V16.7656C8.75 16.6206 8.69238 16.4815 8.58982 16.3789C8.48727 16.2764 8.34817 16.2188 8.20312 16.2188ZM7.65625 19.5H5.46875V17.3125H7.65625V19.5Z" fill="#181D62"/>
            <path d="M12.0313 21.6875C11.5986 21.6875 11.1757 21.8158 10.8159 22.0562C10.4562 22.2965 10.1758 22.6382 10.0103 23.0379C9.8447 23.4376 9.80138 23.8774 9.88578 24.3018C9.97019 24.7261 10.1785 25.1159 10.4845 25.4218C10.7904 25.7277 11.1802 25.9361 11.6045 26.0205C12.0288 26.1049 12.4687 26.0616 12.8684 25.896C13.2681 25.7304 13.6097 25.45 13.8501 25.0903C14.0905 24.7306 14.2188 24.3076 14.2188 23.875C14.2188 23.2948 13.9883 22.7384 13.578 22.3282C13.1678 21.918 12.6114 21.6875 12.0313 21.6875ZM12.0313 24.9687C11.8149 24.9687 11.6035 24.9046 11.4236 24.7844C11.2437 24.6642 11.1035 24.4934 11.0208 24.2936C10.938 24.0937 10.9163 23.8738 10.9585 23.6616C11.0007 23.4495 11.1049 23.2546 11.2579 23.1016C11.4108 22.9486 11.6057 22.8445 11.8179 22.8023C12.03 22.7601 12.25 22.7817 12.4498 22.8645C12.6497 22.9473 12.8205 23.0875 12.9407 23.2673C13.0609 23.4472 13.125 23.6587 13.125 23.875C13.125 24.1651 13.0098 24.4433 12.8046 24.6484C12.5995 24.8535 12.3213 24.9687 12.0313 24.9687Z" fill="#181D62"/>
            <path d="M8.20312 21.6875H4.92188C4.77683 21.6875 4.63774 21.7451 4.53518 21.8477C4.43262 21.9502 4.375 22.0893 4.375 22.2344V25.5156C4.375 25.6607 4.43262 25.7998 4.53518 25.9023C4.63774 26.0049 4.77683 26.0625 4.92188 26.0625H8.20312C8.34817 26.0625 8.48727 26.0049 8.58982 25.9023C8.69238 25.7998 8.75 25.6607 8.75 25.5156V22.2344C8.75 22.0893 8.69238 21.9502 8.58982 21.8477C8.48727 21.7451 8.34817 21.6875 8.20312 21.6875ZM7.65625 24.9688H5.46875V22.7812H7.65625V24.9688Z" fill="#181D62"/>
        </svg>
        <span class="ml-2">Important Dates</span>
    </a>

    <div id="sticky-date-window" class="popup-sticky text-navy bg-green">
        <div class="pop-up-header">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <a href="#" class="btn-close">
                        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M30 1.76757L28.2324 0L15 13.2324L1.76757 0L0 1.76757L13.2324 15L0 28.2324L1.76757 30L15 16.7676L28.2324 30L30 28.2324L16.7676 15L30 1.76757Z" fill="#181D62"/>
                        </svg>
                    </a>
                    <h2 style="font-size: 16px;font-weight:800; text-decoration: underline">Important Dates</h2>
                </div>
            </div>
        </div>
        <div class="pop-up-body text-right mt-3">
            <ul class="unstyled-list" style="list-style: none">
                <li>
                    <b>Abstract Submission Deadline</b>
                    <div>June 07, 2021</div>
                </li>
                <li>
                    <b>Abstract Acceptance Notification</b>
                    <div> June 12,2021</div>
                </li>
                <li>
                    <b>Early Bird Registration Deadline</b>
                    <div>June 14, 2021</div>
                </li>
                <li>
                    <b>Full Paper Submission Deadline</b>
                    <div>June 24, 2021</div>
                </li>
                <li>
                    <b>Conference Date</b>
                    <div>June 22 - 24, 2021</div>
                </li>
            </ul>
        </div>
    </div>

    <a href="<?php echo site_url('event/icb-rev-2021/agenda');?>" id="btn-register-sticky" class="btn btn-md bg-yellow text-navy">
        Register Now <i class="fas fa-arrow-right"></i>
    </a>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-mobile/1.4.5/jquery.mobile.min.js' integrity='sha512-FbnNPigZZCDhMM22ezvan3xN2/E/oBd9GxqtpiBcK1VZuzBAt0V1EmTtteoYuYxWrqwqBA6zb1OTB887QSjAsw==' crossorigin='anonymous'></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js' integrity='sha512-XKa9Hemdy1Ui3KSGgJdgMyYlUg1gM+QhL6cnlyTe2qzMCYm4nAZ1PsVerQzTTXzonUR+dmswHqgJPuwCq1MaAg==' crossorigin='anonymous'></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js" integrity="sha512-YUkaLm+KJ5lQXDBdqBqk7EVhJAdxRnVdT2vtCzwPHSweCzyMgYV/tgGF4/dCyqtCC2eCphz0lRQgatGVdfR0ww==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.0.2/sweetalert2.all.min.js" integrity="sha512-vv+P7rUgMUz0h083AoSfE5UcQS32P1IxVPeGXlN62MOFqZDeW3Zh6TXppvcgU1PgWzFa6VnP4UqjhEFCHjw3pA==" crossorigin="anonymous"></script>
    <script src="<?php echo base_url('assets/vendors/countdownjs/simplyCountdown.min.js');?>"></script>
    <script src="<?=base_url('assets\vendors\jquery-smartwizard-master\dist\js\jquery.smartWizard.min.js');?>" type="text/javascript"></script>

    <script src="https://hcaptcha.com/1/api.js?hl=en" async defer></script>

    <?php if(ENVIRONMENT != NULL):?>
        <?php $active_env = strtolower(ENVIRONMENT); ?>
        <?php if($active_env == 'production'): ?>
            <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCjwCjFIDt3mh2WJs4I3tXr0ja3rKeandk"></script>
        <?php else: ?>
            <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58"></script>
        <?php endif; ?>
    <?php endif; ?>
    <script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script>
        var siteURL = "<?=site_url();?>";

        jQuery.fn.exists = function(){ return this.length > 0; }

        
    </script>

    
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
        (function () {
            var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/60991cb8b1d5182476b77635/1f5b1gdtl';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
        </script>
    <!--End of Tawk.to Script-->
    <script>
        function submitForm(token){
            $("input[name=y-captcha-response]").val(token);
            $("#form_drop_email").submit();
        }
        $("#form_drop_email").on("submit", function (e) {
            e.preventDefault();
            var options = {
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                datatype: 'json',
                clearForm: true,
                resetForm: true,
                beforeSubmit: function () {
                    Swal.fire({
                        title: 'Submitting',
                        html: "<center><div class='dot-typing mt-5 mb-5'></div></center>"
                    });
                },
                success: function (resp) {
                    var option = {
                        html: "<div class='text-center mt-5 mb-5'>" + resp.message.text + "</div>'"
                    }
                    
                    if (resp.status) {
                        option.title = 'Email Submitted!';
                        option.icon = 'success';
                        option.showCancelButton = false;
                        option.showConfirmButton = false;
                        option.allowOutsideClick = false;
                    } else {
                        option.title = 'Email not Submitted!';
                        option.icon = 'error'
                    }
                    
                    Swal.update(option);
                }
            }
            
            // console.log(options);
            $(this).ajaxSubmit(options);
            
            // return false;
        });
        </script>
        <script src="<?=base_url('assets/js/custom.js');?>" type="text/javascript" charset="utf-8"></script>
        <script src="<?=base_url('assets/js/event.js');?>" type="text/javascript" charset="utf-8"></script>
</body>

</html>