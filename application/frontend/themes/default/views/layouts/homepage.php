<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    
    <!-- Primary Meta Tags -->
    <title>NBRI - National Battery Research Institute</title>
    <meta name="title" content="NBRI - National Battery Research Institute">
    <meta name="description" content="The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education.">

   <!-- Search Engine -->
    <meta name="description" content="The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education.">
    <meta name="image" content="<?=base_url('assets/images/082742d33ff1187bd3f12ebb5b000e77.png');?>">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="NBRI - National Battery Research Institute">
    <meta itemprop="description" content="The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education.">
    <meta itemprop="image" content="<?=base_url('assets/images/082742d33ff1187bd3f12ebb5b000e77.png');?>">

     <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:site_property" content="NBRI - National Battery Research Institute">
    <meta property="og:url" content="http://n-bri.org/">
    <meta property="og:locale" content="en_US">
    <meta property="og:title" content="NBRI - National Battery Research Institute">
    <meta property="og:description" content="The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education.">
    <meta property="og:image" content="<?=base_url('assets/images/082742d33ff1187bd3f12ebb5b000e77.png');?>">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="http://n-bri.org/">
    <meta property="twitter:title" content="NBRI - National Battery Research Institute">
    <meta property="twitter:description" content="The National Battery Research Institute (NBRI) was legally established on 17th December 2020 as The Center of Excellence Innovation of Battery and Renewable Energy Foundation, with Prof.Dr. Evvy Kartini as a Founder and Prof Alan J. Drew as Co-Founder. NBRI is Indonesia's independent institute for electrochemical energy storage science and technology, supporting research, training, and education.">
    <meta property="twitter:site" content="@NBRI_Indonesia">
    <meta property="twitter:creator" content="@NBRI_Indonesia">
    <meta property="twitter:image:src" content="<?=base_url('assets/images/082742d33ff1187bd3f12ebb5b000e77.png');?>">
    <!-- Bootstrap CSS -->
    <title><?=@$template['title'];?> - National Battery Research Institute</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/navbar.css');?>">
    <script src="https://kit.fontawesome.com/e7c6f13eeb.js" crossorigin="anonymous"></script>

    <?php if(ENVIRONMENT != NULL):?>
        <?php $active_env = strtolower(ENVIRONMENT); ?>
        <?php if($active_env == 'production'): ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178037418-1"></script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-178037418-1');
            </script>
        <?php endif; ?>
    <?php endif; ?>

</head>

<body>
    <?php echo @$template['partials']['header']; ?>

    <?php echo modules::run('keypoints/slider'); ?>

    <?php echo $template['body']; ?>

    <?php   echo @$template['partials']['footer']; ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <?php if(ENVIRONMENT != NULL):?>
        <?php $active_env = strtolower(ENVIRONMENT); ?>
        <?php if($active_env == 'production'): ?>
            <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCjwCjFIDt3mh2WJs4I3tXr0ja3rKeandk"></script>
        <?php else: ?>
            <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58"></script>
        <?php endif; ?>
    <?php endif; ?>
    <!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script> -->
    <!-- <script src="jquery.counterup.min.js"></script> -->
    <script>
        function createCookie(name, value, days) {
            var expires;
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            }
            else {
                expires = "";
            }
            document.cookie = name + "=" + value + expires + "; path=/";
        }
        
        $(document).ready(function(){
            var acceptCookie = (document.cookie.match(/^(?:.*;)?\s*acceptCookie\s*=\s*([^;]+)(?:.*)?$/)||[,null])[1];

            if(acceptCookie == null){
                $('#privacyCookieModal').modal('show')
            }

            $("#btnAcceptCookie").click(function(e){
                e.preventDefault();

                createCookie('acceptCookie','true',1000);
                $('#privacyCookieModal').modal('hide')
            });
        })
    </script>
</body>

</html>