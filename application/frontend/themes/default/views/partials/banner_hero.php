<section id="page-hero" style="min-height: 400px; max-height: 400px">
    <div class="container-fluid">
        <div class="row" style="height: 100%">
            <div class="col-md-12 hero-container"
                style="background-image: url(<?=base_url('assets/images/banner/'.$banner_details['img']);?>); height: 400px; position: relative">
                <h1 class="page-title"
                    style="position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%);text-shadow: 3px 3px 20px black">
                    <?=$banner_details['caption'];?> <br>
                    <?php if(array_key_exists('subcaption',$banner_details)):?>
                        <?php foreach($banner_details['subcaption'] as $text):?>
                            <span style="font-size: <?=(array_key_exists('font-size', $text) ? $text['font-size']:'18px');?>"><?=$text['text'];?></span>
                            </br>
                        <?php endforeach;?>
                    <?php else:?>
                            <span style="font-size: 18px">Delivering a battery revolution - reducing the drivers of climate change in Indonesia<br>12th – 14th July 2021</span>
                    <?php endif;?>
                </h1>
                    
            </div>
        </div>
    </div>
</section>