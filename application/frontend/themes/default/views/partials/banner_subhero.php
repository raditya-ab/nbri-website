<section id="page-subtitle" class="bg-yellow">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>The Presidential regulation No.55/2019 states that the transportation in Indonesia will be transform
                    into 30% the electric vehicles (EV) in 2030, which based on lithium-ion battery (LIB) technology. In
                    order to accelerate the drive towards electric vehicles (EVs), it requires the optimisation of
                    lithium-ion battery technology. So far, the performance of current LIB has several limitations, and
                    they need some improvements for example the battery materials, battery chemistries, life cycle,
                    energy density, cost and safety. So, in the medium to long term, the innovation of new battery
                    chemistries and the commercialisation of new materials from the local resources will be very
                    crucial. Due to this reason, the National Battery Research Institute (NBRI) exits to gather all the
                    resources to achieve that goal.</p>
            </div>
        </div>
    </div>
</section>