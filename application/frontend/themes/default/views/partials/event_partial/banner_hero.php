<?php $page_banner_pic = (isset($page_banner)) ? 'assets/uploads/' . $this->uri->segment(2) . '/images/' . $page_banner : 'assets/images/banner_bg.png'; ?>

<section id="page-hero" class="banner-normal"
				 style="background-image: url(<?php echo base_url($page_banner_pic); ?>)">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12" style="background-color: rgba(0,0,0,0.4);">
				<div class="banner-content d-flex align-items-center justify-content-center">
					<h1 class="banner-title font-medium font-size-bannertitle"><?php echo $banner_title; ?></h1>
					<div class="banner-subtitle">
						<p class="font-size-bigger text-white"><i><?=(@$banner_subtitle !== null) ? @$banner_subtitle : "The International Conference on Battery for Renewable Energy and
								Electric Vehicles 2022";?></i></p>
					</div>
					<!-- <a href="#speakers-section" class="btn btn-readmore bg-yellow">View All Speakers</a> -->
				</div>
			</div>
		</div>
	</div>
</section>