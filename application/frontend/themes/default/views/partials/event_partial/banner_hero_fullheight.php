<!-- <section id="page-hero">
    <div class="container-fluid">
        <div class="row" style="height: 100vh">
            <div class="col-md-12 hero-container"
                style="background-image: url(<?= base_url('assets/images/banner/' . $banner_details['img']); ?>); height: 400px; position: relative">
                <h1 class="page-title"
                    style="position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%);text-shadow: 3px 3px 20px black">
                    <?= $banner_details['caption']; ?> <br>
                    <?php if (array_key_exists('subcaption', $banner_details)) : ?>
                        <?php foreach ($banner_details['subcaption'] as $text) : ?>
                            <span style="font-size: <?= (array_key_exists('font-size', $text) ? $text['font-size'] : '18px'); ?>"><?= $text['text']; ?></span>
                            </br>
                        <?php endforeach; ?>
                    <?php else : ?>
                            <span style="font-size: 18px">Delivering a battery revolution - reducing the drivers of climate change in Indonesia<br>12th – 14th July 2021</span>
                    <?php endif; ?>
                </h1>

            </div>
        </div>
    </div>
</section> -->
<section id="page-hero" class="banner-fullheight" style="max-height: 100% !important; background-image: url(<?php echo base_url('assets/uploads/'.$this->uri->segment(2).'/images/1__header_home.jpg'); ?>)">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12" style="background-color: rgba(0,0,0,0.4);">
                <div class="banner-content d-flex align-items-center justify-content-center">
                    <h1 class="banner-title mb-5"><?php echo $banner_details['title']; ?></h1>
                    <?php if (array_key_exists('subtitle', $banner_details)) : ?>
                        <h2 style="font-style: italic; font-size: 18px !important; color: #FFF">&ldquo;<?php echo $banner_details['subtitle']; ?>&ldquo;</h2>
                    <?php endif; ?>
                    <div class="countdown mb-3" data-year="<?= date('Y', strtotime($active_event['event_date'])); ?>" data-month="<?= date('m', strtotime($active_event['event_date'])); ?>" data-day="<?= date('d', strtotime($active_event['event_date'])); ?>" data-hours="<?= date('H', strtotime($active_event['event_date'])); ?>"></div>
                    <div class="banner-subtitle">
                        <?php if (array_key_exists('event_date', $banner_details)) : ?>
                            <span id="event-date"><?php echo $banner_details['event_date']; ?></span>
                        <?php endif; ?>
                    </div>
                    <a href="#speakers-section" class="btn btn-readmore bg-yellow">View All Speakers</a>
                </div>
            </div>
        </div>
    </div>
</section>