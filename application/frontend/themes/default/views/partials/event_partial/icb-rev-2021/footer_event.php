<style>
    footer a {
        color: #181d62;
    }
    footer a:hover {
        font-family: 'Sailec Medium';
        color: #181d62;
    }
    footer .footer-wrapper {
        display: flex;
        flex-wrap: wrap;
        justify-content: flex-start;
        width: 1200px;
    }
    footer .footer-wrapper .footer-sect {
        flex-wrap: wrap;
        flex-shrink: 1;
        /* flex-grow: 1; */
    }

    footer .footer-wrapper .footer-sect .footer-nav-item {
        margin-bottom: 25px;
    }
    footer .footer-wrapper .footer-sect .footer-nav-item > a {
        font-family: 'Sailec Medium';
    }
    footer .footer-wrapper .footer-sect .footer-nav-item > a:hover {
        font-family: 'Sailec Bold';
    }
    footer .footer-wrapper .footer-sect:nth-of-type(1) {
        flex-wrap: wrap;
        /* flex-shrink: 1; */
        /* flex-grow: 1; */
        flex-basis: 100px;
    }
    footer .footer-wrapper .footer-sect:nth-of-type(3) {
        flex-wrap: wrap;
        /* flex-shrink: 1; */
        flex-grow: 3;
        flex-basis: 100px;
    }
    footer .footer-wrapper .footer-sect .details-footer {
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
        margin-top: 15px;
    }
    
</style>
<footer class="bg-green py-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 footer-wrapper">
                <div class="footer-sect mx-5 ml-md-0 mr-5 mb-5 text-left" style="flex-shrink: 3">
                    <a href="#">
                        <img src="<?=base_url('assets/images/nbri_new_logo_blue_with_text.png');?>" width="150" alt="">
                    </a>
                </div>
                <div class="footer-sect mx-5" style="flex-shrink: 3">
                    <ul class="unstyled-list">
                        <li class="footer-nav-item">
                            <a href="<?=site_url('contact');?>">Contact</a>
                            <div class="details-footer flex-column">
                                <a href="">Tel: <?php echo @$website_var['phone'];?></a>
                                <a href="">Fax: <?php echo @$website_var['fax'];?></a>
                                <address><?php echo @$website_var['address'];?></address>
                            </div>
                        </li>
                        <li class="footer-nav-item"><a href="<?=site_url();?>">Home</a></li>
                        <li class="footer-nav-item"><a href="<?=site_url('about');?>">About</a></li>
                        <li class="footer-nav-item"><a href="<?=site_url('membership');?>">Membership</a></li>
                    </ul>
                </div>
                <div class="footer-sect ml-5">
                    <ul class="unstyled-list">
                        <li class="footer-nav-item">
                            <a href="#">Connect</a>
                            <div class="details-footer">
                                <a class="mr-3" href="<?php echo $website_var['facebook_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/facebook.png');?>" alt="" height="25"></a>
                                <a class="mr-3" href="mailto:<?php echo $website_var['email'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/mail.png');?>" alt="" height="25"></a>
                                <a class="mr-3" href="<?php echo $website_var['instagram_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/instagram.png');?>" alt="" height="25"></a>
                                <a class="mr-3" href="<?php echo $website_var['youtube_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/youtube.png');?>" alt="" height="25"></a>
                                <a class="mr-3" href="<?php echo $website_var['twitter_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/twitter.png');?>" alt="" height="25"></a>
                                <a class="mr-3" href="<?php echo $website_var['google_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/linkedin.png');?>" alt="" height="25"></a>
                            </div>
                        </li>
                        <li class="footer-nav-item">
                            <a href="#">Supported by</a>
                            <div class="details-footer">
                                <img class="mr-3 mb-4" src="<?=base_url('assets/images/footer-assets/mrs-ina.png');?>" alt="" height="60">
                                <img class="mr-3 mb-4" src="<?=base_url('assets/images/footer-assets/qmul.png');?>" alt="" height="60">
                                <img class="mr-3 mb-4" src="<?=base_url('assets/images/footer-assets/iumrs.png');?>" alt="" height="60">
                                <img class="mr-3 mb-4" src="<?=base_url('assets/images/footer-assets/gcrf.png');?>" alt="" height="60">
                                <img class="mr-3 mb-4" src="<?=base_url('assets/images/footer-assets/infien.png');?>" alt="" height="60">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            This site is protected by hCaptcha and its
            <a class="text-navy" href="https://hcaptcha.com/privacy"><u>Privacy Policy</u></a> and
            <a class="text-navy" href="https://hcaptcha.com/terms"><u>Terms of Service</u></a> apply
            </div>
        </div>
        <div class="row">
            <div class="col">
                <p class="footer-copy">&copy; 2020 National Battery Research Institute. <br>All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>

<div class="modal modal-member fade" id="privacyCookieModal" tabindex="-1" role="dialog" aria-labelledby="privacyCookieModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-full modal-dialog-centered d-flex flex-wrap" role="document">
            <div class="modal-content bg-navy font-color-green"
                style="padding: 50px; position: relative;">
                <div class="modal-body p-0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
                        </span>
                    </button>
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Your Privacy</h5>
                            <p style="font-size:14px">
                                Your privacy is important to us <br><br>
                                Cookies are very small text files that are stored on your computer when you visit a website. We use cookies for a variety of purposes and to enhance your online experience on our website (for example, to remember your account login details).<br><br>

                                You can change your preferences and decline certain types of cookies to be stored on your computer while browsing our website. You can also remove any cookies already stored on your computer, but keep in mind that deleting cookies may prevent you from using parts of our website.<br><br>
                                </p>
                            <h5>Strictly necessary cookies</h5>
                            <p style="font-size:14px">
                                These cookies are essential to provide you with services available through our website and to enable you to use certain features of our website.<br><br>

                                Without these cookies, we cannot provide you certain services on our website.
                            </p>
                            <button class="btn btn-readmore bg-yellow m-0" id="btnAcceptCookie">Yes, I understand</button>
                            <p>
                            <br>
                                By accpting this, you are agree to our privacy policy and you will not prosecute related to this matter in the future
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-border p-0">
                    
                </div>
            </div>
        </div>
    </div>