<nav class="navbar navbar-expand-lg navbar-custom">
    <div class="container">
        <a class="navbar-brand" href="<?php echo site_url();?>">
            <img src="<?=base_url('assets/images/nbri_new_logo_with_text.png');?>" height="45" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNBRI" aria-controls="navbarNBRI" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNBRI">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('event/icb-rev-2021');?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <!-- Navdropdown Mobile Begin (Agenda)-->
                <li class="nav-item nav-mobile dropdown d-md-none d-lg-none d-xl-none" id="navMobileAgenda">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMobileAgenda" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Agenda
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMobileAgenda">
                        <a class="dropdown-item" href="<?php echo site_url('event/icb-rev-2021/agenda');?>">View All Agenda</a>
                        <a class="dropdown-item" href="<?php echo site_url('event/icb-rev-2021/agenda/conference');?>">Conference</a>
                        <a class="dropdown-item" href="<?php echo site_url('event/icb-rev-2021/agenda/school');?>">School</a>
                        <a class="dropdown-item" href="<?php echo site_url('event/icb-rev-2021/agenda/sponsors');?>">Sponsors</a>
                    </div>
                </li>
                <!-- Navdropdown Mobile End (Agenda)-->
                <!-- NavDropdown Desktop Begin (Agenda)-->
                <li class="nav-item nav-desktop dropdown mega-menu-item d-none d-sm-none d-md-flex" id="navDesktopAgenda">
                    <a class="nav-link dropdown-toggle" href="<?php echo site_url('event/icb-rev-2021/agenda');?>" id="navbarDropdownDesktopAgenda" aria-haspopup="true" aria-expanded="false">
                    Agenda
                    </a>
                    <div class="dropdown-menu megamenu" aria-labelledby="navbarDropdownDesktopAgenda">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                <a class="megamenu-dropdown-item" href="<?php echo site_url('event/icb-rev-2021/agenda/conference');?>">Conference</a>
                                <a class="megamenu-dropdown-item" href="<?php echo site_url('event/icb-rev-2021/agenda/school');?>">School</a>
                                <a class="megamenu-dropdown-item" href="<?php echo site_url('event/icb-rev-2021/agenda/sponsors');?>">Sponsors</a>
                            </div>
                        </div>
                    </div>
                </li>
                <!-- NavDropdown Desktop End (Research)-->
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('event/icb-rev-2021/speakers');?>">Speakers</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('event/icb-rev-2021/publication');?>">Publication</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('event/icb-rev-2021/program-book');?>">Program Book</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('event/icb-rev-2021/organization');?>">Organization</a>
                </li>
            </ul>
        </div>

    </div>
</nav>