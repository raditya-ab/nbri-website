<section id="supporter-section" class="page-section bg-green text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="supporting-title">Sponsored by:</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col d-flex justify-content-center">
              <img class="supporters mx-5" src="<?=base_url('assets/images/footer-assets/telkom.png');?>" alt="" height="75">
              <img class="supporters mx-5" src="<?=base_url('assets/images/footer-assets/gcrf.png');?>" alt="" height="75">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <h2 class="supporting-title">Supported by:</h2>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col d-flex justify-content-center">
                <img class="supporters mx-5" src="<?=base_url('assets/images/footer-assets/infien.png');?>" alt="" height="75">
                <img class="supporters mx-5" src="<?=base_url('assets/images/footer-assets/iumrs.png');?>" alt="" height="75">
                <img class="supporters mx-5" src="<?=base_url('assets/images/footer-assets/qmul.png');?>" alt="" height="75">
                <img class="supporters mx-5" src="<?=base_url('assets/images/footer-assets/mrs-ina.png');?>" alt="" height="75">
            </div>
        </div>
    </div>
</section>