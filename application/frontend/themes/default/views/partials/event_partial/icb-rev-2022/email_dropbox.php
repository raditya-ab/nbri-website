<section id="drop-email-section" class="page-section py-80 bg-yellow">
    <div class="container">
        <?php echo form_open('event/icbrev2021/a',array('class' => 'form w-100','id' => 'form_drop_email','method'=>'post'),array('event_name' => 'icb-rev-2021','y-captcha-response'=>''));?>
            <div class="row">
                <div class="col d-flex flex-column align-items-stretch">
                    <h2 class="section-title mb-0">Need more info?</h2>
                    <p class="text-left">Drop us your email</p>
                </div>
                <div class="col-md-7 d-flex align-items-center">
                    <input type="text" name="email" class="custom-input no-border text-big text-navy" placeholder="Insert you email here..">
                </div>
                <div class="col d-flex align-items-center justify-content-center mt-3">
                    <button type="button" data-sitekey="ae5242f9-574b-48bd-bd78-3055d070f1cc" data-callback="submitForm" class="btn btn-custom bg-navy text-green h-captcha"><i class="fas fa-paper-plane"></i> Submit</button>
                </div>
            </div>
        <?php echo form_close();?>
    </div>
</section>