<footer id="footer" class="content-section bg-green">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <a href="#">
                    <img src="<?=base_url('assets/images/nbri_new_logo_blue_with_text.png');?>" width="150" alt="">
                </a>
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col">
                        <div id="middle-footer" class="footer-section align-top">
                            <ul class="footer-menu">
                                <li class="footer-nav">
                                    <a href="<?=site_url('contact');?>">Contact</a>
                                    <ul class="company-detail">
                                        <li>Tel. <?php echo @$website_var['phone'];?></li>
                                        <li>Fax. <?php echo @$website_var['fax'];?></li>
                                        <li style="white-space: pre-wrap; max-width: 300px"><?php echo @$website_var['address'];?></li>
                                    </ul>
                                </li>
                                <li class="footer-nav"><a href="<?=site_url();?>">Home</a></li>
                                <li class="footer-nav"><a href="<?=site_url('about');?>">About</a></li>
                                <li class="footer-nav"><a href="<?=site_url('membership');?>">Membership</a></li>
                            </ul>
                        </div>
                        <div id="middle-footer" class="footer-section align-top">
                            <div class="row">
                                <ul class="footer-menu" style="margin-bottom: 30px;">
                                    <li class="footer-nav">
                                        <a href="">Connect</a>

                                        <div class="connect-container">
                                            <ul class="connect-link">
                                                <li><a href="<?php echo $website_var['facebook_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/facebook.png');?>" alt=""></a></li>
                                                <li><a href="mailto:<?php echo $website_var['email'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/mail.png');?>" alt=""></a></li>
                                                <li><a href="<?php echo $website_var['instagram_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/instagram.png');?>" alt=""></a></li>
                                                <li><a href="<?php echo $website_var['youtube_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/youtube.png');?>" alt=""></a></li>
                                                <li><a href="<?php echo $website_var['twitter_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/twitter.png');?>" alt=""></a></li>
                                                <li><a href="<?php echo $website_var['google_url'];?>" target="_blank"><img class="social-img" src="<?=base_url('assets/images/footer-assets/linkedin.png');?>" alt=""></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <div class="supported-container align-items-bottom ">
                                    <ul class="footer-menu">
                                        <li class="footer-nav">
                                            <a href="">Supported by:</a>
                                            <div class="connect-container">
                                                <ul class="connect-link">
                                                    <li><img class="supporters" src="<?=base_url('assets/images/footer-assets/mrs-ina.png');?>" alt=""></li>
                                                    <li><img class="supporters" src="<?=base_url('assets/images/footer-assets/qmul.png');?>" alt="" height="42"></li>
                                                    <li><img class="supporters" src="<?=base_url('assets/images/footer-assets/iumrs.png');?>" alt="" height="75"></li>
																									<!-- <li><img class="supporters" src="<?=base_url('assets/images/footer-assets/gcrf.png');?>" alt="" height="55"></li> -->
                                                </ul>
                                                <ul class="connect-link">
                                                    <li><img class="supporters" src="<?=base_url('assets/images/footer-assets/infien.png');?>" alt="" height="95"></li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="footer-section">
                            <p class="footer-copy">&copy; 2020 National Battery Research Institute. <br>All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="modal modal-member fade" id="privacyCookieModal" tabindex="-1" role="dialog" aria-labelledby="privacyCookieModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-full modal-dialog-centered" role="document">
            <div class="modal-content bg-navy font-color-green"
                style="padding: 50px; position: relative;">
                <div class="modal-body p-0">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="<?=base_url('assets/images/clear_24px.svg');?>" alt="">
                        </span>
                    </button>
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Your Privacy</h5>
                            <p style="font-size:14px">
                                Your privacy is important to us <br><br>
                                Cookies are very small text files that are stored on your computer when you visit a website. We use cookies for a variety of purposes and to enhance your online experience on our website (for example, to remember your account login details).<br><br>

                                You can change your preferences and decline certain types of cookies to be stored on your computer while browsing our website. You can also remove any cookies already stored on your computer, but keep in mind that deleting cookies may prevent you from using parts of our website.<br><br>
                                </p>
                            <h5>Strictly necessary cookies</h5>
                            <p style="font-size:14px">
                                These cookies are essential to provide you with services available through our website and to enable you to use certain features of our website.<br><br>

                                Without these cookies, we cannot provide you certain services on our website.
                            </p>
                            <button class="btn btn-readmore bg-yellow m-0" id="btnAcceptCookie">Yes, I understand</button>
                            <p>
                            <br>
                                By accpting this, you are agree to our privacy policy and you will not prosecute related to this matter in the future
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-border p-0">
                    
                </div>
            </div>
        </div>
    </div>