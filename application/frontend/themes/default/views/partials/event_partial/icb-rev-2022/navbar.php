<nav class="navbar navbar-expand-lg navbar-custom">
	<div class="container">
		<a class="navbar-brand" href="<?php echo site_url(); ?>">
			<img src="<?= base_url('assets/images/nbri_new_logo_with_text.png'); ?>" height="45" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNBRI"
						aria-controls="navbarNBRI" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNBRI">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="<?= site_url('event/' . $this->uri->segment(2)); ?>">Home <span class="sr-only">(current)</span></a>
				</li>
				<!-- Navdropdown Mobile Begin (Agenda)-->
				<li class="nav-item nav-mobile dropdown d-md-none d-lg-none d-xl-none" id="navMobileAgenda">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMobileAgenda" role="button"
						 data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Agenda
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMobileAgenda">
						<a class="dropdown-item" href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda'); ?>">View
							All Agenda</a>
						<a class="dropdown-item"
							 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/conference'); ?>">ICB-REV 2022</a>
						<a class="dropdown-item"
							 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/ibs-2022'); ?>">IBS 2022</a>
						<a class="dropdown-item"
							 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/isrus-2022'); ?>">ISRUS 2022</a>
						<a class="dropdown-item"
							 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/nbri-yic-2022'); ?>">NBRI-YIC 2022</a>
					</div>
				</li>
				<!-- Navdropdown Mobile End (Agenda)-->
				<!-- NavDropdown Desktop Begin (Agenda)-->
				<li class="nav-item nav-desktop dropdown mega-menu-item d-none d-sm-none d-md-flex" id="navDesktopAgenda">
					<a class="nav-link dropdown-toggle"
						 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda'); ?>"
						 id="navbarDropdownDesktopAgenda" aria-haspopup="true" aria-expanded="false">
						Agenda
					</a>
					<div class="dropdown-menu megamenu" aria-labelledby="navbarDropdownDesktopAgenda">
						<div class="row">
							<div class="col-12 d-flex justify-content-center">
								<a class="megamenu-dropdown-item"
									 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/conference'); ?>">ICB-REV 2022</a>
								<a class="megamenu-dropdown-item"
									 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/ibs-2022'); ?>">IBS 2022</a>
								<a class="megamenu-dropdown-item"
									 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/isrus-2022'); ?>">ISRUS 2022</a>
								<a class="megamenu-dropdown-item"
									 href="<?php echo site_url('event/' . $this->uri->segment(2) . '/agenda/nbri-yic-2022'); ?>">NBRI-YIC 2022</a>
							</div>
						</div>
					</div>
				</li>
				<!-- NavDropdown Desktop End (Agenda)-->
				<li class="nav-item">
					<a class="nav-link" href="<?= site_url('event/' . $this->uri->segment(2) . '/speakers'); ?>">Speakers</a>
				</li>
				<li class="nav-item">
					<a class="nav-link"
						 href="<?= site_url('event/' . $this->uri->segment(2) . '/publication'); ?>">Publication</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= site_url('event/' . $this->uri->segment(2) . '/program-book'); ?>">Program
						Book</a>
				</li>
				<li class="nav-item">
					<a class="nav-link"
						 href="<?= site_url('event/' . $this->uri->segment(2) . '/organization'); ?>">Organization</a>
				</li>
			</ul>
		</div>

	</div>
</nav>