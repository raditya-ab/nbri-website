<section id="supporter-section" class="page-section bg-green text-center">
	<div class="container">
		<div class="row mt-3">
			<div class="col-12">
				<h2 class="supporting-title">Supported by:</h2>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col d-flex flex-wrap justify-content-center">
				<img class="supporters mx-5 mt-3" src="<?= base_url('assets/images/footer-assets/qmul.png'); ?>" alt="" height="75">
				<img class="supporters mx-5 mt-3" src="<?= base_url('assets/images/footer-assets/mrs-ina.png'); ?>" alt="" height="75">
				<img class="supporters mx-5 mt-3" src="<?= base_url('assets/images/footer-assets/iumrs.png'); ?>" alt="" height="75">
				<img class="supporters mx-5 mt-3" src="<?= base_url('assets/images/footer-assets/infien.png'); ?>" alt="" height="75">
				<img class="supporters mx-5 mt-3" src="<?= base_url('assets/images/footer-assets/apni.png'); ?>" alt="" height="75">
				<img class="supporters mx-5 mt-3" src="<?= base_url('assets/images/footer-assets/brin.png'); ?>" alt="" height="75">
				<img class="supporters mx-5 mt-3" src="<?= base_url('assets/images/footer-assets/jne.png'); ?>" alt="" height="75">
			</div>
		</div>
	</div>
</section>