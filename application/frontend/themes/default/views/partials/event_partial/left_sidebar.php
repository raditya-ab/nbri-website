<h2 class="section-title">News</h2>
<div class="list-group">
    <?php if(empty($news_list)):?>
        <p>No posted news</p>
    <?php else:?>
        <?php foreach($news_list as $news):?>
            <?php
                $news['title'] = character_limiter($news['title'],25,'...');
                $news['content'] = character_limiter(strip_tags(html_entity_decode($news['content'])),25,'...');
            ?>
        <a href="<?=site_url('event/'.$news['category_slug'].'/news/'.$news['slug']);?>" class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1"><?=$news['title'];?></h5>
                <small><?=timeago($news['created']);?></small>
            </div>
            <p class="mb-1"><?=$news['content'];?>
            </p>
        </a>
        <?php endforeach;?>
    <?php endif;?>
</div>