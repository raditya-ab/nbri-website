<style>
    .navbar ul
    {
        display: table;
        height: 80px;
        /*border-spacing: 30px 0;*/
    }
    .navbar ul li.nav-item
    {
        display: table-cell;
        vertical-align: middle;
        padding: 0 20px;
    }
    li.nav-item:hover {
        background-color: #b9dc00;
    }
    li.nav-item:hover .dropdown-toggle::after
    {
        color: #181d62;
    }
    li.nav-item.dropdown:hover .dropdown-menu
    {
        display: block;
    }
</style>

<nav class="navbar fixed-top navbar-expand-lg navbar-dark justify-content-between bg-navy">
    <div class="container" style="height: inherit;">
        <a class="navbar-brand" href="<?php echo site_url(); ?>">
            <img src="<?=base_url('assets/images/nbri_new_logo_with_text.png');?>" height="45" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto" style="height: 80px; display: table">
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('event/climate-challenge-workshop');?>">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('event/climate-challenge-workshop/about');?>">About</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('event/climate-challenge-workshop/news');?>">News</a>
                </li>
                <!-- <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="<?php echo site_url('research'); ?>" id="navDropdownResearch">
                        Research
                    </a>
                    <?php if(array_key_exists('research',$menu)): ?>
                    <div class="dropdown-menu bg-green" aria-labelledby="navDropdownResearch">
                        <div class="dropdown-content d-flex justify-content-center align-items-center">
                            <?php foreach($menu['research'] as $research_menu): ?>
                            <a class="dropdown-item" href="<?=site_url('research/'.$research_menu['slug']);?>"><?=$research_menu['name'];?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </li> -->
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('event/climate-challenge-workshop/mentor');?>">Mentor</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('event/climate-challenge-workshop/programme-and-speaker');?>">Programme &amp; Speaker</a>
                </li>
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="<?php echo site_url('event/climate-challenge-workshop/climate-challenge-workshop'); ?>" id="navDropdownClimate">
                        Prizes &amp; Awards
                    </a>
                    <!-- <a class="nav-link dropdown-toggle" href="<?=site_url('event/climate-challenge-workshop/climate-challenge-workshop');?>" id="navDropdownClimate" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a> -->
                    <div class="dropdown-menu bg-green" aria-labelledby="navDropdownClimate">
                        <div class="dropdown-content d-flex justify-content-center align-items-center">
                            <a class="dropdown-item" href="<?=site_url('event/climate-challenge-workshop/research-prize');?>">Research Prize</a>
                            <a class="dropdown-item" href="<?=site_url('event/climate-challenge-workshop/dissemination-awards');?>">Dissemination Awards</a>
                        </div>
                    </div>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('event/climate-challenge-workshop/registration-application');?>">Registration &amp; Application</a>
                </li>
                <!-- <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="<?php echo site_url('event/climate-challenge-workshop/registration-application'); ?>" id="navDropdownregistration-application">
                        Registration &amp; Application
                    </a>
                    <a class="nav-link dropdown-toggle" href="<?=site_url('event/climate-challenge-workshop/climate-challenge-workshop');?>" id="navDropdownClimate" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                    <div class="dropdown-menu bg-green" aria-labelledby="navDropdownregistration-application">
                        <div class="dropdown-content d-flex justify-content-center align-items-center">
                            <a class="dropdown-item" href="<?=site_url('event/climate-challenge-workshop/author-guideline');?>">Author Guideline</a>
                        </div>
                    </div>
                </li> -->
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('event/climate-challenge-workshop/faq');?>">FAQ</a>
                </li>
                </li>
                <!--<li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('nbri-onemap');?>">NBRI One Map</a>
                </li>
                <li class="nav-item active dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navDropdownMembership" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Membership</a>
                    <div class="dropdown-menu bg-green" aria-labelledby="navDropdownMembership">
                        <div class="dropdown-content d-flex justify-content-center align-items-center">
                            <a class="dropdown-item" href="<?=site_url('membership/individual');?>">Individual Member</a>
                            <a class="dropdown-item" href="<?=site_url('membership/institution');?>">Institutional Member</a>
                            <a class="dropdown-item" href="<?=site_url('membership/corporate');?>">Corporate Member</a>
                        </div>
                    </div>
                </li> -->

            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid py-2" style="background: lavender; margin-top: 75px">
    <div class="row py-2">
        <div class="col-md-12 text-center">
            <span><b>Supported by:</b></span>
        </div>
    </div>
    <div class="row py-4">
        <div class="container">
            <div class="row">
                <div class="col text-center"><img class="supporters" src="<?=base_url('assets/images/footer-assets/mrs-ina.png');?>" alt="" height="90"></div>
                <div class="col text-center"><img class="supporters" src="<?=base_url('assets/images/footer-assets/qmul.png');?>" alt="" height="85"></div>
                <div class="col text-center"><img class="supporters" src="<?=base_url('assets/images/footer-assets/iumrs.png');?>" alt="" height="95"></div>
                <div class="col text-center"><img class="supporters" src="<?=base_url('assets/images/footer-assets/infien.png');?>" alt="" height="95"></div>
                <div class="col text-center"><img class="supporters" src="<?=base_url('assets/images/footer-assets/british-council.png');?>" alt="" height="95"></div>

                <!-- <div class="col text-center"><img class="supporters" src="<?=base_url('assets/images/footer-assets/gcrf.png');?>" alt="" height="90"></div> -->
            </div>
        </div>
    </div>
</div>