<style>
    .list-group-item {
        border-bottom: 0;
    }
</style>
<h2 class="section-title">Key Dates</h2>
<div class="list-group">
    <a class="list-group-item list-group-item-action flex-column align-items-start no-border pl-0 pt-2" data-toggle="collapse" href="#collapseResearchPrizes" role="button" aria-expanded="false" aria-controls="collapseResearchPrizes">
        <div class="d-flex w-100 justify-content-between">
            <h5>Research Prizes</h5>
            <small>Click for details</small>
        </div>
    </a>
    <div class="collapse" id="collapseResearchPrizes">
        <div class="list-group">
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Registration Deadline</h5>
                </div>
                <p class="mb-1">20 May 2021</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Networking Event</h5>
                </div>
                <p class="mb-1">25-26 May 2021</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Submission Deadline </h5>
                </div>
                <p class="mb-1">22 June 2021</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Candidate Announcement</h5>
                </div>
                <p class="mb-1">10 July 2021</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Workshop Event</h5>
                </div>
                <p class="mb-1">12-14 July 2021</p>
            </div>
        </div>
    </div>
    <a class="list-group-item list-group-item-action flex-column align-items-start no-border pl-0 pt-2" data-toggle="collapse" href="#collapseDisseminationAwards" role="button" aria-expanded="false" aria-controls="collapseDisseminationAwards">
        <div class="d-flex w-100 justify-content-between">
            <h5>Dissemination Awards</h5>
            <small>Click for details</small>

        </div>
    </a>
    <div class="collapse" id="collapseDisseminationAwards">
        <div class="list-group">
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Abstract Submission Deadline</h5>
                </div>
                <p class="mb-1">9 July 2021</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Candidate Announcement</h5>
                </div>
                <p class="mb-1">10 July 2021</p>
            </div>
            <div class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Workshop Event</h5>
                </div>
                <p class="mb-1">12-14 July 2021</p>
            </div>
        </div>
    </div>
</div>