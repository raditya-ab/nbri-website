
<div class="topbar">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-between">
                <a href="#" class="topbar-link">
                    <i class="fas fa-search" aria-hidden="true"></i>
                </a>
                <?php if(!$this->aauth->is_loggedin()): ?>
                    <a href="<?php echo site_url('membership/login');?>" class="topbar-link">
                        Login/Register
                    </a>
                <?php else: ?>
                    <a href="<?=site_url('membership/profile/'.sha1($_SESSION['username']));?>" class="topbar-link">
                    My Profile
                    </a>
                    <a href="<?=site_url('membership/logout');?>" class="topbar-link">
                        Log Out
                    </a>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-custom">
    <div class="container">
        <a class="navbar-brand" href="<?php echo site_url();?>">
            <img src="<?=base_url('assets/images/nbri_new_logo_with_text.png');?>" height="45" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNBRI" aria-controls="navbarNBRI" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNBRI">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url();?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <!-- Navdropdown Mobile Begin (Research)-->
                <li class="nav-item nav-mobile dropdown d-md-none d-lg-none d-xl-none" id="navMobileResearch">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMobileResearch" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Research
                    </a>
                    <?php if(array_key_exists('research',$menu)):?>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMobileResearch">
                            <a class="dropdown-item" href="<?php echo site_url('research');?>">All Research</a>
                        <?php foreach($menu['research'] as $research_menu): ?>
                            <a class="dropdown-item" href="<?=site_url('research/'.$research_menu['slug']);?>"><?=$research_menu['name'];?></a>
                        <?php endforeach; ?>
                        </div>
                    <?php endif;?>
                </li>
                <!-- Navdropdown Mobile End (Research)-->
                <!-- NavDropdown Desktop Begin (Research)-->
                <li class="nav-item nav-desktop dropdown mega-menu-item d-none d-sm-none d-md-flex" id="navDesktopResearch">
                    <a class="nav-link dropdown-toggle" href="<?php echo site_url('research');?>" id="navbarDropdownDesktopResearch" aria-haspopup="true" aria-expanded="false">
                    Research
                    </a>
                    <?php if(array_key_exists('research',$menu)):?>
                        <div class="dropdown-menu megamenu" aria-labelledby="navbarDropdownDesktopResearch">
                            <div class="row">
                                <div class="col-12 d-flex justify-content-center">
                                    <?php foreach($menu['research'] as $research_menu): ?>
                                        <a href="<?=site_url('research/'.$research_menu['slug']);?>" class="megamenu-dropdown-item"><?=$research_menu['name'];?></a>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
                </li>
                <!-- NavDropdown Desktop End (Research)-->
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('publication');?>">Publication</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('opportunities');?>">Opportunities</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?=site_url('education-and-skill');?>">Education &amp; Skills</a>
                </li>
                <!-- Navdropdown Mobile Begin (News & Event)-->
                <li class="nav-item nav-mobile dropdown d-md-none d-lg-none d-xl-none" id="navMobileNewsEvents">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMobileNewsEvents" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    News &amp; Events
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMobileNewsEvents">
                        <a class="dropdown-item" href="<?=site_url('news');?>">News</a>
                        <a class="dropdown-item" href="<?=site_url('events');?>">Event</a>
                        <a class="dropdown-item" href="<?=site_url('event/climate-challenge-workshop');?>">Climate Challenge Workshop</a>
                        <a class="dropdown-item" href="<?=site_url('event/icb-rev-2022');?>">ICB-REV 2022</a>
                        <a class="dropdown-item" href="https://www.nbri-events.org/">ICAMT 2021</a>
                    </div>
                </li>
                <!-- Navdropdown Mobile End (News & Event)-->
                <!-- NavDropdown Desktop Begin (News & Event)-->
                <li class="nav-item nav-desktop dropdown mega-menu-item d-none d-sm-none d-md-flex" id="navDesktopNewsEvents">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownDesktopNewsEvents" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    News &amp; Events
                    </a>
                    <div class="dropdown-menu megamenu" aria-labelledby="navbarDropdownDesktopNewsEvents">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                <a class="megamenu-dropdown-item" href="<?=site_url('news');?>">News</a>
                                <a class="megamenu-dropdown-item" href="<?=site_url('events');?>">Event</a>
                                <a class="megamenu-dropdown-item" href="<?=site_url('event/climate-challenge-workshop');?>">Climate Challenge Workshop</a>
                                <a class="megamenu-dropdown-item" href="<?=site_url('event/icb-rev-2022');?>">ICB-REV 2022</a>
                                <a class="megamenu-dropdown-item" href="https://www.nbri-events.org/">ICAMT 2021</a>
                            </div>
                        </div>
                    </div>
                </li>
                <!-- NavDropdown Desktop End (News & Event)-->
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('nbri-onemap');?>">NBRI One Map</a>
                </li>
                <!-- Navdropdown Mobile Begin (Membership)-->
                <li class="nav-item nav-mobile dropdown d-md-none d-lg-none d-xl-none" id="navMobileMembership">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMobileMembership" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Membership
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMobileMembership">
                        <a class="dropdown-item" href="<?=site_url('membership/individual');?>">Individual Member</a>
                        <a class="dropdown-item" href="<?=site_url('membership/institution');?>">Institutional Member</a>
                        <a class="dropdown-item" href="<?=site_url('membership/corporate');?>">Corporate Member</a>
                    </div>
                </li>
                <!-- Navdropdown Mobile End (Membership)-->
                <!-- NavDropdown Desktop Begin (Membership)-->
                <li class="nav-item nav-desktop dropdown mega-menu-item d-none d-sm-none d-md-flex" id="navDesktopMembership">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownDesktopMembership" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Membership
                    </a>
                    <div class="dropdown-menu megamenu" aria-labelledby="navbarDropdownDesktopMembership">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-center">
                                <a class="megamenu-dropdown-item" href="<?=site_url('membership/individual');?>">News</a>
                                <a class="megamenu-dropdown-item" href="<?=site_url('membership/institution');?>"">Event</a>
                                <a class="megamenu-dropdown-item" href="<?=site_url('membership/corporate');?>">Climate Challenge Workshop</a>
                            </div>
                        </div>
                    </div>
                </li>
                <!-- NavDropdown Desktop End (Membership)-->
            </ul>
        </div>

    </div>
</nav>