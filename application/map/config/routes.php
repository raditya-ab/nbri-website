<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'pages/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();

$db->select('slug');
$db->from('company_categories');

$result = $db->get()->result();

foreach( $result as $row )
{
    $route[ $row->slug ] = 'pages/map_overview/'.$row->slug;
    // $route[ 'product/category/'.$row->gender.'/(:num)/(:any)' ] = 'product/detail/$1/$2';
}
$route['hulu'] = 'pages/map_overview/hulu';
$route['hilir'] = 'pages/map_overview/hilir';
$route['battery-cell'] = 'pages/map_overview/battery-cell';
$route['sdm'] = 'pages/map_overview/sdm';

$route['home'] = 'pages/home';
$route['home2'] = 'pages/home2';
$route['home3'] = 'pages/home3';
$route['about'] = 'pages/about';
$route['contact'] = 'pages/contact';
$route['expert_panel'] = 'pages/expert_panel';
$route['research'] = 'pages/research';
$route['research/(:any)'] = 'pages/research_sub/$1';
$route['research/(:any)/(:any)'] = 'pages/research_detail/$1/$2';
$route['publication'] = 'pages/publication';
$route['opportunities'] = 'pages/opportunities';
$route['opportunities/(:any)'] = 'pages/opportunities_detail/$1';
$route['educationskill'] = 'pages/educationskill';
$route['articles'] = 'pages/articles';
$route['articles/(:any)'] =  'pages/articles/$1';
$route['articles/(:any)/(:any)'] =  'pages/articlesdetail/$1/$2';