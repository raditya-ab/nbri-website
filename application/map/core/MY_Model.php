<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}

    public function get_navigation_menu()
    {
        $this->db->select('name, slug');
        $this->db->from('company_categories');
        return $this->db->get()->result_array();
    }

	public function is_active($field,$value,$table_name){
		$this->db->where('status', 1);
		$this->db->where($field, $value);
		return ($this->db->count_all_results($table_name) > 0) ? TRUE : FALSE;
	}

	public function is_inactive($field,$value,$table_name){
		$this->db->where('status', 0);
		$this->db->where($field, $value);
		return ($this->db->count_all_results($table_name) > 0) ? TRUE : FALSE;
	}

	public function get_lens_option(){
		$this->db->select('id,name,slug,price');
		$this->db->from('lens_option');
		$this->db->where('status', 1);

		return $this->db->get()->result_array();
	}

	public function get_brands()
	{
		$this->db->select('brand_name, slug');
		$this->db->from('brands');
		$this->db->where('status', 1);

		return $this->db->get()->result_array();
	}

}

/* End of file MY_Model.php */
/* Location: ./application/frontend/core/MY_Model.php */