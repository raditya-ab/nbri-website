<?php
    if(!function_exists('get_website_setting')){
        function get_website_setting(){
            $setting_file = file_get_contents("../../website_setting.json");

            $website_setting = array();
            $website_setting = json_decode($setting_file, true);
            return $website_setting;
        }
    }
    if(!function_exists('set_website_variables')){
        function set_website_variables(){
            $CI =& get_instance();

            $website_setting = get_website_setting();

            $session_data = array();
            $session_data = $website_setting;
            $CI->session->set_userdata( $session_data );

            return $website_setting;
        }
    }
    if(!function_exists('get_website_variables_session')){
        function get_website_variables_session(){
            $setting_file = file_get_contents("../../website_setting.json");

            $website_setting = array();
            $website_setting = json_decode($setting_file, true);

            return $website_setting;
        }
    }
    if(!function_exists('set_website_setting')){
        function set_website_setting($values = array()){
            if(!empty($values)){
                $new_website_setting = json_encode($values);
                if(file_put_contents("../../website_setting.json", $new_website_setting, LOCK_EX)){
                    return 1;
                }
            }
            return 0;
        }
    }