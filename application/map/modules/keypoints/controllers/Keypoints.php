<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keypoints extends Public_Controller {
    private $markers = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function load_marker_data($category = NULL,$return_as_array = FALSE)
    {
        $this->load->model('maps_m');

        $data = [];

        $marker_data = $this->maps_m->fetch_markers($category);

        foreach($marker_data as $marker){
            $temp = array(
                'id' => $marker['id'],
                'img_thumb' => $marker['img_thumb'],
                'category' => $marker['category'],
                'category_name' => $marker['category_name'],
                'category_slug' => $marker['category_slug'],
                'title' => $marker['title'],
                'slug' => $marker['slug'],
                'description' => $marker['description'],
                'location' => $marker['location'],
                'latitude' => (float)$marker['latitude'],
                'longitude' => (float)$marker['longitude'],
                'url' => $marker['url']
            );

            $data[] = $temp;
        }


        if($return_as_array){
            return $data;
        } else {
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }

    }
    public function sidebar_results($category = NULL)
    {
        $data = array();
        // echo "<pre>";
        // print_r ($variable);
        // echo "</pre>";
        if(!empty($this->input->post('markers', TRUE))){

            $data['data'] = $this->load_marker_data($category,TRUE);
            $data['markers'] = $this->input->post('markers', TRUE);


            $this->load->view('sidebar_results', $data, FALSE);

        }
    }
    public function sidebar_detail($id)
    {
        $this->load->model('maps_m');

        $data['currentLocation'] = $this->maps_m->fetch_single_marker($id);

        $this->load->view('sidebar_detail', $data, FALSE);
    }

    public function show_modal($id)
    {
        $this->load->model('maps_m');

        $this->data['currentLocation'] = $this->maps_m->fetch_single_marker($id);

        // Modal HTML code

        $this->data['latitude'] = "";
        $this->data['longitude'] = "";
        $this->data['address'] = "";

        if( !empty($this->data['currentLocation']['latitude']) ){
            $this->data['latitude'] = $this->data['currentLocation']['latitude'];
        }

        if( !empty($currentLocation['longitude']) ){
            $this->data['longitude'] = $this->data['currentLocation']['longitude'];
        }

        if( !empty($currentLocation['address']) ){
            $this->data['address'] = $this->data['currentLocation']['address'];
        }

        $this->load->view('maps-modal', $this->data, FALSE);
    }

}

/* End of file Keypoints.php */
/* Location: ./application/modules/keypoints/controllers/Keypoints.php */
