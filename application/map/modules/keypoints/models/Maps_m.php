<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maps_m extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    // public function fetch_markers()
    // {
    //     $select = array(
    //         'c.id',
    //         'c.category',
    //         'cc.name as category_name',
    //         'cc.slug as category_slug',
    //         'c.name as title',
    //         'c.slug',
    //         'c.description',
    //         'c.address as location',
    //         'c.latitude',
    //         'c.longitude',
    //         'c.website as url'
    //     );
    //     $this->db->select($select);
    //     $this->db->from('companies c');
    //     $this->db->join('company_categories cc', 'c.category = cc.id', 'left');
    //     $this->db->where('c.status', '1');
    //     $this->db->where('cc.status', '1');
    //     $this->db->order_by('c.category', 'asc');

    //     return $this->db->get()->result_array();
    // }

    public function fetch_markers($category = NULL)
    {
        $onbounds = FALSE;

        $select = array(
            'c.id',
            'c.category',
            'cc.name as category_name',
            'cc.slug as category_slug',
            'c.name as title',
            'c.img_thumb',
            'c.slug',
            'c.description',
            'c.address as location',
            'c.latitude',
            'c.longitude',
            'c.website as url'
        );

        $this->db->select($select);

        $this->db->from('companies c');
        $this->db->join('company_categories cc', 'c.category = cc.id', 'left');
        $this->db->where('c.status', '1');
        $this->db->where('cc.status', '1');

        if($category != NULL && !empty($category)){
          $this->db->where('cc.slug', $category);
        }

        if($this->input->post()){
            $post_data = $this->input->post(NULL, TRUE);

            if(array_key_exists('limit', $post_data)){
                $this->db->limit($post_data['limit']);
            }

            if(array_key_exists('optimized_loading', $post_data)){
                if(!empty($post_data['optimized_loading']) && $post_data['optimized_loading'] != NULL){
                    $onbounds = $post_data['optimized_loading'];
                }
            }
            if($onbounds == TRUE)
            {
                $this->db->where("latitude <= $post_data[north_east_lat]");
                $this->db->where("latitude >= $post_data[south_west_lat]");
                $this->db->where("longitude <= $post_data[north_east_lng]");
                $this->db->where("longitude >= $post_data[south_west_lng]");
            }
        }

        return $this->db->get()->result_array();
    }

    public function fetch_single_marker($id = FALSE)
    {
        $select = array(
            'c.id',
            'c.category',
            'cc.name as category_name',
            'cc.slug as category_slug',
            'c.name as title',
            'c.slug',
            'c.description',
            'c.img_thumb',
            'c.address as location',
            'c.latitude',
            'c.longitude',
            'c.website as url'
        );

        $this->db->select($select);

        $this->db->from('companies c');
        $this->db->join('company_categories cc', 'c.category = cc.id', 'left');
        $this->db->where('c.status', '1');
        $this->db->where('cc.status', '1');

        if($id !== FALSE){
            $this->db->where('c.id', $id);

        }

        return $this->db->get()->row_array();
    }

}

/* End of file Maps_m.php */
/* Location: ./application/map/modules/pages/models/Maps_m.php */
