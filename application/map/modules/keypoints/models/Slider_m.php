<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider_m extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }
    public function get_slider(){
        $this->db->where('status', '1');
        $this->db->order_by('modified', 'desc');
        $this->db->order_by('created', 'desc');

        return $this->db->get('sliders')->result_array();
    }

}

/* End of file Pages_m.php */
/* Location: ./application/models/Pages_m.php */