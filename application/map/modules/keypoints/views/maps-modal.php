<?php
    $img = (!empty($currentLocation['img_thumb']) && $currentLocation['img_thumb'] != NULL) ? $currentLocation['img_thumb'] : 'placeholder.png';

?>
<div class="modal-item-detail modal-dialog" role="document" data-latitude="<?=$latitude;?>" data-longitude="<?=$longitude;?>" data-address="<?=$address;?>" style="width: 900px">
    <div class="modal-content bg-navy">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <!--end modal-header-->
        <div class="modal-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="img-company" style="width: 100%; height: 450px; background-repeat: none; background-size: contain; background-position: center top;background-repeat: no-repeat; background-image: url(<?=base_url('../assets/images/companies/'.$img);?>)"></div>
                </div>
                <div class="col-md-8">
                    <div class="modal-section-title" style="display: flex; justify-content: space-between;">
                        <h2 class="modal-title-company" style="font-size: 24px;margin-top: 0"><?=$currentLocation['title'];?></h2>
                        <div class="category bg-yellow text-center" style="display: flex; align-items: center; min-height: 25px; min-width: 75px; padding: 5px 25px; font-size: 11px"><?=strtoupper($currentLocation['category_name']);?></div>
                    </div>
                    <div class="modal-section-content" style="height: 100%">
                        <div class="content-item">
                            <h3>About</h3>
                            <p><?=$currentLocation['description'];?></p>
                        </div>
                        <div class="content-item">
                            <h3>Address</h3>
                            <p><?=$currentLocation['location'];?></p>
                        </div>
                        <div class="content-item">
                            <h3>Other Information</h3>
                            <p>You must be a member to unlock the special contents</p>
                        </div>
                        <?php if($this->aauth->is_loggedin()): ?>
                            <a href="<?=site_url('company/detail/'.$currentLocation['slug']);?>" class="btn-readmore bg-green">View Detail</a>
                        <?php else: ?>
                            <a href="<?=site_url('../membership/register');?>" class="btn-readmore bg-green">Become a Member now</a>
                        <?php endif; ?>
                        <section style="margin-top: 30px">
                            <div class="social-share"></div>
                        </section>

                    </div>
                </div>
            </div>
        </div>
        <!--end modal-body-->
    </div>
    <!--end modal-content-->
</div>
<!--end modal-dialog-->