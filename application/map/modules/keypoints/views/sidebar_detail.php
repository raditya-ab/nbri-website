<div class="sidebar-content" data-id="<?=$currentLocation['id'];?>">
    <div class="back"></div>
    <!--end back-button-->

    <div class="controls-more">
        <ul>
            <li><a href="#">Add to favorites</a></li>
            <li><a href="#">Add to watchlist</a></li>
        </ul>
    </div>
    <!--end controls-more-->

    <div class="section-title">
        <a href="<?=$currentLocation['url'];?>" class="btn btn-primary btn-framed btn-rounded btn-xs full-detail">Full Detail</a>
        <?php if( !empty($currentLocation['title']) ):?>
        <h2><?=currentLocation['title'];?></h2>;
        <?php endif; ?>

        <?php if( !empty($currentLocation['location']) ): ?>
        <h4><?=$currentLocation['location'];?></h4>
        <?php endif; ?>

        <?php if( !empty($currentLocation['category']) ): ?>
        <div class="label label-default"><?=$currentLocation['category'];?></div>
        <?php endif; ?>

        <?php if( !empty($currentLocation['ribbon']) ): ?>
        <figure class="ribbon"><?=$currentLocation['ribbon'];?></figure>
        <?php endif; ?>

        <?php if( !empty($currentLocation['rating']) ): ?>
        <div class="rating-passive" data-rating="<?=$currentLocation['rating'];?>">
            <span class="stars"></span>
            <span class="reviews"><?=$currentLocation['reviews_number'];?></span>
        </div>
        <?php endif; ?>
    </div>
    <!--end section-title-->