<style type="text/css" media="screen">
    .result-item-detail .image {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }
</style>
<?php for( $i=0; $i < count($data); $i++): ?>
    <?php for( $j=0; $j < count($markers); $j++): ?>
        <?php if( $data[$i]['id'] == $markers[$j] ): ?>
            <div class="result-item" data-id="<?=$data[$i]['id'];?>">
                <a href="<?=$data[$i]['url'];?>">

                <?php if( !empty($data[$i]['title']) ): ?>
                    <h3><?=$data[$i]['title'];?></h3>
                <?php endif; ?>

                    <div class="result-item-detail">
                        <?php  if( !empty($data[$i]['img_thumb']) ): ?>
                            <div class="image" style="background-image: url(../assets/images/companies/<?=$data[$i]['img_thumb'];?>)">
                                <!-- <?php if( !empty($data[$i]['price']) ): ?>
                                    <div class="price"><?=$data[$i]['price'];?></div>
                                <?php endif; ?> -->
                            </div>
                        <?php else: ?>
                            <div class="image" style="background-image: url(../assets/images/companies/placeholder.png)">
                                <?php if( !empty($data[$i]['price']) ): ?>
                                    <figure class="price"><?=$data[$i]['price'];?></figure>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <div class="description">
                            <?php if( !empty($data[$i]['location']) ): ?>
                                <h5><i class="fa fa-map-marker"></i><?=$data[$i]['location'];?></h5>
                            <?php endif; ?>
                            <?php if( !empty($data[$i]['category']) ): ?>
                                <div class="label label-default"><?=$data[$i]['category_name'];?></div>
                            <?php endif; ?>
                            <?php if( !empty($data[$i]['description']) ): ?>
                                <p><?=character_limiter($data[$i]['description'],30);?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </a>
                <!-- <div class="controls-more">
                    <ul>
                        <li><a href="#" class="add-to-favorites">Add to favorites</a></li>
                        <li><a href="#" class="add-to-watchlist">Add to watchlist</a></li>
                    </ul>
                </div> -->
            </div>
        <?php endif; ?>
    <?php endfor; ?>
<?php endfor; ?>