<div id="slider-nrbi">
    <?php foreach($slider['slides'] as $slide): ?>
        <?php if($slide['type'] == 1):?>
            <div class="section full-screen highlight-right section-border-bottom " style="padding-top:40px; padding-bottom:0px; background-image:url(../assets/images/slider/<?=$slide['filename'];?>); background-repeat:no-repeat; background-position:-50% 40% !important;background-size:cover !important; overflow: hidden; -webkit-background-size:cover;">
                <div class="section_wrapper clearfix">
                    <div class="items_group clearfix">
                        <!-- One Second (1/2) Column -->
                        <div class="column one-second column_placeholder">
                            <div class="placeholder">
                                &nbsp;
                            </div>
                        </div>
                        <!-- One Second (1/2) Column -->
                        <div class="column one-second column_column" style="background: rgba(255,255,255,0.7) !important">
                            <div class="column_attr slider-caption animate fadeInRight">
                                <div style="padding: 40px; box-sizing: border-box; -webkit-box-sizing: border-box;">
                                    <!-- <h5 style="letter-spacing: 4px;">New at Be Space</h5> -->
                                    <h2 style="margin-right: 10%;"><?php echo $slide['title']; ?></h2>
                                    <hr class="no_line hrmargin_b_30" />
                                    <div style="position: relative; margin-left: -20%;"><img class="scale-with-grid" src='<?=base_url("assets/images/home_space_hr.png");?>' alt="">
                                    </div>
                                    <hr class="no_line hrmargin_b_30" />
                                    <h6 style="margin-right: 30%;"><?php echo $slide['subtitle'] ?></h6>
                                    <hr class="no_line hrmargin_b_40" />
                                    <?php if(!empty($slide['url']) AND !is_null($slide['url'])): ?>
                                        <a class="button button_large button_theme button_js" href="<?php echo $slide['url'] ?>"><span class="button_label">Read more</span></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <?php elseif($slide['type'] == 2): ?>
            <div class="section full-screen section-border-bottom " style="padding-top:40px; padding-bottom:0px; background-image:url(assets/images/slider/<?=$slide['filename'];?>); background-repeat:no-repeat; background-position:-60% 40% !important;background-size:cover !important; -webkit-background-size:cover;">
                <div class="section_wrapper clearfix">
                    <div class="items_group clearfix">
                        <div class="column two-third column_placeholder">
                            <div class="placeholder">
                                &nbsp;
                            </div>
                        </div>
                        <div class="column one-third column_column" style="background: rgba(255,255,255,.7) !important">
                            <div class="column_attr slider-caption animate fadeInRight">
                                <div style="padding: 40px; box-sizing: border-box; -webkit-box-sizing: border-box;">
                                    <h2 style="margin-right: 10%; font-size: 45px"><?php echo $slide['title']; ?></h2>
                                    <hr class="no_line hrmargin_b_30" />
                                    <div style="position: relative; margin-left: -20%;"><img class="scale-with-grid" src='<?=base_url("assets/images/home_space_hr.png");?>' alt="">
                                    </div>
                                    <hr class="no_line hrmargin_b_30" />
                                    <h6 style="margin-right: 30%;"><?php echo $slide['subtitle'] ?></h6>
                                    <hr class="no_line hrmargin_b_40" />
                                    <?php if(!empty($slide['url']) AND !is_null($slide['url'])): ?>
                                        <a class="button button_large button_theme button_js" href="<?php echo $slide['url'] ?>"><span class="button_label">Read more</span></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="section full-screen section-border-bottom" style="padding-top:40px; padding-bottom:40px; background-image:url(assets/images/slider/<?=$slide['filename'];?>); background-position: center;background-repeat:no-repeat;background-size:cover !important; -webkit-background-size:cover;">
                <div class="section_wrapper clearfix">
                    <div class="items_group clearfix" style="position: relative;">
                        <div class="column one-second column_column" style="background: rgba(255,255,255,.7) !important; position: relative; left: 50%; top: 0 !important; transform: translate(-50%,-5%);position: absolute;">
                            <div class="column_attr slider-caption animate fadeInRight">
                                <?php if(!empty($slide['url']) && $slide['url'] !== NULL): ?><a href="#" class="slider-link"><?php endif; ?>
                                    <div style="padding: 20px; box-sizing: border-box; -webkit-box-sizing: border-box;">
                                        <h3 style="text-align: center"><?php echo $slide['title']; ?></h3>
                                        <hr class="no_line hrmargin_b_30" />
                                        <div style="position: relative;">
                                            <img class="scale-with-grid" src='<?=base_url("assets/images/home_space_hr.png");?>' alt="" style="position: absolute; left: 50%;transform: translateX(-50%);">
                                        </div>
                                        <hr class="no_line hrmargin_b_30" />
                                        <h6 style="text-align: center"><?php echo $slide['subtitle'] ?></h6>
                                    </div>
                                <?php if(!empty($slide['url']) && $slide['url'] !== NULL): ?></a><?php endif; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>