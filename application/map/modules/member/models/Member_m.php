<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_m extends MY_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_member()
    {
        $this->db->where('status', 1);
        $this->db->order_by('name', 'asc');
        return $this->db->get('partner')->result_array();
    }

}

/* End of file Member_m.php */
/* Location: ./application/modules/keypoints/models/Member_m.php */