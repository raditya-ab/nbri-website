<div class="section" style="background-color:#eef1ec; color: #D10922; padding:60px 40px 0px">
    <div class="section_wrapper clearfix">
        <div class="items_group clearfix">
            <!-- Three Fourth (3/4) Column -->
            <div class="column three-fourth column_column">
                <div class="column_attr" style=" padding:0 0 0 8%;">
                    <!-- <hr class="no_line hrmargin_b_40" />
                    <div class="column one">
                        <h4>Government Institutions</h4>
                    </div> -->
                    <div class="wrapper-member">
                        <!-- One Third (1/3) Column -->
                        <?php foreach($member_list as $member): ?>
                            <div class="column one-fourth">
                                <div style="margin-right: 20%;">
                                    <div class="image_frame image_item no_link scale-with-grid alignnone no_border">
                                        <div class="image_wrapper"><img class="scale-with-grid" src="<?php echo base_url('assets/images/member/'.$member['img']) ?>" onerror="this.src='http://via.placeholder.com/100x100'" alt="" width="380" height="342" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>
            <!-- One Fourth (1/4) Column -->
            <div class="column one-fourth column_column space-border">
                <div class="column_attr" style=" padding:0 0 0 8%;">
                    <hr class="no_line hrmargin_b_40" />
                    <h3>Member of NBRI</h3>
                    <hr class="no_line hrmargin_b_30" />
                </div>
            </div>

        </div>
    </div>
</div>