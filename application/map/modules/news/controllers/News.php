<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends Public_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_m');
	}

	public function index($category = NULL)
	{
		if(!is_null($category) && !empty($category)){
			$news_category = urldecode($this->security->xss_clean(trim($category)));
			$category_id = $this->news_m->get_category_id($this->security->xss_clean($news_category));

			if(!empty($category_id) && !is_null($category_id)){
				$this->data['list_news'] = $this->news_m->get_news($category_id);
			} else {
				show_404();
			}
		} else {
			$this->data['list_news'] = $this->news_m->get_news();
		}

		$this->data['slider'] = [
			'title' => 'News',
			'background' => 'https://i.ytimg.com/vi/VIIU1nbogzs/maxresdefault.jpg'
		];


		$this->data['list_categories'] = $this->news_m->get_categories();


		$this->template->build('index', $this->data);
	}

	public function read($category_slug, $news_slug)
	{
		if(!$this->news_m->news_is_exist($category_slug, $news_slug)){
			show_404();
		}

		$this->data['news_detail'] = $this->news_m->get_detail($category_slug,$news_slug);

		if(is_null($this->data['news_detail']['img']) ||empty($this->data['news_detail']['img'])){
		  $this->data['news_detail']['bgimg'] = base_url('assets/images/'.$this->session->userdata('website_var')['logo']);
		} else {
		  $this->data['news_detail']['bgimg'] = base_url('assets/images/article/'.$this->data['news_detail']['img']);
		}

		// $this->data['slider'] = [
		// 	'title' => $this->data['news_detail']['title'],
		// 	'background' => base_url('assets/images/article/'.$this->data['news_detail']['bgimg'])
		// ];

        $this->template->set('page_title',$this->data['news_detail']['title']);

		$this->load->view('articles_detail', $data, FALSE);
	}

}

/* End of file News.php */
/* Location: ./application/frontend/modules/news/controllers/News.php */