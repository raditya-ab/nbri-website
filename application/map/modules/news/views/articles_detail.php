<div class="content_wrapper clearfix">
    <div class="sections_group">
        <div class="entry-content">
            <div class="section" style="padding-top:70px; padding-bottom:30px; ">
                <div class="section_wrapper clearfix">
                    <div class="items_group clearfix">
                        <!-- One Second (1/2) Column -->
                        <div class="column one column_column">
                            <h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum fugit sequi, nostrum ad vero sed aspernatur magnam at dolorum corrupti.</h5>
                            <p class="big">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis voluptates reiciendis sapiente maxime voluptas sit amet iure, dignissimos accusamus dolorem quod illo quaerat laborum nisi dolores ut, eius dolorum id itaque. Ipsam officia reiciendis, consectetur architecto molestiae! Autem nulla quos, temporibus consectetur dolorum recusandae, atque laudantium magnam alias adipisci consequatur?</p>
                            <div class="customised-part">
                                This is a fully customisable div, you can add text, images, link through the editor directly
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section the_content no_content">
                <div class="section_wrapper">
                    <div class="the_content_wrapper"></div>
                </div>
            </div>
        </div>
    </div>
</div>
