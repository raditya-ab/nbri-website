<style>
    .news-item .photo
    {
        transition: all .5s;
    }
    .news-item .photo:hover
    {
        transition: all .3s;
        transform: scale(1.1);
    }
</style>
<div class="client">
    <div class="section" style="padding: 50px 0;">
        <div class="container ">
            <div class="row">
               <div class="col-md-2 form-group pull-right text-right">
                   <h3 class="category-label">Filter</h3>
                   <select class="form-control custom-select" id="news_category">
                       <option value="">Category</option>
                       <?php foreach($list_categories as $category): ?>
                         <option value="<?=$category['id'];?>"><?=$category['category_name'];?></option>
                       <?php endforeach; ?>
                   </select>
               </div> 
            </div>
            <div class="row">
              <?php foreach($list_news as $news): ?>
                <?php
                  $tags = array();
                  if(!is_null($news['tags']) && !empty($news['tags'])){
                    $tags = explode(',',$news['tags']);
                  }
                  if(is_null($news['img']) ||empty($news['img'])){
                    $img = base_url('assets/images/'.$this->session->userdata('website_var')['logo']);
                  } else {
                    $img = base_url('assets/images/article/'.$news['img']);
                  }
                ?>
                <article class="news-item col-md-3 col-sm-6 col-xs-12 <?=$news['category_slug'];?>">
                    <a href="<?=site_url('news/read/'.$news['category_slug'].'/'.$news['news_slug']);?>">
                        <div class="photo" style="background-image: url('<?=$img;?>')">
                          <?php if(!empty($tags)): ?>
                            <div class="tags text-right" style="width: 100%; padding: 15px 0px">
                              <?php foreach($tags as $tag): ?>
                                <span style="margin: 0px 5px;padding: 5px 10px; outline: 1px solid #fff; color: #fff; font-size: 10px"><?=$tag;?></span>
                              <?php endforeach; ?>
                            </div>
                          <?php endif; ?>
                            <header class="text-left">
                                <h2 class="news-title" style="word-break:break-all"><?=$news['title'];?></h2>
                                <div style="text-decoration: underline; color: #FFF; font-weight: 600">
                                  <?=$news['category_name'];?>
                                </div>
                                <span href="#." class="readmore">Read More</span>
                            </header>
                        </div>
                    </a>
                </article>
              <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
