<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Public_Controller {

    public function __construct()
    {
        parent::__construct();
        // $this->template->set_layout('default_template');
    }

    public function home()
    {
        $this->template->set_layout('homepage');
        $this->template->build('homepage');
    }

    public function map_overview($category = NULL)
    {
        $this->template->set_layout('homepage');
        $this->template->set('category', $category);
        $this->template->build('maps');
    }

    public function get_markers_ajax()
    {
        $this->load->model('maps_m');
        $data = [];

        $marker_data = $this->maps_m->fetch_markers();

        foreach($marker_data as $marker){
            $temp = array(
                'id' => $marker['id'],
                'category' => $marker['category'],
                'category_name' => $marker['category_name'],
                'category_slug' => $marker['category_slug'],
                'title' => $marker['title'],
                'slug' => $marker['slug'],
                'description' => $marker['description'],
                'location' => $marker['location'],
                'latitude' => (float)$marker['latitude'],
                'longitude' => (float)$marker['longitude'],
                'url' => $marker['url']
            );

            $data[] = $temp;
        }


        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function detail($slug)
    {
        $this->data['detail'] = $this->company_m->get_detail($slug);
    }

}

/* End of file Home.php */
/* Location: ./application/modules/home/controllers/Home.php */