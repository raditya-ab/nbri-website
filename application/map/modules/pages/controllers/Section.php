<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends Public_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pages/section_m','section_m');
        $this->load->model('pages_m');
    }

    public function show_section($pages_slug,$section_slug)
    {
        $section_detail = $this->section_m->get_section_detail(NULL,$section_slug);
        echo stripslashes(htmlspecialchars_decode($section_detail['content']));

    }

}

/* End of file Section.php */
/* Location: ./application/modules/pages/controllers/Section.php */