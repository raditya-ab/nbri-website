<div class="hero-section has-background full-screen">
    <div class="wrapper">
        <div class="inner">
            <div class="center">
                <div class="page-title">
                    <h1 style="font-weight: 800">NBRI One Map</h1>
                </div>
            </div>
        </div>
        <!--end element-->
    </div>
    <!--end vertical-aligned-elements-->

    <div class="slider">
        <div class="owl-carousel opacity-40" data-owl-nav="0" data-owl-dots="0" data-owl-autoplay="1" data-owl-fadeout="1" data-owl-loop="1">
            <div class="image">
                <div class="bg-transfer"><img src="assets/img/background-05.jpg" alt=""></div>
            </div>
            <div class="image">
                <div class="bg-transfer"><img src="assets/img/background-06.jpg" alt=""></div>
            </div>
            <div class="image">
                <div class="bg-transfer"><img src="assets/img/background-03.jpg" alt=""></div>
            </div>
        </div>
        <!--end owl-carousel-->
        <div class="background-wrapper">
            <div class="background-color background-color-black"></div>
        </div>
        <!--end background-wrapper-->
    </div>
    <!--end slider-->
</div>
<!--end hero-section-->

<section class="block" style="margin-top: 50px">
    <div class="container">
        <div class="center">
            <div class="section-title">
                <div class="center">
                    <h2>Recent Places</h2>
                    <h3 class="subtitle">Fusce eu mollis dui, varius convallis mauris. Nam dictum id</h3>
                </div>
            </div>
            <!--end section-title-->
        </div>
        <!--end center-->
        <div class="row slider-show">
            <div class="col-md-12 center">
                <img src="assets/img/onemap.png" alt="" style="max-width: 500px">
                <h2>sdsdsd</h2>
            </div>
        </div>
        <div class="row slider-nav" style="margin-top: 30px">
            <div class="col-md-3">
                <img src="assets/img/onemap.png" alt="" style="max-width: 500px">
                <h2>sdsdsd</h2>
            </div>
            <div class="col-md-3">
                <img src="assets/img/onemap.png" alt="" style="max-width: 500px">
                <h2>sdsdsd</h2>
            </div>
            <div class="col-md-3">
                <img src="assets/img/onemap.png" alt="" style="max-width: 500px">
                <h2>sdsdsd</h2>
            </div>
            <div class="col-md-3">
                <img src="assets/img/onemap.png" alt="" style="max-width: 500px">
                <h2>sdsdsd</h2>
            </div>
        </div>
        <!--end row-->
    <!--end center-->
    </div>
    <!--end container-->
</section>
<!--end block-->
