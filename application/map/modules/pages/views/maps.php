<div class="hero-section full-screen has-map has-sidebar" style="position: relative;">
    <div class="map-wrapper" style="width: 70%">
        <div class="geo-location">
            <i class="fa fa-map-marker"></i>
        </div>
        <div class="map" id="map-homepage"></div>
    </div>
    <!--end map-wrapper-->

    <div class="results-wrapper" style="width: 30%; color: #181D62">
        <div class="form search-form inputs-underline" id="form-result-search">
            <form>
                <div class="section-title">
                    <h2>Search</h2>
                    <button class="btn bg-navy no-border pull-right" type="button" id="btn-collapse-search">SHOW</button>
                </div>
                <div id="form-search-field">
                    <div class="form-group">
                        <input type="text" class="form-control" name="keyword" placeholder="Enter keyword">
                    </div>
                    <!--end form-group-->
                    <!-- <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <select class="form-control selectpicker" name="city">
                                    <option value="">Facility</option>
                                        <option value="1">New York</option>
                                        <option value="2">Washington</option>
                                        <option value="3">London</option>
                                        <option value="4">Paris</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <select class="form-control selectpicker" name="category">
                                    <option value="">Category</option>
                                    <option value="hulu">HULU</option>
                                    <option value="hilir">HILIR</option>
                                    <option value="battery-cell">Battery Cell</option>
                                    <option value="sdm">SDM</option>
                                </select>
                            </div>
                        </div>
                    </div> -->
                    <!--end row-->
                    <div class="row">
                        <!-- <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control date-picker" name="min-price" placeholder="" data-date-format="MM/DD/YYYY/HH:mm">
                            </div>
                        </div> -->
                        <!-- <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <div class="ui-slider" id="price-slider" data-value-min="10" data-value-max="400" data-value-type="price" data-currency="" data-currency-placement="before">
                                    <div class="values clearfix">
                                        <input class="value-min" name="value-min[]" readonly>
                                        <input class="value-max" name="value-max[]" readonly>
                                    </div>
                                    <div class="element"></div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <!--end row-->
                    <div class="form-group">
                        <button type="submit" data-ajax-response="map" data-ajax-data-file="assets/external/data_2.php" data-ajax-auto-zoom="1" class="btn bg-navy no-border pull-right"><i class="fa fa-search"></i></button>
                    </div>
                    <!--end form-group-->
                </div>
            </form>
            <!--end form-hero-->
        </div>


        <div class="results">
            <div class="tse-scrollable">
                <div class="tse-content">
                    <div class="section-title">
                        <h2>Search Results<span class="results-number"></span></h2>
                    </div>
                    <!--end section-title-->
                    <div class="results-content"></div>
                    <!--end results-content-->
                </div>
                <!--end tse-content-->
            </div>
            <!--end tse-scrollable-->
        </div>
        <!--end results-->
    </div>
    <!--end results-wrapper-->
</div>