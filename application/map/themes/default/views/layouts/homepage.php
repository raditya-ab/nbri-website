<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Raditya">

    <link href="<?php echo base_url('assets/fonts/font-awesome.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/fonts/elegant-fonts.css');?>" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.css');?>" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/zabuto_calendar.min.css');?>" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.css');?>" type="text/css">

    <link rel="stylesheet" href="<?php echo base_url('assets/css/trackpad-scroll-emulator.css');?>" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.nouislider.min.css');?>" type="text/css">
    <script src="https://kit.fontawesome.com/e7c6f13eeb.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css');?>" type="text/css">

    <?php if(ENVIRONMENT != NULL):?>
        <?php $active_env = strtolower(ENVIRONMENT); ?>
        <?php if($active_env == 'production'): ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178037418-1"></script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-178037418-1');
            </script>
        <?php endif; ?>
    <?php endif; ?>

    <title><?=$template['title'];?></title>

</head>

<body class="homepage">
<div class="page-wrapper">
    <?=$template['partials']['header'];?>
    <!--end page-header-->

    <div id="page-content">
        <?=$template['body'];?>
        <!--end hero-section-->

        <!--end container-->

        <!--end block-->
    </div>
    <!--end page-content-->

    <?=$template['partials']['footer'];?>
    <!--end page-footer-->
</div>
<!--end page-wrapper-->
<a href="#" class="to-top scroll" data-show-after-scroll="600"><i class="arrow_up"></i></a>

<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-2.2.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-select.min.js');?>"></script>
<?php if(ENVIRONMENT != NULL):?>
    <?php $active_env = strtolower(ENVIRONMENT); ?>
    <?php if($active_env == 'production'): ?>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCjwCjFIDt3mh2WJs4I3tXr0ja3rKeandk"></script>
    <?php else: ?>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58"></script>
    <?php endif; ?>
<?php endif; ?>
<script type="text/javascript" src="<?php echo base_url('assets/js/richmarker-compiled.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/markerclusterer_packed.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/infobox.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.fitvids.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/icheck.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/owl.carousel.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.trackpad-scroll-emulator.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/moment.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datetimepicker.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.nouislider.all.min.js');?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script type="text/javascript" src="<?php echo base_url('assets/js/custom.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/maps.js');?>"></script>

<script>
    var siteURL = "<?=site_url();?>";
    var baseURL = "<?=base_url();?>";
    var category = "<?=$category;?>";
    var optimizedDatabaseLoading = 1;
    var _latitude = -6.295643;
    var _longitude = 106.666775;
    var element = "map-homepage";
    var markerTarget = "modal"; // use "sidebar", "infobox" or "modal" - defines the action after click on marker
    var sidebarResultTarget = "modal"; // use "sidebar", "modal" or "new_page" - defines the action after click on marker
    var showMarkerLabels = true; // next to every marker will be a bubble with title
    var mapDefaultZoom = 8; // default zoom
    heroMap(_latitude,_longitude, element, markerTarget, sidebarResultTarget, showMarkerLabels, mapDefaultZoom, siteURL, category);
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#btn-collapse-search").on('click', function(e){
            e.preventDefault();

            $("#form-search-field").toggle('slow');
        })
    });

    $('.results-wrapper').resizable({
        handles: 'n,w,s,e',minWidth: 200,
        maxWidth: 400
    });
</script>

</body>

