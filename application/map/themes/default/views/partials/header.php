<style type="text/css" media="screen">
    #page-header nav
    {
        background-color: #181D62;
        height: 60px;
    }
    #page-header nav .right .secondary-nav a
    {
        color: #FFF
    }
    #page-header nav .right
    {
        width: 100%;
    }
    #page-header nav .right .secondary-nav a:hover
    {
        color: #b9dc00;
    }
    .homepage h2,
    .hero-section .results-wrapper .result-item > a h3
    {
        color: #181D62;
    }
    .btn.btn-primary
    {
        background-color: #181D62;
        color: #b9dc00;
    }
</style>
<header id="page-header">
    <div class="topbar bg-green">
        <div class="container" style="height: 30px">
            <div class="row" style="height: 30px; vertical-align: middle">
                <!-- <div class="container"> -->
                    <div class="col-md-6 search" style="display: flex; align-items: center;height: inherit">
                        <a href="#" class="font-color-navy">
                            <i class="fas fa-search"></i>
                        </a>
                    </div>

                    <div class="col-md-6 login" style="padding-right: 40px;display: flex; justify-content: flex-end; align-items: center;height: inherit">
                        <?php if(!$this->aauth->is_loggedin()): ?>
                        <a href="<?=site_url('../membership/login');?>" class="font-color-navy">
                            Login/Register
                        </a>
                        <?php else: ?>
                            <a href="<?=site_url('../membership/profile');?>" class="font-color-navy dropdown" style="margin-right: 20px">
                                My Profile

                            </a>
                            <a href="<?=site_url('../membership/logout');?>" class="font-color-navy dropdown">
                                Log Out
                            </a>
                        <?php endif; ?>
                    </div>

                <!-- </div> -->
            </div>
        </div>
    </div>
    <nav>
        <div class="container">
            <div class="left">
                <a href="<?=site_url('../');?>" class="brand"><img src='<?=base_url('../assets/images/nbri_new_logo_with_text.png');?>' alt="" style="height: 40px"></a>
            </div>
            <!--end left-->
            <?=$template['partials']['navbar'];?>
            <!--end right-->
        </div>
    </nav>
    <!--end nav-->
</header>