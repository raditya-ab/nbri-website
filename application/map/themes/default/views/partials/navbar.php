<div class="right">
    <!--end primary-nav-->
    <div class="secondary-nav">
        <a href="<?=site_url('../');?>">Home</a>
        <a href="<?=site_url('../nbri-onemap');?>">NBRI One Map</a>
        <?php foreach($nav_menu as $menu): ?>
            <a href="<?=site_url($menu['slug']);?>"><?=$menu['name'];?></a>
        <?php endforeach; ?>
    </div>
    <!--end secondary-nav-->
    <div class="nav-btn">
        <i></i>
        <i></i>
        <i></i>
    </div>
    <!--end nav-btn-->
</div>