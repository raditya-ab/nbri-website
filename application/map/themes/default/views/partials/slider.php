<div id="slider-nrbi">
    <div class="section full-screen highlight-right section-border-bottom " style="padding-top:40px; padding-bottom:0px; background-image:url(https://faraday.ac.uk/wp-content/uploads/2019/04/faraday-day-2-19-resized-1.jpg); background-repeat:no-repeat; background-position:-80% 40% !important;background-size:contain !important; overflow: hidden; -webkit-background-size:contain;">
        <div class="section_wrapper clearfix">
            <div class="items_group clearfix">
                <!-- One Second (1/2) Column -->
                <div class="column one-second column_placeholder">
                    <div class="placeholder">
                        &nbsp;
                    </div>
                </div>
                <!-- One Second (1/2) Column -->
                <div class="column one-second column_column" style="background: rgba(255,255,255,0.7) !important">
                    <div class="column_attr slider-caption animate fadeInRight">
                        <div style="padding: 40px; box-sizing: border-box; -webkit-box-sizing: border-box;">
                            <!-- <h5 style="letter-spacing: 4px;">New at Be Space</h5> -->
                            <h2 style="margin-right: 10%;">Education
                                <br>
                                and Skills Development</h2>
                            <hr class="no_line hrmargin_b_30" />
                            <div style="position: relative; margin-left: -20%;"><img class="scale-with-grid" src='<?=base_url("assets/images/home_space_hr.png");?>' alt="">
                            </div>
                            <hr class="no_line hrmargin_b_30" />
                            <h6 style="margin-right: 30%;">The race to electrification needs young people, interested in science and engineering, with the drive to play a part in changing all our futures. Join our programmes.</h6>
                            <hr class="no_line hrmargin_b_40" />
                            <a class="button button_large button_theme button_js" href="about.html"><span class="button_label">Read more</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section full-screen section-border-bottom " style="padding-top:40px; padding-bottom:0px; background-image:url(https://faraday.ac.uk/wp-content/uploads/2019/04/faraday-day-2-19-resized-1.jpg); background-repeat:no-repeat; background-position:-60% 40% !important;background-size:cover !important; -webkit-background-size:cover;">
        <div class="section_wrapper clearfix">
            <div class="items_group clearfix">
                <div class="column two-third column_placeholder">
                    <div class="placeholder">
                        &nbsp;
                    </div>
                </div>
                <div class="column one-third column_column" style="background: rgba(255,255,255,.7) !important">
                    <div class="column_attr slider-caption animate fadeInRight">
                        <div style="padding: 40px; box-sizing: border-box; -webkit-box-sizing: border-box;">
                            <h2 style="margin-right: 10%; font-size: 45px">Education
                                <br>
                                and Skills Development</h2>
                            <hr class="no_line hrmargin_b_30" />
                            <div style="position: relative; margin-left: -20%;"><img class="scale-with-grid" src='<?=base_url("assets/images/home_space_hr.png");?>' alt="">
                            </div>
                            <hr class="no_line hrmargin_b_30" />
                            <h6 style="margin-right: 30%;">The race to electrification needs young people, interested in science and engineering, with the drive to play a part in changing all our futures. Join our programmes.</h6>
                            <hr class="no_line hrmargin_b_40" />
                            <a class="button button_large button_theme button_js" href="about.html"><span class="button_label">Read more</span></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="section full-screen section-border-bottom " style="padding-top:40px; padding-bottom:40px; background-image:url(https://faraday.ac.uk/wp-content/uploads/2019/04/faraday-day-2-19-resized-1.jpg); background-repeat:no-repeat; background-position:-60% 40% !important;background-size:cover !important; -webkit-background-size:cover;">
        <div class="section_wrapper clearfix">
            <div class="items_group clearfix" style="position: relative;">

                <div class="column one-second column_column" style="background: rgba(255,255,255,.7) !important; position: relative; left: 50%; top: 0 !important; transform: translate(-50%,-5%);position: absolute;">
                    <div class="column_attr slider-caption animate fadeInRight">
                        <div style="padding: 20px; box-sizing: border-box; -webkit-box-sizing: border-box;">
                            <h3 style="text-align: center">Education
                                <br>
                                and Skills Development</h3>
                            <hr class="no_line hrmargin_b_30" />
                            <div style="position: relative;">
                                <img class="scale-with-grid" src='<?=base_url("assets/images/home_space_hr.png");?>' alt="" style="position: absolute; left: 50%;transform: translateX(-50%);">
                            </div>
                            <hr class="no_line hrmargin_b_30" />
                            <h6 style="text-align: center">The race to electrification needs young people, interested in science and engineering, with the drive to play a part in changing all our futures. Join our programmes.</h6>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>