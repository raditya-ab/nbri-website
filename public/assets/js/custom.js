var acceptCookie = (document.cookie.match(/^(?:.*;)?\s*acceptCookie\s*=\s*([^;]+)(?:.*)?$/) || [, null])[1];

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

$(document).ready(function () {
    if (acceptCookie == null) {
        $('#privacyCookieModal').modal('show')
    }

    $("#btnAcceptCookie").click(function (e) {
        e.preventDefault();

        createCookie('acceptCookie', 'true', 1000);
        $('#privacyCookieModal').modal('hide')
    });
    $('#map-slider-focus').slick({
        asNavFor: '#maps-slider-nav',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        focusOnSelect: true,
    });
    $('#maps-slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        asNavFor: '#map-slider-focus',
        // dots: true,
        centerMode: true,
        focusOnSelect: true,
        responsive: [{
                breakpoint: 1199,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }
        ]
        // prevArrow: $("#btn-left"),
        // nextArrow: $("#btn-right")
    });

    $("#quotes_slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: $("#btn-text-slider-left"),
        nextArrow: $("#btn-text-slider-right")
    })

    $("#expert-slider").slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: $("#btn-expert-prev"),
        nextArrow: $("#btn-expert-next"),
        pauseOnHover: true,
        responsive: [{
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }
        ]
    });

    $("#article-featured-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
        prevArrow: $("#btn-expert-prev"),
        nextArrow: $("#btn-expert-next"),
        pauseOnHover: true,
        responsive: [{
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }
        ]
    });

    $("#accordionExample").on('shown.bs.collapse', function (e) {
        $(this).find('.card-header').removeClass('bg-yellow');
        $(this).find('.card-header').addClass('bg-navy');
        $(this).find('.show').siblings('.card-header').removeClass('bg-navy');
        $(this).find('.show').siblings('.card-header').addClass('bg-yellow');
        $(this).find('.show').siblings('.card-header').find('i').removeClass('fa-chevron-down');
        $(this).find('.show').siblings('.card-header').find('i').addClass('fa-chevron-up');
        // console.log($(this).find(".card-header").attr('id'));
    })
    $("#accordionExample").on('hide.bs.collapse', function (e) {
        $(this).find('.card-header').removeClass('bg-yellow');
        $(this).find('.card-header').addClass('bg-navy');
        $(this).find('.collapsing').siblings('.card-header').find('i').addClass('fa-chevron-down');

    })

    $("#form_login").on("submit", function (e) {
        e.preventDefault();

        var options = {
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            datatype: 'json',
            clearForm: true,
            resetForm: true,
            beforeSubmit: function () {
                Swal.fire({
                    title: 'Logging In',
                    html: "<center><div class='dot-typing mt-5 mb-5'></div></center>"
                });
            },
            success: function (resp) {
                var option = {
                    html: "<div class='text-center mt-5 mb-5'>" + resp.message.text + "</div>'"
                }

                if (resp.status) {
                    option.title = 'Login Success!';
                    option.icon = 'success';
                    option.showCancelButton = false;
                    option.showConfirmButton = false;
                    option.allowOutsideClick = false;
                } else {
                    option.title = 'Login Failed!';
                    option.icon = 'error'
                }

                Swal.update(option);

                if (resp.status) {
                    setTimeout(function () {
                        window.location.href = siteURL;
                    }, 3000)
                }
            }
        }

        // console.log(options);
        $(this).ajaxSubmit(options);

        // return false;
    });

    $("#form_register").on("submit", function (e) {
        e.preventDefault();

        var options = {
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            datatype: 'json',
            clearForm: false,
            resetForm: false,
            beforeSubmit: function () {
                Swal.fire({
                    title: 'Submitting your data',
                    html: "<center><div class='dot-typing mt-5 mb-5'></div></center>"
                });
            },
            success: function (resp) {
                var option = {
                    html: "<div class='text-center mt-5 mb-5'>" + resp.message.text + "</div>'"
                }

                if (resp.status) {
                    option.title = 'Register Success!';
                    option.icon = 'success';
                    option.showCancelButton = false;
                    option.showConfirmButton = false;
                    option.allowOutsideClick = false;
                } else {
                    option.title = 'Error!';
                    option.icon = 'error'
                }

                Swal.update(option);

                if (resp.status) {
                    setTimeout(function () {
                        window.location.href = siteURL;
                    }, 3000)
                }
            }
        }

        // console.log(options);
        $(this).ajaxSubmit(options);

        // return false;
    })

    $("#photo").on('change', function (e) {
        readURL($(this));
    })

    $("#form_edit_profile").on('submit', function (e) {
        e.preventDefault();
        var _this = $(this);

        var options = {
            url: _this.attr('action'),
            type: _this.attr('method'),
            datatype: 'json',
            contentType: false,
            processData: false,
            beforeSubmit: function () {
                Swal.fire({
                    title: 'Submitting your data',
                    html: "<center><div class='dot-typing mt-5 mb-5'></div></center>"
                });
            },
            success: function (resp) {
                var option = {
                    html: "<div class='text-center mt-5 mb-5'>" + resp.message.text + "</div>"
                }

                if (resp.status) {
                    option.title = 'Update Success!';
                    option.html += "<br>We'll take you back to profile page in 3 seconds.";
                    option.icon = 'success';
                    option.showCancelButton = false;
                    option.showConfirmButton = false;
                    option.allowOutsideClick = false;
                } else {
                    option.title = 'Update Failed!';
                    option.icon = 'error'
                }

                Swal.update(option);

                if (resp.status) {
                    setTimeout(function () {
                        window.location.href = siteURL + 'membership/profile/' + _this.data('identifier');
                    }, 3000)
                }
            }
        }

        // console.log(options);
        $(this).ajaxSubmit(options);
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $("#img-profile").css('background-image', 'url(' + e.target.result + ')');
        }
        reader.readAsDataURL(input.files[0]);
    }
}
