var popupOpened = 0;
var btnDateSticky = $("#btn-date-sticky");
var btnDateStickyPos = btnDateSticky.css("right");
var stickyDateWindow = $("#sticky-date-window");

function openFixedPopup(elem, anchor = "right", defaultPos = 0) {
  var style = {};
  style[anchor] = defaultPos;
  elem.animate(style, 300);
}

function closeFixedPopup(elem, elemOuterWidth, anchor = "right") {
  var style = {};
  style[anchor] = -elemOuterWidth;
  elem.animate(style, 300);
}

function adjustWindowHeight() {
  $("#smartwizard").height("100%");
  $(".tab-content").height("100%");
}
$(document).ready(function () {
    $('#btn-date-sticky').on('click', function (e) {
        e.preventDefault();
        openFixedPopup($('#sticky-date-window'));

        popupOpened = 1;
    });

    $('.btn-close').on('click', function (e) {
        e.preventDefault();
        var parentElem = $(this).closest('.popup-sticky');
        if (popupOpened) {
            closeFixedPopup(parentElem, parentElem.outerWidth());
            popupOpened = 0;
        }
    });

    var countdownElem = $('.countdown');
    countdownElem.each((index, elem) => {
        var year = $(elem).data('year');
        var month = $(elem).data('month');
        var day = $(elem).data('day');
        var hours = $(elem).data('hours');

        simplyCountdown(elem, {
            year: year,
            month: month,
            day: day,
            hours: hours,
            enableUtc: true,
            refresh: 1000,
        });
    });

    // $("#smartwizard").smartWizard({
    //   theme: "dots",
    //   enableURLhash: false,
    //   showStepURLhash: false,
    //   cycleSteps: false,
    //   anchorSettings: {
    //     anchorClickable: false, // Enable/Disable anchor navigation
    //     enableAllAnchors: false, // Activates all anchors clickable all times
    //     markDoneStep: true, // Add done state on navigation
    //     markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
    //     removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
    //     enableAnchorOnDoneStep: true, // Enable/Disable the done steps navigation
    //   },
    //   toolbarSettings: {
    //     toolbarPosition: "none", // none, top, bottom, both
    //     toolbarButtonPosition: "right", // left, right, center
    //     showNextButton: true, // show/hide a Next button
    //     showPreviousButton: false, // show/hide a Previous button
    //     toolbarExtraButtons: [], // Extra buttons to show on toolbar, array of jQuery input/buttons elements
    //   },
    //   autoAdjustHeight: true,
    //   transition: {
    //     animation: "fade", // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
    //     speed: "400", // Transion animation speed
    //     easing: "", // Transition animation easing. Not supported without a jQuery easing plugin
    //   },
    // });
    var selectedCategory = '';

    $('input[type=radio][name=event_category]').on('change', function (e) {
        var selected = $(this).val();
        var option_text = $(this).data('text');
        selectedCategory = selected;

        $('input[type=hidden][name=event_category_name').val(option_text);
        $('input[type=hidden][name=event_package_type]').val('');
    });

    $('#nationality,select[name=occupation]').on('change', function () {
        var nationality = $('#nationality').val();
        var occupation = $('select[name=occupation]').val();

        if (nationality != '' && occupation != '') {
            $('.package-option')
                .not('.' + nationality + '-option' + '.' + occupation + '-option')
                .fadeOut();
            $('.' + nationality + '-option' + '.' + occupation + '-option').fadeIn();
        } else if (nationality != '' && occupation == '') {
            $('.package-option')
                .not('.' + nationality + '-option')
                .fadeOut();
            $('.' + nationality + '-option').fadeIn();
        } else if (nationality == '' && occupation != '') {
            $('.package-option')
                .not('.' + occupation + '-option')
                .fadeOut();
            $('.' + occupation + '-option').fadeIn();
        } else {
            $('.package-option').fadeIn();
        }

        // adjustWindowHeight();
    });

    $('input[type=radio][name=event_package]').on('change', function () {
        var _this = $(this);
        var option_type = $(this).data('type');

        var currency = _this.data('currency_price');
        var currency_code = currency == '$' ? 'USD' : 'IDR';
        var earlyBirdPrice = _this.data('early_bird_price');
        var normalPrice = _this.data('normal_price');
        var selected_card = _this.closest('.package-option');

        $('input[type=hidden][name=event_package_type]').val(option_type);

        selected_card.siblings().removeClass('bg-yellow bg-green').addClass('bg-green');
        selected_card.removeClass('bg-green');
        selected_card.addClass('bg-yellow');

        $('input[name=currency]').val(currency);
        $('input[name=currency_code]').val(currency_code);
        $('input[name=early_bird_price]').val(earlyBirdPrice);
        $('input[name=normal_price]').val(normalPrice);

        if (selected_card.hasClass('presenter-option')) {
            $('#conference_presenter_field').find('input, select').removeAttr('disabled');
            $('#conference_presenter_field').fadeIn();
        } else {
            $('#conference_presenter_field').fadeOut();
            $('#conference_presenter_field').find('input, select').attr('disabled', 'true');
        }
    });

    $('#prefix').on('change', function () {
        var value = $(this).val();

        if (value == 'Other') {
            $('#prefix_other').fadeIn();
            $('#prefix_other').find('input').removeAttr('disabled');
            $('#prefix_other').find('input').attr('required');
        } else {
            $('#prefix_other').fadeOut();
            $('#prefix_other').prop('disabled', true);
            $('#prefix_other').find('input').removeAttr('required');
        }

        adjustWindowHeight();
    });
    $('#referer').on('change', function () {
        var value = $(this).val();

        if (value == 'Other') {
            $('#referer_other').fadeIn();
            $('#referer_other').find('input').removeAttr('disabled');
            $('#referer_other').find('input').attr('required');
        } else {
            $('#referer_other').fadeOut();
            $('#referer_other').prop('disabled', true);
            $('#referer_other').find('input').removeAttr('required');
        }

        adjustWindowHeight();
    });

    $('#smartwizard').on('leaveStep', function (e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
        if (stepDirection === 'forward') {
            // var emptyRequiredField = $("#step-"+(currentStepIndex+1)).find("input:not(:disabled), select:not(:disabled)").filter('[required]:visible');
            var emptyRequiredField = $('#step-' + (currentStepIndex + 1))
                .find('.required-field')
                .find(
                    'input:visible:not(:disabled), input[type=radio]:visible:not(:disabled), select:visible:not(:disabled)'
                )
                .filter(function () {
                    // if($(this).is(':radio')){
                    //     $(this).attr('name')
                    // }
                    return $(this).attr('required') && $(this).val().length == 0;
                });

            // console.log("#step-"+(currentStepIndex+1));
            // console.log($("#form-registration-event").serializeArray());

            if (emptyRequiredField.length > 0) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops..',
                    text: 'You have to fill all required field!',
                });
                // alert('You have to fill all required field!');
                return false;
            }
        }

        return true;
    });

    $('#smartwizard').on('showStep', function (e, anchorObject, stepIndex, stepDirection, stepPosition) {
        if (stepPosition === 'last') {
            $('#btn-checkout').show();
        } else {
            $('#btn-checkout').hide();
        }
    });

    // $("#form-registration-event").on('submit', function(e){
    //     e.preventDefault();
    //     _this_form = $(this);
    //     var swalOption = {
    //         title: 'Submitting',
    //         html: "<center><div class='dot-typing mt-5 mb-5'></div></center>",
    //         showCancelButton: false,
    //         showConfirmButton: false,
    //         allowOutsideClick: false
    //     };

    //     var options = {
    //         url: _this_form.attr('action'),
    //         type: _this_form.attr('method'),
    //         datatype: 'json',
    //         clearForm: false,
    //         resetForm: false,
    //         beforeSubmit: function () {
    //             Swal.fire(swalOption);
    //         },
    //         success: function(resp){
    //             if (resp.status) {
    //                 Swal.close();

    //                 swalOption.title = resp.message.text;
    //                 Swal.update(swalOption);

    //                 setTimeout(function(){
    //                     window.location = resp.redirect_url;
    //                 }, 1000);
    //             } else {
    //                 Swal.fire({
    //                     icon: 'error',
    //                     title: 'Error',
    //                     html: resp.message.text,
    //                     showCancelButton: false,
    //                     showConfirmButton: true,
    //                     allowOutsideClick: true
    //                 });
    //             }
    //         },
    //         error: function(){
    //             Swal.fire({
    //                 title: 'Error',
    //                 text: "An error happened when submitting your data, please contact your network administrator",
    //                 showCancelButton: false,
    //                 showConfirmButton: true,
    //                 allowOutsideClick: true
    //             });
    //         }
    //     }

    //     _this_form.ajaxSubmit(options)
    //     // .done(function (resp) {

    //     // }).fail(function(err){
    //     //     Swal.fire({
    //     //         title: 'Error',
    //     //         text: "An error happened when submitting your data, please contact your network administrator",
    //     //         showCancelButton: false,
    //     //         showConfirmButton: true,
    //     //         allowOutsideClick: true
    //     //     });
    //     // })
    // })

    // $('input[name=conference-package]').on('change', function(e){
    //     var _this = $(this);
    //     $("#conference-package-text").val(_this.data('text-value'));

    //     if(_this.val().indexOf("participant") != -1){
    //         $(".conference-field-only input,.conference-field-only select").prop('disabled', function(i, v) { return !v; });
    //         $(".conference-field-only").hide();
    //     } else {
    //         $(".conference-field-only input,.conference-field-only select").removeAttr('disabled');
    //         $(".conference-field-only").show();
    //     }
    // })

    $('.btn-checkout').on('click', function (e) {
        e.preventDefault();

        var _this = $(this);

        _this.html('Loading... <i class="fa fa-spinner fa-spin"></i>');
        $.ajax({
            url: siteURL + 'event/icb-rev-2022/registration/generate_order_token',
            type: 'post',
            dataType: 'json',
        }).done(function (response) {
            _this.text('Checkout');
            if (response.status) {
                snap.pay(response.token),
                    {
                        // Optional
                        onSuccess: function (res) {
                            console.log(res);
                        },
                        // Optional
                        onPending: function (res) {
                            console.log(res);
                        },
                        // Optional
                        onError: function (res) {
                            console.log(res);
                        },
                        onClose: function () {
                            console.log('customer closed the popup without finishing the payment');
                        },
                    };
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    html: response.message.text + "<br><br>You'll be redirected to event homepage in 3 seconds...",
                    showCancelButton: false,
                    showConfirmButton: true,
                    allowOutsideClick: true,
                });
            }
        });
    });

    $('#btn-checkout-fix').on('click', function (e) {
        e.preventDefault();

        var swalOption = {
            title: 'Submitting',
            html: "<center><div class='dot-typing mt-5 mb-5'></div></center>",
            showCancelButton: false,
            showConfirmButton: false,
            allowOutsideClick: false,
        };

        var options = {
            url: siteURL + 'event/icb-rev-2021/registration/checkout',
            type: 'post',
            datatype: 'json',
            clearForm: false,
            resetForm: false,
            beforeSubmit: function () {
                Swal.fire(swalOption);
            },
            success: function (resp) {
                if (resp.status) {
                    Swal.close();

                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        html: resp.message.text + "<br><br>You'll be redirected to event homepage in 3 seconds...",
                        showCancelButton: false,
                        showConfirmButton: true,
                        allowOutsideClick: true,
                    }).then((result) => {
                        window.location = resp.redirect_url;
                    });

                    setTimeout(function () {
                        window.location = resp.redirect_url;
                    }, 3000);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        html: resp.message.text,
                        showCancelButton: false,
                        showConfirmButton: true,
                        allowOutsideClick: true,
                    });
                }
            },
            error: function () {
                Swal.fire({
                    title: 'Error',
                    text: 'An error happened when submitting your data, please contact your network administrator',
                    showCancelButton: false,
                    showConfirmButton: true,
                    allowOutsideClick: true,
                });
            },
        };
        $.ajax(options);
    });

    $('#wywl-wrap').slick({
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '30px',
        dots: true,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    });

    $('input[type=radio][name="event_package"]').change(function (e) {
        var _class = $(this).data('class');

        $('.nbri-yic_2022_competition').prop('disabled');
        $('.nbri-yic_2022_competition').hide();

        $('.nbri-yic_2022_field').show();
        $('#abstract_title').removeAttr('disabled');

        $('#' + _class + '_symposia').removeAttr('disabled');
        $('#' + _class + '_symposia').show();

        // alert($(this).data('class'));
    });
});
