var datatable;

function TeamList(instance) {
  this.instance = $(instance);
  this.init = function(){
    return this.instance.mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'POST',
            url: siteURL + 'expert/get_expert_list',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 1000,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
        saveState: {
          cookie: false,
          webstorage: false
        }
      },
      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },

      search: {
        input: $('#generalSearch'),
        onEnter: false
      },

      // columns definition
      columns: [
        {
          field: 'no',
          title: '#',
          sortable: false, // disable sort for this column
          width: 40,
          selector: false,
          textAlign: 'center',
        }, {
          field: 'img',
          title: 'Logo',
          sortable: 'false', // default sort
          filterable: false, // disable or enable filtering
          width: 100,
          textAlign: 'center',
          template: function(row){
            return "<img src='"+frontURL+"assets/images/"+ row.img +"' onerror=\"this.src='http://via.placeholder.com/100x100'\" width='100'>";
          }
          // basic templating support for column rendering,
          // template: '{{OrderID}} - {{ShipCountry}}',
        },{
          field: 'name',
          title: 'Name',
          filterable: true, // disable or enable filtering
          width: 150
          // basic templating support for column rendering,
          // template: '{{OrderID}} - {{ShipCountry}}',
        },
        // {
        //   field: 'author',
        //   title: 'Author',
        //   width: 150,
        //   textAlign: 'center'
        //   // template: function(row) {
        //   //   // callback function support for column rendering
        //   //   return row.ShipCountry + ' - ' + row.ShipCity;
        //   // },
        // },
        {
          field: 'created',
          title: 'Date Created',
          sortable: 'asc',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
        {
          field: 'modified',
          title: 'Date Modified',
          sortable: 'desc',
          type: 'date',
          format: 'YYYY-MM-DD',
        },
        // {
        //   field: 'published',
        //   title: 'Date Published',
        //   // type: 'date',
        //   // format: 'YYYY-MM-DD',
        //   template: function(row){
        //     if(row.created == row.published)
        //       return 'Immediately';
        //     else
        //       return row.published;
        //   }
        // },
        {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          // callback function support for column rendering
          template: function(row) {
            var status = {
              '-1': {'title': 'Scheduled', 'class': 'm-badge--warning'},
              '0': {'title': 'Draft', 'class': 'm-badge--metal'},
              '1': {'title': 'Published', 'class': ' m-badge--success'}
            };
            return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
          },
        }, {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function (row, index, datatable) {
            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            return '\
            <a href="'+siteURL+'expert/edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }
      ],
    });
  }
}

jQuery(document).ready(function() {

  var expert = new TeamList('.m_datatable');

  datatable = expert.init();

  var query = datatable.getDataSourceQuery();


  $('#m_form_status').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'status');
  });

  $('#m_form_type').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'Type');
  });

  $('#m_form_status, #m_form_type').selectpicker();
  $('.m_datatable').on('click','.btn-delete',function(e){
    e.preventDefault();
    var expertID = $(this).data('id');

    swal({
      title: 'Delete Partner',
      html: 'Are you sure to delete this expert ?',
      type: 'warning',
      showConfirmButton: true,
      showCancelButton: true
    }).then((result) => {
      if(result.value){
        deletePartner(expertID);
      }
    })
  })
});

function deletePartner(id){
  $.ajax({
    url: siteURL + 'expert/delete/' + id,
    type: 'post',
    dataType: 'json'
  })
  .done(function(res) {
    if(res.status == 1){
      swal('Success','Partner deleted','success');
      datatable.reload();
    } else {
      swal('Error','Partner not deleted','error');
    }
  })
  .fail(function(thrownError) {
    swal("Error", thrownError.textResponse,'error');
  });

}