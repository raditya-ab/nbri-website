var datatable;

function EntryList(instance) {
    this.instance = $(instance);
    this.init = function () {
        return this.instance.mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: siteURL + 'icb_rev_2022/get_registration_list',
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 1000,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false,
                },
            },
            // layout definition
            layout: {
                scroll: true,
                footer: false,
            },

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#search'),
                onEnter: true,
            },

            // columns definition
            columns: [
                {
                    field: 'no',
                    title: '#',
                    sortable: false, // disable sort for this column
                    width: 40,
                    selector: false,
                    textAlign: 'center',
                },
                {
                    field: 'order_id',
                    title: 'Order #',
                    // sortable: 'asc', // default sort
                    filterable: true, // disable or enable filtering
                    // basic templating support for column rendering,
                    // template: '{{OrderID}} - {{ShipCountry}}',
                },
                {
                    field: 'full_name',
                    title: 'Name',
                    // sortable: 'asc', // default sort
                    filterable: true, // disable or enable filtering
                    // basic templating support for column rendering,
                    // template: '{{OrderID}} - {{ShipCountry}}',
                },
                {
                    field: 'email',
                    title: 'Email',
                    // sortable: 'asc', // default sort
                    filterable: true, // disable or enable filtering
                    // basic templating support for column rendering,
                    template: function (row) {
                        return (
                            '<span data-toggle="tooltip" data-placement="top" title="' +
                            row.email +
                            '">' +
                            row.email +
                            '</span>'
                        );
                    },
                },
                {
                    field: 'nationality',
                    title: 'Nationality',
                    // sortable: 'asc', // default sort
                    filterable: true, // disable or enable filtering
                    // basic templating support for column rendering,
                    // template: '{{OrderID}} - {{ShipCountry}}',
                },
                {
                    field: 'occupation',
                    title: 'Occupation',
                    // sortable: 'asc', // default sort
                    filterable: true, // disable or enable filtering
                    // basic templating support for column rendering,
                    // template: '{{OrderID}} - {{ShipCountry}}',
                },
                {
                    field: 'package_type',
                    title: 'Package',
                    // sortable: 'asc', // default sort
                    filterable: true, // disable or enable filtering
                    // basic templating support for column rendering,
                    template: function (row) {
                        return (
                            '<span data-toggle="tooltip" data-placement="top" title="' +
                            row.package_type +
                            '">' +
                            row.package_type +
                            '</span>'
                        );
                    },
                },
                {
                    field: 'currency',
                    title: 'Currency',
                    // sortable: 'asc', // default sort
                    filterable: true, // disable or enable filtering
                    // basic templating support for column rendering,
                    // template: '{{OrderID}} - {{ShipCountry}}',
                },
                {
                    field: 'total_billed_in_idr',
                    title: 'Amount',
                    textAlign: 'right',
                    // sortable: 'asc', // default sort
                    filterable: true, // disable or enable filtering
                    // basic templating support for column rendering,
                    // template: '{{OrderID}} - {{ShipCountry}}',
                },
                {
                    field: 'created_at',
                    title: 'Date Created',
                    type: 'date',
                    format: 'YYYY-MM-DD',
                    sortable: 'desc',
                },
                {
                    field: 'updated_at',
                    title: 'Date Modified',
                    type: 'date',
                    format: 'YYYY-MM-DD',
                    sortable: 'desc',
                },
                {
                    field: 'transaction_status',
                    title: 'Status',
                    textAlign: 'center',
                    // callback function support for column rendering
                    template: function (row) {
                        if (row.transaction_status == 'pending') {
                            return (
                                '<button data-order_id="' +
                                row.order_id +
                                '" class="btn btn-update-status btn-danger m-badge m-badge--warning m-badge--wide">' +
                                row.transaction_status +
                                '</button>'
                            );
                        } else if (row.transaction_status == 'capture' || row.transaction_status == 'settlement') {
                            return (
                                '<button data-order_id="' +
                                row.order_id +
                                '" class="btn btn-success m-badge m-badge--success m-badge--wide">' +
                                row.transaction_status +
                                '</button>'
                            );
                        } else {
                            return (
                                '<button data-order_id="' +
                                row.order_id +
                                '" class="btn btn-update-status btn-danger m-badge m-badge--danger m-badge--wide">' +
                                row.transaction_status +
                                '</button>'
                            );
                        }
                    },
                },
                //       {
                //           field: 'Actions',
                //           width: 110,
                //           title: 'Actions',
                //           sortable: false,
                //           overflow: 'visible',
                //           template: function (row, index, datatable) {
                //               var dropup = datatable.getPageSize() - index <= 4 ? 'dropup' : '';
                //               return (
                //                   '\
                //   <div class="dropdown ' +
                //                   dropup +
                //                   '">\
                //     <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                //                       <i class="la la-ellipsis-h"></i>\
                //                   </a>\
                //       <div class="dropdown-menu dropdown-menu-right">\
                //         <a class="dropdown-item" href="' +
                //                   siteURL +
                //                   'onemap/edit/' +
                //                   row.id +
                //                   '"><i class="la  la-external-link"></i> View Entry</a>\
                //         <a class="dropdown-item" href="#"><i class="la la-star-o"></i> Set Featured</a>\
                //       </div>\
                //   </div>\
                //   <a href="' +
                //                   siteURL +
                //                   'onemap/edit/' +
                //                   row.id +
                //                   '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                //     <i class="la la-edit"></i>\
                //   </a>\
                //   <a href="javascript:void(0)" data-id="' +
                //                   row.id +
                //                   '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">\
                //     <i class="la la-trash"></i>\
                //   </a>\
                // '
                //               );
                //           },
                //       },
            ],
        });
    };
}

jQuery(document).ready(function () {
    var entry = new EntryList('.m_datatable');

    datatable = entry.init();

    var query = datatable.getDataSourceQuery();

    $('#m_form_status, #m_form_category, #m_form_type').selectpicker();

    $('#m_form_status').on('change', function () {
        datatable.search($(this).val().toLowerCase(), 'status');
    });

    $('#m_form_category').on('change', function () {
        datatable.search($(this).val().toLowerCase(), 'category');
    });

    $('.m_datatable').on('click', '.btn-delete', function (e) {
        e.preventDefault();
        var entryID = $(this).data('id');

        swal({
            title: 'Delete Entry',
            html: 'Are you sure to delete this entry ?',
            type: 'warning',
            showConfirmButton: true,
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                deleteEntry(entryID);
            }
        });
    });

    $('.m_datatable').on('click', '.btn-update-status', function (e) {
        e.preventDefault();
        var orderID = $(this).data('order_id');
        $(this).text('loading..');

        orderConfirmation(orderID);
    });

    function orderConfirmation(id) {
        $.ajax({
            url: siteURL + 'icb_rev_2022/get_payment_status/' + id,
            type: 'post',
            dataType: 'json',
        })
            .done(function (res) {
                if (res.status == 1) {
                    swal('Success', 'Entry updated', 'success');
                    datatable.reload();
                } else {
                    swal('Error', 'Entry not updated', 'error');
                    datatable.reload();
                }
            })
            .fail(function (thrownError) {
                swal('Error', thrownError.textResponse, 'error');
            });
    }
});
