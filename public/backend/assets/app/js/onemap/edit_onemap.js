$(document).ready(function() {


	$("#title").on('blur', function(e){
		var title = $(this).val();
        var id = $("#id").val();
		setTimeout(function(){
			if( title.trim().length > 0 ){
				$("#slug_link").html('<i class="la la-spinner la-spin"></i>');
				$.ajax({
					url: siteURL+'common/generate_slug/companies',
					type: 'POST',
					dataType: 'json',
					data: {
                        param: title,
                        id: id
                    },
				})
				.done(function(resp) {
					$("#slug").val(resp.data);
				})
				.fail(function() {
					console.log("error");
				});
			} else {
				$("#slug").val('');
			}
		}, 500)
	});

	$("#category").on('change', function(e)
    {
        var categoryID = $(this).val();

        if(categoryID != '' && categoryID != null)
        {
            $.ajax({
                url: siteURL+'onemap/category/get_subcategory',
                type: 'post',
                dataType: 'json',
                data: {
                    category: categoryID
                },
            })
            .done(function(res) {
                $('#subcategory_opt')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="">Select Sub-category</option>')
                    .val('');
                if(res.length > 0){
                    $("#subcategory_opt").prop('disabled',false);
                    $.each(res,function(key,row){
                        $("#subcategory_opt").append(new Option(row.name,row.id));
                    })
                } else {
                    $("#subcategory_opt").prop('disabled','disabled');
                }
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        }
    });

	$("#btn-draft").click(function(e){
		$("input[name=status]").val(0);
		// $(".form_update").submit();
	});

	$("#btn-publish").click(function(e){
		$("input[name=status]").val(1);
		// $(".form_update").submit();
	});

	$(".form_update").ajaxForm({
		url: $(".form_update").attr('action'),
		type: 'post',
		dataType: 'json',
		beforeSerialize: function(){
			if($("input[name=own_meta]").val() == '0'){
				tinyMCE.triggerSave();
				$("#metatitle").val($("#title").val());
				$("#metadescription").val($("#content").val());
			}
		},
		success: function(res){
			$(".form_update .btn-submit").removeAttr("disabled");
			$(".form_update .btn-submit i").remove();
			if(res.status == 1){
				swal({
				   title: "Success",
				   html: 'Entry <b>#' + res.message.onemap_id + '</b>, Entry Slug <b>' + res.message.onemap_slug + '</b> has been updated.',
				   type: "success",
				   showCloseButton: true,
				   showConfirmButton: true,
				   showCancelButton: true,
				   cancelButtonText: "Stay in this page",
				   confirmButtonText: "Go to Entry List"
				})
				.then((result) => {
					if(result.value){
						window.location.href = siteURL + "onemap";
					}
				})
			} else {
				swal("Error",res.message.error,"error");
				$(".form_update .btn-submit").removeAttr("disabled");
	            $(".form_update .btn-submit i").remove();
			}
		}, error: function(errorThrown){
			swal("Error",errorThrown.textResponse,"error");
		}
	});

	// var forms = new Dropzone(".form_update",{
	// 	url: $(".form_update").attr('action'),
	//     paramName: 'inputfile',
	//     // chunking: true,
	//     acceptedFiles: 'image/*',
	//     maxFiles: 1,
	//     parallelUploads: 10,
	//     autoProcessQueue: false, // Make sure the files aren't queued until manually added,
	//     previewsContainer: '#dropzone-preview',
	//     clickable: "#dropzone-preview", // Define the element that should be used as click trigger to select files.
	//     addRemoveLinks: true,
	//     maxFileSize: 50,
	//     createImageThumbnails: true,
	//     resizeHeight: 600,
	//     addRemoveLinks: true,
	//     dictRemoveFile: "",
	//     dictMaxFilesExceeded: "Maximum Number of Uploaded File Has Exceeded",
	//     dictDefaultMessage: "",
	//     init: function() {
	//         var myForm = this;

	//         var submitButton = $(".btn-submit");
	//         var btnID;
	//         var onemapID = $('.form_update').find('#id').val();

	//         submitButton.on("click", function(e) {
	//             e.preventDefault();
	//             e.stopPropagation();

	//             btnID = $(this).attr('id');

	//             $(this).prop("disabled", "disabled");
	//             $(this).append('<i class="fa fa-spinner fa-spin"></i>');

	//             if (myForm.getQueuedFiles().length > 0) {
	//                 myForm.processQueue();
	//             } else {
	//                 $(".form_update").submit();
	//             }
	//         });

	//         this.on("addedfile", function(file) {
	//             $(".dz-message").hide();
	//             var removeButton = Dropzone.createElement("<div style='width: 100%; text-align: center'><button class='btn btn-sm btn-danger' style='margin-top: 5px'><i class='fa fa-trash'></i></button></div>");

	//             removeButton.addEventListener("click", function(e) {
	//                 e.preventDefault();
	//                 e.stopPropagation();
	//                 myForm.removeFile(file);
	//             });

	//             file.previewElement.appendChild(removeButton);
	//         });

	//         this.on("maxfilesexceeded", function (file) {
	//             myForm.removeFile(file);
	//         })

	//         this.on("removedfile", function() {
	//             if(myForm.getQueuedFiles().length == 0){
	//             	$(".dz-message").show();
	//             }
	//         })

	//         this.on("sendingmultiple", function(file, xhr, formData) {
	//             $(".help-block").hide();
	//             var formDataArray = $(".form_update").serializeArray();
	//             for(var i = 0; i < formDataArray.length; i++){
	//                 var formDataItem = formDataArray[i];
	//                 formData.append(formDataItem.name, formDataItem.value);
	//             }
	//         });

	//         this.on("completemultiple", function() {
	//             $(".form_update .btn-submit").removeAttr("disabled");
	//             $(".form_update .btn-submit i").remove();
	//         });

	//         this.on("error", function(files, errorMessage, xhr) {
	//             $(".form_update").find("#btn-publish").html("Submit").prop('disabled',false);
	//             files.status = Dropzone.QUEUED;
	//             swal({
	//                 title: "Error",
	//                 text: errorMessage,
	//                 type: "error"
	//             })
	//         });

	//         $.getJSON(siteURL + 'onemap/get_onemap_image/' + onemapID, function(json, textStatus) {
	//         	if(json.length > 0){
	// 	        	// Create the mock file:
	// 	        	var mockFile = {
	// 	        		name: json.filename,
	// 	        		size: json.filesize
	// 	        	};
	// 	        	// Call the default addedfile event handler
	// 	        	myForm.emit("addedfile", mockFile);
	// 	        	// And optionally show the thumbnail of the file:
	// 	        	myForm.emit("thumbnail", mockFile, "http://theglowgeeks.local/backend/assets/demo/demo4/media/img/logo/logo.png");
	// 	        	// Make sure that there is no progress bar, etc...
	// 	        	myForm.emit("complete", mockFile);
	//         	}
	//         });
	//     }
	// });

});