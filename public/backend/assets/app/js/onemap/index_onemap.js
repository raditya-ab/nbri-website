var datatable;

function EntryList(instance) {
  this.instance = $(instance);
  this.init = function(){
    return this.instance.mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'POST',
            url: siteURL + 'onemap/get_onemap_list',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 1000,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
        saveState: {
          cookie: false,
          webstorage: false
        }
      },
      // layout definition
      layout: {
        scroll: true,
        footer: false
      },

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },

      search: {
        input: $('#search'),
        onEnter: true
      },

      // columns definition
      columns: [
        {
          field: 'no',
          title: '#',
          sortable: false, // disable sort for this column
          width: 40,
          selector: false,
          textAlign: 'center',
        }, {
          field: 'name',
          title: 'Name',
          // sortable: 'asc', // default sort
          filterable: true, // disable or enable filtering
          // basic templating support for column rendering,
          // template: '{{OrderID}} - {{ShipCountry}}',
        }, {
          field: 'category_name',
          title: 'Category',
          textAlign: 'center',
          template: function(row){
              return (row.category_name != '' && row.category_name != null) ? row.category_name : '-';
          }
          // width: 150,
          // textAlign: 'center'
          // template: function(row) {
          //   // callback function support for column rendering
          //   return row.ShipCountry + ' - ' + row.ShipCity;
          // },
        }, {
          field: 'subcategory_name',
          title: 'Sub-category',
          textAlign: 'center',
          template: function(row){
              return (row.subcategory_name != '' && row.subcategory_name != null) ? row.subcategory_name : '-';
          }
          // width: 150,
          // textAlign: 'center'
          // template: function(row) {
          //   // callback function support for column rendering
          //   return row.ShipCountry + ' - ' + row.ShipCity;
          // },
        }, {
          field: 'img_thumb',
          title: 'Image',
          width: 150,
          textAlign: 'center',
          template: function(row){
            if(row.img_thumb != '' && row.img_thumb != null){
                return '<img width="150" src="'+ frontURL +'assets/images/companies/'+ row.img_thumb +'"/>'
            } else {
                return '-';
            }
          }
          // width: 150,
          // textAlign: 'center'
          // template: function(row) {
          //   // callback function support for column rendering
          //   return row.ShipCountry + ' - ' + row.ShipCity;
          // },
        },{
          field: 'created',
          title: 'Date Created',
          type: 'date',
          format: 'YYYY-MM-DD',
          sortable: 'desc'
        }, {
          field: 'modified',
          title: 'Date Modified',
          type: 'date',
          format: 'YYYY-MM-DD',
          sortable: 'desc'
        }, {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          // callback function support for column rendering
          template: function(row) {
            var status = {
              '-1': {'title': 'Deleted', 'class': 'm-badge--warning'},
              '0': {'title': 'Draft', 'class': 'm-badge--metal'},
              '1': {'title': 'Published', 'class': ' m-badge--success'}
            };
            return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
          },
        }, {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function (row, index, datatable) {
            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            return '\
            <div class="dropdown ' + dropup + '">\
              <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                                <i class="la la-ellipsis-h"></i>\
                            </a>\
                <div class="dropdown-menu dropdown-menu-right">\
                  <a class="dropdown-item" href="'+siteURL+'onemap/edit/'+row.id+'"><i class="la  la-external-link"></i> View Entry</a>\
                  <a class="dropdown-item" href="#"><i class="la la-star-o"></i> Set Featured</a>\
                </div>\
            </div>\
            <a href="'+siteURL+'onemap/edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }
      ],
    });
  }
}

jQuery(document).ready(function() {

  var entry = new EntryList('.m_datatable');

  datatable = entry.init();

  var query = datatable.getDataSourceQuery();

  $('#m_form_status, #m_form_category, #m_form_type').selectpicker();

  $('#m_form_status').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'status');
  });

  $('#m_form_category').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'category');
  });

  $('.m_datatable').on('click','.btn-delete',function(e){
    e.preventDefault();
    var entryID = $(this).data('id');

    swal({
      title: 'Delete Entry',
      html: 'Are you sure to delete this entry ?',
      type: 'warning',
      showConfirmButton: true,
      showCancelButton: true
    }).then((result) => {
      if(result.value){
        deleteEntry(entryID);
      }
    })
  })
});

function deleteEntry(id){
  $.ajax({
    url: siteURL + 'onemap/delete/' + id,
    type: 'post',
    dataType: 'json'
  })
  .done(function(res) {
    if(res.status == 1){
      swal('Success','Entry deleted','success');
      datatable.reload();
    } else {
      swal('Error','Entry not deleted','error');
    }
  })
  .fail(function(thrownError) {
    swal("Error", thrownError.textResponse,'error');
  });

}