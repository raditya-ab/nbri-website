$(document).ready(function() {

	$("#title").on('blur', function(e){
		var title = $(this).val();

		if( $(this).val().trim().length > 0 ){
			setTimeout(function(){
				$("#slug_link").html('<i class="la la-spinner la-spin"></i>');
				$.ajax({
					url: siteURL+'common/generate_slug/companies',
					type: 'POST',
					dataType: 'json',
					data: {param: title},
				})
				.done(function(resp) {
					$("#slug").val(resp.data);
				})
				.fail(function() {
					console.log("error");
				});

			}, 500)
		} else {
			$("#slug").val('');
		}
	});

    $("#category").on('change', function(e)
    {
        var categoryID = $(this).val();

        if(categoryID != '' && categoryID != null)
        {
            $.ajax({
                url: siteURL+'onemap/category/get_subcategory',
                type: 'post',
                dataType: 'json',
                data: {
                    category: categoryID
                },
            })
            .done(function(res) {
                $('#subcategory_opt')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="">Select Sub-category</option>')
                    .val('');
                if(res.length > 0){
                    $("#subcategory_opt").prop('disabled',false);
                    $.each(res,function(key,row){
                        $("#subcategory_opt").append(new Option(row.name,row.id));
                    })
                } else {
                    $("#subcategory_opt").prop('disabled','disabled');
                }
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        }
    });

    $('subcategory_opt').on('change', function(){
        var subcategory = $(this).val()
        $("#subcategory").val(subcategory);
    })

	$("#btn-draft").click(function(e){
		$("input[name=status]").val(0);
		// $(".form_create").submit();
	});

	$("#btn-publish").click(function(e){
		$("input[name=status]").val(1);
		// $(".form_create").submit();
	});

	$(".form_create").ajaxForm({
		url: $(this).attr('action'),
		type: 'post',
		dataType: 'json',
		// clearForm: true,
		success: function(res){
			if(res.status == 1){
				swal({
					title: "Saved",
					html: 'Entry <b>#' + res.message.onemap_id + '</b>, Entry Slug <b>' + res.message.onemap_slug + '</b>.',
					type: "success",
					showCloseButton: true,
					showConfirmButton: true,
					showCancelButton: true,
					cancelButtonText: "Back to Entry List",
					confirmButtonText: "Add New Data"
				}).then((result)=>{
					if(result.value){
						window.location.href = window.location.href;
					} else {
						window.location.href = siteURL + "onemap";
					}
				});
			} else {
				swal({
					title: "Error",
					html: res.message.error,
					type: "error",
					showCancelButton: false,
					confirmButtonText: "Recheck form",
					// confirmButtonText: "Add New Data"
				});
			}
		}, error: function(errorThrown){
			$(".form_create .btn-submit").removeAttr("disabled");
			$(".form_create .btn-submit i").remove();

			swal({
				title: "Fatal Error",
				html: errorThrown.textResponse,
				type: "error",
				showCancelButton: true,
				cancelButtonText: "Back to Entry List",
				confirmButtonText: "Add New Data"
			}).then((result)=>{
				if(result.value){
					window.location.href = window.location.href;
				} else {
					window.location.href = siteURL + "onemap";
				}
			});
		}
	});

	// var forms = new Dropzone(".form_create",{
	// 	url: $(".form_create").attr('action'),
	//     paramName: 'inputfile',
	//     // chunking: true,
	//     acceptedFiles: 'image/*',
	//     maxFiles: 1,
	//     parallelUploads: 10,
	//     autoProcessQueue: false, // Make sure the files aren't queued until manually added,
	//     previewsContainer: '#dropzone-preview',
	//     clickable: "#dropzone-preview", // Define the element that should be used as click trigger to select files.
	//     addRemoveLinks: true,
	//     maxFileSize: 50,
	//     createImageThumbnails: true,
	//     resizeHeight: 600,
	//     addRemoveLinks: true,
	//     dictRemoveFile: "",
	//     dictMaxFilesExceeded: "Maximum Number of Uploaded File Has Exceeded",
	//     dictDefaultMessage: "",
	//     init: function() {
	//         var myForm = this;
	//         var imageCounter = 0;

	//         var submitButton = $(".btn-submit");
	//         var btnID;

	//         submitButton.on("click", function(e) {
	//             e.preventDefault();
	//             e.stopPropagation();

	//             btnID = $(this).attr('id');

	//             $(this).prop("disabled", "disabled");
	//             $(this).append('<i class="fa fa-spinner fa-spin"></i>');
	//             // console.log("image : " + imageCounter);

	//             if (myForm.getQueuedFiles().length > 0) {
	//                 myForm.processQueue();
	//             } else {
	//                 $(".form_create").submit();
	//             }
	//         });

	//         this.on("addedfile", function(file) {
	//             $(".dz-message").hide();
	//             var removeButton = Dropzone.createElement("<div style='width: 100%; text-align: center'><button class='btn btn-sm btn-danger' style='margin-top: 5px'><i class='fa fa-trash'></i></button></div>");

	//             removeButton.addEventListener("click", function(e) {
	//                 e.preventDefault();
	//                 e.stopPropagation();
	//                 myForm.removeFile(file);
	//             });

	//             file.previewElement.appendChild(removeButton);
	//         });

	//         this.on("maxfilesexceeded", function (file) {
	//             myForm.removeFile(file);
	//         })

	//         this.on("removedfile", function() {
	//             if(myForm.getQueuedFiles().length == 0){
	//             	$(".dz-message").show();
	//             }
	//         })

	//         this.on("sending", function(file, xhr, formData) {

	//             $(".help-block").hide();
	//             console.log("SINI");
	//             console.log(xhr);
	//             console.log(formData);
	//             if($("input[name=own_meta]").val() == '0'){
	//             	tinyMCE.triggerSave();
	//             	$("#metatitle").val($("#title").val());
	//             	$("#metadescription").val($("#content").val());
	//             }
	//             var formDataArray = $("#form_create").serializeArray();
	//             for(var i = 0; i < formDataArray.length; i++){
	//                 var formDataItem = formDataArray[i];
	//                 formData.append(formDataItem.name, formDataItem.value);
	//             }
	//         });

	//         this.on("success", function(files, response) {
	//             if (forms.getUploadingFiles().length === 0 && forms.getQueuedFiles().length === 0) {
	//                if(res.status == 1){
	//                	swal({
	//                		title: "Saved",
	//                		html: 'Article <b>#' + res.message.onemap_id + '</b>, Article Slug <b>' + res.message.onemap_slug + '</b>.',
	//                		type: "success",
	//                		showCloseButton: true,
	//                		showConfirmButton: true,
	//                		showCancelButton: true,
	//                		cancelButtonText: "Back to Article List",
	//                		confirmButtonText: "Add New Data"
	//                	}).then((result)=>{
	//                		if(result.value){
	//                			window.location.href = window.location.href;
	//                		} else {
	//                			window.location.href = siteURL + "onemap";
	//                		}
	//                	});
	//                } else {
	//                	swal({
	//                		title: "Error",
	//                		html: res.message.error,
	//                		type: "error",
	//                		showCancelButton: true,
	//                		cancelButtonText: "Recheck form",
	//                		confirmButtonText: "Add New Data"
	//                	}).then((result)=>{
	//                		if(result.value){
	//                			window.location.href = window.location.href;
	//                		}
	//                	});
	//                }
	//             }
	//         });

	//         this.on("complete", function() {
	//             $(".form_create #btn-publish").removeAttr("disabled");
	//             $(".form_create #btn-publish").html('Save');
	//         });

	//         this.on("error", function(files, errorMessage, xhr) {
	//         	console.log(files);
	//         	myForm.removeFile(files);
	//             $(".form_create").find("#btn-publish").html("Submit").prop('disabled',false);
	//             files.status = Dropzone.QUEUED;
	//             swal({
	//                 title: "Error",
	//                 text: errorMessage,
	//                 type: "error"
	//             })
	//         });
	//     }
	// });

});