$(document).ready(function () {
  $("#tag_name").on("keyup", function (e) {
    var title = $(this).val();

    if ($(this).val().trim().length > 0) {
      setTimeout(function () {
        $("#slug_link").html('<i class="la la-spinner la-spin"></i>');
        $.ajax({
          url: siteURL + "common/generate_slug/tags",
          type: "POST",
          dataType: "json",
          data: { param: title },
        })
          .done(function (resp) {
            $("#slug_link").text(resp.data);
            $("#slug").val(resp.data);
          })
          .fail(function (errorThrown) {
            swal("Error", "Error when generating slug, check console", "error");
            console.log(errorThrown);
          });
      }, 500);
    } else {
      $("#slug_link").text("<slug>");
      $("#slug").val("");
    }
  });

  $(".form_update").ajaxForm({
    url: $(".form_update").attr("action"),
    type: "post",
    dataType: "json",
    success: function (res) {
      $(".form_update .btn-submit").removeAttr("disabled");
      $(".form_update .btn-submit i").remove();
      if (res.status == 1) {
        swal({
          title: "Success",
          html:
            "Tag <b>#" +
            res.message.tag_id +
            "</b>, Tag Name <b>" +
            res.message.tag_name +
            "</b> has been updated",
          type: "success",
          showCloseButton: true,
          showConfirmButton: true,
          showCancelButton: true,
          cancelButtonText: "Stay in this page",
          confirmButtonText: "Go to Tag List",
        }).then((result) => {
          if (result.value) {
            window.location.href = siteURL + "press/tags";
          } else {
            window.location.href = window.location.href;
          }
        });
      } else {
        swal({
          title: "Error",
          html: res.message.error,
          type: "error",
          showCancelButton: true,
          cancelButtonText: "Go to Tag List",
          confirmButtonText: "Recheck form",
        }).then((result) => {
          if (result.value) {
          } else {
            window.location.href = siteURL + "press/tags";
          }
        });
      }
    },
    error: function (errorThrown) {
      swal({
        title: "Fatal Error",
        html: errorThrown.responseText,
        type: "error",
        showCancelButton: true,
        cancelButtonText: "Back to Press List",
        confirmButtonText: "Refresh Page",
      }).then((result) => {
        if (result.value) {
          window.location.href = window.location.href;
        } else {
          window.location.href = siteURL + "press";
        }
      });
    },
  });
});
