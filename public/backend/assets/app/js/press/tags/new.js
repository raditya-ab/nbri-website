$(document).ready(function () {
  $("#tag_name").on("keyup", function (e) {
    var title = $(this).val();

    if ($(this).val().trim().length > 0) {
      setTimeout(function () {
        $("#slug_link").html('<i class="la la-spinner la-spin"></i>');
        $.ajax({
          url: siteURL + "common/generate_slug/tags",
          type: "POST",
          dataType: "json",
          data: { param: title },
        })
          .done(function (resp) {
            $("#slug_link").text(resp.data);
            $("#slug").val(resp.data);
          })
          .fail(function (errorThrown) {
            swal("Error", "Error when generating slug, check console", "error");
            console.log(errorThrown);
          });
      }, 500);
    } else {
      $("#slug_link").text("<slug>");
      $("#slug").val("");
    }
  });

  $(".form_create").ajaxForm({
    url: $(".form_create").attr("action"),
    type: "post",
    dataType: "json",
    success: function (res) {
      $(".form_create .btn-submit").removeAttr("disabled");
      $(".form_create .btn-submit i").remove();

      if (res.status == 1) {
        swal({
          title: "Saved",
          html:
            "Tag <b>#" +
            res.message.tag_id +
            "</b>, Tag Name <b>" +
            res.message.tag_name +
            "</b>.",
          type: "success",
          showCloseButton: true,
          showConfirmButton: true,
          showCancelButton: true,
          cancelButtonText: "Back to Tag List",
          confirmButtonText: "Add New Data",
        }).then((result) => {
          if (result.value) {
            window.location.href = window.location.href;
          } else {
            window.location.href = siteURL + "press/tags";
          }
        });
      } else {
        swal({
          title: "Error",
          html: res.message.error,
          type: "error",
          showCancelButton: true,
          cancelButtonText: "Recheck form",
          confirmButtonText: "Add New Data",
        }).then((result) => {
          if (result.value) {
            window.location.href = window.location.href;
          }
        });
      }
    },
    error: function (errorThrown) {
      $(".form_create .btn-submit").removeAttr("disabled");
      $(".form_create .btn-submit i").remove();

      swal({
        title: "Fatal Error",
        html: errorThrown.textResponse,
        type: "error",
        showCancelButton: true,
        cancelButtonText: "Back to Tag List",
        confirmButtonText: "Add New Data",
      }).then((result) => {
        if (result.value) {
          window.location.href = window.location.href;
        } else {
          window.location.href = siteURL + "press/tags";
        }
      });
    },
  });
});
