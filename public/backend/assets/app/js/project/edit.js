	$(document).ready(function() {
	tinymce.init({
	    selector: '#content',
		mode: 'textareas',
		branding: true,
		relative_urls: false,
		remove_script_hosts: false,
		document_base_url: siteURL,
		custom_undo_redo_levels: 10,
		plugins: 'autolink autoresize autosave code codesample contextmenu hr help image imagetools link lists paste preview searchreplace media table',
		autoresize_bottom_margin: 50,
		autoresize_max_height: 500,
		autoresize_min_height: 350,
		autosave_retention: "20m",
		toolbar: 'newdocument | formatselect fontselect fontsizeselect removeformat | bold italic underline strikethrough | subscript superscript | alignleft aligncenter alignright alignjustify | outdent indent visualaid | blockquote | hr | bullist numlist | table tablecellprops tablemergedcellslink unlink | image media | preview code help |',
		menu: {
		    file: {title: 'File', items: 'newdocument'},
		    edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
		    insert: {title: 'Insert', items: 'link image | template hr'},
		    view: {title: 'View', items: 'visualaid'},
		    format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
		    table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
		    tools: {title: 'Tools', items: 'spellchecker code'}
		},
		entity_encoding : "named",
		contextmenu: "cut copy paste pastetext | link image inserttable | cell row column deletetable",
		image_caption: true,
		image_description: false,
		imagetools_cors_hosts: ['localhost','13.229.78.30','asiangames2018.id'],
		images_upload_url: "<?=site_url('editor_image_handler/upload_news_image_content'); ?>",
		video_template_callback: function(data) {
		   return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
		 },
		setup: function (editor) {
		    editor.on('change', function () {
		        editor.save();
		    });
		}
	  });

	$("#project_name").on('keyup', function(e){
		var title = $(this).val();
		setTimeout(function(){
			if( title.trim().length > 0 ){
				$("#slug_link").html('<i class="la la-spinner la-spin"></i>');
				$.ajax({
					url: siteURL+'common/generate_slug/project',
					type: 'POST',
					dataType: 'json',
					data: {param: title},
				})
				.done(function(resp) {
					$("#slug_link").text(resp.data);
					$("#slug").val(resp.data);
				})
				.fail(function() {
					console.log("error");
				});				
			} else {
				$("#slug_link").text('<slug>');
				$("#slug").val('');
			}
		}, 500)
	});

	$("#show_meta").click(function(e){
		e.preventDefault();
		$("#metatitle").val('');
		$("#metadescription").val('');

		if($(this).hasClass('opened')){
			$("input[name=own_meta]").val('0');
			$(this).removeClass('opened');
			$(this).text(' or you can edit manually.');
		} else {
			$("input[name=own_meta]").val('1');
			$(this).addClass('opened');
			$(this).text(' and I will use the generated tag instead.');
		}

		$("#meta").toggle('slow');
	});

	$("input[type=radio][name=category]").change(function(){
		var slug = $("input[type=radio][name=category]:checked").data('slug');
		var value = $("input[type=radio][name=category]:checked").val();

		$("#category_slug").text(slug);
	})

	$("#btn-draft").click(function(e){
		$("input[name=status]").val(0);
		// $(".form_update").submit();
	});

	$("#btn-publish").click(function(e){
		$("input[name=status]").val(1);
		// $(".form_update").submit();
	});

	$(".btn-delete-photo").on('click', function(e){
		e.preventDefault();

		var id = $(this).data('id');
		var _that = $(this);

		swal({
			title: "Delete Photo",
			text: "Are you sure to delete this photo?",
			type: "warning",
			showCancelButton: true,
			reverseButtons: true
		}).then((result)=>{
			if(result.value){
				$.ajax({
					url: siteURL + 'project/delete_photo/' + id,
					type: 'post',
					dataType: 'json',
					data: {
						id: id
					},
				})
				.done(function(res) {
					_that.closest('#photo_' + id).fadeOut('slow', function() {
						_that.closest('#photo_' + id).remove();
					});
				})
				.fail(function(errorThrown) {
					console.log(errorThrown);
					swal("Fatal Error", errorThrown.textResponse, "error");
				});
			}
		})

		
		
	})

	$(".form_update").ajaxForm({
		url: $(".form_update").attr('action'),
		type: 'post',
		dataType: 'json',
		beforeSerialize: function(){
			if($("input[name=own_meta]").val() == '0'){
				tinyMCE.triggerSave();
				$("#metatitle").val($("#project_name").val());
				$("#metadescription").val($("#content").val());
			}
		},
		success: function(res){
			$(".form_update .btn-submit").removeAttr("disabled");
			$(".form_update .btn-submit i").remove();
			if(res.status == 1){
				swal({
				   title: "Success",
				   html: 'Project <b>#' + res.message.project_id + '</b>, Project Slug <b>' + res.message.project_slug + '</b> has been updated.',
				   type: "success",
				   showCloseButton: true,
				   showConfirmButton: true,
				   showCancelButton: true,
				   cancelButtonText: "Stay in this page",
				   confirmButtonText: "Go to Project List"
				}).then((result)=>{
					if(result.value){
						window.location.href = siteURL + "project";
					} else {
						window.location.href = window.location.href;
					}
				});
			} else {
				swal({
					title: "Error",
					html: res.message.error,
					type: "error",
					showCancelButton: true,
					cancelButtonText: "Go to Project List",
					confirmButtonText: "Recheck form"
				}).then((result)=>{
					if(result.value){
					} else {
						window.location.href = window.location.href;
					}
				});
			}
		}, error: function(errorThrown){
			swal({
				title: "Fatal Error",
				html: errorThrown.textResponse,
				type: "error",
				showCancelButton: true,
				cancelButtonText: "Back to Project List",
				confirmButtonText: "Refresh Page"
			}).then((result)=>{
				if(result.value){
					window.location.href = window.location.href;
				} else {
					window.location.href = siteURL + "project";
				}
			});
		}
	});

	var forms = new Dropzone(".form_update",{
		url: $(".form_update").attr('action'),
	    paramName: 'inputfile',
	    // chunking: true,
	    acceptedFiles: 'image/*',
	    maxFiles: 5,
	    parallelUploads: 10,
	    autoProcessQueue: false, // Make sure the files aren't queued until manually added,
	    previewsContainer: '#dropzone-preview',
	    clickable: "#dropzone-preview", // Define the element that should be used as click trigger to select files.
	    addRemoveLinks: true,
	    maxFileSize: 50,
	    createImageThumbnails: true,
	    resizeHeight: 600,
	    addRemoveLinks: true,
	    uploadMultiple: true,
	    dictRemoveFile: "",
	    dictMaxFilesExceeded: "Maximum Number of Uploaded File Has Exceeded",
	    dictDefaultMessage: "",
	    init: function() {
	        var myForm = this;

	        var submitButton = $(".btn-submit");
	        var btnID;
	        var articleID = $('.form_update').find('#id').val();

	        submitButton.on("click", function(e) {
	            e.preventDefault();
	            e.stopPropagation();

	            btnID = $(this).attr('id');

	            $(this).prop("disabled", "disabled");
	            $(this).append('<i class="fa fa-spinner fa-spin"></i>');

	            if (myForm.getQueuedFiles().length > 0) {
	                myForm.processQueue();
	            } else {
	                $(".form_update").submit();
	            }
	        });

	        this.on("addedfile", function(file) {
	            $(".dz-message").hide();
	            var removeButton = Dropzone.createElement("<div style='width: 100%; text-align: center'><button class='btn btn-sm btn-danger' style='margin-top: 5px'><i class='fa fa-trash'></i></button></div>");

	            removeButton.addEventListener("click", function(e) {
	                e.preventDefault();
	                e.stopPropagation();
	                myForm.removeFile(file);
	            });

	            file.previewElement.appendChild(removeButton);
	        });

	        this.on("maxfilesexceeded", function (file) {
	            myForm.removeFile(file);
	        })

	        this.on("removedfile", function() {
	            if(myForm.getQueuedFiles().length == 0){
	            	$(".dz-message").show();
	            }
	        })

	        this.on("sendingmultiple", function(file, xhr, formData) {
	            $(".help-block").hide();
	            if($("input[name=own_meta]").val() == '0'){
	            	tinyMCE.triggerSave();
	            	$("#metatitle").val($("#project_name").val());
	            	$("#metadescription").val($("#content").val());
	            }
	            var formDataArray = $(".form_update").serializeArray();
	            for(var i = 0; i < formDataArray.length; i++){
	                var formDataItem = formDataArray[i];
	                formData.append(formDataItem.name, formDataItem.value);
	            }
	        });

	        this.on("successmultiple", function(files, response) {
	        	// console.log(response);
	            if (forms.getUploadingFiles().length === 0 && forms.getQueuedFiles().length === 0) {
	            if(response.status == 1){
	            	swal({
	            		title: "Saved",
	            		text: "Project ID: "+response.message.project_id+", Project slug: "+response.message.project_slug,
	            		type: "success",
	            		showCancelButton: true,
	            		cancelButtonText: "Back to Project List",
	            		confirmButtonText: "Edit Again"
	            	}).then((result)=>{
	            		if(result.value){
	            			window.location.href = window.location.href;
	            		} else {
	            			window.location.href = siteURL + "project";
	            		}
	            	});
	            } else {
	            	files.status = Dropzone.QUEUED;
	            	swal({
	            		title: "Error",
	            		html: response.message.error,
	            		type: "error",
	            		showCancelButton: true,
	            		cancelButtonText: "Recheck form",
	            		confirmButtonText: "Edit Again"
	            	}).then((result)=>{
	            		if(result.value){
	            			window.location.href = window.location.href;
	            		} else {
	            			$(".form_create #btn-publish").removeAttr("disabled");
	            			$(".form_create #btn-publish").html('Publish');
	            		}
	            	});
	            }
	            }
	        });

	        this.on("completemultiple", function() {
	            $(".form_update .btn-submit").removeAttr("disabled");
	            $(".form_update .btn-submit i").remove();
	        });

	        this.on("error", function(files, errorMessage, xhr) {
	        	myForm.removeFile(files);
	            $(".form_update").find("#btn-publish").html("Submit").prop('disabled',false);
	            files.status = Dropzone.QUEUED;
	            swal("Error", errorMessage, "error");
	        });

	        // $.getJSON(siteURL + 'article/get_article_image/' + articleID, function(json, textStatus) {
	        // 	if(json.length > 0){
		       //  	// Create the mock file:
		       //  	var mockFile = {
		       //  		name: json.filename,
		       //  		size: json.filesize
		       //  	};
		       //  	// Call the default addedfile event handler
		       //  	myForm.emit("addedfile", mockFile);
		       //  	// And optionally show the thumbnail of the file:
		       //  	myForm.emit("thumbnail", mockFile, "http://theglowgeeks.local/backend/assets/demo/demo4/media/img/logo/logo.png");
		       //  	// Make sure that there is no progress bar, etc...
		       //  	myForm.emit("complete", mockFile);
	        // 	}
	        // });
	    }
	});

});