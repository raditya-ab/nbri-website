var datatable;

function ProjectList(instance) {
  this.instance = $(instance);
  this.init = function(){
    return this.instance.mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'POST',
            url: siteURL + 'project/get_project_list',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 1000,
        // serverPaging: true,
        // serverFiltering: true,
        // serverSorting: true,
        saveState: {
          cookie: false,
          webstorage: false
        }
      },
      // layout definition
      layout: {
        scroll: false,
        footer: false
      },
      pagination: true,
      sortable: true,
      toolbar: {
        layout: ['pagination','info'],
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },
      search: {
        input: $('#generalSearch'),
        onEnter: true
      },

      // columns definition
      columns: [
        {
          field: 'no',
          title: '#',
          sortable: false, // disable sort for this column
          width: 40,
          selector: false,
          textAlign: 'center',
        }, {
          field: 'name',
          title: 'Name',
          sortable: 'asc', // default sort
          filterable: true, // disable or enable filtering
          width: 150,
          template: function(row){
            console.log(row);
            return '<a target="_blank" href="' + frontURL +'projects/' + row.type_slug + '/' + row.project_slug +'">' + row.name + '</a>'
          }
          // basic templating support for column rendering,
          // template: '{{OrderID}} - {{ShipCountry}}',
        }, {
          field: 'type_name',
          title: 'Type',
          sortable: 'asc', // default sort
          filterable: true, // disable or enable filtering
          width: 150,
          template: function(row){
            return '<a target="_blank" href="' + frontURL +'projects/' + row.type_slug + '/">' + row.type_name + '</a>'
          }
          // basic templating support for column rendering,
          // template: '{{OrderID}} - {{ShipCountry}}',
        }, {
          field: 'created',
          title: 'Date Created',
          type: 'date',
          format: 'YYYY-MM-DD',
          textAlign: 'center',
        }, {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          // callback function support for column rendering
          template: function(row) {
            var status = { 
              '0': {'title': 'Not Published', 'class': 'm-badge--metal'},
              '1': {'title': 'Published', 'class': ' m-badge--success'}
            };
            return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
          },
        }, {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          textAlign: 'center',
          template: function (row, index, datatable) {
            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            return '\
            <div class="dropdown ' + dropup + '">\
              <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                                <i class="la la-ellipsis-h"></i>\
                            </a>\
                <div class="dropdown-menu dropdown-menu-right">\
                  <a class="dropdown-item" href="' + frontURL +'projects/' + row.type_slug + '/' + row.project_slug + '"><i class="la  la-external-link"></i> View Project</a>\
                </div>\
            </div>\
            <a href="'+siteURL+'project/edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }
      ],
    });
  }
}

jQuery(document).ready(function() {

  var project = new ProjectList('.m_datatable');

  datatable = project.init();

  var query = datatable.getDataSourceQuery();


  $('#m_form_status').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'status');
  });

  $('#m_form_type').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'Type');
  });

  $('#m_form_status, #m_form_type').selectpicker();
  $('.m_datatable').on('click','.btn-delete',function(e){
    e.preventDefault();
    var projectID = $(this).data('id');

    swal({
      title: 'Delete Project',
      html: 'Are you sure to delete this project ?',
      type: 'warning',
      showConfirmButton: true,
      showCancelButton: true
    }).then((result) => {
      if(result.value){
        deleteProject(projectID);
      }
    })
  })
});

function deleteProject(id){
  $.ajax({
    url: siteURL + 'project/delete/' + id,
    type: 'post',
    dataType: 'json'
  })
  .done(function(res) {
    if(res.status == 1){
      swal('Success','Project deleted','success');
      datatable.reload();
    } else {
      swal('Error','Project not deleted','error');
    }
  })
  .fail(function(thrownError) {
    swal("Error", thrownError.textResponse,'error');
  });
  
}