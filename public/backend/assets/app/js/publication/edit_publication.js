$(document).ready(function() {
	tinymce.init({
		selector: '#content',
		theme: "modern",
        mode: 'textareas',
        branding: false,
        relative_urls: false,
        remove_script_hosts: false,
		document_base_url: siteURL,
		convert_urls: false,
		language: 'en_GB',
        custom_undo_redo_levels: 10,
        plugins: 'autolink autoresize autosave code codesample contextmenu hr help image imagetools link lists paste preview searchreplace media table responsivefilemanager',
        autoresize_bottom_margin: 50,
        autoresize_max_height: 500,
        autoresize_min_height: 350,
        autosave_retention: "20m",
        toolbar: 'newdocument | formatselect fontselect fontsizeselect removeformat | bold italic underline strikethrough | subscript superscript | alignleft aligncenter alignright alignjustify | outdent indent | blockquote | hr | bullist numlist | table tablecellprops tablemergedcells link unlink responsivefilemanager | image media | preview code help |',
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Insert', items: 'link image | template hr'},
            view: {title: 'View', items: 'visualaid'},
            format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'}
        },
		entity_encoding : "named",
		allow_html_in_named_anchor: true,
        contextmenu: "cut copy paste pastetext | link image inserttable | cell row column deletetable",
        image_caption: true,
        image_description: false,
        imagetools_cors_hosts: ['localhost','n-bri.org','nbri.raditya.site','n-bri.local'],
        images_upload_url: siteURL + "editor_image_handler/uploads_image_content/publication",
        images_upload_base_path: '../',
        video_template_callback: function(data) {
           return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
         },
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
		},
		external_filemanager_path:"/assets/filemanager/",
		filemanager_title:"Choose File" ,
		external_plugins: { "filemanager" : "/assets/filemanager/plugin.min.js"}
    });

	$("#title").on('keyup', function(e){
		var title = $(this).val();
		var id = $("#id").val();
		setTimeout(function(){
			if( title.trim().length > 0 ){
				$("#slug_link").html('<i class="la la-spinner la-spin"></i>');
				$.ajax({
					url: siteURL+'common/generate_slug/publication',
					type: 'POST',
					dataType: 'json',
					data: {
						param: title,
						id: id
					},
				})
				.done(function(resp) {
					$("#slug_link").text(resp.data);
					$("#slug").val(resp.data);
				})
				.fail(function() {
					console.log("error");
				});
			} else {
				$("#slug_link").text('<slug>');
				$("#slug").val('');
			}
		}, 500)
	});

	$("#btn-draft").click(function(e){
		$("input[name=status]").val(0);
		// $(".form_update").submit();
	});

	$("#btn-publish").click(function(e){
		$("input[name=status]").val(1);
		// $(".form_update").submit();
	});

	$(".form_update").ajaxForm({
		url: $(".form_update").attr('action'),
		type: 'post',
		dataType: 'json',
		beforeSerialize: function(){
			if($("input[name=own_meta]").val() == '0'){
				tinyMCE.triggerSave();
				$("#metatitle").val($("#title").val());
				$("#metadescription").val($("#content").val());
			}
		},
		success: function(res){
			$(".form_update .btn-submit").removeAttr("disabled");
			$(".form_update .btn-submit i").remove();
			if(res.status == 1){
				swal({
				   title: "Success",
				   html: 'Article <b>#' + res.message.publication_id + '</b>, Article Slug <b>' + res.message.publication_slug + '</b> has been updated.<br>' + res.message.upload_info,
				   type: "success",
				   showCloseButton: true,
				   showConfirmButton: true,
				   showCancelButton: true,
				   cancelButtonText: "Stay in this page",
				   confirmButtonText: "Go to Publication List"
				})
				.then((result) => {
					if(result.value){
						window.location.href = siteURL + "publication";
					}
				})
			} else {
				swal("Error",res.message.error,"error");
				$(".form_update .btn-submit").removeAttr("disabled");
	            $(".form_update .btn-submit i").remove();
			}
		}, error: function(errorThrown){
			swal("Error",errorThrown.textResponse,"error");
		}
	});

	$("#imgInp").change(function() {
		readURL(this);
	});
});
