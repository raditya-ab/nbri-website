var datatable;

function PublicationList(instance) {
  this.instance = $(instance);
  this.init = function(){
    return this.instance.mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'POST',
            url: siteURL + 'publication/get_publication_list',
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 1000,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
        saveState: {
          cookie: false,
          webstorage: false
        }
      },
      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },

      search: {
        input: $('#generalSearch'),
        onEnter: true
      },

      // columns definition
      columns: [
        {
          field: 'no',
          title: '#',
          sortable: false, // disable sort for this column
          width: 40,
          selector: false,
          textAlign: 'center',
        }, {
          field: 'name',
          title: 'Publication Name',
          sortable: 'asc', // default sort
          filterable: true, // disable or enable filtering
          width: 150,
          // basic templating support for column rendering,
          // template: '{{OrderID}} - {{ShipCountry}}',
        }, {
          field: 'filename',
          title: 'File',
          width: 150,
          textAlign: 'center',
          template: function(row) {
            // callback function support for column rendering
            return '<a href="'+frontURL+'assets/images/publication/'+row.filename+'"" target="_blank">View PDF</a>';
          },
        }, {
          field: 'created',
          title: 'Date Created',
          type: 'date',
          format: 'YYYY-MM-DD',
        }, {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          // callback function support for column rendering
          template: function(row) {
            var status = {
              '-1': {'title': 'Deleted', 'class': 'm-badge--warning'},
              '0': {'title': 'Draft', 'class': 'm-badge--metal'},
              '1': {'title': 'Published', 'class': ' m-badge--success'}
            };
            return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
          },
        }, {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function (row, index, datatable) {
            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            return '\
            <a href="'+siteURL+'publication/edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">\
              <i class="la la-trash"></i>\
            </a>\
          ';
          },
        }
      ],
    });
  }
}

jQuery(document).ready(function() {

  var publication = new PublicationList('.m_datatable');

  datatable = publication.init();

  var query = datatable.getDataSourceQuery();


  $('#m_form_status').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'status');
  });

  $('#m_form_type').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'Type');
  });

  $('#m_form_status, #m_form_type').selectpicker();
  $('.m_datatable').on('click','.btn-delete',function(e){
    e.preventDefault();
    var articleID = $(this).data('id');

    swal({
      title: 'Delete Article',
      html: 'Are you sure to delete this article ?',
      type: 'warning',
      showConfirmButton: true,
      showCancelButton: true
    }).then((result) => {
      if(result.value){
        deleteArticle(articleID);
      }
    })
  })
});

function deleteArticle(id){
  $.ajax({
    url: siteURL + 'publication/delete/' + id,
    type: 'post',
    dataType: 'json'
  })
  .done(function(res) {
    if(res.status == 1){
      swal('Success','Article deleted','success');
      datatable.reload();
    } else {
      swal('Error','Article not deleted','error');
    }
  })
  .fail(function(thrownError) {
    swal("Error", thrownError.textResponse,'error');
  });

}