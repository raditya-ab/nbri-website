var datatable;

function ArticleList(instance) {
    this.instance = $(instance);
    this.init = function() {
        return this.instance.mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: siteURL + 'research/get_research_list',
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 1000,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                scroll: false,
                footer: false
            },

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch'),
                onEnter: true
            },

            // columns definition
            columns: [{
                field: 'no',
                title: '#',
                sortable: false, // disable sort for this column
                width: 40,
                selector: false,
                textAlign: 'center',
            }, {
                field: 'category_name',
                title: 'Research Name',
                sortable: 'asc', // default sort
                filterable: true, // disable or enable filtering
                // width: 150,
                // template: function(row){
                //   if(row.featured == '1')
                //     return row.title + '&nbsp;<i class="fa fa-star m--font-warning"></i>';
                //   else
                //     return row.title;
                // }
                // basic templating support for column rendering,
                // template: '{{OrderID}} - {{ShipCountry}}',
            }, {
                sortable: false, // disable sort for this column
                field: 'thumb_img',
                title: 'Thumbnail',
                width: 100,
                textAlign: 'center',
                template: function(row) {
                    if (row.thumb_img != undefined && row.thumb_img != null && row.thumb_img != '') {
                        return '<img src="' + frontURL + 'assets/images/research/' + row.thumb_img + '" alt="photo" style="max-width: 100px">';
                    } else {
                        return '-';
                    }
                },
            }, {
                field: 'created',
                title: 'Date Created',
                type: 'date',
                textAlign: 'center',
                format: 'YYYY-MM-DD',
            }, {
                field: 'status',
                title: 'Status',
                textAlign: 'center',
                // callback function support for column rendering
                template: function(row) {
                    var status = {
                        '-1': { 'title': 'Deleted', 'class': 'm-badge--warning' },
                        '0': { 'title': 'Draft', 'class': 'm-badge--metal' },
                        '1': { 'title': 'Published', 'class': ' m-badge--success' }
                    };
                    return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
                },
            }, {
                field: 'Actions',
                width: 110,
                title: 'Actions',
                sortable: false,
                overflow: 'visible',
                template: function(row, index, datatable) {
                    var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                    return '\
            <div class="dropdown ' + dropup + '">\
              <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                                <i class="la la-ellipsis-h"></i>\
                            </a>\
                <div class="dropdown-menu dropdown-menu-right">\
                  <a class="dropdown-item" href="' + frontURL + 'research/' + row.slug + '" target="_blank"><i class="la  la-external-link"></i> View Page</a>\
                  <a class="dropdown-item" href="' + siteURL + 'subresearch/' + row.id + '"><i class="la  la-external-link"></i> View Sub-Research</a>\
                </div>\
            </div>\
            <a href="' + siteURL + 'research/edit/' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
              <i class="la la-edit"></i>\
            </a>\
            <a href="javascript:void(0)" data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">\
              <i class="la la-trash"></i>\
            </a>\
          ';
                },
            }],
        });
    }
}

jQuery(document).ready(function() {

    var article = new ArticleList('.m_datatable');

    datatable = article.init();

    var query = datatable.getDataSourceQuery();


    $('#m_form_status').on('change', function() {
        datatable.search($(this).val().toLowerCase(), 'status');
    });

    $('#m_form_type').on('change', function() {
        datatable.search($(this).val().toLowerCase(), 'Type');
    });

    $('#m_form_status, #m_form_type').selectpicker();
    $('.m_datatable').on('click', '.btn-delete', function(e) {
        e.preventDefault();
        var researchID = $(this).data('id');

        swal({
            title: 'Delete Research Article',
            html: 'Are you sure to delete this article and its subresearch ?',
            type: 'warning',
            showConfirmButton: true,
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                deleteResearch(researchID);
            }
        })
    })
});

function deleteResearch(id) {
    $.ajax({
            url: siteURL + 'research/delete/' + id,
            type: 'post',
            dataType: 'json'
        })
        .done(function(res) {
            if (res.status == 1) {
                swal('Success', 'Article deleted', 'success');
                datatable.reload();
            } else {
                swal('Error', 'Article not deleted', 'error');
            }
        })
        .fail(function(thrownError) {
            swal("Error", thrownError.textResponse, 'error');
        });

}