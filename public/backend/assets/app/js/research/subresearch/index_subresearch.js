var datatable;

function SubresearchList(instance) {
    this.instance = $(instance);
    this.init = function(param = null) {
        console.log(isNaN(param));
        var _id = '';
        if(param != null && !isNaN(param)){
            _id = param;
        }

        console.log(_id);
        return this.instance.mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'POST',
                        url: siteURL + 'subresearch/get_subresearch_list/'+_id,
                        map: function(raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                                console.log(dataSet);
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 1000,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState: {
                    cookie: false,
                    webstorage: false
                }
            },
            // layout definition
            layout: {
                scroll: false,
                footer: false
            },

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch'),
                onEnter: true
            },

            // columns definition
            columns: [{
                field: 'no',
                title: '#',
                sortable: false, // disable sort for this column
                width: 40,
                selector: false,
                textAlign: 'center',
            }, {
                field: 'category_name',
                title: 'Research Name',
                sortable: 'asc', // default sort
                filterable: true, // disable or enable filtering
                width: 150,
                template: function(row){
                    return "<a href='"+siteURL+"subresearch/"+row.id+"' target='_blank'>"+row.category_name+"</a>";
                }

                // basic templating support for column rendering,
                // template: '{{OrderID}} - {{ShipCountry}}',
            }, {
                field: 'research_name',
                title: 'Sub-Research Name',
                sortable: 'asc', // default sort
                filterable: true, // disable or enable filtering
                template: function(row){
                  return "<b>"+row.research_name+"</b>"
                }
                // width: 150,
                // template: function(row){
                //   if(row.featured == '1')
                //     return row.title + '&nbsp;<i class="fa fa-star m--font-warning"></i>';
                //   else
                //     return row.title;
                // }
                // basic templating support for column rendering,
                // template: '{{OrderID}} - {{ShipCountry}}',
            }, {
                sortable: false, // disable sort for this column
                field: 'img_thumb',
                title: 'Thumbnail',
                width: 100,
                textAlign: 'center',
                template: function(row) {
                    if (row.thumb_img != undefined && row.thumb_img != null && row.thumb_img != '') {
                        return '<img src="' + frontURL + 'assets/images/research/' + row.thumb_img + '" alt="photo" style="max-width: 100px">';
                    } else {
                        return '-';
                    }
                },
            }, {
                field: 'created',
                title: 'Date Created',
                type: 'date',
                textAlign: 'center',
                format: 'YYYY-MM-DD',
            }, {
                field: 'status',
                title: 'Status',
                textAlign: 'center',
                // callback function support for column rendering
                template: function(row) {
                    var status = {
                        '-1': { 'title': 'Deleted', 'class': 'm-badge--warning' },
                        '0': { 'title': 'Draft', 'class': 'm-badge--metal' },
                        '1': { 'title': 'Published', 'class': ' m-badge--success' }
                    };
                    return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
                },
            }, {
                field: 'Actions',
                width: 110,
                title: 'Actions',
                sortable: false,
                overflow: 'visible',
                template: function(row, index, datatable) {
                    var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                    return '\
                        <a href="' + frontURL + 'research/' + row.category_slug + "/" + row.slug + '" target="_blank" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View Page">\
                          <i class="la la-external-link"></i>\
                        </a>\
                        <a href="' + siteURL + 'subresearch/edit/' + row.research_id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\
                          <i class="la la-edit"></i>\
                        </a>\
                        <a href="javascript:void(0)" data-id="' + row.research_id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill btn-delete" title="Delete">\
                          <i class="la la-trash"></i>\
                        </a>\
                      ';
                },
            }],
        });
    }
}

jQuery(document).ready(function() {

    var subResearch = new SubresearchList('.m_datatable');
    var url = $(location).attr('href'),
        parts = url.split('/'),
        last_part = parts[parts.length-1];

    datatable = subResearch.init(last_part);

    var query = datatable.getDataSourceQuery();


    $('#m_form_status').on('change', function() {
        datatable.search($(this).val().toLowerCase(), 'status');
    });

    $('#m_form_type').on('change', function() {
        datatable.search($(this).val().toLowerCase(), 'Type');
    });

    $('#m_form_status, #m_form_type').selectpicker();
    $('.m_datatable').on('click', '.btn-delete', function(e) {
        e.preventDefault();
        var articleID = $(this).data('id');

        swal({
            title: 'Delete Article',
            html: 'Are you sure to delete this article ?',
            type: 'warning',
            showConfirmButton: true,
            showCancelButton: true
        }).then((result) => {
            if (result.value) {
                deleteArticle(articleID);
            }
        })
    })
});

function deleteArticle(id) {
    $.ajax({
            url: siteURL + 'subresearch/delete/' + id,
            type: 'post',
            dataType: 'json'
        })
        .done(function(res) {
            if (res.status == 1) {
                swal('Success', 'Article deleted', 'success');
                datatable.reload();
            } else {
                swal('Error', 'Article not deleted', 'error');
                datatable.reload();
            }
        })
        .fail(function(thrownError) {
            swal("Error", thrownError.textResponse, 'error');
        });

}