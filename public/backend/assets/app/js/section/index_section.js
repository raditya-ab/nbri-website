var datatable;

function PagesList(instance, pageID, pageSlug) {
  this.instance = $(instance);
  this.init = function(){
    return this.instance.mDatatable({
      // datasource definition
      data: {
        type: 'remote',
        source: {
          read: {
            // sample GET method
            method: 'POST',
            url: siteURL + 'sections/get_section_list/'+pageID+'/'+pageSlug,
            params: {
              page_id: pageID,
              page_slug: pageSlug
            },
            map: function(raw) {
              // sample data mapping
              var dataSet = raw;
              if (typeof raw.data !== 'undefined') {
                dataSet = raw.data;
              }
              return dataSet;
            },
          },
        },
        pageSize: 1000,
        serverPaging: true,
        serverFiltering: true,
        serverSorting: true,
        saveState: {
          cookie: false,
          webstorage: false
        }
      },
      // layout definition
      layout: {
        scroll: false,
        footer: false
      },

      pagination: true,

      toolbar: {
        // toolbar items
        items: {
          // pagination
          pagination: {
            // page size select
            pageSizeSelect: [10, 20, 30, 50, 100],
          },
        },
      },

      search: {
        input: $('#generalSearch'),
        onEnter: false
      },

      // columns definition
      columns: [
        {
          field: 'no',
          title: '#',
          sortable: false, // disable sort for this column
          width: 40,
          selector: false,
          textAlign: 'center',
        }, {
          field: 'page_name',
          title: 'Page',
          sortable: 'false', // default sort
          filterable: false, // disable or enable filtering
          width: 100,
          textAlign: 'center',
          // template: function(row){
          //   return "<img src='"+frontURL+"assets/images/member/"+ row.img +"' onerror=\"this.src='http://via.placeholder.com/100x100'\" width='100'>";
          // }
          // basic templating support for column rendering,
          // template: '{{OrderID}} - {{ShipCountry}}',
        },{
          field: 'section_name',
          title: 'Section Name',
          sortable: 'asc', // default sort
          filterable: true, // disable or enable filtering
          width: 150
          // basic templating support for column rendering,
          // template: '{{OrderID}} - {{ShipCountry}}',
        },
        // {
        //   field: 'author',
        //   title: 'Author',
        //   width: 150,
        //   textAlign: 'center'
        //   // template: function(row) {
        //   //   // callback function support for column rendering
        //   //   return row.ShipCountry + ' - ' + row.ShipCity;
        //   // },
        // },
        // {
        //   field: 'created',
        //   title: 'Date Created',
        //   type: 'date',
        //   format: 'YYYY-MM-DD',
        // },
        // {
        //   field: 'published',
        //   title: 'Date Published',
        //   // type: 'date',
        //   // format: 'YYYY-MM-DD',
        //   template: function(row){
        //     if(row.created == row.published)
        //       return 'Immediately';
        //     else
        //       return row.published;
        //   }
        // },
        {
          field: 'status',
          title: 'Status',
          textAlign: 'center',
          // callback function support for column rendering
          template: function(row) {
            var status = {
              '-1': {'title': 'Draft', 'class': 'm-badge--warning'},
              '0': {'title': 'Hidden', 'class': 'm-badge--metal'},
              '1': {'title': 'Published', 'class': ' m-badge--success'}
            };
            return '<span class="m-badge ' + status[row.status].class + ' m-badge--wide">' + status[row.status].title + '</span>';
          },
        }, {
          field: 'Actions',
          width: 110,
          title: 'Actions',
          sortable: false,
          overflow: 'visible',
          template: function (row, index, datatable) {
            var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
            if(row.status == 1){
                return '\
                    <a href="'+siteURL+'pages/'+row.page_slug+'/'+row.section_slug+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Section">\
                        <i class="la la-edit"></i>\
                    </a>\
                    <a href="javascript:void(0)" data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill btn-hide" title="Hide">\
                        <i class="la la-eye-slash"></i>\
                    </a>\
                '
            } else {
                return '\
                    <a href="'+siteURL+'pages/'+row.page_slug+'/'+row.section_slug+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit Section">\
                        <i class="la la-edit"></i>\
                    </a>\
                    <a href="javascript:void(0)" data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill btn-unhide" title="Publish">\
                        <i class="la la-eye"></i>\
                    </a>\
                '
            }
            ;
          },
        }
      ],
    });
  }
}

jQuery(document).ready(function() {

  var this_js_script = $("script[src*='index_section.js']"); // or better regexp to get the file name..

  var pageSlug = this_js_script.attr('data-page_slug');
  if (typeof pageSlug === "undefined" ) {
     var pageSlug = null;
  }

  var pageID = this_js_script.attr('data-page_id');
  if (typeof pageID === "undefined" ) {
     var pageID = null;
  }


  var pages = new PagesList('.m_datatable', pageID, pageSlug);

  datatable = pages.init();

  var query = datatable.getDataSourceQuery();


  $('#m_form_status').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'status');
  });

  $('#m_form_type').on('change', function() {
    datatable.search($(this).val().toLowerCase(), 'Type');
  });

  $('#m_form_status, #m_form_type').selectpicker();

  $('.m_datatable').on('click','.btn-hide',function(e){
    e.preventDefault();
    var sectionID = $(this).data('id');

    swal({
      title: 'Hide Page Section',
      html: 'Are you sure to Hide this section ?',
      type: 'warning',
      showConfirmButton: true,
      showCancelButton: true
    }).then((result) => {
      if(result.value){
        hideSection(sectionID);
      }
    })
  })

  $('.m_datatable').on('click','.btn-unhide',function(e){
    e.preventDefault();
    var sectionID = $(this).data('id');

    swal({
      title: 'Publish Page Section',
      html: 'Are you sure to Publish this section ?',
      type: 'warning',
      showConfirmButton: true,
      showCancelButton: true
    }).then((result) => {
      if(result.value){
        publishSection(sectionID);
      }
    })
  })
});

function hideSection(id){
  $.ajax({
    url: siteURL + 'sections/hide/' + id,
    type: 'post',
    dataType: 'json'
  })
  .done(function(res) {
    if(res.status == 1){
      swal('Success','Section Hidden','success');
      datatable.reload();
    } else {
      swal('Error','Error when hiding the section','error');
    }
  })
  .fail(function(thrownError) {
    swal("Fatal Error", thrownError.textResponse,'error');
  });

}

function publishSection(id){
  $.ajax({
    url: siteURL + 'sections/publish/' + id,
    type: 'post',
    dataType: 'json'
  })
  .done(function(res) {
    if(res.status == 1){
      swal('Success','Section Published','success');
      datatable.reload();
    } else {
      swal('Error','Error when publishing the section','error');
    }
  })
  .fail(function(thrownError) {
    swal("Fatal Error", thrownError.textResponse,'error');
  });

}