var marker;
var map;
var inpLat;
var inpLng;
var current_setting;

$(document).ready(function() {
	$.getJSON(siteURL + 'common/get_website_setting', function(json, textStatus) {
		if(json.status == 1){
			current_setting = json.message.settings;

			initAutocomplete(current_setting);
		} else {

		}
	});

	$(".form_update").ajaxForm({
		url: $(".form_update").attr('action'),
		type: 'post',
		dataType: 'json',
		success: function(res){
			$(".form_update .btn-submit").removeAttr("disabled");
			$(".form_update .btn-submit i").remove();
            console.log(res);
			if(res.status == 1){
				swal({
					title: "Saved",
					html: 'Website Setting Saved',
					type: "success",
					showCloseButton: true,
					showConfirmButton: true,
					showCancelButton: true,
				}).then((result)=>{
					window.location.href = window.location.href;
				});
			} else {
				swal({
					title: "Error",
					html: res.message.error.message,
					type: "error",
					confirmButtonText: "Recheck form"
				});
			}
		}, error: function(errorThrown){
			$(".form_update .btn-submit").removeAttr("disabled");
			$(".form_update .btn-submit i").remove();

			swal({
				title: "Fatal Error",
				html: errorThrown.responseText,
				type: "error",
			});
		}
	});
});

function initAutocomplete(setting) {
    // console.log(setting);
	var inpLat = document.getElementById("lat");
	var inpLng = document.getElementById("lng");
	var markers = [];

	var map = new google.maps.Map(document.getElementById('map'), {
		center: {
			lat: parseFloat(setting.lat),
			lng: parseFloat(setting.lng)
		},
		zoom: 13,
		streetViewControl: false,
		mapTypeId: 'roadmap'
	});

    var marker = new google.maps.Marker({
		map: map,
		draggable: true,
		position: {
			lat: parseFloat(setting.lat),
			lng: parseFloat(setting.lng)
		}
    });


    markers.push(marker);
    inpLat.value = marker.getPosition().lat();
            inpLng.value = marker.getPosition().lng();

            console.log(marker);
            marker.addListener('dragend', function (evt) {
                console.log(evt);
                inpLat.value = marker.getPosition().lat();
                inpLng.value = marker.getPosition().lng();
            });

	// Create the search box and link it to the UI element.
	var input = document.getElementById('map-search');
	var searchBox = new google.maps.places.SearchBox(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});

    // Listen for the event fired when the user selects a prediction and retrieve more details for that place.
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
            return;
		}

		// Clear out the old markers.
		markers.forEach(function(marker) {
			marker.setMap(null);
		});

		markers = [];

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
            if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
            }

            // Create a marker for each place.
            var marker = new google.maps.Marker({
				map: map,
				draggable: true,
				title: place.name,
				position: place.geometry.location
            });

            inpLat.value = marker.getPosition().lat();
            inpLng.value = marker.getPosition().lng();

            markers.push(marker);

            if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
            } else {
				bounds.extend(place.geometry.location);
            }
		});
		map.fitBounds(bounds);
	});
}