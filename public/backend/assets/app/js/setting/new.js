var marker;
var map;
var inpLat;
var inpLng;

$(document).ready(function() {
	initAutocomplete();

	$(".form_create").ajaxForm({
		url: $(".form_create").attr('action'),
		type: 'post',
		dataType: 'json',
		success: function(res){
			$(".form_create .btn-submit").removeAttr("disabled");
			$(".form_create .btn-submit i").remove();

			if(res.status == 1){
				swal({
					title: "Saved",
					html: 'Website Setting Saved',
					type: "success",
					showCloseButton: true,
					showConfirmButton: true,
					showCancelButton: true,
				}).then((result)=>{
					window.location.href = window.location.href;
				});
			} else {
				swal({
					title: "Error",
					html: res.message.error,
					type: "error",
					confirmButtonText: "Recheck form"
				});
			}
		}, error: function(errorThrown){
			$(".form_create .btn-submit").removeAttr("disabled");
			$(".form_create .btn-submit i").remove();

			swal({
				title: "Fatal Error",
				html: errorThrown.responseText,
				type: "error",
			});
		}
	});
});

function initAutocomplete() {
	var inpLat = document.getElementById("lat");
	var inpLng = document.getElementById("lng");

	var map = new google.maps.Map(document.getElementById('map'), {
		center: {
			lat: -6.196923799999999,
			lng: 106.82824370000003
		},
		zoom: 13,
		mapTypeId: 'roadmap'
	});

	// Create the search box and link it to the UI element.
	var input = document.getElementById('map-search');
	var searchBox = new google.maps.places.SearchBox(input);

	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
	});

    var markers = [];

    // Listen for the event fired when the user selects a prediction and retrieve more details for that place.
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
            return;
		}

		// Clear out the old markers.
		markers.forEach(function(marker) {
			marker.setMap(null);
		});

		markers = [];

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
            if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
            }

            // Create a marker for each place.
            var marker = new google.maps.Marker({
				map: map,
				draggable: true,
				title: place.name,
				position: place.geometry.location
            });

            inpLat.value = marker.getPosition().lat();
            inpLng.value = marker.getPosition().lng();

            marker.addListener('dragend', function (evt) {
				inpLat.value = marker.getPosition().lat();
				inpLng.value = marker.getPosition().lng();
            });

            markers.push(marker);

            if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
            } else {
				bounds.extend(place.geometry.location);
            }
		});
		map.fitBounds(bounds);
	});
}