	$(document).ready(function() {

	$("#btn-draft").click(function(e){
		$("input[name=status]").val(0);
		// $(".form_update").submit();
	});

	$("#btn-publish").click(function(e){
		$("input[name=status]").val(1);
		// $(".form_update").submit();
	});

	$(".form_update").ajaxForm({
		url: $(".form_update").attr('action'),
		type: 'post',
		dataType: 'json',
		success: function(res){
			$(".form_update .btn-submit").removeAttr("disabled");
			$(".form_update .btn-submit i").remove();
			if(res.status == 1){
				swal({
				   title: "Success",
				   html: 'Slide <b>#' + res.message.slide_id + '</b> has been updated.',
				   type: "success",
				   showCloseButton: true,
				   showConfirmButton: true,
				   showCancelButton: true,
				   cancelButtonText: "Stay in this page",
				   confirmButtonText: "Go to Slide List"
				}).then((result)=>{
					if(result.value){
						window.location.href = siteURL + "slider";
					} else {
						window.location.href = window.location.href;
					}
				});
			} else {
				swal({
					title: "Error DB",
					html: res.message.error.message,
					type: "error",
					showCancelButton: true,
					cancelButtonText: "Go to Slide List",
					confirmButtonText: "Recheck form"
				}).then((result)=>{
					if(result.value){
					} else {
						window.location.href = siteURL + 'slider';
					}
				});
			}
		}, error: function(errorThrown){
			swal({
				title: "Fatal Error",
				html: errorThrown.responseText,
				type: "error",
				showCancelButton: true,
				cancelButtonText: "Back to Slide List",
				confirmButtonText: "Refresh Page"
			}).then((result)=>{
				if(result.value){
					window.location.href = window.location.href;
				} else {
					window.location.href = siteURL + "slider";
				}
			});
		}
	});
});