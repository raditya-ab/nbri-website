$(document).ready(function() {
	$("#btn-draft").click(function(e){
		$("input[name=status]").val(0);
		// $(".form_create").submit();
	});

	$("#btn-publish").click(function(e){
		$("input[name=status]").val(1);
		// $(".form_create").submit();
	});

	$(".form_create").ajaxForm({
		url: $(".form_create").attr('action'),
		type: 'post',
		dataType: 'json',
		success: function(res){
			$(".form_create .btn-submit").removeAttr("disabled");
			$(".form_create .btn-submit i").remove();

			if(res.status == 1){
				swal({
					title: "Saved",
					html: 'Slide <b>#' + res.message.slide_id + '</b>.',
					type: "success",
					showCloseButton: true,
					showConfirmButton: true,
					showCancelButton: true,
					cancelButtonText: "Back to Slide List",
					confirmButtonText: "Add New Data"
				}).then((result)=>{
					if(result.value){
						window.location.href = window.location.href;
					} else {
						window.location.href = siteURL + "slider";
					}
				});
			} else {
				console.log(res);
				swal({
					title: "Error DB",
					html: res.message.error,
					type: "error",
					showCancelButton: true,
					cancelButtonText: "Recheck form",
					confirmButtonText: "Add New Data"
				}).then((result)=>{
					if(result.value){
						window.location.href = window.location.href;
					}
				});
			}
		}, error: function(errorThrown){
			$(".form_create .btn-submit").removeAttr("disabled");
			$(".form_create .btn-submit i").remove();

			swal({
				title: "Fatal Error",
				html: errorThrown.responseText,
				type: "error",
				showCancelButton: true,
				cancelButtonText: "Back to Slide List",
				confirmButtonText: "Add New Data"
			}).then((result)=>{
				if(result.value){
					window.location.href = window.location.href;
				} else {
					window.location.href = siteURL + "slider";
				}
			});
		}
	});
});