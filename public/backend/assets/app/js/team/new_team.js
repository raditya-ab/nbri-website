$(document).ready(function() {
	tinymce.init({
        selector: '#content',
        mode: 'textareas',
        branding: true,
        relative_urls: false,
        remove_script_hosts: false,
        document_base_url: siteURL,
        custom_undo_redo_levels: 10,
        plugins: 'autolink autoresize autosave code codesample contextmenu hr help image imagetools link lists paste preview searchreplace media table',
        autoresize_bottom_margin: 50,
        autoresize_max_height: 500,
        autoresize_min_height: 350,
        autosave_retention: "20m",
        toolbar: 'newdocument | formatselect fontselect fontsizeselect removeformat | bold italic underline strikethrough | subscript superscript | alignleft aligncenter alignright alignjustify | outdent indent | blockquote | hr | bullist numlist | table tablecellprops tablemergedcellslink unlink | image media | preview code help |',
        menu: {
            file: {title: 'File', items: 'newdocument'},
            edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
            insert: {title: 'Insert', items: 'link image | template hr'},
            view: {title: 'View', items: 'visualaid'},
            format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
            tools: {title: 'Tools', items: 'spellchecker code'}
        },
        entity_encoding : "named",
        contextmenu: "cut copy paste pastetext | link image inserttable | cell row column deletetable",
        image_caption: true,
        image_description: false,
        imagetools_cors_hosts: ['localhost','n-bri.org','nbri.raditya.site','n-bri.local'],
        images_upload_url: siteURL + "editor_image_handler/uploads_image_content/member",
        images_upload_base_path: '../',
        video_template_callback: function(data) {
           return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
         },
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });

	$("#name").on('blur', function(e){
		var name = $(this).val();

		if( $(this).val().trim().length > 0 ){
			setTimeout(function(){
				$("#slug_link").html('<i class="la la-spinner la-spin"></i>');
				$.ajax({
					url: siteURL+'common/generate_slug/team',
					type: 'POST',
					dataType: 'json',
					data: {param: name},
				})
				.done(function(resp) {
					$("#slug").val(resp.data);
				})
				.fail(function() {
					console.log("error");
				});

			}, 500)
		} else {
			$("#slug").val('');
		}
	});

	$("#btn-publish").click(function(e){
		$("input[name=status]").val(1);
		// $(".form_create").submit();
	});

	$(".form_create").ajaxForm({
		url: siteURL + 'team/add',
		type: 'post',
		dataType: 'json',
		// clearForm: true,
		success: function(res){
			if(res.status == 1){
				swal({
					title: "Success",
					html: 'Member <b>#' + res.message.team_id + '</b>, Member Slug <b>' + res.message.team_slug + '</b>.',
					type: "success",
					showCloseButton: true,
					showConfirmButton: true,
					showCancelButton: true,
					cancelButtonText: "Back to Member List",
					confirmButtonText: "Add New Data"
				}).then((result)=>{
					if(result.value){
						window.location.href = window.location.href;
					} else {
						window.location.href = siteURL + "team";
					}
				});
			} else {
				swal({
					title: "Error",
					html: res.message.error,
					type: "error",
					showCancelButton: false,
					confirmButtonText: "Recheck form",
					// confirmButtonText: "Add New Data"
				});
			}
		}, error: function(errorThrown){
			$(".form_create .btn-submit").removeAttr("disabled");
			$(".form_create .btn-submit i").remove();

			swal({
				title: "Fatal Error",
				html: errorThrown.textResponse,
				type: "error",
				showCancelButton: true,
				cancelButtonText: "Back to Member List",
				confirmButtonText: "Add New Data"
			}).then((result)=>{
				if(result.value){
					window.location.href = window.location.href;
				} else {
					window.location.href = siteURL + "member";
				}
			});
		}
	});

	// var forms = new Dropzone(".form_create",{
	// 	url: $(".form_create").attr('action'),
	//     paramName: 'inputfile',
	//     // chunking: true,
	//     acceptedFiles: 'image/*',
	//     maxFiles: 1,
	//     parallelUploads: 10,
	//     autoProcessQueue: false, // Make sure the files aren't queued until manually added,
	//     previewsContainer: '#dropzone-preview',
	//     clickable: "#dropzone-preview", // Define the element that should be used as click trigger to select files.
	//     addRemoveLinks: true,
	//     maxFileSize: 50,
	//     createImageThumbnails: true,
	//     resizeHeight: 600,
	//     addRemoveLinks: true,
	//     dictRemoveFile: "",
	//     dictMaxFilesExceeded: "Maximum Number of Uploaded File Has Exceeded",
	//     dictDefaultMessage: "",
	//     init: function() {
	//         var myForm = this;
	//         var imageCounter = 0;

	//         var submitButton = $(".btn-submit");
	//         var btnID;

	//         submitButton.on("click", function(e) {
	//             e.preventDefault();
	//             e.stopPropagation();

	//             btnID = $(this).attr('id');

	//             $(this).prop("disabled", "disabled");
	//             $(this).append('<i class="fa fa-spinner fa-spin"></i>');
	//             // console.log("image : " + imageCounter);

	//             if (myForm.getQueuedFiles().length > 0) {
	//                 myForm.processQueue();
	//             } else {
	//                 $(".form_create").submit();
	//             }
	//         });

	//         this.on("addedfile", function(file) {
	//             $(".dz-message").hide();
	//             var removeButton = Dropzone.createElement("<div style='width: 100%; text-align: center'><button class='btn btn-sm btn-danger' style='margin-top: 5px'><i class='fa fa-trash'></i></button></div>");

	//             removeButton.addEventListener("click", function(e) {
	//                 e.preventDefault();
	//                 e.stopPropagation();
	//                 myForm.removeFile(file);
	//             });

	//             file.previewElement.appendChild(removeButton);
	//         });

	//         this.on("maxfilesexceeded", function (file) {
	//             myForm.removeFile(file);
	//         })

	//         this.on("removedfile", function() {
	//             if(myForm.getQueuedFiles().length == 0){
	//             	$(".dz-message").show();
	//             }
	//         })

	//         this.on("sending", function(file, xhr, formData) {

	//             $(".help-block").hide();
	//             console.log("SINI");
	//             console.log(xhr);
	//             console.log(formData);
	//             if($("input[name=own_meta]").val() == '0'){
	//             	tinyMCE.triggerSave();
	//             	$("#metatitle").val($("#title").val());
	//             	$("#metadescription").val($("#content").val());
	//             }
	//             var formDataArray = $("#form_create").serializeArray();
	//             for(var i = 0; i < formDataArray.length; i++){
	//                 var formDataItem = formDataArray[i];
	//                 formData.append(formDataItem.name, formDataItem.value);
	//             }
	//         });

	//         this.on("success", function(files, response) {
	//             if (forms.getUploadingFiles().length === 0 && forms.getQueuedFiles().length === 0) {
	//                if(res.status == 1){
	//                	swal({
	//                		title: "Saved",
	//                		html: 'Article <b>#' + res.message.article_id + '</b>, Article Slug <b>' + res.message.article_slug + '</b>.',
	//                		type: "success",
	//                		showCloseButton: true,
	//                		showConfirmButton: true,
	//                		showCancelButton: true,
	//                		cancelButtonText: "Back to Article List",
	//                		confirmButtonText: "Add New Data"
	//                	}).then((result)=>{
	//                		if(result.value){
	//                			window.location.href = window.location.href;
	//                		} else {
	//                			window.location.href = siteURL + "article";
	//                		}
	//                	});
	//                } else {
	//                	swal({
	//                		title: "Error",
	//                		html: res.message.error,
	//                		type: "error",
	//                		showCancelButton: true,
	//                		cancelButtonText: "Recheck form",
	//                		confirmButtonText: "Add New Data"
	//                	}).then((result)=>{
	//                		if(result.value){
	//                			window.location.href = window.location.href;
	//                		}
	//                	});
	//                }
	//             }
	//         });

	//         this.on("complete", function() {
	//             $(".form_create #btn-publish").removeAttr("disabled");
	//             $(".form_create #btn-publish").html('Save');
	//         });

	//         this.on("error", function(files, errorMessage, xhr) {
	//         	console.log(files);
	//         	myForm.removeFile(files);
	//             $(".form_create").find("#btn-publish").html("Submit").prop('disabled',false);
	//             files.status = Dropzone.QUEUED;
	//             swal({
	//                 title: "Error",
	//                 text: errorMessage,
	//                 type: "error"
	//             })
	//         });
	//     }
	// });

});